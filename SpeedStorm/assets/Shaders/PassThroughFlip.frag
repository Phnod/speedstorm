/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor = texture(uSceneTex, vec2(texcoord.x, 1.0 - texcoord.y));
}