#version 420


float serialize(float var);
vec3 serialize(vec3 var);
float unserialize(float var);
vec3 unserialize(vec3 var);

float serialize(float var)
{
	return (var * 0.5 + 0.5);
}

float unserialize(float var)
{
	return (var + var - 1);
}