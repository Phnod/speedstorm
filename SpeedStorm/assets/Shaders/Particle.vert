/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.4

// Vertex Shader Inputs
// These are the attributes of the vertex
layout(location = 0) in vec3 in_vert;	//0
layout(location = 1) in vec2 in_uv;		//1
layout(location = 2) in vec3 in_normal;	//2

// Uniforms
// Constants throughout the entire pipeline
// These values are sent from C++ (glsendUniform*)
uniform mat4 uProj;
uniform mat4 uModel;

out vec2 texcoord;
out vec3 norm;
//out vec3 pos;
//out vec4 viewSpace;
//out mat3 normalView;

void main()
{
	// Pass attributes through pipeline
	texcoord = in_uv;
	norm = in_normal;

	// Note: We are NOT applying the mvp transform onto the input vertex
	gl_Position = vec4(in_vert, 1.0);
}

//uniform mat4 uModel; //Set up the matrices for the shader.
//uniform mat4 uView;
//uniform mat4 uProj;
//
//layout(location = 0)in vec3 in_vert;	//0
//layout(location = 1)in vec2 in_uv;		//1
//layout(location = 2)in vec3 in_normal;	//2
////layout(location = 3)in vec3 in_tangent;
////layout(location = 4)in vec3 in_bitangent;
////Force the matrices to be in specific slots for portability across systems. These are the same slots as defined by the mesh classes.
//
//out vec2 texcoord;
//out vec3 norm;
//out vec3 pos;
//out vec4 viewSpace;
//out mat3 normalView;
////out mat3 tangentToWorld;
//
//void main()
//{
//	texcoord = in_uv;
//	mat4 uViewModel = uView * uModel;
//	norm = mat3(uViewModel) * in_normal; // Send normals as screen-space.
//	viewSpace = uViewModel * vec4(in_vert, 1.0f);
//	//normal = (uModel * vec4(in_normal, 0.0f)).xyz; //send world-space normals, which basically amounts to colored lighting for shading tests.
//		
//	normalView = inverse(mat3(uViewModel));
//
//	gl_Position = uProj * viewSpace; //Set up the vertex data as a vec4 in view space so we can use it.
//	//R U D E - we can't have the position change (E) being mulitplied by 0.
//	//All operations happen from right to left with matrices. This is important to know.
//	pos.xyz = viewSpace.xyz;
//	
//}