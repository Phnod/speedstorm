/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
uniform float uMult;
uniform vec3 uMultVec = {1.0, 1.0, 1.0};

in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor.rgb = texture(uSceneTex, texcoord).rgb * uMultVec * vec3(uMult);
	outColor.a = 1.0;
}