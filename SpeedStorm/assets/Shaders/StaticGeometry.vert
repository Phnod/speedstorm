/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform mat4 uModel; //Set up the matrices for the shader.
uniform mat4 uView;
uniform mat4 uProj;

uniform vec4 uScale = vec4(1.0);


layout(location = 0)in vec4 in_vert;		//0
layout(location = 1)in vec4 in_uv;			//1
layout(location = 2)in vec4 in_normal;		//2
layout(location = 3)in vec4 in_tangent;		//3
layout(location = 4)in vec4 in_bitangent;	//4
//Force the data to be in specific slots for portability across systems. These are the same slots as defined by the mesh classes.

out vec2 texcoord;
out vec3 norm;
out vec3 tangent;
out vec3 bitangent;
out vec3 pos;


void main()
{
	texcoord = in_uv.xy;
	mat4 uViewModel = uView * uModel;
	norm =		mat3(uViewModel) * in_normal.xyz; // Send normals as screen-space.
	tangent =	mat3(uViewModel) * in_tangent.xyz; // Send normals as screen-space.
	bitangent =	mat3(uViewModel) * in_bitangent.xyz; // Send normals as screen-space.
	
	vec4 vertScale = in_vert * uScale;
	vertScale = uViewModel * vertScale;
		
	gl_Position = uProj * vertScale; //Set up the vertex data as a vec4 in view space so we can use it.
	//R U D E - we can't have the position change (E) being mulitplied by 0.
	//All operations happen from right to left with matrices. This is important to know.
	pos.xyz = vertScale.xyz;	
}