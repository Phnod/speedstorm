/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uDiffuseTex;
uniform sampler2D uNormalTex;
uniform sampler2D uSpecularTex;
uniform sampler2D uShinyTex;
uniform sampler2D uEmissiveTex;
uniform samplerCube uCubeMap;
uniform sampler2D uParallaxTex;
uniform float	uReflectAmount = 0.0f;
uniform bool	uParallaxActive = false;
uniform float	uParallaxScale = 0.0f;

uniform vec3 uDiffuseMult = 		{1.0, 1.0, 1.0};
uniform vec3 uAmbientMult = 		{1.0, 1.0, 1.0};
uniform vec3 uSpecularMult = 		{1.0, 1.0, 1.0};
uniform vec3 uEmissiveMult = 		{1.0, 1.0, 1.0};
uniform float uShinyMult = 			1.0;
uniform vec3 uDiffuseAdd  = 		{0.0, 0.0, 0.0};
uniform vec3 uAmbientAdd = 			{0.0, 0.0, 0.0};
uniform vec3 uSpecularAdd = 		{0.0, 0.0, 0.0};
uniform vec3 uEmissiveAdd = 		{0.0, 0.0, 0.0};
uniform float uShinyAdd = 			0.0;

uniform float uNear = 10.0f; // projection matrix's near plane
uniform float uFar = 50.0f; // projection matrix's far plane

in vec2 texcoord;	//colors
in vec3 norm;	//normals
in vec3 tangent;
in vec3 bitangent;
in vec3 pos;	//position
//in vec4 viewSpace;
in mat3 normalView;
//in vec3 viewAngle;

layout (location = 2)	out vec4 outNonDeferred;
//layout (location = 5)	out vec3 outEmissive;




//http://www.valvesoftware.com/publications/2006/SIGGRAPH06_Course_ShadingInValvesSourceEngine.pdf
//vec3 AmbientLight(const vec3 worldNormal)
//{
//	vec3 nSquared = worldNormal * worldNormal;
//	//ivec3 is a vec3 with integers
//	//as opposed to 
//	ivec3 isNegative; 
//	isNegative.x = int(worldNormal.x < 0.0 );
//	isNegative.y = int(worldNormal.y < 0.0 );
//	isNegative.z = int(worldNormal.z < 0.0 );
//	 
//	vec3 linearColor;
//	linearColor = nSquared.x * cAmbientCube[isNegative.x] +
//	nSquared.y * cAmbientCube[isNegative.y+2] +
//	nSquared.z * cAmbientCube[isNegative.z+4];
//	return linearColor;
//} 

vec2 ParallaxMapping(vec2 texcoords, vec3 viewDirection)
{
	float height = texture(uParallaxTex, texcoords).r;
	vec2 p = viewDirection.xy / viewDirection.z * (height * uParallaxScale);
	
	return vec2(texcoords.x - p.x, texcoords.y - p.y);
}

float LinearizeDepth(float depth)
{
    float z = depth * 2.0 - 1.0; // Back to NDC 
    return (2.0 * uNear * uFar) / (uFar + uNear - z * (uFar - uNear));	
}

void main()
{
	vec3 Normal = normalize(norm);
	vec3 Tangent = normalize(tangent); 
	vec3 Bitangent = normalize(bitangent);
	mat3 tangentToWorld = mat3(Tangent.x, Bitangent.x, Normal.x,
                           Tangent.y, Bitangent.y, Normal.y,
                           Tangent.z, Bitangent.z, Normal.z);

	vec2 texcoordOffset = texcoord;

	//if(uParallaxActive == true)
	//{
	//	texcoordOffset = ParallaxMapping(texcoord, Normal);
	//}

	//Standard color output
	vec4 textureColor = 	(texture(uDiffuseTex, texcoordOffset) 	+	uDiffuseAdd)	* uDiffuseMult;
	//outViewSpace.rgb =	normalize(pos.xyz) * 0.5 + 0.5;
	vec3 specularInput = 	(texture(uSpecularTex, texcoordOffset).rgb  + uSpecularAdd)	* uSpecularMult;
	float shinyInput = 		(texture(uSpecularTex, texcoordOffset).a	+ uShinyAdd)	* uShinyMult;
	outEmissive.rgb = 	(texture(uEmissiveTex, texcoordOffset).rgb	+ uEmissiveAdd) * uEmissiveMult + (uAmbientAdd * uAmbientMult * outColor.rgb);




	

	//Pack normals
	//in -> [-1, 1]
	//out -> [0, 1]	
	Normal = normalize((texture2D(uNormalTex, texcoordOffset).rgb * 2.0 - 1.0) * tangentToWorld);

	//outNormal = Bitangent * 0.5 + 0.5;
	//outNormal = norm * 0.5 + 0.5;
	//Unpack, multiply, repack
	//Also this is bad, it should be done on the CPU as Dan says


	if(uReflectAmount > 0.05)
	{
		//outColor.rgb = mix(outColor.rgb, texture(uCubeMap, outNormal).rgb, uReflectAmount);
		vec3 R = reflect(pos, normalize(Normal + Normal - 1.0));
		outColor.rgb = mix(outColor.rgb, texture(uCubeMap, R).rgb, uReflectAmount);
	}
	
	Normal = Normal * 0.5 + 0.5;
}