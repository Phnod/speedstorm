/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

// Geometry shader which turns points into billboarded quads

#version 420

layout(points) in;
layout(triangle_strip, max_vertices = 4) out;

uniform mat4 uView;
uniform mat4 uModel;
uniform mat4 uProj;

// Input from Vertex shader
//in VertexData
//{
//	vec3 normal;
//	vec3 texCoord;
//	vec3 colour;
//	vec3 lightVector;
//} vIn[]; // array size = num vertices in primitve
varying vec2 texcoord;
varying vec3 norm;

// Output from geometry shader
//out VertexData
//{
//	vec3 normal;
//	vec3 texCoord;
//	vec3 colour;
//	vec3 lightVector;
//} vOut; 


// Creates a quad of specified size around point p
void PointToQuadBillboarded(vec4 p, float size)
{
	float halfSize = size * 0.5;

	vec4 pEye = uModel * p;

	//Shift the points
	vec4 topLeft =		vec4(pEye.xy+vec2(-halfSize, halfSize), pEye.z, 1.0);
	vec4 topRight =		vec4(pEye.xy+vec2(halfSize, halfSize), pEye.z, 1.0);
	vec4 bottomLeft =	vec4(pEye.xy+vec2(-halfSize, -halfSize), pEye.z, 1.0);
	vec4 bottomRight =	vec4(pEye.xy+vec2(halfSize, -halfSize), pEye.z, 1.0);

	gl_Position = uProj * bottomLeft;
	texcoord.xy = vec2(0.0, 0.0);
	EmitVertex();

	gl_Position = uProj * topLeft;
	texcoord.xy = vec2(0.0, 1.0);
	EmitVertex();

	gl_Position = uProj * bottomRight;
	texcoord.xy = vec2(1.0, 0.0);
	EmitVertex();

	gl_Position = uProj * topRight;
	texcoord.xy = vec2(1.0, 1.0);
	EmitVertex();

	EndPrimitive();

}

void main()
{
	PointToQuadBillboarded(gl_in[0].gl_Position, 1.0);
}




