/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2


//color data
uniform mat4 ViewToShadowMap;

uniform sampler2D uScene; //We can have multiple of these for additional bound textures.
uniform sampler2D uNormalMap;
uniform sampler2D uPositionMap;
uniform sampler2D uAmbientMap;
uniform sampler2D uSpecularMap;
uniform sampler2D uShinyMap;
uniform sampler2D uEmissiveMap;
//uniform sampler2D uNormalPolyMap;
//uniform sampler2D uDepthMap;
uniform sampler2D uEdgeMap;
uniform sampler2D uShadowMap;
uniform sampler2D uSSAOMap;
uniform sampler2D uShadowTex;

//uniform vec2 uPixelSize; //x = 1 / Width, y = 1 / Height

//light data
uniform vec3 uLightDirection;
uniform vec3 uLightAmbient;
uniform vec3 uLightDiffuse;
uniform vec3 uLightSpecular;
uniform float uLightSpecularExponent = 200.0;

uniform float uRimLightSmoothStep = 0.7;
uniform vec3 uRimLightIntensity = vec3(1.0, 1.0, 1.0);

//uniform float timeLerp = 0.0;

//scalars
uniform vec2 uShadowSize = vec2(0.0009765625); //		1.0 / 1024.0
uniform float shadowMax = 0.25; //intensity of the shadow

out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;











//Get the normal from the vertex shader, which has been interpolated from the rasterizer. We now have per-pixel normals.

float Shadow_Lerp(sampler2D texmap, vec2 texcoord)
{
	float sum = 0.0;
	vec2 size = uShadowSize*0.5;
	//Left column
	sum += texture(texmap, vec2(texcoord.x - size.x, texcoord.y + size.y)).r;
	sum += texture(texmap, vec2(texcoord.x - size.x, texcoord.y - size.y)).r;
	sum -= texture(texmap, vec2(texcoord.x + size.x, texcoord.y - size.y)).r;
	sum -= texture(texmap, vec2(texcoord.x + size.x, texcoord.y + size.y)).r;
	
	return sum;
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
	//return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{

	//Get Data
	//float SSAO = texture(uSSAOMap, texcoord).r;
	vec3 textureColor = texture(uScene, texcoord).rgb;// * vec3(SSAO);
	vec3 normal = normalize(texture(uNormalMap, texcoord).xyz * 2.0 - 1.0); //Unpack
	vec3 pos = texture(uPositionMap, texcoord).xyz;
	vec3 specular = texture(uSpecularMap, texcoord).rgb;
	float shiny = texture(uShinyMap, texcoord).r;
	//float shiny = 4.0;
	//float depth = texture(uDepthMap, vec2(texcoord.x, texcoord.y)).r;
	float edgeFactor = texture(uEdgeMap, texcoord).r;
	
	
	if(length(pos) == 0.0)
	{
		discard;
	}
	
	vec3 lighting = vec3(0.0);
	//outcolor = texture(uTex, texcoord);
	outColor.rgb = uLightAmbient + texture(uAmbientMap, texcoord).rgb;
	//account for rasterizer interpolating the normals
		
	float NdotL = dot(normal, uLightDirection);

	//vec3 viewDir;	
	
	///determine Shadow  ////
	vec4 shadowCoord = ViewToShadowMap * vec4(pos, 1.0);
	
	//where is the point in the shadow map?
	//what is the depth?
	//float shadowDepth = ;

	//// Calculate Light ////
	//is there an occluder there?
	float bias = 0.001*tan(acos(NdotL)); // cosTheta is dot( n,l ), clamped between 0 and 1
	bias = clamp(bias, 0.0001 ,0.002);
	
	//0.0009765625
	const int numberOfTests = 9;
	const float shadowIncrement = 1.0 / numberOfTests;
	float amountOfShadow = 0.0f;
	
	if(texture(uShadowMap, shadowCoord.xy).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;		
	}	
	


	if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x,0.0)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x,0.0)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;		
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(0.0, uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(0.0, -uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x, uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x, -uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x, uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	
	
	if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x, -uShadowSize.y)).r > shadowCoord.z - bias) //0.0005
	{
		amountOfShadow += shadowIncrement;	
	}	

	//amountOfShadow = min(amountOfShadow * , 1.0);


	vec3 negativePos = normalize(-pos);

	if(NdotL > 0.0 && amountOfShadow > 0.0) //Calculate light
	{
		//Light is hitting the surface, so it's not a waste to finish calculations.
				
		//Compute diffuse lighting
		outColor.rgb += uLightDiffuse * NdotL * amountOfShadow * texture(uShadowTex, shadowCoord.xy).rgb;
		
		//Blinn-Phong half vector			
		float NdotHV = dot(normal, normalize(uLightDirection + negativePos));
					
		if(NdotHV > 0.0)
		{
			//Calculate specular lighting
			outColor.rgb += uLightSpecular.rgb * specular.rgb * pow(NdotHV, shiny*4) * amountOfShadow * texture(uShadowTex, shadowCoord.xy).rgb;
		}
	}
	
	
	//http://www.roxlu.com/2014/037/opengl-rim-shader
	//This helped with the rim lighting
	float vdn = 1.0 - max(dot(normal, negativePos), 0.0);	// the rim contribution
 	outColor.rgb += vec3(smoothstep(uRimLightSmoothStep, 1.0, vdn)) * uRimLightIntensity;

	outColor.rgb *= textureColor.rgb;

	
	outColor.rgb += texture(uEmissiveMap, texcoord).xyz;	
	outColor.rgb *= edgeFactor;
	outColor.a = 1.0;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// STARTING MARCO's GOOCH AND CEL SHADING SHADER ///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////


/*
uniform sampler2D u_tex;

uniform vec3 alpha; // alpha value passed in
uniform vec3 beta; // beta value passed in
uniform vec3 kCool; // cool coefficient
uniform vec3 kWarm; // warm coefficient
uniform vec3 kDif; // diffuse colour
uniform vec3 lightPos; // light position
uniform vec3 kSpec; // specular colour
uniform vec3 cameraPos; // camera position

uniform mat4 u_mv;
// Fragment Shader Inputs
in VertexData

{
	vec3 normal;
	vec3 texCoord;
	vec4 colour;
	//vec3 lightVector;
	vec3 vertex;
} vIn;

layout(location = 0) out vec4 FragColor;
//layout(location = 1) out vec3 vIn_vertex;

float serialize(float value);
//float reserialize(float value);



void main()
{
	vec3 lightVector = normalize(lightPos - vIn.vertex);   // light vector
	vec3 normalizedNormal = normalize((vIn.normal).xyz);   // normal
	float dotProduct = dot(normalizedNormal, lightVector); // dot product of light and normal
	float shininess = 100.0f; // shininess coefficient
	vec3 cam = normalize(cameraPos - vIn.vertex);

	
	// cool and warm gooch colours
	vec3 kCoolDif = kCool + alpha.x*kDif;
	vec3 kWarmDif = kWarm + alpha.x*kDif;

	vec4 diffuse = vec4((((1+dotProduct)/2) * kWarmDif) + ((1-(1+dotProduct)/2)*kCoolDif), 1.0);
	
	// specular lighting
	vec3 H = normalize(lightVector + cam);
	vec3 spec = vec3(1.0, 1.0, 1.0) * pow(max(0.0, dot(normalizedNormal, H)), shininess); // good blinn-phong

	// making cells for cel shading
	float intensity = pow(max(0.0, dot(normalizedNormal, H)), shininess) + serialize(dotProduct);
	float shadeIntensity = ceil(intensity * 4)/ 4;

	// final frag colour
	FragColor = (vec4(spec, 1.0) + diffuse) * shadeIntensity; // * shadeIntensity for cel shading, * 1.0f for no cel shading
}

*/ 

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////// END MARCO's GOOCH AND CEL SHADING SHADER ////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////