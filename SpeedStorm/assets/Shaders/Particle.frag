/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

// binding this sampler to texture unit 0
// Note: this is a glsl 4.2 feature (hence the version 420)
// If we set the binding here, we do not have to set the uniform
// for the sampler on the CPU!
layout(binding = 0) uniform sampler2D u_tex;
//layout(binding = 1) uniform sampler2D u_tex2;

uniform mat4 uModel;

uniform float uAlpha = 1.0;

// Fragment Shader Inputs
varying vec2 texcoord;
varying vec3 norm;

layout(location = 0) out vec4 FragColor;

float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
}

void main()
{
	vec2 uv = texcoord.xy;
	vec4 source = texture(u_tex, uv);

	if(rand(vec2(texcoord.x, texcoord.y)) > source.a * uAlpha)
	{
		discard;
	}

	

	//FragColor.rgb = texture(u_tex2, uv).rgb;
	//FragColor.a = texture(u_tex, uv).a;

	FragColor.rgb = source.rgb;
	FragColor.a = 1.0;
}
