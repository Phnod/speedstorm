/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2


//precision lowp float;

//color data
uniform mat4 ViewToShadowMap;

uniform sampler2D uScene; //We can have multiple of these for additional bound textures.
uniform sampler2D uNormalMap;
uniform sampler2D uPositionMap;
uniform sampler2D uAmbientMap;
uniform sampler2D uSpecularMap;
uniform sampler2D uShinyMap;
uniform sampler2D uEmissiveMap;
//uniform sampler2D uNormalPolyMap;
uniform sampler2D uDepthMap;
uniform sampler2D uShadowMap;
uniform sampler2D uSSAOMap;
uniform sampler2D uShadowTex;

//uniform vec2 uPixelSize; //x = 1 / Width, y = 1 / Height

//light data
uniform vec3 uLightDirection;
uniform vec3 uLightAmbient;
uniform vec3 uLightDiffuse;
uniform vec3 uLightSpecular;

//light data
uniform vec3 uLightPerspPosition = vec3(0.0);

uniform float uRimLightSmoothStep = 0.7;
uniform vec3 uRimLightIntensity = vec3(1.0, 1.0, 1.0);

//uniform float timeLerp = 0.0;

uniform mat4 uProjBiasMatrixInverse;
uniform bool uShadowOrthoActive = false;

//light data
struct Light 
{
    vec3 Position;
    vec3 Color;
    
	float Constant;
    float Linear;
    float Quadratic;
    float Radius;
	float InnerRadius;
};

const int NR_LIGHTS = 64;
uniform int uNumLights = 1;
uniform Light lights[NR_LIGHTS];




//scalars
uniform vec2 uShadowSize = vec2(0.0009765625); //		1.0 / 1024.0
uniform float shadowMax = 0.25; //intensity of the shadow


uniform vec3 uCameraPos = vec3(0.0);

out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;











//Get the normal from the vertex shader, which has been interpolated from the rasterizer. We now have per-pixel normals.

float Shadow_Lerp(sampler2D texmap, vec2 texcoord)
{
	float sum = 0.0;
	vec2 size = uShadowSize * 0.5;
	//Left column
	sum += texture(texmap, vec2(texcoord.x - size.x, texcoord.y + size.y)).r;
	sum += texture(texmap, vec2(texcoord.x - size.x, texcoord.y - size.y)).r;
	sum -= texture(texmap, vec2(texcoord.x + size.x, texcoord.y - size.y)).r;
	sum -= texture(texmap, vec2(texcoord.x + size.x, texcoord.y + size.y)).r;
	
	return sum;
}

float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
	//return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	float depth =  texture(uDepthMap, texcoord).r;
	
	if(depth == 1.0)
	{
		discard;
	}
	
	vec4 pos = uProjBiasMatrixInverse * vec4(texcoord, depth, 1.0);
	pos /= pos.w;
	
	//vec3 pos = texture(uPositionMap, texcoord).xyz;
	//
	////This saves time for fragments not using with no G-Buffer data
	//if(length(pos) == 0.0)
	//{
	//	discard;
	//}
	
	vec3 textureColor = texture(uScene, texcoord).rgb;// * vec3(SSAO);
	vec3 normal = normalize(texture(uNormalMap, texcoord).xyz * 2.0 - 1.0); //Unpack
	vec3 specular = texture(uSpecularMap, texcoord).rgb;
	float shiny = texture(uShinyMap, texcoord).r;
	//float edgeFactor = texture(uEdgeMap, texcoord).r;


	



	vec3 outDiffuse	 = vec3(0.0);
	vec3 outSpecular = vec3(0.0);

	//account for rasterizer interpolating the normals

	vec3 lightDirection;
	if(uShadowOrthoActive)
	{
		lightDirection = uLightPerspPosition;
	}
	else
	{
		lightDirection = uLightPerspPosition - pos.xyz;
	}

	//float NdotL = dot(normal, uLightDirection);
	float NdotL = dot(normal, normalize(lightDirection));

	///determine Shadow  ////
	vec4 shadowCoord = ViewToShadowMap * pos;
	
	//// Calculate Light ////
	//is there an occluder there?
	float depthBias = 0.001*tan(acos(NdotL)); 
	depthBias = clamp(depthBias, 0.0001 ,0.002);
	
	
	//0.0009765625
	const int numberOfTests = 1;
	const float shadowIncrement = 1.0 / numberOfTests;
	float amountOfShadow = 0.0f;
	
	if(uShadowOrthoActive)
	{
		if(texture2D(uShadowMap, shadowCoord.xy).r > shadowCoord.z - depthBias) //0.0005
		{
			amountOfShadow += shadowIncrement;		
		}	
	}
	else
	{
		if(textureProj(uShadowMap, shadowCoord).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
		{
			amountOfShadow += shadowIncrement;		
		}
	}

	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(uShadowSize.x,0.0), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(-uShadowSize.x,0.0), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;		
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(0.0, uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(0.0, -uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(uShadowSize.x, uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(uShadowSize.x, -uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(-uShadowSize.x, uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(textureProj(uShadowMap, vec4(shadowCoord.xy + vec2(-uShadowSize.x, -uShadowSize.y), shadowCoord.zw)).r > (shadowCoord.z - depthBias) / shadowCoord.w) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	


	//if(texture(uShadowMap, shadowCoord.xy).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;		
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x,0.0)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x,0.0)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;		
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(0.0, uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(0.0, -uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x, uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(uShadowSize.x, -uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x, uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	
	//
	//if(texture(uShadowMap, shadowCoord.xy + vec2(-uShadowSize.x, -uShadowSize.y)).r > shadowCoord.z - depthBias) //0.0005
	//{
	//	amountOfShadow += shadowIncrement;	
	//}	

	//amountOfShadow = min(amountOfShadow * , 1.0);


	vec3 negativePos = normalize(-pos.xyz-uCameraPos);
	if(NdotL > 0.0 && amountOfShadow > 0.0) //Calculate light
	{
		//Light is hitting the surface, so it's not a waste to finish calculations.
				
		vec3 spotlight = textureProj(uShadowTex, shadowCoord).rgb;

		//Compute diffuse lighting
		outDiffuse += uLightDiffuse * NdotL * amountOfShadow * spotlight;
		

		//Blinn-Phong half vector			
		float NdotHV = dot(normal, normalize(negativePos+normalize(lightDirection)));
					
		if(NdotHV > 0.0)
		{
			//Calculate specular lighting
			outSpecular += uLightDiffuse.rgb * uLightSpecular.rgb * specular.rgb * pow(NdotHV, shiny*4) * amountOfShadow * spotlight;
		}
	}
	
	//if(amountOfShadow > 0.0)
	//{
	//	outDiffuse += 1.0f;
	//}






	
	vec3 viewDir = negativePos;	
	
	
	//// Calculate Light ////	
	for(int i = 0; i < uNumLights; ++i)
	{
		// Calculate distance between light source and current fragment
		float distanceD = dot(lights[i].Position - pos.xyz, lights[i].Position - pos.xyz);
		if(distanceD < dot(lights[i].Radius * 2.0, lights[i].Radius * 2.0))
		{
			float distanceDsqrt = sqrt(distanceD);
			// Diffuse
			vec3 lightDir = normalize(lights[i].Position - pos.xyz);
			vec3 diffuse = max(dot(normal, lightDir), 0.0) * lights[i].Color;
			
			// Specular
			vec3 halfwayDir = normalize(lightDir + viewDir);  
			vec3 spec = lights[i].Color.rgb * vec3(pow(max(dot(normal, halfwayDir), 0.0), shiny * 4));
			
			// Attenuation
			float attenuation = 1.0 / 
			( lights[i].Constant
			+ lights[i].Linear * distanceDsqrt
			+ lights[i].Quadratic * distanceDsqrt * distanceDsqrt);
			
			//We mess around with the attenuation so that it doesn't die off as fast farther away, but dies before it reaches the radius
			float innerRadius = lights[i].Radius * lights[i].InnerRadius;
			attenuation *= (lights[i].Radius - distanceDsqrt) / (lights[i].Radius - innerRadius);
			attenuation = clamp(attenuation, 0.0, 1.0);
			
			outDiffuse += diffuse * attenuation;
			outSpecular += spec * max((((lights[i].Radius*2.0)-distanceDsqrt) / (lights[i].Radius * 2.0)), 0.0);
		}
	}

	
	//http://www.roxlu.com/2014/037/opengl-rim-shader
	//This helped with the rim lighting
	float vdn = 1.0 - max(dot(normal, negativePos), 0.0);	// the rim contribution
 	outDiffuse.rgb += vec3(smoothstep(uRimLightSmoothStep, 1.0, vdn)) * uRimLightIntensity;
	
	outColor.rgb = (uLightAmbient + outDiffuse) * textureColor;
	outColor.rgb += outSpecular * specular.rgb;
	outColor.rgb += texture(uEmissiveMap, texcoord).rgb;	

	outColor.a = 1.0;
}
