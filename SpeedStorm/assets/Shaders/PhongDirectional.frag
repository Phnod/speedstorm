#version 420 //Version of OpenGL we're using. - 4.4

uniform vec3 uLightDirection;
//color data
uniform mat4 ViewToShadowMap;

uniform vec3 uLightAmbient;
uniform vec3 uLightDiffuse;
uniform vec3 uLightSpecular;

uniform float timeLerp = 0.0;

//scalars
uniform float uLightSpecularExponent;

uniform vec2 grain = vec2(1.0, 1.0);
uniform bool toonShade = false;
uniform bool filmGrain = false;
uniform bool invertColour = false;

uniform sampler2D uTex; //We can have multiple of these for additional bound textures.
uniform sampler2D uNormalTex;
uniform sampler2D uSpecularTex;
uniform sampler2D uEmissiveTex;
uniform sampler2D uTex2;
uniform sampler2D uShadowMap;


uniform float shadowMax = 0.45;

in vec3 norm;
in vec3 pos; //position in viewspace for the surface of the model.

out vec4 outColor; 
in vec2 texcoord;
//Get the normal from the vertex shader, which has been interpolated from the rasterizer. We now have per-pixel normals.



float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
	//return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	//outcolor = texture(uTex, texcoord);
	outColor.rgb = uLightAmbient;
	//account for rasterizer interpolating the normals
	
	//vec3 normal = normalize((2.0 * texture2D(uNormalTex, texcoord).xyz - 1.0));
	//vec3 normal = normalize(norm);
	vec3 normal = cross(normalize(norm),normalize((2.0 * texture2D(uNormalTex, texcoord).xyz - 1.0)));
	//vec3 normal = 2.0 * texture2D(uNormalTex, gl_TexCoord[0].st).rgb - 1.0;
	//vec3 normal = normalize(norm) * normalize((2.0 * texture2D(uNormalTex, texcoord).xyz - 1.0));
	
	
	float NdotL = dot(normal, uLightDirection);

	vec3 viewDir;	
	
	///determine Shadow  ////

	vec4 shadowCoord = ViewToShadowMap * vec4(pos, 1.0);
	
	//where is the point in the shadow map?
	//what is the depth?
	float shadowDepth = texture(uShadowMap, shadowCoord.xy).r;

	//// Calculate Light ////
	if(NdotL > 0.0)
	{
		//Light is hitting the surface, so it's not a waste to finish calculations.
				
		//Compute diffuse lighting
		//if(shadowDepth < shadowCoord.z - 0.0005f)
		//{
		outColor.rgb += uLightDiffuse * NdotL;
		//}
		
		//Blinn-Phong half vector
		if(shadowDepth > shadowCoord.z - 0.001)
		{
			float NdotHV = max(dot(normal, normalize(uLightDirection + normalize(-pos))), 0.0);
						
			//Calculate specular lighting
			//outColor.rgb += uLightSpecular * pow(NdotHV, uLightSpecularExponent);
			outColor.r += uLightSpecular.r * pow(NdotHV, 100.0f * texture(uSpecularTex, texcoord).r);			
			outColor.g += uLightSpecular.g * pow(NdotHV, 100.0f * texture(uSpecularTex, texcoord).g);			
			outColor.b += uLightSpecular.b * pow(NdotHV, 100.0f * texture(uSpecularTex, texcoord).b);
		}
	}
	vec4 texturecolor;
	if(timeLerp > 0.0)
	{
		texturecolor.rgb = (1.0 - timeLerp)*(texture(uTex, texcoord).rgb) + (texture(uTex2, texcoord).rgb*timeLerp);
	}
	else
	{
		texturecolor = texture(uTex, texcoord);
	}
	outColor.rgb *= texturecolor.rgb;
	outColor.a = texturecolor.a;
	
	
	
	//outColor = vec4(norm, 1.0f); //Use normals for the color instead. (This will make it look like an object-space normal map)
	//outColor = vec4(0.05f, 1.0f 0.5f, 1.0f); //Super quick temp RGBA color. Note to self: figure out how to convert from 255-space to 1-space.


	
	
	//is there an occluder there?
	if(shadowDepth < shadowCoord.z - 0.0005)// && shadowDepth >= shadowCoord.z - 0.075f)
	{
		outColor.rgb *= shadowMax;		
	}
	
	//outColor.rgb -= clamp(gl_FragCoord.z*0.25, 0.0, 0.25); 
	
	
	if(invertColour)
	{
		outColor.rgb = 1.0-outColor.rgb;
	}
	
	outColor.rgb+=texture(uEmissiveTex, texcoord).rgb;
	
	if(toonShade)
	{
		outColor = ceil(outColor * 24.0) / 24.0;
	}
	

	if(filmGrain)
	{
		//outColor *= rand(vec2(grain.x + texcoord.x, grain.y + texcoord.y)) * 0.25f + (1.0f - (0.25f * 2.0f));
		outColor *= rand(vec2(grain.x + texcoord.x, grain.y + texcoord.y));
	}
		
	//if(gl_FragCoord.z > 0.6)
	//{
	//	outColor.r = gl_FragCoord.z; 
	//	outColor.g = gl_FragCoord.z; 
	//	outColor.b = gl_FragCoord.z; 
	//}
	//outColor.g = gl_FragCoord.z; 
	//outColor.b = gl_FragCoord.z; 

}




