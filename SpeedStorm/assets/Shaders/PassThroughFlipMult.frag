/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
uniform float uMult = 1.0;
uniform vec3 uMultVec = {1.0, 1.0, 1.0};
uniform vec3 uAddVec = {0.0, 0.0, 0.0};

in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor.rgb = texture(uSceneTex, vec2(texcoord.x, 1.0 - texcoord.y)).rgb * uMultVec * vec3(uMult) + uAddVec;
	outColor.a = 1.0f;
}