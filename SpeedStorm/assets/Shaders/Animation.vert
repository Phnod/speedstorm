/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform mat4 uModel; //Set up the matrices for the shader.
uniform mat4 uView;
uniform mat4 uProj;

uniform float uInterpParam;

layout(location = 0)in vec3 in_vertA;		//0
layout(location = 1)in vec2 in_uvA;		//1
layout(location = 2)in vec3 in_normalA;	//2
layout(location = 3)in vec3 in_vertB;		//3
layout(location = 4)in vec3 in_normalB;	//4
//Force the matrices to be in specific slots for portability across systems. These are the same slots as defined by the mesh classes.

out vec2 texcoord;
out vec3 norm;
out vec3 tangent;
out vec3 bitangent;
out vec3 pos;
out vec4 viewSpace;
out mat3 normalView;

void main()
{
	vec3 vPos = mix(in_vertA, in_vertB, uInterpParam);
	vec3 vNorm = mix(in_normalA, in_normalB, uInterpParam);
	texcoord = in_uvA;
	norm = mat3(uView) * mat3(uModel) * vNorm; // Send normals as screen-space.
	viewSpace = uView * uModel * vec4(vPos, 1.0f);
	//normal = (uModel * vec4(in_normal, 0.0f)).xyz; //send world-space normals, which basically amounts to colored lighting for shading tests.
	normalView = mat3(1.0);
	
	gl_Position = uProj * viewSpace; //Set up the vertex data as a vec4 in view space so we can use it.
	//R U D E - we can't have the position change (E) being mulitplied by 0.
	//All operations happen from right to left with matrices. This is important to know.
	pos = viewSpace.xyz;
	
}