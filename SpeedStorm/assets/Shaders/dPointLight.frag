/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2


//color data
uniform sampler2D uScene;
uniform sampler2D uDiffuseMap; //We can have multiple of these for additional bound textures.
uniform sampler2D uNormalMap;
uniform sampler2D uPositionMap;
uniform sampler2D uSpecularMap;
uniform sampler2D uShinyMap;
//uniform sampler2D uDepthMap;
uniform sampler2D uEdgeMap;
uniform sampler2D uSSAOMap;

//uniform vec2 uPixelSize; //x = 1 / Width, y = 1 / Height

//light data
struct Light 
{
    vec3 Position;
    vec3 Color;
    
	float Constant;
    float Linear;
    float Quadratic;
    float Radius;
	float InnerRadius;
};

const int NR_LIGHTS = 64;
uniform int uNumLights = 1;
uniform Light lights[NR_LIGHTS];

//uniform float timeLerp = 0.0;

//scalars


uniform vec2 shadowSize = vec2(0.0009765625); //		1.0 / 1024.0
uniform float shadowMax = 0.25; //intensity of the shadow

out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;



//Get the normal from the vertex shader, which has been interpolated from the rasterizer. We now have per-pixel normals.
float rand(vec2 co)
{
    return fract(sin(dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
	//return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	//Get Data
	//float SSAO = texture(uSSAOMap, texcoord).r;
	outColor = vec4(vec3(0.0), 1.0);
	vec3 textureColor = texture(uDiffuseMap, texcoord).rgb;// * SSAO;
	vec3 normal = texture(uNormalMap, texcoord).xyz; //Unpack
		normal = normalize((normal + normal) - 1.0);
	vec3 pos = texture(uPositionMap, texcoord).xyz;
	vec3 specular = texture(uSpecularMap, texcoord).rgb;
	float shiny = texture(uShinyMap, texcoord).r;
	//float depth = texture(uDepthMap, vec2(texcoord.x, texcoord.y)).r;
	float edgeFactor = texture(uEdgeMap, texcoord).r;

	
	if(length(pos) <= 0.0)
	{
		discard;
	}
	
	vec3 lighting = vec3(0.0);		
	vec3 viewDir = -pos;	
	
	
	//// Calculate Light ////	
	for(int i = 0; i < uNumLights; ++i)
	{
		// Calculate distance between light source and current fragment
		float distanceD = dot(lights[i].Position - pos, lights[i].Position - pos);
		if(distanceD < dot(lights[i].Radius, lights[i].Radius))
		{
			distanceD = sqrt(distanceD);
			// Diffuse
			vec3 lightDir = normalize(lights[i].Position - pos);
			vec3 diffuse = max(dot(normal, lightDir), 0.0) * textureColor * lights[i].Color;
			
			// Specular
			vec3 halfwayDir = normalize(lightDir + viewDir);  
			vec3 spec;

			spec.r = lights[i].Color.r * specular.r * pow(max(dot(normal, halfwayDir), 0.0), shiny * 4);
			spec.g = lights[i].Color.g * specular.g * pow(max(dot(normal, halfwayDir), 0.0), shiny * 4);
			spec.b = lights[i].Color.b * specular.b * pow(max(dot(normal, halfwayDir), 0.0), shiny * 4);
			
			// Attenuation
			float attenuation = 1.0 / 
			( lights[i].Constant 
			+ lights[i].Linear * distanceD 
			+ lights[i].Quadratic * distanceD * distanceD );
			
			//We mess around with the attenuation so that it doesn't die off as fast farther away, but dies before it reaches the radius
			float innerRadius = lights[i].Radius * lights[i].InnerRadius;
			attenuation *= (lights[i].Radius - distanceD) / (lights[i].Radius - innerRadius);
			
			diffuse *= attenuation;
			spec *= attenuation;
			lighting += diffuse + spec;
		}
	}
	
	outColor.rgb += lighting;
	
	
	//outColor.rgb *= textureColor.rgb * texture(uSSAOMap, texcoord).r;
	outColor.rgb *= textureColor.rgb;

	outColor.rgb *= edgeFactor;
	outColor.rgb += texture(uScene, texcoord).rgb;
	
	outColor.a = 1.0;
}




