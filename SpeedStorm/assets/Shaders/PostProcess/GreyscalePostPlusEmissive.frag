/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
uniform sampler2D uEmissiveTex; 
uniform float uAmount = 1.0f;
in vec2 texcoord;
out vec4 outColor;

void main()
{
	vec4 emissiveTexture = texture(uEmissiveTex, texcoord);
	vec4 source = texture(uSceneTex, texcoord);
	source.rgb -= emissiveTexture.rgb;
	
	float luminance = 0.2989 * source.r + 0.5874 * source.g + 0.1137 * source.b ;
	outColor.rgb = mix(source.rgb, vec3(luminance), uAmount) + emissiveTexture.rgb;
	outColor.a = 1.0f;
}