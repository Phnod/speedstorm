/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 

//scalars
uniform vec2 uGrain = vec2(1.0, 1.0);
uniform float uAmount = 1.00;
uniform vec2 uPixelSize = vec2(200.0, 200.0f);
uniform vec2 uAspect = vec2(1.7777777777777777778, 1.0f);
out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;

float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
}

void main()
{
	vec3 textureColor = texture(uSceneTex, texcoord).rgb;
	vec2 texCoordRound = ceil(texcoord*uAspect*uPixelSize)/uPixelSize;
	outColor.rgb = mix(textureColor.rgb, vec3(rand(vec2(uGrain.x + texCoordRound.x, uGrain.y + texCoordRound.y))), uAmount);
	
	outColor.a = 1.0;	
}




