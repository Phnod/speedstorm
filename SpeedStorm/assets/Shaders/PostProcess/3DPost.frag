/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2
//Thanks to Dan's paper on Stereoscopic 3D

uniform sampler2D uLeftTex; 
uniform sampler2D uRightTex;
uniform sampler2D uInterlaceTex;  
uniform vec2 uResolution = vec2(1600, 900);
in vec2 texcoord;
out vec4 outColor;


void main() 
{
	vec4 leftSource = texture(uLeftTex, texcoord);	
	vec4 rightSource = texture(uRightTex, texcoord);	
	float interlaceSource = texture(uInterlaceTex, texcoord).r;

	outColor.a = 1.0;
	outColor.rgb = mix(leftSource.rgb, rightSource.rgb, interlaceSource.r);
}