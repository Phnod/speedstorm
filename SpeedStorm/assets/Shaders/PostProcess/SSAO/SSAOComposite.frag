/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
uniform sampler2D uDiffuseTex;

uniform float uAmount = 0.5f;

in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor.rgb =  texture(uDiffuseTex, texcoord).rgb * float(texture(uSceneTex, texcoord).r);
	outColor.a = 1.0;
}