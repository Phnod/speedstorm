/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uTex; //source image

uniform float uPixelSize; //1.0 / window_height

in vec2 texcoord;

out float outColor;

void main()
{
	
	//sample pixels in vertical column
	// the weights add up to 1.
	outColor = 0.0;


	outColor += texture(uTex, vec2(texcoord.x, texcoord.y - 2.0 * uPixelSize)).r * 0.15;
	outColor += texture(uTex, vec2(texcoord.x, texcoord.y -		  uPixelSize)).r * 0.20;
	outColor += texture(uTex, vec2(texcoord.x, texcoord.y					)).r * 0.30;
	outColor += texture(uTex, vec2(texcoord.x, texcoord.y +		  uPixelSize)).r * 0.20;
	outColor += texture(uTex, vec2(texcoord.x, texcoord.y + 2.0 * uPixelSize)).r * 0.15;
	
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y - 4.0 * uPixelSize)).r * 0.06;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y - 3.0 * uPixelSize)).r * 0.09;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y - 2.0 * uPixelSize)).r * 0.12;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y -		  uPixelSize)).r * 0.15;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y					)).r * 0.16;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y +		  uPixelSize)).r * 0.15;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y + 2.0 * uPixelSize)).r * 0.12;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y + 3.0 * uPixelSize)).r * 0.09;
	//outColor += texture(uTex, vec2(texcoord.x, texcoord.y + 4.0 * uPixelSize)).r * 0.06;
}