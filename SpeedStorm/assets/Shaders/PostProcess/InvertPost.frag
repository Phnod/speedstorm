/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 

in vec2 texcoord;
out vec4 outColor;

void main()
{
	vec4 source = texture(uSceneTex, texcoord);
	
	outColor.rgb = 0.75-(source.rgb*0.75);
	outColor.a = 1.0f;
}