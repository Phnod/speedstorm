/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uTopTex; 

in vec2 texcoord;
out vec4 outColor;

void main()
{
	vec4 source = texture(uTopTex, texcoord);

	outColor = source;
}