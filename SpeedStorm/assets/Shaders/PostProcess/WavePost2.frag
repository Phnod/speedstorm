/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
uniform vec2 uWaveCount = vec2(4.0, 4.0);
uniform vec2 uWaveIntensity = vec2(0.03, 0.03);
uniform vec2 uTime = vec2(0.0, 0.0);
in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor = texture(uSceneTex, texcoord + vec2(sin(texcoord.x * uWaveCount.x + uTime.x) * uWaveIntensity.x, sin(texcoord.y * uWaveCount.y + uTime.y) * uWaveIntensity.y));
}

//outColor = texture(uSceneTex, texcoord + vec2(sin((texcoord.x + texcoord.y) * uWaveCount.x + uTime.x) * uWaveIntensity.x, sin((texcoord.x + texcoord.y) * uWaveCount.y + uTime.y) * uWaveIntensity.y));
//outColor = texture(uSceneTex, texcoord + vec2(sin(texcoord.x * uWaveCount.x + uTime.x) * uWaveIntensity.x, sin(texcoord.y * uWaveCount.y + uTime.y) * uWaveIntensity.y));

