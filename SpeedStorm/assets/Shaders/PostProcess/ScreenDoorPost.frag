/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
uniform vec2 uResolution = vec2(1600, 900);
uniform vec2 uAmount = vec2(0.15, 0.15);
uniform int uModX = 4;
uniform int uModY = 4;
uniform int uThresholdX = 2;
uniform int uThresholdY = 2;
in vec2 texcoord;
out vec4 outColor;


void main() 
{
	vec4 source = texture(uSceneTex, texcoord);	

	outColor.rgb = source.rgb;
	outColor.a = 1.0;
	
	if(uAmount.x < uAmount.y)
	{
		if	(int(texcoord.x * uResolution.x) % uModX < uThresholdX)
		{
			outColor.rgb *= uAmount.x;
		}
		else if	(int(texcoord.y * uResolution.y) % uModY < uThresholdY)
		{
			outColor.rgb *= uAmount.y;
		}
	}
	else 
	{
		if	(int(texcoord.y * uResolution.y) % uModY < uThresholdY)
		{
			outColor.rgb *= uAmount.y;
		}
		else if		(int(texcoord.x * uResolution.x) % uModX < uThresholdX)
		{
			outColor.rgb *= uAmount.x;
		}
	}
	
	//outColor.rgb = mix(source.rgb, source.rgb * 0.25, (int(texcoord.y * uResolution.y) % 2));
}