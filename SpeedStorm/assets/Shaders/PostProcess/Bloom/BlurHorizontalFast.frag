/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uTex; //source image

uniform float uPixelSize; //1.0 / window_width

in vec2 texcoord;

out vec3 outColor;

void main()
{	
	//sample pixels in horizontal row
	// the weights add up to 1.
	outColor = vec3(0.0, 0.0, 0.0);
	
	outColor += texture(uTex, vec2(texcoord.x -	2.0 * uPixelSize, texcoord.y)).rgb * 0.0625;
	outColor += texture(uTex, vec2(texcoord.x -		  uPixelSize, texcoord.y)).rgb * 0.25;
	outColor += texture(uTex, vec2(texcoord.x					, texcoord.y)).rgb * 0.375;
	outColor += texture(uTex, vec2(texcoord.x +		  uPixelSize, texcoord.y)).rgb * 0.25;
	outColor += texture(uTex, vec2(texcoord.x +	2.0 * uPixelSize, texcoord.y)).rgb * 0.0625;
}