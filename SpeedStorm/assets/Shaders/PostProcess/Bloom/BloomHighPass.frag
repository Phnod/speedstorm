/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uTex; //source image
uniform sampler2D uEmissiveTex;
uniform float uThresholdLower;
uniform float uThresholdUpper;

in vec2 texcoord;

out vec3 outColor;

void main()
{
	vec3 color = texture(uTex, texcoord).rgb;
	vec3 emissiveColor = texture(uEmissiveTex, texcoord).rgb;
	
	float luminance = dot((color.rgb) + (emissiveColor.rgb * 128.0), vec3(0.299, 0.587, 0.114));

	if (luminance > uThresholdUpper)
	{
		outColor = color;
	}
	else if (luminance > uThresholdLower)
	{
		outColor = mix(vec3(0.0, 0.0, 0.0), color, (luminance-uThresholdLower) / (uThresholdUpper-uThresholdLower));
	}
	else
	{
		outColor = vec3(0.0, 0.0, 0.0);
	}
	
	
}