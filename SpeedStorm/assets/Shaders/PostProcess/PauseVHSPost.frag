/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
uniform sampler2D uGrainTex; 
uniform float uMultAmount = 0.04;
uniform float uSlideAmount;
uniform vec4 uGrain;
uniform vec2 uResolution = vec2(1600.0, 900.0);
uniform vec2 uGrainTexMult = vec2(1.0, 1.0);
uniform float uTime = 0.0;

in vec2 texcoord;
out vec4 outColor;

float rand(vec2 co)
{
    return fract(sin(1000.0 * dot(co.xy, vec2(21.5739, 43.421))) * 617284.3);
}

void main()
{
	float randomNum = rand(texcoord.yy + uTime);

	vec2 uvOffset = texcoord + vec2((randomNum - 0.5f) * uSlideAmount, 0.0);

	uvOffset.x += (sin(uTime + texcoord.y * 12.0)) * 0.01;

	vec4 source = texture2D(uSceneTex, uvOffset);
	
	vec3 colorMult = vec3(
		rand(uGrain.rr + randomNum),
		rand(uGrain.gg + randomNum),
		rand(uGrain.bb + randomNum)
		); 

	colorMult *= uMultAmount;
	colorMult -= uMultAmount * 0.5;
	//colorMult += 1.0;

	ivec2 grainSize = textureSize(uGrainTex, 0);

	vec4 grain = texture2D(uGrainTex, texcoord * uGrainTexMult * (uResolution/grainSize) + vec2(uGrain.a, uGrain.a * 0.001));
	
	outColor.rgb = source.rgb;
	outColor.rgb += colorMult.rgb;
	outColor.rgb = mix(outColor.rgb, grain.rgb, grain.a * 1.0);
	outColor.a = 1.0;
}