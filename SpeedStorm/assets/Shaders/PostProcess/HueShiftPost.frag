/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
//uniform vec3 vHSV = vec3(1.0, 1.0, -0.75);
uniform float uHue = 0.0f;
in vec2 texcoord;
out vec4 outColor;


vec3 hueAdjust(vec3 color, float hueAdjust)
{
    const vec3  kRGBToYPrime = vec3 (0.299, 0.587, 0.114);
    const vec3  kRGBToI     = vec3 (0.596, -0.275, -0.321);
    const vec3  kRGBToQ     = vec3 (0.212, -0.523, 0.311);

    const vec3  kYIQToR   = vec3 (1.0, 0.956, 0.621);
    const vec3  kYIQToG   = vec3 (1.0, -0.272, -0.647);
    const vec3  kYIQToB   = vec3 (1.0, -1.107, 1.704);

    // Convert to YIQ
    float   YPrime  = dot (color, kRGBToYPrime);
    float   I      = dot (color, kRGBToI);
    float   Q      = dot (color, kRGBToQ);

    // Calculate the hue and chroma
    float   hue     = atan (Q, I);
    float   chroma  = sqrt (I * I + Q * Q);

    // Make the user's adjustments
    hue += hueAdjust;

    // Convert back to YIQ
    Q = chroma * sin (hue);
    I = chroma * cos (hue);

    // Convert back to RGB
    vec3    yIQ   = vec3 (YPrime, I, Q);
    color.r = dot (yIQ, kYIQToR);
    color.g = dot (yIQ, kYIQToG);
    color.b = dot (yIQ, kYIQToB);

    // Save the result
    return color;
}

void main()
{
	vec4 source = texture2D(uSceneTex, texcoord);
	//source.rgb -= 1e-5;
	//source.r = min(source.r	, 0.95);
	//source.g = min(source.g	, 0.95);
	//source.b = min(source.b	, 0.95);

    // Hue
	//if(source.rgb != vec3(1.0, 1.0, 1.0)) //Figure out how to make this less cancerous later
	if(source.r < 1.0 || source.g < 1.0 || source.b < 1.0 )
	{
		vec3 hueAdjustedColor = hueAdjust(source.rgb, uHue);	
		outColor.rgb = hueAdjustedColor.rgb;
	}
	else
	{
		outColor.rgb =	source.rgb;
	}
	
	outColor.a = 1.0f;
}

//vec3 rgb2hsv(vec3 c)
//{
//    vec4 K = vec4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
//    vec4 p = mix(vec4(c.bg, K.wz), vec4(c.gb, K.xy), step(c.b, c.g));
//    vec4 q = mix(vec4(p.xyw, c.r), vec4(c.r, p.yzx), step(p.x, c.r));
//
//    float d = q.x - min(q.w, q.y);
//    float e = 1.0e-10;
//    return vec3(abs(q.z + (q.w - q.y) / (6.0 * d + e)), d / (q.x + e), q.x);
//}
//
//vec3 hsv2rgb(vec3 c)
//{
//    vec4 K = vec4(1.0, 2.0 / 3.0, 1.0 / 3.0, 3.0);
//    vec3 p = abs(fract(c.xxx + K.xyz) * 6.0 - K.www);
//    return c.z * mix(K.xxx, clamp(p - K.xxx, 0.0, 1.0), c.y);
//}
//
//void main() 
//{
//    vec4 source = texture2D(uSceneTex, texcoord);
//    vec3 fragRGB = source.rgb;
//    vec3 fragHSV = rgb2hsv(fragRGB);
//    float h = vHSV.x / 360.0;
//    fragHSV.x *= h;
//    fragHSV.yz *= vHSV.yz;
//    fragHSV.x = mod(fragHSV.x, 1.0);
//    fragHSV.y = mod(fragHSV.y, 1.0);
//    fragHSV.z = mod(fragHSV.z, 1.0);
//    fragRGB = hsv2rgb(fragHSV);
//    outColor.rgb = vec3(hsv2rgb(fragHSV));
//	outColor.a = 1.0f;
//}