/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2



uniform sampler2D uSceneTex; 
uniform float uAmount;
in vec2 texcoord;
out vec4 outColor;

void main()
{
	vec4 source = texture(uSceneTex, texcoord);

	float luminanceR = pow(source.r, uAmount);
	float luminanceG = pow(source.g, uAmount);
	float luminanceB = pow(source.b, uAmount);
	outColor.rgb = vec3(luminanceR, luminanceG, luminanceB);
	outColor.a = 1.0f;
}