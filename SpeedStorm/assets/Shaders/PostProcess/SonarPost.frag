/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex;
uniform sampler2D uDepthTex;
uniform sampler2D uEmissiveTex;

uniform mat4 uProjBiasMatrixInverse;

uniform float uTime = 0.00;
uniform vec4 uColorAdd = vec4(0.0, 0.5, 1.0, 0.00); //vec3(1.0, 0.0, 0.0); //vec3(0.0, 0.5, 1.0); //
uniform float uMultiplier = 0.05;
uniform vec4 uParameter = vec4(0.05, 0.0, 0.0, 0.0);

in vec2 texcoord;
out vec4 outColor;

float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
}

float InverseLerp(float v, float v0, float v1) 
{		
	return (v - v0) / (v1 - v0);	
}

float Pack(float param)
{
	return param * 0.5 + 0.5;
}

float Unpack(float param)
{
	return param + param - 1.0;
}

void main()
{
	// Get position
	float depth =  texture(uDepthTex, texcoord).r;
	
	{

		vec4 fragPos = uProjBiasMatrixInverse * vec4(texcoord, depth, 1.0);
		fragPos /= fragPos.w;

		float fragDistance = length(fragPos.xyz) * uParameter.x; //uMultiplier;

		float fraction = fract(fragDistance - uColorAdd.a);
		float powFraction;

		if(fraction < 0.05)
		{
			fraction = mix(1.0, 0.05, InverseLerp(fraction, 0.0, 0.05));
			powFraction = fraction;
		}
		else
		{
			powFraction = pow(fraction, 2.0);
		}

		vec3 addition = uColorAdd.rgb * powFraction * (rand(texcoord + vec2(fraction)) * 0.5 + 0.25);

		vec3 emissive = texture(uEmissiveTex, texcoord).rgb * uParameter.z;	
		vec3 color = (texture(uSceneTex, texcoord).rgb - (emissive)) * uParameter.y;

		outColor.rgb = emissive + (color + addition) * vec3(mix(fraction, 1.0, uParameter.a));// * (1.0 - pow(depth, 8.0));


	}
	outColor.a = 1.0;
}