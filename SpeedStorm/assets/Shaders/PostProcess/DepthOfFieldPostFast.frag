/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex;
uniform sampler2D uDepthTex;
 
uniform vec2 uResolution = vec2(1600.0f, 900.0f);
uniform vec2 uPixelSize = vec2(1.0/1600.0f, 1.0/900.0f);
uniform vec2 uPixelLook = vec2(200.0f, 400.0f);

uniform float uDispersal = 100.00;

uniform float blurclamp = 1.05;  // max blur amount
uniform float bias = 0.06; //aperture - bigger values for shallower depth of field
uniform float focus = 0.8f;  // this is the depth to focus at
 
in vec2 texcoord;
out vec4 outColor;

vec2 Pack(vec2 param)
{
	return param * 0.5 + 0.5;
}

vec2 Unpack(vec2 param)
{
	return param + param - 1.0;
}

void main()
{
	//Make it so that the blur happens in a circle and not an oval if the screen isn't square 
	//(Which is impossible in our game because of the minimum aspect ratio)
	float aspectRatio = uResolution.x/uResolution.y;
	vec2 aspectRatioVector = vec2(1.0,aspectRatio);
	
	float depth   = texture2D(uDepthTex, texcoord).r;
	
	//Is the fragment part of an object?
	//This saves time since it doesn't blur the background
	if(depth < 1.01)
	{
		float factor = (depth - focus);
		
		//float mouseFocus = texture2D(uDepthTex, uPixelLook / uResolution).r;
		//float factor = (depth.x - mouseFocus);
		
		vec2 dofblur = vec2(clamp(factor * bias, -blurclamp, blurclamp));
				
		vec4 col = vec4(0.0);

		//vec2 edgeBlur;
		//vec2 TexOffset = Unpack(texcoord);
		//TexOffset = abs(TexOffset);
		//if((TexOffset.x) != 0.0)
		//{
		//	float distanceToCenter = TexOffset.x;
		//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
		//	
		//	edgeBlur.x = clamp(TexOffset.x * pow(TexOffset.x, distanceToCenterSquared), 0.0, 1.0);
		//	dofblur.x = mix(dofblur.x, blurclamp, edgeBlur.x);	
		//}
		//if((TexOffset.y) != 0.0)
		//{
		//	float distanceToCenter = TexOffset.y * aspectRatio;
		//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
		//	
		//	edgeBlur.y = clamp(TexOffset.y * pow(TexOffset.y, distanceToCenterSquared), 0.0, 1.0);
		//	dofblur.y = mix(dofblur.y, blurclamp, edgeBlur.y);	
		//}

		
		col += texture2D(uSceneTex, texcoord);
		col += texture2D(uSceneTex, texcoord + (vec2( 0.15,0.37 )	* aspectRatioVector) * dofblur);
		col += texture2D(uSceneTex, texcoord + (vec2( -0.37,0.15 )	* aspectRatioVector) * dofblur);     
		col += texture2D(uSceneTex, texcoord + (vec2( 0.37,-0.15 )	* aspectRatioVector) * dofblur);         
		col += texture2D(uSceneTex, texcoord + (vec2( -0.15,-0.37 )	* aspectRatioVector) * dofblur); 
		col += texture2D(uSceneTex, texcoord + (vec2( -0.15,0.37 )	* aspectRatioVector) * dofblur);
		col += texture2D(uSceneTex, texcoord + (vec2( 0.37,0.15 )	* aspectRatioVector) * dofblur); 
		col += texture2D(uSceneTex, texcoord + (vec2( -0.37,-0.15 )	* aspectRatioVector) * dofblur);        
		col += texture2D(uSceneTex, texcoord + (vec2( 0.15,-0.37 )	* aspectRatioVector) * dofblur);
		
		col += texture2D(uSceneTex, texcoord + (vec2( 0.29,0.29 )	* aspectRatioVector) * dofblur*0.7);
		col += texture2D(uSceneTex, texcoord + (vec2( 0.4,0.0 )		* aspectRatioVector) * dofblur*0.7);       
		col += texture2D(uSceneTex, texcoord + (vec2( 0.29,-0.29 )	* aspectRatioVector) * dofblur*0.7);   
		col += texture2D(uSceneTex, texcoord + (vec2( 0.0,-0.4 )	* aspectRatioVector) * dofblur*0.7);     
		col += texture2D(uSceneTex, texcoord + (vec2( -0.29,0.29 )	* aspectRatioVector) * dofblur*0.7);
		col += texture2D(uSceneTex, texcoord + (vec2( -0.4,0.0 )	* aspectRatioVector) * dofblur*0.7);     
		col += texture2D(uSceneTex, texcoord + (vec2( -0.29,-0.29 )	* aspectRatioVector) * dofblur*0.7);   
		col += texture2D(uSceneTex, texcoord + (vec2( 0.0,0.4 )		* aspectRatioVector) * dofblur*0.7);    
		               
		outColor = col / 17.0;
	}
	else
	{
		//if part of the fog don't blur
		outColor = texture2D(uSceneTex, texcoord);
	}
	
	outColor.a = 1.0;
}