/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
uniform vec2 uResolution = vec2(1600.0f, 900.0f);
uniform vec2 uPixelSize = vec2(1.0/1600.0f, 1.0/900.0f);
uniform float uAmount = 0.0;
uniform int uModX = 3;
uniform int uModY = 3;
uniform int uThresholdX = 2;
uniform int uThresholdY = 1;
in vec2 texcoord;
out vec4 outColor;


void main() 
{
	if(int(texcoord.x * uResolution.x) % 3 == 0)
	{
		outColor.r = (texture(uSceneTex, texcoord).r + texture(uSceneTex, vec2(texcoord.x + uPixelSize.x, texcoord.y)).r + texture(uSceneTex, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y)).r) / 3.0;
	}
	if(int(texcoord.x * uResolution.x) % 3 == 1)
	{
		outColor.g = (texture(uSceneTex, vec2(texcoord.x - uPixelSize.x, texcoord.y)).g + texture(uSceneTex, texcoord).g + texture(uSceneTex, vec2(texcoord.x + uPixelSize.x, texcoord.y)).g) / 3.0;
	}
	if(int(texcoord.x * uResolution.x) % 3 == 2)
	{
		outColor.b = (texture(uSceneTex, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y)).b + texture(uSceneTex, vec2(texcoord.x - uPixelSize.x, texcoord.y)).b + texture(uSceneTex, texcoord).b) / 3.0;
	}

	if(int(texcoord.y * uResolution.y) % uModY < uThresholdY)
	{
		outColor.rgb *= uAmount;
	}

	outColor.a = 1.0;
}