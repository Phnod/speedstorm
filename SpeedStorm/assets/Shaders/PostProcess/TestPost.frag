/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex;
uniform sampler2D uPreviousSceneTex;
uniform float uAmount = 0.75f;
in vec2 texcoord;
out vec4 outColor;

void main( void ) 
{
	outColor.rgb = mix(texture(uSceneTex, texcoord), texture(uPreviousSceneTex, texcoord), uAmount).rgb;
	outColor.a = 1.0;
}