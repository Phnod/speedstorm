/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uNormalMap;
uniform sampler2D uDepthMap;
uniform vec2 uPixelSize; //x = 1 / Width, y = 1 / Height

in vec2 texcoord; //fullscreen quad

out float outEdge;

uniform float edgeThresholdNormal = 128.0; //5.0
uniform float edgeThresholdDepth = 2.24; //0.0010

float Sobel_Normal_Horizontal(sampler2D texmap, vec2 texcoord);
float Sobel_Normal_Vertical(sampler2D texmap, vec2 texcoord);
float Sobel_Depth_Horizontal(sampler2D texmap, vec2 texcoord);
float Sobel_Depth_Vertical(sampler2D texmap, vec2 texcoord);

float Sobel_Normal_Horizontal(sampler2D texmap, vec2 texcoord)
{
	vec3 sum = vec3(0.0);
	
	//Left column
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y + uPixelSize.y * 2.0)).rgb;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y + uPixelSize.y)).rgb * 4.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y)).rgb * 6.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y - uPixelSize.y)).rgb * 4.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y - uPixelSize.y * 2.0)).rgb;

	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y + uPixelSize.y * 2.0)).rgb * 2.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y + uPixelSize.y)).rgb * 8.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y)).rgb * 12.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y - uPixelSize.y)).rgb * 8.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y - uPixelSize.y * 2.0)).rgb * 2.0;

	//Right column
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y + uPixelSize.y * 2.0)).rgb;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y + uPixelSize.y)).rgb * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y)).rgb * 6.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y - uPixelSize.y)).rgb * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y - uPixelSize.y * 2.0)).rgb;

	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y + uPixelSize.y * 2.0)).rgb * 2.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y + uPixelSize.y)).rgb * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y)).rgb * 12.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y - uPixelSize.y)).rgb * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y - uPixelSize.y * 2.0)).rgb * 2.0;

	//sqrt(x * x + y * y + z * z) = length(vector)
	//x * x + y * y + z * z		= dot(sum, sum)
	float edgeFactor = dot(sum, sum);
	
	if(edgeFactor < edgeThresholdNormal)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}

float Sobel_Normal_Vertical(sampler2D texmap, vec2 texcoord)
{
	vec3 sum = vec3(0.0);
	


	//Top column
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 2.0)).rgb * 1.0;
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 2.0)).rgb * 4.0;
	sum += texture(texmap, vec2(texcoord.x						, texcoord.y + uPixelSize.y * 2.0)).rgb * 6.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 2.0)).rgb * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 2.0)).rgb * 1.0;

	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 1.0)).rgb * 2.0;
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 1.0)).rgb * 8.0;
	sum += texture(texmap, vec2(texcoord.x						, texcoord.y + uPixelSize.y * 1.0)).rgb * 12.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 1.0)).rgb * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 1.0)).rgb * 2.0;


	//Bottom column
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 2.0)).rgb * 1.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 2.0)).rgb * 4.0;
	sum -= texture(texmap, vec2(texcoord.x						, texcoord.y - uPixelSize.y * 2.0)).rgb * 6.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 2.0)).rgb * 4.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 2.0)).rgb * 1.0;

	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 1.0)).rgb * 2.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 1.0)).rgb * 8.0;
	sum -= texture(texmap, vec2(texcoord.x						, texcoord.y - uPixelSize.y * 1.0)).rgb * 12.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 1.0)).rgb * 8.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 1.0)).rgb * 2.0;
	
	//sqrt(x * x + y * y + z * z) = length(vector)
	//x * x + y * y + z * z		= dot(sum, sum)
	float edgeFactor = dot(sum, sum);
	
	if(edgeFactor < edgeThresholdNormal)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}

float Sobel_Depth_Horizontal(sampler2D texmap, vec2 texcoord)
{
	float sum = 0.0;
	
	//Left column
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y + uPixelSize.y * 2.0)).r;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y + uPixelSize.y)).r * 4.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y)).r * 6.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y - uPixelSize.y)).r * 4.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0, texcoord.y - uPixelSize.y * 2.0)).r;

	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y + uPixelSize.y * 2.0)).r * 2.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y + uPixelSize.y)).r * 8.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y)).r * 12.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y - uPixelSize.y)).r * 8.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0, texcoord.y - uPixelSize.y * 2.0)).r * 2.0;

	//Right column
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y + uPixelSize.y * 2.0)).r;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y + uPixelSize.y)).r * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y)).r * 6.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y - uPixelSize.y)).r * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0, texcoord.y - uPixelSize.y * 2.0)).r;

	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y + uPixelSize.y * 2.0)).r * 2.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y + uPixelSize.y)).r * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y)).r * 12.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y - uPixelSize.y)).r * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0, texcoord.y - uPixelSize.y * 2.0)).r * 2.0;


	if(abs(sum) < edgeThresholdDepth)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}

float Sobel_Depth_Vertical(sampler2D texmap, vec2 texcoord)
{
	float sum = 0.0;
	
	//Top column
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 2.0)).r * 1.0;
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 2.0)).r * 4.0;
	sum += texture(texmap, vec2(texcoord.x						, texcoord.y + uPixelSize.y * 2.0)).r * 6.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 2.0)).r * 4.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 2.0)).r * 1.0;

	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 1.0)).r * 2.0;
	sum += texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 1.0)).r * 8.0;
	sum += texture(texmap, vec2(texcoord.x						, texcoord.y + uPixelSize.y * 1.0)).r * 12.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y + uPixelSize.y * 1.0)).r * 8.0;
	sum += texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y + uPixelSize.y * 1.0)).r * 2.0;


	//Bottom column
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 2.0)).r * 1.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 2.0)).r * 4.0;
	sum -= texture(texmap, vec2(texcoord.x						, texcoord.y - uPixelSize.y * 2.0)).r * 6.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 2.0)).r * 4.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 2.0)).r * 1.0;

	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 1.0)).r * 2.0;
	sum -= texture(texmap, vec2(texcoord.x - uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 1.0)).r * 8.0;
	sum -= texture(texmap, vec2(texcoord.x						, texcoord.y - uPixelSize.y * 1.0)).r * 12.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 1.0	, texcoord.y - uPixelSize.y * 1.0)).r * 8.0;
	sum -= texture(texmap, vec2(texcoord.x + uPixelSize.x * 2.0	, texcoord.y - uPixelSize.y * 1.0)).r * 2.0;
	
	if(abs(sum) < edgeThresholdDepth)
	{
		return 1.0;
	}
	else
	{
		return 0.0;
	}
}


void main()
{
	//if(texture(uDepthMap, texcoord).r > 0.95)
	//{
	//	outEdge = 1.0;
	//}
	//else
	{
		//compute some edge factor	
		float depthSobel = Sobel_Depth_Horizontal(uDepthMap, texcoord) * Sobel_Depth_Vertical(uDepthMap, texcoord);
		float normalSobel = Sobel_Normal_Horizontal(uNormalMap, texcoord) * Sobel_Normal_Vertical(uNormalMap, texcoord);
		outEdge = depthSobel * normalSobel;
	}
}



