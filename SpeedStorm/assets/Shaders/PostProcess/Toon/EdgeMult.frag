/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uScene;
uniform sampler2D uEdgeMap;

in vec2 texcoord; //fullscreen quad

out vec3 outColor;

void main()
{
	outColor.rgb = texture(uScene, texcoord).rgb * vec3(texture(uEdgeMap, texcoord).r);
}


//	1	0	0	0	1
//	2	0	0	0	2
//	4	0	0	0	4
//	2	0	0	0	2	
//	1	0	0	0	1
