/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2016 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 

//scalars
uniform vec2 uGrain = vec2(1.0, 1.0);
uniform bool uMultiply = false;
uniform float uAmount = 1.00;

out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;



float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
}

void main()
{
	//Get Data
	vec3 textureColor = texture(uSceneTex, texcoord).rgb;
		
	outColor.rgb = textureColor.rgb;
	if(uMultiply == true)
	{
		outColor.rgb *= vec3(rand(vec2(uGrain.x + texcoord.x, uGrain.y + texcoord.y)));
	}
	else
	{
		outColor.rgb = mix(outColor.rgb, vec3(0.25 + 0.5 * rand(vec2(uGrain.x + texcoord.x, uGrain.y + texcoord.y))), uAmount);
	}
	
	outColor.a = 1.0;
	
}




