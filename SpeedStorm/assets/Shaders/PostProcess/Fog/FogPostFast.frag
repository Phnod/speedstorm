/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex; 
uniform sampler2D uEmissiveTex; 
//uniform sampler2D uPositionMap;
uniform sampler2D uDepthMap;
uniform mat4 uProjBiasMatrixInverse;
uniform float uAspectRatio = 1600.0/900.0;

//const vec3 fogColor = vec3(0.15, 0.15,0.15);
uniform vec3 uFogColor = vec3(0.0, 0.0, 0.0);
const float FogDensity = 0.05;
uniform float uFogNear = 30.0f;
uniform float uFogFar = 60.0f;

uniform vec2 uRange = vec2(0.3, 0.5);
uniform vec3 uPosMult = vec3(1.0);

uniform float uEmissivePass = 1.0f;

in vec2 texcoord;
out vec3 outColor;

void main()
{
	float depth =  texture(uDepthMap, texcoord).r;
		
	vec4 pos = uProjBiasMatrixInverse * vec4(texcoord, depth, 1.0);
	pos.z /= pos.w;
	//vec3 pos = texture(uPositionMap, texcoord).rgb;
	
	float rangeAdjustY = uRange.y * uAspectRatio;
	
	float xBonus = texcoord.x + texcoord.x - 1.0;
	xBonus = xBonus * xBonus * uRange.x;
	float heightBonus = (1.0 - rangeAdjustY) + (texcoord.y + texcoord.y) * rangeAdjustY + (xBonus);
	vec3 modifier = vec3(heightBonus, heightBonus * 0.995, heightBonus * 1.005);

	if(depth == 1.0)
	{
		outColor.rgb = uFogColor.rgb * modifier;
		//outColor.r = uFogColor.r * heightBonus;
		//outColor.g = uFogColor.g * heightBonus * 0.995;
		//outColor.b = uFogColor.b * heightBonus * 1.005;
	}
	else
	{		
		float dist = -pos.z;
		
		vec3 emissive = texture(uEmissiveTex, texcoord).rgb * uEmissivePass;	
		vec3 color = texture(uSceneTex, texcoord).rgb - (emissive);

		// Near - fog starts; Far - fog ends
		float fogFactor = (uFogFar - dist) / (uFogFar - uFogNear);
		fogFactor = clamp( fogFactor, 0.0, 1.0 );

		outColor.rgb = mix(uFogColor.rgb * modifier, color.rgb, fogFactor) + emissive;
	}
}