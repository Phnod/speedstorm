/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex; 
uniform sampler2D uEmissiveTex; 
//uniform sampler2D uPositionMap;
uniform sampler2D uDepthMap;

uniform sampler2D uFogTex0;
uniform sampler2D uFogTex1;
uniform float uFogTexLerpParam = 0.5;

uniform mat4 uProjBiasMatrixInverse;
uniform float uAspectRatio = 1600.0/900.0;

uniform vec3 uFogColor = vec3(0.0, 0.0, 0.0);
uniform vec3 uFogTopColor = vec3(0.0, 0.0, 0.0);
uniform vec3 uFogBottomColor = vec3(0.0, 0.0, 0.0);
uniform float uFogNear = 30.0;
uniform float uFogFar = 60.0;

uniform vec2 uTexOffsetPosAdd = vec2(0.00);
uniform vec2 uTexOffsetPosMult = vec2(0.05);
uniform vec2 uTexOffsetMult = vec2(1.0, 1.0);
uniform vec2 uOffset = vec2(0.0, 0.0);
uniform vec2 uWaveCount = vec2(4.0, 4.0);
uniform vec2 uWaveIntensity = vec2(0.03, 0.03);
uniform vec2 uTime = vec2(0.0, 0.0);

uniform vec2 uRange = vec2(0.3, 0.5);
uniform vec3 uPosMult = vec3(1.0);

uniform float uEmissivePass = 1.0f;

in vec2 texcoord;
out vec3 outColor;

float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
}

void main()
{
	float depth =  texture(uDepthMap, texcoord).r;
		
	vec4 pos = uProjBiasMatrixInverse * vec4(texcoord, depth, 1.0);
	pos /= pos.w;
	//vec3 pos = texture(uPositionMap, texcoord).rgb;
	
	if(abs(pos.z) > uFogFar)
	{
		pos.xyz *= (uFogFar / abs(pos.z));
	}
	
	float dist = length(pos.xyz * uPosMult.xyz);
	float fogFactor = (uFogFar - dist) / (uFogFar - uFogNear);

	if(fogFactor > 1.0)
	{
		outColor.rgb = texture(uSceneTex, texcoord).rgb;
	}
	else 
	{
		float rangeAdjustY = uRange.y * uAspectRatio;
		float xBonus = texcoord.x + texcoord.x - 1.0;
		xBonus = xBonus * xBonus * uRange.x;
		float heightBonus = (1.0 - rangeAdjustY) + (texcoord.y + texcoord.y) * rangeAdjustY + (xBonus);
		//xBonus = xBonus * xBonus * 0.3;
		//float heightBonus = 0.5 + texcoord.y * 1.0 + (xBonus);
		vec3 modifier = vec3(heightBonus, heightBonus * 0.995, heightBonus * 1.005);
		modifier *= (1.0 + rand(texcoord + vec2(modifier)) * 0.05);

		vec3 fogColorLerp = mix(uFogBottomColor, uFogTopColor, texcoord.y);

		vec2 offset = texcoord * uTexOffsetMult * vec2(uAspectRatio, 1.0)
		 + uOffset + (pos.xy * uTexOffsetPosMult + uTexOffsetPosAdd) + vec2(
			sin(texcoord.y * uWaveCount.x + uTime.x) * uWaveIntensity.x, 
			sin(texcoord.x * uWaveCount.y + uTime.y) * uWaveIntensity.y);
		
		vec4 textureFog = mix(texture(uFogTex0, offset), texture(uFogTex1, offset), uFogTexLerpParam);
		fogColorLerp = fogColorLerp * textureFog.rgb;

		vec3 emissive = texture(uEmissiveTex, texcoord).rgb * uEmissivePass;	
		vec3 color = texture(uSceneTex, texcoord).rgb - (emissive);

		if(fogFactor < 0.0)
		{
			//outColor.rgb = fogColorLerp.rgb * modifier;
			outColor.rgb = mix(fogColorLerp.rgb * modifier, color.rgb, (1.0-textureFog.a));
		}
		else
		{		
			//fogFactor = clamp( fogFactor, 0.0, 1.0 );
			outColor.rgb = mix(fogColorLerp.rgb * modifier, color.rgb, 1.0 - (1.0-fogFactor) * (textureFog.a)) + emissive;
			//outColor.rgb = mix(fogColorLerp.rgb * modifier, color.rgb, fogFactor * (textureFog.a)) + emissive;
		}
	}
}