/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420

uniform sampler2D uSceneTex; 
uniform sampler2D uEmissiveTex; 
//uniform sampler2D uPositionMap;
uniform sampler2D uDepthMap;
uniform mat4 uProjBiasMatrixInverse;

//const vec3 fogColor = vec3(0.15, 0.15,0.15);
uniform vec3 uFogColor = vec3(0.0, 0.0, 0.0);
const float FogDensity = 0.05;
uniform float uFogNear = 30.0f;
uniform float uFogFar = 60.0f;

in vec2 texcoord;
out vec3 outColor;

void main()
{
	float depth =  texture(uDepthMap, texcoord).r;
		
	vec4 pos = uProjBiasMatrixInverse * vec4(texcoord, depth, 1.0);
	//pos.z /= pos.w;
	//vec3 pos = texture(uPositionMap, texcoord).rgb;
	
	
	if(depth == 1.0)
	{
		outColor.rgb = uFogColor.rgb;
	}
	else
	{		
		float dist = -pos.z / pos.w;
		
		vec3 emissive = texture(uEmissiveTex, texcoord).rgb;	
		vec3 color = texture(uSceneTex, texcoord).rgb - emissive;

		// Near - fog starts; Far - fog ends
		float fogFactor = (uFogFar - dist) / (uFogFar - uFogNear);
		fogFactor = clamp( fogFactor, 0.0, 1.0 );

		outColor.rgb = mix(uFogColor.rgb, color.rgb, fogFactor) + emissive;
	}
}