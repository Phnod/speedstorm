/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 

uniform float uAmount = 1.0f;
in vec2 texcoord;
out vec4 outColor;

void main()
{
	vec4 source = texture(uSceneTex, texcoord);
	
	vec3 sepia;
	//sepia = 0.2989 * source.r + 0.5874 * source.g + 0.1137 * source.b;

	sepia.r = (source.r * 0.393) + (source.g * 0.769) + (source.b * 0.189);
    sepia.g = (source.r * 0.349) + (source.g * 0.686) + (source.b * 0.168);    
    sepia.b = (source.r * 0.272) + (source.g * 0.534) + (source.b * 0.131);

	outColor.rgb = mix(source.rgb, sepia.rgb, uAmount);
	outColor.a = 1.0f;
}