/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
uniform vec2 rOffset = vec2(-0.002, 0.0);
uniform vec2 gOffset = vec2(0.0, 0.0);
uniform vec2 bOffset = vec2(0.002, 0.0);
in vec2 texcoord;
out vec4 outColor;

void main () 
{
	vec4 rValue = texture2D(uSceneTex, texcoord + rOffset);
	vec4 gValue = texture2D(uSceneTex, texcoord + gOffset);
	vec4 bValue = texture2D(uSceneTex, texcoord + bOffset);

	// Combine the offset colors.
	outColor =	vec4(rValue.r, gValue.g, bValue.b, 1.0);
}