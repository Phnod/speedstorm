/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
//uniform float uOffset = 1.000;

uniform vec2 uAspect =  vec2(1.0, 1.7777777777777777778);
uniform float uDispersal = 2.00;


uniform float rOffset = -0.050;
uniform float gOffset =  0.000;
uniform float bOffset =  0.050;

in vec2 texcoord;
out vec4 outColor;

vec2 Pack(vec2 param)
{
	return param * 0.5 + 0.5;
}

vec2 Unpack(vec2 param)
{
	return param + param - 1.0;
}

void main () 
{
	vec2 TexOffset =  Unpack(texcoord);

	vec2 rTexOffset = TexOffset;
	vec2 gTexOffset = TexOffset;
	vec2 bTexOffset = TexOffset;
	
	vec2 absTexOffset = abs(TexOffset);
	vec2 isPositive = absTexOffset / TexOffset;

	if((TexOffset.x) != 0.0)
	{
		float distanceToCenter = absTexOffset.x * uAspect.x;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.x += rOffset * distanceToCenterSquared * isPositive.x;
		gTexOffset.x += gOffset * distanceToCenterSquared * isPositive.x;
		bTexOffset.x += bOffset * distanceToCenterSquared * isPositive.x;
	}

	if((TexOffset.y) != 0.0)
	{
		float distanceToCenter = absTexOffset.y * uAspect.y;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.y += rOffset * distanceToCenterSquared * isPositive.y;
		gTexOffset.y += gOffset * distanceToCenterSquared * isPositive.y;
		bTexOffset.y += bOffset * distanceToCenterSquared * isPositive.y;
	}

	vec4 rValue;
	vec4 gValue;
	vec4 bValue;

	rTexOffset = Pack(rTexOffset);
	gTexOffset = Pack(gTexOffset);
	bTexOffset = Pack(bTexOffset);

	rValue += texture2D(uSceneTex, rTexOffset);
	gValue += texture2D(uSceneTex, gTexOffset);
	bValue += texture2D(uSceneTex, bTexOffset);

	// Combine the offset colors.
	outColor =	vec4(rValue.r, gValue.g, bValue.b, 1.0);
}
