/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
//uniform float uOffset = 1.000;

uniform vec2 uAspect =  vec2(1.0, 1.7777777777777777778);
uniform float uDispersal = 2.00;


uniform float rOffset = -0.050;
uniform float gOffset =  0.000;
uniform float bOffset =  0.050;

in vec2 texcoord;
out vec4 outColor;

vec2 Pack(vec2 param)
{
	return param * 0.5 + 0.5;
}

vec2 Unpack(vec2 param)
{
	return param + param - 1.0;
}

void main () 
{
	vec2 TexOffset =  Unpack(texcoord);

	vec2 rTexOffset = TexOffset;
	vec2 gTexOffset = TexOffset;
	vec2 bTexOffset = TexOffset;
	
	//if(abs(TexOffset.x) > 0.0 && abs(TexOffset.y) > 0.0)
	//if((TexOffset.x) != 0.0 && (TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = length(TexOffset * uAspect);
	//	//float distanceToCenterSquared = distanceToCenter * distanceToCenter;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset = TexOffset * pow(abs(TexOffset), vec2(rOffset * distanceToCenterSquared));
	//	gTexOffset = TexOffset * pow(abs(TexOffset), vec2(gOffset * distanceToCenterSquared));
	//	bTexOffset = TexOffset * pow(abs(TexOffset), vec2(bOffset * distanceToCenterSquared));
	//}

	//if((TexOffset.x) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.x) * uAspect.x;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), rOffset * distanceToCenterSquared);
	//	gTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), gOffset * distanceToCenterSquared);
	//	bTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), bOffset * distanceToCenterSquared);
	//}
	//
	//if((TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.y) * uAspect.y;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), rOffset * distanceToCenterSquared);
	//	gTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), gOffset * distanceToCenterSquared);
	//	bTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), bOffset * distanceToCenterSquared);
	//}

	vec2 absTexOffset = abs(TexOffset);
	vec2 isPositive = absTexOffset / TexOffset;

	if((TexOffset.x) != 0.0)
	{
		float distanceToCenter = absTexOffset.x * uAspect.x;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.x += rOffset * distanceToCenterSquared * isPositive.x;
		gTexOffset.x += gOffset * distanceToCenterSquared * isPositive.x;
		bTexOffset.x += bOffset * distanceToCenterSquared * isPositive.x;
	}

	if((TexOffset.y) != 0.0)
	{
		float distanceToCenter = absTexOffset.y * uAspect.y;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.y += rOffset * distanceToCenterSquared * isPositive.y;
		gTexOffset.y += gOffset * distanceToCenterSquared * isPositive.y;
		bTexOffset.y += bOffset * distanceToCenterSquared * isPositive.y;
	}

	vec4 rValue;
	vec4 gValue;
	vec4 bValue;

	rTexOffset = Pack(rTexOffset);
	gTexOffset = Pack(gTexOffset);
	bTexOffset = Pack(bTexOffset);

	vec4 sceneR4 = texture2D(uSceneTex, rTexOffset);
	vec4 sceneR3 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250));
	vec4 sceneR2 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500));
	vec4 sceneR1 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750));
	vec4 sceneG0 = texture2D(uSceneTex, gTexOffset);
	vec4 sceneB1 = texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750));
	vec4 sceneB2 = texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500));
	vec4 sceneB3 = texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250));
	vec4 sceneB4 = texture2D(uSceneTex, bTexOffset);




	
	rValue += sceneR4 * 0.333	;
	gValue += sceneG0;
	bValue += sceneB4 * 0.333	;
	
	rValue += sceneR3 * 0.667	;
	rValue += sceneR2			;
	rValue += sceneR1 * 0.667	;
	rValue += sceneG0 * 0.333	;

	bValue += sceneB3 * 0.667	;
	bValue += sceneB2			;
	bValue += sceneB1 * 0.667	;
	bValue += sceneG0 * 0.333	;

	gValue += sceneR1 * 0.667	;
	gValue += sceneB1 * 0.667	;
	gValue += sceneR2 * 0.333	;
	gValue += sceneB2 * 0.333	;

	rValue *= 0.33333333333333333;
	gValue *= 0.33333333333333333;
	bValue *= 0.33333333333333333;
	//rValue *= 0.33333333333333333;
	//gValue *= 0.33333333333333333;
	//bValue *= 0.33333333333333333;

	// Combine the offset colors.
	outColor =	vec4(rValue.r, gValue.g, bValue.b, 1.0);
}






//	rValue += texture2D(uSceneTex, rTexOffset)							* 0.667	;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250))			;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500))	* 0.667	;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750))	* 0.333	;
//	rValue += texture2D(uSceneTex, bTexOffset)							* 0.333	;
//	
//	bValue += texture2D(uSceneTex, bTexOffset)							* 0.667	;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250))			;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500))	* 0.667	;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750))	* 0.333	;
//	bValue += texture2D(uSceneTex, rTexOffset)							* 0.333	;
//	
//	gValue += texture2D(uSceneTex, gTexOffset)									;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.250))	* 0.667	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250))	* 0.667	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.500))	* 0.333	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500))	* 0.333	;

















//rValue += texture2D(uSceneTex, rTexOffset) * 0.6;
//gValue += texture2D(uSceneTex, gTexOffset) * 0.6;
//bValue += texture2D(uSceneTex, bTexOffset) * 0.6;
//
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250)) * 0.8;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500)) * 0.6;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750)) * 0.4;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 1.000)) * 0.2;
//
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250)) * 0.8;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500)) * 0.6;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750)) * 0.4;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 1.000)) * 0.2;
//
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.250)) * 0.75;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250)) * 0.75;
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.500)) * 0.50;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500)) * 0.50;
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.750)) * 0.25;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.750)) * 0.25;













//uniform vec2 rOffset = vec2(-0.002, 0.0);
//uniform vec2 gOffset = vec2(0.0, 0.0);
//uniform vec2 bOffset = vec2(0.002, 0.0);


//rTexOffset *= pow(abs(rTexOffset), vec2(rOffset));
//gTexOffset *= pow(abs(gTexOffset), vec2(gOffset));
//bTexOffset *= pow(abs(bTexOffset), vec2(bOffset));