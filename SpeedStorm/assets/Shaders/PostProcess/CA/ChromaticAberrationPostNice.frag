/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
//uniform float uOffset = 1.000;

uniform vec2 uAspect =  vec2(1.0, 1.7777777777777777778);
uniform float uDispersal = 2.00;


uniform float rOffset = -0.050;
uniform float gOffset =  0.000;
uniform float bOffset =  0.050;

in vec2 texcoord;
out vec4 outColor;

vec2 Pack(vec2 param)
{
	return param * 0.5 + 0.5;
}

vec2 Unpack(vec2 param)
{
	return param + param - 1.0;
}

void main () 
{
	vec2 TexOffset =  Unpack(texcoord);

	vec2 rTexOffset = TexOffset;
	vec2 gTexOffset = TexOffset;
	vec2 bTexOffset = TexOffset;
	
	//if(abs(TexOffset.x) > 0.0 && abs(TexOffset.y) > 0.0)
	//if((TexOffset.x) != 0.0 && (TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = length(TexOffset * uAspect);
	//	//float distanceToCenterSquared = distanceToCenter * distanceToCenter;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset = TexOffset * pow(abs(TexOffset), vec2(rOffset * distanceToCenterSquared));
	//	gTexOffset = TexOffset * pow(abs(TexOffset), vec2(gOffset * distanceToCenterSquared));
	//	bTexOffset = TexOffset * pow(abs(TexOffset), vec2(bOffset * distanceToCenterSquared));
	//}

	//if((TexOffset.x) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.x) * uAspect.x;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), rOffset * distanceToCenterSquared);
	//	gTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), gOffset * distanceToCenterSquared);
	//	bTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), bOffset * distanceToCenterSquared);
	//}
	//
	//if((TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.y) * uAspect.y;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), rOffset * distanceToCenterSquared);
	//	gTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), gOffset * distanceToCenterSquared);
	//	bTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), bOffset * distanceToCenterSquared);
	//}

	vec2 absTexOffset = abs(TexOffset);
	vec2 isPositive = absTexOffset / TexOffset;

	if((TexOffset.x) != 0.0)
	{
		float distanceToCenter = absTexOffset.x * uAspect.x;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.x += rOffset * distanceToCenterSquared * isPositive.x;
		gTexOffset.x += gOffset * distanceToCenterSquared * isPositive.x;
		bTexOffset.x += bOffset * distanceToCenterSquared * isPositive.x;
	}

	if((TexOffset.y) != 0.0)
	{
		float distanceToCenter = absTexOffset.y * uAspect.y;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.y += rOffset * distanceToCenterSquared * isPositive.y;
		gTexOffset.y += gOffset * distanceToCenterSquared * isPositive.y;
		bTexOffset.y += bOffset * distanceToCenterSquared * isPositive.y;
	}

	vec4 rValue;
	vec4 gValue;
	vec4 bValue;

	rTexOffset = Pack(rTexOffset);
	gTexOffset = Pack(gTexOffset);
	bTexOffset = Pack(bTexOffset);

	

	vec4 sceneR8 = texture2D(uSceneTex, rTexOffset);
	vec4 sceneR7 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.125));
	vec4 sceneR6 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250));
	vec4 sceneR5 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.375));
	vec4 sceneR4 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500));
	vec4 sceneR3 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.625));
	vec4 sceneR2 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750));
	vec4 sceneR1 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.875));
	vec4 sceneG0 = texture2D(uSceneTex, gTexOffset);
	vec4 sceneB1 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.125));
	vec4 sceneB2 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250));
	vec4 sceneB3 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.375));
	vec4 sceneB4 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500));
	vec4 sceneB5 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.625));
	vec4 sceneB6 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.750));
	vec4 sceneB7 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.875));
	vec4 sceneB8 = texture2D(uSceneTex, bTexOffset);




	
	
	rValue += sceneR8 * 0.200	;
	rValue += sceneR7 * 0.400	;
	rValue += sceneR6 * 0.600	;
	rValue += sceneR5 * 0.800	;
	rValue += sceneR4			;
	rValue += sceneR3 * 0.800	;
	rValue += sceneR2 * 0.600	;
	rValue += sceneR1 * 0.400	;
	rValue += sceneG0 * 0.200	;
	
	bValue += sceneB8 * 0.200	;
	bValue += sceneB7 * 0.400	;
	bValue += sceneB6 * 0.600	;
	bValue += sceneB5 * 0.800	;
	bValue += sceneB4			;
	bValue += sceneB3 * 0.800	;
	bValue += sceneB2 * 0.600	;
	bValue += sceneB1 * 0.400	;
	bValue += sceneG0 * 0.200	;
	
	gValue += sceneG0			;
	gValue += sceneR1 * 0.800	;
	gValue += sceneB1 * 0.800	;
	gValue += sceneR2 * 0.600	;
	gValue += sceneB2 * 0.600	;
	gValue += sceneR3 * 0.400	;
	gValue += sceneB3 * 0.400	;
	gValue += sceneR4 * 0.200	;
	gValue += sceneB4 * 0.200	;

	rValue *= 0.2;
	gValue *= 0.2;
	bValue *= 0.2;


	// Combine the offset colors.
	outColor =	vec4(rValue.r, gValue.g, bValue.b, 1.0);
}





//	vec4 rValue;
//	vec4 gValue;
//	vec4 bValue;
//
//	rTexOffset = Pack(rTexOffset);
//	gTexOffset = Pack(gTexOffset);
//	bTexOffset = Pack(bTexOffset);
//
//	rValue += texture2D(uSceneTex, rTexOffset) * 0.333;
//	gValue += texture2D(uSceneTex, gTexOffset);
//	bValue += texture2D(uSceneTex, bTexOffset) * 0.333;
//	
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250)) * 0.667;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500));
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750)) * 0.667;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 1.000)) * 0.333;
//
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250)) * 0.667;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500));
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750)) * 0.667;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 1.000)) * 0.333;
//
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.250)) * 0.667;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250)) * 0.667;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.500)) * 0.333;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500)) * 0.333;






//uniform vec2 rOffset = vec2(-0.002, 0.0);
//uniform vec2 gOffset = vec2(0.0, 0.0);
//uniform vec2 bOffset = vec2(0.002, 0.0);


//rTexOffset *= pow(abs(rTexOffset), vec2(rOffset));
//gTexOffset *= pow(abs(gTexOffset), vec2(gOffset));
//bTexOffset *= pow(abs(bTexOffset), vec2(bOffset));