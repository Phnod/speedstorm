/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2

uniform sampler2D uSceneTex; 
//uniform float uOffset = 1.000;

uniform vec2 uAspect =  vec2(1.0, 1.7777777777777777778);
uniform float uDispersal = 2.00;


uniform float rOffset = -0.050;
uniform float gOffset =  0.000;
uniform float bOffset =  0.050;

in vec2 texcoord;
out vec4 outColor;

vec2 Pack(vec2 param)
{
	return param * 0.5 + 0.5;
}

vec2 Unpack(vec2 param)
{
	return param + param - 1.0;
}

void main () 
{
	vec2 TexOffset =  Unpack(texcoord);

	vec2 rTexOffset = TexOffset;
	vec2 gTexOffset = TexOffset;
	vec2 bTexOffset = TexOffset;
	
	//if(abs(TexOffset.x) > 0.0 && abs(TexOffset.y) > 0.0)
	//if((TexOffset.x) != 0.0 && (TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = length(TexOffset * uAspect);
	//	//float distanceToCenterSquared = distanceToCenter * distanceToCenter;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset = TexOffset * pow(abs(TexOffset), vec2(rOffset * distanceToCenterSquared));
	//	gTexOffset = TexOffset * pow(abs(TexOffset), vec2(gOffset * distanceToCenterSquared));
	//	bTexOffset = TexOffset * pow(abs(TexOffset), vec2(bOffset * distanceToCenterSquared));
	//}

	//if((TexOffset.x) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.x) * uAspect.x;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), rOffset * distanceToCenterSquared);
	//	gTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), gOffset * distanceToCenterSquared);
	//	bTexOffset.x = TexOffset.x * pow(abs(TexOffset.x), bOffset * distanceToCenterSquared);
	//}
	//
	//if((TexOffset.y) != 0.0)
	//{
	//	float distanceToCenter = abs(TexOffset.y) * uAspect.y;
	//	float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	//	
	//	rTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), rOffset * distanceToCenterSquared);
	//	gTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), gOffset * distanceToCenterSquared);
	//	bTexOffset.y = TexOffset.y * pow(abs(TexOffset.y), bOffset * distanceToCenterSquared);
	//}

	vec2 absTexOffset = abs(TexOffset);
	vec2 isPositive = absTexOffset / TexOffset;

	if((TexOffset.x) != 0.0)
	{
		float distanceToCenter = absTexOffset.x * uAspect.x;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.x += rOffset * distanceToCenterSquared * isPositive.x;
		gTexOffset.x += gOffset * distanceToCenterSquared * isPositive.x;
		bTexOffset.x += bOffset * distanceToCenterSquared * isPositive.x;
	}

	if((TexOffset.y) != 0.0)
	{
		float distanceToCenter = absTexOffset.y * uAspect.y;
		float distanceToCenterSquared = pow(distanceToCenter, uDispersal);
	
		rTexOffset.y += rOffset * distanceToCenterSquared * isPositive.y;
		gTexOffset.y += gOffset * distanceToCenterSquared * isPositive.y;
		bTexOffset.y += bOffset * distanceToCenterSquared * isPositive.y;
	}

	vec4 rValue;
	vec4 gValue;
	vec4 bValue;

	rTexOffset = Pack(rTexOffset);
	gTexOffset = Pack(gTexOffset);
	bTexOffset = Pack(bTexOffset);


	vec4 sceneR16 = texture2D(uSceneTex, rTexOffset);
	vec4 sceneR15 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.0625));
	vec4 sceneR14 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.1250));
	vec4 sceneR13 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.1875));
	vec4 sceneR12 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.2500));
	vec4 sceneR11 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.3125));
	vec4 sceneR10 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.3750));
	vec4 sceneR09 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.4375));
	vec4 sceneR08 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.5000));
	vec4 sceneR07 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.5625));
	vec4 sceneR06 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.6250));
	vec4 sceneR05 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.6875));
	vec4 sceneR04 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.7500));
	vec4 sceneR03 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.8125));
	vec4 sceneR02 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.8750));
	vec4 sceneR01 = texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.9375));
	vec4 sceneG00 = texture2D(uSceneTex, gTexOffset);
	vec4 sceneB01 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.0625));
	vec4 sceneB02 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.1250));
	vec4 sceneB03 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.1875));
	vec4 sceneB04 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.2500));
	vec4 sceneB05 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.3125));
	vec4 sceneB06 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.3750));
	vec4 sceneB07 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.4375));
	vec4 sceneB08 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.5000));
	vec4 sceneB09 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.5625));
	vec4 sceneB10 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.6250));
	vec4 sceneB11 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.6875));
	vec4 sceneB12 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.7500));
	vec4 sceneB13 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.8125));
	vec4 sceneB14 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.8750));
	vec4 sceneB15 = texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.9375));
	vec4 sceneB16 = texture2D(uSceneTex, bTexOffset);




	
	rValue += sceneR16 * 0.111	;
	rValue += sceneR15 * 0.222	;
	rValue += sceneR14 * 0.333	;
	rValue += sceneR13 * 0.444	;
	rValue += sceneR12 * 0.555	;
	rValue += sceneR11 * 0.666	;
	rValue += sceneR10 * 0.777	;
	rValue += sceneR09 * 0.888	;
	rValue += sceneR08			;
	rValue += sceneR07 * 0.888	;
	rValue += sceneR06 * 0.777	;
	rValue += sceneR05 * 0.666	;
	rValue += sceneR04 * 0.555	;
	rValue += sceneR03 * 0.444	;
	rValue += sceneR02 * 0.333	;
	rValue += sceneR01 * 0.222	;
	rValue += sceneG00 * 0.111	;
	
	bValue += sceneB16 * 0.111	;
	bValue += sceneB15 * 0.222	;
	bValue += sceneB14 * 0.333	;
	bValue += sceneB13 * 0.444	;
	bValue += sceneB12 * 0.555	;
	bValue += sceneB11 * 0.666	;
	bValue += sceneB10 * 0.777	;
	bValue += sceneB09 * 0.888	;
	bValue += sceneB08			;
	bValue += sceneB07 * 0.888	;
	bValue += sceneB06 * 0.777	;
	bValue += sceneB05 * 0.666	;
	bValue += sceneB04 * 0.555	;
	bValue += sceneB03 * 0.444	;
	bValue += sceneB02 * 0.333	;
	bValue += sceneB01 * 0.222	;
	bValue += sceneG00 * 0.111	;
	
	gValue += sceneG00			;
	gValue += sceneR01 * 0.888	;
	gValue += sceneB01 * 0.888	;
	gValue += sceneR02 * 0.777	;
	gValue += sceneB02 * 0.777	;
	gValue += sceneR03 * 0.666	;
	gValue += sceneB03 * 0.666	;
	gValue += sceneR04 * 0.555	;
	gValue += sceneB04 * 0.555	;
	gValue += sceneR05 * 0.444	;
	gValue += sceneB05 * 0.444	;
	gValue += sceneR06 * 0.333	;
	gValue += sceneB06 * 0.333	;
	gValue += sceneR07 * 0.222	;
	gValue += sceneB07 * 0.222	;
	gValue += sceneR08 * 0.111	;
	gValue += sceneB08 * 0.111	;

	rValue *= 0.111111111111;
	gValue *= 0.111111111111;
	bValue *= 0.111111111111;

	//rValue *= 0.33333333333333333;
	//gValue *= 0.33333333333333333;
	//bValue *= 0.33333333333333333;

	// Combine the offset colors.
	outColor =	vec4(rValue.r, gValue.g, bValue.b, 1.0);
}






//	rValue += texture2D(uSceneTex, rTexOffset)							* 0.667	;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250))			;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500))	* 0.667	;
//	rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750))	* 0.333	;
//	rValue += texture2D(uSceneTex, bTexOffset)							* 0.333	;
//	
//	bValue += texture2D(uSceneTex, bTexOffset)							* 0.667	;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250))			;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500))	* 0.667	;
//	bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750))	* 0.333	;
//	bValue += texture2D(uSceneTex, rTexOffset)							* 0.333	;
//	
//	gValue += texture2D(uSceneTex, gTexOffset)									;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.250))	* 0.667	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250))	* 0.667	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.500))	* 0.333	;
//	gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500))	* 0.333	;

















//rValue += texture2D(uSceneTex, rTexOffset) * 0.6;
//gValue += texture2D(uSceneTex, gTexOffset) * 0.6;
//bValue += texture2D(uSceneTex, bTexOffset) * 0.6;
//
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.250)) * 0.8;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.500)) * 0.6;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 0.750)) * 0.4;
//rValue += texture2D(uSceneTex, mix(rTexOffset, gTexOffset, 1.000)) * 0.2;
//
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.250)) * 0.8;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.500)) * 0.6;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 0.750)) * 0.4;
//bValue += texture2D(uSceneTex, mix(bTexOffset, gTexOffset, 1.000)) * 0.2;
//
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.250)) * 0.75;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.250)) * 0.75;
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.500)) * 0.50;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.500)) * 0.50;
//gValue += texture2D(uSceneTex, mix(gTexOffset, rTexOffset, 0.750)) * 0.25;
//gValue += texture2D(uSceneTex, mix(gTexOffset, bTexOffset, 0.750)) * 0.25;













//uniform vec2 rOffset = vec2(-0.002, 0.0);
//uniform vec2 gOffset = vec2(0.0, 0.0);
//uniform vec2 bOffset = vec2(0.002, 0.0);


//rTexOffset *= pow(abs(rTexOffset), vec2(rOffset));
//gTexOffset *= pow(abs(gTexOffset), vec2(gOffset));
//bTexOffset *= pow(abs(bTexOffset), vec2(bOffset));