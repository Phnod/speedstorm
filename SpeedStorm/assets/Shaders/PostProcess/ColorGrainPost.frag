/*
===========================================================================

SpeedStorm Shader Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#version 420 //Version of OpenGL we're using. - 4.2


uniform sampler2D uSceneTex; 

//uniform vec2 uPixelSize; //x = 1 / Width, y = 1 / Height

//uniform float timeLerp = 0.0;

//scalars
uniform vec3 uGrain = vec3(1.0, 1.0, 1.0);
uniform bool uMultiply = false;
uniform float uAmount = 1.00;

out vec4 outColor; 
in vec2 texcoord;
in vec4 viewSpace;



float rand(vec2 co)
{
    return fract(sin(1000.0*dot(co.xy ,vec2(21.5739,43.421))) * 617284.3);
	//return fract(sin(dot(co.xy ,vec2(12.9898,78.233))) * 43758.5453);
}

void main()
{
	//Get Data
	vec3 textureColor = texture(uSceneTex, texcoord).rgb;
		
	outColor.rgb = textureColor.rgb;
	//outColor.rgb *= rand(pos);
	if(uMultiply == true)
	{
		vec3 blendMix;
		blendMix.r = rand(vec2(uGrain.r + texcoord.x, uGrain.r + texcoord.y)) * 2.0 - 1.0;
		blendMix.g = rand(vec2(uGrain.g + texcoord.x, uGrain.g + texcoord.y)) * 2.0 - 1.0;
		blendMix.b = rand(vec2(uGrain.b + texcoord.x, uGrain.b + texcoord.y)) * 2.0 - 1.0;
		
		blendMix.b += 0.01;
		
		blendMix = normalize(blendMix) * 0.5 + 0.5;
		
		outColor.r = mix(outColor.r, blendMix.r, uAmount);
		outColor.g = mix(outColor.g, blendMix.g, uAmount);
		outColor.b = mix(outColor.b, blendMix.b, uAmount);
	}
	else
	{
		outColor.r = mix(outColor.r, rand(vec2(uGrain.r + texcoord.x, uGrain.r + texcoord.y)), uAmount);
		outColor.g = mix(outColor.g, rand(vec2(uGrain.g + texcoord.x, uGrain.g + texcoord.y)), uAmount);
		outColor.b = mix(outColor.b, rand(vec2(uGrain.b + texcoord.x, uGrain.b + texcoord.y)), uAmount);		
	}

	outColor.a = 1.0;
}




