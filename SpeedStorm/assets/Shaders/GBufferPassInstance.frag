#version 420

uniform sampler2D uTex;
uniform sampler2D uNormalTex;
uniform sampler2D uSpecularTex;
uniform sampler2D uEmissiveTex;

in vec2 texcoord;	//colors
in vec3 norm;	//normals
in vec3 pos;	//position
in vec4 viewSpace;
in mat3 normalView;

layout (location = 0)	out vec4 outColor; 
layout (location = 1)	out vec3 outNormal;
layout (location = 2)	out vec3 outPosition;
layout (location = 3)	out vec3 outSpecular;
layout (location = 4)	out vec3 outEmissive;
layout (location = 5)	out vec3 outWVP;

const vec3 fogColor = vec3(0.15, 0.15,0.15);
const float FogDensity = 0.05;

void main()
{
	//Standard color output
	outColor.rgb = texture(uTex, texcoord).rgb;
	outColor.a = 1.0;
	
	outEmissive.rgb = texture(uEmissiveTex, texcoord).rgb;
	outSpecular.rgb = texture(uSpecularTex, texcoord).rgb;
	
	//distance
	float dist = 0;
	float fogFactor = 0;
	
	dist = length(viewSpace);
	// 2 - fog starts; 10 - fog ends
	fogFactor = (70 - dist)/(70 - 20);
	fogFactor = clamp( fogFactor, 0.0, 1.0 );

	//if you inverse color in glsl mix function you have to
	//put 1.0 - fogFactor
	outColor.rgb = mix(fogColor.rgb, outColor.rgb, fogFactor);
	
	//Pack normals
	//in -> [-1, 1]
	//out -> [0, 1]	
	
	//mat3 transpose(inverse(gl_ModelViewMatrix))
	
	vec3 Normal = normalize(normalView * norm);
	vec3 Tangent = normalize(normalView[0]); 
	vec3 Binormal = normalize(normalView[1]);
	mat3 tangentToWorld = mat3(Tangent.x, Binormal.x, Normal.x,
                           Tangent.y, Binormal.y, Normal.y,
                           Tangent.z, Binormal.z, Normal.z);
	
	outNormal = ((texture2D(uNormalTex, texcoord).rgb * 2.0 - 1.0) * tangentToWorld) * 0.5 + 0.5;
	//outNormal = normalize(norm) * 0.5 + 0.5; 
	
	//vec3 normals = normalize(cross(normalize(norm), normalize(2.0 * texture2D(uNormalTex, texcoord).xyz - 1.0)));
	//outNormal = normalize(cross(normalize(norm), normalize(2.0 * texture2D(uNormalTex, texcoord).xyz - 1.0))) * 0.5 + 0.5;
	
	//view space positions. No need to pack!
	outPosition = pos;
}