Size: 300 16
WinPoint: 240

DOFBiasMult: 0 0.5


FogActive: 1
FogDetail: 2
FogDistance: 0 50.0 100.0
FogColor: 0 0.0 0.0 0.0
FogPos: 0 0.0
FogPos: 1 6
FogPos: 2 16
FogPos: 3 380
FogPos: 4 400


//FogActive: 1
//
//FogDetail: 10
//FogNumLayers: 1
//
//FogLayerColor: 0, 0000 1.00 1.00 1.00
//FogLayerColor: 0, 0000 0.50 0.50 0.50
//FogLayerDistance: 0, 0000 35.0 100.0
//FogLayerOffsetPosMult: 0, 0000 0.0 0.0
//FogLayerOffsetMult: 0, 0000 1.0 -1.0
//FogLayerVelocity: 0, 0000 1.0 0.0
//
////FogLayerImage: 0, 0000 ../Assets/Textures/Default/defaultTestNormal.png
////FogLayerImage: 0, 0002 ../Assets/Textures/Default/defaultTestTexture.png
//
//FogLayerImage: 0, 0000 ../Assets/Textures/sky.jpg
//
//FogLayerPos: 0, 0000 000
//FogLayerPos: 0, 0001 075
//FogLayerPos: 0, 0002 100
//FogLayerPos: 0, 0003 230
//FogLayerPos: 0, 0004 240



//BackgroundVisualFilenameDiffuse: 00 ../Assets/Textures/Retro/inWall_tex.png
//BackgroundVisualFilenameNormal: 00 ../Assets/Textures/Retro/binWall_norm.png
//BackgroundVisualFilenameSpecular: 00 ../Assets/Textures/Retro/binWall_spec.png
//BackgroundVisualFilenameEmissive: 00 ../Assets/Textures/Retro/binWall_tex_emissive.png
BackgroundVisualFilenameModel: 00 ../Assets/Models/Background/retro_snake.obj

BackgroundVisualSpecularMult: 00 0.3 0.3 0.3

BackgroundVisualPos: 00 0.0 40.0 10.0
BackgroundVisualScale: 00 5.0 5.0 2.5
BackgroundVisualLoop: 00 20.0 65.0
BackgroundVisualDiffuseMult: 00 0.2 0.2 0.6

BackgroundVisualPos: 01 20.0 40.0 10.0
BackgroundVisualDiffuseMult: 01 0.6 0.6 0.2

BackgroundVisualPos: 02 0.0 40.0 20.0
BackgroundVisualScale: 02 7.5 7.5 3.75
BackgroundVisualLoop: 02 30.0 65.0
BackgroundVisualDiffuseMult: 02 0.2 0.6 0.2

BackgroundVisualPos: 03 30.0 40.0 20.0
BackgroundVisualDiffuseMult: 03 0.6 0.2 0.6

BackgroundVisualPos: 04 0.0 40.0 35.0
BackgroundVisualScale: 04 10.0 10.0 5.0
BackgroundVisualLoop: 04 40.0 65.0
BackgroundVisualDiffuseMult: 04 0.6 0.2 0.2

BackgroundVisualPos: 05 40.0 40.0 35.0
BackgroundVisualDiffuseMult: 05 0.2 0.6 0.6

BackgroundVisualPos: 06 0.0 40.0 50.0
BackgroundVisualScale: 06 15.0 15.0 7.5
BackgroundVisualLoop: 06 60.0 60.0
BackgroundVisualDiffuseMult: 06 0.2 0.2 0.6

BackgroundVisualPos: 07 60.0 40.0 50.0
BackgroundVisualDiffuseMult: 07 0.6 0.6 0.2

BackgroundVisualPos: 08 0.0 40.0 65.0
BackgroundVisualScale: 08 20.0 20.0 10.0
BackgroundVisualLoop: 08 80.0 60.0
BackgroundVisualDiffuseMult: 08 0.2 0.6 0.2

BackgroundVisualPos: 09 80.0 40.0 70.0
BackgroundVisualDiffuseMult: 09 0.6 0.2 0.6



VisualFilenameDiffuse: 0 ../Assets/Textures/Retro/binWall_tex.png
VisualFilenameNormal: 0 ../Assets/Textures/Retro/binWall_norm.png
VisualFilenameSpecular: 0 ../Assets/Textures/Retro/binWall_specMedium.png
VisualFilenameEmissive: 0 ../Assets/Textures/Retro/binWall_tex_emissive.png
VisualFilenameModel: 0 ../Assets/Models/CastleBlockCarpet.obj

VisualChar: 0 X
VisualParam: 0 1 0 0
VisualEnv: 0 0.0
VisualDead: 0 =
VisualPos: 0 0.0 0.0 0.0

VisualChar: 1 =
VisualParam: 1 0 0 0
VisualEnv: 1 0.0
VisualDead: 1 -
VisualPos: 1 0.0 0.0 4.0
//VisualDiffuseMult: 1 0.0 0.0 0.0
VisualDiffuseMult: 1 0.15 0.15 0.15
VisualEmissiveMult: 1 0.125 0.125 0.125
VisualSpecularMult: 1 0.3 0.3 0.3

VisualChar: 2 V
VisualParam: 2 0 0 0
VisualEnv: 2 0.0
VisualDead: 2 -
VisualPos: 2 0.0 0.0 -4.0
VisualDiffuseMult: 2 0.5 0.5 0.5
VisualEmissiveMult: 2 0.25 0.25 0.25

VisualChar: 3 x
VisualParam: 3 1 1 0
VisualEnv: 3 0.0
VisualDead: 3 =
VisualPos: 3 0.0 0.0 0.0
VisualDiffuseMult: 3 1.0 1.0 1.0
VisualEmissiveMult: 3 1.0 1.0 1.0
VisualSpecularMult: 3 1.0 1.0 1.0

VisualChar: 4 d
VisualParam: 4 0 0 0
VisualEnv: 4 0.0
VisualDead: 4 =
VisualPos: 4 0.0 0.0 4.0
VisualDiffuseMult: 4 0.5 0.5 0.5
VisualEmissiveMult: 4 0.25 0.25 0.25
VisualSpecularMult: 4 0.3 0.3 0.3

VisualChar: 5 Z
VisualParam: 5 1 0 0
VisualEnv: 5 0.0
VisualDead: 5 V
VisualPos: 5 0.0 0.0 -4.0
VisualDiffuseMult: 5 0.5 0.5 0.5
VisualEmissiveMult: 5 0.25 0.25 0.25
VisualSpecularMult: 5 0.3 0.3 0.3

VisualChar: 6 Z
VisualDead: 6 V
VisualPos: 6 0.0 0.0 0.0
VisualDiffuseMult: 6 0.5 0.5 0.5
VisualEmissiveMult: 6 0.25 0.25 0.25

VisualChar: 07 X
VisualParam: 07 1 0 0
VisualEnv: 07 0.0
VisualDead: 07 =
VisualPos: 07 0.0 0.0 4.0
//VisualDiffuseMult: 07 0.0 0.0 0.0
VisualDiffuseMult: 07 0.15 0.15 0.15
VisualEmissiveMult: 07 0.125 0.125 0.125
VisualSpecularMult: 07 0.3 0.3 0.3

VisualChar: 08 x
VisualParam: 08 1 1 0

//VisualFilenameDiffuse: 7 ../Assets/Textures/Powerup/shield.png
//VisualFilenameNormal: 7 ../Assets/Textures/Default/defaultNormal.png
//VisualFilenameSpecular: 7 ../Assets/Textures/Default/defaultSpecular.png
//VisualFilenameEmissive: 7 ../Assets/Textures/Powerup/shield.png
////../Assets/Textures/Default/defaultEmissive.png
//VisualFilenameModel: 7 ../Assets/Models/Powerup/box.obj
//
//VisualChar: 7 P
//VisualParam: 7 0 0 0
//VisualDoubleShootPickup: 7 1
//VisualEnv: 7 0.0
//VisualDead: 7 -
//VisualPos: 7 0.0 0.0 0.0

//////////////////////////////////////////////////////////////////////////

EnemyFilenameDiffuse: 1 ../Assets/Textures/Enemy/red.png
EnemyFilenameNormal: 1 ../Assets/Textures/Enemy/spikeNorm.png
EnemyFilenameModel: 1 ../Assets/Models/spike.obj
//EnemyFilenameModel: 1 ../Assets/Models/Retro/Atari_wormyThing.obj


EnemyChar: 0 A
EnemyParam: 0 1 0 1
EnemyEnv: 0 0.5
EnemyDead: 0 D
EnemyPos: 0 0.0 0.0 -0.5

EnemyChar: 1 D
EnemyParam: 1 1 0 1
EnemyEnv: 1 0.0
EnemyDead: 1 -
EnemyPos: 1 0.0 0.0 -0.5

EnemyChar: 2 d
EnemyParam: 2 1 0 1
EnemyEnv: 2 0.0
EnemyDead: 2 -
EnemyPos: 2 0.0 0.0 -0.5

//////////////////////////////////////////////////////////////////////////

//BlockAmbientAdd: 0 0.0 0.0 0.0
//BlockAmbientMult: 0 1.0 1.0 1.0
//BlockDiffuseAdd: 0 0.0 0.0 0.0
////BlockDiffuseMult: 0 0.9 0.9 0.9
//BlockSpecularAdd: 0 0.0 0.0 0.0
//BlockSpecularMult: 0 1.0 1.0 1.0
//BlockEmissiveAdd: 0 0.0 0.0 0.0
//BlockEmissiveMult: 0 1.0 1.0 1.0
//BlockShinyAdd: 0 0.0
//BlockShinyMult: 0 1.0

BlockDiffuseMult: 0 1.0 1.0 1.0
BlockDiffuseMult: 1 1.0 0.9 0.9
BlockDiffuseMult: 2 0.9 1.0 0.9
BlockDiffuseMult: 3 0.9 0.9 1.0
BlockDiffuseMult: 4 0.95 0.95 0.95
BlockDiffuseMult: 5 0.9 0.9 0.9

BlockEmissiveMult: 0 1.0 1.0 1.0
BlockEmissiveMult: 1 1.0 0.9 0.9
BlockEmissiveMult: 2 1.0 1.0 0.9
BlockEmissiveMult: 3 0.9 1.0 1.0
BlockEmissiveMult: 4 0.9 0.9 0.9
BlockEmissiveMult: 5 0.8 0.8 0.8

BlockRandomAmbientAdd: 0 
BlockRandomAmbientMult: 0 
BlockRandomDiffuseAdd: 0 
BlockRandomDiffuseMult: 2 
BlockRandomSpecularAdd: 0 
BlockRandomSpecularMult: 0 
BlockRandomEmissiveAdd: 0 
BlockRandomEmissiveMult: 2 
BlockRandomShinyAdd: 0 
BlockRandomShinyMult: 0 

///////////////////////////////////////////////////////////



EyePos: 0 00.0
EyePos: 1 06.0
EyePos: 2 07.0
EyePos: 3 17.0

EyeConvergence: 0 30
EyeConvergence: 1 30
EyeConvergence: 2 30
EyeConvergence: 3 30

EyeShift: 0 0.0 0.0
EyeShift: 1 50.0 0.0
EyeShift: 2 50.0 0.0
EyeShift: 3 0.0 0.0

///////////////////////////////////////////////////////////

SoundFilename: 0 ../Assets/Sound/Effect/Level1/ControlsWS.ogg

SoundFilename: 2 ../Assets/Sound/Effect/Level1/ControlsAndTheWalls.ogg
SoundFilename: 3 ../Assets/Sound/Effect/Level1/ControlsDoNotTouchAnything.ogg
SoundFilename: 6 ../Assets/Sound/Effect/Level1/ControlsAD.ogg

SoundPos: 0 17.0
SoundPos: 2 43.0
SoundPos: 3 55.0
SoundPos: 6 100.0

SoundVolume: 0 0.5
SoundVolume: 2 0.5
SoundVolume: 3 0.5
SoundVolume: 6 0.5

/////////////////////////////////////




SpeedPos: 0 0.0
SpeedMoving: 0 0.0 0.0 0.0 0.0
SpeedPos: 1 6.0
SpeedMoving: 1 0.8 0.0 0.0 0.0
SpeedPos: 2 16.0
SpeedMoving: 2 0.8 5.0 0.0 0.0
SpeedPos: 3 18.0
SpeedMoving: 3 1.6 5.0 0.4 1.8
SpeedPos: 4 25.0
SpeedMoving: 4 2.0 5.0 0.5 3.0
SpeedPos: 5 197.0
SpeedMoving: 5 5.0 5.0 1.25 2.5

//SpeedPos: 3 95.0
//SpeedMovingSimple: 3 1.5 5.0
//SpeedPos: 4 105.0
//SpeedMovingSimple: 4 0.75 5.0
//SpeedPos: 5 115.0
//SpeedMovingSimple: 5 0.75 5.0
//SpeedPos: 6 120.0
//SpeedMovingSimple: 6 1.5 5.0
//SpeedPos: 7 125.0
//SpeedMovingSimple: 7 3.0 5.0
//SpeedPos: 8 200.0
//SpeedMovingSimple: 8 3.0 5.0
//SpeedPos: 9 205.0
//SpeedMovingSimple: 9 1.5 5.0
//SpeedPos: 10 230.0
//SpeedMovingSimple: 10 1.5 5.0
//SpeedPos: 11 235.0
//SpeedMovingSimple: 11 3.0 5.0



BloomActive: 1
BloomPos: 0 0.0
BloomLower: 0 0.3
BloomUpper: 0 1.0

ContrastActive: 0
ContrastPos: 0 50.0
ContrastAmount: 0 1.0
ContrastPos: 1 100.0
ContrastAmount: 1 1.0

/////////////////////////////////////

WaveActive: 1

//WaveTime: 0 8.0 2.0
//WavePos: 0 380
//WavePos: 1 400
//WaveCount: 0 10 10
//WaveCount: 1 50 50
//WaveIntensity: 0 0.000 0.000
//WaveIntensity: 1 0.75 0.150

WaveTime: 0 2.0 1.0
WavePos: 0 380
WavePos: 1 440
WaveCount: 0 0 0
WaveCount: 1 5 4
WaveIntensity: 0 0.000 0.000
WaveIntensity: 1 0.5 0.1

/////////////////////////////////////


SongFilename: ../Assets/Sound/Music/FirstLevelMaybe.mp3
SongFilename: ../Assets/Sound/Music/ThreeTwoOne.ogg
SongTempo: 130
SongFilename: ../Assets/Sound/Music/Wave1.ogg
SongTempo: 140