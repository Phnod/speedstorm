Size: 280 16
WinPoint: 240


BackgroundVisualFilenameModel: 00 ../Assets/Models/Background/icicle.obj
BackgroundVisualFilenameNormal: 00 ../Assets/Textures/Ice/IcicleNormal.png

BackgroundVisualShinyMult: 00 16.0
BackgroundVisualDiffuseMult: 00 0.7 0.7 0.9
BackgroundVisualEnv: 00 0.5

BackgroundVisualPos: 00 0.0 0.0 4.0
BackgroundVisualScale: 00 1.0 2.5 1.0
BackgroundVisualLoop: 00 13.0 90.0

BackgroundVisualPos: 02 8.0 0.0 6.0
BackgroundVisualScale: 02 1.2 2.75 1.2
BackgroundVisualLoop: 02 15.0 90.0

BackgroundVisualPos: 05 10.0 0.0 8.0
BackgroundVisualScale: 05 1.0 2.75 1.0
BackgroundVisualLoop: 05 17.0 90.0


BackgroundVisualPos: 10 -5.0 0.0 10.0
BackgroundVisualScale: 10 1.25 3.0 1.25
BackgroundVisualLoop: 10 19.0 90.0

BackgroundVisualPos: 11 9.0 0.0 12.0
BackgroundVisualScale: 11 1.5 3.5 1.5
BackgroundVisualLoop: 11 21.0 90.0

BackgroundVisualPos: 20 -10.0 0.0 15.0
BackgroundVisualScale: 20 1.75 4.0 1.75
BackgroundVisualLoop: 20 23.0 90.0


BackgroundVisualPos: 30 10.0 00.0 20.0
BackgroundVisualScale: 30 2.0 5.0 2.0
BackgroundVisualLoop: 30 25.0 90.0


BackgroundVisualEnv: 40 0.5
BackgroundVisualFilenameModel: 40 ../Assets/Models/Background/floor.obj
BackgroundVisualPos: 40 0.0 19.0 75.0
BackgroundVisualDiffuseMult: 40 1.0 1.0 1.0
BackgroundVisualScale: 40 2.0 1.0 2.0
BackgroundVisualLoop: 40 50.0 60.0
BackgroundVisualRotate: 40 15.0 0.0 0.0

BackgroundVisualPos: 41 0.0 22.5 75.0
BackgroundVisualRotate: 41 -05.0 0.0 0.0



VisualFilenameDiffuse: 0 ../Assets/Textures/Block/IceWall.png
VisualFilenameNormal: 0 ../Assets/Textures/Block/IceWallNorm.png
//VisualFilenameSpecular: 0 ../Assets/Textures/Block/binWall_spec.png
//VisualFilenameEmissive: 0 ../Assets/Textures/Block/binWall_tex_emissive.png
VisualFilenameModel: 0 ../Assets/Models/Ice/Ice_wall.obj

VisualChar: 0 N
VisualParam: 0 0 0 0
//VisualEnv: 0 0.5
VisualDead: 0 V
VisualPos: 0 0.0 0.0 -6.0
VisualDiffuseMult: 0 0.5 0.5 0.5

VisualChar: 1 V
VisualParam: 1 0 0 0
//VisualEnv: 1 0.75
VisualDead: 1 X
VisualPos: 1 0.0 0.0 -3.0
VisualDiffuseMult: 1 0.65 0.65 0.65

VisualChar: 2 X
VisualParam: 2 1 0 0
//VisualEnv: 2 0.5
VisualDead: 2 =
VisualPos: 2 0.0 0.0 0.0
VisualDiffuseMult: 2 1.2 1.2 1.2

VisualChar: 3 x
VisualParam: 3 1 1 0
//VisualEnv: 3 0.5
VisualDead: 3 =
VisualPos: 3 0.0 0.0 0.0
VisualDiffuseMult: 3 1.0 1.0 1.0

VisualChar: 4 =
VisualParam: 4 0 0 0
//VisualEnv: 4 0.0
VisualDead: 4 -
VisualPos: 4 0.0 0.0 3.0
VisualDiffuseMult: 4 0.3 0.3 0.3
VisualSpecularMult: 4 0.25 0.25 0.25

VisualChar: 5 d
VisualParam: 5 0 0 0
VisualDead: 5 =
VisualPos: 5 0.0 0.0 3.0
VisualDiffuseMult: 5 0.3 0.3 0.3

VisualChar: 6 c
VisualDead: 6 d
VisualChar: 7 b
VisualChar: 8 a
VisualDead: 8 b

//////////////////////////////////////////////////////////////////////////

EnemyFilenameDiffuse: 0 ../Assets/Textures/Enemy/red.png
EnemyFilenameNormal: 0 ../Assets/Textures/Default/defaultNormal.png
EnemyFilenameSpecular: 0 ../Assets/Textures/Default/defaultSpecular.png
EnemyFilenameEmissive: 0 ../Assets/Textures/Enemy/red.png
EnemyFilenameModel: 0 ../Assets/Models/Ice/Ice_waffle_stack.obj

EnemyChar: 0 A
EnemyParam: 0 1 0 1
EnemyEnv: 0 0.25
EnemyDead: 0 B
EnemyPos: 0 0.0 0.0 -0.5

EnemyChar: 1 a
EnemyDead: 1 b
EnemyPos: 1 0.0 0.0 -0.5

EnemyFilenameModel: 2 ../Assets/Models/Ice/Ice_waffle_single_hipoly.obj
//EnemyFilenameModel: 2 ../Assets/Models/spike.obj

EnemyChar: 2 B
EnemyDead: 2 -
EnemyPos: 2 0.0 0.0 -0.5

EnemyChar: 3 b
EnemyDead: 3 =
EnemyPos: 3 0.0 0.0 -0.5

EnemyFilenameModel: 4 ../Assets/Models/Ice/Ice_popsicle_double_rotate.obj

EnemyChar: 4 C
EnemyDead: 4 D
EnemyPos: 4 0.0 0.0 -0.5

EnemyChar: 5 c
EnemyDead: 5 d
EnemyPos: 5 0.0 0.0 -0.5

EnemyFilenameModel: 6 ../Assets/Models/Ice/Ice_popsicle_single_rotate.obj

EnemyChar: 6 D
EnemyDead: 6 -
EnemyPos: 6 0.0 0.0 -0.5

EnemyChar: 7 d
EnemyDead: 7 =
EnemyPos: 7 0.0 0.0 -0.5

//////////////////////////////////////////////////////////////////////////

//BlockAmbientAdd: 0 0.0 0.0 0.0
//BlockAmbientMult: 0 1.0 1.0 1.0
//BlockDiffuseAdd: 0 0.0 0.0 0.0
////BlockDiffuseMult: 0 0.9 0.9 0.9
//BlockSpecularAdd: 0 0.0 0.0 0.0
//BlockSpecularMult: 0 1.0 1.0 1.0
//BlockEmissiveAdd: 0 0.0 0.0 0.0
//BlockEmissiveMult: 0 1.0 1.0 1.0
//BlockShinyAdd: 0 0.0
//BlockShinyMult: 0 1.0

BlockDiffuseMult: 00 1.00 0.69 0.69
BlockDiffuseMult: 01 0.95 0.65 0.69
BlockDiffuseMult: 02 0.95 0.69 0.65
BlockDiffuseMult: 03 0.95 0.65 0.69
BlockDiffuseMult: 04 0.95 0.65 0.65
BlockDiffuseMult: 05 0.95 0.65 0.65
BlockDiffuseMult: 06 0.95 0.65 0.69
BlockDiffuseMult: 07 1.00 0.69 0.69
BlockDiffuseMult: 08 1.00 0.69 0.69
BlockDiffuseMult: 09 1.00 0.69 0.69

//BlockEmissiveMult: 0 1.0 1.0 1.0
//BlockEmissiveMult: 1 1.0 0.9 0.9
//BlockEmissiveMult: 2 1.0 1.0 0.9
//BlockEmissiveMult: 3 0.9 1.0 1.0
//BlockEmissiveMult: 4 0.9 0.9 0.9
//BlockEmissiveMult: 5 0.8 0.8 0.8

BlockRandomAmbientAdd: 0 
BlockRandomAmbientMult: 0 
BlockRandomDiffuseAdd: 0 
BlockRandomDiffuseMult: 2 
BlockRandomSpecularAdd: 0 
BlockRandomSpecularMult: 0 
BlockRandomEmissiveAdd: 0 
BlockRandomEmissiveMult: 0 
BlockRandomShinyAdd: 0 
BlockRandomShinyMult: 0 

///////////////////////////////////////////////////////////

DOFBiasMult: 0 0.5

//EyePos: 0 0.0
//EyePos: 1 6.0
//EyePos: 2 7.00
//EyePos: 3 13.0
//
//EyeConvergence: 0 30
//EyeConvergence: 1 30
//EyeConvergence: 2 30
//EyeConvergence: 3 30
//
//EyeShift: 0 0.0 0.0
//EyeShift: 1 20.0 0.0
//EyeShift: 2 20.0 0.0
//EyeShift: 3 0.0 0.0

///////////////////////////////////////////////////////////

//LightDiffuse: 00 0.85 0.35 0.35 
//LightAmbient: 00 0.02 0.05 0.30 
//LightRim: 00 0.30 0.10 0.10

LightDiffuse: 00 0.35 0.35 0.85 
LightRim: 00 0.1 0.1 0.3

LightPos: 01 20
LightPos: 02 70

LightDiffuse: 02 0.85 0.35 0.35 
LightAmbient: 02 0.02 0.05 0.30 
LightRim: 02 0.30 0.10 0.10


/////////////////////////////////////




SpeedPos: 0 0.0
SpeedMovingSimple: 0 4.0 7.0


//SpeedPos: 3 95.0
//SpeedMovingSimple: 3 1.5 5.0
//SpeedPos: 4 105.0
//SpeedMovingSimple: 4 0.75 5.0
//SpeedPos: 5 115.0
//SpeedMovingSimple: 5 0.75 5.0
//SpeedPos: 6 120.0
//SpeedMovingSimple: 6 1.5 5.0
//SpeedPos: 7 125.0
//SpeedMovingSimple: 7 3.0 5.0
//SpeedPos: 8 200.0
//SpeedMovingSimple: 8 3.0 5.0
//SpeedPos: 9 205.0
//SpeedMovingSimple: 9 1.5 5.0
//SpeedPos: 10 230.0
//SpeedMovingSimple: 10 1.5 5.0
//SpeedPos: 11 235.0
//SpeedMovingSimple: 11 3.0 5.0



BloomActive: 1
BloomPos: 0 0.0
BloomLower: 0 0.0
BloomUpper: 0 1.0
BloomPos: 1 250.0
BloomLower: 1 0.0
BloomUpper: 1 1.0

ContrastActive: 0
ContrastPos: 0 50.0
ContrastAmount: 0 1.0
ContrastPos: 1 100.0
ContrastAmount: 1 1.0

/////////////////////////////////////

WaveActive: 1

//WaveTime: 0 8.0 2.0
//WavePos: 0 380
//WavePos: 1 400
//WaveCount: 0 10 10
//WaveCount: 1 50 50
//WaveIntensity: 0 0.000 0.000
//WaveIntensity: 1 0.75 0.150

WaveTime: 0 2.0 1.0
WavePos: 0 380
WavePos: 1 440
WaveCount: 0 0 0
WaveCount: 1 5 4
WaveIntensity: 0 0.000 0.000
WaveIntensity: 1 0.5 0.1

/////////////////////////////////////

//FogActive: 1
//FogPos: 0 50
//FogPos: 1 120
//FogPos: 2 130
//FogPos: 3 140
//FogPos: 4 190
//FogPos: 5 200
//FogPos: 6 250
//FogPos: 7 365
//FogPos: 8 385
//FogDistance: 0 100 200
////FogDistance: 1 15 100
////FogDistance: 2 15 50
////FogDistance: 3 15.0 55.0
////FogDistance: 4 15.0 55.0
////FogDistance: 5 1.0 70.0
////FogDistance: 6 1.0 200.0
////FogDistance: 7 1.0 100.0
//FogDistance: 8 1.0 2.0
//FogColor: 0 0.125 0.125 0.125
////FogColor: 5 0.30 0.15 0.15
////FogColor: 6 0.10 0.30 0.30
////FogColor: 7 0.20 0.20 0.20
//FogColor: 8 0.00 0.00 0.00

FogActive: 1
FogPos: 00 0
FogPos: 01 6
FogPos: 02 16
FogPos: 03 70
FogPos: 04 230
FogPos: 05 240
FogDistance: 00 1.0 2.0
FogDistance: 01 1.0 2.0
FogDistance: 02 50.0 100.0
FogDistance: 03 50.0 100.0
FogDistance: 04 50.0 100.0
FogDistance: 05 1.0 2.0
FogColor: 00 0.0 0.0 0.0
FogColor: 01 0.0 0.0 0.0
FogColor: 02 0.0 0.0 0.2
FogColor: 03 0.1 0.0 0.0 
FogColor: 04 0.1 0.0 0.0 
FogColor: 05 0.0 0.0 0.0

SongFilename: ../Assets/Sound/Music/GrooveTimeSlow.mp3
