/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Camera.h"

Camera::Camera()
{
	perspective = true;
	projection.PerspectiveProjection(60.0f, 1.0f, 0.1f, 100.0f);
}

Camera::Camera(int width, int height)
{
	perspective = true;
	projection.PerspectiveProjection(60.0f, width/height, 0.1f, 100.0f);
}

void Camera::Perspective(float fovyDegrees, float aspectRatio, float zNear, float zFar)
{
	perspective = true;
	projection.PerspectiveProjection(fovyDegrees, aspectRatio, zNear, zFar);
}

void Camera::PerspectiveStereo(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skew, float convergence)
{
	perspective = true;
	projection.PerspectiveStereoProjection(fovyDegrees, aspectRatio, zNear, zFar, skew, convergence);
	projectionNoSkew.PerspectiveProjection(fovyDegrees, aspectRatio, zNear, zFar);
}

void Camera::PerspectiveStereo(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY)
{
	perspective = true;
	projection.PerspectiveStereoProjection(fovyDegrees, aspectRatio, zNear, zFar, skewX, skewY, convergenceX, convergenceY);
	projectionNoSkew.PerspectiveProjection(fovyDegrees, aspectRatio, zNear, zFar);
}

void Camera::Orthographic(float top, float bottom, float left, float right, float zNear, float zFar)
{
	perspective = false;
	projection.OrthoProjection(top, bottom, left, right, zNear, zFar);
	projectionNoSkew.OrthoProjection(top, bottom, left, right, zNear, zFar);
}

void Camera::OrthographicStereo(float top, float bottom, float left, float right, float zNear, float zFar, float skewX, float convergence)
{
	perspective = false;
	projection.OrthoStereoProjection(top, bottom, left, right, zNear, zFar, skewX, convergence);
	projectionNoSkew.OrthoProjection(top, bottom, left, right, zNear, zFar);
}

void Camera::OrthographicStereo(float top, float bottom, float left, float right, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY)
{
	perspective = true;
	projection.OrthoStereoProjection(top, bottom, left, right, zNear, zFar, skewX, skewY, convergenceX, convergenceY);
	projectionNoSkew.OrthoProjection(top, bottom, left, right, zNear, zFar);
}

void Camera::Process() 
{
	satMat4 transform = world * local;

	for (unsigned i = 0; i < children.size(); ++i)
	{
		children[i]->setWorld(transform);
		children[i]->Process();
	}
}

satMat4 Camera::GetView() const
{
	satMat4 transform =  world * local;
	//transform.Inverse();
	//return transform.Inverse();
	return transform;
}

float * Camera::GetUniformProjection()
{
	return projection.Float();
}

float * Camera::GetUniformProjectionInverse()
{
	return projection.Inverse().Float();
}

satMat4 Camera::GetProjection()
{
	return projection;
}

satMat4 Camera::GetProjectionInverse()
{
	return projection.Inverse();
}










//void Camera::Perspective(float fovyDegrees, float aspectRatio, float zNear, float zFar)
//{
//	projection.PerspectiveProjection(fovyDegrees, aspectRatio, zNear, zFar);
//}
//
//void Camera::Orthographic(float top, float bottom, float left, float right, float zNear, float zFar)
//{
//	projection.OrthoProjection(top, bottom, left, right, zNear, zFar);
//}
//
//void Camera::Process() 
//{
//	satMat4 transform = world * local;
//
//	for (unsigned i = 0; i < children.size(); ++i)
//	{
//		children[i]->setWorld(transform);
//		children[i]->Process();
//	}
//}
//
//satMat4 Camera::GetView() const
//{
//	satMat4 transform = world * local;
//	transform.Inverse();
//	return transform;
//}
//
//float * Camera::GetUniformProjection()
//{
//	return projection.Float();
//}
