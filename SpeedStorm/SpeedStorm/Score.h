/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include <iostream>
#include <string>
#include <vector>

#include <fstream>
#include <string>
#include <assert.h>
#include <sstream>

#include <vector>

class Score
{
public:
	Score();
	~Score();
	//Enter the 'place' (1st - 10th), recieve score at said place
	//Level 0 is overal game high scores
	int readScore(const int &place, const int &level);

	//Reads each score of given level, saves to vector.
	std::vector<int> readFullLevel(const int &level);

	//Enter score into highscore's table. Score omitted if too small.
	//Level 0 is overal game high scores
	//Returns 'true' is score fits into list
	bool enterScore(const int &score, const int &level);

	//Adds player's score at end of level to their overall score
	void addToHiScore(const int &score);

	bool resetHiScore(const int &level, const int &score);

	//The player's current score in the level
	int currScore;
	//The high score in level
	int hiScore;
	//Player's total score in single run
	int totScore;
};