/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <string>
#include <vector>
#include <utility>
#include <memory>
//#include "Entity.h"	//Make this eventually

//#include "Animation.h"
#include "Resource.h"
#include "ShaderProgram.h"
#include "Mesh.h"
#include "Texture.h"
#include "Sound.h"

//class vertHolder;
//class objectHolder;
//class Model;
//class RenderManager;
//class ResourceManager;
//typedef std::shared_ptr<ResourceManager> ResourceManagerPtr;

//class ResourceManager
//{
//public:
//
//	static Mesh* AddMesh(const std::string &file, bool isBinary);
//	static Texture* AddTexture(const std::string &file);
//	//static ShaderProgram* AddShaderProgram(const std::string &vertShader, const std::string &fragShader);
//	static SoundLoc* AddSound(const std::string &file);
//
//	//Animation* AddAnimation(const std::string & filePrefix, unsigned startingIncrement = 0);
//
//	static std::vector<Mesh*> Meshes;
//	static std::vector<Texture*> Textures;
//	static std::vector<ShaderProgram*> ShaderPrograms;
//	static std::vector<SoundLoc*> Sounds;
//	//static std::vector<Animation*> Animations;
//};



namespace ResourceManager
{
	Mesh* AddMesh(const std::string &file, bool isBinary);
	Texture* AddTexture(const std::string &file);
	ShaderProgram* AddShader(ShaderProgram *shader);

	void reloadShaders();
	
	
	
	
	
	//static ShaderProgram* AddShaderProgram(const std::string &vertShader, const std::string &fragShader);
	//SoundLoc* AddSound(const std::string &file);

	//Animation* AddAnimation(const std::string & filePrefix, unsigned startingIncrement = 0);

	
	//static std::vector<ShaderProgram*> ShaderPrograms;
	//std::vector<SoundLoc*> Sounds;
}
