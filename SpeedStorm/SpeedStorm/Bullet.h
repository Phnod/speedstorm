/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <vector>
#include <memory>
#include "Mesh.h"
#include "Math.h"

//Still need to be done
#include "Sound.h"
#include "Light.h"
#include "Model.h"
#include "Object.h"
#include "level.h"
//#include "Game.h"

class Game;

//#include "Matrix.h"

//#include "ResourceManager.h"
//#include "Utilities.h"

class Bullet
{
public:
	Bullet();
	Bullet(Mesh * model, Texture * tex);
	Bullet(Mesh * model, Texture * tex, const satVec3 acc, const satVec3 vel, const satVec3 pos, const float life);

	satVec3 acceleration;
	satVec3 velocity;
	satVec3 position;
	satVec3 positionOld;
	satVec3 displacement;
	float lifetime;
	float totalLifetime;
	
	Mesh* mesh;
	Texture* texture;
	
	LightAddMult light;

	void update();
	virtual void process();
};

class BulletGood : public Bullet
{
public:
	BulletGood();
	BulletGood(Mesh * model, Texture * tex);
	BulletGood(Game* _game, Level* _level, Mesh * model, Texture * tex, const satVec3 acc, const satVec3 vel, const satVec3 pos, const float life);

	void process();

private:
	Game* game;
	Level* level;
};

class BulletBad : public Bullet
{
public:
	void process();

};

class ParticleN : public Bullet
{
public:
	ParticleN();
	ParticleN(Mesh * model, Texture * tex);
	ParticleN(Game * _game, Level * _level, Mesh * model, Texture * tex, const LightAddMult * light, const satVec3 acc, const satVec3 vel, const satVec3 pos, const float life);

	void process();

	
private:
	Game* game;
	Level* level;
};