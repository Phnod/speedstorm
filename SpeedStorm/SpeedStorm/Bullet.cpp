/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Bullet.h"
#include "Game.h"


Bullet::Bullet()
{

}

Bullet::Bullet(Mesh* model, Texture* tex)
{
	mesh = model;
	texture = tex;
	velocity = 0.0f;
	position = 0.0f;
	displacement = 0.0f;
}


Bullet::Bullet(Mesh* model, Texture* tex, satVec3 acc, satVec3 vel, satVec3 pos, float life)
{
	mesh = model;
	texture = tex;
	velocity = vel;
	position = pos;
	lifetime = life;
	displacement = 0.0f;
}

void Bullet::update()
{
	process();
}

void Bullet::process()
{

}

BulletGood::BulletGood()
{

}

BulletGood::BulletGood(Mesh* model, Texture* tex)
{
	mesh = model;
	texture = tex;
	velocity = 0.0f;
	position = 0.0f;
	displacement = 0.0f;
}


BulletGood::BulletGood(Game* _game, Level* _level, Mesh* model, Texture* tex, const satVec3 acc, const satVec3 vel, const satVec3 pos, const float life)
{
	game = _game;
	level = _level;
	mesh = model;
	texture = tex;
	acceleration = acc*2;	//divide/multiply these by two while the framerate is half the future framerate
	velocity = vel*2;
	position = pos;
	lifetime = life/2;
	totalLifetime = lifetime;
	displacement = 0.0f;
}

void BulletGood::process()
{
	lifetime -= FPU * Timer::frame;
	positionOld.x = position.x;
	velocity.x += (float)acceleration.x * FPU * Timer::frame;
	position.x += (float)velocity.x * FPU * Timer::frame;
	displacement.x = 0.0f;
	satVec2 arrayPos;
	arrayPos.x = (float)((-position.x / 2.0) + Game::adjust.x + game->TotalTranslation.x - (displacement.x* 0.5f) - 2.0f);
	arrayPos.y = (float)((((position.y + game->TotalTranslation.y + displacement.y + 12.5f) / 2.0f)) + 0.8f);
	
	if (arrayPos.x >= 0 && arrayPos.y >= 0 && arrayPos.y < level->Y && arrayPos.x <= level->X)
	{
		char *check = &level->lMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
		bool shootable = level->lShootableMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
		bool collision = level->lCollisionMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x] 
						|level->lKillMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x]
						|level->lBreakableMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
		char *changeTo = &level->lChangeToMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];

		if (shootable == true)
		{
			level->ChangeTo(satVec2(arrayPos.y, arrayPos.x));
			game->BoomSound.playSound(0.3f, 1.0f, 1.2f);
			Game::HighScore += Game::ScoreEnemy * Game::ScoreMult;
			Game::timeContrastNegative = Game::timeContrastMax;
			lifetime = 0;			
		}
		elf (collision == true)
		{
			velocity.x = -velocity.x;
			position.x += (float)velocity.x * FPU * Timer::frame;
			position.x += (float)velocity.x * FPU * Timer::frame;
			//velocity *= 0.95f;
			//lifetime = 0;
			lifetime -= Timer::framef();
		}
	}
	else
	{
		//lifetime = 0;
	}


	if(lifetime > 0)
	{
		positionOld.y = position.y;
		velocity.y += (float)acceleration.y * FPU * Timer::frame;
		position.y += (float)velocity.y * FPU * Timer::frame;
		
		positionOld.z = position.z;
		velocity.z += (float)acceleration.z * FPU * Timer::frame;
		position.z += (float)velocity.z * FPU * Timer::frame;

		arrayPos.x = (float)((-position.x / 2.0) + Game::adjust.x + game->TotalTranslation.x - 2.0f);
		arrayPos.y = (float)((((position.y + game->TotalTranslation.y + displacement.y + 12.5f) / 2.0f)) + 0.8f);

		if (arrayPos.x >= 0 && arrayPos.y >= 0 && arrayPos.y < level->Y && arrayPos.x <= level->X)
		{
			char *check = &level->lMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
			bool shootable = level->lShootableMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
			bool collision = level->lCollisionMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
			char *changeTo = &level->lChangeToMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];

			if (shootable == true)
			{
				level->ChangeTo(satVec2(arrayPos.y, arrayPos.x));
				game->BoomSound.playSound(0.3f, 1.0f, 1.2f);
				Game::HighScore += Game::ScoreEnemy * Game::ScoreMult;
				Game::timeContrastNegative = Game::timeContrastMax;
				lifetime = 0;			
			}
			elf (collision == true)
			{
				velocity.y = -velocity.y;
				position.y += (float)velocity.y * FPU * Timer::frame;
				//velocity *= 0.8f;
				lifetime -= Timer::framef();
			}
		}
		else
		{
			//lifetime = 0;
		}
	}
}

void BulletBad::process()
{

}

ParticleN::ParticleN(Mesh* model, Texture* tex)
{
	mesh = model;
	texture = tex;
	velocity = 0.0f;
	position = 0.0f;
}


ParticleN::ParticleN(Game* _game, Level* _level, Mesh* model, Texture* tex, const LightAddMult * light, const satVec3 acc, const satVec3 vel, const satVec3 pos, const float life)
{
	game = _game;
	level = _level;
	mesh = model;
	texture = tex;
	acceleration = acc * 2;
	velocity = vel * 2;
	position = pos;
	lifetime = life / 2;
	totalLifetime = lifetime;
}

void ParticleN::process()
{
	lifetime -= (float)FPU * Timer::frame;
	positionOld = position;
	velocity += acceleration * FPU * (float)Timer::frame;
	position += velocity * FPU * (float)Timer::frame;

	//satVec2 arrayPos;
	//arrayPos.x = (float)((-position.x / 2.0) + Game::adjust.x + game->TotalTranslation.x - 2.0f);
	//arrayPos.y = (float)((position.y + ((game->TotalTranslation.y + 12.5f) / 2.0f)) + 1.0f);
	////satVec2 tempPos = satVec2((TotalTranslation.y + 12.5f) / 2.0f, adjust.x);
	//
	//arrayPos.x = Between(arrayPos.x, 0.0f, (float) level->X);
	//arrayPos.y = Between(arrayPos.y, 0.0f, (float) level->Y);
	//
	//char *check = &level->lMatrix[(unsigned)arrayPos.y][(unsigned)arrayPos.x];
	//
	//if (arrayPos.x >= 0 && arrayPos.y >= 0 && arrayPos.y < level->Y && arrayPos.x < level->X)
	//{
	//	if (*check == BlockWall)
	//	{
	//		lifetime = 0;
	//	}
	//}
	//else
	//{
	//
	//}
}

