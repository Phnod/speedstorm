/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.
#include "QuickMesh.h"
#include <fstream>
#include <iostream>
//#include <minimath\Core.h> //Minimath Library. Replace eventually

#define CHAR_BUFFER_SIZE 128 //Size of the character buffer.
#define BUFFER_OFFSET(i) ((char *)0 + (i))

//OBJ face structure

struct MeshFace
{
	MeshFace()
	{
		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;

		textureUVs[0] = 0;
		textureUVs[1] = 0;
		textureUVs[2] = 0;

		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;

	}
	MeshFace(unsigned v1, unsigned v2, unsigned v3,
		unsigned t1, unsigned t2, unsigned t3,
		unsigned n1, unsigned n2, unsigned n3)
	{
		vertices[0] = v1;
		vertices[1] = v2;
		vertices[2] = v3;

		textureUVs[0] = t1;
		textureUVs[1] = t2;
		textureUVs[2] = t3;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;
	}
	unsigned vertices[3];
	unsigned textureUVs[3];
	unsigned normals[3];
};

QuickMesh::QuickMesh()
{
	BUFFER_OFFSET(23); //Macros are useful.
}
QuickMesh::~QuickMesh()
{

}

bool QuickMesh::ConvertFile(const std::string &inputFile)
{
	std::ifstream input;
	input.open(inputFile);

	if (!input)
	{
		std::cout << "Error: Could not open file \"" << inputFile << "\"!\n";
		return false;

	}
	char inputString[CHAR_BUFFER_SIZE];

	//Unique data
	std::vector<satVec3> vertexData;
	std::vector<satVec2> textureData;
	std::vector<satVec3> normalData;
	//index/face data
	std::vector<MeshFace> faceData;
	//Use the face data to create an array of verts, uvs, and normals for faster use in openGL
	std::vector<float> unPackedVertexData;
	std::vector<float> unPackedTextureData;
	std::vector<float> unPackedNormalData;

	while (!input.eof()) //While the file has not ended, continue pulling in data.
	{
		input.getline(inputString, CHAR_BUFFER_SIZE);
		if (std::strstr(inputString, "#") != nullptr) //strstr, or string string, checks to see if a string contains a substring. It's like substring, 
			//but with the entire string instead of a set amount of characters. For this line, we want to handle comments.
		{
			//This line is a comment.
			continue;

		}
		else if (std::strstr(inputString, "vt") != nullptr)
		{
			//This line has uv data
			satVec2 temp;
			std::sscanf(inputString, "vt %f %f", &temp.x, &temp.y);
			textureData.push_back(temp);

		}
		else if (std::strstr(inputString, "vn") != nullptr)
		{
			//This line has normal data
			satVec3 temp;
			std::sscanf(inputString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normalData.push_back(temp);

		}
		else if (std::strstr(inputString, "v") != nullptr)
		{
			//This line has vertex data
			satVec3 temp;
			std::sscanf(inputString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			vertexData.push_back(temp);

		}


		else if (std::strstr(inputString, "f") != nullptr)
		{
			//This line contains face data.
			MeshFace temp;

			std::sscanf(inputString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]
				);
			faceData.push_back(temp);

		}
	}
	input.close();

	//Unpack the data
	for (unsigned i = 0; i < faceData.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].x);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].y);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].z);//We use -1 because OBJ specs start at 1 instead of 0.

			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].x);
			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].y);

			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].x);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].y);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].z);
		}

	}
	_NumFaces = faceData.size();
	_NumVertices = _NumFaces * 3;

	//Write to file
	std::string newFile(inputFile);
	FILE *binaryFile = nullptr;
	newFile.pop_back();
	newFile.pop_back();
	newFile.pop_back();
	newFile += "mesh";
	fopen_s(&binaryFile, newFile.c_str(), "wb");

	fwrite(&_NumFaces, sizeof(unsigned int), 1, binaryFile);
	fwrite(&_NumVertices, sizeof(unsigned int), 1, binaryFile);
	fwrite(&unPackedVertexData[0], sizeof(float), _NumVertices * 3, binaryFile);
	fwrite(&unPackedTextureData[0], sizeof(float), _NumVertices * 2, binaryFile);
	fwrite(&unPackedNormalData[0], sizeof(float), _NumVertices * 3, binaryFile);
	fclose(binaryFile);

	
	vertexData.clear(); 
	textureData.clear();
	normalData.clear();
	faceData.clear();
	unPackedVertexData.clear();
	unPackedTextureData.clear();
	unPackedNormalData.clear();

	return true;
}

//END NEW OFFLINE CODE


//Load a quickmesh from file and sent it to memory
bool QuickMesh::LoadFromFile(const std::string &InputFile)
{
	FILE *binaryFile = nullptr;
	fopen(InputFile.c_str(), "rb");
	if (binaryFile == nullptr)
	{
		std::cout << "Error loading file \"" << InputFile << "\".\n";
	}

	fread(&_NumFaces, sizeof(unsigned int), 1, binaryFile);
	fread(&_NumVertices, sizeof(unsigned int), 1, binaryFile);

	float *vertices = new float[_NumVertices * 3];
	float *uvs = new float[_NumVertices * 2];
	float *normals = new float[_NumVertices * 3];

	fread(vertices, sizeof(float), _NumVertices * 3, binaryFile);
	fread(uvs, sizeof(float), _NumVertices * 2, binaryFile);
	fread(normals, sizeof(float), _NumVertices * 3, binaryFile);
	fclose(binaryFile);




	//Send data to OpenGL.
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO_Vertices);
	glGenBuffers(1, &VBO_UVs);
	glGenBuffers(1, &VBO_Normals);

	glBindVertexArray(VAO);
	//The VAO keeps track of what we're doing with the VBOs until it is unbound.
	//This lets us keep track of what we specify for all our VBOs and their shader data, instead of repeating what's inside the buffers each time.
	//It's like handing OpenGL a list instead of orally telling it each frame.

	glEnableVertexAttribArray(0); //Vertex
	glEnableVertexAttribArray(1); //Uvs
	glEnableVertexAttribArray(2); //Normals

	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vertices);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVertices * 3, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	//Uvs
	glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVertices * 2, uvs, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, BUFFER_OFFSET(0));
	//Normals
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVertices * 3, normals, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	//OBJ data is now modern GL ready!

	//Begin cleanup

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind VBO
	glBindVertexArray(0); //Unbind VAO

	delete[] vertices;
	delete[] uvs;
	delete[] normals; //delete pointer data so we don't leak.

	return true;
}
//release data from system and GPU memory
void QuickMesh::Unload() //Clears the mesh data from the GPU
{
	//Remove buffers in the opposite order they were created
	glDeleteBuffers(1, &VBO_Normals);
	glDeleteBuffers(1, &VBO_UVs);
	glDeleteBuffers(1, &VBO_Vertices);
	glDeleteVertexArrays(1, &VAO);
	//Clear out contents of variables.
	VBO_Normals = 0;
	VBO_UVs = 0;
	VBO_Vertices = 0;
	VAO = 0;

	_NumFaces = 0;
	_NumVertices = 0;
}

unsigned int QuickMesh::GetNumFaces() const
{
	return _NumFaces;
}
unsigned int QuickMesh::GetNumVertices() const
{
	return _NumVertices;
}
