/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

//#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.
//
//#include "Entity.h"
//#include "File.h"
//#include "Utilities.h"
//#include <iostream>
//
//Entity::Entity(ResourceManager* resources, RenderManager* renderer) : Model(resources, renderer)
//{
//	//nuttin here
//}
//
//bool Entity::SetBB()
//{
//	if (!_Mesh->_File.empty())
//	{
//		if (SetBB(_Mesh->_File))
//		{
//			return true;
//		}
//	}
//
//	return false;
//}
//
//bool Entity::SetBB(const std::string & file)
//{
//	FILE *binaryFile = nullptr;
//	binaryFile = fopen(file.c_str(), "rb");
//
//	if (binaryFile == nullptr)
//	{
//		std::cout << "Could not open file for reading: " << file << std::endl;
//		return false;
//	}
//
//	unsigned NumFaces, NumVertices;
//	fread(&NumFaces, sizeof(unsigned int), 1, binaryFile);
//	fread(&NumVertices, sizeof(unsigned int), 1, binaryFile);
//
//	float *vertices = new float[NumVertices * 3];
//
//	fread(vertices, sizeof(float), NumVertices * 3, binaryFile);
//
//	fclose(binaryFile);
//
//	if (NumVertices < 6)
//		return false;
//
//	topCorn[0] = Max(vertices[0], vertices[3]);
//	topCorn[1] = Max(vertices[1], vertices[4]);
//	topCorn[2] = Max(vertices[2], vertices[5]);
//
//	btmCorn[0] = Min(vertices[0], vertices[3]);
//	btmCorn[1] = Min(vertices[1], vertices[4]);
//	btmCorn[2] = Min(vertices[2], vertices[5]);
//
//	for (unsigned i = 6; i <= NumVertices * 3 - 3; i += 3)
//	{
//		topCorn[0] = Max(topCorn[0], vertices[i]);
//		topCorn[1] = Max(topCorn[1], vertices[i + 1]);
//		topCorn[2] = Max(topCorn[2], vertices[i + 2]);
//
//		btmCorn[0] = Min(btmCorn[0], vertices[i]);
//		btmCorn[1] = Min(btmCorn[1], vertices[i + 1]);
//		btmCorn[2] = Min(btmCorn[2], vertices[i + 2]);
//	}
//
//	delete[] vertices;
//	return true;
//}
//
//void Entity::DrawBB()
//{
//	satVec3 scale = topCorn - btmCorn;
//	satVec3 offset = scale * 0.5f - topCorn;
//
//	satMat4 temp = local;
//	temp.Scale(scale);
//	temp.Translate(-offset);
//
//	//_Renderer->BindShaderProgram(BBoxShaderProgram);
//	//_Renderer->BindTexture(BBoxTexture);
//	//_Renderer->BindMesh(BBoxMesh);
//	//
//	//_Renderer->Draw(world * temp);
//	//
//	//_Renderer->UnbindMesh();
//	//_Renderer->UnbindTexture();
//	//_Renderer->UnbindShaderProgram();
//}
//
//void Entity::Process()
//{
//	satMat4 transform = world * local;
//
//	if (_Renderer != nullptr && IsVisible)
//	{
//		if (BBoxIsVisible && isBinaryMesh)
//			DrawBB();
//
//		//_Renderer->BindShaderProgram(_ShaderProgram);
//		//_Renderer->BindTexture(_Texture);
//		//_Renderer->BindMesh(_Mesh);
//		//
//		//_Renderer->Draw(transform);
//		//
//		//_Renderer->UnbindMesh();
//		//_Renderer->UnbindTexture();
//		//_Renderer->UnbindShaderProgram();
//	}
//
//	for (unsigned i = 0; i < children.size(); ++i)
//	{
//		children[i]->setWorld(transform);
//		children[i]->Process();
//	}
//}
//
//
//void Entity::ScaleBBUp(float f)
//{
//	topCorn.y *= f;
//}
//
//void Entity::ScaleBBDown(float f)
//{
//	btmCorn.y *= f;
//}
//
//void Entity::ScaleBBLeft(float f)
//{
//	btmCorn.x *= f;
//}
//
//void Entity::ScaleBBRight(float f)
//{
//	topCorn.x *= f;
//}
//
//void Entity::ScaleBBNear(float f)
//{
//	topCorn.z *= f;
//}
//
//void Entity::ScaleBBFar(float f)
//{
//	btmCorn.z *= f;
//}
//
//void Entity::SetMesh(const std::string &meshFile, bool isBinaryMesh)
//{
//	isBinaryMesh = isBinaryMesh;
//	Model::SetMesh(meshFile, isBinaryMesh);
//	SetBB();
//}
//
//void Entity::RotateX(float degrees)
//{
//	satVec3 offset = (topCorn - btmCorn) * 0.5f - topCorn;
//
//	local.Translate(-offset);
//	local.RotateX(degrees);
//	local.Translate(offset);
//}
//
//void Entity::RotateY(float degrees)
//{
//	satVec3 offset = (topCorn - btmCorn) * 0.5f - topCorn;
//
//	local.Translate(-offset);
//	local.RotateY(degrees);
//	local.Translate(offset);
//}
//
//void Entity::RotateZ(float degrees)
//{
//	satVec3 offset = (topCorn - btmCorn) * 0.5f - topCorn;
//
//	local.Translate(-offset);
//	local.RotateZ(degrees);
//	local.Translate(offset);
//}
//
//satVec3 Entity::GetBBoxMax()
//{
//	return topCorn;
//}
//
//satVec3 Entity::GetBBoxMin()
//{
//	return btmCorn;
//}