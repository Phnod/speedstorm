/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <SFML\Graphics.hpp>
#include <SFML\OpenGL.hpp>
//#define GLFW_INCLUDE_GLU
//#include <GLFW\glfw3.h>
#include <iostream>
#include <map>
#include "Math.h"

#include "SerialPort.h"

namespace sat //Space Alpha Team
{
	namespace ard
	{
		SerialPort* InitArduino();
		char* ReadPort();
		bool ParseLine(char string[], float &x, float &y, float &z);
		bool ParseLine(float &x, float &y, float &z);
		bool ParseLine(satVec3 &vec, int &shoot);
		bool ArduinoActive();
	}

	namespace key
	{
		void Init();
		void Update();

		void Map();
		void setVar();
		
		float IsPressed(const sf::Keyboard::Key & key);
		float IsPressed(const std::string & key);
		bool CheckPressed(const sf::Keyboard::Key & key);
		bool CheckPressed(const sf::Keyboard::Key & key, const float & num);
		bool CheckPressed(const std::string &key);
		bool CheckPressed(const std::string &key, const float & num);
		bool CheckKey(const sf::Keyboard::Key &key);
		bool CheckKey(const std::string &key);

		int toggleBoolean(const sf::Keyboard::Key &key, bool &boolean);
		int toggleBoolean(const std::string &key, bool &boolean);
		
		bool addInt(const sf::Keyboard::Key &key, int &variable, const int &number);

		void CheckPressedAll();
	}

	namespace joy
	{
		void Init();
		void Update();

		float IsPressed(const int button);
		bool CheckPressed(const int button);
		bool CheckPressed(const int button, const float &num);
		bool CheckPressed(const int button, const int &num);
		bool CheckButton(const int button);

		void Map();
		void setVar();

		float CheckAxis(sf::Joystick::Axis joy, const float deadzone);
	}

	namespace mouse
	{
		void Init();
		void Update();
		void Update(int &width, int &height);

		satVec2 Pos();
		int PosX();
		int PosY();
		
		long long IsClicked(const sf::Mouse::Button key);
		//long long IsClicked(std::string key);
		bool CheckClicked(const sf::Mouse::Button key);
		bool CheckClicked(const sf::Mouse::Button key, const long long &num);
		bool CheckClicked(const sf::Mouse::Button key, const int &num);
		//bool CheckClicked(std::string key);
		//bool CheckClicked(std::string key, long long num);
		bool CheckButton(const sf::Mouse::Button key);
		//bool CheckKey(std::string key);
	}
}