/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Utilities.h"
#include <GL/glew.h>
#include <iostream>

#define BUFFER_OFFSET(i) ((char *)0 + (i))

//HANDLE handle = INVALID_HANDLE_VALUE;

GLuint _FullScreenQuadVAO = GL_NONE;
GLuint _FullScreenQuadVBO = GL_NONE;

unsigned StringToID(std::string const& _string)
{
	unsigned id = 0;

	for (unsigned int i = 0; i < _string.size(); ++i)
	{
		id += i * (unsigned)_string[i];
	}

	return id;
}

void InitFullScreenQuad()
{
	float VBO_DATA[] =
	{
		-1.0f, -1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,

		1.0f, 1.0f, 0.0f,
		-1.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 0.0f,

		0.0f, 0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f,

		1.0f, 1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f,
	};

	int vertexSize = 6 * 3 * sizeof(float);
	int texCoordSize = 6 * 2 * sizeof(float);

	glGenVertexArrays(1, &_FullScreenQuadVAO);
	glBindVertexArray(_FullScreenQuadVAO);

	glEnableVertexAttribArray(0); //verticies
	glEnableVertexAttribArray(1); //UVS

		glGenBuffers(1, &_FullScreenQuadVBO);

	glBindBuffer(GL_ARRAY_BUFFER, _FullScreenQuadVBO);
	glBufferData(GL_ARRAY_BUFFER, vertexSize + texCoordSize, VBO_DATA, GL_STATIC_DRAW);

	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(vertexSize));

	glBindBuffer(GL_ARRAY_BUFFER, GL_NONE);
	glBindVertexArray(GL_NONE);
}

void DrawFullScreenQuad()
{
	glBindVertexArray(_FullScreenQuadVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(GL_NONE);

}

LPCWSTR StringToLPCWSTR(const std::string& s)
{
	std::wstring stemp = std::wstring(s.begin(), s.end());
	LPCWSTR sw = stemp.c_str();
	return sw;
}

bool SatGetFileTime(std::string _string, LPFILETIME &lpCreationTime, LPFILETIME &lpLastAccessTime, LPFILETIME &lpLastWriteTime)
{
	handle =  CreateFile(StringToLPCWSTR(_string), GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE | FILE_SHARE_DELETE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	std:: cout << GetLastError() << "\t";
	if(handle != INVALID_HANDLE_VALUE)
	{
		GetFileTime(handle, lpCreationTime, lpLastAccessTime, lpLastWriteTime);
		CloseHandle(handle);
		return true;
	}	
	
	std::cout << "OH NO MAN, IT'S MESSED" << std::endl;
	CloseHandle(handle);
	return false;
	system("PAUSE");
	exit(0);
}