/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Math.h"

/*
//=======================================================================================
//
//	satVec2 - 2D vector
//
//=======================================================================================
*/

#pragma region satVec2

  //=======================//
 // satVec2::constructors //
//=======================//

satVec2::satVec2()
{
}

satVec2::satVec2(const float _x)
{
	this->x = _x;
	this->y = _x;
}

satVec2::satVec2(const float _x, const float _y)
{
	this->x = _x;
	this->y = _y;
}

void satVec2::Set(const float x, const float y)
{
	this->x = x;
	this->y = y;
}

void satVec2::Zero()
{
	x = y = 0.0f;
}

  //==============================//
 // end of satVec2::constructors //
//==============================//

  //====================//
 // satVec2::operators //
//====================//

float	satVec2::operator[](int a) const
{
	return (&x)[a];
}

float& satVec2::operator[](int a)
{
	return (&x)[a];
}

satVec2	satVec2::operator-() const
{
	return satVec2(-x, -y);
}

satVec2	satVec2::operator+(const satVec2 &a) const
{
	return satVec2(x + a.x, y + a.y);
}

satVec2	satVec2::operator+(const int &a) const
{
	return satVec2(x + a, y + a);
}

satVec2	satVec2::operator-(const satVec2 &a) const
{
	return satVec2(x - a.x, y - a.y);
}

float	satVec2::Dot(const satVec2 &a) const
{
	return x * a.x + y * a.y;
}

satVec2	satVec2::operator*(const float a) const
{
	return satVec2(x * a, y * a);
}

satVec2	satVec2::operator*(const satVec2 a) const
{
	return satVec2(x * a.x, y * a.y);
}

satVec2 operator*(const float a, const satVec2 b)
{
	return satVec2(b.x * a, b.y * a);
}

satVec2	satVec2::operator/(const float a) const
{
	return satVec2(x / a, y / a);
}

satVec2&	satVec2::operator+=(const satVec2 &a)
{
	this->x += a.x;
	this->y += a.y;

	return *this;
}

satVec2&	satVec2::operator-=(const satVec2 &a)
{
	this->x -= a.x;
	this->y -= a.y;

	return *this;
}

satVec2&	satVec2::operator*=(const satVec2 &a)
{
	this->x *= a.x;
	this->y *= a.y;

	return *this;
}

satVec2&	satVec2::operator/=(const satVec2 &a)
{
	this->x /= a.x;
	this->y /= a.y;

	return *this;
}

satVec2&	satVec2::operator*=(const float a)
{
	this->x *= a;
	this->y *= a;

	return *this;
}

satVec2&	satVec2::operator/=(const float a)
{
	this->x /= a;
	this->y /= a;

	return *this;
}

bool satVec2::operator==(const satVec2 &a) const
{
	return Equals(a);
}

bool satVec2::operator!=(const satVec2 &a) const
{
	return !Equals(a);
}

  //===========================//
 // end of satVec2::operators //
//===========================//

  //============================//
 // satVec2::general functions //
//============================//

bool satVec2::Equals(const satVec2 &a) const
{
	return (x == a.x) && (y == a.y);
}

bool satVec2::Equals(const satVec2 &a, const float margin) const
{
	if (abs(x - a.x) > margin ||
		abs(y - a.y) > margin)
	{
		return false;
	}
	return true;
}

float satVec2::Length() const
{
	return sqrt(LengthSqr());
}

float satVec2::LengthSqr() const
{
	return (x*x + y*y);
}

float satVec2::Normalize()
{
	float sqr = LengthSqr(), length = 1.f / Length();

	//x *= length;
	//y *= length;
	*this *= length;

	return sqr * length;
}

void satVec2::MinMax(const satVec2 &min, const satVec2 &max)
{
	if (x < min.x)
	{
		x = min.x;
	}
	else if (x > max.x)
	{
		x = max.x;
	}

	if (y < min.y)
	{
		y = min.y;
	}
	else if (y > max.y)
	{
		y = max.y;
	}
}

void satVec2::Int()
{
	x = floor(x + 0.5f);
	y = floor(y + 0.5f);
}

void satVec2::IntFloor()
{
	x = float(int(x));
	y = float(int(y));
}

satVec2 satVec2::Scale(const satVec2 &a) const
{
	return satVec2(x * a.x, y * a.y);
}

int satVec2::GetDimension() const
{
	return 2;
}

const float *satVec2::ToFloatPtr() const
{
	return &x;
}

float *satVec2::ToFloatPtr()
{
	return &x;
}

//==================//
// satVec2::Lerp()  // Linear interpolation
//==================// 

void satVec2::Lerp(const satVec2 &v0, const satVec2 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
	}
	else
	{		
		//*this = v0 + t * (v1 - v0);
		*this = (1.0f - t) * v0 + (t) * v1;
	}
}

void satVec2::LerpNoBounds(const satVec2 &v0, const satVec2 &v1, const float t)
{
	//*this = v0 + t * (v1 - v0);
	*this = (1.0f - t) * v0 + (t) * v1;
}

satVec2 satVec2::LerpR(const satVec2 &v0, const satVec2 &v1, const float t)
{
	if (t <= 0.0f)
	{
		 return v0;
	}
	else if (t >= 1.0f)
	{
		return v1;
	}
	else
	{
		return (1.0f - t) * v0 + (t)* v1;
		//return v0 + t * (v1 - v0);
	}
}

satVec2 satVec2::LerpNoBoundsR(const satVec2 &v0, const satVec2 &v1, const float t)
{
	//return v0 + t * (v1 - v0);
	return (1.0f - t) * v0 + (t)* v1;
}

void	satVec2::BiLerp(const satVec2 &v0, const satVec2 &v1, const satVec2 &v2, const satVec2 &v3, const float t0, const float t1)
{
	*this = LerpR(LerpR(v0, v1, t0), LerpR(v2, v3, t0), t1);
}

void	satVec2::BiLerpNoBounds(const satVec2 &v0, const satVec2 &v1, const satVec2 &v2, const satVec2 &v3, const float t0, const float t1)
{
	*this = LerpNoBoundsR(LerpNoBoundsR(v0, v1, t0), LerpNoBoundsR(v2, v3, t0), t1);
}







#pragma endregion satVec2

/*
//=======================================================================================
//
//	satVec3 - 3D vector
//
//=======================================================================================
*/

#pragma region satVec3

//================//
// satVec3::Yaw() //
//================//

float satVec3::Yaw() const
{
	if (y == 0.0f && x == 0.0f)
	{
		return 0.0f;
	}
	else
	{
		float yaw = RTD(atan2(y, x));
		if (yaw < 0.0f)
		{
			yaw += 360.0f;
		}
		return yaw;
	}
}

//==================//
// satVec3::Pitch() //
//==================//

float satVec3::Pitch() const
{
	if (x == 0.0f && y == 0.0f)
	{
		if (z > 0.0f)
		{
			return 90.0f;
		}
		else
		{
			return 270.0f;
		}
	}
	else
	{
		float pitch = atan2(z, sqrt(x * x + y * y));
		pitch = RTD(pitch);
		if (pitch < 0.0f)
		{
			pitch += 360.0f;
		}
		return pitch;
	}
}


#if 0

//=======================//
// satVec3::constructors //
//=======================//

satVec3::satVec3()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}

satVec3::satVec3(const float xyz)
{
	Set(xyz, xyz, xyz);
}

satVec3::satVec3(const float x, const float y, const float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void satVec3::Set(const float x, const float y, const float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

void satVec3::Zero()
{
	x = y = z = 0.0f;
}

//==============================//
// end of satVec3::constructors //
//==============================//

//====================//
// satVec3::operators //
//====================//

float	satVec3::operator[](int a) const
{
	return (&x)[a];
}

float& satVec3::operator[](int a)
{
	return (&x)[a];
}

satVec3 satVec3::operator-() const
{
	return satVec3(-x, -y, -z);
}

satVec3&	satVec3::operator=(const satVec3 &a)
{
	x = a.x;
	y = a.y;
	z = a.z;

	return *this;
}

satVec3&	satVec3::operator=(const float a)
{
	x = a;
	y = a;
	z = a;

	return *this;
}

satVec3 satVec3::operator+(const satVec3 &a) const
{
	return satVec3(x + a.x, y + a.y, z + a.z);
}

satVec3 satVec3::operator-(const satVec3 &a) const
{
	return satVec3(x - a.x, y - a.y, z - a.z);
}

float satVec3::Dot(const satVec3 &a) const
{
	return x * a.x + y * a.y + z * a.z;
}

float satVec3::Dot(const satVec3 &a, const satVec3 &b)
{
	return b.x * a.x + b.y * a.y + b.z * a.z;
}

//float satVec3::Dot(satVec3 &a) const
//{
//	return x * a.x + y * a.y + z * a.z;
//}

satVec3 satVec3::operator*(const float a) const
{
	return satVec3(x * a, y * a, z * a);
}

satVec3 operator*(const float a, const satVec3 b)
{
	return satVec3(b.x * a, b.y * a, b.z * a);
}

satVec3 satVec3::operator*(const satVec3 &a) const
{
	return satVec3(x * a.x, y * a.y, z * a.z);
}

satVec3	satVec3::operator/(const satVec3 &a) const
{
	return satVec3(x / a.x, y / a.y, z / a.z);
}

satVec3	satVec3::operator/(const float a) const
{
	return satVec3(x / a, y / a, z / a);
}

satVec3&	satVec3::operator+=(const satVec3 &a)
{
	this->x += a.x;
	this->y += a.y;
	this->z += a.z;

	return *this;
}

satVec3&	satVec3::operator-=(const satVec3 &a)
{
	this->x -= a.x;
	this->y -= a.y;
	this->z -= a.z;

	return *this;
}

satVec3&	satVec3::operator*=(const satVec3 &a)
{
	this->x *= a.x;
	this->y *= a.y;
	this->z *= a.z;

	return *this;
}

satVec3&	satVec3::operator/=(const satVec3 &a)
{
	this->x /= a.x;
	this->y /= a.y;
	this->z /= a.z;

	return *this;
}

satVec3&	satVec3::operator*=(const float a)
{
	this->x *= a;
	this->y *= a;
	this->z *= a;

	return *this;
}

satVec3&	satVec3::operator/=(const float a)
{
	this->x /= a;
	this->y /= a;
	this->z /= a;

	return *this;
}



bool satVec3::operator==(const satVec3 &a) const
{
	return Equals(a);
}

bool satVec3::operator!=(const satVec3 &a) const
{
	return !Equals(a);
}

//===========================//
// end of satVec3::operators //
//===========================//

//============================//
// satVec3::general functions //
//============================//

bool satVec3::Equals(const satVec3 &a) const
{
	return (x == a.x) && (y == a.y) && (z == a.z);
}

bool satVec3::Equals(const satVec3 &a, const float margin) const
{
	if (abs(x - a.x) > margin ||
		abs(y - a.y) > margin ||
		abs(z - a.z) > margin)
	{
		return false;
	}
	return true;
}

satVec2 satVec3::xy() const
{
	return satVec2(x, y);
}

satVec2 satVec3::yz() const
{
	return satVec2(y, z);
}

satVec2 satVec3::xz() const
{
	return satVec2(x, z);
}

satVec2		satVec3::setXY(float x, float y)
{
	this->x = x; this->y = y;
	return xy();
}

satVec2		satVec3::setYZ(float y, float z)
{
	this->y = y; this->z = z;
	return yz();
}

satVec2		satVec3::setXZ(float x, float z)
{
	this->x = x; this->z = z;
	return xz();
}

satVec2		satVec3::setXY(satVec2 vec)
{
	this->x = vec.x; this->y = vec.y;
	return xy();
}

satVec2		satVec3::setYZ(satVec2 vec)
{
	this->y = vec.x; this->z = vec.y;
	return yz();
}

satVec2		satVec3::setXZ(satVec2 vec)
{
	this->x = vec.x; this->z = vec.y;
	return xz();
}

satVec2		satVec3::setXY(satVec3 vec)
{
	this->x = vec.x; this->y = vec.y;
	return xy();
}

satVec2		satVec3::setYZ(satVec3 vec)
{
	this->y = vec.x; this->z = vec.y;
	return yz();
}

satVec2		satVec3::setXZ(satVec3 vec)
{
	this->x = vec.x; this->z = vec.z;
	return xz();
}

satVec3 satVec3::Cross(const satVec3 &a) const
{
	return satVec3(
		y * a.z - z * a.y,
		z * a.x - x * a.z,
		x * a.y - y * a.x);
}

satVec3& satVec3::Cross(const satVec3 &a, const satVec3 &b)
{
	//x = a.y * b.z - a.z * b.y;
	//y = a.z * b.x - a.x * b.z;
	//z = a.x * b.y - a.y * b.x;
	//
	//return *this;

	return satVec3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

satVec3 satVec3::cross(const satVec3 &a, const satVec3 &b)
{
	return satVec3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

float satVec3::Length() const
{
	return sqrt(LengthSqr());
}

float satVec3::LengthSqr() const
{
	return (x*x + y*y + z*z);
}

float satVec3::Normalize()
{
	float sqr = LengthSqr(), length = 1.f / Length();

	//x *= length;
	//y *= length;
	//z *= length;
	*this *= length;

	return sqr * length;
}

satVec3 satVec3::Normalize(const satVec3 &a)
{
	float sqr = a.LengthSqr(), length = 1.f / a.Length();
	satVec3 temp = a;
	//x *= length;
	//y *= length;
	//z *= length;
	temp *= length;

	return temp;
}

void satVec3::MinMax(const satVec3 &min, const satVec3 &max)
{
	if (x < min.x)
	{
		x = min.x;
	}
	else if (x > max.x)
	{
		x = max.x;
	}

	if (y < min.y)
	{
		y = min.y;
	}
	else if (y > max.y)
	{
		y = max.y;
	}

	if (z < min.z)
	{
		z = min.z;
	}
	else if (z > max.z)
	{
		z = max.z;
	}
}

void satVec3::Int()
{
	x = floor(x + 0.5f);
	y = floor(y + 0.5f);
	z = floor(z + 0.5f);
}

void satVec3::IntFloor()
{
	x = float(int(x));
	y = float(int(y));
	z = float(int(z));
}

int satVec3::GetDimension() const
{
	return 3;
}

const satVec2 &satVec3::ToVec2() const
{
	return *reinterpret_cast<const satVec2 *>(this);
}

satVec2 &satVec3::ToVec2()
{
	return *reinterpret_cast<satVec2 *>(this);
}

const satColor & satVec3::ToColor() const
{
	return satColor((float)x, (float)y, (float)z);
}

const float* satVec3::ToFloatPtr() const
{
	return &x;
}

float* satVec3::ToFloatPtr()
{
	return &x;
}

//===================================//
// end of satVec3::general functions //
//===================================//

//================//
// satVec3::Yaw() //
//================//

float satVec3::Yaw() const
{
	if (y == 0.0f && x == 0.0f)
	{
		return 0.0f;
	}
	else
	{
		float yaw = RTD(atan2(y, x));
		if (yaw < 0.0f)
		{
			yaw += 360.0f;
		}
		return yaw;
	}
}

  //==================//
 // satVec3::Pitch() //
//==================//

float satVec3::Pitch() const
{
	if (x == 0.0f && y == 0.0f)
	{
		if (z > 0.0f)
		{
			return 90.0f;
		}
		else
		{
			return 270.0f;
		}
	}
	else
	{
		float pitch = atan2(z, sqrt(x * x + y * y));
		pitch = RTD(pitch);
		if (pitch < 0.0f)
		{
			pitch += 360.0f;
		}
		return pitch;
	}
}

  //==================//
 // satVec3::Lerp()  // Linear interpolation
//==================// 

void satVec3::Lerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
	}
	else
	{
		*this = (1.0f - t) * v0 + (t)* v1;
		//*this = v0 + t * (v1 - v0);
	}
}

void satVec3::LerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//*this = v0 + t * (v1 - v0);
	*this = (1.0f - t) * v0 + (t)* v1;
}

satVec3 satVec3::LerpR(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		return v0;
	}
	else if (t >= 1.0f)
	{
		return v1;
	}
	else
	{
		return (1.0f - t) * v0 + (t)* v1;
		//return v0 + t * (v1 - v0);
	}
}

satVec3 satVec3::LerpNoBoundsR(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//return v0 + t * (v1 - v0);
	return (1.0f - t) * v0 + (t)* v1;
}

void	satVec3::BiLerp(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1)
{
	*this = LerpR(LerpR(v0, v1, t0), LerpR(v2, v3, t0), t1);
}

void	satVec3::BiLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1)
{
	*this = LerpNoBoundsR(LerpNoBoundsR(v0, v1, t0), LerpNoBoundsR(v2, v3, t0), t1);
}

  //==================//
 // satVec3::NLerp() // Normalized Linear interpolation
//==================// Vectors should be normalized

void satVec3::NLerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
		return;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
		return;
	}

	*this = Normalize((1.0f - t) * v0 + (t)* v1);
	//Normalize();
}

void satVec3::NLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	*this = Normalize((1.0f - t) * v0 + (t)* v1);
	//Normalize();
}

  //==================//
 // satVec3::SLerp() // Spherical Linear interpolation
//==================// Vectors should be normalized

void satVec3::SLerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
		return;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
		return;
	}

	//omega, cosOmega, sinOmega, scale0, scale1
	float cosOmega, scale0, scale1;
	cosOmega = v0.Dot(v1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = v0 * scale0 + v1 * scale1;
}

void satVec3::SLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//omega, cosOmega, sinOmega, scale0, scale1

	float cosOmega, scale0, scale1;
	cosOmega = v0.Dot(v1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = v0 * scale0 + v1 * scale1;
}

#endif

#pragma endregion satVec3

/*
//=======================================================================================
//
//	satVec4 - 4D vector
//
//=======================================================================================
*/

#pragma region satVec4

  //=======================//
 // satVec4::constructors //
//=======================//

satVec4::satVec4()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
	this->w = 0.0f;
}

satVec4::satVec4(const float xyzw)
{
	Set(xyzw, xyzw, xyzw, xyzw);
}

satVec4::satVec4(const float x, const float y, const float z, const float w)
{
	Set(x, y, z, w);
}

satVec4::satVec4(const satVec3& vec, const float w)
{
	Set(vec.x, vec.y, vec.z, w);
}

void satVec4::Set(const float x, const float y, const float z, const float w)
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

void satVec4::Zero()
{
	x = y = z = w = 0.0f;
}

  //==============================//
 // end of satVec4::constructors //
//==============================//

  //====================//
 // satVec4::operators //
//====================//

float	satVec4::operator[](int a) const
{
	return (&x)[a];
}

float& satVec4::operator[](int a)
{
	return (&x)[a];
}

satVec4 satVec4::operator-() const
{
	return satVec4(-x, -y, -z, -w);
}

satVec4&	satVec4::operator=(const satVec4 &a)
{
	x = a.x;
	y = a.y;
	z = a.z;
	w = a.w;
	return *this;
}

satVec4 satVec4::operator+(const satVec4 &a) const
{
	return satVec4((x + a.x), (y + a.y), (z + a.z), (w + a.w));
}

satVec4 satVec4::operator-(const satVec4 &a) const
{
	return satVec4((x - a.x), (y - a.y), (z - a.z), (w - a.w));
}

float satVec4::Dot(const satVec4 &a) const
{
	return x * a.x + y * a.y + z * a.z;
}

satVec4 satVec4::operator*(const satVec4 &a) const
{
	return satVec4((x * a.x), (y * a.y), (z * a.z), (w - a.w));
}

satVec4 satVec4::operator*(const float a) const
{
	return satVec4((x * a), (y * a), (z * a), (w * a));
}

satVec4 operator*(const float a, const satVec4 b)
{
	return satVec4((b.x * a), (b.y * a), (b.z * a), (b.w * a));
}

satVec4	satVec4::operator/(const satVec4 &a) const
{
	return satVec4((x / a.x), (y / a.y), (z / a.z), (w / a.w));
}

satVec4	satVec4::operator/(const float a) const
{
	return satVec4((x / a), (y / a), (z / a), (w / a));
}

satVec4&	satVec4::operator+=(const satVec4 &a)
{
	this->x += a.x;
	this->y += a.y;
	this->z += a.z;
	this->w += a.w;

	return *this;
}

satVec4&	satVec4::operator-=(const satVec4 &a)
{
	this->x -= a.x;
	this->y -= a.y;
	this->z -= a.z;
	this->w -= a.w;

	return *this;
}

satVec4&	satVec4::operator/=(const satVec4 &a)
{
	this->x /= a.x;
	this->y /= a.y;
	this->z /= a.z;
	this->w /= a.w;

	return *this;
}

satVec4&	satVec4::operator/=(const float a)
{
	this->x /= a;
	this->y /= a;
	this->z /= a;
	this->w /= a;

	return *this;
}

satVec4&	satVec4::operator*=(const satVec4 &a)
{
	this->x *= a.x;
	this->y *= a.y;
	this->z *= a.z;
	this->w *= a.w;

	return *this;
}

satVec4&	satVec4::operator*=(const float a)
{
	this->x *= a;
	this->y *= a;
	this->z *= a;
	this->w *= a;

	return *this;
}

bool satVec4::operator==(const satVec4 &a) const
{
	return Equals(a);
}

bool satVec4::operator!=(const satVec4 &a) const
{
	return !Equals(a);
}

  //===========================//
 // end of satVec4::operators //
//===========================//

  //============================//
 // satVec4::general functions //
//============================//

bool satVec4::Equals(const satVec4 &a) const
{
	return (x == a.x) && (y == a.y) && (z == a.z) && (w == a.w);
}

bool satVec4::Equals(const satVec4 &a, const float margin) const
{
	if (abs(x - a.x) > margin ||
		abs(y - a.y) > margin ||
		abs(z - a.z) > margin ||
		abs(w - a.w) > margin)
	{
		return false;
	}
	return true;
}

satVec2		satVec4::xy() const
{
	return satVec2(x, y);
}

satVec2		satVec4::yz() const
{
	return satVec2(y, z);
}

satVec2		satVec4::xz() const
{
	return satVec2(x, z);
}

satVec3		satVec4::xyz() const
{
	return satVec3(x, y, z);
}


float satVec4::Length() const
{
	return sqrt(LengthSqr());
}

float satVec4::LengthSqr() const
{
	return (x*x + y*y + z*z + w*w);
}

float satVec4::Normalize()
{
	float sqr = LengthSqr(), length = 1.f / Length();

	//x *= length;
	//y *= length;
	//z *= length;
	//w *= length;

	*this *= length;

	return sqr * length;
}

void satVec4::MinMax(const satVec4 &min, const satVec4 &max)
{
	if (x < min.x)
	{
		x = min.x;
	}
	else if (x > max.x)
	{
		x = max.x;
	}

	if (y < min.y)
	{
		y = min.y;
	}
	else if (y > max.y)
	{
		y = max.y;
	}

	if (z < min.z)
	{
		z = min.z;
	}
	else if (z > max.z)
	{
		z = max.z;
	}

	if (w < min.w)
	{
		w = min.w;
	}
	else if (w > max.w)
	{
		w = max.w;
	}
}

void satVec4::Int()
{
	x = floor(x + 0.5f);
	y = floor(y + 0.5f);
	z = floor(z + 0.5f);
	w = floor(w + 0.5f);
}

void satVec4::IntFloor()
{
	x = float(int(x));
	y = float(int(y));
	z = float(int(z));
	w = float(int(w));
}

int satVec4::GetDimension() const
{
	return 4;
}

const satVec2 &satVec4::ToVec2() const
{
	return *reinterpret_cast<const satVec2 *>(this);
}

satVec2 &satVec4::ToVec2()
{
	return *reinterpret_cast<satVec2 *>(this);
}

const satVec3 &satVec4::ToVec3() const
{
	return *reinterpret_cast<const satVec3 *>(this);
}

satVec3 &satVec4::ToVec3()
{
	return *reinterpret_cast<satVec3 *>(this);
}

const float* satVec4::ToFloatPtr() const
{
	return &x;
}

float* satVec4::ToFloatPtr()
{
	return &x;
}

  //===================================//
 // end of satVec4::general functions //
//===================================//

  //==================//
 // satVec4::Lerp()  // Linear interpolation
//==================// 

void satVec4::Lerp(const satVec4 &v0, const satVec4 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
	}
	else
	{
		*this = (1.0f - t) * v0 + (t)* v1;
		//*this = v0 + t * (v1 - v0);
	}
}

void satVec4::LerpNoBounds(const satVec4 &v0, const satVec4 &v1, const float t)
{
	//*this = v0 + t * (v1 - v0);
	*this = (1.0f - t) * v0 + (t)* v1;
}

satVec4 satVec4::LerpR(const satVec4 &v0, const satVec4 &v1, const float t)
{
	if (t <= 0.0f)
	{
		return v0;
	}
	else if (t >= 1.0f)
	{
		return v1;
	}
	else
	{
		//return v0 + t * (v1 - v0);
		return (1.0f - t) * v0 + (t)* v1;
	}
}

satVec4 satVec4::LerpNoBoundsR(const satVec4 &v0, const satVec4 &v1, const float t)
{
	//return v0 + t * (v1 - v0);
	return (1.0f - t) * v0 + (t)* v1;
}

void	satVec4::BiLerp(const satVec4 &v0, const satVec4 &v1, const satVec4 &v2, const satVec4 &v3, const float t0, const float t1)
{
	*this = LerpR(LerpR(v0, v1, t0), LerpR(v2, v3, t0), t1);
}

void	satVec4::BiLerpNoBounds(const satVec4 &v0, const satVec4 &v1, const satVec4 &v2, const satVec4 &v3, const float t0, const float t1)
{
	*this = LerpNoBoundsR(LerpNoBoundsR(v0, v1, t0), LerpNoBoundsR(v2, v3, t0), t1);
}

#pragma endregion satVec4






