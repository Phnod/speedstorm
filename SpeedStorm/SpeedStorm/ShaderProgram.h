/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include "GL\glew.h"
#include <string>
#include <memory>
#include "Math.h"
#include "Resource.h"
#include "Color.h"
#include "Light.h"

class PointLight;
class ShaderProgram;
typedef std::shared_ptr<ShaderProgram> ShaderProgramPtr;

class ShaderProgram : public Resource
{
public:
	ShaderProgram();
	ShaderProgram(const std::string & vertShader, const std::string & fragShader);
	ShaderProgram(const std::string & vertShader, const std::string & fragShader, const std::string & geoShader);
	~ShaderProgram();


	static bool initDefault();
	void setDefault();

	bool load(const std::string &vertFile, const std::string &fragFile); //Load vertex and fragment shader files, place them in the program object.
	bool load(const std::string &vertFile, const std::string &fragFile, const std::string &geoFile);
	bool reload();
	bool isLoaded() const; //Allow us to check if the files are loaded
	void unload(); //Clear all shader data from OpenGL
	bool linkProgram(); //Allows us to manually link/relink the program from the outside.

	void bind() const; //Bind shaders to data - the shader becomes the active shader.
	static void unbind(); //Unbind shaders from data - no longer active shader.

	void addAttribute(unsigned int index, const std::string &attribName);//Requires a relink before openGL will register the change.

	// Returns -1 if the attribute doesn't exist. Hopefully this will not ever have to do so.
	int getAttriLocation(const std::string &attribName);
	// Returns -1 if uniform doesn't exist.
	int getUniformLocation(const std::string &uniformName);

	// Send Data to shaders with various overload parameters
	void sendUniform(const std::string & name, int integer);
	void sendUniform(const std::string & name, bool boolean);
	void sendUniform(const std::string & name, unsigned int unsigned_integer);
	void sendUniform(const std::string & name, float scalar);
	void sendUniformDouble(const std::string & name, double scalar);
	void sendUniform(const std::string & name, const satVec2 &vector);
	void sendUniform(const std::string & name, const satVec3 &vector);
	void sendUniform(const std::string & name, const satVec4 &vector);
	void sendUniform(const std::string & name, const satColor &color);
	void sendUniformPointLight(int i, PointLight &light);
	void sendUniformMat3(const std::string & name, float *matrix, bool transpose);
	void sendUniformMat4(const std::string & name, float *matrix, bool transpose);
	void sendUniformMat4(const std::string & name, float *matrix);



	void sendUniform(const GLint &location, int integer);
	void sendUniform(const GLint &location, bool boolean);
	void sendUniform(const GLint &location, unsigned int unsigned_integer);
	void sendUniform(const GLint &location, float scalar);
	void sendUniformDouble(const GLint &location, double scalar);
	void sendUniform(const GLint &location, const satVec2 &vector);
	void sendUniform(const GLint &location, const satVec3 &vector);
	void sendUniform(const GLint &location, const satVec4 &vector);
	void sendUniform(const GLint &location, const satColor &color);
	void sendUniformMat3(const GLint &location, float *matrix, bool transpose);
	void sendUniformMat4(const GLint &location, float *matrix, bool transpose);
	void sendUniformMat4(const GLint &location, float *matrix);


	

private:
	bool _IsInit = false;
	GLuint _VertShader	= 0;
	GLuint _FragShader		= 0;
	GLuint _GeoShader		= 0;
	GLuint _Program			= 0;

	std::string vertFilename;
	std::string fragFilename;
	std::string geomFilename;

	static bool _IsInitDefault;
	static GLuint _VertShaderDefault;
	static GLuint _FragShaderDefault;
	static GLuint _ProgramDefault;

	std::string readFile(const std::string &fileName) const;
	bool compileShader(GLuint shader) const;
	void outputShaderLog(GLuint shader) const; //Output the error list of a shader if the shader doesn't compile properly. Handy debugging. ^_^
	void outputProgramLog() const; //Outputs debug info relative to the entire shader program.


};

inline int ShaderProgram::getUniformLocation(const std::string &uniformName)
{
	int uniformLoc = glGetUniformLocation(_Program, uniformName.c_str());
	if(uniformLoc != -1)
	{
		return uniformLoc;
	}
	//std::cout << "WARNING: Uniform " << uniformName << " not found!" << std::endl;
	return uniformLoc;
}

// Send Data to shaders with various overload parameters
inline void ShaderProgram::sendUniform(const std::string & name, int integer)
{
	GLint location = getUniformLocation(name);
	glUniform1i(location, integer);
}

inline void ShaderProgram::sendUniform(const std::string & name, bool boolean)
{
	GLint location = getUniformLocation(name);
	glUniform1i(location, boolean);
}

inline void ShaderProgram::sendUniform(const std::string & name, unsigned int unsigned_integer)
{
	GLint location = getUniformLocation(name);
	glUniform1ui(location, unsigned_integer);
}

inline void ShaderProgram::sendUniform(const std::string & name, float scalar)
{
	GLint location = getUniformLocation(name);
	glUniform1f(location, scalar);
}

inline void ShaderProgram::sendUniformDouble(const std::string & name, double scalar)
{
	GLint location = getUniformLocation(name);
	glUniform1d(location, scalar);
}

inline void ShaderProgram::sendUniform(const std::string & name, const satVec2 &vector)
{
	GLint location = getUniformLocation(name);
	glUniform2f(location, vector.x, vector.y);
}

inline void ShaderProgram::sendUniform(const std::string & name, const satVec3 &vector)
{
	GLint location = getUniformLocation(name);
	glUniform3f(location, vector.x, vector.y, vector.z);
}

inline void ShaderProgram::sendUniform(const std::string & name, const satVec4 &vector)
{
	GLint location = getUniformLocation(name);
	glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}

inline void ShaderProgram::sendUniform(const std::string & name, const satColor &color)
{
	GLint location = getUniformLocation(name);
	glUniform3f(location, color.r, color.g, color.b);
}

inline void ShaderProgram::sendUniformPointLight(int i, PointLight &light)
{
	sendUniform(("lights[" + std::to_string(i) + "].Position"), light.pos);
	sendUniform(("lights[" + std::to_string(i) + "].Color"), light.color);
	sendUniform(("lights[" + std::to_string(i) + "].Constant"), light.constant);
	sendUniform(("lights[" + std::to_string(i) + "].Linear"), light.linear);
	sendUniform(("lights[" + std::to_string(i) + "].Quadratic"), light.quad);
	//light.calcRadius();
	sendUniform(("lights[" + std::to_string(i) + "].Radius"), light.radius);
	sendUniform(("lights[" + std::to_string(i) + "].InnerRadius"), light.innerRadius);
}

inline void ShaderProgram::sendUniformMat3(const std::string & name, float *matrix, bool transpose)
{
	GLint location = getUniformLocation(name);
	glUniformMatrix3fv(location, 1, transpose, matrix);
}

inline void ShaderProgram::sendUniformMat4(const std::string & name, float *matrix, bool transpose)
{
	GLint location = getUniformLocation(name);
	glUniformMatrix4fv(location, 1, transpose, matrix);
}

inline void ShaderProgram::sendUniformMat4(const std::string & name, float *matrix)
{
	GLint location = getUniformLocation(name);
	glUniformMatrix4fv(location, 1, false, matrix);
}




// Send Data to shaders with various overload parameters
inline void ShaderProgram::sendUniform(const GLint &location, int integer)
{
	glUniform1i(location, integer);
}

inline void ShaderProgram::sendUniform(const GLint &location, bool boolean)
{
	glUniform1i(location, boolean);
}

inline void ShaderProgram::sendUniform(const GLint &location, unsigned int unsigned_integer)
{
	glUniform1ui(location, unsigned_integer);
}

inline void ShaderProgram::sendUniform(const GLint &location, float scalar)
{
	glUniform1f(location, scalar);
}

inline void ShaderProgram::sendUniformDouble(const GLint &location, double scalar)
{
	glUniform1d(location, scalar);
}

inline void ShaderProgram::sendUniform(const GLint &location, const satVec2 &vector)
{
	glUniform2f(location, vector.x, vector.y);
}

inline void ShaderProgram::sendUniform(const GLint &location, const satVec3 &vector)
{
	glUniform3f(location, vector.x, vector.y, vector.z);
}

inline void ShaderProgram::sendUniform(const GLint &location, const satVec4 &vector)
{
	glUniform4f(location, vector.x, vector.y, vector.z, vector.w);
}

inline void ShaderProgram::sendUniform(const GLint &location, const satColor &color)
{
	glUniform3f(location, color.r, color.g, color.b);
}

inline void ShaderProgram::sendUniformMat3(const GLint &location, float *matrix, bool transpose)
{
	glUniformMatrix3fv(location, 1, transpose, matrix);
}

inline void ShaderProgram::sendUniformMat4(const GLint &location, float *matrix, bool transpose)
{
	glUniformMatrix4fv(location, 1, transpose, matrix);
}

inline void ShaderProgram::sendUniformMat4(const GLint &location, float *matrix)
{
	glUniformMatrix4fv(location, 1, false, matrix);
}

