/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Texture.h"
#include "SOIL\SOIL.h"
#include "Math.h"
#include "DebugLog.h"

#include <iostream>

Texture::Texture(const std::string &file)
{
	load(file);
}

Texture::~Texture()
{
	unload();
}

bool Texture::load(const std::string &file)
{
	//TexObj = SOIL_load_OGL_texture(file.c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS);
	SetID(file);

	sf::Image tex;

	if (!tex.loadFromFile(file))
	{
		std::cout << "Failed to load texture from " << file << "\n";
		return false;
	}

	sizeX = tex.getSize().x;
	sizeY = tex.getSize().y;

	glGenTextures(1, &TexObj);
	glBindTexture(GL_TEXTURE_2D, TexObj);
	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGBA8, sizeX, sizeY, GL_RGBA, GL_UNSIGNED_BYTE, tex.getPixelsPtr());

	//size.x = float(tex.getSize().x);
	//size.y = float(tex.getSize().y);

	if (TexObj == 0)
	{
		std::cout << "[Texture.cpp] Texture failed to load from file " << file << "\n";
		return false;
	}
	
	glBindTexture(GL_TEXTURE_2D, TexObj);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); //u
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); //v
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 4.0f);	//MMM BABY THAT ANISOTROPIC FILTERING

	glBindTexture(GL_TEXTURE_2D, GL_NONE);

	//tex.

	return true;
}

bool Texture::load(const std::string &file, const float &anisotropyLevel, const GLint &filterMag, const GLint &filterMin, const GLint &wrapUV)
{
	SetID(file);

	float anisotropy = Between(anisotropyLevel, 1.0f, 32.0f);
	if(anisotropy > 1.0f && anisotropy < 2.0f)
	{
		anisotropy = 1.0f;
	}
	else if(anisotropy > 2.0f && anisotropy < 4.0f)
	{
		anisotropy = 2.0f;
	}
	else if(anisotropy > 4.0f && anisotropy < 8.0f)
	{
		anisotropy = 4.0f;
	}
	else if(anisotropy > 8.0f && anisotropy < 16.0f)
	{
		anisotropy = 8.0f;
	}
	else if(anisotropy > 16.0f && anisotropy < 32.0f)
	{
		anisotropy = 16.0f;
	}

	Anisotropy = anisotropy;
	FilterMag  = filterMag;
	FilterMin  = filterMin;
	WrapTexU  = wrapUV;
	WrapTexV  = wrapUV;

	bool mipMaps = false;

	if(anisotropy > 1.0f || (
		filterMin == GL_LINEAR_MIPMAP_LINEAR  || filterMin == GL_LINEAR_MIPMAP_NEAREST  || 
		filterMin == GL_NEAREST_MIPMAP_LINEAR || filterMin == GL_NEAREST_MIPMAP_NEAREST ))
	{
		mipMaps = true;
	}

	glGenTextures(1, &TexObj);
	glActiveTexture(GL_TEXTURE0);

	int width, height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_2D, TexObj);
	TexObj = SOIL_load_OGL_texture(file.c_str(), SOIL_LOAD_RGBA, SOIL_CREATE_NEW_ID, (SOIL_FLAG_MIPMAPS * mipMaps) | (0 * SOIL_FLAG_INVERT_Y));
	
	if (TexObj == 0)
	{
		std::cout << "[Texture.cpp] Texture failed to load from file " << file << "\n";
		return false;
	}

	glBindTexture(GL_TEXTURE_2D, TexObj);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterMag); //GL_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterMin); //GL_LINEAR_MIPMAP_LINEAR

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, WrapTexU); //u	GL_CLAMP_TO_EDGE
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, WrapTexV); //v	GL_CLAMP_TO_EDGE
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Anisotropy);	//MMM BABY THAT ANISOTROPIC FILTERING

	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	
	return true;
}


GLuint Texture::loadCubemap(std::vector<const GLchar*> faces)
{
	glGenTextures(1, &TexObj);
	glActiveTexture(GL_TEXTURE0);

	int width, height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_CUBE_MAP, TexObj);
	for (GLuint i = 0; i < faces.size(); i++)
	{
		image = SOIL_load_image(faces[i], &width, &height, 0, SOIL_LOAD_RGB);
        glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
            GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
        );
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);

	return TexObj;
}

GLuint Texture::loadCubemap(const std::string filename)
{
	std::vector<std::string> faces;
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Right.png");
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Left.png");
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Top.png");
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Bottom.png");
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Front.png");
	faces.push_back("../Assets/Textures/CubeMap/" + filename + "Back.png");

	glGenTextures(1, &TexObj);
	glActiveTexture(GL_TEXTURE0);

	int width, height;
	unsigned char* image;

	glBindTexture(GL_TEXTURE_CUBE_MAP, TexObj);
	for (GLuint i = 0; i < faces.size(); i++)
	{
		image = SOIL_load_image(faces[i].c_str(), &width, &height, 0, SOIL_LOAD_RGB);
		glTexImage2D(
			GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0,
			GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, image
		);
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
	
	return TexObj;
}

void Texture::unload()
{
	if (TexObj != 0)
	{
		//remove data from GPU
		glDeleteTextures(1, &TexObj);
		TexObj = 0;
	}
}

void Texture::setAnisotropyLevel(const float level)
{
	Anisotropy = level;
	glBindTexture(GL_TEXTURE_2D, TexObj);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, level);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
}