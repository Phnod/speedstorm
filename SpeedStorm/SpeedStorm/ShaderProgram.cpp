/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "ShaderProgram.h"
#include <fstream>
#include <iostream>
#include <vector>

bool ShaderProgram::_IsInitDefault = false;
GLuint ShaderProgram::_VertShaderDefault = 0;
GLuint ShaderProgram::_FragShaderDefault = 0;
GLuint ShaderProgram::_ProgramDefault = 0;

ShaderProgram::ShaderProgram()
{

}

ShaderProgram::ShaderProgram(std::string const& vertShader, std::string const& fragShader)
{
	load(vertShader, fragShader);
}

ShaderProgram::ShaderProgram(std::string const& vertShader, std::string const& fragShader, std::string const& geoShader)
{
	load(vertShader, fragShader, geoShader);
}

ShaderProgram::~ShaderProgram()
{
	if (_IsInit)
	{
		unload();
	}
}

bool ShaderProgram::initDefault()
{
	if (!_IsInitDefault)
	{
		ShaderProgram errorShader;
		bool compileSuccess = errorShader.load("../assets/shaders/error.vert", "../assets/shaders/error.frag");
		if (!compileSuccess)
		{
			std::cout << "Could not compile default Shader!";
			system("PAUSE");
			exit(EXIT_FAILURE);
		}
		ShaderProgram::_VertShaderDefault = errorShader._VertShader;
		ShaderProgram::_FragShaderDefault = errorShader._FragShader;
		ShaderProgram::_ProgramDefault = errorShader._Program;
		_IsInitDefault = true;
	}
	return _IsInitDefault;
}

void ShaderProgram::setDefault()
{
	_VertShader = _VertShaderDefault;
	_FragShader = _FragShaderDefault;
	_Program = _ProgramDefault;
}

bool ShaderProgram::load(const std::string &vertFile, const std::string &fragFile) //Load vertex and fragment shader files, place them in the program object.
{
	//create shader and program objects
	vertFilename = vertFile;
	fragFilename = fragFile;

	_VertShader = glCreateShader(GL_VERTEX_SHADER);
	_FragShader = glCreateShader(GL_FRAGMENT_SHADER);
	_Program = glCreateProgram();

	//Load our source code for shaders
	std::string source = readFile(vertFile);
	const GLchar *temp = static_cast<const GLchar *>(source.c_str());
	glShaderSource(_VertShader, 1, &temp, NULL);

	source = readFile(fragFile);
	temp = static_cast<const GLchar *>(source.c_str());
	glShaderSource(_FragShader, 1, &temp, NULL);

	//Compile the code
	if (!compileShader(_VertShader))
	{ //If the vertex shader doesn't compile, output an error message.
		std::cout << "Vertex Shader failed to compile.\n";
		outputShaderLog(_VertShader);
		unload();
		setDefault();
		return false;
	}

	if (!compileShader(_FragShader))
	{ //If the frag shader doesn't compile, output an error message.
		std::cout << "Fragment Shader failed to compile.\n";
		outputShaderLog(_FragShader);
		unload();
		setDefault();
		return false;
	}
	//set up our program object
	glAttachShader(_Program, _VertShader);
	glAttachShader(_Program, _FragShader);

	if (!linkProgram())
	{
		std::cout << "Shader Program failed to link.\n";
		outputProgramLog();
		unload();
		setDefault();
		return false;
	}
	_IsInit = true;
	return true;
}

bool ShaderProgram::load(const std::string &vertFile, const std::string &fragFile, const std::string &geoFile) //Load vertex and fragment shader files, place them in the program object.
{
	//create shader and program objects

	_VertShader = glCreateShader(GL_VERTEX_SHADER);
	_FragShader = glCreateShader(GL_FRAGMENT_SHADER);
	_GeoShader = glCreateShader(GL_GEOMETRY_SHADER);
	_Program = glCreateProgram();

	//Load our source code for shaders
	std::string source = readFile(vertFile);
	const GLchar *temp = static_cast<const GLchar *>(source.c_str());
	glShaderSource(_VertShader, 1, &temp, NULL);

	source = readFile(fragFile);
	temp = static_cast<const GLchar *>(source.c_str());
	glShaderSource(_FragShader, 1, &temp, NULL);

	source = readFile(geoFile);
	temp = static_cast<const GLchar *>(source.c_str());
	glShaderSource(_GeoShader, 1, &temp, NULL);

	//Compile the code
	if (!compileShader(_VertShader))
	{ //If the vertex shader doesn't compile, output an error message.
		std::cout << "Vertex Shader failed to compile.\n";
		outputShaderLog(_VertShader);
		unload();
		setDefault();
		system("pause");
		return false;
	}

	if (!compileShader(_FragShader))
	{ //If the frag shader doesn't compile, output an error message.
		std::cout << "Fragment Shader failed to compile.\n";
		outputShaderLog(_FragShader);
		unload();
		setDefault();
		system("pause");
		return false;
	}

	if (!compileShader(_GeoShader))
	{ //If the frag shader doesn't compile, output an error message.
		std::cout << "Geometry Shader failed to compile.\n";
		outputShaderLog(_GeoShader);
		unload();
		setDefault();
		system("pause");
		return false;
	}

	//set up our program object
	glAttachShader(_Program, _VertShader);
	glAttachShader(_Program, _FragShader);
	glAttachShader(_Program, _GeoShader);

	if (!linkProgram())
	{
		std::cout << "Shader Program failed to link.\n";
		outputProgramLog();
		unload();
		setDefault();
		system("pause");
		return false;
	}
	_IsInit = true;
	return true;
}

bool ShaderProgram::reload()
{
	unload();
	return load(vertFilename, fragFilename);
}

bool ShaderProgram::isLoaded() const //Allow us to check if the files are loaded
{
	return _IsInit;
}

void ShaderProgram::unload() //Clear all shader data from OpenGL
{
	if (_VertShader != 0) //If the Vertex shader is not null, remove it.
	{
		glDetachShader(_Program, _VertShader);
		glDeleteShader(_VertShader);
		_VertShader = 0;
	}
	if (_FragShader != 0)
	{
		glDetachShader(_Program, _FragShader);
		glDeleteShader(_FragShader);
		_FragShader = 0;
	}
	if (_Program != 0) //If the Vertex shader is not null, remove it.
	{
		glDeleteProgram(_Program);
		_Program = 0;
	}
	_IsInit = false;
}

bool ShaderProgram::linkProgram() //Allows us to manually link/relink the program from the outside.
{
	glLinkProgram(_Program);
	GLint success;
	glGetProgramiv(_Program, GL_LINK_STATUS, &success);
	return success == GL_TRUE;
}

void ShaderProgram::bind() const //Bind shaders to data - the shader becomes the active shader.
{
	glUseProgram(_Program);
}

void ShaderProgram::unbind() //Unbind shaders from data - no longer active shader.
{
	glUseProgram(0);
}

void ShaderProgram::addAttribute(unsigned int index, const std::string &attribName)  //Requires a relink before openGL will register the change.
{
	glBindAttribLocation(_Program, index, attribName.c_str());
}

// Returns -1 if the attribute doesn't exist. Hopefully this will not ever have to do so.
int ShaderProgram::getAttriLocation(const std::string &attribName)
{
	return glGetAttribLocation(_Program, attribName.c_str());
}

// Returns -1 if uniform doesn't exist.


//We want to compile a list of all uniforms in a shader and store those, since the uniform isn't going to change without specific input.
//It would be faster to implement a lookup table.


std::string ShaderProgram::readFile(const std::string &fileName) const
{
	std::ifstream inStream(fileName);
	if (!inStream.good())
	{
		std::cout << "Shader File not found:\"" <<fileName<< "\"\n";
		return std::string();
	}
	std::string data(std::istreambuf_iterator<char>(inStream), (std::istreambuf_iterator<char>()));
	return data;
}

bool ShaderProgram::compileShader(GLuint shader) const
{
	glCompileShader(shader);
	GLint success;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
	return success == GL_TRUE;
}
	
void ShaderProgram::outputShaderLog(GLuint shader) const //Output the error list of a shader if the shader doesn't compile properly. Handy debugging. ^_^
{
	std::vector<char> infoLog;
	infoLog.resize(512);

	GLint infoLen;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLen);
	glGetShaderInfoLog(shader, sizeof(char) * 512, &infoLen, &infoLog[0]); //Size of char array in bits for portability, rather than characters.
	std::cout << std::string(infoLog.begin(), infoLog.end()) << "\n";
}

void ShaderProgram::outputProgramLog() const // Outputs debug info relative to the entire shader program.
{
	std::vector<char> infoLog;
	infoLog.resize(512);

	GLint infoLen;
	glGetProgramiv(_Program, GL_INFO_LOG_LENGTH, &infoLen);
	glGetProgramInfoLog(_Program, sizeof(char) * 512, &infoLen, &infoLog[0]); // Size of char array in bits for portability, rather than characters.
	std::cout << std::string(infoLog.begin(), infoLog.end()) << "\n";
}