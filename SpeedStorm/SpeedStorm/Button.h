/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Vector.h"
#include <string>

class Button
{
public:
	Button();

	bool canClick;
	satVec2 pos; //camera pos -1 to 1
	satVec3 menuPos; //position in the world
	satVec2 screenPos; //position on the screen
	satVec2 size;	//width and height
	satVec2 menuSize; //size of the menu
	satVec2 screenSize; //size of the window
	std::string name;

	bool bClick(satVec2 mousePos);  //return whether mouse is over and can click button
	bool bHover(satVec2 mousePos);	//return whether mouse is over button
	void updateWindowSize(satVec2 WindowSize);
};