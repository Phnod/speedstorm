/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.
#include "Mesh.h"
#include <fstream>
#include <iostream>

#define CHAR_BUFFER_SIZE 128 //Size of the character buffer.
#define BUFFER_OFFSET(i) ((char *)0 + (i))

Mesh::Mesh(){

}

Mesh::Mesh(std::string meshFile, bool isBinary)
{
	SetID(meshFile);

	if (isBinary == false)
	{
		bool result = LoadFile(meshFile);
		//if(loaded == true)
		//{
		//	result = SatGetFileTime(meshFile, createTime, lastReadTime, lastWriteTime);
		//	std:: cout << GetLastError();
		//	
		//	if(result == true)
		//	{
		//
		//	}
		//	else
		//	{
		//
		//	}
		//}
		//else
		//{
		//	//system("PAUSE");
		//}
		//
		//handle =  CreateFile(StringToLPCWSTR(meshFile), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		//
		//if(handle != INVALID_HANDLE_VALUE)
		//{
		//	GetFileTime(handle, createTime, lastReadTime, lastWriteTime);
		//
		//	CloseHandle(handle);
		//}
		//else
		//{
		//	std::cout << "OH NO MAN, THAT MODEL IS MESSED\n" << meshFile <<  "\n" << GetLastError() << std::endl;
			
		//	//exit(0);
		//}		
		//
		//LoadFile(meshFile);
	}
	else
	{
		LoadFromBinary(meshFile);
	}
}

Mesh::~Mesh(){

}
//OBJ face structure

struct MeshFace
{
	MeshFace()
	{
		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;

		textureUVs[0] = 0;
		textureUVs[1] = 0;
		textureUVs[2] = 0;

		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;

		tangent[0] = 0;
		tangent[1] = 0;
		tangent[2] = 0;
	}

	MeshFace(unsigned v1, unsigned v2, unsigned v3,
		unsigned u1, unsigned u2, unsigned u3,
		unsigned n1, unsigned n2, unsigned n3,
		unsigned t1, unsigned t2, unsigned t3)
	{
		vertices[0] = v1;
		vertices[1] = v2;
		vertices[2] = v3;

		textureUVs[0] = u1;
		textureUVs[1] = u2;
		textureUVs[2] = u3;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;

		tangent[0] = t1;
		tangent[1] = t2;
		tangent[2] = t3;
	}

	unsigned vertices[3];
	unsigned textureUVs[3];
	unsigned normals[3];
	unsigned tangent[3];
	unsigned bitangent[3];
};


bool Mesh::LoadFromObj(const std::string &file)
{
	_File = file;
	SetID(file);

	std::ifstream inFile(file);

	if (!inFile)
	{
		std::cout << "Could not open the file " << file << " for reading." << std::endl;
		loaded = false;
		return false;
	}

	char line[CHAR_BUFFER_SIZE];

	// Unique Data
	MeshFace temp;

	std::vector<satVec3> v;
	std::vector<satVec2> vt;
	std::vector<satVec3> vn;
	std::vector<MeshFace> f;
	float x, y, z;

	// OpenGL Data
	std::vector<float> unpackedPositionData;
	std::vector<float> unpackedUVData;
	std::vector<float> unpackedNormalData;
	std::vector<float> unpackedTangentData;
	std::vector<float> unpackedBiTangentData;

	while (!inFile.eof())
	{
		inFile.getline(line, CHAR_BUFFER_SIZE);

		switch (line[0])
		{
		case 'v':
			switch (line[1])
			{
			case ' ':
				std::sscanf(line, "v %f %f %f", &x, &y, &z);
				v.push_back(satVec3(x, y, z));
				break;
			case 't':
				std::sscanf(line, "vt %f %f", &x, &y);
				vt.push_back(satVec2(x, y));
				break;
			case 'n':
				std::sscanf(line, "vn %f %f %f", &x, &y, &z);
				vn.push_back(satVec3(x, y, z));
				break;
			}
			break;
		case 'f':
			std::sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]);
			f.push_back(temp);
			break;
		case 'o':
		case '#':
		case 'm':
		default:
			break;
		}
	}

	inFile.close();

	for (unsigned i = 0; i < f.size(); ++i)
	{
		for (unsigned j = 0; j < 3; ++j)
		{
			unpackedPositionData.push_back(v[f[i].vertices[j] - 1].x);
			unpackedPositionData.push_back(v[f[i].vertices[j] - 1].y);
			unpackedPositionData.push_back(v[f[i].vertices[j] - 1].z);

			unpackedUVData.push_back(vt[f[i].textureUVs[j] - 1].x);
			unpackedUVData.push_back(vt[f[i].textureUVs[j] - 1].y);

			unpackedNormalData.push_back(vn[f[i].normals[j] - 1].x);
			unpackedNormalData.push_back(vn[f[i].normals[j] - 1].y);
			unpackedNormalData.push_back(vn[f[i].normals[j] - 1].z);

			//satVec3 tan1;
			//satVec3 tan2;
			//
			//float x1 = v[f[i].vertices[1] - 1].x - v[f[i].vertices[0] - 1].x;
			//float x2 = v[f[i].vertices[2] - 1].x - v[f[i].vertices[0] - 1].x;
			//float y1 = v[f[i].vertices[1] - 1].y - v[f[i].vertices[0] - 1].y;
			//float y2 = v[f[i].vertices[2] - 1].y - v[f[i].vertices[0] - 1].y;
			//float z1 = v[f[i].vertices[1] - 1].z - v[f[i].vertices[0] - 1].z;
			//float z2 = v[f[i].vertices[2] - 1].z - v[f[i].vertices[0] - 1].z;
			//
			//float s1 = vt[f[i].textureUVs[1] - 1].x - vt[f[i].textureUVs[0] - 1].x;
			//float s2 = vt[f[i].textureUVs[2] - 1].x - vt[f[i].textureUVs[0] - 1].x;
			//float t1 = vt[f[i].textureUVs[1] - 1].y - vt[f[i].textureUVs[0] - 1].y;
			//float t2 = vt[f[i].textureUVs[2] - 1].y - vt[f[i].textureUVs[0] - 1].y;
			//
			//float r = 1.0F / (s1 * t2 - s2 * t1);
			//satVec3 sdir((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r,
			//	(t2 * z1 - t1 * z2) * r);
			//satVec3 tdir((s1 * x2 - s2 * x1) * r, (s1 * y2 - s2 * y1) * r,
			//	(s1 * z2 - s2 * z1) * r);
			//
			//tan1[i1] += sdir;
			//tan1[i2] += sdir;
			//tan1[i3] += sdir;
			//
			//tan2[i1] += tdir;
			//tan2[i2] += tdir;
			//tan2[i3] += tdir;
			//
			//satVec3 tangent;
			//
			//unpackedTangentData;
			//unpackedBiTangentData;

		}
	}

	////v[f[i].vertices[j] - 1].x
	//for (unsigned int i = 0; i < f.size(); i += 3)
	//{
	//	//MeshFace& v0 = f[i];
	//	//MeshFace& v1 = f[i + 1];
	//	//MeshFace& v2 = f[i + 2];
	//	//
	//	//v[f[i].vertices[0] - 1].x;
	//	//
	//	//satVec3 Edge1 = v1.vertices - v0.vertices;
	//	//satVec3 Edge2 = v2.vertices - v0.vertices;
	//	//
	//	//float DeltaU1 = v1.textureUVs.x - v0.textureUVs.x;
	//	//float DeltaV1 = v1.textureUVs.y - v0.textureUVs.y;
	//	//float DeltaU2 = v2.textureUVs.x - v0.textureUVs.x;
	//	//float DeltaV2 = v2.textureUVs.y - v0.textureUVs.y;
	//	//
	//	//float f = 1.0f / (DeltaU1 * DeltaV2 - DeltaU2 * DeltaV1);
	//	//
	//	//satVec3 Tangent, Bitangent;
	//	//
	//	//Tangent.x = f * (DeltaV2 * Edge1.x - DeltaV1 * Edge2.x);
	//	//Tangent.y = f * (DeltaV2 * Edge1.y - DeltaV1 * Edge2.y);
	//	//Tangent.z = f * (DeltaV2 * Edge1.z - DeltaV1 * Edge2.z);
	//	//
	//	//Bitangent.x = f * (-DeltaU2 * Edge1.x - DeltaU1 * Edge2.x);
	//	//Bitangent.y = f * (-DeltaU2 * Edge1.y - DeltaU1 * Edge2.y);
	//	//Bitangent.z = f * (-DeltaU2 * Edge1.z - DeltaU1 * Edge2.z);
	//	//
	//	//v0.tangent += Tangent;
	//	//v1.tangent += Tangent;
	//	//v2.tangent += Tangent;
	//}
	//
	//for (unsigned int i = 0; i < Vertices.size(); i++)
	//{
	//	Vertices[i].m_tangent.Normalize();
	//}

	_NumFaces = f.size();
	_NumVert = _NumFaces * 3;

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO_Vert);
	glGenBuffers(1, &VBO_UVs);
	glGenBuffers(1, &VBO_Normals);

	glBindVertexArray(VAO);

	glEnableVertexAttribArray(0); //< Position
	glEnableVertexAttribArray(1); //< UVs
	glEnableVertexAttribArray(2); //< Normals

	size_t vertSize = sizeof(float) * unpackedPositionData.size();
	size_t uvSize = sizeof(float) * unpackedUVData.size();
	size_t normSize = sizeof(float) * unpackedNormalData.size();

	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	glBufferData(GL_ARRAY_BUFFER, vertSize, &unpackedPositionData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	glBufferData(GL_ARRAY_BUFFER, uvSize, &unpackedUVData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	glBufferData(GL_ARRAY_BUFFER, normSize, &unpackedNormalData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	v.clear();
	vt.clear();
	vn.clear();
	f.clear();
	unpackedPositionData.clear();
	unpackedUVData.clear();
	unpackedNormalData.clear();
	loaded = true;
	return true;
}

bool Mesh::LoadFromBinary(const std::string &file)
{
	//file
	
	_File = file;
	SetID(file);

	FILE *binaryFile = nullptr;
	binaryFile = fopen(file.c_str(), "rb");

	if (binaryFile == nullptr)
	{
		std::cout << "Could not open file for reading: " << file << std::endl;
		return false;
	}

	fread(&_NumFaces, sizeof(unsigned int), 1, binaryFile);
	fread(&_NumVert, sizeof(unsigned int), 1, binaryFile);

	float *vertices = new float[_NumVert * 3];
	float *uvs = new float[_NumVert * 2];
	float *normals = new float[_NumVert * 3];

	fread(vertices, sizeof(float), _NumVert * 3, binaryFile);
	fread(uvs, sizeof(float), _NumVert * 2, binaryFile);
	fread(normals, sizeof(float), _NumVert * 3, binaryFile);

	fclose(binaryFile);

	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO_Vert);
	glGenBuffers(1, &VBO_UVs);
	glGenBuffers(1, &VBO_Normals);

	glBindVertexArray(VAO);

	glEnableVertexAttribArray(0); //< Position
	glEnableVertexAttribArray(1); //< UVs
	glEnableVertexAttribArray(2); //< Normals

	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVert * 3, vertices, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVert * 2, uvs, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * _NumVert * 3, normals, GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);

	delete[] vertices;
	delete[] uvs;
	delete[] normals;

	return true;
}



//Load a mesh from file and sent it to v-memory
bool Mesh::LoadFile(const std::string &file)
{
	std::ifstream input;
	input.open(file);

	if (!input)
	{
		std::cout << "Error: Could not open file \"" << file << "\"!\n";
		loaded = false;
		return false;
		
	}
	char line[CHAR_BUFFER_SIZE];

	satVec3 topCorn; //Bounding box corner
	satVec3 btmCorn; //Bounding box corner

	//Unique data
	std::vector<satVec3> vertexData;
	std::vector<satVec2> textureData;
	std::vector<satVec3> normalData;
	//index/face data
	std::vector<MeshFace> faceData;
	
	float x, y, z;
	MeshFace faceTemp;

	//Use the face data to create an array of verts, uvs, and normals for faster use in openGL
	std::vector<float> unPackedVertexData;
	std::vector<float> unPackedTextureData;
	std::vector<float> unPackedNormalData;
	std::vector<float> unPackedTangentData;
	std::vector<float> unPackedBitangentData;
	std::vector<float> unPackedInterleaveData;

	while (!input.eof()) //While the file has not ended, continue pulling in data.
	{
		input.getline(line, CHAR_BUFFER_SIZE);

		switch (line[0])
		{
		case 'v':
			switch (line[1])
			{
			case ' ':
				std::sscanf(line, "v %f %f %f", &x, &y, &z);
				vertexData.push_back(satVec3(x, y, z));

				if (vertexData.size() <= 1)
				{
					topCorn = { x, y, z };
					btmCorn = { x, y, z };
				}


				if (x > topCorn.x)
				{
					topCorn.x = x;
				}
				if (x < btmCorn.x)
				{
					btmCorn.x = x;
				}
				if (y > topCorn.y)
				{
					topCorn.y = y;
				}
				if (y < btmCorn.y)
				{
					btmCorn.y = y;
				}
				if (z > topCorn.z)
				{
					topCorn.z = z;
				}
				if (z < btmCorn.z)
				{
					btmCorn.z = z;
				}
				break;
			case 't':
				std::sscanf(line, "vt %f %f", &x, &y);
				textureData.push_back(satVec2(x, y));
				break;
			case 'n':
				std::sscanf(line, "vn %f %f %f", &x, &y, &z);
				normalData.push_back(satVec3(x, y, z));
				break;
			}
			break;
		case 'f':
			std::sscanf(line, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&faceTemp.vertices[0], &faceTemp.textureUVs[0], &faceTemp.normals[0],
				&faceTemp.vertices[1], &faceTemp.textureUVs[1], &faceTemp.normals[1],
				&faceTemp.vertices[2], &faceTemp.textureUVs[2], &faceTemp.normals[2]);
			faceData.push_back(faceTemp);
			break;
		case 'o':
		case '#':
		case 'm':
		default:
			break;
		}
	}
	input.close();

	//Unpack the data
	for (unsigned i = 0; i < faceData.size(); i++)
	{				
		satVec3 vertex0, vertex1, vertex2;
		satVec2 uv0, uv1, uv2;

		vertex0 = vertexData[faceData[i].vertices[0] - 1];
		vertex1 = vertexData[faceData[i].vertices[1] - 1];
		vertex2 = vertexData[faceData[i].vertices[2] - 1];

		uv0 = textureData[faceData[i].textureUVs[0] - 1];
		uv1 = textureData[faceData[i].textureUVs[1] - 1];
		uv2 = textureData[faceData[i].textureUVs[2] - 1];

		satVec3 DeltaPos1 = vertex1 - vertex0;
		satVec3 DeltaPos2 = vertex2 - vertex0;

		satVec2 DeltaUV1 = uv1 - uv0;
		satVec2 DeltaUV2 = uv2 - uv0;
		
		float f = 1.0f / (DeltaUV1.x * DeltaUV2.y - DeltaUV1.y * DeltaUV2.x);
		
		satVec3 Tangent, Bitangent;
		
		Tangent = f * (DeltaPos1 * DeltaUV2.y - DeltaPos2 * DeltaUV1.y);
		Bitangent = f * (DeltaPos2 * DeltaUV1.x - DeltaPos1 * DeltaUV2.x);

		//std::cout << "Tangent:   " << Tangent << std::endl;
		//std::cout << "Bitangent: " << Bitangent << std::endl;

		//v0.tangent += Tangent;
		//v1.tangent += Tangent;
		//v2.tangent += Tangent;

		//satVec3 

		for (unsigned j = 0; j < 3; j++)
		{
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].x);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].y);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].z);//We use -1 because OBJ specs start at 1 instead of 0.
			unPackedVertexData.push_back(1.0f);

			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].u);
			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].v);
			unPackedTextureData.push_back(0.0f);
			unPackedTextureData.push_back(1.0f);

			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].x);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].y);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].z);
			unPackedNormalData.push_back(1.0f);	

			//unPackedNormalData.push_back(satVec3::Cross(Tangent, Bitangent).x);
			//unPackedNormalData.push_back(satVec3::Cross(Tangent, Bitangent).y);
			//unPackedNormalData.push_back(satVec3::Cross(Tangent, Bitangent).z);
			//unPackedNormalData.push_back(1.0f);	

			unPackedTangentData.push_back(Tangent.x);
			unPackedTangentData.push_back(Tangent.y);
			unPackedTangentData.push_back(Tangent.z);
			unPackedTangentData.push_back(1.0f);	

			unPackedBitangentData.push_back(Bitangent.x);
			unPackedBitangentData.push_back(Bitangent.y);
			unPackedBitangentData.push_back(Bitangent.z);
			unPackedBitangentData.push_back(1.0f);	
		}

		

	}
	_NumFaces = faceData.size();
	_NumVert = _NumFaces * 3;

//Send data to OpenGL.
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO_Vert);
	glGenBuffers(1, &VBO_UVs);
	glGenBuffers(1, &VBO_Normals);
	glGenBuffers(1, &VBO_Tangents);
	glGenBuffers(1, &VBO_Bitangents);

	glBindVertexArray(VAO);
	//The VAO keeps track of what we're doing with the VBOs until it is unbound.
	//This lets us keep track of what we specify for all our VBOs and their shader data, instead of repeating what's inside the buffers each time.
	//It's like handing OpenGL a list instead of orally telling it each frame.

	glEnableVertexAttribArray(0); //Vertex
	glEnableVertexAttribArray(1); //Uvs
	glEnableVertexAttribArray(2); //Normals
	glEnableVertexAttribArray(3); //Tangent
	glEnableVertexAttribArray(4); //Normal

	////Vertex
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedInterleaveData.size(), &unPackedInterleaveData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4 * 5, (void*)(sizeof(float) * 0));
	//glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4 * 5, (void*)(sizeof(float) * 4));
	//glVertexAttribPointer((GLuint)2, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4 * 5, (void*)(sizeof(float) * 8));
	//glVertexAttribPointer((GLuint)3, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4 * 5, (void*)(sizeof(float) * 12));
	//glVertexAttribPointer((GLuint)4, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4 * 5, (void*)(sizeof(float) * 16));

	////Vertex
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedVertexData.size(), &unPackedVertexData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(sizeof(float) * 0 * 4));
	//
	////Uvs
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTextureData.size(), &unPackedTextureData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(sizeof(float) * 0 * 4));
	//
	////Normals
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalData.size(), &unPackedNormalData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)2, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(sizeof(float) * 0 * 4));
	//
	////Tangents
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Tangents);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalData.size(), &unPackedNormalData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)3, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(sizeof(float) * 0 * 4));
	//
	////Bitangents
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Bitangents);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalData.size(), &unPackedNormalData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)4, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(sizeof(float) * 0 * 4));




	//Vertex
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedVertexData.size(), &unPackedVertexData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(0));
	
	//Uvs
	glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTextureData.size(), &unPackedTextureData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(0));
	
	//Normals
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalData.size(), &unPackedNormalData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(0));
	
	//Tangents
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Tangents);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTangentData.size(), &unPackedTangentData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)3, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(0));
	
	//Bitangents
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Bitangents);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedBitangentData.size(), &unPackedBitangentData[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)4, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 4, BUFFER_OFFSET(0));
	
	//OBJ data is now modern GL ready!

	//Begin cleanup

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind VBO
	glBindVertexArray(0); //Unbind VAO

	vertexData.clear(); //Clear the vectors from RAM now that everything's in the GPU.
	textureData.clear();
	normalData.clear();
	faceData.clear();
	unPackedVertexData.clear();
	unPackedTextureData.clear();
	unPackedNormalData.clear();
	unPackedInterleaveData.clear();
	loaded = true;
	return true;
}



//release data from system and GPU memory
void Mesh::Unload() //Clears the mesh data from the GPU
{
	//Remove buffers in the opposite order they were created
	glDeleteBuffers(1, &VBO_Bitangents);
	glDeleteBuffers(1, &VBO_Tangents);
	glDeleteBuffers(1, &VBO_Normals);
	glDeleteBuffers(1, &VBO_UVs);
	glDeleteBuffers(1, &VBO_Vert);
	glDeleteVertexArrays(1, &VAO);
	//Clear out contents of variables.
	VBO_Bitangents = 0;
	VBO_Tangents = 0;
	VBO_Normals = 0;
	VBO_UVs = 0;
	VBO_Vert = 0;
	VAO = 0;

	_NumFaces = 0;
	_NumVert = 0;

	loaded = 0;
}

unsigned int Mesh::GetNumFaces() const
{
	return _NumFaces;
}
unsigned int Mesh::GetNumVertices() const
{
	return _NumVert;
}

void Mesh::Bind() const
{
	glBindVertexArray(VAO);
}

void Mesh::Unbind()
{
	glBindVertexArray(0);
}

void Mesh::Draw() const
{
	glDrawArrays(GL_TRIANGLES, 0, _NumVert);
}


















//FRAME ANIMATION

Frame::Frame(){

}

Frame::Frame(std::string meshFileA, std::string meshFileB)
{
	LoadFile(meshFileA, meshFileB);
}

Frame::~Frame(){

}
//OBJ face structure


//Load a mesh from file and sent it to v-memory
bool Frame::LoadFile(const std::string &fileA, const std::string &fileB)
{
	std::ifstream input;
	input.open(fileA);

	if (!input)
	{
		std::cout << "Error: Could not open file \"" << fileA << "\"!\n";
		return false;

	}
	char inputString[CHAR_BUFFER_SIZE];

	satVec3 topCorn; //Bounding box corner
	satVec3 btmCorn; //Bounding box corner

	//Unique data
	std::vector<satVec3> vertexDataA;
	std::vector<satVec2> textureDataA;
	std::vector<satVec3> normalDataA;
	//index/face data
	std::vector<MeshFace> faceDataA;
	//Use the face data to create an array of verts, uvs, and normals for faster use in openGL
	std::vector<float> unPackedVertexDataA;
	std::vector<float> unPackedTextureDataA;
	std::vector<float> unPackedNormalDataA;

	std::vector<satVec3> vertexDataB;
	std::vector<satVec2> textureDataB;
	std::vector<satVec3> normalDataB;
	//index/face data
	std::vector<MeshFace> faceDataB;
	//Use the face data to create an array of verts, uvs, and normals for faster use in openGL
	std::vector<float> unPackedVertexDataB;
	std::vector<float> unPackedTextureDataB;
	std::vector<float> unPackedNormalDataB;

	while (!input.eof()) //While the file has not ended, continue pulling in data.
	{
		input.getline(inputString, CHAR_BUFFER_SIZE);
		if (std::strstr(inputString, "#") != nullptr) //strstr, or string string, checks to see if a string contains a substring. It's like substring, 
			//but with the entire string instead of a set amount of characters. For this line, we want to handle comments.
		{
			//This line is a comment. This line in the code is also a comment, coincidence?
			continue;

		}
		else if (std::strstr(inputString, "vt") != nullptr)
		{
			//This line has uv data
			satVec2 temp;
			std::sscanf(inputString, "vt %f %f", &temp.x, &temp.y);
			textureDataA.push_back(temp);

		}
		else if (std::strstr(inputString, "vn") != nullptr)
		{
			//This line has normal data
			satVec3 temp;
			std::sscanf(inputString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normalDataA.push_back(temp);

		}
		else if (std::strstr(inputString, "v") != nullptr)
		{
			//This line has vertex data
			satVec3 temp;
			std::sscanf(inputString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			vertexDataA.push_back(temp);

			if (vertexDataA.size() <= 1)
			{
				topCorn = { temp.x, temp.y, temp.z };
				btmCorn = { temp.x, temp.y, temp.z };
			}

			if (temp.x > topCorn.x)
			{
				topCorn.x = temp.x;
			}
			if (temp.x < btmCorn.x)
			{
				btmCorn.x = temp.x;
			}
			if (temp.y > topCorn.y)
			{
				topCorn.y = temp.y;
			}
			if (temp.y < btmCorn.y)
			{
				btmCorn.y = temp.y;
			}
			if (temp.z > topCorn.z)
			{
				topCorn.z = temp.z;
			}
			if (temp.z < btmCorn.z)
			{
				btmCorn.z = temp.z;
			}
		}


		else if (std::strstr(inputString, "f") != nullptr)
		{
			//This line contains face data.
			MeshFace temp;

			std::sscanf(inputString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]
				);
			faceDataA.push_back(temp);

		}
	}
	input.close();




//	std::ifstream input;
	input.open(fileB);

	if (!input)
	{
		std::cout << "Error: Could not open file \"" << fileB << "\"!\n";
		return false;

	}


	while (!input.eof()) //While the file has not ended, continue pulling in data.
	{
		input.getline(inputString, CHAR_BUFFER_SIZE);
		if (std::strstr(inputString, "#") != nullptr) //strstr, or string string, checks to see if a string contains a substring. It's like substring, 
			//but with the entire string instead of a set amount of characters. For this line, we want to handle comments.
		{
			//This line is a comment. This line in the code is also a comment, coincidence?
			continue;

		}
		else if (std::strstr(inputString, "vt") != nullptr)
		{
			//This line has uv data
			satVec2 temp;
			std::sscanf(inputString, "vt %f %f", &temp.x, &temp.y);
			textureDataB.push_back(temp);

		}
		else if (std::strstr(inputString, "vn") != nullptr)
		{
			//This line has normal data
			satVec3 temp;
			std::sscanf(inputString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normalDataB.push_back(temp);

		}
		else if (std::strstr(inputString, "v") != nullptr)
		{
			//This line has vertex data
			satVec3 temp;
			std::sscanf(inputString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			vertexDataB.push_back(temp);

			//if (vertexDataB.size() <= 1)
			//{
			//	topCorn = { temp.x, temp.y, temp.z };
			//	btmCorn = { temp.x, temp.y, temp.z };
			//}
			//
			//if (temp.x > topCorn.x)
			//{
			//	topCorn.x = temp.x;
			//}
			//if (temp.x < btmCorn.x)
			//{
			//	btmCorn.x = temp.x;
			//}
			//if (temp.y > topCorn.y)
			//{
			//	topCorn.y = temp.y;
			//}
			//if (temp.y < btmCorn.y)
			//{
			//	btmCorn.y = temp.y;
			//}
			//if (temp.z > topCorn.z)
			//{
			//	topCorn.z = temp.z;
			//}
			//if (temp.z < btmCorn.z)
			//{
			//	btmCorn.z = temp.z;
			//}
		}


		else if (std::strstr(inputString, "f") != nullptr)
		{
			//This line contains face data.
			MeshFace temp;

			std::sscanf(inputString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]
				);
			faceDataB.push_back(temp);

		}
	}
	input.close();

	//satVec3 midPos;	//Middle of the obj, calculated by taking the average of the corners
	//
	//midPos = { (btmCorn.x + topCorn.x) / 2, (btmCorn.y + topCorn.y) / 2, (btmCorn.z + topCorn.z) / 2 };
	//
	//for (unsigned i = 0; i < vertexData.size(); i++)
	//{
	//	vertexData[i] -= midPos;
	//}

	//Unpack the data
	for (unsigned i = 0; i < faceDataA.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			unPackedVertexDataA.push_back(vertexDataA[faceDataA[i].vertices[j] - 1].x);
			unPackedVertexDataA.push_back(vertexDataA[faceDataA[i].vertices[j] - 1].y);
			unPackedVertexDataA.push_back(vertexDataA[faceDataA[i].vertices[j] - 1].z);//We use -1 because OBJ specs start at 1 instead of 0.

			unPackedTextureDataA.push_back(textureDataA[faceDataA[i].textureUVs[j] - 1].u);
			unPackedTextureDataA.push_back(textureDataA[faceDataA[i].textureUVs[j] - 1].v);

			unPackedNormalDataA.push_back(normalDataA[faceDataA[i].normals[j] - 1].x);
			unPackedNormalDataA.push_back(normalDataA[faceDataA[i].normals[j] - 1].y);
			unPackedNormalDataA.push_back(normalDataA[faceDataA[i].normals[j] - 1].z);
		}

	}

	for (unsigned i = 0; i < faceDataB.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			unPackedVertexDataB.push_back(vertexDataB[faceDataB[i].vertices[j] - 1].x);
			unPackedVertexDataB.push_back(vertexDataB[faceDataB[i].vertices[j] - 1].y);
			unPackedVertexDataB.push_back(vertexDataB[faceDataB[i].vertices[j] - 1].z);//We use -1 because OBJ specs start at 1 instead of 0.

			unPackedTextureDataB.push_back(textureDataB[faceDataB[i].textureUVs[j] - 1].u);
			unPackedTextureDataB.push_back(textureDataB[faceDataB[i].textureUVs[j] - 1].v);

			unPackedNormalDataB.push_back(normalDataB[faceDataB[i].normals[j] - 1].x);
			unPackedNormalDataB.push_back(normalDataB[faceDataB[i].normals[j] - 1].y);
			unPackedNormalDataB.push_back(normalDataB[faceDataB[i].normals[j] - 1].z);
		}

	}
	_NumFaces = faceDataA.size();
	_NumVert = _NumFaces * 3;

	//unsigned vao = NULL;
	unsigned vbo = NULL;
	unsigned ubo = NULL;
	unsigned nbo = NULL;

	//glGenVertexArrays(1, &vao);
	glGenBuffers(1, &vbo);
	glGenBuffers(1, &ubo);
	glGenBuffers(1, &nbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  unPackedVertexDataA.size(), &unPackedVertexDataA[0], GL_STATIC_DRAW);

	
	glBindBuffer(GL_ARRAY_BUFFER, ubo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTextureDataA.size(), &unPackedTextureDataA[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, nbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  unPackedNormalDataA.size(), &unPackedNormalDataA[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	VBO_Vert.push_back(vbo);
	VBO_UVs.push_back(ubo);	
	VBO_Normals.push_back(nbo);
	//Send data to OpenGL.

	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  unPackedVertexDataB.size(), &unPackedVertexDataB[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, nbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) *  unPackedNormalDataB.size(), &unPackedNormalDataB[0], GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	VBO_Vert.push_back(vbo);
	VBO_Normals.push_back(nbo);

	unsigned frame;
	glGenVertexArrays(1, &frame);
	glBindVertexArray(frame);
	VAO.push_back(frame);

	//glGenVertexArrays(1, &VAO[0]);
	glGenBuffers(1, &VBO_Vert[0]);
	glGenBuffers(1, &VBO_UVs[0]);
	glGenBuffers(1, &VBO_Normals[0]);

	glBindVertexArray(VAO[0]);
	//The VAO keeps track of what we're doing with the VBOs until it is unbound.
	//This lets us keep track of what we specify for all our VBOs and their shader data, instead of repeating what's inside the buffers each time.
	//It's like handing OpenGL a list instead of orally telling it each frame.

	glEnableVertexAttribArray(0); //Vertex A
	glEnableVertexAttribArray(1); //UVs A
	glEnableVertexAttribArray(2); //Normals A
	glEnableVertexAttribArray(3); //Vertex B
	glEnableVertexAttribArray(4); //Normals B

	//Vertex A
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedVertexDataA.size(), &unPackedVertexDataA[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	//UVs A
	glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTextureDataA.size(), &unPackedTextureDataA[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, BUFFER_OFFSET(0));
	//Normals A
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalDataA.size(), &unPackedNormalDataA[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	//Vertex B
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedVertexDataB.size(), &unPackedVertexDataB[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)3, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	//Normals B
	glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals[1]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalDataB.size(), &unPackedNormalDataB[0], GL_STATIC_DRAW);
	glVertexAttribPointer((GLuint)4, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));

	//OBJ data is now modern GL ready!

	//Begin cleanup

	glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind VBO
	glBindVertexArray(0); //Unbind VAO

	vertexDataA.clear(); //Clear the vectors from RAM now that everything's in the GPU.
	textureDataA.clear();
	normalDataA.clear();
	faceDataA.clear();
	unPackedVertexDataA.clear();
	unPackedTextureDataA.clear();
	unPackedNormalDataA.clear();

	vertexDataB.clear(); //Clear the vectors from RAM now that everything's in the GPU.
	textureDataB.clear();
	normalDataB.clear();
	faceDataB.clear();
	unPackedVertexDataB.clear();
	unPackedTextureDataB.clear();
	unPackedNormalDataB.clear();

	

	return true;
}
//release data from system and GPU memory
void Frame::Unload() //Clears the mesh data from the GPU
{
	//Remove buffers in the opposite order they were created
	glDeleteBuffers(VBO_Normals.size(), &VBO_Normals[0]);
	glDeleteBuffers(VBO_UVs.size(), &VBO_UVs[0]);
	glDeleteBuffers(VBO_Vert.size(), &VBO_Vert[0]);
	glDeleteVertexArrays(VAO.size()-1, &VAO[0]);
	//Clear out contents of variables.
	VBO_Normals.clear();
	VBO_UVs.clear();
	VBO_Vert.clear();
	VAO.clear();

	_NumFaces = 0;
	_NumVert = 0;
}

unsigned int Frame::GetNumFaces() const
{
	return _NumFaces;
}
unsigned int Frame::GetNumVertices() const
{
	return _NumVert;
}

void Frame::Bind() const
{
	glBindVertexArray(VAO[0]);
}

void Frame::Unbind()
{
	glBindVertexArray(0);
}

void Frame::Draw() const
{
	glDrawArrays(GL_TRIANGLES, 0, _NumVert);
}

