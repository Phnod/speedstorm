/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include <string>
#include <windows.h>	

typedef	__int8	s8;
typedef	__int16	s16;
typedef	__int32	s32;
typedef __int64	s64; 

static HANDLE handle = INVALID_HANDLE_VALUE;

void InitFullScreenQuad();

void DrawFullScreenQuad();

unsigned StringToID(std::string const& _string);

LPCWSTR StringToLPCWSTR(const std::string& s);


bool SatGetFileTime(std::string _string, LPFILETIME &lpCreationTime, LPFILETIME &lpLastAccessTime, LPFILETIME &lpLastWriteTime);


//LPCWSTR StringToLPCWSTR(const std::string& s);
//{
//	std::wstring stemp = std::wstring(s.begin(), s.end());
//	LPCWSTR sw = stemp.c_str();
//	return sw;
//}

//unsigned StringToID(std::string const& str)
//{
//	unsigned id = 0;
//
//	for (size_t i = 0; i < str.size(); ++i)
//	{
//		//if (i % 2 == 0)
//		//	id += i * (int)str[i];
//		//else
//		//	id -= i * (int)str[i];
//
//		id += i * (unsigned)str[i];
//	}
//
//	return id;
//}