/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

//#include "Entity.h"
#include "Node.h"

class Camera;
typedef std::shared_ptr<Camera> CameraPtr;

class Camera : public Node
{
public:
	Camera();
	Camera(int width, int height);

	void Perspective(float fovyDegrees, float aspectRatio, float zNear, float zFar);
	void PerspectiveStereo(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skew, float convergence);
	void PerspectiveStereo(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY);

	void Orthographic(float top, float bottom, float left, float right, float zNear, float zFar);
	void OrthographicStereo(float top, float bottom, float left, float right, float zNear, float zFar, float skew, float convergence);
	void OrthographicStereo(float top, float bottom, float left, float right, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY);

	void Process() override;
	satMat4 GetView() const;
	float * GetUniformProjection();
	float * GetUniformProjectionInverse();
	satMat4 GetProjection();
	satMat4 GetProjectionInverse();
public:
	bool perspective;
	satMat4 projection;
	satMat4 projectionNoSkew;
};

//class Camera : public Node
//{
//public:
//	void Perspective(float fovyDegrees, float aspectRatio, float zNear, float zFar);
//	void Orthographic(float top, float bottom, float left, float right, float zNear, float zFar);
//	void Process() override;
//	satMat4 GetView() const;
//	float * GetUniformProjection();
//private:
//	satMat4 projection;
//};