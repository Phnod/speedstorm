/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <string>
#include <vector>
#include <GL/glew.h>
#include "Math.h"

struct MeshFaceFloat;

class MeshBinary
{
public:
	MeshBinary();
	~MeshBinary();

	//Load a mesh from file and sent it to memory
	bool WriteBinaryFile(const std::string &file, std::vector<MeshFaceFloat> &faceData);
	bool LoadFile(const std::string &file);
	//release data from system and GPU memory
	void Unload();

	unsigned int GetNumFaces() const;	 //These are worthless since I made it public but whatever
	unsigned int GetNumVertices() const; //These are worthless since I made it public but whatever

	//OpenGL buffer objects
	GLuint VBO_Vert = 0;
	GLuint VBO_UVs = 0;
	GLuint VBO_Normals = 0;
	GLuint VAO = 0; //VAO is needed for Modern OpenGL

	unsigned int _NumFaces = 0;
	unsigned int _NumVert = 0;

	//private:
	//Why the hell would I make anything private?

	//unsigned int _NumFaces = 0;
	//unsigned int _NumVert = 0;
};

