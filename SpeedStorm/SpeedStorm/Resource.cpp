/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Resource.h"

Resource::~Resource()
{

}

int Resource::GetID() const
{
	return ID;
}

bool Resource::HasID(int id)
{
	return (ID == id);
}

void Resource::SetID(std::string const& path)
{
	ID = StringToID(path);
}

void Resource::SetID(std::string const& path1, std::string const& path2)
{
	ID = StringToID(path1) + StringToID(path2);
}