/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "LevelList.h"
#include "Math.h"

namespace sat
{
	namespace level
	{
		template <typename T>
		void VectorResize(std::vector<T> &vec, unsigned int &u)
		{
			//if(vec.size() < u)
			//{
			//	vec.push_back(T());
			//	//vec.resize(u, T());
			//	//if(vec.size() > 1)
			//	//{
			//	//	vec[u] = vec[u - 1];
			//	//}
			//}
		}

		//template <typename T>
		//T calc(std::vector<List> list, T value, float shipPos)
		//{
		//	if(shipPos > list[list.size() - 1].pos)
		//	{
		//		return list[list.size() - 1].value;
		//	}
		//
		//	for(unsigned i = 1; i < list.size(); ++i)
		//	{
		//		if(shipPos < list[i].pos)
		//		{
		//			return Lerp(list[i-1].value, list[i].value, 
		//				InverseLerp(shipPos, list[i-1].pos, list[i].pos));
		//		}
		//	}
		//
		//	return list[1].value;
		//}

		void Resize(std::vector<ListLight> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				//std::cout << "List resized to " << vec.size() << std::endl;
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}

			//u = vec.size() - 1;
			//std::cout << "ListSize = " << vec.size() << std::endl;
		}

		void Resize(std::vector<ListFog> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListFogLayer> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListEye> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListWave> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListSpeed> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListSound> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = ListSound();
				}
			}
		}

		void Resize(std::vector<ListBloom> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListContrast> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListEnvironment> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListVisual> &vec, unsigned int &u)
		{
			while (vec.size() < u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListEnemy> &vec, unsigned int &u)
		{
			while (vec.size() < u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}



		void Resize(std::vector<ListBlockColor> &vec, unsigned int &u)
		{
			while (vec.size() < u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListShipLight> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}


		void Resize(std::vector<ListGreyscale> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListDOF> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListScanline> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListSobel> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListSonar> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListShadow> &vec, unsigned int &u)
		{
			while(vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if(vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<ListMusic> &vec, unsigned int &u)
		{
			while (vec.size() <= u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		void Resize(std::vector<BackgroundVisual> &vec, unsigned int &u)
		{
			while (vec.size() < u + 1)
			{
				vec.resize(vec.size() + 1);
				if (vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}
				
		void Resize(std::vector<Visual> &vec, unsigned int &u)
		{
			while(vec.size() < u + 1)
			{
				vec.resize(vec.size() + 1);
				if(vec.size() > 1)
				{
					vec[vec.size() - 1] = vec[vec.size() - 2];
				}
			}
		}

		List::List()
		{
			position = -1.0f;
		}

		ListLight::ListLight()
		{
			position = -1.0f;
			lightRim = 0.3f;
			lightAmbient = 0.1f;
			lightDiffuse = 0.5f;
			lightSpecular = 1.0f;
		}

		ListFog::ListFog()
		{
			FogLayer.resize(1);
			FogLayer[0] = ListFogLayer();
		}

		ListFogLayer::ListFogLayer()
		{

			position = -1.0f;
			FogNear = 40.0f;
			FogFar = 100.0f;
			FogRange = satVec2(0.3f, 0.5f);
			FogColor = 0.125f;
			FogBottomColor = 0.125f;
			FogTopColor = 0.125f;
			FogPosMult = 1.0f;
		}

		ListSound::ListSound()
		{
			soundFilename = "../Assets/Sound/Effect/Test/0000.ogg";
			sound;
			soundPlayed = true;
			soundBlank = true;
			frequencyRandom = 1.0f;
			frequencyMult = 1.0f;
			volume = 1.0f;
		}

		ListWave::ListWave()
		{
			position = -1.0f;
			intensity = 0.0f;
			count = 0.0f;
			time = 0.0f;
		}

		ListSpeed::ListSpeed()
		{
			position = -1.0f;
			mult = 1.0f;
			shrink = 0.5f;
			moving = satVec4(5.0f, 5.0f, 5.0f * 0.25f, 5.0f * 0.5f);
		}
		
		ListSpeed & ListSpeed::operator=(const ListSpeed &m)
		{
			moving = m.moving;
			mult = m.mult;
			shrink = m.shrink;
			useCatmull = m.useCatmull;
			
			return *this;
		}

		ListVisual::ListVisual()
		{
			environmentAmount = 0.0f;
			character = 'Q';
			transform = 0.0f;
			changeTo = '-';
			object.resize(1);
			object[0] = Visual();

			//collidable = false;
			//breakable = false;
			//shootable = false;
			//killer = false;
			//healable = false;
			//invinciblePickup = false;
			//doubleShootPickup = false;

			diffuseMultEqualOne = true;
			diffuseMult = 1.0f;

			emissiveMultEqualOne = true;
			emissiveMult = 1.0f;

			ambientMultEqualOne = true;
			ambientMult = 1.0f;

			specularMultEqualOne = true;
			specularMult = 1.0f;

			shinyMultEqualOne = true;
			shinyMult = 4.0f;


			diffuseAddEqualZero = true;
			diffuseAdd = 0.0f;

			emissiveAddEqualZero = true;
			emissiveAdd = 0.0f;

			ambientAddEqualZero = true;
			ambientAdd = 0.0f;

			specularAddEqualZero = true;
			specularAdd = 0.0f;

			shinyAddEqualZero = true;
			shinyAdd = 0.0f;
		}

		ListEnemy::ListEnemy()
		{
			environmentAmount = 0.0f;
			character = 'D';
			transform = 0.0f;
			changeTo = '-';
			object[0] = Visual();

			collidable = true;
			breakable = false;
			shootable = true;

			//LightAddMult(0.0f, 0.0f, 1.0f, satColor(0.0f, 0.0f, 0.0f), 5.0f, 0.0f, 1.0f, satColor(1.25f, 0.5f, 0.5f), 1.0f, 10.0f);

			diffuseMultEqualOne = true;
			diffuseMult = 1.0f;

			emissiveMultEqualOne = true;
			emissiveMult = 1.0f;

			ambientMultEqualOne = false;
			ambientMult = 0.0f;

			specularMultEqualOne = true;
			specularMult = 1.0f;

			shinyMultEqualOne = false;
			shinyMult = 10.0f;


			diffuseAddEqualZero = true;
			diffuseAdd = 0.0f;

			emissiveAddEqualZero = false;
			emissiveAdd = satColor(0.5, 0.0f, 0.0f);

			ambientAddEqualZero = true;
			ambientAdd = 0.0f;

			specularAddEqualZero = false;
			specularAdd = 1.0f;

			shinyAddEqualZero = false;
			shinyAdd = 5.0f;


			pointLightActive = true;
			pointLightColor = sat::color::lightred;
			pointLightMult = 1.0f;
			pointLightAttenMult = 1.0f;
			pointLightPos = satVec3(0.0f, 0.0f, 3.0f);
		}

		ListBloom::ListBloom()
		{
			position = -1.0f;
			bloomThresholdLower = 0.65f;
			bloomThresholdUpper = 1.0f;
		}

		ListContrast::ListContrast()
		{
			position = -1.0f;
			contrastAmount = 1.0f;
		}

		ListEnvironment::ListEnvironment()
		{
			position = -1.0f;
			backgroundAmount = 0.5f;
		}

		ListBlockColor::ListBlockColor()
		{
			ambientAdd = satColor(0.0f, 0.0f, 0.0f);
			diffuseAdd = satColor(0.0f, 0.0f, 0.0f);
			specularAdd =	satColor( 0.0f, 0.0f, 0.0f );
			emissiveAdd = satColor(0.0f, 0.0f, 0.0f);
			shinyAdd =		0.0f;
			ambientMult = satColor(1.0f, 1.0f, 1.0f);
			diffuseMult = satColor(1.0f, 1.0f, 1.0f);
			specularMult = satColor(1.0f, 1.0f, 1.0f);
			emissiveMult = satColor(1.0f, 1.0f, 1.0f);
			shinyMult = 10.0f;
		}


		ListBlockColor & ListBlockColor::operator=(const ListBlockColor &m)
		{
			material = m.material;
			return *this;
		}

		ListBlockColor::~ListBlockColor()
		{
			
		}
		

		ListBlockColorToggle::ListBlockColorToggle()
		{
			blockRandomToggle = 0;
			blockRandomToggleAmbientAdd = 0;
			blockRandomToggleAmbientMult = 0;
			blockRandomToggleDiffuseAdd = 0;
			blockRandomToggleDiffuseMult = 0;
			blockRandomToggleSpecularAdd = 0;
			blockRandomToggleSpecularMult = 0;
			blockRandomToggleEmissiveAdd = 0;
			blockRandomToggleEmissiveMult = 0;
			blockRandomToggleShinyAdd = 0;
			blockRandomToggleShinyMult = 0;
		}

		ListShipLight::ListShipLight()
		{
			position = -1.0f;
			colorMultiplier = 1.0f;
			constant = 1.0f;
			linear = 0.00375f * 2.0f;
			quad = 0.001875f * 2.0f;
			radius = PointLight::getRadius(constant, linear, quad);			
			innerRadius = 0.5f;
		}

		ListEye::ListEye()
		{
			position = -1.0f;
			pos = 0.0f;
			EyeShift = 0.0f;
			EyeDistanceMult = 0.2f;
			Convergence = 30.0f;
		}


		ListGreyscale::ListGreyscale()
		{
			position = -1.0f;
			amount = 0.0f;
			amountSepia = 0.0f;
		}

		ListDOF::ListDOF()
		{
			position = -1.0f;
			focus = 30.0f;
			//focus = 0.84f;
			bias = 0.05f;
			clamp = 0.0125f;

			biasMult = 1.0f;
			clampMult = 1.0f;
		}

		ListScanline::ListScanline()
		{
			position = -1.0f;
			Amount = satVec2(0.90f, 0.1250f);
			ModX = 5;
			ModY = 5;
			ThresholdX = 2;
			ThresholdY = 2;
		}

		BackgroundVisual::BackgroundVisual()
		{
			environmentAmount = 0.0f;
			transform = 0.0f;
			transformScale = 1.0f;
			transformRotate = 0.0f;
			rotate = 0.0f;
			object = Visual();
			
			appearLoop = 4.0f;

			speedRotate = 0.0f;
			rangeRotate = 0.0f;
			rangeSpeedRotate = 0.0f;

			diffuseMultEqualOne = true;
			diffuseMult = 1.0f;

			emissiveMultEqualOne = true;
			emissiveMult = 1.0f;
						
			ambientMultEqualOne = true;
			ambientMult = 1.0f;

			specularMultEqualOne = true;
			specularMult = 1.0f;
			
			shinyMultEqualOne = true;
			shinyMult = 1.0f;


			diffuseAddEqualZero = true;
			diffuseAdd = 0.0f;

			emissiveAddEqualZero = true;
			emissiveAdd = 0.0f;
						
			ambientAddEqualZero = true;
			ambientAdd = 0.0f;

			specularAddEqualZero = true;
			specularAdd = 0.0f;
			
			shinyAddEqualZero = true;
			shinyAdd = 0.0f;

		};

		ListMusic::ListMusic()
		{
			pitchMult = 1.0f;
			volumeMult = 1.0f;
			lowpass = 999999.0f;
			highpass = 0.0f;
		}

		ListSobel::ListSobel()
		{			
			normalThreshold			= 6.0f;
			depthThreshold			= 0.14f;
			normalThickThreshold	= 256.0f;
			depthThickThreshold		= 0.34f;
		}

		ListSonar::ListSonar()
		{
			colorAddMult = satVec4(0.0f, 0.5f, 1.0f, 0.5f);

			multiplier = 0.02f;
			diffusePass = 0.0f;
			emissivePass = 0.0f;
			fourthParam = 0.0f;
		}

		ListSonar & ListSonar::operator=(const ListSonar &m)
		{
			colorAddMult = m.colorAddMult;
			param = m.param;

			return *this;
		}
		
		
		ListShadow::ListShadow()
		{
                        FOV = 60.0f;

			rotation			= satVec3(0.0f, 20.0f, 0.0f);
			translation			= satVec3(35.0f, 0.0f, 60.0f);
			
			translationRotation = satVec3(0.0f, 0.0f, 0.0f);
			
			speedRotation		= satVec3(0.0f, 0.0f, 0.0f);
			rangeRotation		= satVec3(0.0f, 0.0f, 0.0f);
			rangeSpeedRotation	= satVec3(0.0f, 0.0f, 0.0f);

			rangeTranslation		= satVec3(0.0f, 0.0f, 0.0f);
			rangeSpeedTranslation	= satVec3(0.0f, 0.0f, 0.0f);
				
			rangeTranslationRotation		= satVec3(0.0f, 0.0f, 0.0f);
			rangeSpeedTranslationRotation	= satVec3(0.0f, 0.0f, 0.0f);
		}
	}
}

