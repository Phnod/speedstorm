/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
class satColor;
//class satMat2;

#include <ostream>

/*
//=======================================================================================
//
//	satVec2 - 2D vector
//
//=======================================================================================
*/

#pragma region satVec2

class satVec2
{
public:

	union
	{
		struct
		{
			float x, y;
		};

		struct
		{
			float u, v;
		};

		float rows[2];
	};

	satVec2();
	satVec2(const float x);
	satVec2(const float x, const float y);

	void				Set(const float x, const float y);

	float				operator[](int a) const;
	float&			operator[](int a);
	satVec2			operator-() const;
	satVec2			operator+(const satVec2 &a) const;
	satVec2			operator+(const int &a) const;
	satVec2			operator-(const satVec2 &a) const;

	float				Dot(const satVec2 &a) const;

	satVec2			operator*(const float a) const;
	satVec2			operator*(const satVec2 a) const;
	friend satVec2 operator*(const float a, const satVec2 b);
	satVec2			operator/(const float a) const;


	satVec2&			operator+=(const satVec2 &a);
	satVec2&			operator-=(const satVec2 &a);
	satVec2&			operator*=(const satVec2 &a);
	satVec2&			operator/=(const satVec2 &a);
	satVec2&			operator*=(const float a);
	satVec2&			operator/=(const float a);

	bool				operator==(const satVec2 &a) const;
	bool				operator!=(const satVec2 &a) const;

	

	void				Zero();

	satVec2			Scale(const satVec2 &a) const;

	void				Int();
	void				IntFloor();

	float				Length() const;
	float				LengthSqr() const;

	bool				Equals(const satVec2 &a) const;
	bool				Equals(const satVec2 &a, const float margin) const;		// compare with epsilon


	float				Normalize();
	void				MinMax(const satVec2 &min, const satVec2 &max);

	int				GetDimension() const;

	const float*	ToFloatPtr() const;
	float*			ToFloatPtr();

	void				Lerp			(const satVec2 &v0, const satVec2 &v1, const float l);
	void				LerpNoBounds	(const satVec2 &v0, const satVec2 &v1, const float l);
	static satVec2	LerpR(const satVec2 &v0, const satVec2 &v1, const float l);
	static satVec2	LerpNoBoundsR(const satVec2 &v0, const satVec2 &v1, const float l);
	void				BiLerp			(const satVec2 &v0, const satVec2 &v1, const satVec2 &v2, const satVec2 &v3, const float t0, const float t1);
	void				BiLerpNoBounds	(const satVec2 &v0, const satVec2 &v1, const satVec2 &v2, const satVec2 &v3, const float t0, const float t1);

};

inline std::ostream& operator<<(std::ostream& os, const satVec2& v)
{
	os << "| ";
	os << v.x << ' ' << v.y << ' ';
	os << "|";
	return os;
}

#pragma endregion satVec2

/*
//=======================================================================================
//
//	satVec3 - 3D vector
//
//=======================================================================================
*/

#pragma region satVec3

class satVec3
{
public:

	union
	{
		struct
		{
			float x, y, z;
		};
		struct
		{
			float r, g, b;
		};
		float rows[3];
	};

	satVec3();
	satVec3(const float xyz);
	satVec3(const float x, const float y, const float z);

	void 			Set(const float x, const float y, const float z);
	void			Zero();

	float			operator[](const int index) const;
	float &		operator[](const int index);
	satVec3		operator-() const;
	satVec3 &	operator=(const satVec3 &a);		// required because of a msvc 6 & 7 bug
	satVec3 &	operator=(const float a);
	float			Dot(const satVec3 &a) const;
	static float Dot(const satVec3 &a, const satVec3 &b);
	//float			Dot(satVec3 &a) const;
	satVec3		operator*(const satVec3 &a) const;
	satVec3		operator*(const float a) const;
	satVec3		operator/(const satVec3 &a) const;
	satVec3		operator/(const float a) const;
	satVec3		operator+(const satVec3 &a) const;
	satVec3		operator-(const satVec3 &a) const;
	satVec3 &	operator+=(const satVec3 &a);
	satVec3 &	operator-=(const satVec3 &a);
	satVec3 &	operator/=(const satVec3 &a);
	satVec3 &	operator*=(const satVec3 &a);
	satVec3 &	operator/=(const float a);
	satVec3 &	operator*=(const float a);

	friend satVec3	operator*(const float a, const satVec3 b);

	bool			Equals(const satVec3 &a) const;
	bool			Equals(const satVec3 &a, const float margin) const;
	bool			operator==(const satVec3 &a) const;
	bool			operator!=(const satVec3 &a) const;

	satVec2		xy() const;
	satVec2		yz() const;
	satVec2		xz() const;

	satVec2		setXY(float x, float y);
	satVec2		setYZ(float y, float z);
	satVec2		setXZ(float x, float z);

	satVec2		setXY(satVec2 vec);
	satVec2		setYZ(satVec2 vec);
	satVec2		setXZ(satVec2 vec);
	
	satVec2		setXY(satVec3 vec);
	satVec2		setYZ(satVec3 vec);
	satVec2		setXZ(satVec3 vec);

	satVec3		Cross(const satVec3 &a) const;
	static satVec3 	Cross(const satVec3 &a, const satVec3 &b);

	static satVec3 cross(const satVec3 &a, const satVec3 &b);

	float			Length() const;
	float			LengthSqr() const;
	float			Normalize();
	static satVec3	Normalize(const satVec3 &a);
	void			MinMax(const satVec3 &min, const satVec3 &max);
	void			Int();
	void			IntFloor();

	int			GetDimension() const;

	float					Yaw() const;
	float					Pitch() const;
	//satMat3			Mat3() const;		// sometime soon

	//const satColor & ToColor() const;
	const satVec2 &	ToVec2() const;
	satVec2 &			ToVec2();
	const float *		ToFloatPtr() const;
	float *				ToFloatPtr();

	void Lerp(const satVec3 &v0, const satVec3 &v1, const float l);
	void LerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float l);
	static satVec3		LerpR(const satVec3 &v0, const satVec3 &v1, const float l);
	static satVec3		LerpNoBoundsR(const satVec3 &v0, const satVec3 &v1, const float l);
	void				BiLerp(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1);
	void				BiLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1);
	void NLerp(const satVec3 &v0, const satVec3 &v1, const float l);
	void NLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float l);
	void SLerp(const satVec3 &v0, const satVec3 &v1, const float l);
	void SLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float l);
};

inline std::ostream& operator<<(std::ostream& os, const satVec3& v)
{
	os << "| ";
	os << v.x << '\t' << v.y << '\t' << v.z << '\t';
	os << "|";
	return os;
}

#pragma endregion satVec3

/*
//=======================================================================================
//
//	satVec4 - 4D vector
//
//=======================================================================================
*/

#pragma region satVec4

class satVec4 {
public:

	union
	{
		struct
		{
			float x, y, z, w;
		};
		float rows[4];
	};

	satVec4();
	satVec4(const float x);
	satVec4(const float x, const float y, const float z, const float w);
	satVec4(const satVec3& vec, const float w);

	void 			Set(const float x, const float y, const float z, const float w);
	void			Zero();

	float				operator[](const int index) const;
	float &			operator[](const int index);
	satVec4			operator-() const;
	satVec4&			operator=(const satVec4 &a);
	float				Dot(const satVec4 &a) const;
	satVec4			operator*(const satVec4 &a) const;
	satVec4			operator*(const float a) const;
	satVec4			operator/(const satVec4 &a) const;
	satVec4			operator/(const float a) const;
	satVec4			operator+(const satVec4 &a) const;
	satVec4			operator-(const satVec4 &a) const;
	satVec4 &		operator+=(const satVec4 &a);
	satVec4 &		operator-=(const satVec4 &a);
	satVec4 &		operator/=(const satVec4 &a);
	satVec4 &		operator/=(const float a);
	satVec4 &		operator*=(const satVec4 &a);
	satVec4 &		operator*=(const float a);

	friend satVec4	operator*(const float a, const satVec4 b);

	bool			Equals(const satVec4 &a) const;
	bool			Equals(const satVec4 &a, const float margin) const;
	bool			operator==(const satVec4 &a) const;
	bool			operator!=(const satVec4 &a) const;

	satVec2		xy() const;
	satVec2		yz() const;
	satVec2		xz() const;
	satVec3		xyz() const;

	float			Length() const;
	float			LengthSqr() const;
	float			Normalize();
	void			MinMax(const satVec4 &min, const satVec4 &max);
	void			Int();
	void			IntFloor();

	int				GetDimension() const;

	const satVec2 &	ToVec2() const;
	satVec2 &		ToVec2();
	const satVec3 &	ToVec3() const;
	satVec3 &		ToVec3();
	const float *	ToFloatPtr() const;
	float *			ToFloatPtr();

	void			Lerp(const satVec4 &v0, const satVec4 &v1, const float t);
	void			LerpNoBounds(const satVec4 &v0, const satVec4 &v1, const float t);
	static satVec4		LerpR(const satVec4 &v0, const satVec4 &v1, const float l);
	static satVec4		LerpNoBoundsR(const satVec4 &v0, const satVec4 &v1, const float l);
	void				BiLerp(const satVec4 &v0, const satVec4 &v1, const satVec4 &v2, const satVec4 &v3, const float t0, const float t1);
	void				BiLerpNoBounds(const satVec4 &v0, const satVec4 &v1, const satVec4 &v2, const satVec4 &v3, const float t0, const float t1);
};

inline std::ostream& operator<<(std::ostream& os, const satVec4& v)
{
	os << "| ";
	os << v.x << ' ' << v.y << ' ' << v.z << ' ' << v.w << ' ';
	os << "|";
	return os;
}

#pragma endregion satVec4







/*
//=======================================================================================
//
//	satVec3 - 3D vector
//
//=======================================================================================
*/

#pragma region satVec3

//=======================//
// satVec3::constructors //
//=======================//

inline satVec3::satVec3()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}

inline satVec3::satVec3(const float xyz)
{
	Set(xyz, xyz, xyz);
}

inline satVec3::satVec3(const float x, const float y, const float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

inline void satVec3::Set(const float x, const float y, const float z)
{
	this->x = x;
	this->y = y;
	this->z = z;
}

inline void satVec3::Zero()
{
	x = y = z = 0.0f;
}

//==============================//
// end of satVec3::constructors //
//==============================//

//====================//
// satVec3::operators //
//====================//

inline float	satVec3::operator[](int a) const
{
	return (&x)[a];
}

inline float& satVec3::operator[](int a)
{
	return (&x)[a];
}

inline satVec3 satVec3::operator-() const
{
	return satVec3(-x, -y, -z);
}

inline satVec3&	satVec3::operator=(const satVec3 &a)
{
	x = a.x;
	y = a.y;
	z = a.z;

	return *this;
}

inline satVec3&	satVec3::operator=(const float a)
{
	x = a;
	y = a;
	z = a;

	return *this;
}

inline satVec3 satVec3::operator+(const satVec3 &a) const
{
	return satVec3(x + a.x, y + a.y, z + a.z);
}

inline satVec3 satVec3::operator-(const satVec3 &a) const
{
	return satVec3(x - a.x, y - a.y, z - a.z);
}

inline float satVec3::Dot(const satVec3 &a) const
{
	return x * a.x + y * a.y + z * a.z;
}

inline float satVec3::Dot(const satVec3 &a, const satVec3 &b)
{
	return b.x * a.x + b.y * a.y + b.z * a.z;
}

//float satVec3::Dot(satVec3 &a) const
//{
//	return x * a.x + y * a.y + z * a.z;
//}

inline satVec3 satVec3::operator*(const float a) const
{
	return satVec3(x * a, y * a, z * a);
}

inline satVec3 operator*(const float a, const satVec3 b)
{
	return satVec3(b.x * a, b.y * a, b.z * a);
}

inline satVec3 satVec3::operator*(const satVec3 &a) const
{
	return satVec3(x * a.x, y * a.y, z * a.z);
}

inline satVec3	satVec3::operator/(const satVec3 &a) const
{
	return satVec3(x / a.x, y / a.y, z / a.z);
}

inline satVec3	satVec3::operator/(const float a) const
{
	return satVec3(x / a, y / a, z / a);
}

inline satVec3&	satVec3::operator+=(const satVec3 &a)
{
	this->x += a.x;
	this->y += a.y;
	this->z += a.z;

	return *this;
}

inline satVec3&	satVec3::operator-=(const satVec3 &a)
{
	this->x -= a.x;
	this->y -= a.y;
	this->z -= a.z;

	return *this;
}

inline satVec3&	satVec3::operator*=(const satVec3 &a)
{
	this->x *= a.x;
	this->y *= a.y;
	this->z *= a.z;

	return *this;
}

inline satVec3&	satVec3::operator/=(const satVec3 &a)
{
	this->x /= a.x;
	this->y /= a.y;
	this->z /= a.z;

	return *this;
}

inline satVec3&	satVec3::operator*=(const float a)
{
	this->x *= a;
	this->y *= a;
	this->z *= a;

	return *this;
}

inline satVec3&	satVec3::operator/=(const float a)
{
	this->x /= a;
	this->y /= a;
	this->z /= a;

	return *this;
}



inline bool satVec3::operator==(const satVec3 &a) const
{
	return Equals(a);
}

inline bool satVec3::operator!=(const satVec3 &a) const
{
	return !Equals(a);
}

//===========================//
// end of satVec3::operators //
//===========================//

//============================//
// satVec3::general functions //
//============================//

inline bool satVec3::Equals(const satVec3 &a) const
{
	return (x == a.x) && (y == a.y) && (z == a.z);
}

inline bool satVec3::Equals(const satVec3 &a, const float margin) const
{
	if (abs(x - a.x) > margin ||
		abs(y - a.y) > margin ||
		abs(z - a.z) > margin)
	{
		return false;
	}
	return true;
}

inline satVec2 satVec3::xy() const
{
	return satVec2(x, y);
}

inline satVec2 satVec3::yz() const
{
	return satVec2(y, z);
}

inline satVec2 satVec3::xz() const
{
	return satVec2(x, z);
}

inline satVec2		satVec3::setXY(float x, float y)
{
	this->x = x; this->y = y;
	return xy();
}

inline satVec2		satVec3::setYZ(float y, float z)
{
	this->y = y; this->z = z;
	return yz();
}

inline satVec2		satVec3::setXZ(float x, float z)
{
	this->x = x; this->z = z;
	return xz();
}

inline satVec2		satVec3::setXY(satVec2 vec)
{
	this->x = vec.x; this->y = vec.y;
	return xy();
}

inline satVec2		satVec3::setYZ(satVec2 vec)
{
	this->y = vec.x; this->z = vec.y;
	return yz();
}

inline satVec2		satVec3::setXZ(satVec2 vec)
{
	this->x = vec.x; this->z = vec.y;
	return xz();
}

inline satVec2		satVec3::setXY(satVec3 vec)
{
	this->x = vec.x; this->y = vec.y;
	return xy();
}

inline satVec2		satVec3::setYZ(satVec3 vec)
{
	this->y = vec.x; this->z = vec.y;
	return yz();
}

inline satVec2		satVec3::setXZ(satVec3 vec)
{
	this->x = vec.x; this->z = vec.z;
	return xz();
}

inline satVec3 satVec3::Cross(const satVec3 &a) const
{
	return satVec3(
		y * a.z - z * a.y,
		z * a.x - x * a.z,
		x * a.y - y * a.x);
}

inline satVec3 satVec3::Cross(const satVec3 &a, const satVec3 &b)
{
	return satVec3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline satVec3 satVec3::cross(const satVec3 &a, const satVec3 &b)
{
	return satVec3(
		a.y * b.z - a.z * b.y,
		a.z * b.x - a.x * b.z,
		a.x * b.y - a.y * b.x);
}

inline float satVec3::Length() const
{
	return sqrt(LengthSqr());
}

inline float satVec3::LengthSqr() const
{
	return (x*x + y*y + z*z);
}

inline float satVec3::Normalize()
{
	float sqr = LengthSqr(), length = 1.f / Length();
	*this *= length;
	
	return sqr * length;
}

inline satVec3 satVec3::Normalize(const satVec3 &a)
{
	float sqr = a.LengthSqr(), length = 1.f / a.Length();
	satVec3 temp = a;
	temp *= length;

	return temp;
}

inline void satVec3::MinMax(const satVec3 &min, const satVec3 &max)
{
	if (x < min.x)
	{
		x = min.x;
	}
	else if (x > max.x)
	{
		x = max.x;
	}

	if (y < min.y)
	{
		y = min.y;
	}
	else if (y > max.y)
	{
		y = max.y;
	}

	if (z < min.z)
	{
		z = min.z;
	}
	else if (z > max.z)
	{
		z = max.z;
	}
}

inline void satVec3::Int()
{
	x = floor(x + 0.5f);
	y = floor(y + 0.5f);
	z = floor(z + 0.5f);
}

inline void satVec3::IntFloor()
{
	x = float(int(x));
	y = float(int(y));
	z = float(int(z));
}

inline int satVec3::GetDimension() const
{
	return 3;
}

inline const satVec2 &satVec3::ToVec2() const
{
	return *reinterpret_cast<const satVec2 *>(this);
}

inline satVec2 &satVec3::ToVec2()
{
	return *reinterpret_cast<satVec2 *>(this);
}

//inline const satColor & satVec3::ToColor() const
//{
//	return satColor((float)x, (float)y, (float)z);
//}

inline const float* satVec3::ToFloatPtr() const
{
	return &x;
}

inline float* satVec3::ToFloatPtr()
{
	return &x;
}

//===================================//
// end of satVec3::general functions //
//===================================//


//==================//
// satVec3::Lerp()  // Linear interpolation
//==================// 

inline void satVec3::Lerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
	}
	else
	{
		*this = (1.0f - t) * v0 + (t)* v1;
		//*this = v0 + t * (v1 - v0);
	}
}

inline void satVec3::LerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//*this = v0 + t * (v1 - v0);
	*this = (1.0f - t) * v0 + (t)* v1;
}

inline satVec3 satVec3::LerpR(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		return v0;
	}
	else if (t >= 1.0f)
	{
		return v1;
	}
	else
	{
		return (1.0f - t) * v0 + (t)* v1;
		//return v0 + t * (v1 - v0);
	}
}

inline satVec3 satVec3::LerpNoBoundsR(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//return v0 + t * (v1 - v0);
	return (1.0f - t) * v0 + (t)* v1;
}

inline void	satVec3::BiLerp(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1)
{
	*this = LerpR(LerpR(v0, v1, t0), LerpR(v2, v3, t0), t1);
}

inline void	satVec3::BiLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const satVec3 &v2, const satVec3 &v3, const float t0, const float t1)
{
	*this = LerpNoBoundsR(LerpNoBoundsR(v0, v1, t0), LerpNoBoundsR(v2, v3, t0), t1);
}

//==================//
// satVec3::NLerp() // Normalized Linear interpolation
//==================// Vectors should be normalized

inline void satVec3::NLerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
		return;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
		return;
	}

	*this = Normalize((1.0f - t) * v0 + (t)* v1);
	//Normalize();
}

inline void satVec3::NLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	*this = Normalize((1.0f - t) * v0 + (t)* v1);
	//Normalize();
}

//==================//
// satVec3::SLerp() // Spherical Linear interpolation
//==================// Vectors should be normalized

inline void satVec3::SLerp(const satVec3 &v0, const satVec3 &v1, const float t)
{
	if (t <= 0.0f)
	{
		*this = v0;
		return;
	}
	else if (t >= 1.0f)
	{
		*this = v1;
		return;
	}

	//omega, cosOmega, sinOmega, scale0, scale1
	float cosOmega, scale0, scale1;
	cosOmega = v0.Dot(v1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = v0 * scale0 + v1 * scale1;
}

inline void satVec3::SLerpNoBounds(const satVec3 &v0, const satVec3 &v1, const float t)
{
	//omega, cosOmega, sinOmega, scale0, scale1

	float cosOmega, scale0, scale1;
	cosOmega = v0.Dot(v1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = v0 * scale0 + v1 * scale1;
}

#pragma endregion satVec3