/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <string>
#include <vector>

#include "Light.h"
#include "Color.h"
#include "Sound.h"
#include "Object.h"

namespace sat
{
	namespace level
	{
		

		class List
		{
		public:
			List();

			union
			{	struct
				{	float position; //// X position in level
				};				
				struct
				{	float pos; //// X position in level
				};
			};
		};

		class ListLight : public List
		{
		public:	
			ListLight();
			
			satColor lightRim;
			satColor lightAmbient;
			satColor lightDiffuse;
			satColor lightSpecular;
		};

		class ListFogLayer
		{	
		public:
			ListFogLayer();
			union
			{
				struct
				{
					float pos;
				};
				float position;
			};
			float FogNear;
			float FogFar;
			satVec2 FogRange;
			satColor FogColor;
			satColor FogBottomColor;
			satColor FogTopColor;
			satVec3 FogPosMult; 
			float FogEmissivePass = 0.0f;
			
			//the good stuff is here
			satVec2 offset = 0.0f;
			satVec2 offsetMult = 1.0f;
			satVec2 offsetPosMult = 0.00f;
			
			satVec2 velocity = 0.0f;
			satVec2 waveCount = 0.0f; //The number of waves on screen (This is multiplied by PI to get the correct value)
			satVec2 waveIntensity = 0.0f;	//How much of the screen do the waves move?
			satVec2 waveTime = 1.0f; //How fast the waves move (This value should remain constant, it'll get messed up otherwise)

			std::string imageFilename = "../Assets/Textures/Default/defaultTexture.png";
			Texture* image;
		};

		class ListFog
		{
		public:
			ListFog();
			
			std::vector<ListFogLayer> FogLayer;
		};


		class ListSound : public List
		{
		public:
			ListSound();
			std::string soundFilename;
			SoundLoc sound;
			bool soundPlayed;
			bool soundBlank;
			
			float frequencyRandom;
			float frequencyMult;
			float volume;
				
		};
	
		class ListWave : public List
		{
		public:
			ListWave();

			//float
			satVec2 count; //The number of waves on screen (This is multiplied by PI to get the correct value)
			satVec2 intensity;	//How much of the screen do the waves move?
			satVec2 time; //How fast the waves move (This value should remain constant, it'll get messed up otherwise)
		};

		class ListSpeed : public List
		{
		public:
			ListSpeed();

			ListSpeed & operator=(const ListSpeed &m);

			union
			{	
				struct
				{	
					satVec4 moving;	//xSpeed, ySpeed, backwardSpeed, forwardSpeed
				};
				//struct
				//{
					float xSpeed;
					float ySpeed;
					float backwardSpeed;					
					float forwardSpeed;					
				//};
			};

			satVec2 mult;
			satVec2 shrink;

			bool useCatmull = false;
		};


		class ListVisual //: public List
		{
		public:
			ListVisual();

			std::vector<Visual> object;
			char character;
			char changeTo;
			float environmentAmount;
			satVec3 originRotate = satVec3(1.0f, -1.0f, -1.0f);
			satVec3 transform = 0.0f;
			satVec3 transformScale = 1.0f;
			satVec3 transformRotate = 0.0f;
			satVec3 rotate;
			satVec3 speedRotate;
			satVec3 rangeRotate;
			satVec3 rangeSpeedRotate;
			
			satVec3 rotateRandomAngle = 0.0f;

			bool collidable = false;
			bool breakable = false;
			bool shootable = false;
			bool killer = false;
			bool healable = false;
			bool invinciblePickup = false;
			bool doubleShootPickup = false;
			bool autoShootPickup = false;

			satVec3 cascadeMult = satVec3(0.00000000015, 0.0000175, 0.0000175);
			satVec3 cascadeExp = satVec3(8.0f, 4.0f, 5.0f);
			satVec3 cascadeMultMod = satVec3(1.0f);
			satVec3 cascadeExpMod = satVec3(1.0f);

			bool		diffuseMultEqualOne;
			satColor	diffuseMult;
			bool		emissiveMultEqualOne;
			satColor	emissiveMult;			
			bool		ambientMultEqualOne;
			satColor	ambientMult;
			bool		specularMultEqualOne;
			satColor	specularMult;
			bool		shinyMultEqualOne;
			float		shinyMult;

			bool		diffuseAddEqualZero;
			satColor	diffuseAdd;
			bool		emissiveAddEqualZero;
			satColor	emissiveAdd;			
			bool		ambientAddEqualZero;
			satColor	ambientAdd;
			bool		specularAddEqualZero;
			satColor	specularAdd;
			bool		shinyAddEqualZero;
			float		shinyAdd;
		};

		
		
		class ListEnemy : public ListVisual
		{
		public:
			ListEnemy();
			
			bool		pointLightActive;
			satColor	pointLightColor;
			float		pointLightMult;
			float		pointLightAttenMult;
			satVec3		pointLightPos;
		};

		class ListBloom : public List
		{
		public:	
			ListBloom();
			
			float bloomThresholdLower;
			float bloomThresholdUpper;
		};
		
		class ListContrast : public List
		{
		public:	
			ListContrast();
			
			//bool contrastActive;
			float contrastAmount;
		};

		class ListEnvironment : public List
		{
		public:	
			ListEnvironment();
			
			//bool contrastActive;
			float backgroundAmount;
		};

		class ListBlockColor
		{
		public:
			ListBlockColor();	
			~ListBlockColor();

			ListBlockColor & operator=(const ListBlockColor &m);

			union
			{			
				struct
				{
					LightAddMult material;
				};
			
				struct
				{
					satColor ambientAdd;
					satColor diffuseAdd;
					satColor specularAdd;
					satColor emissiveAdd;
					float	 shinyAdd;
					satColor ambientMult;
					satColor diffuseMult;
					satColor specularMult;
					satColor emissiveMult;	
					float	 shinyMult;
				};
			};
		};


		class ListBlockColorToggle
		{
		public:
			ListBlockColorToggle();

			unsigned int blockRandomToggle;
			unsigned int blockRandomToggleAmbientAdd;
			unsigned int blockRandomToggleAmbientMult;
			unsigned int blockRandomToggleDiffuseAdd;
			unsigned int blockRandomToggleDiffuseMult;
			unsigned int blockRandomToggleSpecularAdd;
			unsigned int blockRandomToggleSpecularMult;
			unsigned int blockRandomToggleEmissiveAdd;
			unsigned int blockRandomToggleEmissiveMult;
			unsigned int blockRandomToggleShinyAdd;
			unsigned int blockRandomToggleShinyMult;		
		};

		class ListShipLight : public List
		{
		public:	
			ListShipLight();
			
			float colorMultiplier;

			float constant;
			float linear;
			float quad;
			float radius;

			float innerRadius;
		};



		class ListEye : public List
		{
		public:
			ListEye();
			
			satVec2 EyeShift;
			float EyeDistanceMult;
			float Convergence;
		};



		class ListGreyscale : public List
		{
		public:
			ListGreyscale();
			
			float amount;
			float amountSepia;
		};

		class ListDOF : public List
		{
		public:
			ListDOF();
			
			float focus;
			float bias;
			float clamp;
			float biasMult; 
			float clampMult;
		};

		class ListScanline : public List
		{
		public:
			ListScanline();

			satVec2 Amount;
			int ModX;
			int ModY;
			int ThresholdX;
			int ThresholdY;
		};


		class BackgroundVisual
		{
		public:
			BackgroundVisual();

			Visual object;
			float environmentAmount;
			satVec3 transform;
			satVec3 transformRotate;
			satVec3 rotate;
			satVec3 transformScale;

			satVec2 appearLoop;
			
			satVec3 speedRotate;
			satVec3 rangeRotate;
			satVec3 rangeSpeedRotate;

			bool		diffuseMultEqualOne;
			satColor	diffuseMult;
			bool		emissiveMultEqualOne;
			satColor	emissiveMult;			
			bool		ambientMultEqualOne;
			satColor	ambientMult;
			bool		specularMultEqualOne;
			satColor	specularMult;
			bool		shinyMultEqualOne;
			float		shinyMult;

			bool		diffuseAddEqualZero;
			satColor	diffuseAdd;
			bool		emissiveAddEqualZero;
			satColor	emissiveAdd;			
			bool		ambientAddEqualZero;
			satColor	ambientAdd;
			bool		specularAddEqualZero;
			satColor	specularAdd;
			bool		shinyAddEqualZero;
			float		shinyAdd;
		};



		class ListMusic : public List
		{
		public:
			ListMusic();
			
			float pitchMult;
			float volumeMult;
			float lowpass;
			float highpass;
		};


		class ListSobel : public List
		{
		public:
			ListSobel();

			float normalThreshold;
			float depthThreshold;
			float normalThickThreshold;
			float depthThickThreshold;
		};
		
		class ListSonar : public List
		{
		public:
			ListSonar();
			
			ListSonar & operator=(const ListSonar &m);

			union
			{
				struct
				{
					satColor colorAdd;
					float speedMult;
				};
				struct
				{
					satVec4 colorAddMult;
				};

			};

			union
			{
				struct
				{
					satVec4 param;
				};

				struct
				{
					float multiplier;
					float diffusePass;
					float emissivePass;
					float fourthParam;
				};
				
				
			};
		};

		class ListShadow : public List
		{
		public:
			ListShadow();

			float FOV;
			Texture* projectedTexture;

			satVec3 rotation;
			satVec3 translation;
			satVec3 translationRotation;

			satVec3 speedRotation;
			satVec3 rangeRotation;
			satVec3 rangeSpeedRotation;

			satVec3 rangeTranslation;
			satVec3 rangeSpeedTranslation;
						
			satVec3 rangeTranslationRotation;
			satVec3 rangeSpeedTranslationRotation;
		};

		class ListAsdfads
		{
		
		};

		template <typename T>
		void VectorResize(std::vector<T> &vec, unsigned int &u);
	
		//template <typename T>
		//T calc(std::vector<List> list, T value, float shipPos);

		void Resize(std::vector<ListLight> &vec, unsigned int &u);
		void Resize(std::vector<ListFog> &vec, unsigned int &u);
		void Resize(std::vector<ListFogLayer> &vec, unsigned int &u);
		void Resize(std::vector<ListEye> &vec, unsigned int &u);
		void Resize(std::vector<ListWave> &vec, unsigned int &u);
		void Resize(std::vector<ListSpeed> &vec, unsigned int &u);
		void Resize(std::vector<ListSound> &vec, unsigned int &u);
		void Resize(std::vector<ListBloom> &vec, unsigned int &u);
		void Resize(std::vector<ListContrast> &vec, unsigned int &u);
		void Resize(std::vector<ListEnvironment> &vec, unsigned int &u);
		void Resize(std::vector<ListShipLight> &vec, unsigned int &u);
		void Resize(std::vector<ListGreyscale> &vec, unsigned int &u);
		void Resize(std::vector<ListDOF> &vec, unsigned int &u);
		void Resize(std::vector<ListScanline> &vec, unsigned int &u);
		void Resize(std::vector<ListSobel> &vec, unsigned int &u);
		void Resize(std::vector<ListSonar> &vec, unsigned int &u);
		void Resize(std::vector<ListShadow> &vec, unsigned int &u);
		void Resize(std::vector<ListMusic> &vec, unsigned int &u);

		void Resize(std::vector<ListVisual> &vec, unsigned int &u);
		void Resize(std::vector<ListEnemy> &vec, unsigned int &u);
		void Resize(std::vector<ListBlockColor> &vec, unsigned int &u);
		void Resize(std::vector<BackgroundVisual> &vec, unsigned int &u);
		
		void Resize(std::vector<Visual> &vec, unsigned int &u);
	}
}