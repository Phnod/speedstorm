/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include <cstdio>
#include <Windows.h>
	
#define SAT_LOG(message, ...)			printf(message, ##__VA_ARGS__); printf("\n");

#define SAT_LOG_ERROR(message, ...)		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x0C);\
										printf(message, ##__VA_ARGS__);	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x07); printf("\n");

#define SAT_LOG_WARNING(message, ...)	SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x0E);	printf(message, ##__VA_ARGS__);\
										SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x07); printf("\n");

#define SAT_LOG_SAFE(message, ...)		SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE),0x0A);	printf(message, ##__VA_ARGS__);\
										SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), 0x07); printf("\n");