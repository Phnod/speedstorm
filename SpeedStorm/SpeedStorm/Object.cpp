/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "ResourceManager.h"
#include "Object.h"


Visual::Visual(const std::string& mesh, 
	const std::string& diffuse, 
	const std::string& normal,
	const std::string& specular,
	const std::string& emissive)
{
	modelFilename = mesh;
	diffuseFilename = diffuse;
	normalFilename = normal;
	specularFilename = specular;
	emissiveFilename = emissive;	

	Load();
}

void Visual::Load()
{
	LoadModel();
	LoadDiffuse();
	LoadNormal();
	LoadSpecular();
	LoadEmissive();

	InitializeTextureParam();
	InitializePrimitiveParam();

	diffuse->FilterMag = textureFilterMag;
	diffuse->FilterMin = textureFilterMin;
	diffuse->WrapTexU = textureWrapTexU;
	diffuse->WrapTexV = textureWrapTexV;

	normal->FilterMag = textureFilterMag;
	normal->FilterMin = textureFilterMin;
	normal->WrapTexU = textureWrapTexU;
	normal->WrapTexV = textureWrapTexV;

	specular->FilterMag = textureFilterMag;
	specular->FilterMin = textureFilterMin;
	specular->WrapTexU = textureWrapTexU;
	specular->WrapTexV = textureWrapTexV;

	emissive->FilterMag = textureFilterMag;
	emissive->FilterMin = textureFilterMin;
	emissive->WrapTexU = textureWrapTexU;
	emissive->WrapTexV = textureWrapTexV;
}





void Visual::LoadModel()
{
	model = ResourceManager::AddMesh(modelFilename, false);
}

void Visual::LoadDiffuse()
{
	diffuse = ResourceManager::AddTexture(diffuseFilename);
}

void Visual::LoadNormal()
{
	normal = ResourceManager::AddTexture(normalFilename);
}

void Visual::LoadSpecular()
{
	specular = ResourceManager::AddTexture(specularFilename);
}

void Visual::LoadEmissive()
{
	emissive = ResourceManager::AddTexture(emissiveFilename);
}

void Visual::InitializeTextureParam()
{
	if(textureStringFilterMag == "GL_LINEAR")
	{
		textureFilterMag = GL_LINEAR;
	}
	else if(textureStringFilterMag == "GL_NEAREST")
	{
		textureFilterMag = GL_NEAREST;
	}
	//else if(textureStringFilterMag == "GL_LINEAR_MIPMAP_LINEAR")
	//{
	//	textureFilterMag = GL_LINEAR_MIPMAP_LINEAR;
	//}
	//else if(textureStringFilterMag == "GL_LINEAR_MIPMAP_LINEAR")
	//{
	//	textureFilterMag = GL_LINEAR_MIPMAP_LINEAR;
	//}

	if(textureStringFilterMin == "GL_LINEAR")
	{
		textureFilterMin = GL_LINEAR;
	}
	else if(textureStringFilterMin == "GL_NEAREST")
	{
		textureFilterMin = GL_NEAREST;
	}
	else if(textureStringFilterMin == "GL_LINEAR_MIPMAP_LINEAR")
	{
		textureFilterMin = GL_LINEAR_MIPMAP_LINEAR;
	}
	else if(textureStringFilterMin == "GL_LINEAR_MIPMAP_NEAREST")
	{
		textureFilterMin = GL_LINEAR_MIPMAP_NEAREST;
	}
	else if(textureStringFilterMin == "GL_NEAREST_MIPMAP_LINEAR")
	{
		textureFilterMin = GL_NEAREST_MIPMAP_LINEAR;
	}
	else if(textureStringFilterMin == "GL_NEAREST_MIPMAP_NEAREST")
	{
		textureFilterMin = GL_NEAREST_MIPMAP_NEAREST;
	}

	if(textureStringWrapTexU == "GL_CLAMP_TO_EDGE")
	{
		textureWrapTexU = GL_CLAMP_TO_EDGE;
	}
	else if(textureStringWrapTexU == "GL_CLAMP_TO_BORDER")
	{
		textureWrapTexU = GL_CLAMP_TO_BORDER;
	}
	else if(textureStringWrapTexU == "GL_REPEAT")
	{
		textureWrapTexU = GL_REPEAT;
	}
	else if(textureStringWrapTexU == "GL_MIRRORED_REPEAT")
	{
		textureWrapTexU = GL_MIRRORED_REPEAT;
	}
	else if(textureStringWrapTexU == "GL_MIRROR_CLAMP_TO_EDGE")
	{
		textureWrapTexU = GL_MIRROR_CLAMP_TO_EDGE;
	}

	if(textureStringWrapTexV == "GL_CLAMP_TO_EDGE")
	{
		textureWrapTexV = GL_CLAMP_TO_EDGE;
	}
	else if(textureStringWrapTexV == "GL_CLAMP_TO_BORDER")
	{
		textureWrapTexV = GL_CLAMP_TO_BORDER;
	}
	else if(textureStringWrapTexV == "GL_REPEAT")
	{
		textureWrapTexV = GL_REPEAT;
	}
	else if(textureStringWrapTexV == "GL_MIRRORED_REPEAT")
	{
		textureWrapTexV = GL_MIRRORED_REPEAT;
	}
	else if(textureStringWrapTexV == "GL_MIRROR_CLAMP_TO_EDGE")
	{
		textureWrapTexV = GL_MIRROR_CLAMP_TO_EDGE;
	}













}


void Visual::InitializePrimitiveParam()
{
	//polygonModeString = "GL_FILL";
	//cullFaceString = "GL_BACK";
	//
	//polygonMode = GL_FILL;
	//cullFace = GL_BACK;


	if(primitiveStringPolygonMode == "GL_FILL")
	{
		primitivePolygonMode = GL_FILL;
	}
	else if(primitiveStringPolygonMode == "GL_LINE")
	{
		primitivePolygonMode = GL_LINE;
	}
	else if(primitiveStringPolygonMode == "GL_POINT")
	{
		primitivePolygonMode = GL_POINT;
	}

	if(primitiveStringCullFace == "GL_BACK")
	{
		primitiveCullFace = GL_BACK;
	}
	else if(primitiveStringCullFace == "GL_FRONT")
	{
		primitiveCullFace = GL_FRONT;
	}
	else if(primitiveStringCullFace == "GL_NONE")
	{
		primitiveCullFace = GL_NONE;
	}
}

































































