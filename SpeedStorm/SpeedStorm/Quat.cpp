/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Quat.h"
#include <assert.h>

satQuat::satQuat() 
{
	v = satVec3(0.0f);
	w = 1.0f;
}

satQuat::satQuat(float x, float y, float z, float w) 
{
	this->x = x;
	this->y = y;
	this->z = z;
	this->w = w;
}

satQuat::satQuat(float x)
{
	this->x = x;
	this->y = x;
	this->z = x;
	this->w = x;
}

//satQuat::satQuat(float degreets, satVec3 v) 
//{
//	this->a = degreets;
//	this->b = x;
//	this->c = y;
//	this->d = z;
//}

satQuat::satQuat(satVec3 v)
{
	this->a = 0.0f;
	this->b = x;
	this->c = y;
	this->d = z;
}

satQuat::satQuat(float angleRad, satVec3 axis)
{
	float c = cos(angleRad / 2.0f);
	float a = sin(angleRad / 2.0f);

	w = c;
	v = axis * a;
}

satQuat satQuat::operator*(const satQuat &b) const
{
	satQuat ret;

	ret.w = w * b.w - satVec3::Dot(v, b.v);
	ret.v = b.v * w + v * b.w + satVec3::Cross(v, b.v);

	return ret;
}

float satQuat::magnitude(satQuat a)
{
	return sqrt(
		a.w * a.w +
		a.x * a.x +
		a.y * a.y +
		a.z * a.z);
}

satQuat satQuat::normalize(satQuat a)
{
	float mag = magnitude(a);

	a.w /= mag;
	a.x /= mag;
	a.y /= mag;
	a.z /= mag;

	return a;
}

satVec3 satQuat::rotateVector(satQuat q, const satVec3 b)
{
	return b + satVec3::Cross((q.v * 2.0f), satVec3::Cross(q.v, b) + (b * q.w));
}

float satQuat::operator[](int index) const 
{
	assert((index >= 0) && (index < 4));
	return (&x)[index];
}

float& satQuat::operator[](int index) 
{
	assert((index >= 0) && (index < 4));
	return (&x)[index];
}

satQuat satQuat::operator-() const
{
	return satQuat(-x, -y, -z, -w);
}

satQuat &satQuat::operator=(const satQuat &q0)
{
	x = q0.x;
	y = q0.y;
	z = q0.z;
	w = q0.w;

	return *this;
}

satQuat satQuat::operator+(const satQuat &q0) const
{
	return satQuat(x + q0.x, y + q0.y, z + q0.z, w + q0.w);
}

satQuat& satQuat::operator+=(const satQuat &q0)
{
	x += q0.x;
	y += q0.y;
	z += q0.z;
	w += q0.w;

	return *this;
}

satQuat satQuat::operator-(const satQuat &q0) const
{
	return satQuat(x - q0.x, y - q0.y, z - q0.z, w - q0.w);
}

satQuat& satQuat::operator-=(const satQuat &q0)
{
	this->x -= q0.x;
	this->y -= q0.y;
	this->z -= q0.z;
	this->w -= q0.w;

	return *this;
}

//satQuat satQuat::operator*(const satQuat &q0) const
//{
//	return satQuat(
//		w*q0.x + x*q0.w + y*q0.z - z*q0.y,
//		w*q0.y + y*q0.w + z*q0.x - x*q0.z,
//		w*q0.z + z*q0.w + x*q0.y - y*q0.x,
//		w*q0.w - x*q0.x - y*q0.y - z*q0.z);
//}

satVec3 satQuat::operator*(const satVec3 &q0) const
{
	float xxzz = x*x - z*z;
	float wwyy = w*w - y*y;

	float xw2 = x*w*2.0f;
	float xy2 = x*y*2.0f;
	float xz2 = x*z*2.0f;
	float yw2 = y*w*2.0f;
	float yz2 = y*z*2.0f;
	float zw2 = z*w*2.0f;

	return satVec3(
		(xxzz + wwyy)*q0.x + (xy2 + zw2)*q0.y + (xz2 - yw2)*q0.z,
		(xy2 - zw2)*q0.x + (y*y + w*w - x*x - z*z)*q0.y + (yz2 + xw2)*q0.z,
		(xz2 + yw2)*q0.x + (yz2 - xw2)*q0.y + (wwyy - xxzz)*q0.z	);
}

satQuat satQuat::operator*(float a) const
{
	return satQuat(x * a, y * a, z * a, w * a);
}

satQuat operator*(const float a, const satQuat &q0)
{
	return q0 * a;
}

satVec3 operator*(const satVec3 &v0, const satQuat &q0)
{
	return q0 * v0;
}

satQuat& satQuat::operator*=(const satQuat &q0)
{
	*this = *this * q0;

	return *this;
}

satQuat& satQuat::operator*=(float a)
{
	x *= a;
	y *= a;
	z *= a;
	w *= a;

	return *this;
}

satQuat	satQuat::operator/(float a)
{
	return satQuat(x / a, y / a, z / a, w / a);
}

satQuat& satQuat::operator/=(float a)
{
	x *= a;
	y *= a;
	z *= a;
	w *= a;

	return *this;
}

bool satQuat::Equals(const satQuat &q0) const
{
	return ((x == q0.x) && (y == q0.y) && (z == q0.z) && (w == q0.w));
}

bool satQuat::Equals(const satQuat &q0, const float epsilon) const
{
	if (fabs(x - q0.x) > epsilon)
	{
		return false;
	}
	if (fabs(y - q0.y) > epsilon)
	{
		return false;
	}
	if (fabs(z - q0.z) > epsilon)
	{
		return false;
	}
	if (fabs(w - q0.w) > epsilon)
	{
		return false;
	}
	return true;
}

bool satQuat::operator==(const satQuat &a) const
{
	return Equals(a);
}

bool satQuat::operator!=(const satQuat &a) const
{
	return !Equals(a);
}

satQuat satQuat::Inverse() const
{
	return satQuat(-x, -y, -z, w);
}

float satQuat::Length() const
{
	float len;

	len = x * x + y * y + z * z + w * w;
	return sqrt(len);
}

satQuat& satQuat::Normalize()
{
	float len;
	float ilength;

	len = this->Length();
	if (len)
	{
		ilength = 1 / len;
		qt.x *= ilength;
		qt.y *= ilength;
		qt.z *= ilength;
		qt.w *= ilength;
	}
	return *this;
}

float satQuat::Dot(const satQuat &q0) const
{
	return 
		qt.x * q0.x + 
		qt.y * q0.y + 
		qt.z * q0.z + 
		qt.w * q0.w;
}

void satQuat::SLerp(const satQuat &q0, const satQuat &q1, const float t)
{
	if (t <= 0.0f)
	{
		*this = q0;
		return;
	}
	else if (t >= 1.0f)
	{
		*this = q1;
		return;
	}

	//omega, cosOmega, sinOmega, scale0, scale1
	float cosOmega, scale0, scale1;
	cosOmega = q0.Dot(q1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = q0 * scale0 + q1 * scale1;
}

void satQuat::SLerpNoBounds(const satQuat &q0, const satQuat &q1, const float t)
{
	//omega, cosOmega, sinOmega, scale0, scale1

	float cosOmega, scale0, scale1;
	cosOmega = q0.Dot(q1);
	if (1.0f - cosOmega > 1e-6)
	{
		float omega, sinOmega, oneOverSinOmega;
		omega = acos(cosOmega);
		sinOmega = sin(omega);
		oneOverSinOmega = 1.0f / sinOmega;
		scale0 = sin((1.0f - t) * omega) * oneOverSinOmega;
		scale1 = sin(t * omega) * oneOverSinOmega;
	}
	else
	{
		scale0 = 1.0f - t;
		scale1 = t;
	}

	*this = q0 * scale0 + q1 * scale1;
}














satMat3 satQuat::ToMat3() const 
{
	satMat3	mat;
	float	wx, wy, wz;
	float	xx, yy, yz;
	float	xy, xz, zz;
	float	x2, y2, z2;

	x2 = x + x;
	y2 = y + y;
	z2 = z + z;

	xx = x * x2;
	xy = x * y2;
	xz = x * z2;

	yy = y * y2;
	yz = y * z2;
	zz = z * z2;

	wx = w * x2;
	wy = w * y2;
	wz = w * z2;

	mat[0][0] = 1.0f - (yy + zz);
	mat[0][1] = xy - wz;
	mat[0][2] = xz + wy;

	mat[1][0] = xy + wz;
	mat[1][1] = 1.0f - (xx + zz);
	mat[1][2] = yz - wx;

	mat[2][0] = xz - wy;
	mat[2][1] = yz + wx;
	mat[2][2] = 1.0f - (xx + yy);

	return mat;
}

satMat4 satQuat::ToMat4() const
{
	//Transpose it I think?

	satMat3 temp = ToMat3();
	return satMat4
		(
		temp.mat[0][0], temp.mat[1][0], temp.mat[2][0], 0.0f,
		temp.mat[0][1], temp.mat[1][1], temp.mat[2][1], 0.0f,
		temp.mat[0][2], temp.mat[1][2], temp.mat[2][2], 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);
}
