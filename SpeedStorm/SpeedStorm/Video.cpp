/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Video.h"
#include <iostream>
#include "SOIL\SOIL.h"
#include <SFML\Graphics.hpp>
#include "Game.h"
#include "Utilities.h"

//#ifndef CDS_FULLSCREEN											// CDS_FULLSCREEN Is Not Defined By Some
//#define CDS_FULLSCREEN 4										// Compilers. By Defining It This Way,
//#endif															// We Can Avoid Errors

//GL_Window*	g_window;

// User Defined Variables

bool dontgetangry;

float		angle;												// Used For Rotation
int			next;												// Used For Animation
int			frame=0;											// Frame Counter
int			effect;												// Current Effect
bool		sp;													// Space Bar Pressed?
bool		env=TRUE;											// Environment Mapping (Default On)
bool		ep;													// 'E' Pressed?
bool		bg=TRUE;											// Background (Default On)
bool		bp;													// 'B' Pressed?

GLuint VideoTexHandle = 0;


AVISTREAMINFO		psi;										// Pointer To A Structure Containing Stream Info
PAVISTREAM			pavi;										// Handle To An Open Stream
PGETFRAME			pgf;										// Pointer To A GetFrame Object
BITMAPINFOHEADER	bmih;										// Header Information For DrawDibDraw Decoding
long				lastframe;									// Last Frame Of The Stream
int					width;										// Video Width
int					height;										// Video Height
char				*pdata;										// Pointer To Texture Data
int					mpf;										// Will Hold Rough Milliseconds Per Frame

GLUquadricObj *quadratic;										// Storage For Our Quadratic Objects

HDRAWDIB hdd;													// Handle For Our Dib
HBITMAP hBitmap;												// Handle To A Device Dependant Bitmap
HDC hdc = CreateCompatibleDC(0);								// Creates A Compatible Device Context
unsigned char* data = 0;										// Pointer To Our Resized Image

void flipIt(void* buffer)										// Flips The Red And Blue Bytes (512x512)
{
	void* b = buffer;											// Pointer To The Buffer
	__asm														// Assembler Code To Follow
	{
		mov ecx, 512*512										// Counter Set To Dimensions Of Our Memory Block
		mov ebx, b												// Points ebx To Our Data (b)
		label:													// Label Used For Looping
			mov al,[ebx+0]										// Loads Value At ebx Into al
			mov ah,[ebx+2]										// Loads Value At ebx+2 Into ah
			mov [ebx+2],al										// Stores Value In al At ebx+2
			mov [ebx+0],ah										// Stores Value In ah At ebx
			
			add ebx,3											// Moves Through The Data By 3 Bytes
			dec ecx												// Decreases Our Loop Counter
			jnz label											// If Not Zero Jump Back To Label
	}
}

void OpenAVI(LPCWSTR szFile)										// Opens An AVI File (szFile)
{
	TCHAR	title[100];											// Will Hold The Modified Window Title

	AVIFileInit();												// Opens The AVIFile Library

	//Opens The AVI Stream
	if (AVIStreamOpenFromFile(&pavi, szFile, streamtypeVIDEO, 0, OF_READ, NULL) != 0)
	{
		//YOU DONE MESSED UP LOADING THE VIDEO
		MessageBox (HWND_DESKTOP, L"Failed To Open The AVI", L"ERROR", MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}

	AVIStreamInfo(pavi, &psi, sizeof(psi));						// Reads Information About The Stream Into psi
	width=psi.rcFrame.right-psi.rcFrame.left;					// Width Is Right Side Of Frame Minus Left
	height=psi.rcFrame.bottom-psi.rcFrame.top;					// Height Is Bottom Of Frame Minus Top

	lastframe=AVIStreamLength(pavi);							// The Last Frame Of The Stream

	mpf=AVIStreamSampleToTime(pavi,lastframe)/lastframe;		// Calculate Rough Milliseconds Per Frame

	bmih.biSize = sizeof (BITMAPINFOHEADER);					// Size Of The BitmapInfoHeader
	bmih.biPlanes = 1;											// Bitplanes	
	bmih.biBitCount = 24;										// Bits Format We Want (24 Bit, 3 Bytes)
	bmih.biWidth = 512;											// Width We Want (512 Pixels)
	bmih.biHeight = 512;										// Height We Want (512 Pixels)
	bmih.biCompression = BI_RGB;								// Requested Mode = RGB

	hBitmap = CreateDIBSection (hdc, (BITMAPINFO*)(&bmih), DIB_RGB_COLORS, (void**)(&data), NULL, NULL);
	SelectObject (hdc, hBitmap);								// Select hBitmap Into Our Device Context (hdc)

	pgf=AVIStreamGetFrameOpen(pavi, NULL);						// Create The PGETFRAME	Using Our Request Mode
	if (pgf==NULL)
	{
		// An Error Occurred Opening The Frame
		MessageBox (HWND_DESKTOP, L"Failed To Open The AVI Frame", L"Error", MB_OK | MB_ICONEXCLAMATION);
		exit(0);
	}

	// Information For The Title Bar (Width / Height / Last Frame)
	wsprintf (title, L"NeHe's AVI Player: Width: %d, Height: %d, Frames: %d", width, height, lastframe);
	//SetWindowText(g_window->hWnd, title);						// Modify The Title Bar
}

int ReturnAVILength()
{
	return lastframe;
}

void OpenAVI(const std::string& s)										// Opens An AVI File (szFile)
{
	OpenAVI(StringToLPCWSTR(s));
}

void GrabAVIFrame(int frame)									// Grabs A Frame From The Stream
{
	LPBITMAPINFOHEADER lpbi;									// Holds The Bitmap Header Information
	lpbi = (LPBITMAPINFOHEADER)AVIStreamGetFrame(pgf, frame);	// Grab Data From The AVI Stream
	pdata=(char *)lpbi+lpbi->biSize+lpbi->biClrUsed * sizeof(RGBQUAD);	// Pointer To Data Returned By AVIStreamGetFrame

	// Convert Data To Requested Bitmap Format
	DrawDibDraw (hdd, hdc, 0, 0, 512, 512, lpbi, pdata, 0, 0, width, height, 0);

	flipIt(data);												// Swap The Red And Blue Bytes (GL Compatability)

	// Update The Texture
	glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 512, 512, GL_BGR, GL_UNSIGNED_BYTE, data);
}

GLuint ReturnAVIFrame(int frame)									// Grabs A Frame From The Stream
{
	LPBITMAPINFOHEADER lpbi;									// Holds The Bitmap Header Information
	lpbi = (LPBITMAPINFOHEADER)AVIStreamGetFrame(pgf, frame);	// Grab Data From The AVI Stream
	pdata=(char *)lpbi+lpbi->biSize+lpbi->biClrUsed * sizeof(RGBQUAD);	// Pointer To Data Returned By AVIStreamGetFrame

	// Convert Data To Requested Bitmap Format
	DrawDibDraw (hdd, hdc, 0, 0, 512, 512, lpbi, pdata, 0, 0, width, height, 0);

	flipIt(data);												// Swap The Red And Blue Bytes (GL Compatability)
	

	////// Update The Texture
	////glTexSubImage2D (GL_TEXTURE_2D, 0, 0, 0, 512, 512, GL_RGB, GL_UNSIGNED_BYTE, data);
	//////
	if(VideoTexHandle == 0)
	{
		glActiveTexture(GL_TEXTURE0);
		glGenTextures(1, &VideoTexHandle);	
		glBindTexture(GL_TEXTURE_2D, VideoTexHandle);
		//glTexImage2D (GL_TEXTURE_2D, VideoTexHandle, 0, 0, 512, 512, GL_RGB, GL_UNSIGNED_BYTE, data);
		
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); //u
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); //v
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 8.0f);	//MMM BABY THAT ANISOTROPIC FILTERING
		//glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 1.0f);	//MMM BABY THAT ANISOTROPIC FILTERING
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		
		//if(Game::WinScreenActive)
		//{	
		//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, 512, 512, GL_RGB, GL_UNSIGNED_BYTE, data);
		//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//}
		//else
		//{
		//
		//	
		//}
		
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
	}
	else
	{
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, VideoTexHandle);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, 512, 512, GL_RGB, GL_UNSIGNED_BYTE, data);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		//if (Game::WinScreenActive)
		//{
		//	gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, 512, 512, GL_BGR, GL_UNSIGNED_BYTE, data);
		//	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		//}
		//else
		//{
		//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		//}


		//gluBuild2DMipmaps(GL_TEXTURE_2D, GL_RGB8, 512, 512, GL_BGR, GL_UNSIGNED_BYTE, data);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
	}

	return VideoTexHandle;
}

void VideoUnload(GLuint& VideoTexHandle)
{
	if (VideoTexHandle != 0)
	{
		//remove data from GPU
		glDeleteTextures(1, &VideoTexHandle);
		VideoTexHandle = 0;
	}
}

void CloseAVI(void)												// Properly Closes The Avi File
{
	DeleteObject(hBitmap);										// Delete The Device Dependant Bitmap Object
	DrawDibClose(hdd);											// Closes The DrawDib Device Context
	AVIStreamGetFrameClose(pgf);								// Deallocates The GetFrame Resources
	AVIStreamRelease(pavi);										// Release The Stream
	AVIFileExit();												// Release The File
}

BOOL Initialize (const std::string& s)					// Any GL Init Code & User Initialiazation Goes Here
{
	return Initialize(StringToLPCWSTR(s));
}

BOOL Initialize(LPCWSTR szFile)					// Any GL Init Code & User Initialiazation Goes Here
{
	//g_window	= window;

	// Start Of User Initialization
	angle		= 0.0f;											// Set Starting Angle To Zero
	
	hdd = DrawDibOpen();										// Grab A Device Context For Our Dib
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST);	// Set Texture Max Filter
	//glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST);	// Set Texture Min Filter
	
	OpenAVI(szFile);						// Open The AVI File

	// Create The Texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	return TRUE;												// Return TRUE (Initialization Successful)
}

BOOL Initialize()					// Any GL Init Code & User Initialiazation Goes Here
{
	//g_window	= window;

	// Start Of User Initialization
	angle		= 0.0f;											// Set Starting Angle To Zero
	hdd = DrawDibOpen();										// Grab A Device Context For Our Dib
	//glClearColor (0.0f, 0.0f, 0.0f, 0.5f);						// Black Background
	//glClearDepth (1.0f);										// Depth Buffer Setup
	//glDepthFunc (GL_LEQUAL);									// The Type Of Depth Testing (Less Or Equal)
	//glEnable(GL_DEPTH_TEST);									// Enable Depth Testing
	//glShadeModel (GL_SMOOTH);									// Select Smooth Shading
	//glHint (GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);			// Set Perspective Calculations To Most Accurate

	//quadratic=gluNewQuadric();									// Create A Pointer To The Quadric Object
	//gluQuadricNormals(quadratic, GLU_SMOOTH);					// Create Smooth Normals 
	//gluQuadricTexture(quadratic, GL_TRUE);						// Create Texture Coords 

	glEnable(GL_TEXTURE_2D);									// Enable Texture Mapping
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);	// Set Texture Max Filter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	// Set Texture Min Filter
	//
	//glTexGeni(GL_S, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);		// Set The Texture Generation Mode For S To Sphere Mapping
	//glTexGeni(GL_T, GL_TEXTURE_GEN_MODE, GL_SPHERE_MAP);		// Set The Texture Generation Mode For T To Sphere Mapping

	OpenAVI(L"../assets/video/face2.avi");						// Open The AVI File

	// Create The Texture
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 512, 512, 0, GL_RGB, GL_UNSIGNED_BYTE, data);

	return TRUE;												// Return TRUE (Initialization Successful)
}

void Deinitialize (void)										// Any User DeInitialization Goes Here
{
	CloseAVI();													// Close The AVI File
}

void Update (DWORD milliseconds)								// Perform Motion Updates Here
{
	//if (g_keys->keyDown [VK_ESCAPE] == TRUE)					// Is ESC Being Pressed?
	//{
	//	TerminateApplication (g_window);						// Terminate The Program
	//}
	//
	//if (g_keys->keyDown [VK_F1] == TRUE)						// Is F1 Being Pressed?
	//{
	//	ToggleFullscreen (g_window);							// Toggle Fullscreen Mode
	//}
	//
	//if ((g_keys->keyDown [' ']) && !sp)							// Is Space Being Pressed And Not Held?
	//{
	//	sp=TRUE;												// Set sp To True
	//	effect++;												// Change Effects (Increase effect)
	//	if (effect>3)											// Over Our Limit?
	//		effect=0;											// Reset Back To 0
	//}
	//
	//if (!g_keys->keyDown[' '])									// Is Space Released?
	//	sp=FALSE;												// Set sp To False
	//
	//if ((g_keys->keyDown ['B']) && !bp)							// Is 'B' Being Pressed And Not Held?
	//{
	//	bp=TRUE;												// Set bp To True
	//	bg=!bg;													// Toggle Background Off/On
	//}
	//
	//if (!g_keys->keyDown['B'])									// Is 'B' Released?
	//	bp=FALSE;												// Set bp To False
	//
	//if ((g_keys->keyDown ['E']) && !ep)							// Is 'E' Being Pressed And Not Held?
	//{
	//	ep=TRUE;												// Set ep To True
	//	env=!env;												// Toggle Environment Mapping Off/On
	//}
	//
	//if (!g_keys->keyDown['E'])									// Is 'E' Released?
	//	ep=FALSE;												// Set ep To False
	//
	//angle += (float)(milliseconds) / 60.0f;						// Update angle Based On The Timer
	//
	//next+=milliseconds;											// Increase next Based On The Timer
	//frame=next/mpf;												// Calculate The Current Frame
	//
	//if (frame>=lastframe)										// Are We At Or Past The Last Frame?
	//{
	//	frame=0;												// Reset The Frame Back To Zero (Start Of Video)
	//	next=0;													// Reset The Animation Timer (next)
	//}
}

void Draw (void)												// Draw Our Scene
{
	glClear (GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);		// Clear Screen And Depth Buffer

	GrabAVIFrame(frame);										// Grab A Frame From The AVI

	//if (bg)														// Is Background Visible?
	//{
	//	glLoadIdentity();										// Reset The Modelview Matrix
	//	glBegin(GL_QUADS);										// Begin Drawing The Background (One Quad)
	//		// Front Face
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f( 11.0f,  8.3f, -20.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f(-11.0f,  8.3f, -20.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f(-11.0f, -8.3f, -20.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f( 11.0f, -8.3f, -20.0f);
	//	glEnd();												// Done Drawing The Background
	//}
	//
	//glLoadIdentity ();											// Reset The Modelview Matrix
	//glTranslatef (0.0f, 0.0f, -10.0f);							// Translate 10 Units Into The Screen
	//
	//if (env)													// Is Environment Mapping On?
	//{
	//	glEnable(GL_TEXTURE_GEN_S);								// Enable Texture Coord Generation For S (NEW)
	//	glEnable(GL_TEXTURE_GEN_T);								// Enable Texture Coord Generation For T (NEW)
	//}
	//
	//glRotatef(angle*2.3f,1.0f,0.0f,0.0f);						// Throw In Some Rotations To Move Things Around A Bit
	//glRotatef(angle*1.8f,0.0f,1.0f,0.0f);						// Throw In Some Rotations To Move Things Around A Bit
	//glTranslatef(0.0f,0.0f,2.0f);								// After Rotating Translate To New Position
	//
	//switch (effect)												// Which Effect?
	//{
	//case 0:														// Effect 0 - Cube
	//	glRotatef (angle*1.3f, 1.0f, 0.0f, 0.0f);				// Rotate On The X-Axis By angle
	//	glRotatef (angle*1.1f, 0.0f, 1.0f, 0.0f);				// Rotate On The Y-Axis By angle
	//	glRotatef (angle*1.2f, 0.0f, 0.0f, 1.0f);				// Rotate On The Z-Axis By angle
	//	glBegin(GL_QUADS);										// Begin Drawing A Cube
	//		// Front Face
	//		glNormal3f( 0.0f, 0.0f, 0.5f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
	//		// Back Face
	//		glNormal3f( 0.0f, 0.0f,-0.5f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
	//		// Top Face
	//		glNormal3f( 0.0f, 0.5f, 0.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
	//		// Bottom Face
	//		glNormal3f( 0.0f,-0.5f, 0.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
	//		// Right Face
	//		glNormal3f( 0.5f, 0.0f, 0.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f( 1.0f, -1.0f, -1.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f( 1.0f,  1.0f, -1.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f( 1.0f,  1.0f,  1.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f( 1.0f, -1.0f,  1.0f);
	//		// Left Face
	//		glNormal3f(-0.5f, 0.0f, 0.0f);
	//		glTexCoord2f(0.0f, 0.0f); glVertex3f(-1.0f, -1.0f, -1.0f);
	//		glTexCoord2f(1.0f, 0.0f); glVertex3f(-1.0f, -1.0f,  1.0f);
	//		glTexCoord2f(1.0f, 1.0f); glVertex3f(-1.0f,  1.0f,  1.0f);
	//		glTexCoord2f(0.0f, 1.0f); glVertex3f(-1.0f,  1.0f, -1.0f);
	//	glEnd();												// Done Drawing Our Cube
	//	break;													// Done Effect 0
	//
	//case 1:														// Effect 1 - Sphere
	//	glRotatef (angle*1.3f, 1.0f, 0.0f, 0.0f);				// Rotate On The X-Axis By angle
	//	glRotatef (angle*1.1f, 0.0f, 1.0f, 0.0f);				// Rotate On The Y-Axis By angle
	//	glRotatef (angle*1.2f, 0.0f, 0.0f, 1.0f);				// Rotate On The Z-Axis By angle
	//	gluSphere(quadratic,1.3f,20,20);						// Draw A Sphere
	//	break;													// Done Drawing Sphere
	//
	//case 2:														// Effect 2 - Cylinder
	//	glRotatef (angle*1.3f, 1.0f, 0.0f, 0.0f);				// Rotate On The X-Axis By angle
	//	glRotatef (angle*1.1f, 0.0f, 1.0f, 0.0f);				// Rotate On The Y-Axis By angle
	//	glRotatef (angle*1.2f, 0.0f, 0.0f, 1.0f);				// Rotate On The Z-Axis By angle
	//	glTranslatef(0.0f,0.0f,-1.5f);							// Center The Cylinder
	//	gluCylinder(quadratic,1.0f,1.0f,3.0f,32,32);			// Draw A Cylinder
	//	break;													// Done Drawing Cylinder
	//}
	//
	//if (env)													// Environment Mapping Enabled?
	//{
	//	glDisable(GL_TEXTURE_GEN_S);							// Disable Texture Coord Generation For S (NEW)
	//	glDisable(GL_TEXTURE_GEN_T);							// Disable Texture Coord Generation For T (NEW)
	//}
	//
	//glFlush ();													// Flush The GL Rendering Pipeline
}


















