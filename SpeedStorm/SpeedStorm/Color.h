/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include "Vector.h"

class satVec3;

class satColor
{
public:

	union
	{
		struct
		{
			float r, g, b;
		};
		struct
		{
			float x, y, z;
		};
		float rows[3];
	};
	
	//static satColor ColorChar(const unsigned char x, const unsigned char y, const unsigned char z);

	satColor::satColor();
	satColor::satColor(const float rgb);
	satColor::satColor(const float r, const float g, const float b);
	satColor::satColor(const unsigned char r, const unsigned char g, const unsigned char b);

	satColor & satColor::operator= (const satVec3 &a);
	satColor & satColor::operator= (const float &a);
	satColor & satColor::operator= (const int &a);
	satColor satColor::operator+(const satColor &a) const;
	satColor satColor::operator-() const;
	satColor satColor::operator-(const satColor &a) const;
	satColor satColor::operator*(const float &a) const;
	satColor satColor::operator*(const satColor &a) const;
	satColor	satColor::operator/(const satColor &a) const;
	satColor	satColor::operator/(const float &a) const;
	satColor &	satColor::operator+=(const satColor &a);
	satColor &	satColor::operator-=(const satColor &a);
	satColor &	satColor::operator*=(const satColor &a);
	satColor &	satColor::operator/=(const satColor &a);
	satColor &	satColor::operator*=(const float &a);
	satColor &	satColor::operator/=(const float &a);

	//friend satVec3	satColor::operator=(satVec3 &a, const satColor &b);

	inline satColor satColor::Adjust(satColor &a) //UC = Unsigned Char
	{
		//	0.2989	Red
		//	0.5874	Green
		//	0.1137	Blue
		//0.2989 * a.r + 0.5874 * a.g + 0.1137 * a.b 
		//When these are added together they will equal the average luminance
		*this = satColor(a.r*0.8967f, a.g*1.7622f, a.b*0.3411f);
		return *this;
	}

	static inline satColor satColor::adjust(satColor &a) //UC = Unsigned Char
	{
		//	0.2989	Red
		//	0.5874	Green
		//	0.1137	Blue
		//0.2989 * a.r + 0.5874 * a.g + 0.1137 * a.b 
		//When these are added together they will equal the average luminance
		return satColor(a.r/0.8967f, a.g/1.7622f, a.b/0.3411f);
	}	

	//You can set colors with values from 0-255, and this will clamp them
	static inline satColor satColor::UC(const unsigned char r, const unsigned char g, const unsigned char b) //UC = Unsigned Char
	{
		return satColor(r*0.003921568f, g*0.003921568f,	b*0.003921568f);	//0.0039215686274509803921568627451 is 1/255
	}
};

//inline satVec3 satColor::operator=(satVec3 &a, const satColor &b)
//{
//	a.x = b.x;
//	a.y = b.y;
//	a.z = b.z;
//
//	return a;
//}

//inline satVec3&	satVec3::operator=(const satVec3 &a)
//{
//	
//}
	
inline satColor::satColor()
{
	this->x = 0.0f;
	this->y = 0.0f;
	this->z = 0.0f;
}

inline satColor::satColor(const float rgb)
{
	this->x = rgb;
	this->y = rgb;
	this->z = rgb;
}

inline satColor::satColor(const float r, const float g, const float b)
{
	this->r = r;
	this->g = g;
	this->b = b;
}

inline satColor::satColor(const unsigned char r, const unsigned char g, const unsigned char b)
{
	this->r = r*0.003921568f; //0.0039215686274509803921568627451 is 1/255
	this->g = g*0.003921568f;
	this->b = b*0.003921568f;
}

inline satColor & satColor::operator= (const satVec3 &a)
{
	x = a.x;
	y = a.y;
	z = a.z;

	return *this;
}

//inline satColor & satColor::operator= (float &a)
//{
//	x = a;
//	y = a;
//	z = a;
//
//	return *this;
//}
//
//inline satColor & satColor::operator= (int &a)
//{
//	x = (float)a;
//	y = (float)a;
//	z = (float)a;
//
//	return *this;
//}

inline satColor satColor::operator-() const
{
	return satColor(-r, -g, -b);
}

inline satColor satColor::operator+(const satColor &a) const
{
	return satColor(r + a.r, g + a.g, b + a.b);
}

inline satColor satColor::operator-(const satColor &a) const
{
	return satColor(r - a.r, g - a.g, b - a.b);
}

inline satColor satColor::operator*(const float &a) const
{
	return satColor(r * a, g * a, b * a);
}

inline satColor satColor::operator*(const satColor &a) const
{
	return satColor(r * a.r, g * a.g, b * a.b);
}

inline satColor	satColor::operator/(const satColor &a) const
{
	return satColor(r / a.r, g / a.g, b / a.b);
}

inline satColor	satColor::operator/(const float &a) const
{
	float t = 1.0f / a;
	return satColor(r * t, g / t, b / t);
}

inline satColor &	satColor::operator+=(const satColor &a)
{
	this->r += a.r;
	this->g += a.g;
	this->b += a.b;

	return *this;
}

inline satColor &	satColor::operator-=(const satColor &a)
{
	this->r -= a.r;
	this->g -= a.g;
	this->b -= a.b;

	return *this;
}

inline satColor &	satColor::operator*=(const satColor &a)
{
	this->r *= a.r;
	this->g *= a.g;
	this->b *= a.b;

	return *this;
}

inline satColor &	satColor::operator/=(const satColor &a)
{
	this->r /= a.r;
	this->g /= a.g;
	this->b /= a.b;

	return *this;
}

inline satColor &	satColor::operator*=(const float &a)
{
	this->r *= a;
	this->g *= a;
	this->b *= a;

	return *this;
}

inline satColor &	satColor::operator/=(const float &a)
{
	float t = 1.0f / a;
	this->r *= t;
	this->g *= t;
	this->b *= t;

	return *this;
}


inline satColor operator*(const float a, const satColor b)
{
	return satColor(b.x * a, b.y * a, b.z * a);
}

namespace sat
{
	namespace color
	{
		const satColor black =	{ 0.0f, 0.0f, 0.0f };
		const satColor white =	{ 1.0f, 1.0f, 1.0f };

		const satColor grey = { 0.5f, 0.5f, 0.5f };

		const satColor greyshade[7] = 
		{ 
			{ 0.125f, 0.125f, 0.125f },
			{ 0.25f, 0.25f, 0.25f },
			{ 0.375f, 0.375f, 0.375f },
			{ 0.5f, 0.5f, 0.5f },
			{ 0.625f, 0.625f, 0.625f },
			{ 0.75f, 0.75f, 0.75f },
			{ 0.875f, 0.875f, 0.875f } 
		};

		const satColor red =	{ 1.0f, 0.0f, 0.0f };
		const satColor green =	{ 0.0f, 1.0f, 0.0f };
		const satColor blue =	{ 0.0f, 0.0f, 1.0f };

		const satColor yellow = { 1.0f, 1.0f, 0.0f };
		const satColor cyan =	{ 0.0f, 1.0f, 1.0f };
		const satColor magenta = { 1.0f, 0.0f, 1.0f };
		const satColor fuchsia = magenta;

		const satColor orange = { 1.0f, 0.5f, 0.0f };
		const satColor brown = { 0.4f, 0.2f, 0.05f };

		const satColor lime = green;

		const satColor darkred =		{ 0.65f, 0.0f, 0.0f };
		const satColor darkgreen =		{ 0.0f, 0.65f, 0.0f };
		const satColor darkblue =		{ 0.0f, 0.0f, 0.65f };
		const satColor darkyellow =		{ 0.65f, 0.65f, 0.0f };
		const satColor darkcyan =		{ 0.0f, 0.5f, 0.5f };
		const satColor darkmagenta =	{ 0.5f, 0.0f, 0.5f };

		const satColor slightlightred = { 1.0f, 0.25f, 0.25f };
		const satColor lightred =		{ 1.0f, 0.5f, 0.5f };
		const satColor lightgreen =		{ 0.5f, 1.0f, 0.5f };
		const satColor lightblue =		{ 0.5f, 0.5f, 1.0f };
		const satColor lightyellow =	{ 1.0f, 1.0f, 0.5f };
		const satColor lightcyan =		{ 0.5f, 1.0f, 1.0f };
		const satColor lightmagenta =	{ 1.0f, 0.5f, 1.0f };
		const satColor lightorange =	{ 1.0f, 0.75f, 0.5f };

		const satColor teal = darkcyan;

		namespace block
		{
			const satColor black = { 0.05f, 0.05f, 0.05f };
			const satColor white = { 0.95f, 0.95f, 0.95f };

			const satColor grey = { 0.5f, 0.5f, 0.5f };

			const satColor greyshade[7] =
			{
				{ 0.125f, 0.125f, 0.125f },
				{ 0.25f, 0.25f, 0.25f },
				{ 0.375f, 0.375f, 0.375f },
				{ 0.5f, 0.5f, 0.5f },
				{ 0.625f, 0.625f, 0.625f },
				{ 0.75f, 0.75f, 0.75f },
				{ 0.875f, 0.875f, 0.875f }
			};

			const satColor red =	{ 0.8f, 0.2f, 0.2f };
			const satColor green =	{ 0.2f, 0.8f, 0.2f };
			const satColor blue =	{ 0.2f, 0.2f, 0.8f };

			const satColor yellow = { 0.8f, 0.8f, 0.3f };
			const satColor cyan =	{ 0.2f, 0.8f, 0.8f };
			const satColor magenta ={ 0.8f, 0.2f, 0.8f };

			const satColor orange =		{ 0.8f, 0.45f, 0.2f };
			const satColor brown =		{ 0.4f, 0.2f, 0.05f };

			const satColor darkred =		{ 0.50f, 0.15f, 0.15f };
			const satColor darkgreen =		{ 0.15f, 0.50f, 0.15f };
			const satColor darkblue =		{ 0.15f, 0.15f, 0.50f };
			const satColor darkyellow =		{ 0.50f, 0.50f, 0.15f };
			const satColor darkcyan =		{ 0.15f, 0.50f, 0.50f };
			const satColor darkmagenta =	{ 0.50f, 0.15f, 0.50f };

			const satColor teal = darkcyan;

			const satColor lightred =		{ 1.0f, 0.5f, 0.5f };
			const satColor lightgreen =		{ 0.5f, 1.0f, 0.5f };
			const satColor lightblue =		{ 0.5f, 0.5f, 1.0f };
			const satColor lightyellow =	{ 1.0f, 1.0f, 0.5f };
			const satColor lightcyan =		{ 0.5f, 1.0f, 1.0f };
			const satColor lightmagenta =	{ 1.0f, 0.5f, 1.0f };
			const satColor lightorange =	{ 1.0f, 0.75f, 0.5f };



			//const satColor white =			satColor::UC( 242, 242, 242 );
			//const satColor lightgrey =		satColor::UC( 160, 160, 160 );
			//const satColor lightyellow =	satColor::UC( 249, 233, 153 );
			//const satColor lightorange =	satColor::UC(203, 132, 66);
			//const satColor brightred =		satColor::UC(196, 40, 27);
			//const satColor violet =			satColor::UC(196, 112, 160);
			//const satColor brightblue =		satColor::UC(13, 105, 171);
			//const satColor brightyellow =	satColor::UC(245, 205, 47);
			//const satColor black =			satColor::UC(27, 42, 52);
			//const satColor darkgrey =		satColor::UC(109, 110, 108);
			//const satColor darkgreen =		satColor::UC(40, 127, 70);

			//const satColor  = satColor::UC(, , );
			//const satColor = satColor::UC(, , );
			//const satColor red = satColor::UC(196, 40, 27);
		}
	}
}