/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Vector.h"
#include "Matrix.h"
#include <vector>
#include <iostream>
#include <cmath>

#ifdef INFINITY
#undef INFINITY
#endif

#ifdef FLT_EPSILON
#undef FLT_EPSILON
#endif

//
//================================================================================================
//
//degrees to radians and vice-versa
//
//================================================================================================
//

#define DTR(i) ( (i) * 0.01745329f)
#define RTD(i) ( (i) * 57.2957795f)

typedef float Degrees;
typedef float Radians;



class satMath
{
public:
	static const float			PI;							// pi
	static const float			TWO_PI;						// pi * 2
	static const float			HALF_PI;					// pi / 2
	static const float			ONEFOURTH_PI;				// pi / 4
	static const float			ONEOVER_PI;					// 1 / pi
	static const float			ONEOVER_TWOPI;				// 1 / pi * 2
	static const float			E;							// e
	static const float			SQRT_TWO;					// sqrt( 2 )
	static const float			SQRT_THREE;					// sqrt( 3 )
	static const float			SQRT_1OVER2;				// sqrt( 1 / 2 )
	static const float			SQRT_1OVER3;				// sqrt( 1 / 3 )
	static const float			M_DEG2RAD;					// degrees to radians multiplier
	static const float			M_RAD2DEG;					// radians to degrees multiplier
	static const float			M_SEC2MS;					// seconds to milliseconds multiplier
	static const float			M_MS2SEC;					// milliseconds to seconds multiplier
	static const float			INFINITY;					// huge number which should be larger than any valid number used
	static const float			FLT_EPSILON;				// smallest positive number such that 1.0+FLT_EPSILON != 1.0		

	//float Lerp(float v0, float v1, float t);
	float Bezier(int a[], float t);
	float Bezier(std::vector<int> b, float t);
	float Catmull(float data0, float data1, float data2, float data3, float u);


	

	//T Bezier(array[], int degreets, float t)
	//if(degreets = 0)
	//{
	//		STOP IT, RETURN THAT SHEET
	//}
	//Lerp(Bezier(array[0], degreets-1, t), Bezier(array[1], degreets-1, t),t);
};



//inline float Lerp(float v0, float v1, float t)
//{
//	return (1.0f - t) * v0 + (t) * v1;
//}

template <typename T>
//Clamped Lerp
inline T Lerp(T v0, T v1, float t)
{
	if (t <= 0.0f)
	{
		return v0;
	}
	else if (t >= 1.0f)
	{
		return v1;
	}
	else
	{
		return (1.0f - t) * v0 + (t)* v1;
	}
}

template <typename T>
//Unclamped Lerp
inline T LerpU(T v0, T v1, float t) 
{	
	return (1.0f - t) * v0 + (t)* v1;
}

template <typename T>
inline float InverseLerp(T v, T v0, T v1) 
{	
	float t = (float)((v - v0) / (v1 - v0));
	if(t < 0.0f)
		return 0.0f;
	else if(t > 1.0f)
		return 1.0f;
	return t;	
}

template <typename T>
inline float InverseLerpU(T v, T v0, T v1) 
{	
	return (float)((v - v0) / (v1 - v0));
}

// (distance - DistanceTable[i - 1].Distance) /
//(DistanceTable[i].Distance - DistanceTable[i - 1].Distance);


template <typename T>
inline T catmull(T data0, T data1, T data2, T data3, float t)
//inline float Catmull(float data0, float data1, float data2, float data3, float t)
{
	//std::cout << t*t*t*(-data0 + 3.0f*data1 - 3.0f*data2 + data3) << std::endl;
	//std::cout << t*t*(2.0f*data0 - 5.0f*data1 + 4.0f*data2 - data3) << std::endl;
	//std::cout << t* (-data0 + data2) << std::endl;
	//std::cout << 2.0f*data1 << std::endl;
	return 0.5f * 
		(t*t*t*(-data0 + 3.0f*data1 - 3.0f*data2 + data3) + 
		t*t*(2.0f*data0 - 5.0f*data1 + 4.0f*data2 - data3) + 
		t* (-data0 + data2) + 
		(2.0f*data1));
}

template <typename T>
T CatMull(T data0, T data1, T data2, T data3, float u)
{
	return 0.5f * (
		u * u * u * (-data0 + 3.0f * data1 - 3.0f * data2 + data3) +
		u * u * (2.0f * data0 - 5.0f * data1 + 4.0f * data2 - data3) +
		u * (-data0 + data2) +
		(2.0f * data1)
		);
}

template <typename T>
T Catmull(T data0, T data1, T data2, T data3, float u)
{
	return 0.5f * (
		u * u * u * (-data0 + 3.0f * data1 - 3.0f * data2 + data3) +
		u * u * (2.0f * data0 - 5.0f * data1 + 4.0f * data2 - data3) +
		u * (-data0 + data2) +
		(2.0f * data1)
		);
}

inline float Bezier(float data0, float data1, float data2, float data3, float t)
{
	return (t*t*t*(-data0 + 3.0f*data1 - 3.0f*data2 + data3) + 
		t*t*(3.0f*data0 - 6.0f*data1 + 3.0f*data2) + 
		t* (-3 * data0 + 3 * data1) + 
		(data0));
}

template <typename T>
inline T LerpNoBounds(const T &v0, const T &v1, const float t)
{
	return (1.0f - t) * v0 + (t)* v1;
}

template <typename T>
inline T BiLerp(const T &v0, const T &v1, const T &v2, const T &v3, const float t0, const float t1)
{
	return LerpR(LerpR(v0, v1, t0), LerpR(v2, v3, t0), t1);
}

template <typename T>
inline T BiLerpNoBounds(const T &v0, const T &v1, const T &v2, const T &v3, const float t0, const float t1)
{
	return LerpNoBoundsR(LerpNoBoundsR(v0, v1, t0), LerpNoBoundsR(v2, v3, t0), t1);
}

template <typename T>
inline T BezierLerpRecur(T data[], int begin, int end, float t)
{
	if (end - begin <= 1)
	{
		return Lerp(data[begin], data[end], t);
	}

	return Lerp(BezierLerpRecur(data, begin, end - 1, t), BezierLerpRecur(data, begin + 1, end, t), t);
}

template <typename T>
inline T BezierLerp(T data[], int end, float t)
{
	return BezierLerpRecur(data, 0, end, t);
}
//*/

template <typename T>
inline T Max(T a, T b)
{
	if (a > b)
		return a;
	else
		return b;
}

template <typename T>
inline T Min(T a, T b)
{
	if (a < b)
		return a;
	else
		return b;
}

template<typename T>
inline T MaxAbs(T a, T b)
{
	if (a*a > b*b)
		return a;
	else
		return b;
}

template<typename T>
inline T MinAbs(T a, T b)
{
	if (a*a > b*b)
		return a;
	else
		return b;
}

template<typename T>
inline T Between(T a, T low, T high)
{
	if (a > high)
		return high;
	else if(a < low)
		return low;
	return a;
}







