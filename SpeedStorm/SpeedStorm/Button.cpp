/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Button.h"
#include <iostream>

Button::Button()
{
	canClick = true;
	pos.Zero(); //camera pos -1 to 1
	menuPos.Zero(); //position in the world
	screenPos.Zero(); //position on the screen
	size.Zero();	//width and height
	menuSize.Zero();	//size of the menu
	screenSize.Zero();	//size of the window
	name;
}

bool Button::bClick(satVec2 mousePos)
{
	if (canClick && 
		mousePos.x >= screenPos.x - size.x / 2 && mousePos.x <= screenPos.x + size.x / 2 && 
		mousePos.y >= screenPos.y - size.y / 2 && mousePos.y <= screenPos.y + size.y / 2)
	{
		return true;
	}
	return false;
}

bool Button::bHover(satVec2 mousePos)
{
	if(mousePos.x >= screenPos.x - size.x / 2 && mousePos.x <= screenPos.x + size.x / 2 &&
		mousePos.y >= screenPos.y - size.y / 2 && mousePos.y <= screenPos.y + size.y / 2)
	{
		return true;
	}
	return false;
}

void Button::updateWindowSize(satVec2 ScreenSize)
{
	screenSize = satVec2(ScreenSize.y, ScreenSize.y);
	menuPos.setXY(pos*menuSize / 2);
	menuPos.x *= -1;
	screenPos = (pos / 2 + 0.5f) * screenSize;
	screenPos.x += (screenSize.x - screenSize.x) / 2;
}
