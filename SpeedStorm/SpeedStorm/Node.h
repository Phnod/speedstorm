/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <vector>
#include <memory>
#include "Vector.h"
#include "Matrix.h"


class Node;
typedef std::shared_ptr<Node> NodePtr;

class Node : public std::enable_shared_from_this<Node>
{
public:
	Node();
	virtual ~Node();
	std::vector<Node*>children; //you can have multiple children
	Node* parent;	//you can only be parented to one node

	satMat4 world; //local and world tranformations
	satMat4 local;	//these don't need to be public but I'm doing it anyways

	void addChild(Node* n);
	void removeChild(Node* n);
	void removeChild(unsigned int index);
	void clearChildren();
	Node* getParent() const;
	unsigned int getNumChildren() const;

	int findChild(Node* n);

	virtual void Process();
	void computeWorld(satMat4 parentTransform);

	satMat4 getWorld() const;
	void setWorld(satMat4 parentTransform);


	virtual void RotateX(float degreets);
	virtual void RotateY(float degreets);
	virtual void RotateZ(float degreets);
	void Translate(satVec3 position);
	void Translate(float x, float y, float z);
	void Scale(satVec3 position);
	void Scale(float x, float y, float z);
};