/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Math.h"

const float	satMath::PI =				3.14159265358979323846f;
const float	satMath::TWO_PI =			2.0f * PI;
const float	satMath::HALF_PI =			0.5f * PI;
const float	satMath::ONEFOURTH_PI =		0.25f * PI;
const float satMath::ONEOVER_PI =		1.0f / satMath::PI;
const float satMath::ONEOVER_TWOPI =	1.0f / satMath::TWO_PI;
const float satMath::E =				2.71828182845904523536f;
const float satMath::SQRT_TWO =			1.41421356237309504880f;
const float satMath::SQRT_THREE =		1.73205080756887729352f;
const float	satMath::SQRT_1OVER2 =		0.70710678118654752440f;
const float	satMath::SQRT_1OVER3 =		0.57735026918962576450f;
const float	satMath::M_DEG2RAD =		PI / 180.0f;
const float	satMath::M_RAD2DEG =		180.0f / PI;
const float	satMath::M_SEC2MS =			1000.0f;
const float	satMath::M_MS2SEC =			0.001f;
const float	satMath::INFINITY =			1e30f;
const float satMath::FLT_EPSILON =		1.192092896e-07f;	// 1.1754944e-038f

