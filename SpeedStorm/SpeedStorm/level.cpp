/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.

#include "level.h"
#include "Game.h"
#include "ResourceManager.h"
#include <iostream>

#include <stdio.h>
#include <sstream>
#include <fstream>



#define elf else if
//#define if if
#define CHAR_BUFFER_SIZE 128 //Size of the character buffer.

class Level;

const std::vector<std::string> LevelManager::levelFilenames = MakeLevelList();
std::vector<Level> LevelManager::level = MakeLevels();
//const std::vector<std::string> LevelManager::levelFilenames = {"../assets/Levels/Tutorial", "../assets/Levels/Tutorial"};
int LevelManager::levelCurrent = 0;
int LevelManager::levelFinalNum = 0;

int LevelManager::currentSongPlaying = 0;
bool LevelManager::tutorialActive = true;
//unsigned int LevelManager::levelFinalNum = 3;

LevelManager::LevelManager()
{

}

void LevelManager::LoadLevelAssets()
{
	for(unsigned i = 0; i < level.size(); ++i)
	{
		//std::cout << levelFilenames[i] << std::endl;
		level[i].Load();
		
		if(level[i].Song.loaded)
		{
			//std::cout << "Song already loaded on stage " << i << "\t" << level[i].SongFilename << std::endl;
		}

		if(level[i].SongRepeat)
		{
			//level[i].SongPtr = level[i - 1].SongPtr;
			//std::cout << "Repeat song on stage " << i << "\t" << level[i].SongFilename << std::endl;
		}

		if (!level[i].SongRepeat && !level[i].Song.loaded)
		{
			level[i].Song.loadFromFile(level[i].SongFilename, false, true);
			//level[i].Song.initDSPHighpass();
			//level[i].Song.initDSPLowpass();
			//level[i].Song.initDSPFlange();
		}
	}
}

std::vector<std::string> LevelManager::MakeLevelList()
{
	std::vector<std::string> tempList;
	
	
	// Open file
	std::ifstream infile("../assets/Settings/LevelList.txt", std::ios::binary);
	assert(infile && "'hiScores.txt' NOT FOUND");

	//		../assets/Levels/

	if (infile)
	{
		//Will eventually hold desired score
		std::string currLine;
		bool activeTutorial = true;

		//For the string-int conversion
		std::stringstream stream;

		//How much of the document to skip (to get to what we want)
		int skipAmt;
		skipAmt = 9;

		std::cout << "Reading levels from file ../assets/Settings/LevelList.txt" << std::endl;

		//skipping the title in the file
		//std::getline(infile, currLine, '\n');
		

		std::getline(infile, currLine, '\n');

		currLine.erase(std::remove(currLine.begin(), currLine.end(), '\n'), currLine.end());
		currLine.erase(std::remove(currLine.begin(), currLine.end(), '\r'), currLine.end());

		stream << currLine;
		stream >> activeTutorial;
		stream.str(""); stream.clear();

		tutorialActive = activeTutorial;

		//Skipping the other levels' scores
		for (int i = 0; i < skipAmt; ++i)
		{
			std::getline(infile, currLine, '\n');

			std::cout << "Skipping Line: " << currLine << std::endl;
		}

		
		//Reading the desired level's 10 scores
		while (!infile.eof())
		{
			std::getline(infile, currLine, '\n');
			
			currLine.erase(std::remove(currLine.begin(), currLine.end(), '\n'), currLine.end());
			currLine.erase(std::remove(currLine.begin(), currLine.end(), '\r'), currLine.end());
			//Converting the string score into an int score
			//stream << currLine;
			//stream >> score;
			//stream.str(""); stream.clear();
			

			if (!infile.eof())
			{
				std::cout << currLine << std::endl;
				tempList.push_back("../assets/Levels/" + currLine);
			}
		}

		infile.close();
	}
	else
	{
		std::cout << "Could not read file, setting default levels" << std::endl;
		tempList.push_back("../assets/Levels/Tutorial1");
		tempList.push_back("../assets/Levels/Tutorial2");
		tempList.push_back("../assets/Levels/Freezer1");
		tempList.push_back("../assets/Levels/Freezer2");
		tempList.push_back("../assets/Levels/Muzik");
		tempList.push_back("../assets/Levels/Castle");
		tempList.push_back("../assets/Levels/Disco");
		tempList.push_back("../assets/Levels/Handland");
		tempList.push_back("../assets/Levels/Pyramid");
		//tempList.push_back("../assets/Levels/Tunnel");
		tempList.push_back("../assets/Levels/Construction");
		tempList.push_back("../assets/Levels/Toy");
		tempList.push_back("../assets/Levels/Psych1");
		tempList.push_back("../assets/Levels/Psych2");
		tempList.push_back("../assets/Levels/RetroHard");
		tempList.push_back("../assets/Levels/FreezerHard1");
		tempList.push_back("../assets/Levels/FreezerHard2");
		tempList.push_back("../assets/Levels/PyramidFAST");
	}

	//tempList.push_back("../assets/Levels/VUM");
	//tempList.push_back("../assets/Levels/VUM2");

	std::cout << "Making Level List" << std::endl;
	levelFinalNum = tempList.size();


	return tempList;


}

std::vector<Level> LevelManager::MakeLevels()
{
	GlobalSettings::initSettings();

	std::cout << "Making Levels" << std::endl;

	std::vector<Level> tempLevels;

	if(levelFinalNum == 0)
	{
		std::cout << "What the fug" << std::endl;
	}

	for(unsigned i = 0; i < levelFinalNum; ++i)
	{
		std::string file = levelFilenames[i] + "Settings.txt";

		std::cout << "Reading Level Settings" << levelFilenames[i] << "Settings.txt" << std::endl;
		//std::cout  << std::endl;

		//tempLevels[i].filename = &levelFilenames[i];

		const int listSize = 2;
		tempLevels.push_back(Level(&levelFilenames[i]));
		tempLevels[i].listLight.resize(listSize);
		tempLevels[i].listFog.resize(listSize);
		tempLevels[i].listEye.resize(listSize);
		tempLevels[i].listWave.resize(listSize);
		tempLevels[i].listSpeed.resize(listSize);
		tempLevels[i].listSound.resize(listSize);
		tempLevels[i].listBloom.resize(listSize);
		tempLevels[i].listContrast.resize(listSize);
		tempLevels[i].listBlockColor.resize(listSize);
		tempLevels[i].listEnvironment.resize(listSize);
		tempLevels[i].listShipLight.resize(listSize);
		tempLevels[i].listGreyscale.resize(listSize);
		tempLevels[i].listDOF.resize(listSize);
		tempLevels[i].listScanline.resize(listSize);
		tempLevels[i].listSobel.resize(listSize);
		tempLevels[i].listSonar.resize(listSize);
		tempLevels[i].listShadow.resize(listSize);
		tempLevels[i].listMusic.resize(listSize);
		tempLevels[i].listFog.resize(10);
		for (int c = 0; c < listSize; ++c)
		{
			tempLevels[i].listLight[c] = sat::level::ListLight();
			
			for(int fogLayerNum = 0; fogLayerNum < tempLevels[i].listFog.size(); ++fogLayerNum)
			{			
				tempLevels[i].listFog[fogLayerNum].FogLayer.resize(listSize);
				tempLevels[i].listFog[fogLayerNum].FogLayer[c] = sat::level::ListFogLayer();
			}
			tempLevels[i].listEye[c] = sat::level::ListEye();
			tempLevels[i].listWave[c] = sat::level::ListWave();
			tempLevels[i].listSpeed[c] = sat::level::ListSpeed();
			tempLevels[i].listSound[c] = sat::level::ListSound();
			tempLevels[i].listBloom[c] = sat::level::ListBloom();
			tempLevels[i].listContrast[c] = sat::level::ListContrast();
			tempLevels[i].listBlockColor[c].material = sat::level::ListBlockColor().material;
			tempLevels[i].listEnvironment[c] = sat::level::ListEnvironment();
			tempLevels[i].listShipLight[c] = sat::level::ListShipLight();
			tempLevels[i].listGreyscale[c] = sat::level::ListGreyscale();
			tempLevels[i].listDOF[c] = sat::level::ListDOF();
			tempLevels[i].listScanline[c] = sat::level::ListScanline();
			tempLevels[i].listSobel[c] = sat::level::ListSobel();
			tempLevels[i].listSonar[c] = sat::level::ListSonar();
			tempLevels[i].listShadow[c] = sat::level::ListShadow();
			tempLevels[i].listMusic[c] = sat::level::ListMusic();
		}
		tempLevels[i].Toggle.blockRandomToggle = 0;
		tempLevels[i].Toggle.blockRandomToggleAmbientAdd = 0;
		tempLevels[i].Toggle.blockRandomToggleAmbientMult = 0;
		tempLevels[i].Toggle.blockRandomToggleDiffuseAdd = 0;
		tempLevels[i].Toggle.blockRandomToggleDiffuseMult = 0;
		tempLevels[i].Toggle.blockRandomToggleSpecularAdd = 0;
		tempLevels[i].Toggle.blockRandomToggleSpecularMult = 0;
		tempLevels[i].Toggle.blockRandomToggleEmissiveAdd = 0;
		tempLevels[i].Toggle.blockRandomToggleEmissiveMult = 0;
		tempLevels[i].Toggle.blockRandomToggleShinyAdd = 0;
		tempLevels[i].Toggle.blockRandomToggleShinyMult = 0;

		

		std::ifstream inFile(file);

		if (!inFile)
		{
			std::cout << "Could not open the file for reading." << file << std::endl;
			//std::cout  << std::endl;
			levelFinalNum = i;
			return tempLevels;
		}

		char line[CHAR_BUFFER_SIZE];
		

		// Unique Data
		//int SizeX;
		//int SizeY;

		//Data Holder
		float x, y, z, w;
		int ix, iy, iz, iw;
		unsigned int ux, uy, uz, uw;
		char lineChar[CHAR_BUFFER_SIZE];
		std::string lineString;

		int lineNumber = 0;

		while (!inFile.eof())
		{
			++lineNumber;

			inFile.getline(line, CHAR_BUFFER_SIZE);
			//lineString = line;

			//if(std::equal(line,line+7,"RimLight"))
			if(!strncmp(line,"asdfasdfasdfasdf: ", 10))
			{
				std::cout << "Wow I didn't expect that" << std::endl;
			}
			elf(!strncmp(line,"Light", 5))
			{
				if(!strncmp(line,"LightPos: ", 9))
				{
					std::sscanf(line, "LightPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listLight, uw);
					tempLevels[i].listLight[uw+1].pos = w;
				}
				elf(!strncmp(line,"LightRim: ", 9))
				{
					std::sscanf(line, "LightRim: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listLight, uw);
					tempLevels[i].listLight[uw+1].lightRim = satColor(x, y, z);
				}
				elf(!strncmp(line,"LightAmbient: ", 12))
				{
					std::sscanf(line, "LightAmbient: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listLight, uw);
					tempLevels[i].listLight[uw+1].lightAmbient = satColor(x, y, z);
					//std::cout << uw << " " << tempLevels[i].listLight[uw].lightAmbient.r << " " << tempLevels[i].listLight[uw].lightAmbient.g << " "  << tempLevels[i].listLight[uw].lightAmbient.b << std::endl;
				}
				elf(!strncmp(line,"LightDiffuse: ", 13))
				{
					std::sscanf(line, "LightDiffuse: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listLight, uw);
					tempLevels[i].listLight[uw+1].lightDiffuse = satColor(x, y, z);
					//std::cout << tempLevels[i].listLight[uw+1].lightDiffuse.r << std::endl;
				}	
				elf(!strncmp(line,"LightSpecular: ", 14))
				{
					std::sscanf(line, "LightSpecular: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listLight, uw);
					tempLevels[i].listLight[uw+1].lightSpecular = satColor(x, y, z);
					//std::cout << tempLevels[i].listLight[uw+1].lightDiffuse.r << std::endl;
				}	
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Scanline", 8))
			{
				if(!strncmp(line,"ScanlineActive: ", 15))
				{
					std::sscanf(line, "ScanlineActive: %u", &uw);			
					tempLevels[i].ScanlineActive = uw;
				}
				elf(!strncmp(line,"ScanlinePos: ", 11))
				{
					std::sscanf(line, "ScanlinePos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listScanline, uw);
					tempLevels[i].listScanline[uw+1].pos = w;
				}
				elf(!strncmp(line,"ScanlineAmount: ", 14))
				{
					std::sscanf(line, "ScanlineAmount: %u %f %f ", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listScanline, uw);
					tempLevels[i].listScanline[uw+1].Amount = satVec2(x, y);
				}
				elf(!strncmp(line,"ScanlineMod: ", 11))
				{
					std::sscanf(line, "ScanlineMod: %u %u %u", &uw, &ux, &uy);
					sat::level::Resize(tempLevels[i].listScanline, uw);
					tempLevels[i].listScanline[uw+1].ModX = ux;
					tempLevels[i].listScanline[uw+1].ModY = uy;
				}
				elf(!strncmp(line,"ScanlineThreshold: ", 18))
				{
					std::sscanf(line, "ScanlineThreshold: %u %u %u", &uw, &ux, &uy);
					sat::level::Resize(tempLevels[i].listScanline, uw);
					tempLevels[i].listScanline[uw+1].ThresholdX = ux;
					tempLevels[i].listScanline[uw+1].ThresholdY = uy;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Song", 4))
			{
				if(!strncmp(line,"SongFilename: ", 13))
				{
					std::sscanf(line, "SongFilename: %s", &lineChar);
					tempLevels[i].SongFilename = lineChar;
				}
				elf(!strncmp(line, "SongTempo: ", 10))
				{
					std::sscanf(line, "SongTempo: %f", &x);
					tempLevels[i].SongTempo = x;
				}
				elf(!strncmp(line, "SongBounce: ", 11))
				{
					std::sscanf(line, "SongBounce: %f", &x);
					tempLevels[i].SongBounce = x;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
				
			}
			elf(!strncmp(line,"Sonar", 5))
			{

				if (!strncmp(line, "SonarPos: ", 5 + 4))
				{
					std::sscanf(line, "SonarPos: %u %f", &uw, &w);
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw + 1].pos = w;
				}
				elf(!strncmp(line,"SonarActive: ", 6+6))
				{
					std::sscanf(line, "SonarActive: %u", &uw);			
					tempLevels[i].SonarActive = uw;
				}
				elf(!strncmp(line,"SonarColor: ", 6+5))
				{
					std::sscanf(line, "SonarColor: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw+1].colorAdd = satColor(x, y, z);
				}
				elf(!strncmp(line,"SonarSpeed: ", 6+5))
				{
					std::sscanf(line, "SonarSpeed: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw+1].speedMult = x;
				}
				elf(!strncmp(line,"SonarMultiplier: ", 6+10))
				{
					std::sscanf(line, "SonarMultiplier: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw+1].multiplier = x;
				}
				elf(!strncmp(line,"SonarDiffusePass: ", 6+7+4))
				{
					std::sscanf(line, "SonarDiffusePass: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw+1].diffusePass = x;
				}
				elf(!strncmp(line,"SonarEmissivePass: ", 6+8+4))
				{
					std::sscanf(line, "SonarEmissivePass: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listSonar, uw);
					tempLevels[i].listSonar[uw+1].emissivePass = x;
				}
			}
			elf(!strncmp(line,"Shadow", 6))
			{
				if(!strncmp(line,"ShadowPos: ", 6+4))
				{
					std::sscanf(line, "ShadowPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].pos = w;
				}
				elf(!strncmp(line,"ShadowOrthoActive: ", 6+5+7))
				{
					std::sscanf(line, "ShadowOrthoActive: %u", &uw);
					tempLevels[i].ShadowOrthographicActive = uw;
				}
				elf(!strncmp(line,"ShadowOrthographicActive: ", 6+12+7))
				{
					std::sscanf(line, "ShadowOrthographicActive: %u", &uw);				
					tempLevels[i].ShadowOrthographicActive = uw;
				}
				elf(!strncmp(line,"ShadowRotation: ", 6+9))
				{
					std::sscanf(line, "ShadowRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowTranslation: ", 6+12))
				{
					std::sscanf(line, "ShadowTranslation: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].translation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowTranslationRotation: ", 6+11+9))
				{
					std::sscanf(line, "ShadowTranslationRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].translationRotation = satVec3(x, y, z);
				}					
				elf(!strncmp(line,"ShadowSpeedRotation: ", 6+5+9))
				{
					std::sscanf(line, "ShadowSpeedRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].speedRotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeRotation: ", 6+5+9))
				{
					std::sscanf(line, "ShadowRangeRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeRotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeSpeedRotation: ", 6+5+5+9))
				{
					std::sscanf(line, "ShadowRangeSpeedRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeSpeedRotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeTranslation: ", 6+5+12))
				{
					std::sscanf(line, "ShadowRangeTranslation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeTranslation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeSpeedTranslation: ", 6+5+5+12))
				{
					std::sscanf(line, "ShadowRangeSpeedTranslation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeSpeedTranslation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeTranslationRotation: ", 6+5+11+9))
				{
					std::sscanf(line, "ShadowRangeTranslationRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeTranslationRotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowRangeSpeedTranslationRotation: ", 6+5+5+11+9))
				{
					std::sscanf(line, "ShadowRangeSpeedTranslationRotation: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].rangeSpeedTranslationRotation = satVec3(x, y, z);
				}
				elf(!strncmp(line,"ShadowFOV: ", 6+4))
				{
					std::sscanf(line, "ShadowFOV: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listShadow, uw);
					tempLevels[i].listShadow[uw+1].FOV = x;
				}
				//elf(!strncmp(line,"LightSpecular: ", 14))
				//{
				//	std::sscanf(line, "LightSpecular: %u %f %f %f", &uw, &x, &y, &z);				
				//	sat::level::Resize(tempLevels[i].listShadow, uw);
				//	tempLevels[i].listShadow[uw+1].lightSpecular = satColor(x, y, z);
				//	//std::cout << tempLevels[i].listLight[uw+1].lightDiffuse.r << std::endl;
				//}	
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"FogLayer", 5+3))
			{
				int layer = 0;
				if(!strncmp(line,"FogLayerDistance: ", 5+12))
				{
					std::sscanf(line, "FogLayerDistance: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogNear = x;
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogFar = y;
				}
				elf(!strncmp(line,"FogLayerRange: ", 5+12))
				{
					std::sscanf(line, "FogLayerRange: %u, %u %f %f", &layer, &uw, &x, &y);			
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogRange = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerColorBottom: ", 5+15))
				{
					std::sscanf(line, "FogLayerColorBottom: %u, %u %f %f %f", &layer, &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogBottomColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogLayerColorTop: ", 5+12))
				{
					std::sscanf(line, "FogLayerColorTop: %u, %u %f %f %f", &layer, &uw, &x, &y, &z);					
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogTopColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogLayerPosMult: ", 5+12))
				{
					std::sscanf(line, "FogLayerPosMult: %u, %u %f %f %f", &layer, &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogPosMult = satVec3(x, y, z);
				}
				elf(!strncmp(line,"FogLayerColor: ", 5+10))
				{
					std::sscanf(line, "FogLayerColor: %u, %u %f %f %f", &layer, &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogColor = satColor(x, y, z);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogTopColor = satColor(x, y, z);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogBottomColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogLayerOffset: ", 5+11))
				{
					std::sscanf(line, "FogLayerOffset: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].offset = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerOffsetMult: ", 5+14))
				{
					std::sscanf(line, "FogLayerOffsetMult: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].offsetMult = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerOffsetPosMult: ", 5+17))
				{
					std::sscanf(line, "FogLayerOffsetPosMult: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].offsetPosMult = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerVelocity: ", 5+12))
				{
					std::sscanf(line, "FogLayerVelocity: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].velocity = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerWaveCount: ", 5+13))
				{
					std::sscanf(line, "FogLayerWaveCount: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].waveCount = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerWaveIntensity: ", 5+17))
				{
					std::sscanf(line, "FogLayerWaveIntensity: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].waveIntensity = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerWaveTime: ", 5+12))
				{
					std::sscanf(line, "FogLayerWaveTime: %u, %u %f %f", &layer, &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].waveTime = satVec2(x, y);
				}
				elf(!strncmp(line,"FogLayerEmissive: ", 5+12))
				{
					std::sscanf(line, "FogLayerEmissive: %u, %u %f", &layer, &uw, &x);				
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].FogEmissivePass = x;
				}
				elf(!strncmp(line,"FogLayerPos: ", 5+7))
				{
					std::sscanf(line, "FogLayerPos: %u, %u %f", &layer, &uw, &w);					
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].pos = w;
				}				
				elf(!strncmp(line,"FogLayerImage: ", 5+9))
				{
					std::sscanf(line, "FogLayerImage: %u, %u %s", &layer, &uw, &lineChar);					
					sat::level::Resize(tempLevels[i].listFog[layer].FogLayer, uw);
					tempLevels[i].listFog[layer].FogLayer[uw+1].imageFilename = lineChar;
				}	
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}	
			elf(!strncmp(line,"Fog", 3))
			{
				if(!strncmp(line,"FogActive: ", 10))
				{
					std::sscanf(line, "FogActive: %u", &uw);			
					tempLevels[i].FogActive = uw;
				}
				elf(!strncmp(line,"FogDetail: ", 10))
				{
					std::sscanf(line, "FogDetail: %u", &uw);			
					tempLevels[i].FogDetail = uw;
				}
				elf(!strncmp(line,"FogNumLayers: ", 13))
				{
					std::sscanf(line, "FogNumLayers: %u", &uw);			
					tempLevels[i].listFog.resize(uw);
					tempLevels[i].FogLayers = uw;
				}
				elf(!strncmp(line,"FogDistance: ", 12))
				{
					std::sscanf(line, "FogDistance: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogNear = x;
					tempLevels[i].listFog[0].FogLayer[uw+1].FogFar = y;
				}
				elf(!strncmp(line,"FogRange: ", 12))
				{
					std::sscanf(line, "FogDistance: %u %f %f", &uw, &x, &y);			
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogRange = satVec2(x, y);
				}
				elf(!strncmp(line,"FogColorBottom: ", 15))
				{
					std::sscanf(line, "FogColorBottom: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogBottomColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogColorTop: ", 12))
				{
					std::sscanf(line, "FogColorTop: %u %f %f %f", &uw, &x, &y, &z);					
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogTopColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogPosMult: ", 12))
				{
					std::sscanf(line, "FogPosMult: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogPosMult = satVec3(x, y, z);
				}
				elf(!strncmp(line,"FogColor: ", 10))
				{
					std::sscanf(line, "FogColor: %u %f %f %f", &uw, &x, &y, &z);				
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogColor = satColor(x, y, z);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogTopColor = satColor(x, y, z);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogBottomColor = satColor(x, y, z);
				}
				elf(!strncmp(line,"FogEmissive: ", 12))
				{
					std::sscanf(line, "FogEmissive: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].FogEmissivePass = x;
				}
				elf(!strncmp(line,"FogPos: ", 7))
				{
					std::sscanf(line, "FogPos: %u %f", &uw, &w);					
					sat::level::Resize(tempLevels[i].listFog[0].FogLayer, uw);
					tempLevels[i].listFog[0].FogLayer[uw+1].pos = w;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}	
			elf(!strncmp(line,"Greyscale", 9))
			{
				if(!strncmp(line,"GreyscalePos: ", 13))
				{
					std::sscanf(line, "GreyscalePos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listGreyscale, uw);
					tempLevels[i].listGreyscale[uw+1].pos = w;
				}
				elf(!strncmp(line, "GreyscaleAmountSepia: ", 20))
				{
					std::sscanf(line, "GreyscaleAmountSepia: %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listGreyscale, uw);
					tempLevels[i].listGreyscale[uw + 1].amountSepia = x;
				}
				elf(!strncmp(line,"GreyscaleAmount: ", 15))
				{
					std::sscanf(line, "GreyscaleAmount: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listGreyscale, uw);
					tempLevels[i].listGreyscale[uw+1].amount = x;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
				
			}
			elf(!strncmp(line,"Sobel", 5))
			{
				if(!strncmp(line,"SobelActive: ", 12))
				{
					std::sscanf(line, "SobelActive: %u", &uw);
					tempLevels[i].SobelActive = uw;
				}
				elf(!strncmp(line,"SobelThickToggle: ", 17))
				{
					std::sscanf(line, "SobelThickToggle: %u", &uw);
					tempLevels[i].SobelThickToggle = uw;
				}
				elf(!strncmp(line,"SobelPos: ", 13))
				{
					std::sscanf(line, "SobelPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listSobel, uw);
					tempLevels[i].listSobel[uw+1].pos = w;
				}
				elf(!strncmp(line,"SobelAmountNormal: ", 12+6))
				{
					std::sscanf(line, "SobelAmountNormal: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listSobel, uw);
					tempLevels[i].listSobel[uw+1].normalThickThreshold = x;
					tempLevels[i].listSobel[uw + 1].normalThreshold = x;
				}
				elf(!strncmp(line, "SobelAmountDepth: ", 12+5))
				{
					std::sscanf(line, "SobelAmountDepth: %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listSobel, uw);
					tempLevels[i].listSobel[uw + 1].depthThickThreshold = x;
					tempLevels[i].listSobel[uw + 1].depthThreshold = x;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}				
			}
			elf(!strncmp(line,"Eye", 3))
			{
				if(!strncmp(line,"EyePos: ", 7))
				{
					std::sscanf(line, "EyePos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listEye, uw);
					tempLevels[i].listEye[uw+1].pos = w;
					//std::cout << "EyePos: " << uw+1 << "\t" << w << std::endl;
				}
				elf(!strncmp(line,"EyeConvergence: ", 15))
				{
					std::sscanf(line, "EyeConvergence: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listEye, uw);
					tempLevels[i].listEye[uw+1].Convergence = x;
					//std::cout << "EyeCovergence: " << uw+1 << "\t" << x << std::endl;
				}
				elf(!strncmp(line,"EyeDistanceMult: ", 16))
				{
					std::sscanf(line, "EyeDistanceMult: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listEye, uw);
					tempLevels[i].listEye[uw+1].EyeDistanceMult = x;
					//std::cout << "EyeDistanceMult: " << uw+1 << "\t" << x << std::endl;
				}
				elf(!strncmp(line,"EyeShift: ", 9))
				{
					std::sscanf(line, "EyeShift: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listEye, uw);
					tempLevels[i].listEye[uw+1].EyeShift = satVec2(x, y);
					//std::cout << "EyeShift: " << uw+1 << "\t" << x << std::endl;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}	
			elf(!strncmp(line,"ShipLight", 9))
			{
				
				if(!strncmp(line,"ShipLight: ", 11))
				{
					std::sscanf(line, "ShipLight: %u %f %f %f %f", &uw, &x, &y, &z, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].constant = x;
					tempLevels[i].listShipLight[uw+1].linear = y;
					tempLevels[i].listShipLight[uw+1].quad = z;
					tempLevels[i].listShipLight[uw+1].innerRadius = w;
				}
				elf(!strncmp(line,"ShipLightMult: ", 14))
				{
					std::sscanf(line, "ShipLightMult: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].colorMultiplier = x;
				}
				elf(!strncmp(line,"ShipLightPos: ", 13))
				{
					std::sscanf(line, "ShipLightPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].pos = w;
				}
				elf(!strncmp(line,"ShipLightConstant: ", 18))
				{
					std::sscanf(line, "ShipLightConstant: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].constant = w;
				}
				elf(!strncmp(line,"ShipLightLinear: ", 16))
				{
					std::sscanf(line, "ShipLightLinear: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].linear = w;
				}
				elf(!strncmp(line,"ShipLightQuad: ", 14))
				{
					std::sscanf(line, "ShipLightQuad: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].quad = w;
				}
				elf(!strncmp(line,"ShipLightRadius: ", 16))
				{
					std::sscanf(line, "ShipLightRadius: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listShipLight, uw);
					tempLevels[i].listShipLight[uw+1].innerRadius = w;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}	
			elf(!strncmp(line,"Wave", 4))
			{
				if(!strncmp(line,"WaveActive: ", 11))
				{
					std::sscanf(line, "WaveActive: %u", &uw);			
					tempLevels[i].WaveActive = uw;
				}
				elf(!strncmp(line,"WavePos: ", 8))
				{
					std::sscanf(line, "WavePos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listWave, uw);
					tempLevels[i].listWave[uw+1].pos = w;
				}
				elf(!strncmp(line,"WaveIntensity: ", 14))
				{
					std::sscanf(line, "WaveIntensity: %u %f %f", &uw, &x, &y);			
					sat::level::Resize(tempLevels[i].listWave, uw);
					tempLevels[i].listWave[uw+1].intensity = satVec2(x, y);
				}
				elf(!strncmp(line,"WaveCount: ", 10))
				{
					std::sscanf(line, "WaveCount: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listWave, uw);
					tempLevels[i].listWave[uw+1].count = satVec2(x, y);
				}
				elf(!strncmp(line,"WaveTime: ", 8))
				{
					std::sscanf(line, "WaveTime: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listWave, uw);
					tempLevels[i].listWave[uw+1].time = satVec2(x, y);
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"DOF", 3))
			{
				if(!strncmp(line,"DOFActive: ", 10))
				{
					std::sscanf(line, "DOFActive: %u", &uw);			
					tempLevels[i].DOFActive = uw;
				}
				elf(!strncmp(line,"DOFPos: ", 7))
				{
					std::sscanf(line, "DOFPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].pos = w;
				}
				elf(!strncmp(line,"DOFBias: ", 8))
				{
					std::sscanf(line, "DOFBias: %u %f", &uw, &x);			
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].bias = x;
				}
				elf(!strncmp(line,"DOFClamp: ", 9))
				{
					std::sscanf(line, "DOFClamp: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].clamp = x;
				}
				elf(!strncmp(line,"DOFBiasMult: ", 12))
				{
					std::sscanf(line, "DOFBiasMult: %u %f", &uw, &x);			
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].biasMult = x;
				}
				elf(!strncmp(line,"DOFClampMult: ", 13))
				{
					std::sscanf(line, "DOFClampMult: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].clampMult = x;
				}
				elf(!strncmp(line,"DOFFocus: ", 9))
				{
					std::sscanf(line, "DOFFocus: %u %f", &uw, &x);				
					sat::level::Resize(tempLevels[i].listDOF, uw);
					tempLevels[i].listDOF[uw+1].focus = x;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Sound", 5))
			{
				if(!strncmp(line,"SoundPos: ", 9))
				{
					std::sscanf(line, "SoundPos: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].pos = w;
				}
				elf(!strncmp(line,"SoundFilename: ", 15))
				{
					std::sscanf(line, "SoundFilename: %u %s", &uw, &lineChar);			
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].soundFilename = lineChar;
					tempLevels[i].listSound[uw+1].soundPlayed = false;
					tempLevels[i].listSound[uw+1].soundBlank = false;
				}
				elf(!strncmp(line,"SoundFreqMult: ", 15))
				{
					std::sscanf(line, "SoundFreqMult: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].frequencyMult = w;
				}
				elf(!strncmp(line,"SoundFreqRan: ", 14))
				{
					std::sscanf(line, "SoundFreqRan: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].frequencyRandom = w;
				}
				elf(!strncmp(line,"SoundVolume: ", 12))
				{
					std::sscanf(line, "SoundVolume: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].volume = w;
				}
				elf(!strncmp(line,"SoundSettings: ", 14))
				{
					std::sscanf(line, "SoundSettings: %u %f %f %f", &uw, &x, &y ,&z);				
					sat::level::Resize(tempLevels[i].listSound, uw);
					tempLevels[i].listSound[uw+1].volume = x;
					tempLevels[i].listSound[uw+1].frequencyMult = y;
					tempLevels[i].listSound[uw+1].frequencyRandom = z;					
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Music", 5))
			{
				if(!strncmp(line,"MusicPos: ", 10))
				{
					std::sscanf(line, "MusicPos: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listMusic, uw);
					tempLevels[i].listMusic[uw+1].pos = w;
				}
				//I might use this elf later I dunno
				//probably not though
				
				//elf(!strncmp(line,"MusicActive: ", 11))
				//{
				//	std::sscanf(line, "MusicActive: %u", &uw);	
				//	tempLevels[i].MusicActive = uw;
				//	//
				//	//std::sscanf(line, "MusicLower: %u %f", &uw, &w);				
				//	//sat::level::Resize(tempLevels[i].listMusic, uw);
				//	//tempLevels[i].listMusic[uw+1].bloomThresholdLower = w;
				//}
				elf(!strncmp(line,"MusicPitch: ", 11))
				{
					std::sscanf(line, "MusicPitch: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listMusic, uw);
					tempLevels[i].listMusic[uw+1].pitchMult = w;
				}
				elf(!strncmp(line,"MusicVolume: ", 12))
				{
					std::sscanf(line, "MusicVolume: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listMusic, uw);
					tempLevels[i].listMusic[uw+1].volumeMult = w;
				}
				elf(!strncmp(line,"MusicParam: ", 11))
				{
					x = 1.0f; y = 1.0f;
					std::sscanf(line, "MusicParam: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listMusic, uw);
					tempLevels[i].listMusic[uw+1].pitchMult = x;
					tempLevels[i].listMusic[uw+1].volumeMult = y;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Block", 5))
			{				
				if(!strncmp(line,"BlockWavyActive: ", 16))
				{
					std::sscanf(line, "BlockWavyActive: %u", &uw);			
					tempLevels[i].BlockWavyActive = uw;

				}
				elf(!strncmp(line,"BlockCascadeActive: ", 19))
				{
					std::sscanf(line, "BlockCascadeActive: %u", &uw);			
					tempLevels[i].BlockCascadeActive = uw;

				}
				elf(!strncmp(line,"BlockNumberOf: ", 14))
				{
					std::sscanf(line, "BlockNumberOf: %u", &uw);	
					//tempLevels[i].listBlockColor.resize(uw);
				}
				elf(!strncmp(line,"BlockAmbientAdd: ", 16))
				{
					std::sscanf(line, "BlockAmbientAdd: %u %f %f %f", &uw, &x, &y, &z);	
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].ambientAdd = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockAmbientMult: ", 17))
				{
					std::sscanf(line, "BlockAmbientMult: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].ambientMult = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockDiffuseAdd: ", 16))
				{
					std::sscanf(line, "BlockDiffuseAdd: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].diffuseAdd = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockDiffuseMult: ", 17))
				{
					std::sscanf(line, "BlockDiffuseMult: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].diffuseMult = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockSpecularAdd: ", 17))
				{
					std::sscanf(line, "BlockSpecularAdd: %u %f %f %f", &uw, &x, &y, &z);	
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].specularAdd = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockSpecularMult: ", 18))
				{
					std::sscanf(line, "BlockSpecularMult: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].specularMult = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockEmissiveAdd: ", 17))
				{
					std::sscanf(line, "BlockEmissiveAdd: %u %f %f %f", &uw, &x, &y, &z);	
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].emissiveAdd = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockEmissiveMult: ", 18))
				{
					std::sscanf(line, "BlockEmissiveMult: %u %f %f %f", &uw, &x, &y, &z);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].emissiveMult = satColor(x, y, z);
				}
				elf(!strncmp(line,"BlockShinyAdd: ", 15))
				{
					std::sscanf(line, "BlockShinyAdd: %u %f", &uw, &x);	
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].shinyAdd = x;
				}
				elf(!strncmp(line,"BlockShinyMult: ", 16))
				{
					std::sscanf(line, "BlockShinyMult: %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listBlockColor, uw);
					tempLevels[i].listBlockColor[uw].shinyMult = x;
				}
				///////////////////////////////////////
				elf(!strncmp(line,"BlockRandomAmbientAdd: ", 22))
				{
					std::sscanf(line, "BlockRandomAmbientAdd: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleAmbientAdd = iw;
				}
				elf(!strncmp(line,"BlockRandomAmbientMult: ", 23))
				{
					std::sscanf(line, "BlockRandomAmbientMult: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleAmbientMult = iw;
				}
				elf(!strncmp(line,"BlockRandomDiffuseAdd: ", 22))
				{
					std::sscanf(line, "BlockRandomDiffuseAdd: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleDiffuseAdd = iw;
				}
				elf(!strncmp(line,"BlockRandomDiffuseMult: ", 23))
				{
					std::sscanf(line, "BlockRandomDiffuseMult: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleDiffuseMult = iw;
				}
				elf(!strncmp(line,"BlockRandomSpecularAdd: ", 23))
				{
					std::sscanf(line, "BlockRandomSpecularAdd: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleSpecularAdd = iw;
				}
				elf(!strncmp(line,"BlockRandomSpecularMult: ", 24))
				{
					std::sscanf(line, "BlockRandomSpecularMult: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleSpecularMult = iw;
				}
				elf(!strncmp(line,"BlockRandomEmissiveAdd: ", 23))
				{
					std::sscanf(line, "BlockRandomEmissiveAdd: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleEmissiveAdd = iw;
				}
				elf(!strncmp(line,"BlockRandomEmissiveMult: ", 24))
				{
					std::sscanf(line, "BlockRandomEmissiveMult: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleEmissiveMult = iw;
				}
				elf(!strncmp(line,"BlockRandomShinyAdd: ", 21))
				{
					std::sscanf(line, "BlockRandomShinyAdd: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleShinyAdd = iw;
				}
				elf(!strncmp(line,"BlockRandomShinyMult: ", 22))
				{
					std::sscanf(line, "BlockRandomShinyMult: %i", &iw);	
					tempLevels[i].Toggle.blockRandomToggleShinyMult = iw;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	

				
			}
			elf(!strncmp(line,"Speed", 5))
			{
				if(!strncmp(line,"SpeedPos: ", 10))
				{
					std::sscanf(line, "SpeedPos: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].pos = w;
				}
				elf(!strncmp(line,"SpeedCatmull: ", 13))
				{
					std::sscanf(line, "SpeedCatmull: %u %u", &uw, &ux);				
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].useCatmull = ux;
				}
				elf(!strncmp(line,"SpeedMoving: ", 13))
				{
					std::sscanf(line, "SpeedMoving: %u %f %f %f %f", &uw, &x, &y, &z, &w);				
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].moving = satVec4(x, y, z, w);
				}
				elf(!strncmp(line,"SpeedMovingSimple: ", 19))
				{
					std::sscanf(line, "SpeedMovingSimple: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].moving = satVec4(x * 2.0f, y, x * 0.5f, x * 1.0f);
				}
				elf(!strncmp(line,"SpeedMult: ", 11))
				{
					std::sscanf(line, "SpeedMult: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].mult = satVec2(x, y);
				}
				elf(!strncmp(line,"SpeedShrink: ", 12))
				{
					std::sscanf(line, "SpeedShrink: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listSpeed, uw);
					tempLevels[i].listSpeed[uw+1].shrink = satVec2(x, y);
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Bloom", 5))
			{
				if(!strncmp(line,"BloomPos: ", 10))
				{
					std::sscanf(line, "BloomPos: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listBloom, uw);
					tempLevels[i].listBloom[uw+1].pos = w;
				}
				elf(!strncmp(line,"BloomActive: ", 11))
				{
					std::sscanf(line, "BloomActive: %u", &uw);	
					tempLevels[i].BloomActive = uw;
					//
					//std::sscanf(line, "BloomLower: %u %f", &uw, &w);				
					//sat::level::Resize(tempLevels[i].listBloom, uw);
					//tempLevels[i].listBloom[uw+1].bloomThresholdLower = w;
				}
				elf(!strncmp(line,"BloomLower: ", 11))
				{
					std::sscanf(line, "BloomLower: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listBloom, uw);
					tempLevels[i].listBloom[uw+1].bloomThresholdLower = w;
				}
				elf(!strncmp(line,"BloomUpper: ", 11))
				{
					std::sscanf(line, "BloomUpper: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listBloom, uw);
					tempLevels[i].listBloom[uw+1].bloomThresholdUpper = w;
				}
				elf(!strncmp(line,"BloomThres: ", 11))
				{
					std::sscanf(line, "BloomThres: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listBloom, uw);
					tempLevels[i].listBloom[uw+1].bloomThresholdLower = x;
					tempLevels[i].listBloom[uw+1].bloomThresholdUpper = y;
				}
				elf(!strncmp(line,"BloomThreshold: ", 15))
				{
					std::sscanf(line, "BloomThreshold: %u %f %f", &uw, &x, &y);				
					sat::level::Resize(tempLevels[i].listBloom, uw);
					tempLevels[i].listBloom[uw+1].bloomThresholdLower = x;
					tempLevels[i].listBloom[uw+1].bloomThresholdUpper = y;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Contrast", 8))
			{
				if(!strncmp(line,"ContrastPos: ", 11))
				{
					std::sscanf(line, "ContrastPos: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listContrast, uw);
					tempLevels[i].listContrast[uw+1].pos = w;
				}
				elf(!strncmp(line,"ContrastAmount: ", 13))
				{
					std::sscanf(line, "ContrastAmount: %u %f", &uw, &w);				
					sat::level::Resize(tempLevels[i].listContrast, uw);
					tempLevels[i].listContrast[uw+1].contrastAmount = w;
				}
				elf(!strncmp(line,"ContrastActive: ", 13))
				{
					std::sscanf(line, "ContrastActive: %u", &uw);	
					tempLevels[i].ContrastActive = uw;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"SSAO", 4))
			{
				if (!strncmp(line, "SSAOActive: ", 11))
				{
					std::sscanf(line, "SSAOActive: %u", &uw);
					tempLevels[i].SSAOActive = uw;
				}				
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Env", 3))
			{
				if(!strncmp(line,"EnvPos: ", 7))
				{
					std::sscanf(line, "EnvPos: %u %f", &uw, &w);
					sat::level::Resize(tempLevels[i].listEnvironment, uw);
					tempLevels[i].listEnvironment[uw+1].pos = w;
				}
				elf(!strncmp(line,"EnvAmount: ", 10))
				{
					std::sscanf(line, "EnvAmount: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listEnvironment, uw);
					tempLevels[i].listEnvironment[uw+1].pos = w;
				}
				elf(!strncmp(line,"EnvBackground: ", 14))
				{
					std::sscanf(line, "EnvBackground: %u %f", &uw, &w);		
					sat::level::Resize(tempLevels[i].listEnvironment, uw);
					tempLevels[i].listEnvironment[uw+1].backgroundAmount = w;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"EnemyRandom", 5+6))
			{				
				unsigned uuu	= 0;

				if(!strncmp(line,"EnemyRandomFilenameDiffuse: ", 6+21))
				{
					std::sscanf(line, "EnemyRandomFilenameDiffuse: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].diffuseFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomFilenameNormal: ", 6+20))
				{
					std::sscanf(line, "EnemyRandomFilenameNormal: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].normalFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomFilenameSpecular: ", 6+22))
				{
					std::sscanf(line, "EnemyRandomFilenameSpecular: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].specularFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomFilenameEmissive: ", 6+22))
				{
					std::sscanf(line, "EnemyRandomFilenameEmissive: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].emissiveFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomFilenameModel: ", 6+19))
				{
					std::sscanf(line, "EnemyRandomFilenameModel: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].modelFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomTextureFilter: ", 6+5+7+7))
				{
					std::sscanf(line, "EnemyRandomTextureFilter: %u, %u %s", &uw, &uuu, &lineChar);	
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);					
					tempLevels[i].listEnemy[uw].object[uuu].textureStringFilterMag = lineChar;
					tempLevels[i].listEnemy[uw].object[uuu].textureStringFilterMin = lineChar;	
				}	
				elf(!strncmp(line,"EnemyRandomTextureFilterMag: ", 5+6+7+7+3))
				{
					std::sscanf(line, "EnemyRandomTextureFilterMag: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].textureStringFilterMag = lineChar;
				}		
				elf(!strncmp(line,"EnemyRandomTextureFilterMin: ", 5+6+7+7+3))
				{
					std::sscanf(line, "EnemyRandomTextureFilterMin: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].textureStringFilterMin = lineChar;
				}	
				elf(!strncmp(line,"EnemyRandomTextureWrap: ", 5+6+7+5))
				{
					std::sscanf(line, "EnemyRandomTextureWrap: %u, %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].textureStringWrapTexU = lineChar;
					tempLevels[i].listEnemy[uw].object[uuu].textureStringWrapTexV = lineChar;
				}	
				elf(!strncmp(line,"EnemyRandomTextureWrapU: ", 5+6+7+6))
				{
					std::sscanf(line, "EnemyRandomTextureWrapU: %u, %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].textureStringWrapTexU = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomTextureWrapV: ", 5+6+7+6))
				{
					std::sscanf(line, "EnemyRandomTextureWrapV: %u, %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].textureStringWrapTexV = lineChar;
				}
				elf(!strncmp(line,"EnemyRandomPrimitivePolygonMode: ", 5+6+9+7+5))
				{
					std::sscanf(line, "EnemyRandomlPrimitivePolygonMode: %u, %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].primitiveStringPolygonMode = lineChar;
				}	
				elf(!strncmp(line,"EnemyRandomPrimitiveCullFace: ", 5+6+9+4+5))
				{
					std::sscanf(line, "EnemyRandomPrimitiveCullFace: %u, %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].primitiveStringCullFace = lineChar;
				}	
				elf(!strncmp(line,"EnemyRandomPrimitiveLineWidth: ", 5+6+9+4+6))
				{
					std::sscanf(line, "EnemyRandomPrimitiveLineWidth: %u, %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					sat::level::Resize(tempLevels[i].listEnemy[uw].object, uuu);
					tempLevels[i].listEnemy[uw].object[uuu].primitiveLineWidth = x;
				}	
			}
			elf(!strncmp(line,"Enemy", 5))
			{
				if(!strncmp(line,"EnemyFilenameDiffuse: ", 21))
				{
					std::sscanf(line, "EnemyFilenameDiffuse: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].diffuseFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyFilenameNormal: ", 20))
				{
					std::sscanf(line, "EnemyFilenameNormal: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].normalFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyFilenameSpecular: ", 22))
				{
					std::sscanf(line, "EnemyFilenameSpecular: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].specularFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyFilenameEmissive: ", 22))
				{
					std::sscanf(line, "EnemyFilenameEmissive: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].emissiveFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyFilenameModel: ", 19))
				{
					std::sscanf(line, "EnemyFilenameModel: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].modelFilename = lineChar;
				}
				elf(!strncmp(line,"EnemyTextureFilter: ", 5+7+7))
				{
					std::sscanf(line, "EnemyTextureFilter: %u %s", &uw, &lineChar);	
					sat::level::Resize(tempLevels[i].listEnemy, uw);					
					tempLevels[i].listEnemy[uw].object[0].textureStringFilterMag = lineChar;
					tempLevels[i].listEnemy[uw].object[0].textureStringFilterMin = lineChar;	
				}	
				elf(!strncmp(line,"EnemyTextureFilterMag: ", 5+7+7+3))
				{
					std::sscanf(line, "EnemyTextureFilterMag: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].textureStringFilterMag = lineChar;
				}		
				elf(!strncmp(line,"EnemyTextureFilterMin: ", 5+7+7+3))
				{
					std::sscanf(line, "EnemyTextureFilterMin: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].textureStringFilterMin = lineChar;
				}	
				elf(!strncmp(line,"EnemyTextureWrap: ", 5+7+5))
				{
					std::sscanf(line, "EnemyTextureWrap: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].textureStringWrapTexU = lineChar;
					tempLevels[i].listEnemy[uw].object[0].textureStringWrapTexV = lineChar;
				}	
				elf(!strncmp(line,"EnemyTextureWrapU: ", 5+7+6))
				{
					std::sscanf(line, "EnemyTextureWrapU: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].textureStringWrapTexU = lineChar;
				}
				elf(!strncmp(line,"EnemyTextureWrapV: ", 5+7+6))
				{
					std::sscanf(line, "EnemyTextureWrapV: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].textureStringWrapTexV = lineChar;
				}
				elf(!strncmp(line,"EnemyPrimitivePolygonMode: ", 5+9+7+5))
				{
					std::sscanf(line, "EnemylPrimitivePolygonMode: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].primitiveStringPolygonMode = lineChar;
				}	
				elf(!strncmp(line,"EnemyPrimitiveCullFace: ", 5+9+4+5))
				{
					std::sscanf(line, "EnemyPrimitiveCullFace: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].primitiveStringCullFace = lineChar;
				}	
				elf(!strncmp(line,"EnemyPrimitiveLineWidth: ", 5+9+4+6))
				{
					std::sscanf(line, "EnemyPrimitiveLineWidth: %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].object[0].primitiveLineWidth = x;
				}	
				elf(!strncmp(line,"EnemyChar: ", 10))
				{
					std::sscanf(line, "EnemyChar: %u %s", &uw, &lineChar);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].character = lineChar[0];
				}
				elf(!strncmp(line,"EnemyParam: ", 11))
				{
					std::sscanf(line, "EnemyParam: %u %u %u %u", &uw, &ux, &uy, &uz);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].collidable = ux;
					tempLevels[i].listEnemy[uw].breakable = uy;
					tempLevels[i].listEnemy[uw].shootable = uz;
				}
				elf(!strncmp(line,"EnemyKiller: ", 12))
				{
					std::sscanf(line, "EnemyKiller: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].killer = ux;
				}
				elf(!strncmp(line,"EnemyEnv: ", 9))
				{
					std::sscanf(line, "EnemyEnv: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].environmentAmount = x;
				}
				elf(!strncmp(line,"EnemyDead: ", 10))
				{
					std::sscanf(line, "EnemyDead: %u %s", &uw, &lineChar);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].changeTo = lineChar[0];
				}		
				elf(!strncmp(line,"EnemyPos: ", 9))
				{
					std::sscanf(line, "EnemyPos: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].transform = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemyOriginRotate: ", 5+6+7))
				{
					std::sscanf(line, "EnemyOriginRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].originRotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemyScale: ", 5+6))
				{
					std::sscanf(line, "EnemyScale: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].transformScale = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemyRotateRandomAngle: ", 5+6+6+6))
				{
					std::sscanf(line, "EnemyRotateRandomAngle: %u %f %f %f", &uw, &x, &y, &z);			
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].rotateRandomAngle = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemyRotate: ", 5+7))
				{
					std::sscanf(line, "EnemyRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].rotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemySpeedRotate: ", 5+5+7))
				{
					std::sscanf(line, "EnemySpeedRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].speedRotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"EnemyDiffuseMult: ", 17))
				{
					std::sscanf(line, "EnemyDiffuseMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].diffuseMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].diffuseMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"EnemyAmbientMult: ", 17))
				{
					std::sscanf(line, "EnemyAmbientMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].ambientMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].ambientMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"EnemyEmissiveMult: ", 18))
				{
					std::sscanf(line, "EnemyEmissiveMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].emissiveMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].emissiveMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"EnemySpecularMult: ", 18))
				{
					std::sscanf(line, "EnemySpecularMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].specularMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].specularMultEqualOne = false;
					}
				}				
				elf(!strncmp(line,"EnemyShinyMult: ", 16))
				{
					std::sscanf(line, "EnemyShinyMult: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].shinyMult = x;
					if(x != 1.0f)
					{
						tempLevels[i].listEnemy[uw].shinyMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"EnemyDiffuseAdd: ", 16))
				{
					std::sscanf(line, "EnemyDiffuseAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].diffuseAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].diffuseAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"EnemyAmbientAdd: ", 16))
				{
					std::sscanf(line, "EnemyAmbientAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].ambientAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].ambientAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"EnemyEmissiveAdd: ", 17))
				{
					std::sscanf(line, "EnemyEmissiveAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].emissiveAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].emissiveAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"EnemySpecularAdd: ", 17))
				{
					std::sscanf(line, "EnemySpecularAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].specularAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listEnemy[uw].specularAddEqualZero = false;
					}
				}				
				elf(!strncmp(line,"EnemyShinyAdd: ", 15))
				{
					std::sscanf(line, "EnemyShinyAdd: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].shinyAdd = x;
					if(x != 1.0f)
					{
						tempLevels[i].listEnemy[uw].shinyAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"EnemyLightActive: ", 17))
				{
					std::sscanf(line, "EnemyLightActive: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].pointLightActive = ux;
				}	
				elf(!strncmp(line,"EnemyLightColor: ", 16))
				{
					std::sscanf(line, "EnemyLightColor: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].pointLightColor = satColor(x, y, z);
				}	
				elf(!strncmp(line,"EnemyLightMult: ", 15))
				{
					std::sscanf(line, "EnemyLightMult: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].pointLightMult = x;
				}	
				elf(!strncmp(line,"EnemyLightAtten: ", 16))
				{
					std::sscanf(line, "EnemyLightAtten: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].pointLightAttenMult = x;
				}
				elf(!strncmp(line,"EnemyLightPos: ", 14))
				{
					std::sscanf(line, "EnemyLightPos: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listEnemy, uw);
					tempLevels[i].listEnemy[uw].pointLightPos = satVec3(x, y, z);
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"VisualRandom", 6+6))
			{				
				unsigned uuu	= 0;
				
				if(!strncmp(line,"VisualRandomVectorSize: ", 6+6+6+5))
				{
					std::sscanf(line, "VisualRandomVectorSize: %u, %u", &uw, &uuu);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object.resize(uuu);
				}
				elf(!strncmp(line,"VisualRandomFilenameDiffuse: ", 6+22))
				{
					std::sscanf(line, "VisualRandomFilenameDiffuse: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].diffuseFilename = lineChar;
				}
				elf(!strncmp(line,"VisualRandomFilenameNormal: ", 6+21))
				{
					std::sscanf(line, "VisualRandomFilenameNormal: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].normalFilename = lineChar;
				}
				elf(!strncmp(line,"VisualRandomFilenameSpecular: ", 6+23))
				{
					std::sscanf(line, "VisualRandomFilenameSpecular: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].specularFilename = lineChar;
				}
				elf(!strncmp(line,"VisualRandomFilenameEmissive: ", 6+23))
				{
					std::sscanf(line, "VisualRandomFilenameEmissive: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].emissiveFilename = lineChar;
				}
				elf(!strncmp(line,"VisualRandomFilenameModel: ", 6+20))
				{
					std::sscanf(line, "VisualRandomFilenameModel: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].modelFilename = lineChar;
				}
				elf(!strncmp(line,"VisualRandomTextureFilter: ", 6+6+7+7))
				{
					std::sscanf(line, "VisualRandomTextureFilter: %u, %u %s", &uw, &uuu, &lineChar);	
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);					
					tempLevels[i].listVisual[uw].object[uuu].textureStringFilterMag = lineChar;
					tempLevels[i].listVisual[uw].object[uuu].textureStringFilterMin = lineChar;	
				}	
				elf(!strncmp(line,"VisualRandomTextureFilterMag: ", 6+6+7+7+3))
				{
					std::sscanf(line, "VisualRandomTextureFilterMag: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].textureStringFilterMag = lineChar;
				}		
				elf(!strncmp(line,"VisualRandomTextureFilterMin: ", 6+6+7+7+3))
				{
					std::sscanf(line, "VisualRandomTextureFilterMin: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].textureStringFilterMin = lineChar;
				}	
				elf(!strncmp(line,"VisualRandomTextureWrap: ", 6+6+7+5))
				{
					std::sscanf(line, "VisualRandomTextureWrap: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].textureStringWrapTexU = lineChar;
					tempLevels[i].listVisual[uw].object[uuu].textureStringWrapTexV = lineChar;
				}	
				elf(!strncmp(line,"VisualRandomTextureWrapU: ", 6+6+7+6))
				{
					std::sscanf(line, "VisualRandomTextureWrapU: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].textureStringWrapTexU = lineChar;
				}
				elf(!strncmp(line,"VisualRandomTextureWrapV: ", 6+6+7+6))
				{
					std::sscanf(line, "VisualRandomTextureWrapV: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].textureStringWrapTexV = lineChar;
				}
				elf(!strncmp(line,"VisualRandomPrimitivePolygonMode: ", 6+6+9+7+5))
				{
					std::sscanf(line, "VisualRandomlPrimitivePolygonMode: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].primitiveStringPolygonMode = lineChar;
				}	
				elf(!strncmp(line,"VisualRandomPrimitiveCullFace: ", 6+6+9+4+5))
				{
					std::sscanf(line, "VisualRandomPrimitiveCullFace: %u, %u %s", &uw, &uuu, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].primitiveStringCullFace = lineChar;
				}	
				elf(!strncmp(line,"VisualRandomPrimitiveLineWidth: ", 6+6+9+4+6))
				{
					std::sscanf(line, "VisualRandomPrimitiveLineWidth: %u, %u %f", &uw, &uuu, &x);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					sat::level::Resize(tempLevels[i].listVisual[uw].object, uuu);
					tempLevels[i].listVisual[uw].object[uuu].primitiveLineWidth = x;
				}	
			}
			elf(!strncmp(line,"Visual", 6))
			{
				if(!strncmp(line,"VisualFilenameDiffuse: ", 22))
				{
					std::sscanf(line, "VisualFilenameDiffuse: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].diffuseFilename = lineChar;
				}
				elf(!strncmp(line,"VisualFilenameNormal: ", 21))
				{
					std::sscanf(line, "VisualFilenameNormal: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].normalFilename = lineChar;
				}
				elf(!strncmp(line,"VisualFilenameSpecular: ", 23))
				{
					std::sscanf(line, "VisualFilenameSpecular: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].specularFilename = lineChar;
				}
				elf(!strncmp(line,"VisualFilenameEmissive: ", 23))
				{
					std::sscanf(line, "VisualFilenameEmissive: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].emissiveFilename = lineChar;
				}
				elf(!strncmp(line,"VisualFilenameModel: ", 20))
				{
					std::sscanf(line, "VisualFilenameModel: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].modelFilename = lineChar;
				}
				elf(!strncmp(line,"VisualTextureFilter: ", 6+7+7))
				{
					std::sscanf(line, "VisualTextureFilter: %u %s", &uw, &lineChar);	
					sat::level::Resize(tempLevels[i].listVisual, uw);					
					tempLevels[i].listVisual[uw].object[0].textureStringFilterMag = lineChar;
					tempLevels[i].listVisual[uw].object[0].textureStringFilterMin = lineChar;	
				}	
				elf(!strncmp(line,"VisualTextureFilterMag: ", 6+7+7+3))
				{
					std::sscanf(line, "VisualTextureFilterMag: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].textureStringFilterMag = lineChar;
				}		
				elf(!strncmp(line,"VisualTextureFilterMin: ", 6+7+7+3))
				{
					std::sscanf(line, "VisualTextureFilterMin: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].textureStringFilterMin = lineChar;
				}	
				elf(!strncmp(line,"VisualTextureWrap: ", 6+7+5))
				{
					std::sscanf(line, "VisualTextureWrap: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].textureStringWrapTexU = lineChar;
					tempLevels[i].listVisual[uw].object[0].textureStringWrapTexV = lineChar;
				}	
				elf(!strncmp(line,"VisualTextureWrapU: ", 6+7+6))
				{
					std::sscanf(line, "VisualTextureWrapU: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].textureStringWrapTexU = lineChar;
				}
				elf(!strncmp(line,"VisualTextureWrapV: ", 6+7+6))
				{
					std::sscanf(line, "VisualTextureWrapV: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].textureStringWrapTexV = lineChar;
				}
				elf(!strncmp(line,"VisualPrimitivePolygonMode: ", 6+9+7+5))
				{
					std::sscanf(line, "VisualPrimitivePolygonMode: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].primitiveStringPolygonMode = lineChar;
				}	
				elf(!strncmp(line,"VisualPrimitiveCullFace: ", 6+9+4+5))
				{
					std::sscanf(line, "VisualPrimitiveCullFace: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].primitiveStringCullFace = lineChar;
				}		
				elf(!strncmp(line,"VisualPrimitiveLineWidth: ", 6+9+4+6))
				{
					std::sscanf(line, "VisualPrimitiveLineWidth: %u %f", &uw, &x);
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].object[0].primitiveLineWidth = x;
				}	
				elf(!strncmp(line,"VisualChar: ", 12))
				{
					std::sscanf(line, "VisualChar: %u %s", &uw, &lineChar);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].character = lineChar[0];
				}
				elf(!strncmp(line,"VisualParam: ", 12))
				{
					std::sscanf(line, "VisualParam: %u %u %u %u", &uw, &ux, &uy, &uz);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].collidable = ux;
					tempLevels[i].listVisual[uw].breakable = uy;
					tempLevels[i].listVisual[uw].shootable = uz;
				}
				elf(!strncmp(line,"VisualCollide: ", 11))
				{
					std::sscanf(line, "VisualCollide: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].collidable = ux;
				}
				elf(!strncmp(line,"VisualBreak: ", 12))
				{
					std::sscanf(line, "VisualBreak: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].breakable = ux;
				}
				elf(!strncmp(line,"VisualShoot: ", 12))
				{
					std::sscanf(line, "VisualShoot: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].shootable = ux;
				}
				elf(!strncmp(line,"VisualKiller: ", 13))
				{
					std::sscanf(line, "VisualKiller: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].killer = ux;
				}
				elf(!strncmp(line,"VisualHeal: ", 11))
				{
					std::sscanf(line, "VisualHeal: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].healable = ux;
				}
				elf(!strncmp(line,"VisualInvinciblePickup: ", 22))
				{
					std::sscanf(line, "VisualInvinciblePickup: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].invinciblePickup = ux;
				}
				elf(!strncmp(line,"VisualDoubleShootPickup: ", 24))
				{
					std::sscanf(line, "VisualDoubleShootPickup: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].doubleShootPickup = ux;
				}
				elf(!strncmp(line,"VisualAutoShootPickup: ", 22))
				{
					std::sscanf(line, "VisualAutoShootPickup: %u %u", &uw, &ux);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].autoShootPickup = ux;
				}
				elf(!strncmp(line,"VisualEnv: ", 10))
				{
					std::sscanf(line, "VisualEnv: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].environmentAmount = x;
				}
				elf(!strncmp(line,"VisualDead: ", 11))
				{
					std::sscanf(line, "VisualDead: %u %s", &uw, &lineChar);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].changeTo = lineChar[0];
				}		
				elf(!strncmp(line,"VisualPos: ", 10))
				{
					std::sscanf(line, "VisualPos: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].transform = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualOriginRotate: ", 6+6+7))
				{
					std::sscanf(line, "VisualOriginRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].originRotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualScale: ", 6+6))
				{
					std::sscanf(line, "VisualScale: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].transformScale = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualRotateRandomAngle: ", 6+6+6+6))
				{
					std::sscanf(line, "VisualRotateRandomAngle: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].rotateRandomAngle = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualRotate: ", 6+7))
				{
					std::sscanf(line, "VisualRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].rotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualSpeedRotate: ", 6+5+7))
				{
					std::sscanf(line, "VisualSpeedRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].speedRotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"VisualDiffuseMult: ", 18))
				{
					std::sscanf(line, "VisualDiffuseMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].diffuseMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].diffuseMultEqualOne = false;
					}
				}				  
				elf(!strncmp(line,"VisualAmbientMult: ", 18))
				{
					std::sscanf(line, "VisualAmbientMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].ambientMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].ambientMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"VisualEmissiveMult: ", 19))
				{
					std::sscanf(line, "VisualEmissiveMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].emissiveMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].emissiveMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"VisualSpecularMult: ", 19))
				{
					std::sscanf(line, "VisualSpecularMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].specularMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].specularMultEqualOne = false;
					}
				}				
				elf(!strncmp(line,"VisualShinyMult: ", 17))
				{
					std::sscanf(line, "VisualShinyMult: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].shinyMult = x;
					if(x != 1.0f)
					{
						tempLevels[i].listVisual[uw].shinyMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"VisualDiffuseAdd: ", 17))
				{
					std::sscanf(line, "VisualDiffuseAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].diffuseAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].diffuseAddEqualZero = false;
					}
				}				  
				elf(!strncmp(line,"VisualAmbientAdd: ", 17))
				{
					std::sscanf(line, "VisualAmbientAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].ambientAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].ambientAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"VisualEmissiveAdd: ", 18))
				{
					std::sscanf(line, "VisualEmissiveAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].emissiveAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].emissiveAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"VisualSpecularAdd: ", 18))
				{
					std::sscanf(line, "VisualSpecularAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].specularAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listVisual[uw].specularAddEqualZero = false;
					}
				}				
				elf(!strncmp(line,"VisualShinyAdd: ", 16))
				{
					std::sscanf(line, "VisualShinyAdd: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listVisual, uw);
					tempLevels[i].listVisual[uw].shinyAdd = x;
					if(x != 1.0f)
					{
						tempLevels[i].listVisual[uw].shinyAddEqualZero = false;
					}
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"BackgroundVisual", 10+6))
			{
				if(!strncmp(line,"BackgroundVisualFilenameDiffuse: ", 10+22))
				{
					std::sscanf(line, "BackgroundVisualFilenameDiffuse: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.diffuseFilename = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualFilenameNormal: ", 10+21))
				{
					std::sscanf(line, "BackgroundVisualFilenameNormal: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.normalFilename = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualFilenameSpecular: ", 10+23))
				{
					std::sscanf(line, "BackgroundVisualFilenameSpecular: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.specularFilename = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualFilenameEmissive: ", 10+23))
				{
					std::sscanf(line, "BackgroundVisualFilenameEmissive: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.emissiveFilename = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualFilenameModel: ", 10+20))
				{
					std::sscanf(line, "BackgroundVisualFilenameModel: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.modelFilename = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualTextureFilter: ", 10+6+7+7))
				{
					std::sscanf(line, "BackgroundVisualTextureFilter: %u %s", &uw, &lineChar);	
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);					
					tempLevels[i].listBackgroundVisual[uw].object.textureStringFilterMag = lineChar;
					tempLevels[i].listBackgroundVisual[uw].object.textureStringFilterMin = lineChar;	
				}	
				elf(!strncmp(line,"BackgroundVisualTextureFilterMag: ", 10+6+7+7+3))
				{
					std::sscanf(line, "BackgroundVisualTextureFilterMag: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.textureStringFilterMag = lineChar;
				}		
				elf(!strncmp(line,"BackgroundVisualTextureFilterMin: ", 10+6+7+7+3))
				{
					std::sscanf(line, "BackgroundVisualTextureFilterMin: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.textureStringFilterMin = lineChar;
				}	
				elf(!strncmp(line,"BackgroundVisualTextureWrap: ", 10+6+7+5))
				{
					std::sscanf(line, "BackgroundVisualTextureWrap: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.textureStringWrapTexU = lineChar;
					tempLevels[i].listBackgroundVisual[uw].object.textureStringWrapTexV = lineChar;
				}	
				elf(!strncmp(line,"BackgroundVisualTextureWrapU: ", 10+6+7+6))
				{
					std::sscanf(line, "BackgroundVisualTextureWrapU: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.textureStringWrapTexU = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualTextureWrapV: ", 10+6+7+6))
				{
					std::sscanf(line, "BackgroundVisualTextureWrapV: %u %s", &uw, &lineChar);
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].object.textureStringWrapTexV = lineChar;
				}
				elf(!strncmp(line,"BackgroundVisualEnv: ", 10+10))
				{
					std::sscanf(line, "BackgroundVisualEnv: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].environmentAmount = x;
				}	
				elf(!strncmp(line,"BackgroundVisualPos: ", 10+10))
				{
					std::sscanf(line, "BackgroundVisualPos: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].transform = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"BackgroundVisualDiffuseMult: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualDiffuseMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].diffuseMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].diffuseMultEqualOne = false;
					}
				}				  
				elf(!strncmp(line,"BackgroundVisualAmbientMult: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualAmbientMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].ambientMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].ambientMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualEmissiveMult: ", 10+19))
				{
					std::sscanf(line, "BackgroundVisualEmissiveMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].emissiveMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].emissiveMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualSpecularMult: ", 10+19))
				{
					std::sscanf(line, "BackgroundVisualSpecularMult: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].specularMult = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].specularMultEqualOne = false;
					}
				}				
				elf(!strncmp(line,"BackgroundVisualShinyMult: ", 10+17))
				{
					std::sscanf(line, "BackgroundVisualShinyMult: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].shinyMult = x;
					if(x != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].shinyMultEqualOne = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualDiffuseAdd: ", 10+17))
				{
					std::sscanf(line, "BackgroundVisualDiffuseAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].diffuseAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].diffuseAddEqualZero = false;
					}
				}				  
				elf(!strncmp(line,"BackgroundVisualAmbientAdd: ", 10+17))
				{
					std::sscanf(line, "BackgroundVisualAmbientAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].ambientAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].ambientAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualEmissiveAdd: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualEmissiveAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].emissiveAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].emissiveAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualSpecularAdd: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualSpecularAdd: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].specularAdd = satColor(x, y, z);
					if(x != 1.0f || y != 1.0f || z != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].specularAddEqualZero = false;
					}
				}				
				elf(!strncmp(line,"BackgroundVisualShinyAdd: ", 10+16))
				{
					std::sscanf(line, "BackgroundVisualShinyAdd: %u %f", &uw, &x);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].shinyAdd = x;
					if(x != 1.0f)
					{
						tempLevels[i].listBackgroundVisual[uw].shinyAddEqualZero = false;
					}
				}
				elf(!strncmp(line,"BackgroundVisualScale: ", 10+12))
				{
					std::sscanf(line, "BackgroundVisualScale: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].transformScale = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"BackgroundVisualRotate: ", 10+13))
				{
					std::sscanf(line, "BackgroundVisualRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].rotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"BackgroundVisualRotateSpeed: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualRotateSpeed: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].speedRotate = satVec3(x, y, z);
				}					
				elf(!strncmp(line,"BackgroundVisualRangeRotate: ", 10+18))
				{
					std::sscanf(line, "BackgroundVisualRangeRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].rangeRotate = satVec3(x, y, z);
				}						
				elf(!strncmp(line,"BackgroundVisualRangeSpeedRotate: ", 10+23))
				{
					std::sscanf(line, "BackgroundVisualRangeSpeedRotate: %u %f %f %f", &uw, &x, &y, &z);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].rangeSpeedRotate = satVec3(x, y, z);
				}	
				elf(!strncmp(line,"BackgroundVisualLoop: ", 10+11))
				{
					std::sscanf(line, "BackgroundVisualLoop: %u %f %f", &uw, &x, &y);		
					sat::level::Resize(tempLevels[i].listBackgroundVisual, uw);
					tempLevels[i].listBackgroundVisual[uw].appearLoop = satVec2(x, y);
				}	
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"Size: ", 5))
			{
				std::sscanf(line, "Size: %i %i", &ix, &iy);
				tempLevels[i].X = ix;
				tempLevels[i].Y = iy;		
			}
			elf(!strncmp(line,"WinPoint: ", 8))
			{
				std::sscanf(line, "WinPoint: %f", &x);
				tempLevels[i].WinPoint = x-6.0f;
			}
			elf(!strncmp(line,"Seed:", 5))
			{
				std::sscanf(line, "Seed: %u", &uw);
				tempLevels[i].Seed = uw;
			}
			elf(!strncmp(line,"Filename", 8))
			{
				std::cout << "LINE " << lineNumber << "\tGOT DEPRECATED FUNCTION: " << line << std::endl;
				
				if(!strncmp(line,"FilenameDiffuse: ", 16))
				{
					std::sscanf(line, "FilenameDiffuse: %s", &lineChar);
					tempLevels[i].Block.diffuseFilename = lineChar;
					//tempLevels[i].Block.LoadDiffuse();
					//tempLevels[i].BlockDiffuseTextureFilename = lineChar;
				}
				elf(!strncmp(line,"FilenameNormal: ", 15))
				{
					std::sscanf(line, "FilenameNormal: %s", &lineChar);
					tempLevels[i].Block.normalFilename = lineChar;
					//tempLevels[i].Block.LoadNormal();
					//tempLevels[i].BlockNormalTextureFilename = lineChar;
				}
				elf(!strncmp(line,"FilenameSpecular: ", 17))
				{
					std::sscanf(line, "FilenameSpecular: %s", &lineChar);
					tempLevels[i].Block.specularFilename = lineChar;
					//tempLevels[i].Block.LoadSpecular();
					//tempLevels[i].BlockSpecularTextureFilename = lineChar;
				}
				elf(!strncmp(line,"FilenameEmissive: ", 17))
				{
					std::sscanf(line, "FilenameEmissive: %s", &lineChar);
					tempLevels[i].Block.emissiveFilename = lineChar;
					//tempLevels[i].Block.LoadEmissive();
					//tempLevels[i].BlockEmissiveTextureFilename = lineChar;
				}
				elf(!strncmp(line,"FilenameModel: ", 14))
				{
					std::sscanf(line, "FilenameModel: %s", &lineChar);
					tempLevels[i].Block.modelFilename = lineChar;
					//tempLevels[i].Block.LoadModel();
					//tempLevels[i].BlockModelFilename = lineChar;
				}
				else
				{
					std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
				}	
			}
			elf(!strncmp(line,"//", 2))
			{
			
			}
			elf(!strncmp(line,";", 1))
			{
			
			}
			elf(line[0] == '\t')
			{
				//std::cout << "Empty Line " << std::endl;
			}
			elf(line[0] == ' ')
			{
				//std::cout << "Empty Line " << std::endl;
			}
			elf(line[0] == 0)
			{
				//std::cout << "Empty Line " << std::endl;
			}
			else
			{
				//std::cout << strcmp(line,"RimLight:") << std::endl;
				std::cout << "LINE " << lineNumber << "\tGOT NUTTIN: " << line << std::endl;
			}
		}

		if(i > 0 && tempLevels[i].SongFilename == tempLevels[i-1].SongFilename)
		{
			tempLevels[i].SongRepeat = true;
		}
		else
		{
			//tempLevels[i].Song.loadFromFile(tempLevels[i].SongFilename, false, true);
		}
			
		if(GlobalSettings::getSongSettings().Override)
		{
			tempLevels[i].SongFilename = GlobalSettings::getSongSettings().Filename;
			if(i > 0)
				tempLevels[i].SongRepeat = true;
		}

		tempLevels[i].listLight.push_back(tempLevels[i].listLight[tempLevels[i].listLight.size() - 1]);
		for(int fogLayerNum = 0; fogLayerNum < tempLevels[i].listFog.size(); ++fogLayerNum)
		{			
			tempLevels[i].listFog[fogLayerNum].FogLayer.push_back(tempLevels[i].listFog[fogLayerNum].FogLayer[tempLevels[i].listFog[fogLayerNum].FogLayer.size() - 1]);
		}		
		tempLevels[i].listEye.push_back(tempLevels[i].listEye[tempLevels[i].listEye.size() - 1]);
		tempLevels[i].listWave.push_back(tempLevels[i].listWave[tempLevels[i].listWave.size() - 1]);
		tempLevels[i].listSpeed.push_back(tempLevels[i].listSpeed[tempLevels[i].listSpeed.size() - 1]);
		tempLevels[i].listSound.push_back(tempLevels[i].listSound[tempLevels[i].listSound.size() - 1]);
		tempLevels[i].listBloom.push_back(tempLevels[i].listBloom[tempLevels[i].listBloom.size() - 1]);
		tempLevels[i].listContrast.push_back(tempLevels[i].listContrast[tempLevels[i].listContrast.size() - 1]);
		tempLevels[i].listBlockColor.push_back(tempLevels[i].listBlockColor[tempLevels[i].listBlockColor.size() - 1]);
		tempLevels[i].listEnvironment.push_back(tempLevels[i].listEnvironment[tempLevels[i].listEnvironment.size() - 1]);
		tempLevels[i].listShipLight.push_back(tempLevels[i].listShipLight[tempLevels[i].listShipLight.size() - 1]);
		tempLevels[i].listGreyscale.push_back(tempLevels[i].listGreyscale[tempLevels[i].listGreyscale.size() - 1]);
		tempLevels[i].listDOF.push_back(tempLevels[i].listDOF[tempLevels[i].listDOF.size() - 1]);
		tempLevels[i].listScanline.push_back(tempLevels[i].listScanline[tempLevels[i].listScanline.size() - 1]);
		tempLevels[i].listSobel.push_back(tempLevels[i].listSobel[tempLevels[i].listSobel.size() - 1]);
		tempLevels[i].listSonar.push_back(tempLevels[i].listSonar[tempLevels[i].listSonar.size() - 1]);
		tempLevels[i].listShadow.push_back(tempLevels[i].listShadow[tempLevels[i].listShadow.size() - 1]);
		tempLevels[i].listMusic.push_back(tempLevels[i].listMusic[tempLevels[i].listMusic.size() - 1]);

		//tempLevels[i].Block.Load();
		inFile.close();
	}


	return tempLevels;
}







Level::Level(const std::string* filename)
{
	this->filename = filename;
	this->Y = 0;
	this->X = 0;

	LevelScore = 0;

	//blockRandomToggle = 0;
	//blockRandomToggleAmbientAdd = 0;
	//blockRandomToggleAmbientMult = 0;
	//blockRandomToggleDiffuseAdd = 0;
	//blockRandomToggleDiffuseMult = 0;
	//blockRandomToggleSpecularAdd = 0;
	//blockRandomToggleSpecularMult = 0;
	//blockRandomToggleEmissiveAdd = 0;
	//blockRandomToggleEmissiveMult = 0;
	//blockRandomToggleShinyAdd = 0;
	//blockRandomToggleShinyMult = 0;
	//0 is first value, 1 is ordered, 2 is random  

	Block.diffuse = 0;
	Block.normal = 0;
	Block.specular = 0;
	Block.emissive = 0;
	Block.model = 0;

	Block.diffuseFilename = "../Assets/Textures/Default/defaultTexture.png";
	Block.normalFilename = "../Assets/Textures/Default/defaultNormal.png";
	Block.specularFilename = "../Assets/Textures/Default/defaultSpecular.png";
	Block.emissiveFilename = "../Assets/Textures/Default/defaultEmissive.png";
	Block.modelFilename = "../Assets/Models/Default/defaultBlockLong.obj";

	
	//"../Assets/Textures/Default/defaultShiny.png"
	
	FogActive = true;	
	WaveActive = false;
	BloomActive = true;
	ContrastActive = false;
	DOFActive = true;
	ScanlineActive = false;
	SSAOActive = true;

	BlockWavyActive = true;
	BlockCascadeActive = true;

	this->WinPoint = -1;

	SongRepeat = false;
	SongFilename = "../Assets/Sound/Music/MainThemeMerge.mp3";
	SongTempo = 120.0f;
	SongBounce = 1.5f;

	LevelTime = 0.0f;

}

Level::~Level()
{
	//delete BlockDiffuseTexture;
	//delete BlockNormalTexture;
	//delete BlockSpecularTexture;
	//delete BlockEmissiveTexture;
	//delete BlockModel;
}

void Level::Load()
{
	//Block.Load();


	for(unsigned i = 0; i < listVisual.size(); ++i)
	{
		for(unsigned k = 0; k < listVisual[i].object.size(); ++k)
		{
			listVisual[i].object[k].Load();
		}
	}

	for(unsigned i = 0; i < listBackgroundVisual.size(); ++i)
	{		
		listBackgroundVisual[i].object.Load();
	}

	for(unsigned i = 0; i < listEnemy.size(); ++i)
	{
		for(unsigned k = 0; k < listEnemy[i].object.size(); ++k)
		{
			listEnemy[i].object[k].Load();
		}
	}
		
	for(unsigned i = 0; i < FogLayers; ++i)
	{
		for(unsigned c = 0; c < listFog[i].FogLayer.size(); ++c)
		{
			listFog[i].FogLayer[c].image = ResourceManager::AddTexture(listFog[i].FogLayer[c].imageFilename);
			//std::cout << "loaded fog image " << listFog[i].FogLayer[c].image->TexObj << "\t" << listFog[i].FogLayer[c].imageFilename;
			//std::cout << std::endl;
		}
	}
	//LoadLevelSounds();
}

void Level::LoadLevelSounds()
{
	for(unsigned i = 0; i < listSound.size(); ++i)
	{
		listSound[i].sound.loadFromFile(listSound[i].soundFilename, false, false);
	}

	ResetLevelSounds();
}

void Level::Unload()
{
	//Block.Load();
	//UnloadLevelSounds();
}

void Level::UnloadLevelSounds()
{
	for(unsigned i = 0; i < listSound.size(); ++i)
	{
		listSound[i].sound.release();
	}
}

void Level::ResetLevelSounds()
{
	for(unsigned i = 0; i < listSound.size(); ++i)
	{
		if(listSound[i].soundBlank == false)
		{
			listSound[i].soundPlayed = false;
		}
	}
}

void Level::setBlockType(Mesh* model, Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive)
{
	Block.diffuse = diffuse;
	Block.normal = normal;
	Block.specular = specular;
	Block.emissive = emissive;
	Block.model = model;
}

void Level::readLevel(const std::string *fileName)
{
	// Open a file
	std::ifstream infile(*fileName + ".txt", std::ios::binary);
	assert(infile && "'LEVEL.txt' NOT FOUND");

	if(WinPoint == -1)
	{
		WinPoint = X - 20;
	}

	if (infile)
	{

		char tempChar;
		infile >> tempChar; infile >> tempChar;

		//std::cout << "Reading from file " << *fileName << ".txt" << std::endl;

		//The actual reading of the file
		lMatrix.resize(Y);
		for (int i = 0; i < Y; ++i)
		{
			lMatrix[i].resize(X);
			//infile.getline(tempChar, length);
			for (int j = 0; j < X; )
			{
				infile >> tempChar;
				if (tempChar != '\t'
					&& tempChar > 0)
				{		//Excel places 'tabs' between each character (horizontally) in the grid. This makes sure they are not entered.
					this->lMatrix[i][j] = tempChar;
					//Below are a bunch of saves for number individual level elements
					//if (tempChar == 'O') ++block_Top;
					//else if (tempChar == 'X') ++block_Front;
					//else if (tempChar == 'Z') ++block_Bottom;
					//else if (tempChar == 'H') ++block_Full;
					//else if (tempChar == 'D') ++e_Die;
					//else if (tempChar == 'C') ++e_Chess_King;
					////else if (tempChar == 'W') j = length;
					//else
					//{
					//	
					//}
					++j;
				}
				//std::cout << (int)tempChar << " " << tempChar << " " << (int)tempChar << "\n";
			}
		}


		//for (int i = 0; i < length; ++i)
		//{
		//	//infile.getline(tempChar, length);
		//	for (int j = 0; j < height; ++j)
		//	{
		//		std::cout << lMatrix[j][i];
		//	}
		//	std::cout << std::endl;
		//}

		infile.close();

		SetMatrixConditions();
	}
}

void Level::readLevel(const std::string *fileName, const int &w, const int &h)
{
// Open a file
	X = w;
	Y = h;

	std::ifstream infile(*fileName, std::ios::binary);
	assert(infile && "'LEVEL.txt' NOT FOUND");


	if (infile)
	{

		char tempChar;
		infile >> tempChar; infile >> tempChar;

		//std::cout << "Reading from file " << *fileName << std::endl;

		//The actual reading of the file
		lMatrix.resize(X);
		for (int i = 0; i < Y; ++i)
		{
			lMatrix[i].resize(X);
			//infile.getline(tempChar, length);
			for (int j = 0; j < X; )
			{
				infile >> tempChar;
				if (tempChar != '\t'
					&& tempChar > 0)
				{		//Excel places 'tabs' between each character (horizontally) in the grid. This makes sure they are not entered.
					this->lMatrix[i][j] = tempChar;
					//Below are a bunch of saves for number individual level elements
					//if (tempChar == 'O') ++block_Top;
					//else if (tempChar == 'X') ++block_Front;
					//else if (tempChar == 'Z') ++block_Bottom;
					//else if (tempChar == 'H') ++block_Full;
					//else if (tempChar == 'D') ++e_Die;
					//else if (tempChar == 'C') ++e_Chess_King;
					////else if (tempChar == 'W') j = length;
					//else
					//{
					//	
					//}
					++j;
				}
				//std::cout << (int)tempChar << " " << tempChar << " " << (int)tempChar << "\n";
			}
		}


		//for (int i = 0; i < length; ++i)
		//{
		//	//infile.getline(tempChar, length);
		//	for (int j = 0; j < height; ++j)
		//	{
		//		std::cout << lMatrix[j][i];
		//	}
		//	std::cout << std::endl;
		//}

		infile.close();
	}
}

void Level::removeStuff()
{
	for (unsigned int i = 0; i < lMatrix.size(); ++i)
	{
		for (unsigned int j = 0; j < lMatrix[i].size(); ++j)
		{
			//if (lMatrix[i][j] == 'S')
			//{
			//	lMatrix[i][j] = 'D';
			//}
			////else if (lMatrix[i][j] == 'C')
			////{
			////	lMatrix[i][j] = 'D';
			////}
			//else if (lMatrix[i][j] == 'P')
			//{
			//	lMatrix[i][j] = 'D';
			//}
			//else if (lMatrix[i][j] == 'B')
			//{
			//	lMatrix[i][j] = 'D';
			//}
			//else if (lMatrix[i][j] == 'G')
			//{
			//	lMatrix[i][j] = 'D';
			//}
			//else if (lMatrix[i][j] == 'Y')
			//{
			//	lMatrix[i][j] = '-';
			//}
			//else if (lMatrix[i][j] == 'M')
			//{
			//	lMatrix[i][j] = 'D';
			//}
			//else if (lMatrix[i][j] == 'R')
			//{
			//	lMatrix[i][j] = 'D';
			//}

		}
	}
}

void Level::SetMatrixConditions()
{
	lCollisionMatrix.resize(Y);
	lBreakableMatrix.resize(Y);
	lKillMatrix.resize(Y);
	lShootableMatrix.resize(Y);
	lChangeToMatrix.resize(Y);
	lHealthMatrix.resize(Y);
	lInvincibleMatrix.resize(Y);
	lDoubleShotMatrix.resize(Y);
	lAutoShotMatrix.resize(Y);
	lRandomMatrix.resize(Y);
	
	int randomNumber = rand();
	srand(0);

	for (unsigned int i = 0; i < Y; ++i)
	{
		lCollisionMatrix[i].resize(X);
		lBreakableMatrix[i].resize(X);
		lShootableMatrix[i].resize(X);
		lKillMatrix[i].resize(X);
		lChangeToMatrix[i].resize(X);
		lHealthMatrix[i].resize(X);
		lInvincibleMatrix[i].resize(X);
		lDoubleShotMatrix[i].resize(X);	
		lAutoShotMatrix[i].resize(X);			
		lRandomMatrix[i].resize(X);
		for (unsigned int j = 0; j < X; ++j)
		{
			SetMatrixCondition(i, j);
		}
	}
	
	srand(randomNumber);
}

void Level::SetMatrixCondition(const int &i, const int &j)
{
	lHealthMatrix[i][j] = false;
	lCollisionMatrix[i][j] = false;
	lBreakableMatrix[i][j] = false;
	lShootableMatrix[i][j] = false;
	lKillMatrix[i][j] = false;
	lChangeToMatrix[i][j] = '-';
	lInvincibleMatrix[i][j] = false;
	lDoubleShotMatrix[i][j] = false;
	lAutoShotMatrix[i][j] = false;
		
	lRandomMatrix[i][j].resize(8);
	for(unsigned int k = 0; k < 8; ++k)
	{
		lRandomMatrix[i][j][k] = rand() + Seed;
	}

	for(unsigned int k = 0; k < listEnemy.size(); ++k)
	{
		if(lMatrix[i][j] == listEnemy[k].character)
		{
			if(lCollisionMatrix[i][j] == false)
			{
				lCollisionMatrix[i][j] = listEnemy[k].collidable;
			}

			if(lBreakableMatrix[i][j] == false)
			{
				lBreakableMatrix[i][j] = listEnemy[k].breakable;
			}

			if(lShootableMatrix[i][j] == false)
			{
				lShootableMatrix[i][j] = listEnemy[k].shootable;
			}

			if(lKillMatrix[i][j] == false)
			{
				lKillMatrix[i][j] = listEnemy[k].killer;
			}
								
			lChangeToMatrix[i][j] = listEnemy[k].changeTo;					
		}
	}

	for(unsigned int k = 0; k < listVisual.size(); ++k)
	{
		if(lMatrix[i][j] == listVisual[k].character)
		{
			if(lCollisionMatrix[i][j] == false)
			{
				lCollisionMatrix[i][j] = listVisual[k].collidable;
			}

			if(lBreakableMatrix[i][j] == false)
			{
				lBreakableMatrix[i][j] = listVisual[k].breakable;
			}

			if(lShootableMatrix[i][j] == false)
			{
				lShootableMatrix[i][j] = listVisual[k].shootable;
			}

			if(lKillMatrix[i][j] == false)
			{
				lKillMatrix[i][j] = listVisual[k].killer;
			}

			if(lHealthMatrix[i][j] == false)
			{
				lHealthMatrix[i][j] = listVisual[k].healable;
			}

			if(lInvincibleMatrix[i][j] == false)
			{
				lInvincibleMatrix[i][j] = listVisual[k].invinciblePickup;
			}

			if(lDoubleShotMatrix[i][j] == false)
			{
				lDoubleShotMatrix[i][j] = listVisual[k].doubleShootPickup;
			}
			
			if(lAutoShotMatrix[i][j] == false)
			{
				lAutoShotMatrix[i][j] = listVisual[k].autoShootPickup;
			}

			lChangeToMatrix[i][j] = listVisual[k].changeTo;
		}
	}

	

	
}

void Level::ChangeTo(satVec2 pos)
{
	lMatrix[pos.x][pos.y] = lChangeToMatrix[pos.x][pos.y];
	SetMatrixCondition(pos.x, pos.y);
}




void Level::setShipPosition(const float &p)
{
	shipPos = p;
}

#pragma region Calculations

#pragma region CalcLight

satColor Level::calcAmbient()
{
	if(shipPos > listLight[listLight.size() - 1].pos)
	{
		return listLight[listLight.size() - 1].lightAmbient;
	}

	for(unsigned i = 1; i < listLight.size(); ++i)
	{
		if(shipPos < listLight[i].pos)
		{
			return Lerp(listLight[i-1].lightAmbient, listLight[i].lightAmbient, 
				InverseLerp(shipPos, listLight[i-1].pos, listLight[i].pos));
		}
	}

	if(listLight.size() <= 3)
	{
		return listLight[0].lightAmbient;
	}
	return listLight[1].lightAmbient;
}

satColor Level::calcDiffuse()
{
	if(shipPos > listLight[listLight.size() - 1].pos)
	{
		return listLight[listLight.size() - 1].lightDiffuse;
	}
	
	for(unsigned i = 1; i < listLight.size(); ++i)
	{
		if(shipPos < listLight[i].pos)
		{
			return Lerp(listLight[i-1].lightDiffuse, listLight[i].lightDiffuse, 
				InverseLerp(shipPos, listLight[i-1].pos, listLight[i].pos));
		}
	}
	
	if(listLight.size() <= 3)
	{
		return listLight[0].lightDiffuse;
	}
	return listLight[1].lightDiffuse;
}

satColor Level::calcSpecular()
{
	if(shipPos > listLight[listLight.size() - 1].pos)
	{
		return listLight[listLight.size() - 1].lightSpecular;
	}

	for(unsigned i = 1; i < listLight.size(); ++i)
	{
		if(shipPos < listLight[i].pos)
		{
			return Lerp(listLight[i-1].lightSpecular, listLight[i].lightSpecular, 
				InverseLerp(shipPos, listLight[i-1].pos, listLight[i].pos));
		}
	}

	if(listLight.size() <= 3)
	{
		return listLight[0].lightSpecular;
	}
	return listLight[1].lightSpecular;
}

satColor Level::calcRim()
{
	if(shipPos > listLight[listLight.size() - 1].pos)
	{
		return listLight[listLight.size() - 1].lightRim;
	}

	for(unsigned i = 1; i < listLight.size(); ++i)
	{
		if(shipPos < listLight[i].pos)
		{
			return Lerp(listLight[i-1].lightRim, listLight[i].lightRim, 
				InverseLerp(shipPos, listLight[i-1].pos, listLight[i].pos));
		}
	}

	if(listLight.size() <= 3)
	{
		return listLight[0].lightRim;
	}
	return listLight[1].lightRim;
}

#pragma endregion CalcLight

#pragma region CalcFog

satColor Level::calcFogColor(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogColor;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogColor, 
				listFog[layer].FogLayer[i    ].FogColor, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogColor, 
				listFog[layer].FogLayer[i - 1].FogColor, 
				listFog[layer].FogLayer[i    ].FogColor, 
				listFog[layer].FogLayer[i + 1].FogColor,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogColor;
	}
	return listFog[layer].FogLayer[1].FogColor;

	//if(shipPos > listFog[listFog.size() - 1].pos)
	//{
	//	return listFog[listFog.size() - 1].FogColor;
	//}
	//
	//for(unsigned i = 1; i < listFog.size(); ++i)
	//{
	//	if(shipPos < listFog[i].pos)
	//	{
	//		return Lerp(listFog[i-1].FogColor, listFog[i].FogColor, 
	//			InverseLerp(shipPos, listFog[i-1].pos, listFog[i].pos));
	//	}
	//}
	//
	//if(listFog.size() <= 3)
	//{
	//	return listFog[0].FogColor;
	//}
	//return listFog[1].FogColor;
}

float Level::calcFogNear(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogNear;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogNear, 
				listFog[layer].FogLayer[i    ].FogNear, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogNear, 
				listFog[layer].FogLayer[i - 1].FogNear, 
				listFog[layer].FogLayer[i    ].FogNear, 
				listFog[layer].FogLayer[i + 1].FogNear,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogNear;
	}
	return listFog[layer].FogLayer[1].FogNear;
}

float Level::calcFogFar(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogFar;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogFar, 
				listFog[layer].FogLayer[i    ].FogFar, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogFar, 
				listFog[layer].FogLayer[i - 1].FogFar, 
				listFog[layer].FogLayer[i    ].FogFar, 
				listFog[layer].FogLayer[i + 1].FogFar,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogFar;
	}
	return listFog[layer].FogLayer[1].FogFar;
}


satVec2		Level::calcFogRange(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogRange;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogRange, 
				listFog[layer].FogLayer[i    ].FogRange, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogRange, 
				listFog[layer].FogLayer[i - 1].FogRange, 
				listFog[layer].FogLayer[i    ].FogRange, 
				listFog[layer].FogLayer[i + 1].FogRange,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogRange;
	}
	return listFog[layer].FogLayer[1].FogRange;
}


satColor	Level::calcFogBottomColor(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogBottomColor;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogBottomColor, 
				listFog[layer].FogLayer[i    ].FogBottomColor, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogBottomColor, 
				listFog[layer].FogLayer[i - 1].FogBottomColor, 
				listFog[layer].FogLayer[i    ].FogBottomColor, 
				listFog[layer].FogLayer[i + 1].FogBottomColor,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogBottomColor;
	}
	return listFog[layer].FogLayer[1].FogBottomColor;
}
	
satColor	Level::calcFogTopColor(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogTopColor;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogTopColor, 
				listFog[layer].FogLayer[i    ].FogTopColor, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogTopColor, 
				listFog[layer].FogLayer[i - 1].FogTopColor, 
				listFog[layer].FogLayer[i    ].FogTopColor, 
				listFog[layer].FogLayer[i + 1].FogTopColor,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogTopColor;
	}
	return listFog[layer].FogLayer[1].FogTopColor;
}

satVec3		Level::calcFogPosMult(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogPosMult;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogPosMult, 
				listFog[layer].FogLayer[i    ].FogPosMult, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogPosMult, 
				listFog[layer].FogLayer[i - 1].FogPosMult, 
				listFog[layer].FogLayer[i    ].FogPosMult, 
				listFog[layer].FogLayer[i + 1].FogPosMult,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogPosMult;
	}
	return listFog[layer].FogLayer[1].FogPosMult;
}

float	Level::calcFogEmissivePass(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].FogEmissivePass;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].FogEmissivePass, 
				listFog[layer].FogLayer[i    ].FogEmissivePass, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].FogEmissivePass, 
				listFog[layer].FogLayer[i - 1].FogEmissivePass, 
				listFog[layer].FogLayer[i    ].FogEmissivePass, 
				listFog[layer].FogLayer[i + 1].FogEmissivePass,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].FogEmissivePass;
	}
	return listFog[layer].FogLayer[1].FogEmissivePass;
}


Texture*	Level::calcFogTex0(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].image;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return listFog[layer].FogLayer[i - 1].image;

		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].image;
	}
	return listFog[layer].FogLayer[1].image;
}

Texture*	Level::calcFogTex1(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].image;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return listFog[layer].FogLayer[i].image;

		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].image;
	}
	return listFog[layer].FogLayer[1].image;
}

float		Level::calcFogLerp(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return 0.0f;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos);
		}
	}

	if (fogLayerSize < 3)
	{
		return 0.0f;
	}
	return 0.0f;
}



satVec2		Level::calcFogWaveIntensity(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].waveIntensity;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].waveIntensity, 
				listFog[layer].FogLayer[i    ].waveIntensity, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].waveIntensity, 
				listFog[layer].FogLayer[i - 1].waveIntensity, 
				listFog[layer].FogLayer[i    ].waveIntensity, 
				listFog[layer].FogLayer[i + 1].waveIntensity,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].waveIntensity;
	}
	return listFog[layer].FogLayer[1].waveIntensity;
}
satVec2		Level::calcFogWaveCount(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].waveCount;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].waveCount, 
				listFog[layer].FogLayer[i    ].waveCount, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].waveCount, 
				listFog[layer].FogLayer[i - 1].waveCount, 
				listFog[layer].FogLayer[i    ].waveCount, 
				listFog[layer].FogLayer[i + 1].waveCount,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].waveCount;
	}
	return listFog[layer].FogLayer[1].waveCount;
}

satVec2		Level::calcFogWaveTime(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].waveTime;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].waveTime, 
				listFog[layer].FogLayer[i    ].waveTime, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].waveTime, 
				listFog[layer].FogLayer[i - 1].waveTime, 
				listFog[layer].FogLayer[i    ].waveTime, 
				listFog[layer].FogLayer[i + 1].waveTime,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].waveTime;
	}
	return listFog[layer].FogLayer[1].waveTime;
}

satVec2		Level::calcFogVelocity(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].velocity;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].velocity, 
				listFog[layer].FogLayer[i    ].velocity, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].velocity, 
				listFog[layer].FogLayer[i - 1].velocity, 
				listFog[layer].FogLayer[i    ].velocity, 
				listFog[layer].FogLayer[i + 1].velocity,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].velocity;
	}
	return listFog[layer].FogLayer[1].velocity;
}

satVec2		Level::calcFogOffset(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].offset;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].offset, 
				listFog[layer].FogLayer[i    ].offset, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].offset, 
				listFog[layer].FogLayer[i - 1].offset, 
				listFog[layer].FogLayer[i    ].offset, 
				listFog[layer].FogLayer[i + 1].offset,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].offset;
	}
	return listFog[layer].FogLayer[1].offset;
}

satVec2		Level::calcFogOffsetMult(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].offsetMult;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].offsetMult, 
				listFog[layer].FogLayer[i    ].offsetMult, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].offsetMult, 
				listFog[layer].FogLayer[i - 1].offsetMult, 
				listFog[layer].FogLayer[i    ].offsetMult, 
				listFog[layer].FogLayer[i + 1].offsetMult,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].offsetMult;
	}
	return listFog[layer].FogLayer[1].offsetMult;
}

satVec2		Level::calcFogOffsetPosMult(const int &layer)
{
	unsigned fogLayerSize = listFog[layer].FogLayer.size() - 1;

	if (shipPos > listFog[layer].FogLayer[fogLayerSize].pos)
	{
		return listFog[layer].FogLayer[fogLayerSize].offsetPosMult;
	}

	for (unsigned i = 2; i < fogLayerSize; ++i)
	{
		if (shipPos < listFog[layer].FogLayer[i].pos)
		{
			return Lerp(
				listFog[layer].FogLayer[i - 1].offsetPosMult, 
				listFog[layer].FogLayer[i    ].offsetPosMult, 
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));

			return Catmull(
				listFog[layer].FogLayer[i - 2].offsetPosMult, 
				listFog[layer].FogLayer[i - 1].offsetPosMult, 
				listFog[layer].FogLayer[i    ].offsetPosMult, 
				listFog[layer].FogLayer[i + 1].offsetPosMult,
				InverseLerp(shipPos, 
				listFog[layer].FogLayer[i - 1].pos, 
				listFog[layer].FogLayer[i    ].pos));
		}
	}

	if (fogLayerSize < 3)
	{
		return listFog[layer].FogLayer[0].offsetPosMult;
	}
	return listFog[layer].FogLayer[1].offsetPosMult;
}


#pragma endregion CalcFog

#pragma region CalcEye

satVec2 Level::calcEyeShift()
{
	if(shipPos > listEye[listEye.size() - 1].pos)
	{
		return listEye[listEye.size() - 1].EyeShift;
	}

	for(unsigned i = 1; i < listEye.size(); ++i)
	{
		if(shipPos < listEye[i].pos)
		{
			return Lerp(listEye[i-1].EyeShift, listEye[i].EyeShift, 
				InverseLerp(shipPos, listEye[i-1].pos, listEye[i].pos));
		}
	}

	if(listEye.size() <= 3)
	{
		return listEye[0].EyeShift;
	}
	return listEye[1].EyeShift;
}

float Level::calcEyeDistanceMult()
{
	if(shipPos > listEye[listEye.size() - 1].pos)
	{
		return listEye[listEye.size() - 1].EyeDistanceMult;
	}

	for(unsigned i = 1; i < listEye.size(); ++i)
	{
		if(shipPos < listEye[i].pos)
		{
			return Lerp(listEye[i-1].EyeDistanceMult, listEye[i].EyeDistanceMult, 
				InverseLerp(shipPos, listEye[i-1].pos, listEye[i].pos));
		}
	}

	if(listEye.size() <= 3)
	{
		return listEye[0].EyeDistanceMult;
	}
	return listEye[1].EyeDistanceMult;
}

float Level::calcEyeConvergence()
{
	if(shipPos > listEye[listEye.size() - 1].pos)
	{
		return listEye[listEye.size() - 1].Convergence;
	}

	for(unsigned i = 1; i < listEye.size(); ++i)
	{
		if(shipPos < listEye[i].pos)
		{
			return Lerp(listEye[i-1].Convergence, listEye[i].Convergence, 
				InverseLerp(shipPos, listEye[i-1].pos, listEye[i].pos));
		}
	}

	if(listEye.size() <= 3)
	{
		return listEye[0].Convergence;
	}
	return listEye[1].Convergence;
}

#pragma endregion CalcEye


#pragma region CalcWave

satVec2 Level::calcWaveIntensity()
{
	if(shipPos > listWave[listWave.size() - 1].pos)
	{
		return listWave[listWave.size() - 1].intensity;
	}

	for(unsigned i = 1; i < listWave.size(); ++i)
	{
		if(shipPos < listWave[i].pos)
		{
			return Lerp(listWave[i-1].intensity, listWave[i].intensity, 
				InverseLerp(shipPos, listWave[i-1].pos, listWave[i].pos));
		}
	}

	if(listWave.size() <= 3)
	{
		return listWave[0].intensity;
	}
	return listWave[1].intensity;
}

satVec2 Level::calcWaveCount()
{
	if(shipPos > listWave[listWave.size() - 1].pos)
	{
		return listWave[listWave.size() - 1].count;
	}

	for(unsigned i = 1; i < listWave.size(); ++i)
	{
		if(shipPos < listWave[i].pos)
		{
			return Lerp(listWave[i-1].count, listWave[i].count, 
				InverseLerp(shipPos, listWave[i-1].pos, listWave[i].pos));
		}
	}

	if(listWave.size() <= 3)
	{
		return listWave[0].count;
	}
	return listWave[1].count;
}

satVec2 Level::calcWaveTime()
{
	if(shipPos > listWave[listWave.size() - 1].pos)
	{
		return listWave[listWave.size() - 1].time;
	}

	for(unsigned i = 1; i < listWave.size(); ++i)
	{
		if(shipPos < listWave[i].pos)
		{
			return Lerp(listWave[i-1].time, listWave[i].time, 
				InverseLerp(shipPos, listWave[i-1].pos, listWave[i].pos));
		}
	}

	if(listWave.size() <= 3)
	{
		return listWave[0].time;
	}
	return listWave[1].time;
}

#pragma endregion CalcWave

#pragma region CalcSpeed

satVec2 Level::calcSpeedMult()
{
	if(shipPos > listSpeed[listSpeed.size() - 1].pos)
	{
		return listSpeed[listSpeed.size() - 1].mult;
	}

	for(unsigned i = 1; i < listSpeed.size(); ++i)
	{
		if(shipPos < listSpeed[i].pos)
		{
			return Lerp(listSpeed[i-1].mult, listSpeed[i].mult, 
				InverseLerp(shipPos, listSpeed[i-1].pos, listSpeed[i].pos));
		}
	}

	if(listSpeed.size() <= 3)
	{
		return listSpeed[0].mult;
	}
	return listSpeed[1].mult;
}

float Level::calcSpeedXMult()
{
	if(shipPos > listSpeed[listSpeed.size() - 1].pos)
	{
		return listSpeed[listSpeed.size() - 1].mult.x;
	}

	for(unsigned i = 1; i < listSpeed.size(); ++i)
	{
		if(shipPos < listSpeed[i].pos)
		{
			return Lerp(listSpeed[i-1].mult.x, listSpeed[i].mult.x, 
				InverseLerp(shipPos, listSpeed[i-1].pos, listSpeed[i].pos));
		}
	}

	if(listSpeed.size() <= 3)
	{
		return listSpeed[0].mult.x;
	}
	return listSpeed[1].mult.x;
}

float Level::calcSpeedYMult()
{
	if(shipPos > listSpeed[listSpeed.size() - 1].pos)
	{
		return listSpeed[listSpeed.size() - 1].mult.y;
	}

	for(unsigned i = 1; i < listSpeed.size(); ++i)
	{
		if(shipPos < listSpeed[i].pos)
		{
			return Lerp(listSpeed[i-1].mult.y, listSpeed[i].mult.y, 
				InverseLerp(shipPos, listSpeed[i-1].pos, listSpeed[i].pos));
		}
	}

	if(listSpeed.size() <= 3)
	{
		return listSpeed[0].mult.y;
	}
	return listSpeed[1].mult.y;
}


satVec4 Level::calcSpeed()
{
	if(shipPos > listSpeed[listSpeed.size() - 1].pos)
	{
		return listSpeed[listSpeed.size() - 1].moving;
	}

	for(unsigned i = 2; i < listSpeed.size() - 1; ++i)
	{
		if(shipPos < listSpeed[i].pos)
		{
			if(listSpeed[i-1].useCatmull)
				return 
					Catmull(
					listSpeed[i - 2].moving, 
					listSpeed[i - 1].moving, 
					listSpeed[i    ].moving, 
					listSpeed[i + 1].moving,
					InverseLerp(shipPos, 
					listSpeed[i - 1].pos, 
					listSpeed[i    ].pos));
			else
				return 
				Lerp(
				listSpeed[i-1].moving, 
				listSpeed[i  ].moving, 
				InverseLerp(shipPos, 
				listSpeed[i-1].pos, 
				listSpeed[i  ].pos));

			//return Lerp(listSpeed[i-1].moving, listSpeed[i].moving, 
			//	InverseLerp(shipPos, listSpeed[i-1].pos, listSpeed[i].pos));
		}
	}

	if(listSpeed.size() <= 3)
	{
		return listSpeed[0].moving;
	}
	return listSpeed[1].moving;
}

satVec2 Level::calcSpeedShrinkMult()
{
	if(shipPos > listSpeed[listSpeed.size() - 1].pos)
	{
		return listSpeed[listSpeed.size() - 1].shrink;
	}

	for(unsigned i = 1; i < listSpeed.size(); ++i)
	{
		if(shipPos < listSpeed[i].pos)
		{
			return Lerp(listSpeed[i-1].shrink, listSpeed[i].shrink, 
				InverseLerp(shipPos, listSpeed[i-1].pos, listSpeed[i].pos));
		}
	}

	if(listSpeed.size() <= 3)
	{
		return listSpeed[0].shrink;
	}
	return listSpeed[1].shrink;
}

#pragma endregion CalcSpeed

#pragma region CalcGreyscale

float	Level::calcGreyscaleAmount()
{
	if(shipPos > listGreyscale[listGreyscale.size() - 1].pos)
	{
		return listGreyscale[listGreyscale.size() - 1].amount;
	}

	for(unsigned i = 1; i < listGreyscale.size(); ++i)
	{
		if(shipPos < listGreyscale[i].pos)
		{
			return Lerp(listGreyscale[i-1].amount, listGreyscale[i].amount, 
				InverseLerp(shipPos, listGreyscale[i-1].pos, listGreyscale[i].pos));
		}
	}

	if(listGreyscale.size() <= 3)
	{
		return listGreyscale[0].amount;
	}
	return listGreyscale[1].amount;
}

float	Level::calcGreyscaleSepiaAmount()
{
	if (shipPos > listGreyscale[listGreyscale.size() - 1].pos)
	{
		return listGreyscale[listGreyscale.size() - 1].amountSepia;
	}

	for (unsigned i = 1; i < listGreyscale.size(); ++i)
	{
		if (shipPos < listGreyscale[i].pos)
		{
			return Lerp(listGreyscale[i - 1].amountSepia, listGreyscale[i].amountSepia,
				InverseLerp(shipPos, listGreyscale[i - 1].pos, listGreyscale[i].pos));
		}
	}

	if (listGreyscale.size() <= 3)
	{
		return listGreyscale[0].amountSepia;
	}
	return listGreyscale[1].amountSepia;
}

#pragma endregion CalcGreyscale


#pragma region		CalcSound

void Level::calcSoundPlay()
{
	if(shipPos > listSound[listSound.size() - 1].pos)
	{
		if(listSound[listSound.size() - 1].soundPlayed == false && listSound[listSound.size() - 1].soundBlank == false)
		{
			//std::cout << shipPos << "\tPlaying Sound\t" << listSound[listSound.size() - 1].pos << "\t" << listSound[listSound.size() - 1].soundFilename << std::endl;
			listSound[listSound.size() - 1].sound.playSound(listSound[listSound.size() - 1].volume, listSound[listSound.size() - 1].frequencyMult, listSound[listSound.size() - 1].frequencyRandom);
			listSound[listSound.size() - 1].soundPlayed = true;
		}
	}

	for(unsigned i = 1; i < listSound.size(); ++i)
	{
		if(shipPos > listSound[i].pos)
		{
			if(listSound[i].soundPlayed == false && listSound[i].soundBlank == false)
			{
				//std::cout << shipPos << "\tPlaying Sound\t" << listSound[i].pos << "\t" << listSound[i].soundFilename << std::endl;
				listSound[i].sound.playSound(listSound[i].volume, listSound[i].frequencyMult, listSound[i].frequencyRandom);
				listSound[i].soundPlayed = true;
			}
		}
	}

	//std::cout << "Calculating Sound" << std::endl;

	//if(listSound.size() <= 3)
	//{
	//	return listSound[0].shrink;
	//}
	//return listSound[1].shrink;
}

#pragma endregion	CalcSound

#pragma region		CalcBloom

bool Level::calcBloomActive()
{
	return BloomActive;
}

float Level::calcBloomUpperThreshold()
{
	if(shipPos > listBloom[listBloom.size() - 1].pos)
	{
		return listBloom[listBloom.size() - 1].bloomThresholdUpper;
	}

	for(unsigned i = 1; i < listBloom.size(); ++i)
	{
		if(shipPos < listBloom[i].pos)
		{
			return Lerp(listBloom[i-1].bloomThresholdUpper, listBloom[i].bloomThresholdUpper, 
				InverseLerp(shipPos, listBloom[i-1].pos, listBloom[i].pos));
		}
	}

	if(listBloom.size() <= 3)
	{
		return listBloom[0].bloomThresholdUpper;
	}
	return listBloom[1].bloomThresholdUpper;
}

float Level::calcBloomLowerThreshold()
{
	if(shipPos > listBloom[listBloom.size() - 1].pos)
	{
		return listBloom[listBloom.size() - 1].bloomThresholdLower;
	}

	for(unsigned i = 1; i < listBloom.size(); ++i)
	{
		if(shipPos < listBloom[i].pos)
		{
			return Lerp(listBloom[i-1].bloomThresholdLower, listBloom[i].bloomThresholdLower, 
				InverseLerp(shipPos, listBloom[i-1].pos, listBloom[i].pos));
		}
	}

	if(listBloom.size() <= 3)
	{
		return listBloom[0].bloomThresholdLower;
	}
	return listBloom[1].bloomThresholdLower;
}

#pragma endregion	CalcBloom

#pragma region	CalcMusic


float	Level::calcMusicPitch()
{
	if(shipPos > listMusic[listMusic.size() - 1].pos)
	{
		return listMusic[listMusic.size() - 1].pitchMult;
	}

	for(unsigned i = 1; i < listMusic.size(); ++i)
	{
		if(shipPos < listMusic[i].pos)
		{
			return Lerp(listMusic[i-1].pitchMult, listMusic[i].pitchMult, 
				InverseLerp(shipPos, listMusic[i-1].pos, listMusic[i].pos));
		}
	}

	if(listMusic.size() <= 3)
	{
		return listMusic[0].pitchMult;
	}
	return listMusic[1].pitchMult;
}

float	Level::calcMusicVolume()
{
	if(shipPos > listMusic[listMusic.size() - 1].pos)
	{
		return listMusic[listMusic.size() - 1].volumeMult;
	}

	for(unsigned i = 1; i < listMusic.size(); ++i)
	{
		if(shipPos < listMusic[i].pos)
		{
			return Lerp(listMusic[i-1].volumeMult, listMusic[i].volumeMult, 
				InverseLerp(shipPos, listMusic[i-1].pos, listMusic[i].pos));
		}
	}

	if(listMusic.size() <= 3)
	{
		return listMusic[0].volumeMult;
	}
	return listMusic[1].volumeMult;
}

#pragma endregion	CalcMusic

#pragma region		CalcContrast

bool Level::calcContrastActive()
{
	return ContrastActive;
}

float Level::calcContrastAmount()
{
	if(shipPos > listContrast[listContrast.size() - 1].pos)
	{
		return listContrast[listContrast.size() - 1].contrastAmount;
	}

	for(unsigned i = 1; i < listContrast.size(); ++i)
	{
		if(shipPos < listContrast[i].pos)
		{
			return Lerp(listContrast[i-1].contrastAmount, listContrast[i].contrastAmount, 
				InverseLerp(shipPos, listContrast[i-1].pos, listContrast[i].pos));
		}
	}

	if(listContrast.size() <= 3)
	{
		return listContrast[0].contrastAmount;
	}
	return listContrast[1].contrastAmount;
}




#pragma endregion	CalcContrast

#pragma region		CalcEnvironment

float	Level::calcEnvironmentBackground()
{
	if(shipPos > listEnvironment[listEnvironment.size() - 1].pos)
	{
		return listEnvironment[listEnvironment.size() - 1].backgroundAmount;
	}

	for(unsigned i = 1; i < listEnvironment.size(); ++i)
	{
		if(shipPos < listEnvironment[i].pos)
		{
			return Lerp(listEnvironment[i-1].backgroundAmount, listEnvironment[i].backgroundAmount, 
				InverseLerp(shipPos, listEnvironment[i-1].pos, listEnvironment[i].pos));
		}
	}

	if(listEnvironment.size() <= 3)
	{
		return listEnvironment[0].backgroundAmount;
	}
	return listEnvironment[1].backgroundAmount;
}

#pragma endregion	CalcEnvironment

#pragma region		CalcBlockColors

const float xRanMult = 4.35f;
const float yRanMult = 5.75f;

LightAddMult Level::calcBlockMaterial(int x, int y)
{
	if(Toggle.blockRandomToggle == 0)
	{
		return listBlockColor[0].material;
	}
	else if(Toggle.blockRandomToggle == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].material;
	}
	else if(Toggle.blockRandomToggle == 2)
	{
		//srand((x+1) * (y+1) + x);
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].material;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default;
}

satColor	Level::calcBlockAmbientAdd(int x, int y)
{
	if(Toggle.blockRandomToggleAmbientAdd == 0)
	{
		return listBlockColor[0].ambientAdd;
	}
	else if(Toggle.blockRandomToggleAmbientAdd == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].ambientAdd;
	}
	else if(Toggle.blockRandomToggleAmbientAdd == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].ambientAdd;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.ambientAdd;
}

satColor	Level::calcBlockAmbientMult(int x, int y)
{
	if(Toggle.blockRandomToggleAmbientMult == 0)
	{
		return listBlockColor[0].ambientMult;
	}
	else if(Toggle.blockRandomToggleAmbientMult == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].ambientMult;
	}
	else if(Toggle.blockRandomToggleAmbientMult == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].ambientMult;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.ambientMult;
}

satColor	Level::calcBlockDiffuseAdd(int x, int y)
{
	if(Toggle.blockRandomToggleDiffuseAdd == 0)
	{
		return listBlockColor[0].diffuseAdd;
	}
	else if(Toggle.blockRandomToggleDiffuseAdd == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].diffuseAdd;
	}
	else if(Toggle.blockRandomToggleDiffuseAdd == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].diffuseAdd;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.diffuseAdd;
}

satColor	Level::calcBlockDiffuseMult(int x, int y)
{
	if(Toggle.blockRandomToggleDiffuseMult == 0)
	{
		return listBlockColor[0].diffuseMult;
	}
	else if(Toggle.blockRandomToggleDiffuseMult == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].diffuseMult;
	}
	else if(Toggle.blockRandomToggleDiffuseMult == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].diffuseMult;
	}

	std::cout << "uhhhhhhhh " << Toggle.blockRandomToggleDiffuseMult << std::endl;
	return sat::light::mod::Default.diffuseMult;
}

satColor	Level::calcBlockSpecularAdd(int x, int y)
{
	if(Toggle.blockRandomToggleSpecularAdd == 0)
	{
		return listBlockColor[0].specularAdd;
	}
	else if(Toggle.blockRandomToggleSpecularAdd == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].specularAdd;
	}
	else if(Toggle.blockRandomToggleSpecularAdd == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].specularAdd;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.specularAdd;
}

satColor	Level::calcBlockSpecularMult(int x, int y)
{
	if(Toggle.blockRandomToggleSpecularMult == 0)
	{
		return listBlockColor[0].specularMult;
	}
	else if(Toggle.blockRandomToggleSpecularMult == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].specularMult;
	}
	else if(Toggle.blockRandomToggleSpecularMult == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].specularMult;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.specularMult;
}

satColor	Level::calcBlockEmissiveAdd(int x, int y)
{
	if(Toggle.blockRandomToggleEmissiveAdd == 0)
	{
		return listBlockColor[0].emissiveAdd;
	}
	else if(Toggle.blockRandomToggleEmissiveAdd == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].emissiveAdd;
	}
	else if(Toggle.blockRandomToggleEmissiveAdd == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].emissiveAdd;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.emissiveAdd;
}

satColor	Level::calcBlockEmissiveMult(int x, int y)
{
	if(Toggle.blockRandomToggleEmissiveMult == 0)
	{
		return listBlockColor[0].emissiveMult;
	}
	else if(Toggle.blockRandomToggleEmissiveMult == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].emissiveMult;
	}
	else if(Toggle.blockRandomToggleEmissiveMult == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].emissiveMult;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.emissiveMult;
}

float		Level::calcBlockShinyAdd(int x, int y)
{
	if(Toggle.blockRandomToggleShinyAdd == 0)
	{
		return listBlockColor[0].shinyAdd;
	}
	else if(Toggle.blockRandomToggleShinyAdd == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].shinyAdd;
	}
	else if(Toggle.blockRandomToggleShinyAdd == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].shinyAdd;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.shinyAdd;
}

float		Level::calcBlockShinyMult(int x, int y)
{
	if(Toggle.blockRandomToggleShinyMult == 0)
	{
		return listBlockColor[0].shinyMult;
	}
	else if(Toggle.blockRandomToggleShinyMult == 1)
	{
		int num = (int)(((x)* 1.0f) + ((y) * 1.0f)) % listBlockColor.size();
		return listBlockColor[num].shinyMult;
	}
	else if(Toggle.blockRandomToggleShinyMult == 2)
	{
		srand(lRandomMatrix[x][y][0]);
		int num = rand() % listBlockColor.size();
		return listBlockColor[num].shinyMult;
	}

	std::cout << "uhhhhhhhh" << std::endl;
	return sat::light::mod::Default.shinyMult;
}


#pragma endregion	CalcBlockColors

#pragma region		CalcBlockColorsToggle

	bool	Level::calcBlockMaterialToggle()
	{
		return Toggle.blockRandomToggle > 0;
	}

	bool	Level::calcBlockAmbientAddToggle()
	{
		return Toggle.blockRandomToggleAmbientAdd > 0;
	}

	bool	Level::calcBlockAmbientMultToggle()
	{
		return Toggle.blockRandomToggleAmbientMult > 0;
	}

	bool	Level::calcBlockDiffuseAddToggle()
	{
		return Toggle.blockRandomToggleDiffuseAdd > 0;
	}

	bool	Level::calcBlockDiffuseMultToggle()
	{
		return Toggle.blockRandomToggleDiffuseMult > 0;
	}

	bool	Level::calcBlockSpecularAddToggle()
	{
		return Toggle.blockRandomToggleSpecularAdd > 0;
	}

	bool	Level::calcBlockSpecularMultToggle()
	{
		return Toggle.blockRandomToggleSpecularMult > 0;
	}

	bool	Level::calcBlockEmissiveAddToggle()
	{
		return Toggle.blockRandomToggleEmissiveAdd > 0;
	}

	bool	Level::calcBlockEmissiveMultToggle()
	{
		return Toggle.blockRandomToggleEmissiveMult > 0;
	}

	bool	Level::calcBlockShinyAddToggle()
	{
		return Toggle.blockRandomToggleShinyAdd > 0;
	}

	bool	Level::calcBlockShinyMultToggle()
	{
		return Toggle.blockRandomToggleShinyMult > 0;
	}


#pragma endregion	CalcBlockColorsToggle

#pragma region		CalcShipLight

float Level::calcShipLightMultiplier()
{
	if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].colorMultiplier;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].colorMultiplier, listShipLight[i].colorMultiplier, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].colorMultiplier;
	}
	return listShipLight[1].colorMultiplier;
}

float Level::calcShipLightConstant()
{
		if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].constant;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].constant, listShipLight[i].constant, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].constant;
	}
	return listShipLight[1].constant;
}

float Level::calcShipLightLinear()
{
	if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].linear;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].linear, listShipLight[i].linear, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].linear;
	}
	return listShipLight[1].linear;
}

float Level::calcShipLightQuad()
{
		if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].quad;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].quad, listShipLight[i].quad, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].quad;
	}
	return listShipLight[1].quad;
}

float Level::calcShipLightRadius()
{
		if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].radius;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].radius, listShipLight[i].radius, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].radius;
	}
	return listShipLight[1].radius;
}

float Level::calcShipLightInnerRadius()
{
		if(shipPos > listShipLight[listShipLight.size() - 1].pos)
	{
		return listShipLight[listShipLight.size() - 1].innerRadius;
	}

	for(unsigned i = 1; i < listShipLight.size(); ++i)
	{
		if(shipPos < listShipLight[i].pos)
		{
			return Lerp(listShipLight[i-1].innerRadius, listShipLight[i].innerRadius, 
				InverseLerp(shipPos, listShipLight[i-1].pos, listShipLight[i].pos));
		}
	}

	if(listShipLight.size() <= 3)
	{
		return listShipLight[0].innerRadius;
	}
	return listShipLight[1].innerRadius;
}

#pragma endregion	CalcShipLight

#pragma region		CalcSong
#pragma endregion	CalcSong

#pragma region	CalcDOF

float Level::calcDOFFocus()
{
	if(shipPos > listDOF[listDOF.size() - 1].pos)
	{
		return listDOF[listDOF.size() - 1].focus;
	}

	for(unsigned i = 1; i < listDOF.size(); ++i)
	{
		if(shipPos < listDOF[i].pos)
		{
			return Lerp(listDOF[i-1].focus, listDOF[i].focus, 
				InverseLerp(shipPos, listDOF[i-1].pos, listDOF[i].pos));
		}
	}

	if(listDOF.size() <= 3)
	{
		return listDOF[0].focus;
	}
	return listDOF[1].focus;
}

float Level::calcDOFClamp()
{
	if(shipPos > listDOF[listDOF.size() - 1].pos)
	{
		return listDOF[listDOF.size() - 1].clamp * listDOF[listDOF.size() - 1].clampMult;
	}

	for(unsigned i = 1; i < listDOF.size(); ++i)
	{
		if(shipPos < listDOF[i].pos)
		{
			return Lerp(listDOF[i-1].clamp * listDOF[i-1].clampMult, listDOF[i].clamp * listDOF[i].clampMult, 
				InverseLerp(shipPos, listDOF[i-1].pos, listDOF[i].pos));
		}
	}

	if(listDOF.size() <= 3)
	{
		return listDOF[0].clamp * listDOF[0].clampMult;
	}
	return listDOF[1].clamp * listDOF[1].clampMult;
}

float Level::calcDOFBias()
{
	if(shipPos > listDOF[listDOF.size() - 1].pos)
	{
		return listDOF[listDOF.size() - 1].bias * listDOF[listDOF.size() - 1].biasMult;
	}

	for(unsigned i = 1; i < listDOF.size(); ++i)
	{
		if(shipPos < listDOF[i].pos)
		{
			return Lerp(listDOF[i-1].bias * listDOF[i-1].biasMult, listDOF[i].bias * listDOF[i].biasMult, 
				InverseLerp(shipPos, listDOF[i-1].pos, listDOF[i].pos));
		}
	}

	if(listDOF.size() <= 3)
	{
		return listDOF[0].bias * listDOF[0].biasMult;
	}
	return listDOF[1].bias * listDOF[1].biasMult;
}


#pragma endregion	CalcDOF



#pragma region		CalcScanline

satVec2 Level::calcScanlineAmount()
{
	if(shipPos > listScanline[listScanline.size() - 1].pos)
	{
		return listScanline[listScanline.size() - 1].Amount;
	}

	for(unsigned i = 1; i < listScanline.size(); ++i)
	{
		if(shipPos < listScanline[i].pos)
		{
			return Lerp(listScanline[i-1].Amount, listScanline[i].Amount, 
				InverseLerp(shipPos, listScanline[i-1].pos, listScanline[i].pos));
		}
	}

	if(listScanline.size() <= 3)
	{
		return listScanline[0].Amount;
	}
	return listScanline[1].Amount;
}

int Level::calcScanlineThresholdX()
{
	if(shipPos > listScanline[listScanline.size() - 1].pos)
	{
		return listScanline[listScanline.size() - 1].ThresholdX;
	}

	for(unsigned i = 1; i < listScanline.size(); ++i)
	{
		if(shipPos < listScanline[i].pos)
		{
			return Lerp(listScanline[i-1].ThresholdX, listScanline[i].ThresholdX, 
				InverseLerp(shipPos, listScanline[i-1].pos, listScanline[i].pos));
		}
	}

	if(listScanline.size() <= 3)
	{
		return listScanline[0].ThresholdX;
	}
	return listScanline[1].ThresholdX;
}

int Level::calcScanlineThresholdY()
{
	if(shipPos > listScanline[listScanline.size() - 1].pos)
	{
		return listScanline[listScanline.size() - 1].ThresholdY;
	}

	for(unsigned i = 1; i < listScanline.size(); ++i)
	{
		if(shipPos < listScanline[i].pos)
		{
			return Lerp(listScanline[i-1].ThresholdY, listScanline[i].ThresholdY, 
				InverseLerp(shipPos, listScanline[i-1].pos, listScanline[i].pos));
		}
	}

	if(listScanline.size() <= 3)
	{
		return listScanline[0].ThresholdY;
	}
	return listScanline[1].ThresholdY;
}

int Level::calcScanlineModX()
{
	if(shipPos > listScanline[listScanline.size() - 1].pos)
	{
		return listScanline[listScanline.size() - 1].ModX;
	}

	for(unsigned i = 1; i < listScanline.size(); ++i)
	{
		if(shipPos < listScanline[i].pos)
		{
			return Lerp(listScanline[i-1].ModX, listScanline[i].ModX, 
				InverseLerp(shipPos, listScanline[i-1].pos, listScanline[i].pos));
		}
	}

	if(listScanline.size() <= 3)
	{
		return listScanline[0].ModX;
	}
	return listScanline[1].ModX;
}

int Level::calcScanlineModY()
{
	if(shipPos > listScanline[listScanline.size() - 1].pos)
	{
		return listScanline[listScanline.size() - 1].ModY;
	}

	for(unsigned i = 1; i < listScanline.size(); ++i)
	{
		if(shipPos < listScanline[i].pos)
		{
			return Lerp(listScanline[i-1].ModY, listScanline[i].ModY, 
				InverseLerp(shipPos, listScanline[i-1].pos, listScanline[i].pos));
		}
	}

	if(listScanline.size() <= 3)
	{
		return listScanline[0].ModY;
	}
	return listScanline[1].ModY;
}

#pragma endregion	CalcScanline

#pragma region		CalcSobel

satVec2 Level::calcSobelThreshold()
{
	if(SobelThickToggle)
	{
		if(shipPos > listSobel[listSobel.size() - 1].pos)
		{
			return satVec2(listSobel[listSobel.size() - 1].normalThickThreshold, listSobel[listSobel.size() - 1].depthThickThreshold);
		}

		for(unsigned i = 1; i < listSobel.size(); ++i)
		{
			if(shipPos < listSobel[i].pos)
			{
				return satVec2(
					Lerp(listSobel[i-1].normalThickThreshold, listSobel[i].normalThickThreshold, 
					InverseLerp(shipPos, listSobel[i-1].pos, listSobel[i].pos)),
					Lerp(listSobel[i-1].depthThickThreshold, listSobel[i].depthThickThreshold, 
					InverseLerp(shipPos, listSobel[i-1].pos, listSobel[i].pos))					
					);
			}
		}

		if(listSobel.size() <= 3)
		{
			return satVec2(listSobel[0].normalThickThreshold, listSobel[0].depthThickThreshold);
		}
		
		return satVec2(listSobel[1].normalThickThreshold, listSobel[1].depthThickThreshold);
	}
	else
	{
		if(shipPos > listSobel[listSobel.size() - 1].pos)
		{
			return satVec2(listSobel[listSobel.size() - 1].normalThreshold, listSobel[listSobel.size() - 1].depthThreshold);
		}

		for(unsigned i = 1; i < listSobel.size(); ++i)
		{
			if(shipPos < listSobel[i].pos)
			{
				return satVec2(
					Lerp(listSobel[i-1].normalThreshold, listSobel[i].normalThreshold, 
					InverseLerp(shipPos, listSobel[i-1].pos, listSobel[i].pos)),
					Lerp(listSobel[i-1].depthThreshold, listSobel[i].depthThreshold, 
					InverseLerp(shipPos, listSobel[i-1].pos, listSobel[i].pos))					
					);
			}
		}

		if(listSobel.size() <= 3)
		{
			return satVec2(listSobel[0].normalThreshold, listSobel[0].depthThreshold);
		}
		
		return satVec2(listSobel[1].normalThreshold, listSobel[1].depthThreshold);
	}
}

#pragma endregion	CalcSobel

#pragma region		CalcSonar

satVec4 Level::calcSonarColor()
{
	if(shipPos > listSonar[listSonar.size() - 1].pos)
	{
		return listSonar[listSonar.size() - 1].colorAddMult;
	}

	for(unsigned i = 1; i < listSonar.size(); ++i)
	{
		if(shipPos < listSonar[i].pos)
		{
			return Lerp(listSonar[i-1].colorAddMult, listSonar[i].colorAddMult, 
				InverseLerp(shipPos, listSonar[i-1].pos, listSonar[i].pos));
		}
	}

	if(listSonar.size() <= 3)
	{
		return listSonar[0].colorAddMult;
	}
	return listSonar[1].colorAddMult;
}

satVec4 Level::calcSonarParam()
{
	if(shipPos > listSonar[listSonar.size() - 1].pos)
	{
		return listSonar[listSonar.size() - 1].param;
	}

	for(unsigned i = 1; i < listSonar.size(); ++i)
	{
		if(shipPos < listSonar[i].pos)
		{
			return Lerp(listSonar[i-1].param, listSonar[i].param, 
				InverseLerp(shipPos, listSonar[i-1].pos, listSonar[i].pos));
		}
	}

	if(listSonar.size() <= 3)
	{
		return listSonar[0].param;
	}
	return listSonar[1].param;
}

#pragma endregion	CalcSonar

#pragma region		CalcShadow

float		Level::calcShadowFOV()
{
	unsigned layer = 0;
	unsigned shadowLayerSize = listShadow.size() - 1;

	if (shipPos > listShadow[shadowLayerSize].pos)
	{
		return listShadow[shadowLayerSize].FOV;
	}

	for (unsigned i = 2; i < shadowLayerSize; ++i)
	{
		if (shipPos < listShadow[i].pos)
		{
			return Lerp(
				listShadow[i - 1].FOV, 
				listShadow[i    ].FOV, 
				InverseLerp(shipPos, 
				listShadow[i - 1].pos, 
				listShadow[i    ].pos));

			return Catmull(
				listShadow[i - 2].FOV, 
				listShadow[i - 1].FOV, 
				listShadow[i    ].FOV, 
				listShadow[i + 1].FOV,
				InverseLerp(shipPos, 
				listShadow[i - 1].pos, 
				listShadow[i    ].pos));
		}
	}

	if (shadowLayerSize < 3)
	{
		return listShadow[0].FOV;
	}
	return listShadow[1].FOV;
}

satVec3 Level::calcShadowRotation()
{
	
	
	if(shipPos > listShadow[listShadow.size() - 1].pos)
	{	
		satVec3 rotation;
		rotation = listShadow[listShadow.size() - 1].rotation + listShadow[listShadow.size() - 1].speedRotation * Game::TimeInLevel;
		rotation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[listShadow.size() - 1].rangeSpeedRotation, 0.0f, listShadow[listShadow.size() - 1].rangeRotation);
		return rotation;

		return listShadow[listShadow.size() - 1].rotation;
	}

	for(unsigned i = 1; i < listShadow.size(); ++i)
	{
		if(shipPos < listShadow[i].pos)
		{
			satVec3 rotation;
			rotation = 
				Catmull(
					listShadow[i - 2].rotation, 
					listShadow[i - 1].rotation, 
					listShadow[i    ].rotation, 
					listShadow[i + 1].rotation,
				InverseLerp(shipPos, 
					listShadow[i - 1].pos, 
					listShadow[i    ].pos)) + 				
				Game::TimeInLevel * Catmull(
					listShadow[i - 2].speedRotation, 
					listShadow[i - 1].speedRotation, 
					listShadow[i    ].speedRotation, 
					listShadow[i + 1].speedRotation,
				InverseLerp(shipPos, 
					listShadow[i - 1].pos, 
					listShadow[i    ].pos));
			rotation += 
				Timer::sinRange(0.0f, 
					Game::TimeInLevel * Catmull(
						listShadow[i - 2].rangeSpeedRotation, 
						listShadow[i - 1].rangeSpeedRotation, 
						listShadow[i    ].rangeSpeedRotation, 
						listShadow[i + 1].rangeSpeedRotation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos))
						, 0.0f, 
					Catmull(
						listShadow[i - 2].rangeRotation, 
						listShadow[i - 1].rangeRotation, 
						listShadow[i    ].rangeRotation, 
						listShadow[i + 1].rangeRotation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos)));

			return rotation;

			return Catmull(
				listShadow[i - 2].rotation, 
				listShadow[i - 1].rotation, 
				listShadow[i    ].rotation, 
				listShadow[i + 1].rotation,
				InverseLerp(shipPos, 
				listShadow[i - 1].pos, 
				listShadow[i    ].pos));

			return Lerp(listShadow[i-1].rotation, listShadow[i].rotation, 
				InverseLerp(shipPos, listShadow[i-1].pos, listShadow[i].pos));
		}
	}

	if(listShadow.size() <= 3)
	{
		satVec3 rotation;
		rotation = listShadow[0].rotation + listShadow[0].speedRotation * Game::TimeInLevel;
		rotation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[0].rangeSpeedRotation, 0.0f, listShadow[0].rangeRotation);
		return rotation;

		return listShadow[0].rotation;
	}
	return listShadow[1].rotation;
}

satVec3 Level::calcShadowTranslation()
{
	if(shipPos > listShadow[listShadow.size() - 1].pos)
	{
		satVec3 translation;
		translation = listShadow[listShadow.size() - 1].translation;
		translation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[listShadow.size() - 1].rangeSpeedTranslation, 0.0f, listShadow[listShadow.size() - 1].rangeTranslation);
		return translation;

		return listShadow[listShadow.size() - 1].translation;
	}

	for(unsigned i = 1; i < listShadow.size(); ++i)
	{
		if(shipPos < listShadow[i].pos)
		{
			satVec3 translation;
			translation = 
				Catmull(
					listShadow[i - 2].translation, 
					listShadow[i - 1].translation, 
					listShadow[i    ].translation, 
					listShadow[i + 1].translation,
				InverseLerp(shipPos, 
					listShadow[i - 1].pos, 
					listShadow[i    ].pos));
			translation += 
				Timer::sinRange(0.0f, 
					Game::TimeInLevel * Catmull(
						listShadow[i - 2].rangeSpeedTranslation, 
						listShadow[i - 1].rangeSpeedTranslation, 
						listShadow[i    ].rangeSpeedTranslation, 
						listShadow[i + 1].rangeSpeedTranslation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos))
						, 0.0f, 
					Catmull(
						listShadow[i - 2].rangeTranslation, 
						listShadow[i - 1].rangeTranslation, 
						listShadow[i    ].rangeTranslation, 
						listShadow[i + 1].rangeTranslation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos)));

			return translation;

			return Lerp(listShadow[i-1].translation, listShadow[i].translation, 
				InverseLerp(shipPos, listShadow[i-1].pos, listShadow[i].pos));
		}
	}

	if(listShadow.size() <= 3)
	{
		satVec3 translation;
		translation = listShadow[0].translation;
		translation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[0].rangeSpeedTranslation, 0.0f, listShadow[0].rangeTranslation);
		return translation;

		return listShadow[0].translation;
	}
	return listShadow[1].translation;
}

satVec3 Level::calcShadowTranslationRotation()
{
	if(shipPos > listShadow[listShadow.size() - 1].pos)
	{
		satVec3 translation;
		translation = listShadow[listShadow.size() - 1].translationRotation;
		translation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[listShadow.size() - 1].rangeSpeedTranslationRotation, 0.0f, listShadow[listShadow.size() - 1].rangeTranslationRotation);
		return translation;

		return listShadow[listShadow.size() - 1].translationRotation;
	}

	for(unsigned i = 1; i < listShadow.size(); ++i)
	{
		if(shipPos < listShadow[i].pos)
		{
			satVec3 rotation;
			rotation = 
				Catmull(
					listShadow[i - 2].translationRotation, 
					listShadow[i - 1].translationRotation, 
					listShadow[i    ].translationRotation, 
					listShadow[i + 1].translationRotation,
				InverseLerp(shipPos, 
					listShadow[i - 1].pos, 
					listShadow[i    ].pos));
			rotation += 
				Timer::sinRange(0.0f, 
					Game::TimeInLevel * Catmull(
						listShadow[i - 2].rangeSpeedTranslationRotation, 
						listShadow[i - 1].rangeSpeedTranslationRotation, 
						listShadow[i    ].rangeSpeedTranslationRotation, 
						listShadow[i + 1].rangeSpeedTranslationRotation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos))
						, 0.0f, 
					Catmull(
						listShadow[i - 2].rangeTranslationRotation, 
						listShadow[i - 1].rangeTranslationRotation, 
						listShadow[i    ].rangeTranslationRotation, 
						listShadow[i + 1].rangeTranslationRotation,
					InverseLerp(shipPos, 
						listShadow[i - 1].pos, 
						listShadow[i    ].pos)));

			return rotation;

			return Lerp(listShadow[i-1].translationRotation, listShadow[i].translationRotation, 
				InverseLerp(shipPos, listShadow[i-1].pos, listShadow[i].pos));
		}
	}

	if(listShadow.size() <= 3)
	{
		satVec3 translation;
		translation = listShadow[0].translationRotation;
		translation += Timer::sinRange(0.0f, Game::TimeInLevel * listShadow[0].rangeSpeedTranslationRotation, 0.0f, listShadow[0].rangeTranslationRotation);
		return translation;

		return listShadow[0].translationRotation;
	}
	return listShadow[1].translationRotation;
}

#pragma endregion	CalcShadow
	
#pragma region		CalcSomething
#pragma endregion	CalcSomething

#pragma region		CalcSomething
#pragma endregion	CalcSomething

#pragma region		CalcSomething
#pragma endregion	CalcSomething

#pragma endregion Calculations





satVec3 Level::calcRandomRotations(sat::level::ListVisual* visual, int x, int y)
{
	satVec3 rotation = 0.0f;

	if(visual->rotateRandomAngle.x > 0)
	{
		rotation.x += lRandomMatrix[x][y][1] % (int)visual->rotateRandomAngle.x * (360.0f / visual->rotateRandomAngle.x);
	}

	if(visual->rotateRandomAngle.y > 0)
	{
		rotation.y += lRandomMatrix[x][y][2] % (int)visual->rotateRandomAngle.y * (360.0f / visual->rotateRandomAngle.y);
	}

	if(visual->rotateRandomAngle.z > 0)
	{
		rotation.z += lRandomMatrix[x][y][3] % (int)visual->rotateRandomAngle.z * (360.0f / visual->rotateRandomAngle.z);
	}

	return rotation;
}






LevelMatrixBlock::LevelMatrixBlock()
{
	kill = false;
	health = false;
	invincible = false;
	doubleShoot = false;
	autoShoot = false;
	random.resize(1);
	random[0] = rand();
}

//T Level::displayLevel(){
//
//}