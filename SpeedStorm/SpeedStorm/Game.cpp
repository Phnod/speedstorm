/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2016 Stephen Thompson

===========================================================================
*/

#include "Game.h"
#include <random>
#include "ResourceManager.h"

//#include "Utilities.h"
//#include "Mesh.h"

#define tru TRUE
#define true FALSE


#define Quaternions CameraTransform
#define Rudolph GBuffer
//Special thanks to Jonathan Umar-Khitab

#define SongBeat (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel)

#define INPUT_FRAME_TIME 1.0f / Timer::framef()
#define FRAME_TIME FPU * Timer::framef()




satVec2 shiftAverage = 0.0f;



satVec3 shipTranslate = satVec3(-1.666666666f, -0.66666666f, 0.333333333f);
satVec3 shipTranslateOriginBack = satVec3(-1.666666666f * 0.5f, -0.66666666f, 0.333333333f);

bool horizontalCollide = false;
bool verticalCollide = false;
float shipWobbleSpeed = satMath::PI * 2.0f;
float IntroEndParam = 0.0f;

bool setUpSSAOKernel = false;
int numOfSSAOSamples = 16;

satVec3 colorGrain;


satMat4 biasMat4 = satMat4(
		0.5f, 0.0f, 0.0f, 0.5f,
		0.0f, 0.5f, 0.0f, 0.5f,
		0.0f, 0.0f, 0.5f, 0.5f,
		0.00f, 0.0f, 0.0f, 1.0f);







#pragma region VariablesToMessAroundWith

//3D stuff
float eyeDistance = 8.0f;
float eyeDistanceOrtho = 8.0f;
satVec2 eyeShift = 0.0f;
float eyeShiftOrtho = 0.0f;
float eyeConvergence = 30.0f;
float eyeConvergenceAdd = 0.0f;
float eyeConvergenceOrtho = 30.0f;
float eyeShiftShipY = 10.0f;
float eyeConvergenceY = 30.0f;

//float eyeShift = -1.5f;
//float eyeConvergence = 0.675f;

float NearPlane = 5.0f;
float FarPlane = 100.0f;

const double defaultScoreMult = 5.0;
const int defaultScore = 100000 / defaultScoreMult;
const double lightShipScoreMult = 10.0;

float tutorialFrameNumber = 0.0f;

std::vector<satVec3> ssaoKernel;

float cheatMult = 0.99f;
float cheatSuperMult = 0.25f;

satVec2 Game::ShipSpawnPosition = satVec2(0.0f, 3.0f);

satVec4 speedMod = 0;
satVec2 speedMult = 0;
satVec2 speedShrinkMult = 0;



float controllerDeadzone = 0.2f;
float controllerXDeadzone = 0.4f;

#pragma endregion VariablesToMessAroundWith

#pragma region blockCascadingVariables

const float blockPolynomialXMult = 0.00000000015;
const float blockPolynomialYMult = 0.0000175;
const float blockPolynomialZMult = 0.0000175;


const float blockPolynomialXExp = 8.0f;
const float blockPolynomialYExp = 4.0f;
const float blockPolynomialZExp = 5.0f;

float RenderWidthHalf = Game::RENDER_WIDTH * 0.5f;
float RenderHeightHalf = Game::RENDER_HEIGHT * 0.5f;
float blockXShiftParameter = 4;
float blockXShift = RenderWidthHalf - blockXShiftParameter;

float blockXSinMult = 2.27f * 0.25f;
float blockXWidthMult = 0.25f * 0.5f;

float blockYSinMult = 4.17f * 0.25f;
float blockYHeightMult = 0.6f;
//float blockYHeightMult = 1.5f;

float blockZSinMult = 5.0f;
float blockZSinAddMult = satMath::PI * 0.3333f * 0.5f;
float blockZDepthMult = 0.5f;

satVec2 averageInput = 0.0f;

satVec2 MoveSpeed = 0.0f;
satVec2 DieSpeedMult = 1.0f;

//satVec2 modifierRGBSplitRed = satVec2(
//	rand() % 60 - 30,
//	rand() % 60 - 30);
//
//satVec2 modifierRGBSplitGreen = satVec2(
//	rand() % 60 - 30,
//	rand() % 60 - 30);
//
//satVec2 modifierRGBSplitBlue = satVec2(
//	rand() % 60 - 30,
//	rand() % 60 - 30);



#pragma endregion blockCascadingVariables



satVec2 modifierRGBSplitRed;
satVec2 modifierRGBSplitGreen;
satVec2 modifierRGBSplitBlue;

#pragma region staticVariables

int Game::SettingsPerformance = 2;

bool Game::FullScreenActive = tru;

bool Game::PauseActive = false;
bool Game::TitleScreenActive = true;
bool Game::SpecialThanksActive = true;
bool Game::WinScreenActive = true;
bool Game::LoadingActive = tru;
float Game::TransitionTitle = 0.0f;
float Game::TransitionTitleEnd = 1.0f;
bool Game::SkipTutorial = false;

bool Game::SatDebugMode = tru;
int  Game::SatDebugDisplayMode = SAT_DEBUG_OUTPUT_SCENE;
int  Game::SatDebugDisplayModeSize = SAT_DEBUG_OUTPUT_SIZE; //SAT_DEBUG_OUTPUT
int  Game::SatDebugFrameRateLimit = 1;
bool Game::SatDebugAutoPauseWindowActive = tru;
bool Game::SatDebugDrawHUDActive = tru;
bool Game::SatDebugDeferredLightActive = tru;
bool Game::SatDebugShadowMapActive = tru;
bool Game::SatDebugTimerPauseActive = false;
bool Game::SatDebugWavyToggleActive = true;
bool Game::SatDebugCascadeToggleActive = true;
int Game::SatDebugCascadeReverseActive = 1;
bool Game::SatDebugShipWaggleToggleActive = tru;

bool Game::SatDebugDrawEnemyActive = tru;
bool Game::SatDebugDrawVisualActive = tru;
bool Game::SatDebugDrawBackgroundActive = tru;

float Game::SatDebugFOVMult = 1.0f;
bool Game::SatDebugForce16x9 = true;

bool Game::SatDebugInputLeftCtrl	= false;
bool Game::SatDebugInputRightCtrl	= false;
bool Game::SatDebugInputLeftAlt		= false;
bool Game::SatDebugInputRightAlt	= false;
bool Game::SatDebugInputLeftShift	= false;
bool Game::SatDebugInputRightShift	= false;

satVec2 Game::ControllerInputJoypadRightStick = 0.0f;
satVec2 Game::ControllerInputJoypadLeftStick = 0.0f;
float	Game::ControllerInputTrigger = 0.0f;
satVec2 Game::ControllerInputDigitalPad = 0.0f;
float	Game::ControllerInputButtonA = 0.0f;
float	Game::ControllerInputButtonB = 0.0f;
float	Game::ControllerInputButtonX = 0.0f;
float	Game::ControllerInputButtonY = 0.0f;
float	Game::ControllerInputButtonSelect = 0.0f;
float	Game::ControllerInputButtonStart = 0.0f;
float	Game::ControllerInputButtonRightBumper = 0.0f;
float	Game::ControllerInputButtonLeftBumper = 0.0f;
float	Game::ControllerInputButtonRightStickClick = 0.0f;
float	Game::ControllerInputButtonLeftStickClick = 0.0f;

//satVec2 Game::ControllerLeftStick = 0.0f;

bool Game::ControllerActive = 0;

bool Game::ThreeDeeActive = 0;
bool Game::UpScaleResolutionX = true;
bool Game::UpScaleResolutionY = true;
int	 Game::UpScaleShadowResolution = 3;
int	 Game::UpScaleShadowResolutionRange = 7;

bool Game::UpScaleSobelResolution = true;

bool Game::SatDebugAutoScrollActive = tru;
bool Game::SatDebugGodModeActive = true;
bool Game::SatDebugDrunkModeActive = true;
bool Game::SatDebugSluggishMovementActive = tru;

int		Game::LightIntensityMode = 0;
int		Game::LightIntensityModeSize = 8;
Light	Game::LightIntensity = sat::light::lit::medium;

//USED FOR SSAO (SCREEN SPACE AMBIENT OCCLUSION)
std::uniform_real_distribution<GLfloat> randomFloats(0.0, 1.0); // random floats between 0.0 - 1.0
std::default_random_engine generator;
GLuint noiseTexture;

GLuint ThreeDeeInterlaceTextureHandle = 0;
GLuint ThreeDeeInterlaceInvertTextureHandle = 0;

int Game::FULLSCREEN_WIDTH = 1920;
int Game::FULLSCREEN_HEIGHT = 1080;

//int Game::WINDOW_WIDTH = 1600;
//int Game::WINDOW_HEIGHT = 900;
int Game::WINDOW_WIDTH = 1280;
int Game::WINDOW_HEIGHT = 720;

int Game::RENDER_WIDTH = 30;
int Game::RENDER_HEIGHT = 12;

//double* Game::timerTotal;
//double* Game::timerOld;
//double* Game::timerFrame;

int Game::UPDATE_COUNTER = 0;

satVec2 Game::InputMove = 0.0f;

int Game::levelWinFrame = 0;

float Game::ShipShrink = 0.0f;
int Game::ShipHealth = 3;
satColor Game::ShipHealthColor[5] = 
{ 
	satColor(0.70f, 0.50f, 0.50f) * 0.75f,
	satColor(0.60f, 0.10f, 0.10f) * 0.75f,
	satColor(0.40f, 0.25f, 0.10f) * 0.75f,
	satColor(0.10f, 0.45f, 0.10f) * 0.75f,
	satColor(0.15f, 0.15f, 0.30f) * 0.75f
};

satColor invincible00	= 0.5f * satColor(1.00f, 0.20f, 0.20f);
satColor invincible01	= 0.5f * satColor(0.20f, 1.00f, 0.00f);
satColor invincible02	= 0.5f * satColor(0.25f, 0.25f, 1.50f); //satColor(0.20f, 1.00f, 0.00f);
satColor invincible03	= satColor(0.25f, 0.25f, 1.50f);
satColor invincible04	= satColor(0.25f, 0.25f, 1.50f);
satColor invincible05	= satColor(0.25f, 0.25f, 1.50f);
satColor invincible06	= satColor(0.25f, 0.25f, 1.50f);
satColor invincible07	= satColor(0.20f, 1.00f, 0.00f);
satColor invincible09	= satColor(0.80f, 0.50f, 0.20f);

satColor Game::ShipInvincibleColor[9] = 
{
	invincible01,
	invincible00,
	invincible02,
	invincible00,
	invincible01,
	invincible02,
	invincible01,
	invincible00,
	invincible02
};

//satColor invincible00	= { 1.00f, 0.00f, 0.00f };
//satColor invincible01	= { 0.00f, 1.00f, 0.00f };
//satColor invincible02	= { 0.25f, 0.25f, 1.50f };
//
//satColor Game::ShipInvincibleColor[9] = 
//{
//	invincible01,
//	invincible00,
//	invincible02,
//	invincible00,
//	invincible01,
//	invincible02,
//	invincible01,
//	invincible00,
//	invincible02
//};

GLuint Game::aviHandle;
Texture Game::aviTexture;

float Game::InterpParam = 0.0f;

double Game::HighScore = defaultScore;
double Game::ScoreEnemy = 25;
double Game::TimeInLevel = 0;
double Game::FrameInLevel = 0;
double Game::TimePaused = 100;
double currentTotalTime = 0;
long long currentTotalScore = 0;
long long totalHighScore = 0;
long long highestTotalHighScore[10] = {
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000,
	10000000000
};

satVec3 Game::adjust = 0.0f;


float Game::collisionTime = 0.0f;
const float Game::invincibleTime = 30.0f / 10.0f;
const float Game::invincibleTimeInverse = 1.0f / invincibleTime;
float Game::timeDead = 0.0f;
float Game::timeDeadMax = 1.5f;
float Game::timeDeadReverse = timeDeadMax * 0.5f;
float Game::timeDeadRewindPosition = 0.0f;
float Game::timeDeadRewindTime = 0.0f;
float Game::timeContrast = 0.0f;
float Game::timeContrastNegative = 0.0f;
const float Game::timeContrastMax = 0.25f;

float Game::timeFaceFlash = 0.0f;
const float Game::timeFaceFlashMax = 0.25f;
const int Game::numOfFaceFlash = 5;
Texture Game::FaceFlashTexture[10];
Texture Game::VHSGrainTexture;
int Game::FaceFlashPick = 0;

float Game::DoubleShootTime		= 0.0f;
float Game::AutoShootTime		= 0.0f;
float Game::NoDamageTime		= 0.0f;

float Game::DoubleShootTimeMax	= 15.0f;
float Game::AutoShootTimeMax	= 15.0f;
float Game::NoDamageTimeMax		= 15.0f;

double Game::ScoreMult = defaultScoreMult;

Score Game::ScoreBoard;




bool Game::postProcessFogActive = tru;
bool Game::postProcessBloomActive = tru;
bool Game::postProcessSSAOActive = tru;  //coo
bool Game::postProcessBlurSSAOActive = tru;
bool Game::postProcessSobelActive = true;
bool Game::postProcessSobelThickActive = true;
bool Game::postProcessFilmGrainActive = tru;
bool Game::postProcessColorGrainActive = tru;	
bool Game::postProcessPauseVHSActive = tru;
bool Game::postProcessGreyscaleActive = true;
bool Game::postProcessGreyscalePlusEmissiveActive = tru;
bool Game::postProcessHueShiftActive = true;
bool Game::postProcessTestActive = true;
bool Game::postProcessWaveActive = tru;
bool Game::postProcessWave2Active = true;
bool Game::postProcessMotionBlurActive = true;
bool Game::postProcessFXAAActive = tru;
bool Game::postProcessSonarActive = tru;
bool Game::postProcessACESActive = true;
bool Game::postProcessContrastActive = tru;
bool Game::postProcessInvertColourActive = true;
bool Game::postProcessInvertBrightnessActive = true;
int Game::postProcessDepthOfFieldActive = 2;
bool Game::postProcessScanlineActive = tru;
bool Game::postProcessSepiaActive = tru;
bool Game::postProcessSepiaPlusEmissiveActive = tru;
bool Game::postProcessRGBSplitActive = true;
bool Game::postProcessRGBSplitEarthquakeActive = true;
bool Game::postProcessChromaticAberrationActive = tru;
bool Game::postProcessCRTActive = true;
bool Game::postProcessInvertActive = true;


int Game::DetailChromaticAberration = 1;



#pragma endregion staticVariables

#pragma region localVariables

const satVec3 initialTranslation = satVec3(8.0f, -2.00f, 30.0f);

satVec3 col;
satVec2 posMouse;
float FOV;

satColor rainbowArray[] = 
{
	sat::color::red,
	sat::color::orange,
	sat::color::yellow,
	sat::color::green,
	sat::color::cyan,
	sat::color::lightblue,
	sat::color::magenta,
	sat::color::red
};
int rainbowArraySize = 7;

std::vector<satMat4> matrixBlockArray;

Material darkLight;

const GLuint NR_LIGHTS = 64;

unsigned int blocksOnScreen = 0;

//float WINDOW_WIDTH = 1280;
//float WINDOW_HEIGHT = 720;

float rotationConst = 1.0f;

#pragma endregion localVariables

Game::Game()
	:GBuffer(6), BackgroundBuffer(1), DeferredComposite(1), FinalComposite(1), PreviousFrame(1), FullScreenOutput(1), EdgeMap(1), ShadowMap(0), 
	WorkBuffer1_2(1), WorkBuffer2_2(1), WorkBuffer1_4(1), WorkBuffer2_4(1), WorkBuffer1_8(1), WorkBuffer2_8(1), SSAOWorkBuffer1(1), SSAOWorkBuffer2(1), LeftEyeBuffer(1),
	CurrentDistance(0.0f),
	CurrentSegment(0),
	InterpolationParam(0.0f)
{
	std::cout << "Beginning Hell...\n";


	//points.max_size();
	//	_RenderMan = new RenderManager(WINDOW_WIDTH, WINDOW_HEIGHT);
	
	FrameBufferFinalActive = true;
	TitleScreenActive = tru;
	//curLevel = new Level(443, 12);
	////curLevel = new Level();
	//curLevel->readLevel("../Assets/levels/1a.txt");
	
	//PINEAPPLE
	//Loads in each level
	//LevelManager::level[LevelManager::levelCurrent].X = 451;
	//LevelManager::level[LevelManager::levelCurrent].Y = 12;

	//Level::levelSizeX[0] = 451;
	//Level::levelSizeY[0] = 12;

	//LevelWidth = 451;
	//LevelHeight = 12;
	
	//curLevel->readLevel("../Assets/levels/1a.txt");

	songPitch = 1.0f;
	songVolume = 1.0f;
	songLowPass = 500.0f;
	songHighPass = 5000.0f;
	songFlange = 1.0f;

	srand(13);


	for (GLfloat i = 0; i < 8.0f; i++)
	{
		GLfloat yPos = (i * 8.0f) - 24.0f;
		for (GLfloat j = 0; j < 8.0f; j++)
		{
			GLfloat xPos = (j * 4.0f);
			GLfloat zPos = 1.0f;
			//lightPoint.push_back(PointLight(satColor(1.0f), satVec3(xPos, yPos, zPos), 1.0f, 0.2f, 0.1f));
		}
	}

	for (GLuint i = 0; i < lightPoint.size(); i++)
	{
		// Also calculate random color
		GLfloat rColor = ((rand() % 100) / 90.0f) + 0.1; // Between 0.1 and 1.0
		GLfloat gColor = ((rand() % 100) / 90.0f) + 0.1; // Between 0.1 and 1.0
		GLfloat bColor = ((rand() % 100) / 90.0f) + 0.1; // Between 0.1 and 1.0

		lightPoint[i].color = (satVec3::Normalize(satVec3(rColor, gColor, bColor)));
		lightPoint[i].getRadius();
		//lightPoint[i].color = satColor(0.0f);
	}


	float darkArea = 0.5f; 

	lightShipHealth.push_back(PointLight(satColor(0.25f, 0.5f, 0.5f), 0.0f, 1.0f, 0.8f * 4.0f * darkArea, 0.4f * 4.0f * darkArea));
	lightShipHealth.push_back(PointLight(sat::color::lightcyan, 0.0f, 1.0f, 0.8f * 4.0f, 0.4f * 4.0f));
	lightShipHealth.push_back(PointLight(sat::color::lightcyan, 0.0f, 1.0f, 0.8f * 4.0f, 0.4f * 4.0f));
	lightShipHealth.push_back(PointLight(sat::color::lightcyan, 0.0f, 1.0f, 0.8f * 4.0f, 0.4f * 4.0f));
	lightShipHealth.push_back(PointLight(sat::color::lightcyan, 0.0f, 1.0f, 0.8f * 4.0f, 0.4f * 4.0f));

	for (GLuint i = 0; i < 3; i++)
	{
		lightShipHealth.push_back(PointLight(sat::color::lightgreen, 0.0f, 1.0f, 0.2f * darkArea, 0.1f * darkArea));
	}

	
	//lightShipInvincible.push_back(PointLight(sat::color::blue	, 0.0f, 1.0f, 0.1f, 0.05f));	
	lightShipInvincible.push_back(PointLight(sat::color::green	, 0.0f, 1.0f, 0.05f, 0.025f));
	lightShipInvincible.push_back(PointLight(sat::color::red	, 0.0f, 1.0f, 0.05f, 0.025f));
	lightShipInvincible.push_back(PointLight(sat::color::green	, 0.0f, 1.0f, 0.05f, 0.025f));
	lightShipInvincible.push_back(PointLight(sat::color::red	, 0.0f, 1.0f, 0.05f, 0.025f));
	

	
	//darkLight.ambient = 0.05f;
	//darkLight.diffuse = 0.4f;
	//darkLight.specular = 1.00f;
	//
	////darkLight.ambient = 0.0f;
	////darkLight.diffuse = 0.0f;
	////darkLight.specular = 0.0f;
	//
	//darkLight.ambient = 0.625f;
	//darkLight.diffuse = 0.15f;
	//darkLight.specular = 0.5f;
	//
	//darkLight.ambient = 0.15f;
	//darkLight.diffuse = 0.35f;
	//darkLight.specular = 0.35f;
	//
	//darkLight.ambient = 0.05f;
	//
	////darkLight.ambient =		satVec3(0.1f, 0.01f, 0.01f);
	////darkLight.diffuse =		satVec3(0.15f, 0.05f, 0.05f);
	////darkLight.specular =	satVec3(0.2f, 0.15f ,0.15f);

	RimLightActive = true;
	EnviromentMapActive = tru;
	OrthographicActive = true;
	

	

	points.resize(5);
	timeL = 0.05f;
	counterL = 0;
	arraySizeL = points.size() - 1;
	incrementConst = 0.0025f;
	
	points[0] = { -5.0f, 0.0f, 0.0f };
	points[1] = { 5.0f, 0.0f, 0.0f };
	points[2] = { 5.0f, 0.0f, 5.0f };
	points[3] = { -5.0f, 5.0f, 5.0f };
	points[4] = points[0];

	//colours[0] = { 0.5f, 0.25f, 0.25f };
	//colours[1] = { 0.5f, 0.5f, 0.0f };
	//colours[2] = { 0.0f, 0.5f, 0.5f };
	//colours[3] = { 0.5f, 0.0f, 0.5f };

	colours[0] = { 0.12f, 0.1f, 0.1f };
	colours[1] = { 0.22f, 0.2f, 0.2f };
	colours[2] = { 0.15f, 0.1f, 0.1f };
	colours[3] = { 0.22f, 0.2f, 0.2f };
	colours[4] = colours[0];
	counterC = 0;

	fovA[0] = 60;
	fovA[1] = 60;
	fovA[2] = fovA[0];
	counterF = 0;
	
	

	FOV = 21.0f;
	FOV *= 2.35f;
	FOV = 47.0f;

	DataPoints.push_back(satVec2(-8.0f + 0.0f, -14.0f));
	DataPoints.push_back(satVec2( 8.0f + 0.0f, -14.0f));
	DataPoints.push_back(satVec2( 8.0f + 0.0f,  14.0f));
	DataPoints.push_back(satVec2(-8.0f + 0.0f,  14.0f));
	DataPoints.push_back(satVec2(-4.0f + 0.0f,	0.0f));
	DataPoints.push_back(satVec2(-2.0f + 0.0f, 2.0f));
	DataPoints.push_back(satVec2( 4.0f + 0.0f, -2.0f));
	DataPoints.push_back(DataPoints[0]);
	DataPoints.push_back(DataPoints[1]);
	DataPoints.push_back(DataPoints[2]);
	UpdateDistanceTable();

	Sys.init();
	//test.loadFromFile("../Assets/Sound/Music/FirstLevelMaybe.mp3", false, tru);
	test.loadFromFile("../Assets/Sound/Music/BossIntense.mp3", false, tru);
	//test.loadFromFile("../Assets/Sound/Music/IcePlanet.mp3", false, tru);
	//test.loadFromFile("../Assets/Sound/Music/Select.mp3", false, tru);
	//test.loadFromFile("../Assets/Sound/Music/MainThemeMerge.mp3", false, tru);
	
	PewSound.loadFromFile("../Assets/Sound/Effect/pew.ogg", false, true);
	FizzSound.loadFromFile("../Assets/Sound/Effect/fizzbuzz.ogg", false, true);
	WallVoiceSound.loadFromFile("../Assets/Sound/Effect/donottouchwall.ogg", false, true);
	BzztSound.loadFromFile("../Assets/Sound/Effect/bzzt.ogg", false, true);
	BoomSound.loadFromFile("../Assets/Sound/Effect/boom.ogg", false, true);
	BlarpSound.loadFromFile("../Assets/Sound/Effect/blarp.ogg", false, true);

	float flangeRate  = 4.0f;
	float flangeParam = 0.5f;
	
	FizzSound.setDSPFlange(flangeRate, flangeParam);
	BzztSound.setDSPFlange(flangeRate, flangeParam);

	ChargeUpSound.loadFromFile("../Assets/Sound/Effect/ChargeUp.wav", false, true);
	ChargeDownSound.loadFromFile("../Assets/Sound/Effect/ChargeDown.wav", false, true);

	aviTexture.Anisotropy = 1.0f;
	aviTexture.FilterMag = GL_LINEAR;
	aviTexture.FilterMin = GL_LINEAR;
	

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);






	//INITIALIZE GAME

	//_Window = &window;
	//_Window.create(sf::VideoMode(1600, 900), "OpenGL", sf::Style::Default, cs);
	//_Window.setFramerateLimit(60);
	
	updateTimer = new Timer();
	wireframe = false;

	//std::cout << "Beginning Hell...\n";

	FrameBuffer::InitFullScreenQuad();

	//std::cout << "Beginning Hell...\n";

	glEnable(GL_DEPTH_TEST); //Enable depth desting so things that are closer are rendered in front.
	glEnable(GL_TEXTURE_2D); //Texturing is not enabled in OpenGL by default. //Actually it is now.
	//glDepthFunc(GL_LEQUAL);
	//glDepthFunc(GL_ALWAYS);
	//glDisable(GL_DEPTH_TEST);

	ShaderProgram::initDefault();
	//loadShader(&StaticGeometry, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/GBufferPass.frag");
	loadShader(&GBufferPass, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/GBufferPass.frag");
	loadShader(&GBufferCubePass, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/GBufferCubePass.frag");
	loadShader(&GBufferPassAlphaDiscard, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/GBufferPassAlphaDiscard.frag");
	loadShader(&GBufferCubePassAlphaDiscard, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/GBufferCubePassAlphaDiscard.frag");
	loadShader(&AnimateModel, "../Assets/Shaders/Animation.vert", "../Assets/Shaders/GBufferCubePass.frag");
	loadShader(&DeferredLighting, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/DeferredLightingPointLights.frag");
	loadShader(&PointLightShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/dPointLight.frag");
	loadShader(&BloomHighPass, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Bloom/BloomHighPass.frag");
	loadShader(&BlurHorizontal, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Bloom/BlurHorizontal.frag");
	loadShader(&BlurVertical, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Bloom/BlurVertical.frag");
	loadShader(&BloomComposite, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Bloom/BloomComposite.frag");
	loadShader(&SSAOShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SSAO/SSAOPost.frag");
	loadShader(&SSAOBlurHorizontal, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SSAO/BlurHorizontal.frag");
	loadShader(&SSAOBlurVertical, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SSAO/BlurVertical.frag");
	loadShader(&SSAOComposite, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SSAO/SSAOComposite.frag");	
	loadShader(&SobelPass, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Toon/Sobel.frag");
	loadShader(&SobelThickPass, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Toon/SobelThick.frag");
	loadShader(&EdgeMultPass, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Toon/EdgeMult.frag");
	FogShader.resize(5);
	loadShader(&FogShader[0], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Fog/FogPostFastest.frag");
	loadShader(&FogShader[1], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Fog/FogPostFast.frag");
	loadShader(&FogShader[2], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Fog/FogPost.frag");
	loadShader(&FogShader[3], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Fog/FogPostNice.frag");
	loadShader(&FogShader[4], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/Fog/FogPostExcellent.frag");
	loadShader(&GreyscalePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/GreyscalePost.frag");
	loadShader(&GreyscalePlusEmissivePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/GreyscalePostPlusEmissive.frag");
	loadShader(&SepiaPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SepiaPost.frag");
	loadShader(&SepiaPlusEmissivePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SepiaPostPlusEmissive.frag");
	loadShader(&InvertPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/InvertPost.frag");
	loadShader(&FilmGrainPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/FilmGrainBiggerPost.frag");
	loadShader(&ColorGrainPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/ColorGrainPost.frag");
	loadShader(&PauseVHSPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/PauseVHSPost.frag");
	loadShader(&ContrastPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/ContrastPost.frag");
	loadShader(&HueShiftPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/HueShiftPost.frag");
	loadShader(&CRTPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CRTPost.frag");
	loadShader(&ScanlinePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/ScreenDoorPost.frag");
	loadShader(&RGBSplitPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/RGBSplitPostNice.frag"); 
	loadShader(&RGBSplitFastPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/RGBSplitPostFast.frag"); 
	ChromaticAberrationPostShader.resize(4);
	loadShader(&ChromaticAberrationPostShader[0], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/ChromaticAberrationPostFast.frag"); 
	loadShader(&ChromaticAberrationPostShader[1], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/ChromaticAberrationPost.frag"); 
	loadShader(&ChromaticAberrationPostShader[2], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/ChromaticAberrationPostNice.frag"); 
	loadShader(&ChromaticAberrationPostShader[3], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/CA/ChromaticAberrationPostExcellent.frag"); 
	DepthOfFieldPostShader.resize(2);
	loadShader(&DepthOfFieldPostShader[0], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/DepthOfFieldPostFast.frag");
	loadShader(&DepthOfFieldPostShader[1], "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/DepthOfFieldPost.frag");
	DepthOfFieldPostShaderPtr = &DepthOfFieldPostShader[1];
	loadShader(&WavePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/WavePost.frag");
	loadShader(&WavePost2Shader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/WavePost2.frag");
	
	loadShader(&MotionBlurPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/MotionBlurPost.frag");
	loadShader(&FXAAPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/AA/FXAA.frag");
	//loadShader(&SMAAPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/AA/SMAA.frag");
	
	loadShader(&SonarShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/SonarPost.frag");

	loadShader(&ACESPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/ACES.frag");

	loadShader(&TestPostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/TestPost.frag");
	loadShader(&PassThroughShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PassThrough.frag");
	loadShader(&PassThroughFlipShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PassThroughFlip.frag");
	loadShader(&PassThroughFlipMultShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PassThroughFlipMult.frag");	
	loadShader(&PassThroughMultShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PassThroughMult.frag");
	loadShader(&PassThroughShadowShader, "../Assets/Shaders/StaticGeometry.vert", "../Assets/Shaders/PassThroughShadow.frag");

	
	loadShader(&ThreeDeePostShader, "../Assets/Shaders/PassThrough.vert", "../Assets/Shaders/PostProcess/3DPost.frag");
	
	//loadShader(&ParticlesShader, "../Assets/Shaders/Particle.vert", "../Assets/Shaders/Particle.frag", "../Assets/Shaders/Particle.geom");
	
	

	std::cout << "Shaders have loaded...\n";

	setUpInterlaceTexture();

	GBuffer.InitDepthTexture(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
	GBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGBA8, GL_LINEAR, GL_CLAMP_TO_EDGE); //Flat colour
	GBuffer.InitColorTexture((unsigned int)1, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Normals (xyz) Does not require 4th channel
	GBuffer.InitColorTexture((unsigned int)2, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);	 //Add On Colours
	GBuffer.InitColorTexture((unsigned int)3, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Specular
	GBuffer.InitColorTexture((unsigned int)4, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_R16F, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Specular Shiny
	GBuffer.InitColorTexture((unsigned int)5, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Emissive
	//GBuffer.InitColorTexture((unsigned int)7, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Normals without normal texture applied
	checkFBO(&GBuffer);

	//BackgroundBuffer.InitDepthTexture(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
	//BackgroundBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGBA8, GL_LINEAR, GL_CLAMP_TO_EDGE); //Flat colour
	//checkFBO(&BackgroundBuffer);

	DeferredComposite.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_LINEAR, GL_MIRRORED_REPEAT);
	checkFBO(&DeferredComposite);

	FinalComposite.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_LINEAR, GL_MIRRORED_REPEAT);
	checkFBO(&FinalComposite);

	LeftEyeBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_MIRRORED_REPEAT);
	checkFBO(&LeftEyeBuffer);

	PreviousFrame.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	//PreviousFrame.InitColorTexture((unsigned int)1, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB32F, GL_LINEAR, GL_CLAMP_TO_EDGE);  //View Space Positions (xyz)
	checkFBO(&PreviousFrame);

	FullScreenOutput.InitColorTexture((unsigned int)0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&FullScreenOutput);

	EdgeMap.InitColorTexture((unsigned int)0, (WINDOW_WIDTH >> UpScaleSobelResolution) >> UpScaleResolutionX, (WINDOW_HEIGHT >> UpScaleSobelResolution)  >> UpScaleResolutionY, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&EdgeMap);

	ShadowMap.InitDepthTexture(SHADOW_RESOLUTION >> UpScaleShadowResolution, SHADOW_RESOLUTION >> UpScaleShadowResolution, GL_LINEAR);
	checkFBO(&ShadowMap);

	WorkBuffer1_2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer1_2);
	WorkBuffer2_2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer2_2);
	WorkBuffer1_4.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer1_4);
	WorkBuffer2_4.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer2_4);
	WorkBuffer1_8.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer1_8);
	WorkBuffer2_8.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&WorkBuffer2_8);

	SSAOWorkBuffer1.InitColorTexture((unsigned int)0, (WINDOW_WIDTH ) / SSAO_DOWNSCALE, (WINDOW_HEIGHT) / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&SSAOWorkBuffer1);

	SSAOWorkBuffer2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / SSAO_DOWNSCALE, (WINDOW_HEIGHT)  / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	checkFBO(&SSAOWorkBuffer2);

	
	curLevel = &LevelManager::level[LevelManager::levelCurrent];
	
	calcCamera();

	//if(OrthographicActive)
	//{
	//	satVec2 aspectRatio = { 1.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT };
	//	CameraProjection.OrthoProjection(25.0f / aspectRatio.y, -25.0f / aspectRatio.y, -25.0f / aspectRatio.x, 25.0f / aspectRatio.x, -50.0f, 50.0f); //Set FOV, Aspect Ratio, near and far clipping planes.
	//}
	//else
	//{
	//	CameraProjection.PerspectiveProjection(FOV, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 10.0f, 50.0f); //Set FOV, Aspect Ratio, near and far clipping planes.
	//}
	//CameraProjection.PerspectiveProjection(FOV, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 0.01f, 45.0f); //Set FOV, Aspect Ratio, near and far clipping planes.
	//CameraProjection.OrthoProjection(25.0f / aspectRatio.y, -25.0f / aspectRatio.y, -25.0f / aspectRatio.x, 25.0f / aspectRatio.x, -50.0f, 50.0f); //Set FOV, Aspect Ratio, near and far clipping planes.
	
	float shadowOrthoSize = 50.0f;

	//ShadowProjection.OrthoProjection(shadowOrthoSize, -shadowOrthoSize, -shadowOrthoSize, shadowOrthoSize, -100.0f, 100.0f);
	ShadowProjection.PerspectiveProjection(55.0f, 1.0f, 1.0f, 200.0f);

	//ShadowProjection.PerspectiveProjection(90.0f, 1.0f, 1.0f, 500.0f); //Set FOV, Aspect Ratio, near and far clipping planes.


	//loadTexture(&DefaultNormal, "../Assets/Textures/Default/defaultNormal.png");
	loadTexture(&DefaultTexture, "../Assets/Textures/Default/defaultTexture.png",		1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultNormal, "../Assets/Textures/Default/defaultNormal.png",			1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultSpecular, "../Assets/Textures/Default/defaultSpecular.png",		1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultShiny, "../Assets/Textures/Default/defaultShiny.png",			1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultEmissive, "../Assets/Textures/Default/defaultEmissive.png",		1.0f, GL_NEAREST, GL_NEAREST);

	loadTexture(&DefaultTestTexture, "../Assets/Textures/Default/defaultTestTexture.png",		1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&DefaultTestNormal, "../Assets/Textures/Default/defaultTestNormal.png",			1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&DefaultTestSpecular, "../Assets/Textures/Default/defaultTestSpecular.png",		1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultTestShiny, "../Assets/Textures/Default/defaultTestShiny.png",			1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultTestEmissive, "../Assets/Textures/Default/defaultTestEmissive.png",		1.0f, GL_NEAREST, GL_NEAREST);
	loadTexture(&DefaultTestHeight, "../Assets/Textures/Default/defaultTestHeight.png",			1.0f, GL_LINEAR, GL_LINEAR);

	std::cout << "Default Textures have loaded" << std::endl;

	loadTexture(&NowLoading, "../Assets/Textures/NowLoading.png", 1.0f, GL_LINEAR, GL_LINEAR);
	
	
	//loadTexture(&TitleScreenTexture, "../Assets/Textures/CortexTitle.png");
	
	loadModel(&TitleScreen, "../Assets/Models/TitleScreen.obj");
	loadModel(&TitleScreenSmall, "../Assets/Models/TitleScreenSmall.obj");
	loadModel(&TitleScreenLarge, "../Assets/Models/TitleScreenLarge.obj");

	std::cout << "Title Screen has loaded" << std::endl;

	//if (!TitleScreenTexture.Load("../Assets/Textures/CortexTitle.png"))
	//{
	//	std::cout << "Texture failed to load.\n";
	//	system("PAUSE");
	//	exit(0);
	//}

}

Game::~Game()
{
	delete updateTimer;
		
	CloseAVI();
}

//////

//////

//////

//	All the models are loaded in here
void Game::initializeGame()
{
	//std::vector<satVec3> ssaoNoise;
	//for (GLuint i = 0; i < 4*4; i++)
	//{
	//	satVec3 noise(
	//		randomFloats(generator),
	//		randomFloats(generator),
	//		0.0f);		
	//	ssaoNoise.push_back(noise);
	//	ssaoNoise[i].Normalize();
	//}

	std::vector<GLubyte> vectorData;

	for (GLuint i = 0; i < 256 * 256; ++i)
	{
		satVec3 vector = satVec3(randomFloats(generator), randomFloats(generator), 0.0f);
		vector.Normalize();

		vectorData.push_back(vector.x * 256);
		vectorData.push_back(vector.y * 256);
		vectorData.push_back(0);

		
	}							

	
	glGenTextures(1, &noiseTexture);
	glBindTexture(GL_TEXTURE_2D, noiseTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 256, 256, 0, GL_RGB, GL_UNSIGNED_BYTE, &vectorData[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	
	





	loadTexture(&TitleScreenTextTexture, "../Assets/Textures/SpeedStormTitleText.png", 1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&TitleScreenTextAlphaTexture, "../Assets/Textures/SpeedStormTitleAlphaTemp.png", 1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&TitleScreenBackgroundTexture, "../Assets/Textures/SpeedstormTitleNoText.png", 1.0f, GL_LINEAR, GL_LINEAR);


	loadTexture(&SpecialThanksScreenTexture, "../Assets/Textures/SpecialThanksText.png", 1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&SpecialThanksScreenAlphaTexture, "../Assets/Textures/SpecialThanksAlpha.png", 1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&SpecialThanksScreenBackgroundTexture, "../Assets/Textures/SpecialThanksBackground.png", 1.0f, GL_LINEAR, GL_LINEAR);


	loadTexture(&TitleScreenTexture, "../Assets/Textures/SpeedstormTempLogo.png", 1.0f, GL_LINEAR, GL_LINEAR);



	loadModel(&Background, "../Assets/Models/backgroundBig.obj");
	loadModel(&Ship, "../Assets/Models/ShipPlayer/ship_mod.obj");

	if (!ShipFrame.LoadFile("../Assets/Models/ShipPlayer/ship_mod.obj", "../Assets/Models/ShipPlayer/ship_small.obj"))
	{
		std::cout << "Ship failed to load.\n";
		system("PAUSE");
		exit(0);
	}

	//std::cout << "Tree loaded" << std::endl;
	//loadModel(&Block, "../Assets/Models/brick.obj");
	//
	//loadModel(&HalfBlock, "../Assets/Models/X__block_half.obj");
	//
	//PINEAPPLE
	//loadModel(&TopBlock, "../Assets/Models/O__block_top.obj");
	//loadModel(&BottomBlock, "../Assets/Models/Z__block_bottom.obj");
	//loadModel(&FrontBlock, "../Assets/Models/X__block_front.obj");
	//loadModel(&FullBlock, "../Assets/Models/block_2x8.obj");
	//loadModel(&Die, "../Assets/Models/spike.obj");

	loadModel(&BulletMesh, "../Assets/Models/ball.obj");
	loadModel(&SmokeMesh, "../Assets/Models/smoke.obj");
	loadModel(&TutorialMesh, "../Assets/Models/tutorial_obj.obj");
	loadModel(&TutorialLessTallMesh, "../Assets/Models/tutorial_2.obj");
	loadModel(&TutorialTallMesh, "../Assets/Models/tutorial_tall_small.obj");
	loadModel(&TutorialWideMesh, "../Assets/Models/tutorial_wide_small.obj");
	
	//BlockModel.resize(4);
	//loadModel(&BlockModel[0], "../Assets/Models/block_2x8.obj");
	//loadModel(&BlockModel[1], "../Assets/Models/CastleBrick.obj");
	//loadModel(&BlockModel[2], "../Assets/Models/CastleBrick.obj");
	//loadModel(&BlockModel[3], "../Assets/Models/CastleBrick.obj");

	//ENDPINEAPPLE


	std::cout << "Models have loaded...\n";


	//Loading Textures

	//loadTexture(&BackgroundTexture[0], "../Assets/Textures/Background/1A_resized.png");
	//loadTexture(&BackgroundTexture[1], "../Assets/Textures/Background/1B_resized.png");
	loadTexture(&BackgroundTexture[0], "../Assets/Textures/Default/black.png", 1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&BackgroundTexture[1], "../Assets/Textures/Default/black.png", 1.0f, GL_LINEAR, GL_LINEAR);

	loadTexture(&ShadowMapFlashlight, "../Assets/Textures/Flashlight.png", 1.0f, GL_LINEAR, GL_LINEAR);
	

	for(int i = 0; i < 10; ++i)
	{
		std::string filename = "../Assets/Textures/HUD/A" + std::to_string(i) + ".png";
		loadTexture(&NumberTex[i], filename,		1.0f, GL_LINEAR, GL_LINEAR);
	}

	loadTexture(&NumberColonTex, "../Assets/Textures/HUD/Acolon.png",		1.0f, GL_LINEAR, GL_LINEAR);
	loadTexture(&NumberPeriodTex, "../Assets/Textures/HUD/Aperiod.png",		1.0f, GL_LINEAR, GL_LINEAR);
			   
	loadTexture(&NumberBlankTex, "../Assets/Textures/HUD/Awhite.png",		1.0f, GL_LINEAR, GL_LINEAR);
	//NumberTex[10];
	//NumberColonTex;




	//CUBE MAPS MAN	
	//http://learnopengl.com/#!Advanced-OpenGL/Cubemaps
	//HERE'S THE SOURCE MAN
	GLuint cubemapTexture = AmbientCubeMapTexture.loadCubemap("Sky/Skybox");
	
	float ani = 16.0f;
	loadTexture(&ShipTexture[0], "../Assets/Textures/ShipPlayer/Texture_0.png",					ani, GL_LINEAR);
	loadTexture(&ShipTexture[1], "../Assets/Textures/ShipPlayer/Texture_1.png",					ani, GL_LINEAR);
	loadTexture(&ShipTexture[2], "../Assets/Textures/ShipPlayer/Texture_2.png",					ani, GL_LINEAR);
	loadTexture(&ShipTexture[3], "../Assets/Textures/ShipPlayer/Texture_3.png",					ani, GL_LINEAR);
	loadTexture(&ShipNormalTexture[0], "../Assets/Textures/ShipPlayer/Normal_1.png",			ani, GL_LINEAR);
	loadTexture(&ShipNormalTexture[1], "../Assets/Textures/ShipPlayer/Normal_2.png",			ani, GL_LINEAR);
	loadTexture(&ShipNormalTexture[2], "../Assets/Textures/ShipPlayer/Normal_2.png",			ani, GL_LINEAR);
	loadTexture(&ShipNormalTexture[3], "../Assets/Textures/ShipPlayer/Normal_3.png",			ani, GL_LINEAR);
	loadTexture(&ShipEmissiveTexture[0], "../Assets/Textures/ShipPlayer/Emissive_0.png",		ani, GL_LINEAR);
	loadTexture(&ShipEmissiveTexture[1], "../Assets/Textures/ShipPlayer/Emissive_1.png",		ani, GL_LINEAR);
	loadTexture(&ShipEmissiveTexture[2], "../Assets/Textures/ShipPlayer/Emissive_2.png",		ani, GL_LINEAR);
	loadTexture(&ShipEmissiveTexture[3], "../Assets/Textures/ShipPlayer/Emissive_3.png",		ani, GL_LINEAR);
	loadTexture(&ShipInvincibleTexture[0], "../Assets/Textures/ShipPlayer/Emissive_0INV.png",	ani, GL_LINEAR);
	loadTexture(&ShipInvincibleTexture[1], "../Assets/Textures/ShipPlayer/Emissive_1INV.png",	ani, GL_LINEAR);
	loadTexture(&ShipInvincibleTexture[2], "../Assets/Textures/ShipPlayer/Emissive_2INV.png",	ani, GL_LINEAR);
	loadTexture(&ShipInvincibleTexture[3], "../Assets/Textures/ShipPlayer/Emissive_3INV.png",	ani, GL_LINEAR);
	loadTexture(&ShipShinyTexture[0], "../Assets/Textures/ShipPlayer/Shiny_0.png",				ani, GL_LINEAR);
	loadTexture(&ShipShinyTexture[1], "../Assets/Textures/ShipPlayer/Shiny_1.png",				ani, GL_LINEAR);
	loadTexture(&ShipShinyTexture[2], "../Assets/Textures/ShipPlayer/Shiny_2.png",				ani, GL_LINEAR);
	loadTexture(&ShipShinyTexture[3], "../Assets/Textures/ShipPlayer/Shiny_3.png",				ani, GL_LINEAR);





	loadTexture(&FaceFlashTexture[0], "../Assets/Textures/BonusFaces/Dan.png"				, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[1], "../Assets/Textures/BonusFaces/EvilStephen.png"		, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[2], "../Assets/Textures/BonusFaces/Jacob.png"				, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[3], "../Assets/Textures/BonusFaces/Jif.png"				, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[4], "../Assets/Textures/BonusFaces/DanYeo.png"			, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[8], "../Assets/Textures/BonusFaces/HighScoreClear.png"	, 1.0f, GL_LINEAR);
	loadTexture(&FaceFlashTexture[9], "../Assets/Textures/BonusFaces/Cheater.png"			, 1.0f, GL_LINEAR);
	
	
	loadTexture(&VHSGrainTexture, "../Assets/Textures/Post/FilmGrainGlitchRamp.png", 1.0f, GL_LINEAR, GL_LINEAR, GL_REPEAT);

	

	WinGameSong.loadFromFile("../Assets/Sound/Music/Victory.mp3", false, true);
	SelectSong.loadFromFile("../Assets/Sound/Music/SelectRemastered.mp3", false, tru);

	//SelectSong;
	//WinGameSong;

	//END PINEAPPLE


	std::cout << "Textures have loaded...\n";

	//OpenAVI(L"../assets/video/face2.avi");
	//Initialize(L"../assets/Video/Duane3.avi");
	Initialize(L"../assets/Video/TutorialControllerUpdated.avi");

	std::cout << "Textures have loaded...\n";

	//...
	//TotalRotation = 0.0f;
	sat::key::Init();

	std::cout << "Beginning...\n";
	//satVec2 tempo = catmull(satVec2(1, 5), satVec2(4, 2), satVec2(7, 5), satVec2(4, 4), 1.0f / 2.0f);
	//satVec2 tempo = catmull(satVec2(2, 1), satVec2(4, 4), satVec2(6, 12), satVec2(5, 5), 1.0f / 3.0f);
	//std::cout << tempo.x << "\t" << tempo.y;

	TotalTranslation.x += initialTranslation.x;
	TotalTranslation.y += initialTranslation.y;
	//TotalTranslation.z += initialTranslation.z;

	TitleScreenActive = tru;

	
	////////END PINEAPPLE

	//test.playSound(0.0f, 1.0f, 1.0f);
	//test.playSound(0.4f, 1.0f, 1.0f);

	windowSelected = tru;
}

void Game::cycleLightMode()
{
	//LightIntensityModeSize = 7;

	++LightIntensityMode;
	LightIntensityMode = LightIntensityMode % LightIntensityModeSize;
	//if(LightIntensityMode >= LightIntensityModeSize)
	//{
	//	LightIntensityMode = 0;
	//}

	if	(LightIntensityMode == 0)
	{
		LightIntensity = sat::light::lit::medium;
	}
	elf	(LightIntensityMode == 1)
	{
		LightIntensity = sat::light::lit::fulldark;		
	}
	elf	(LightIntensityMode == 2)
	{
		LightIntensity = sat::light::lit::darker;
	}
	elf	(LightIntensityMode == 3)
	{
		LightIntensity = sat::light::lit::dark;
	}
	elf	(LightIntensityMode == 4)
	{
		LightIntensity = sat::light::lit::medium;
	}
	elf	(LightIntensityMode == 5)
	{
		LightIntensity = sat::light::lit::bright;
	}
	elf	(LightIntensityMode == 6)
	{
		LightIntensity = sat::light::lit::brighter;
	}
	
	elf	(LightIntensityMode == 7)
	{
		LightIntensity = sat::light::lit::fullbright;
	}
	else
	{
		LightIntensity = sat::light::lit::fulldark;		
	}

	//int		Game::LightIntensityMode = 0;
	//
	//Light	Game::LightIntensity = 0.0f;
}

void Game::cycleLightMode(const Light &light)
{
	LightIntensity = light;
}

void Game::setWindow(GLFWwindow* window)
{
	this->window = window;
}


void Game::setWindow(sf::Window* window)
{
	this->sfWindow = window;
}

void Game::setFullscreenSize(const int *width, const int *height)
{
	FULLSCREEN_WIDTH = *width;
	FULLSCREEN_HEIGHT = *height;

	//FullScreenOutput.Reset(1);
	//FullScreenOutput.InitColorTexture((unsigned int)0, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT, GL_RGBA8, GL_LINEAR, GL_CLAMP_TO_EDGE);
	//checkFBO(&FullScreenOutput);
}

//void Game::setTimers(double* timer, double* timerOld, double* timerFrame)
//{
//	Game::timerTotal = timer;
//	Game::timerOld = timerOld;
//	Game::timerFrame = timerFrame;
//}


void Game::displayDebugFrame()
{
	
	//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

	glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

	FullScreenOutput.Clear();
	if(SatDebugDisplayMode != SAT_DEBUG_OUTPUT_POSITION && SatDebugDisplayMode != SAT_DEBUG_OUTPUT_SHINY)
	{
		PassThroughShader.bind();
		PassThroughShader.sendUniform("uSceneTex", 0);
	}
	else
	{
		PassThroughMultShader.bind();
		PassThroughMultShader.sendUniform("uSceneTex", 0);
		if(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_POSITION)
		{
			PassThroughMultShader.sendUniform("uMult", Timer::sinRange(0.5f, 0.03125f, 0.03125f));
			PassThroughMultShader.sendUniform("uMultVec", satVec3(1.0f, 1.0f, -0.2f));
		}
		else
		{
			PassThroughMultShader.sendUniform("uMult", Timer::sinRange(0.25f, 0.25f, 0.125f * 0.125f * 0.125f));
			PassThroughMultShader.sendUniform("uMultVec", satVec3(1.0f));
		}
	}

	FullScreenOutput.bind();

	glActiveTexture(GL_TEXTURE0);
	
	

	if(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_ALBEDO)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_NORMAL)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_POSITION)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SPECULAR)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(3));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SHINY)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(4));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_EMISSIVE)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));
	}
	//elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_EMISSIVE)
	//{
	//	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(6));
	//}
	//elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_NORMAL_POLY)
	//{
	//	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(7));
	//}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_EDGE_MAP)
	{
		glBindTexture(GL_TEXTURE_2D, EdgeMap.GetColorHandle(0));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SHADOW_MAP)
	{
		//float aspect = (SHADOW_RESOLUTION / SHADOW_RESOLUTION) * (FULLSCREEN_WIDTH / FULLSCREEN_HEIGHT);
		//float aspect = (FULLSCREEN_WIDTH / FULLSCREEN_HEIGHT);
		
		
		glViewport((FULLSCREEN_WIDTH - FULLSCREEN_HEIGHT) >> 1, 0,
			FULLSCREEN_HEIGHT,
			FULLSCREEN_HEIGHT);

		glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_DEPTH)
	{
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_BLOOM_OUTPUT)
	{
		glBindTexture(GL_TEXTURE_2D, WorkBuffer1_2.GetColorHandle(0));
	}
	elf(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SSAO)
	{
		glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer1.GetColorHandle(0));
	}


	FrameBuffer::DrawFullScreenQuad();

	glBindTexture(GL_TEXTURE_2D, GL_NONE);

	FrameBuffer::unbind();
	ShaderProgram::unbind();


	FullScreenOutput.MoveToBackBuffer(FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);

	//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);


	


	// SAT_DEBUG_OUTPUT_SCENE 0				//Regular scene
	// SAT_DEBUG_OUTPUT_ALBEDO 1				//Flat colour
	// SAT_DEBUG_OUTPUT_NORMAL 2				//Normals (xyz) Does not require 4th channel
	// SAT_DEBUG_OUTPUT_POSITION 3				//View Space Positions (xyz)
	// SAT_DEBUG_OUTPUT_AMBIENT 4				//Ambient
	// SAT_DEBUG_OUTPUT_SPECULAR 5				//Specular
	// SAT_DEBUG_OUTPUT_EMISSIVE 6				//Emissive
	// SAT_DEBUG_OUTPUT_NORMAL_POLY 7			//Normals without normal texture applied
	// SAT_DEBUG_OUTPUT_EDGE_MAP 8				//Edge Map
	// SAT_DEBUG_OUTPUT_SHADOW_MAP 9			//Shadow Map
	// SAT_DEBUG_OUTPUT_BLOOM_OUTPUT 10		//Bloom Output
	// SAT_DEBUG_OUTPUT_SSAO 11				//Screen Space Ambient Occlusion

}


void Game::Cheater(float amount)
{
	timeFaceFlash = amount;
	FaceFlashPick = 9;
}









void Game::InputUpdateHead()
{
	ShipShrink -= 0.05f;
	//InputMove *= 0.1001129150390625;
	InputMove = 0.0f;
	levelWinFrame = 0;
	if(DoubleShootTime>0.0f)
	{
		DoubleShootTime	-= Timer::framef();
		if(DoubleShootTime <= 0.0f)
		{
			ChargeDownSound.playSound(1.0f, 1.0f, 1.0f);
		}
	}

	if(AutoShootTime>0.0f)
	{
		AutoShootTime	-= Timer::framef();
		if(AutoShootTime <= 0.0f)
		{
			ChargeDownSound.playSound(1.0f, 1.0f, 1.0f);
		}
	}
	NoDamageTime	-= Timer::framef();
}

void Game::KeyboardDebugUpdate()
{
#pragma region DebugKeyboard

	sat::mouse::CheckButton(sf::Mouse::Middle);
	SatDebugFOVMult;

	SatDebugInputLeftCtrl = sat::key::CheckKey(sf::Keyboard::LControl);
	SatDebugInputLeftAlt = sat::key::CheckKey(sf::Keyboard::LAlt);
	SatDebugInputRightCtrl = sat::key::CheckKey(sf::Keyboard::RControl);
	SatDebugInputRightAlt = sat::key::CheckKey(sf::Keyboard::RAlt);
	SatDebugInputLeftShift = sat::key::CheckKey(sf::Keyboard::LShift);
	SatDebugInputRightShift = sat::key::CheckKey(sf::Keyboard::RShift);

	//No Ctrl/Alt
	if (SatDebugInputLeftCtrl  != tru &&
		SatDebugInputRightCtrl != tru &&
		SatDebugInputLeftAlt   != tru &&
		SatDebugInputRightAlt  != tru)
	{
		if (sat::key::CheckPressed(sf::Keyboard::Tilde))
		{
			system("cls");
			printDebugCommands();
		}

		if (sat::key::CheckPressed(sf::Keyboard::BackSpace))
		{
			if (ThreeDeeActive == 1)
			{
				ThreeDeeActive = 0;
				ReshapeWindow(tru);
			}
			else
			{
				ThreeDeeActive = 1;
				ReshapeWindow(tru);
			}
		}

		if (sat::key::CheckPressed(sf::Keyboard::BackSlash))
		{
			UpScaleResolutionX = !UpScaleResolutionX;
			UpScaleResolutionY = !UpScaleResolutionY;

			ReshapeWindow(tru);
		}

		bool ChangePerformanceSettings = false;

		if (sat::key::CheckPressed(sf::Keyboard::RBracket))
		{
			if (SettingsPerformance <3)
			{
				ChangePerformanceSettings = tru;
				++SettingsPerformance;				
			}
		}

		if (sat::key::CheckPressed(sf::Keyboard::LBracket))
		{
			if (SettingsPerformance > 0)
			{
				ChangePerformanceSettings = tru;
				--SettingsPerformance;
			}
		}

		if (ChangePerformanceSettings)
		{
			if (SettingsPerformance == 0)
			{
				UpScaleShadowResolution = 4;
				postProcessDepthOfFieldActive = 0;
				postProcessFXAAActive = false;
				postProcessSSAOActive = false;
				EnviromentMapActive = false;
				postProcessBloomActive = false;
				postProcessFogActive = false;
				SatDebugDeferredLightActive = false;
			}

			if (SettingsPerformance == 1)
			{
				UpScaleShadowResolution = 3;
				postProcessDepthOfFieldActive = 0;
				postProcessFXAAActive = false;
				postProcessSSAOActive = false;
				EnviromentMapActive = tru;
				postProcessBloomActive = tru;
				postProcessFogActive = tru;
				SatDebugDeferredLightActive = tru;
			}

			if (SettingsPerformance == 2)
			{
				UpScaleShadowResolution = 2;
				postProcessDepthOfFieldActive = 1;
				postProcessFXAAActive = tru;
				postProcessSSAOActive = tru;

				setUpSSAOKernel = false;
				numOfSSAOSamples = 16;
				buildSSAO();

				EnviromentMapActive = tru;
				postProcessBloomActive = tru;
				postProcessFogActive = tru;
				SatDebugDeferredLightActive = tru;
			}

			if (SettingsPerformance == 3)
			{
				UpScaleShadowResolution = 1;
				postProcessDepthOfFieldActive = 2;
				postProcessFXAAActive = tru;
				postProcessSSAOActive = tru;

				setUpSSAOKernel = false;
				numOfSSAOSamples = 32;
				buildSSAO();

				EnviromentMapActive = tru;
				postProcessBloomActive = tru;
				postProcessFogActive = tru;
				SatDebugDeferredLightActive = tru;
			}
			
			ReshapeWindow(tru);
		}
	}

	//Left Ctrl
	if (SatDebugInputLeftCtrl  == tru &&
		SatDebugInputRightCtrl != tru &&
		SatDebugInputLeftAlt   != tru &&
		SatDebugInputRightAlt  != tru)
	{
		if (sat::key::CheckPressed(sf::Keyboard::BackSlash))
		{
			UpScaleResolutionX = !UpScaleResolutionX;

			ReshapeWindow(tru);
		}
	}

	//Left Alt
	if (SatDebugInputLeftCtrl  != tru &&
		SatDebugInputRightCtrl != tru &&
		SatDebugInputLeftAlt   == tru &&
		SatDebugInputRightAlt  != tru)
	{
		if (sat::key::CheckPressed(sf::Keyboard::BackSlash))
		{
			UpScaleResolutionY = !UpScaleResolutionY;

			ReshapeWindow(tru);
		}
	}


	if (SatDebugInputLeftCtrl  != tru &&
		SatDebugInputRightCtrl != tru &&
		SatDebugInputLeftAlt   != tru &&
		SatDebugInputRightAlt  == tru)
	{

	}


	if (SatDebugInputLeftCtrl  != tru &&
		SatDebugInputRightCtrl == tru &&
		SatDebugInputLeftAlt   != tru &&
		SatDebugInputRightAlt  != tru)
	{

	}



	if (SatDebugInputLeftCtrl == tru &&
		//SatDebugInputRightCtrl  	== tru &&
		SatDebugInputLeftAlt == tru &&
		//SatDebugInputRightAlt		== tru &&
		//sat::key::CheckKey(sf::Keyboard::Tilde) &&
		//sat::key::CheckKey(sf::Keyboard::Tab) &&
		sat::key::CheckPressed(sf::Keyboard::Return) &&
		//sat::key::CheckKey(sf::Keyboard::Z) &&
		tru)
	{

		SatDebugMode = !SatDebugMode;
		if (SatDebugMode)
		{
			std::cout << "DEBUG MODE ACTIVE" << std::endl;
			//timeContrastNegative = timeContrastMax * 2.0;
			//timeContrastNegative = 1.0f;
			//timeContrast = 0.0f;
			timeFaceFlash = 1.0f;
			FaceFlashPick = rand() % numOfFaceFlash;
		}
		else
		{
			std::cout << "DEBUG MODE DEACTIVATED" << std::endl;
			//timeContrast = timeContrastMax * 4.0;
			timeFaceFlash = 1.0f;
			FaceFlashPick = rand() % numOfFaceFlash;
			//timeContrastNegative = 0.0f;
		}
	}

	if (SatDebugMode == tru)
	{
		if (
			SatDebugInputRightCtrl == tru &&
			SatDebugInputRightAlt == tru &&
			sat::key::CheckKey(sf::Keyboard::LShift) &&
			sat::key::CheckPressed(sf::Keyboard::Return) &&
			tru)
		{
			timeFaceFlash = 1.0f;
			FaceFlashPick = 8;

			ScoreBoard.resetHiScore(0, (int)(defaultScore * defaultScoreMult * 256));
			for (int c = 1; c <= LevelManager::levelFinalNum; ++c)
			{
				ScoreBoard.resetHiScore(c, (int)(defaultScore * defaultScoreMult));
			}

			ScoreBoard.hiScore = ScoreBoard.readScore(1, LevelManager::levelCurrent + 1);

			//timeContrastNegative = 1.0f;
			timeContrast = 0.0f;
		}


		if (SatDebugInputLeftCtrl != tru &&
			SatDebugInputRightCtrl != tru &&
			SatDebugInputLeftAlt != tru &&
			SatDebugInputRightAlt != tru)
		{
			
			sat::key::toggleBoolean(sf::Keyboard::F5, SatDebugTimerPauseActive);

			//if (sat::key::CheckPressed(sf::Keyboard::F8))
			//{
			//	std::cout << std::endl;
			//	std::cout << std::endl;
			//
			//	//ScoreBoard.enterScore(HighScore * ScoreMult, LevelManager::levelCurrent);
			//
			//	for (int i = 1; i <= 10; ++i)
			//	{
			//		std::cout << ScoreBoard.readScore(i, LevelManager::levelCurrent) << std::endl;
			//	}
			//
			//	std::cout << std::endl;
			//	std::cout << std::endl;
			//	//ScoreBoard.addToHiScore(HighScore * ScoreMult);
			//}

			if (sat::key::CheckPressed(sf::Keyboard::F9))
			{
				NoDamageTime = NoDamageTimeMax;
				NoDamageTime = 9999.0f;
				ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
			}

			if (sat::key::CheckPressed(sf::Keyboard::F10))
			{
				DoubleShootTime = DoubleShootTimeMax;
				DoubleShootTime = 9999.0f;
			}

			if (sat::key::CheckPressed(sf::Keyboard::F11))
			{
				AutoShootTime = AutoShootTimeMax;
				AutoShootTime = 9999.0f;
			}

			//if (sat::key::CheckKey(sf::Keyboard::F11))
			//{
			//	CurrentDistance += 1.0f;
			//}

			sat::key::toggleBoolean(sf::Keyboard::F1 , SatDebugDrawVisualActive		);
			sat::key::toggleBoolean(sf::Keyboard::F2 , SatDebugDrawEnemyActive		);
			sat::key::toggleBoolean(sf::Keyboard::F3 , SatDebugDrawBackgroundActive	);
			sat::key::toggleBoolean(sf::Keyboard::F4 , SatDebugDrawHUDActive);
			//sat::key::toggleBoolean(sf::Keyboard::F5 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F6 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F7 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F8 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F9 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F10 , boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F11, boolNothing);
			//sat::key::toggleBoolean(sf::Keyboard::F12, boolNothing);

			if (sat::key::CheckPressed(sf::Keyboard::Numpad0))
			{
				ResourceManager::reloadShaders();
			}

			

			if (sat::key::CheckPressed(sf::Keyboard::Numpad1))
			{
				if (numOfSSAOSamples > 1)
				{
					numOfSSAOSamples = numOfSSAOSamples >> 1;
					setUpSSAOKernel = false;
					buildSSAO();
				}
			}

			if (sat::key::CheckPressed(sf::Keyboard::Numpad2))
			{
				if (numOfSSAOSamples < 128)
				{
					numOfSSAOSamples = numOfSSAOSamples << 1;
					setUpSSAOKernel = false;
					buildSSAO();
				}
			}

			//if (sat::key::CheckPressed(sf::Keyboard::Numpad1, 60))
			//{
			//	PewSound.playSound(2.0f);
			//}
			//
			//if (sat::key::CheckPressed(sf::Keyboard::Numpad2, 60))
			//{
			//	BlarpSound.playSound(2.0f);
			//}
			//
			//if (sat::key::CheckPressed(sf::Keyboard::Numpad3, 60))
			//{
			//	FizzSound.playSound(2.0f);
			//}
			//
			//if (sat::key::CheckPressed(sf::Keyboard::Numpad4, 60))
			//{
			//	BzztSound.playSound(2.0f);
			//}
			//
			//if (sat::key::CheckPressed(sf::Keyboard::Numpad5, 60))
			//{
			//	BoomSound.playSound(2.0f);
			//}
			//
			//if (sat::key::CheckPressed(sf::Keyboard::Numpad6, 60))
			//{
			//	WallVoiceSound.playSound(2.0f);
			//}


			

			

			if (sat::key::CheckPressed(sf::Keyboard::Num1))
			{
				if (wireframeToggle == GL_LINE)	//	wireframeToggle = !wireframeToggle;
				{
					wireframeToggle = GL_FILL;
					glPolygonMode(GL_FRONT_AND_BACK, wireframeToggle);
					//glPolygonMode(GL_BACK, GL_LINE);
					glLineWidth(0.0f);
					//glDisable(GL_LINE_SMOOTH);
					glEnable(GL_CULL_FACE);
					glCullFace(GL_BACK);
				}
				else if (wireframeToggle == GL_FILL)
				{
					wireframeToggle = GL_LINE;
					glPolygonMode(GL_FRONT_AND_BACK, wireframeToggle);
					//glPolygonMode(GL_BACK, GL_LINE);
					glLineWidth(0.0f);
					//glEnable(GL_LINE_SMOOTH);
					glDisable(GL_CULL_FACE);
					glCullFace(GL_BACK);
				}
			}

			sat::key::toggleBoolean(sf::Keyboard::Num2, postProcessBloomActive);
			if(sat::key::toggleBoolean(sf::Keyboard::Num3, UpScaleSobelResolution) != 0)
			{
				ReshapeWindow(tru);
			}
			sat::key::toggleBoolean(sf::Keyboard::Num4, postProcessGreyscalePlusEmissiveActive);
			if(sat::key::addInt(sf::Keyboard::Num5, postProcessDepthOfFieldActive, 1))
			{
				postProcessDepthOfFieldActive = postProcessDepthOfFieldActive % (DepthOfFieldPostShader.size()+1);
				if(postProcessDepthOfFieldActive > 0)
					DepthOfFieldPostShaderPtr = &DepthOfFieldPostShader[postProcessDepthOfFieldActive - 1];
				else
					DepthOfFieldPostShaderPtr = &DepthOfFieldPostShader[0];
			}
			sat::key::toggleBoolean(sf::Keyboard::Num6, postProcessHueShiftActive);
			sat::key::toggleBoolean(sf::Keyboard::Num7, postProcessColorGrainActive);
			sat::key::toggleBoolean(sf::Keyboard::Num8, postProcessFilmGrainActive);
			//sat::key::toggleBoolean(sf::Keyboard::Num9, postProcessContrastActive);

			if (sat::key::CheckPressed(sf::Keyboard::Num9))
			{
				HighScore *= cheatSuperMult;
				adjust.x = 0.0f;
			}

			if (sat::key::CheckPressed(sf::Keyboard::Num0))
			{
				HighScore *= cheatSuperMult;
				adjust.x = curLevel->WinPoint * 0.9;
			}

			if (sat::key::CheckPressed(sf::Keyboard::Dash, 0.5f) && ShipHealth > 0)
			{
				HighScore /= cheatMult * cheatMult * cheatMult * cheatMult * cheatMult * cheatMult * cheatMult;
				//--ShipHealth;
				collisionTime = 0.0f;
				damageShip(tru);
			}

			if (sat::key::CheckPressed(sf::Keyboard::Equal, 0.5f) && ShipHealth < 3)
			{
				HighScore *= cheatSuperMult;
				++ShipHealth;
				timeDead = 0.0f;
				collisionTime = 0.0f;
				Cheater(0.75f);
			}



		}
		//Left Control + Key Debug
		elf(SatDebugInputLeftCtrl == tru &&
			SatDebugInputRightCtrl != tru  &&
			SatDebugInputLeftAlt != tru &&
			SatDebugInputRightAlt != tru)
		{			
			sat::key::toggleBoolean(sf::Keyboard::F1 , SatDebugAutoPauseWindowActive);
			if (sat::key::CheckPressed(sf::Keyboard::F2))
			{
				++SatDebugFrameRateLimit;
				SatDebugFrameRateLimit = SatDebugFrameRateLimit % 5;
				if(SatDebugFrameRateLimit == 0)
				{
					sfWindow->setFramerateLimit(0);
					printf("Framerate unlocked\n");
				}
				else
				{
					int framerate = 60 / SatDebugFrameRateLimit;
					sfWindow->setFramerateLimit(framerate);
					printf("Framerate is: %i\n", framerate);
				}
			}
			
			if (sat::key::CheckPressed(sf::Keyboard::F5, 0.1f))
			{
				TimeInLevel += 10.0;
			}
						
			if(sat::key::toggleBoolean(sf::Keyboard::F9, SatDebugForce16x9))
			{
				ReshapeWindow(tru);
			}
			

			sat::key::toggleBoolean(sf::Keyboard::Num1, postProcessCRTActive);
			sat::key::toggleBoolean(sf::Keyboard::Num2, postProcessScanlineActive);
			sat::key::toggleBoolean(sf::Keyboard::Num3, postProcessRGBSplitEarthquakeActive);
			sat::key::toggleBoolean(sf::Keyboard::Num4, postProcessGreyscaleActive);
			sat::key::toggleBoolean(sf::Keyboard::Num5, postProcessWaveActive);
			sat::key::toggleBoolean(sf::Keyboard::Num6, postProcessWave2Active);
			sat::key::toggleBoolean(sf::Keyboard::Num7, EnviromentMapActive);
			sat::key::toggleBoolean(sf::Keyboard::Num8, RimLightActive);
			sat::key::toggleBoolean(sf::Keyboard::Num9, postProcessFXAAActive);
			sat::key::toggleBoolean(sf::Keyboard::Num0, postProcessFogActive);

			if (sat::key::CheckPressed(sf::Keyboard::PageUp, 0.25f))
			{
				eyeConvergenceAdd = Min(50.0f, eyeConvergenceAdd + 1.0f);
				std::cout << "EyeConverge: \t" << eyeConvergenceAdd + eyeConvergence << std::endl;
			}

			if (sat::key::CheckPressed(sf::Keyboard::PageDown, 0.25f))
			{
				eyeConvergenceAdd = Max(-50.0f, eyeConvergenceAdd - 1.0f);
				std::cout << "EyeConverge: \t" << eyeConvergenceAdd + eyeConvergence << std::endl;
			}


			if (sat::key::CheckPressed(sf::Keyboard::F4))
			{
				GlobalSettings::initSettings();

				curLevel->UnloadLevelSounds();
				//curLevel->UnloadLevelSounds();
				//LevelManager::levelCurrent = 0;
				
				//setupLevel();
				//setSong();
				//winGame();

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					if (LevelManager::level[i].Song.isPlaying())
					{
						LevelManager::level[i].Song.stopSound();
					}

					if (LevelManager::level[i].Song.loaded)
					{
						//LevelManager::level[i].Song.release();
					}
				}

				std::vector<SoundLoc> songLoaded;
				songLoaded.resize(LevelManager::level.size());

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					songLoaded[i] = LevelManager::level[i].Song;
				}
				
				//TitleScreenActive = tru;
				//WinScreenActive = true;
				//SpecialThanksActive = true;

				LevelManager::level = LevelManager::MakeLevels();

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					LevelManager::level[i].Song = songLoaded[i];
				}
				
				LevelManager::LoadLevelAssets();
								
				curLevel = &LevelManager::level[LevelManager::levelCurrent];
				
				

				//curLevel->Load();
				//curLevel->readLevel(&LevelManager::levelFilenames[LevelManager::levelCurrent]); // curLevel->filename
				//curLevel->removeStuff();
				//curLevel->LoadLevelSounds();

				satVec3 tempPosition = adjust;
				satVec3 tempTranslation = TotalTranslation;

				HighScore = 0;
				ScoreMult = 0;
				LevelManager::levelCurrent--;
				winLevel();

				adjust = tempPosition;
				TotalTranslation = tempTranslation;

				if(!LevelManager::level[LevelManager::currentSongPlaying].Song.isPlaying())
				{
					LevelManager::level[LevelManager::currentSongPlaying].Song.playSound();
				}

				//Sys.update(satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 1.0f), satVec3(0.0f, 1.0f, 0.0f));

				//for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				//{
				//	LevelManager::level[i].Load();
				//
				//}

				//TitleScreenActive = tru;
				//WinScreenActive = true;
				//SpecialThanksActive = true;


				//ShipHealth = 8191;
				//ShipHealth = 8192;
			}

			
			if (sat::key::CheckPressed(sf::Keyboard::RBracket))
			{
				--UpScaleShadowResolution;

				if (UpScaleShadowResolution < 0)
				{
					UpScaleShadowResolution += UpScaleShadowResolutionRange;
				}

				ReshapeWindow(tru);
			}

			if (sat::key::CheckPressed(sf::Keyboard::LBracket))
			{
				++UpScaleShadowResolution;

				if (UpScaleShadowResolution >= UpScaleShadowResolutionRange)
				{
					UpScaleShadowResolution = 0;
				}

				ReshapeWindow(tru);
			}
		}
		//Left Alt + Key Debug
		elf(SatDebugInputLeftCtrl != tru &&
			SatDebugInputRightCtrl != tru &&
			SatDebugInputLeftAlt == tru &&
			SatDebugInputRightAlt != tru)
		{
			if(sat::key::toggleBoolean(sf::Keyboard::Num1, SatDebugAutoScrollActive) == -1)
			{
				MoveSpeed.x = 0.0f;
			}
			int godCheck = sat::key::toggleBoolean(sf::Keyboard::Num2, SatDebugGodModeActive);
			if (godCheck == 1)
			{ 
				Cheater();			
				ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
			}
			elf(godCheck == -1)
			{
				NoDamageTime = 0.0f;				
				ChargeDownSound.playSound(1.0f, 1.0f, 1.0f);
				timeContrast = timeContrastMax * 2.0f; 
			}
			sat::key::toggleBoolean(sf::Keyboard::Num3, postProcessRGBSplitActive);
			sat::key::toggleBoolean(sf::Keyboard::Num4, postProcessChromaticAberrationActive);
			sat::key::toggleBoolean(sf::Keyboard::Num5, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num6, postProcessSobelActive);
			sat::key::toggleBoolean(sf::Keyboard::Num7, postProcessSobelThickActive);
			sat::key::toggleBoolean(sf::Keyboard::Num8, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num9, SatDebugDrunkModeActive);
			sat::key::toggleBoolean(sf::Keyboard::Num0, SatDebugSluggishMovementActive);

			if (sat::key::CheckPressed(sf::Keyboard::PageUp, 0.25f))
			{
				eyeDistance *= 1.5f;
				eyeDistanceOrtho *= 1.5f;
				std::cout << "EyeDistance: \t" << eyeDistance << std::endl;
			}

			if (sat::key::CheckPressed(sf::Keyboard::PageDown, 0.25f))
			{
				eyeDistance /= 1.5f;
				eyeDistanceOrtho /= 1.5f;
				std::cout << "EyeDistance: \t" << eyeDistance << std::endl;
			}

			if (TitleScreenActive == true && SpecialThanksActive == true && WinScreenActive == true)
			{
				if (sat::key::CheckPressed(sf::Keyboard::Dash, 0.25f) && LevelManager::levelCurrent > 0)
				{
					HighScore = 0.0f;
					LevelManager::levelCurrent -= 2;
					winLevel();
					Game::adjust.x = 10.0f;
				}

				if (sat::key::CheckPressed(sf::Keyboard::Equal, 0.25f) && LevelManager::levelCurrent < LevelManager::levelFinalNum)
				{
					HighScore = 0.0f;
					winLevel();
					Game::adjust.x = 10.0f;
				}				
			}

			
			if (sat::key::CheckPressed(sf::Keyboard::F5, 0.1f))
			{
				TimeInLevel = Max(TimeInLevel - 10.0f, 0.0);				
			}


			if (sat::key::CheckPressed(sf::Keyboard::F9))
			{
				NoDamageTime = 5.0f;				
				ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
			}

			if (sat::key::CheckPressed(sf::Keyboard::F10))
			{
				DoubleShootTime = 5.0f;
			}

			if (sat::key::CheckPressed(sf::Keyboard::F11))
			{
				AutoShootTime = 5.0f;
			}

			
		}
		//Right Control + Key Debug
		elf(SatDebugInputLeftCtrl != tru &&
			SatDebugInputRightCtrl == tru &&
			SatDebugInputLeftAlt != tru &&
			SatDebugInputRightAlt != tru)
		{
			sat::key::toggleBoolean(sf::Keyboard::Num1, postProcessSSAOActive);
			sat::key::toggleBoolean(sf::Keyboard::Num2, postProcessBlurSSAOActive);
			sat::key::toggleBoolean(sf::Keyboard::Num3, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num4, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num5, SatDebugShipWaggleToggleActive);
			sat::key::toggleBoolean(sf::Keyboard::Num6, SatDebugCascadeToggleActive);
			sat::key::toggleBoolean(sf::Keyboard::Num7, SatDebugWavyToggleActive);
			sat::key::toggleBoolean(sf::Keyboard::Num8, SatDebugDeferredLightActive);
			sat::key::toggleBoolean(sf::Keyboard::Num9, SatDebugShadowMapActive);

			if (sat::key::CheckPressed(sf::Keyboard::Num0))
			{
				SatDebugDisplayMode = 0;
			}

			if (sat::key::CheckPressed(sf::Keyboard::Dash, 0.25f))
			{
				--SatDebugDisplayMode;
				SatDebugDisplayMode += SatDebugDisplayModeSize;
				SatDebugDisplayMode = SatDebugDisplayMode % SatDebugDisplayModeSize;
			}

			if (sat::key::CheckPressed(sf::Keyboard::Equal, 0.25f))
			{
				++SatDebugDisplayMode;
				SatDebugDisplayMode += SatDebugDisplayModeSize;
				SatDebugDisplayMode = SatDebugDisplayMode % SatDebugDisplayModeSize;
			}

			if (sat::key::CheckPressed(sf::Keyboard::F4))
			{
				curLevel->UnloadLevelSounds();
				//curLevel->UnloadLevelSounds();
				//LevelManager::levelCurrent = 0;
				
				//setupLevel();
				//setSong();
				//winGame();

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					if (LevelManager::level[i].Song.isPlaying())
					{
						LevelManager::level[i].Song.stopSound();
					}

					if (LevelManager::level[i].Song.loaded)
					{
						//LevelManager::level[i].Song.release();
					}
				}

				std::vector<SoundLoc> songLoaded;
				songLoaded.resize(LevelManager::level.size());

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					songLoaded[i] = LevelManager::level[i].Song;
				}
				
				//TitleScreenActive = tru;
				//WinScreenActive = true;
				//SpecialThanksActive = true;

				LevelManager::level = LevelManager::MakeLevels();

				for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				{
					LevelManager::level[i].Song = songLoaded[i];
				}
				
				LevelManager::LoadLevelAssets();
								
				curLevel = &LevelManager::level[LevelManager::levelCurrent];
				
				

				//curLevel->Load();
				//curLevel->readLevel(&LevelManager::levelFilenames[LevelManager::levelCurrent]); // curLevel->filename
				//curLevel->removeStuff();
				//curLevel->LoadLevelSounds();

				satVec3 tempPosition = adjust;
				satVec3 tempTranslation = TotalTranslation;

				HighScore = 0;
				ScoreMult = 0;
				LevelManager::levelCurrent--;
				winLevel();

				adjust = tempPosition;
				TotalTranslation = tempTranslation;

				if(!LevelManager::level[LevelManager::currentSongPlaying].Song.isPlaying())
				{
					LevelManager::level[LevelManager::currentSongPlaying].Song.playSound();
				}

				//Sys.update(satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 1.0f), satVec3(0.0f, 1.0f, 0.0f));

				//for (int i = 0; i < LevelManager::levelFinalNum; ++i)
				//{
				//	LevelManager::level[i].Load();
				//
				//}

				//TitleScreenActive = tru;
				//WinScreenActive = true;
				//SpecialThanksActive = true;


				//ShipHealth = 8191;
				//ShipHealth = 8192;
			}

		}
		//Right Alt + Key Debug
		elf(SatDebugInputLeftCtrl != tru &&
			SatDebugInputRightCtrl != tru &&
			SatDebugInputLeftAlt != tru &&
			SatDebugInputRightAlt == tru)
		{
			sat::key::toggleBoolean(sf::Keyboard::Num1, postProcessSepiaActive);
			sat::key::toggleBoolean(sf::Keyboard::Num2, postProcessSepiaPlusEmissiveActive);
			sat::key::toggleBoolean(sf::Keyboard::Num3, postProcessInvertActive);
			sat::key::toggleBoolean(sf::Keyboard::Num4, postProcessInvertBrightnessActive);
			sat::key::toggleBoolean(sf::Keyboard::Num5, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num6, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num7, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num8, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num9, boolNothing);
			sat::key::toggleBoolean(sf::Keyboard::Num0, boolNothing);

			
			
			
			if (sat::key::CheckPressed(sf::Keyboard::F5, 0.2f))
			{
				TimeInLevel += Timer::framef();
			}


		}

		if (SatDebugGodModeActive && !PauseActive)
		{
			HighScore *= cheatMult;
			//ShipHealth = 3;
			NoDamageTime = 5.0f;			
			//ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
		}



		if (ShipHealth >= 0 && TitleScreenActive != tru)
		{
			if (sat::key::CheckKey(sf::Keyboard::I))
			{
				TotalTranslation.y -= 16.0f * (float)Timer::framef();
			}

			if (sat::key::CheckKey(sf::Keyboard::K))
			{
				TotalTranslation.y += 16.0f * (float)Timer::framef();
			}

			if (sat::key::CheckKey(sf::Keyboard::J))
			{
				HighScore *= cheatMult;

				adjust.x -= (16.0f + 2.0f * speedMod.x * speedMult.x) * Timer::framef() * (SatDebugInputLeftShift + 1);
				if (adjust.x <= 0)
				{
					adjust.x = 0.0f;
				}
			}

			if (sat::key::CheckKey(sf::Keyboard::L))
			{
				HighScore *= cheatMult;

				if (adjust.x < curLevel->WinPoint)
				{
					adjust.x += (16.0f + 1.0f * speedMod.x * speedMult.x) * Timer::framef() * (SatDebugInputLeftShift + 1);
				}
				else
				{
					winLevel();
				}
			}
		}

		//if (sat::key::CheckPressed(sf::Keyboard::LBracket))
		//{
		//	cycleLightMode();
		//}

















	}

#pragma endregion DebugKeyboard

	if (sat::key::CheckPressed(sf::Keyboard::O))
	{
		OrthographicActive = !OrthographicActive;
		
		if(OrthographicActive)
			printf("Orthographic Active!\n");
		else
			printf("Perspective Active!\n");

		calcCamera();
	}
}

void Game::KeyboardUpdate(const int &updateNumber)
{
	sf::Joystick::update();

	posMouse = sat::mouse::Pos();

	KeyboardDebugUpdate();

	//if (sat::key::CheckPressed(sf::Keyboard::Num1))
	//{
	//	if (wireframeToggle == GL_LINES)	//	wireframeToggle = !wireframeToggle;
	//	{
	//		wireframeToggle = GL_TRIANGLES;
	//	}
	//	else if (wireframeToggle == GL_TRIANGLES)
	//	{
	//		wireframeToggle = GL_LINES;
	//	}
	//}

	//No Ctrl/Alt + Key Debug


	if (TitleScreenActive == true)
	{
		if (!PauseActive && (sat::key::CheckPressed(sf::Keyboard::Escape) || sat::joy::CheckPressed(Xbox360Buttons::Start)))
		{
			PauseActive = tru;			
		}
		
		
	}

	

	if (collisionTime > 0.0f)
	{
		collisionTime -= Timer::framef();
		if (collisionTime <= 0.0f)
		{
			//songPitch = 1.0f;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(songPitch);
		}
	}

	

	if (ShipHealth > 0 && TitleScreenActive != tru)
	{
		DieSpeedMult = 1.0f;

		if (sat::key::CheckKey(sf::Keyboard::D) || sat::key::CheckKey(sf::Keyboard::Right))
		{
			InputMove.x += 1.0f;
		}	

		if (sat::key::CheckKey(sf::Keyboard::A) || sat::key::CheckKey(sf::Keyboard::Left))
		{
			InputMove.x -= 1.0f;
		}		

		if (sat::key::CheckKey(sf::Keyboard::W) || sat::key::CheckKey(sf::Keyboard::Up))
		{
			InputMove.y += 1.0f;
		}

		if (sat::key::CheckKey(sf::Keyboard::S) || sat::key::CheckKey(sf::Keyboard::Down))
		{
			InputMove.y -= 1.0f;
		}	
		
		//ControllerUpdate();

		satVec2 InputController = satVec2(
			- sat::joy::CheckAxis(sf::Joystick::R, controllerXDeadzone) // Bad
			+ sat::joy::CheckAxis(sf::Joystick::U, controllerXDeadzone) // Bad
			+ sat::joy::CheckAxis(sf::Joystick::X, controllerXDeadzone)	// Two of these 3 are bad
			- sat::joy::CheckAxis(sf::Joystick::Z, controllerDeadzone * 0.125f)
			+ sat::joy::CheckAxis(sf::Joystick::PovX, controllerDeadzone) * 2.0f, 

			- sat::joy::CheckAxis(sf::Joystick::Y, controllerDeadzone) 
			+ sat::joy::CheckAxis(sf::Joystick::PovY, controllerDeadzone) * 2.0f);
		InputController *= 0.01f;
		InputController.MinMax(-1.0f, 1.0f);
		//InputController = 0.0f;


		float shootDelay = 0.25f;
		if (AutoShootTime > 0.0f)
		{
			shootDelay = 0.01f;
		}

		static satVec3 arduinoVectors = 0.0f;
		int arduinoShoot = -1;
		static float arduinoShootTime = 1.0f;
		if (sat::ard::ArduinoActive() && sat::ard::ParseLine(arduinoVectors, arduinoShoot))
		{
			std::cout << (arduinoVectors) << "\t" << arduinoShoot << std::endl;
			arduinoVectors /= 3000.0f;
			InputMove.y += arduinoVectors.y;
			//arduinoVectors /= 5000.0f;
			InputMove.x += arduinoVectors.z;

			if (arduinoShoot == 0)
			{
				arduinoShootTime = 0.0f;
			}

			if (arduinoShoot && arduinoShootTime > 0.0f)
			{
				arduinoShootTime += Timer::framef();
				arduinoShoot = 0;
			}
			else if (arduinoShoot && arduinoShootTime == 0.0f)
			{
				arduinoShootTime += Timer::framef();
				arduinoShoot = 2;
			}

			if (arduinoShootTime > shootDelay)
			{
				arduinoShootTime = Timer::framef();
				arduinoShoot = 2;
			}
			
		}
		
		//if(InputController != 0.0f)
		//{
		//	ControllerActive = tru;
		//}
		ControllerActive = ((InputController.x >= controllerXDeadzone || InputController.x <= -controllerXDeadzone) && (InputController.y >= controllerDeadzone || InputController.y <= -controllerDeadzone));


		InputMove += InputController;
		InputMove.MinMax(-1.0f, 1.0f);

		//InputMove.x = Between(InputMove.x, -1.0f, 1.0f);
		//InputMove.y = Between(InputMove.y * abs(InputMove.y), -1.0f, 1.0f);

		float averageInputRange = 15.0f;

		if (InputMove.x < 0.0001 && InputMove.x > -0.0001)
		{
			averageInput.x = Lerp(0.0f, averageInput.x, 0.8f);
		}
		if (InputMove.y < 0.0001 && InputMove.y > -0.0001)
		{
			averageInput.y = Lerp(0.0f, averageInput.y, 0.8f);
		}

		if (InputMove.y * averageInputRange < averageInput.y && averageInput.y > 0.0001)
		{
			//averageInput += InputMove * 0.8;
			averageInput.y = Lerp(0.0f, averageInput.y, 0.95f);
		}

		if (InputMove.y * averageInputRange > averageInput.y && averageInput.y < -0.0001)
		{
			//averageInput += InputMove * 0.8;
			averageInput.y = Lerp(0.0f, averageInput.y, 0.95f);
		}

		averageInput += InputMove * 0.8f * 2.0f * Timer::framef() * 60.0f;	
		averageInput.y = Between(averageInput.y, -averageInputRange, averageInputRange);
		//if (sat::key::CheckKey(sf::Keyboard::LShift) || sat::key::CheckKey(sf::Keyboard::RShift))
		//{
		//	//ShipShrink += 0.1f;
		//}
		
		if(!SatDebugAutoScrollActive)
		{
			MoveSpeed.x = 0.0f;
			MoveSpeed.x += InputMove.x;
		}

		if(MoveSpeed.x < 0.05f && MoveSpeed.x > -0.05f && InputMove.x == 0.0f)
		{
			MoveSpeed.x = Lerp(MoveSpeed.x, 0.0f, 1.0f * Timer::framef());
		}
		if(MoveSpeed.y < 1.1f && MoveSpeed.y > -1.1f && InputMove.y == 0.0f)
		{
			if(SatDebugDrunkModeActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 6.0f * Timer::framef());
			}
			else if(SatDebugSluggishMovementActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 18.0f * Timer::framef());
			}
			else
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 30.0f * Timer::framef());
			}
		}
		//MoveSpeed = Lerp(MoveSpeed, satVec2(0.0f), 0.2f);
		if(InputMove.x < 0.0f)
		{			
			float speedDropoff = InverseLerp(MoveSpeed.x, -2.0f, 1.0f);
			MoveSpeed.x += InputMove.x * speedDropoff * 3.5f * Timer::framef();
		}
		else
		{
			float speedDropoff = InverseLerp(MoveSpeed.x, 1.05f, -1.0f);
			MoveSpeed.x += InputMove.x * powf(speedDropoff, 1.00f) * 1.500f * Timer::framef();
			//MoveSpeed.x += InputMove.x * 0.125f * Timer::framef();
		}


		float oppositeSpeedIncreaseMult = 1.0f;

		if ((MoveSpeed.y < 0.0f && InputMove.y > 0.0f) ||
			(MoveSpeed.y > 0.0f && InputMove.y < 0.0f))
		{
			oppositeSpeedIncreaseMult * 2.0f;
			
			if(SatDebugDrunkModeActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 6.0f * Timer::framef());
			}
			else if(SatDebugSluggishMovementActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 18.0f * Timer::framef());
			}
			else
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 1000.0f * Timer::framef());
			}
		}

		if(abs(InputMove.y) >= abs(MoveSpeed.y))
		{
			if(SatDebugDrunkModeActive)
			{
				MoveSpeed.y += InputMove.y * 3.0f * Timer::framef() * oppositeSpeedIncreaseMult;
			}
			else if(SatDebugSluggishMovementActive)
			{
				MoveSpeed.y += InputMove.y * 12.0f * Timer::framef() * oppositeSpeedIncreaseMult;
			}
			else
			{
				MoveSpeed.y += InputMove.y * 30.0f * Timer::framef() * oppositeSpeedIncreaseMult;
			}
		}
		else
		{
			if(SatDebugDrunkModeActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 6.0f * Timer::framef());
			}
			else if(SatDebugSluggishMovementActive)
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 18.0f * Timer::framef());
			}
			else
			{
				MoveSpeed.y = Lerp(MoveSpeed.y, 0.0f, 30.0f * Timer::framef());
			}
		}

		MoveSpeed.x = Between(MoveSpeed.x, -1.0f, 1.0f);		
		MoveSpeed.y = Between(MoveSpeed.y, -1.0f, 1.0f);

		for(int i = 0; i < (MoveSpeed.x * 0.5f + 0.5f) * 1.0f + (rand() % 10 / 10.0f) - 0.5f; ++i)
		{
			//float ySpread = ((rand() % 100) - 50) / 100.0f;
			//ParticleN tempSmoke = ParticleN(this, curLevel, &SmokeMesh, &DefaultTexture, &sat::light::mod::smokeShipYellow, 
			//	satVec3((rand() % 10) / 100.0f, 0.0f, 0.0f), 
			//	satVec3(4.02f - abs(ySpread), ySpread, 0.0f), 
			//	satVec3(0.1f, -0.99f, 0.25f), 1.0f);
			//smokeParticle.push_back(tempSmoke);
		}

		

		if (sat::key::CheckPressed(sf::Keyboard::Space, shootDelay) || 
			sat::joy::CheckPressed(Xbox360Buttons::RightBumper, shootDelay) || 
			sat::joy::CheckPressed(Xbox360Buttons::LeftBumper, shootDelay) ||
			sat::joy::CheckPressed(Xbox360Buttons::A, shootDelay) || 
			sat::joy::CheckPressed(Xbox360Buttons::B, shootDelay) ||
			arduinoShoot == 2)
		{			
			if(adjust.x > 17.5f && adjust.x < curLevel->WinPoint - 20.0f)
			{
				float bulletSpeedX = -12.0f * 1.0f;
				float bulletLifetime = 2.0f * 1.0f;


				//float verticalVelocity = Between(-averageInput.y, -4.0f, 4.0f);
				float verticalVelocity = Between(-averageInput.y, -15.0f, 15.0f) * 0.25f;
				//float verticalVelocity = Between(-MoveSpeed.y, -1.0f, 1.0f) * 4.0;

				if(verticalCollide)
				{
					verticalVelocity = 0.0f;
				}
				//verticalVelocity = 0.0f; 

				if(DoubleShootTime > 0.0)
				{
					PewSound.playSound(0.5f, 1.0f, 1.25f);

					BulletGood tempBullet = BulletGood(this, curLevel, &BulletMesh, &DefaultNormal, 
						satVec3(0.0f, 0.0f, 0.0f), satVec3(bulletSpeedX, verticalVelocity + (rand()%17-8)/8.0f , 0.0f),
						satVec3(Lerp(-1.0f, -1.00f, ShipShrink), verticalVelocity * 0.125f + Lerp(-Timer::sinRange(110.0f, 0.25f, 0.85f), -0.675f, ShipShrink), 0.0f), bulletLifetime * 1.4f);
					bulletGood.push_back(tempBullet);
					lightBullet.push_back(PointLight(sat::color::lightcyan, bulletGood[bulletGood.size() - 1].position - satVec3(0.0f, 0.0f, 2.0f), 4.0f, 0.1f, 0.05f));
				
					tempBullet = BulletGood(this, curLevel, &BulletMesh, &DefaultNormal, 
						satVec3(0.0f, 0.0f, 0.0f), satVec3(bulletSpeedX, verticalVelocity + (rand()%17-8)/8.0f, 0.0f),
						satVec3(Lerp(-2.0f, -0.95f, ShipShrink), verticalVelocity * 0.125f + Lerp(-Timer::sinRange(90.0f, 0.25f, 0.85f), -0.725f, ShipShrink), 0.0f), bulletLifetime * 1.2f);
					bulletGood.push_back(tempBullet);
					lightBullet.push_back(PointLight(sat::color::lightcyan, bulletGood[bulletGood.size() - 1].position - satVec3(0.0f, 0.0f, 2.0f), 4.0f, 0.1f, 0.05f));
				
					tempBullet = BulletGood(this, curLevel, &BulletMesh, &DefaultNormal,
						satVec3(0.0f, 0.0f, 0.0f), satVec3(bulletSpeedX, verticalVelocity + (rand()%17-8)/8.0f, 0.0f),
						satVec3(Lerp(-3.0f, -1.05f, ShipShrink), verticalVelocity * 0.125f + Lerp(-Timer::sinRange(100.0f, 0.25f, 0.85f), -0.70f, ShipShrink), 0.0f), bulletLifetime);
					bulletGood.push_back(tempBullet);
					lightBullet.push_back(PointLight(sat::color::lightcyan, bulletGood[bulletGood.size() - 1].position - satVec3(0.0f, 0.0f, 2.0f), 4.0f, 0.1f, 0.05f));

				}
				else
				{		
					FizzSound.playSound(0.5f, 1.0f, 1.25f);

					BulletGood tempBullet = BulletGood(this, curLevel, &BulletMesh, &DefaultNormal, 
						satVec3(0.0f, 0.0f, 0.0f), satVec3(bulletSpeedX, verticalVelocity, 0.0f),
						satVec3(Lerp(-3.0f, -1.0f, ShipShrink), verticalVelocity * 0.125f + Lerp(-0.55f, -0.70f, ShipShrink), 0.0f), bulletLifetime);
					bulletGood.push_back(tempBullet);
					lightBullet.push_back(PointLight(sat::color::lightcyan, bulletGood[bulletGood.size() - 1].position - satVec3(0.0f, 0.0f, 2.0f), 4.0f, 0.1f, 0.05f));
				}
			}
		}

		MouseUpdate();
		
		if (collisionTime > 0.0f && IntroEndParam > 0.95f)
		{
			float sinRangeParam = InverseLerp(collisionTime, 0.0f, invincibleTime) * 0.125f * 0.5f;

			float freqParam = Timer::sinRange(10.0f, 1.0f - sinRangeParam, 1.0f + sinRangeParam);
			songPitch *= freqParam;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(freqParam);
		}

		float freqAdjust = 0.0f;
		if(adjust.x < 26.5f)
		{
			freqAdjust = InverseLerp(adjust.x, 25.0f, 0.0f);
			float freqParam = 1.0f - freqAdjust * 1.0f;
			songPitch *= freqParam;
			songVolume *= 1.0f;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch();
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume(1.0f);
		}
		if(adjust.x > curLevel->WinPoint - 21.5f)
		{
			freqAdjust = InverseLerp(adjust.x, curLevel->WinPoint - 20.0f, curLevel->WinPoint - 0.0f);
			float fadeout = 1.0f - InverseLerp(adjust.x, curLevel->WinPoint - 20.0f, curLevel->WinPoint - 0.0f);
			float freqParam = 1.0f + powf(freqAdjust, 3.0f) * 36.0f;
			fadeout = powf(fadeout, 2.0f);
			songPitch *= freqParam;
			songVolume *= fadeout;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch();
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume());
				}
			

	}
	else if (ShipHealth == 0)
	{
		timeDead += 0.3f * Timer::framef();
		//0.025f
		float freqParam;
		float volumeParam = 1.0f;
		if (timeDead < timeDeadReverse)
		{
			freqParam = Lerp(1.0f, 0.0f, InverseLerp(timeDead, 0.0f, timeDeadReverse));
			timeDeadRewindPosition = adjust.x;
			timeDeadRewindTime = TimeInLevel;
		}
		else
		{
			freqParam = Lerp(0.0f, -4.0f, InverseLerp(timeDead, timeDeadReverse, timeDeadMax));
			volumeParam = Lerp(1.0f, 0.0f, powf(InverseLerp(timeDead, timeDeadReverse, timeDeadMax), 0.85f));

		}
		songPitch *= freqParam;
		songVolume *= volumeParam;
		//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(songPitch);
		//std::cout << timeDead << std::endl;

		if(timeDead < timeDeadReverse)
		{
			TotalTranslation.y += Timer::framef() * timeDead * FRAMES_PER_SECOND * 2.0;
		}
		else
		{
			TotalTranslation.y = Lerp(ShipSpawnPosition.y, TotalTranslation.y, 1.0f - FPU * 2.0f);
			//TotalTranslation.y -= Timer::framef() * ((timeDeadMax) - (timeDead - timeDeadReverse)) * FRAMES_PER_SECOND * 0.6f;
		}

		//if (TotalTranslation.y > 27.5f || timeDead > 0.4f)
		//{
		if (timeDead > timeDeadMax)
		{
			songPitch = 1.0f;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(songPitch);
			curLevel->readLevel(curLevel->filename);
			curLevel->removeStuff();
			//curLevel->Load();
			//WallVoiceSound.playSound();
			setupLevel();
		}
	}
	
}

void Game::setupLevel()
{
	timeDead = 0.0f;
	TotalTranslation.y = ShipSpawnPosition.y;
	adjust.x = ShipSpawnPosition.x;
	ShipHealth = 3;
	HighScore = defaultScore;
	TimeInLevel = 0.0f;
	ScoreMult = defaultScoreMult;
	collisionTime = 0.0f;
	
	curLevel->ShadowRotationTotal = 0.0f;
	curLevel->ShadowTranslationRotationTotal = 0.0f;

	Quaternions.LoadIdentity();
	Quaternions.Translate(initialTranslation);
	Quaternions.Translate(TotalTranslation.x, TotalTranslation.y, TotalTranslation.z);

	MoveSpeed = 0.0f;

	DoubleShootTime = 0.0f;
	AutoShootTime = 0.0f;
	NoDamageTime = 0.0f;
}

void Game::CollisionUpdate()
{
	horizontalCollide = true;
	verticalCollide = false;
	satVec2 tempPos = satVec2((TotalTranslation.y + 12.5f) / 2.0f, adjust.x + 6.0f);

	//std::cout << TotalTranslation << std::endl;

	tempPos.x = Between(tempPos.x, 0.75f, (float) curLevel->Y - 0.75f);
	tempPos.y = Between(tempPos.y, 0.75f, (float) curLevel->X - 1.5f);
	
	
	speedMod = curLevel->calcSpeed();
	speedMult = curLevel->calcSpeedMult();
	speedShrinkMult = curLevel->calcSpeedShrinkMult();
	
	if(ShipHealth == 0)
	{
		if (adjust.x < curLevel->WinPoint)
		{
			if (timeDead < timeDeadReverse)
			{
				adjust.x += DieSpeedMult.x * Lerp(speedMod.x, speedMod.x * speedShrinkMult.x, ShipShrink) * FRAME_TIME * Lerp(1.0f, 0.0f, timeDead * 2.5f);
			}
			else
			{
				//InverseLerp(timeDeadRewindPosition);
				float t = InverseLerp((timeDeadMax - timeDeadReverse) - (timeDead - timeDeadReverse), timeDeadMax - timeDeadReverse, 0.0f);
				//std::cout << t << std::endl;
				adjust.x = Lerp(timeDeadRewindPosition, 0.0f, t * t * 0.5f);
			}
		}
		else
		{	
			winLevel();	
			tempPos.y = 1;
		}

		satVec2 posOne = satVec2(Lerp(tempPos.x + 0.3f, tempPos.x + 0.45f, ShipShrink),  Lerp(tempPos.y + 1.8f, tempPos.y + 0.45f, ShipShrink));
		satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.6f, tempPos.x + 0.55f, ShipShrink),  Lerp(tempPos.y + 1.8f, tempPos.y + 0.45f, ShipShrink));
		satVec2 posTee = satVec2(Lerp(tempPos.x + 0.7f, tempPos.x + 0.575f, ShipShrink), Lerp(tempPos.y + 1.0f, tempPos.y + 0.30f, ShipShrink));
		satVec2 posFor = satVec2(Lerp(tempPos.x + 0.7f, tempPos.x + 0.575f, ShipShrink), Lerp(tempPos.y + 0.3f, tempPos.y + 0.15f, ShipShrink));
		
		LevelMatrixBlock checkOne = CollisionCheck(posOne);
		LevelMatrixBlock checkTwo = CollisionCheck(posTwo);
		LevelMatrixBlock checkTee = CollisionCheck(posTee);
		LevelMatrixBlock checkFor = CollisionCheck(posFor);
		
		if (checkOne.collision == tru ||
			checkTwo.collision == tru ||
			checkTee.collision == tru ||
			checkFor.collision == tru )
		{
			if (checkOne.collision == tru )
			{
				curLevel->ChangeTo(posOne);
			}
			elf (checkTwo.collision == tru )
			{
				curLevel->ChangeTo(posTwo);
			}
			elf (checkTee.collision == tru )
			{
				curLevel->ChangeTo(posTee);
			}
			elf (checkFor.collision == tru )
			{
				curLevel->ChangeTo(posFor);
			}
		}

		//if (checkOne == BlockBreakWall	|| checkOne == BlockWall ||
		//	checkTwo == BlockBreakWall	|| checkTwo == BlockWall ||
		//	checkTee == BlockBreakWall	|| checkTee == BlockWall ||
		//	checkFor == BlockBreakWall	|| checkFor == BlockWall	)
		//{
		//	if (checkOne == BlockBreakWall || 
		//		checkOne == BlockWall)
		//	{
		//		curLevel->lMatrix[posOne.x][posOne.y] = BlockBackWall;
		//	}
		//	elf(checkTwo == BlockBreakWall || 
		//		checkTwo == BlockWall)
		//	{
		//		curLevel->lMatrix[posTwo.x][posTwo.y] = BlockBackWall;
		//	}
		//	elf(checkTee == BlockBreakWall || 
		//		checkTee == BlockWall)
		//	{
		//		curLevel->lMatrix[posTee.x][posTee.y] = BlockBackWall;
		//	}
		//	elf(checkFor == BlockBreakWall || 
		//		checkFor == BlockWall)
		//	{
		//		curLevel->lMatrix[posFor.x][posFor.y] = BlockBackWall;
		//	}
		//	BzztSound.playSound(0.5f, 1.0f, 1.25f);
		//}
	}

	float CollisionSpeed = InverseLerp(collisionTime, invincibleTime, 0.25f);

	if(ShipHealth > 0 && (SatDebugAutoScrollActive == tru))
	{

		satVec2 posOne = satVec2(Lerp(tempPos.x + 0.375f, tempPos.x + 0.44f, ShipShrink), Lerp(tempPos.y + 1.4f, tempPos.y + 0.45f, ShipShrink));
		satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.400f, tempPos.x + 0.56f, ShipShrink), Lerp(tempPos.y + 1.4f, tempPos.y + 0.45f, ShipShrink));
			
		//char checkOne = CollisionAutoScrollUpdate(posOne);
		//char checkTwo = CollisionAutoScrollUpdate(posTwo);
		
		LevelMatrixBlock checkOne = CollisionCheck(posOne);
		LevelMatrixBlock checkTwo = CollisionCheck(posTwo);

		
		if (checkOne.kill == tru || checkTwo.kill == tru )
		{
			DieSpeedMult *= satVec2(0.0f, 1.0f);
			ShipHealth = 1;
			damageShip(tru);
			//if(checkOne.kill == tru)
			//{
			//	curLevel->ChangeTo(posOne);
			//}
			//elf(checkTwo.kill == tru)
			//{
			//	curLevel->ChangeTo(posTwo);
			//}
			//
			//horizontalCollide = tru;			
		}

		if (checkOne.breakable == tru || checkTwo.breakable == tru )
		{
			if(checkOne.breakable == tru)
			{
				curLevel->ChangeTo(posOne);
			}
			elf(checkTwo.breakable == tru)
			{
				curLevel->ChangeTo(posTwo);
			}

			horizontalCollide = tru;			
		}

		if (checkOne.health == tru || checkTwo.health == tru )
		{
			if(checkOne.health == tru)
			{
				ShipHealth += checkOne.health;
				curLevel->ChangeTo(posOne);
			}
			elf(checkTwo.health == tru)
			{
				ShipHealth += checkTwo.health;
				curLevel->ChangeTo(posTwo);
			}
			ShipHealth = Min(ShipHealth, 3);
		}
				
		if (checkOne.invincible || checkTwo.invincible)
		{
			if(checkOne.invincible)
			{
				NoDamageTime = NoDamageTimeMax;				
				ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
				curLevel->ChangeTo(posOne);
			}
			elf(checkTwo.invincible)
			{
				NoDamageTime = NoDamageTimeMax;
				ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
				curLevel->ChangeTo(posTwo);
			}
		}

		if (checkOne.doubleShoot || checkTwo.doubleShoot)
		{
			if(checkOne.doubleShoot)
			{
				DoubleShootTime = DoubleShootTimeMax;
				curLevel->ChangeTo(posOne);
			}
			elf(checkTwo.doubleShoot)
			{
				DoubleShootTime = DoubleShootTimeMax;
				curLevel->ChangeTo(posTwo);
			}
		}

		if (checkOne.autoShoot || checkTwo.autoShoot)
		{
			if(checkOne.autoShoot)
			{
				AutoShootTime = AutoShootTimeMax;
				curLevel->ChangeTo(posOne);
			}
			elf(checkTwo.autoShoot)
			{
				AutoShootTime = AutoShootTimeMax;
				curLevel->ChangeTo(posTwo);
			}
		}
		
		if (checkOne.collision == false && 
			checkTwo.collision == false)
		{				
			if (adjust.x < curLevel->WinPoint)
			{
				ScoreMult += 0.2 * Timer::framef() * FPU * speedMod.x * CollisionSpeed * IntroEndParam;
				adjust.x += Lerp(speedMod.x, speedMod.x * speedShrinkMult.x, ShipShrink) * FRAME_TIME * CollisionSpeed;
			}
			else
			{	
				winLevel();	
				tempPos.y = 1;
			}
		}
		else
		{	
			horizontalCollide = tru;	
		}
	}
		
	
		


	if (ShipHealth > 0 && (MoveSpeed.y > 0.01f || MoveSpeed.y < -0.01f))
	{
		if (MoveSpeed.x > 0.01 && !ControllerActive)
		{
			speedMult.y *= 1.5f;
		}
		elf(MoveSpeed.x > -0.01f && MoveSpeed.x < 0.01f && !ControllerActive)
		{
			speedMult.y *= 1.5f;
		}
		elf(MoveSpeed.x < -0.01f && !ControllerActive)
		{
			speedMult.y *= 1.25f;
		}
		elf(ControllerActive)
		{
			speedMult.y *= 1.5f;
		}

		if (MoveSpeed.y > 0.01f)
		{	
			
			satVec2 posOne = satVec2(Lerp(tempPos.x + 0.20f, tempPos.x + 0.300f, ShipShrink), Lerp(tempPos.y + 1.3f, tempPos.y + 0.35f, ShipShrink));
			satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.30f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 1.0f, tempPos.y + 0.30f, ShipShrink));
			satVec2 posTee = satVec2(Lerp(tempPos.x + 0.30f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 0.9f, tempPos.y + 0.15f, ShipShrink));

			LevelMatrixBlock checkOne = CollisionCheck(posOne);
			LevelMatrixBlock checkTwo = CollisionCheck(posTwo);
			LevelMatrixBlock checkTee = CollisionCheck(posTee);


			satVec2 posHurtOne = satVec2(Lerp(tempPos.x + 0.35f, tempPos.x + 0.300f, ShipShrink), Lerp(tempPos.y + 1.3f, tempPos.y + 0.35f, ShipShrink));
			satVec2 posHurtTwo = satVec2(Lerp(tempPos.x + 0.35f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 1.0f, tempPos.y + 0.30f, ShipShrink));
			satVec2 posHurtTee = satVec2(Lerp(tempPos.x + 0.35f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 0.9f, tempPos.y + 0.15f, ShipShrink));
			
			LevelMatrixBlock checkHurtOne = CollisionCheck(posHurtOne);
			LevelMatrixBlock checkHurtTwo = CollisionCheck(posHurtTwo);
			LevelMatrixBlock checkHurtTee = CollisionCheck(posHurtTee);

			//char checkOne = CollisionVerticalUpdate(posOne);
			//char checkTwo = CollisionVerticalUpdate(posTwo);
			//char checkTee = CollisionVerticalUpdate(posTee);
			

			if (checkHurtOne.breakable == tru || checkHurtTwo.breakable == tru || checkHurtTee.breakable == tru)
			{
				if(checkHurtOne.breakable == tru)
				{
					curLevel->ChangeTo(posHurtOne);
				}
				elf(checkHurtTwo.breakable == tru)
				{
					curLevel->ChangeTo(posHurtTwo);
				}
				elf(checkHurtTee.breakable == tru)
				{
					curLevel->ChangeTo(posHurtTee);
				}

				horizontalCollide = tru;			
			}

			if (checkOne.health == tru || checkTwo.health == tru || checkTee.health == tru)
			{
				if(checkOne.health == tru)
				{
					ShipHealth += checkOne.health;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.health == tru)
				{
					ShipHealth += checkTwo.health;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.health == tru)
				{
					ShipHealth += checkTee.health;
					curLevel->ChangeTo(posTee);
				}
				ShipHealth = Min(ShipHealth, 3);
			}

			
			if (checkOne.invincible == tru || checkTwo.invincible == tru || checkTee.invincible == tru)
			{
				if(checkOne.invincible == tru)
				{					
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTee);
				}
			}

			if (checkOne.doubleShoot == tru || checkTwo.doubleShoot == tru || checkTee.doubleShoot == tru )
			{
				if(checkOne.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTee);
				}
			}

			if (checkOne.autoShoot == tru || checkTwo.autoShoot == tru || checkTee.autoShoot == tru )
			{
				if(checkOne.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTee);
				}
			}
			
			if (checkOne.collision == false && 
				checkTwo.collision == false &&
				checkTee.collision == false)
			{
				TotalTranslation.y -= Lerp(speedMod.y, speedMod.y * speedShrinkMult.y, ShipShrink) * FRAME_TIME * speedMult.y * MoveSpeed.y;
				
				if(TotalTranslation.y < -12.5f + curLevel->wrapPoint.x * 2.0f)
				{
					TotalTranslation.y = (curLevel->Y * 2.0f) - 12.5f;
				}
			}
			else
			{	
				averageInput.y = Lerp(0.0f, averageInput.y, 0.75f);
				verticalCollide = tru;
				//horizontalCollide = tru;	
			}

			if (checkHurtOne.shootable == false &&
				checkHurtTwo.shootable == false &&
				checkHurtTee.shootable == false)
			{
			
			}
			else
			{
				//averageInput.y = Lerp(0.0f, averageInput.y, 0.5f);
				//verticalCollide = tru;
				horizontalCollide = tru;	
			}
		}

		if (MoveSpeed.y < -0.01f)
		{
			satVec2 posOne = satVec2(Lerp(tempPos.x + 0.666f, tempPos.x + 0.575f, ShipShrink), Lerp(tempPos.y + 1.3f, tempPos.y + 0.35f, ShipShrink));
			satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.566f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 1.0f, tempPos.y + 0.30f, ShipShrink));
			satVec2 posTee = satVec2(Lerp(tempPos.x + 0.566f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 0.9f, tempPos.y + 0.15f, ShipShrink));

			LevelMatrixBlock checkOne = CollisionCheck(posOne);
			LevelMatrixBlock checkTwo = CollisionCheck(posTwo);
			LevelMatrixBlock checkTee = CollisionCheck(posTee);
			
			satVec2 posHurtOne = satVec2(Lerp(tempPos.x + 0.516f, tempPos.x + 0.575f, ShipShrink), Lerp(tempPos.y + 1.3f, tempPos.y + 0.35f, ShipShrink));
			satVec2 posHurtTwo = satVec2(Lerp(tempPos.x + 0.466f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 1.0f, tempPos.y + 0.30f, ShipShrink));
			satVec2 posHurtTee = satVec2(Lerp(tempPos.x + 0.466f, tempPos.x + 0.450f, ShipShrink), Lerp(tempPos.y + 0.9f, tempPos.y + 0.15f, ShipShrink));
			
			LevelMatrixBlock checkHurtOne = CollisionCheck(posHurtOne);
			LevelMatrixBlock checkHurtTwo = CollisionCheck(posHurtTwo);
			LevelMatrixBlock checkHurtTee = CollisionCheck(posHurtTee);

			if (checkHurtOne.breakable == tru || checkHurtTwo.breakable == tru || checkHurtTee.breakable == tru)
			{
				if(checkHurtOne.breakable == tru)
				{
					curLevel->ChangeTo(posHurtOne);
				}
				elf(checkHurtTwo.breakable == tru)
				{
					curLevel->ChangeTo(posHurtTwo);
				}
				elf(checkHurtTee.breakable == tru)
				{
					curLevel->ChangeTo(posHurtTee);
				}

				horizontalCollide = tru;			
			}

			if (checkOne.health == tru || checkTwo.health == tru || checkTee.health == tru)
			{
				if(checkOne.health == tru)
				{
					ShipHealth += checkOne.health;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.health == tru)
				{
					ShipHealth += checkTwo.health;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.health == tru)
				{
					ShipHealth += checkTee.health;
					curLevel->ChangeTo(posTee);
				}
				ShipHealth = Min(ShipHealth, 3);
			}

			
			
			if (checkOne.invincible == tru || checkTwo.invincible == tru || checkTee.invincible == tru)
			{
				if(checkOne.invincible == tru)
				{					
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTee);
				}
			}

			if (checkOne.doubleShoot == tru || checkTwo.doubleShoot == tru || checkTee.doubleShoot == tru )
			{
				if(checkOne.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTee);
				}
			}

			if (checkOne.autoShoot == tru || checkTwo.autoShoot == tru || checkTee.autoShoot == tru )
			{
				if(checkOne.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
				elf(checkTee.autoShoot == tru)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTee);
				}
			}
			
			if (checkOne.collision == false && 
				checkTwo.collision == false &&
				checkTee.collision == false)
			{
				TotalTranslation.y -= Lerp(speedMod.y, speedMod.y * speedShrinkMult.y, ShipShrink) * FRAME_TIME * speedMult.y * MoveSpeed.y;

				if(TotalTranslation.y > (curLevel->Y - curLevel->wrapPoint.y * 2.0f) * 2.0f - 12.5f)
				{
					TotalTranslation.y = - 12.5f;
				}
			}
			else
			{	
				averageInput.y = Lerp(0.0f, averageInput.y, 0.75f);
				verticalCollide = tru;
				//horizontalCollide = tru;	
			}

			if (checkHurtOne.shootable == false &&
				checkHurtTwo.shootable == false &&
				checkHurtTee.shootable == false)
			{
				//TotalTranslation.y -= Lerp(speedMod.y, speedMod.y * speedShrinkMult.y, ShipShrink) * FRAME_TIME * speedMult.y * MoveSpeed.y;
			}
			else
			{
				//averageInput.y = Lerp(0.0f, averageInput.y, 0.5f);
				//verticalCollide = tru;
				horizontalCollide = tru;	
			}
		}
	}

	if (ShipHealth > 0 && (MoveSpeed.x > 0.01f || MoveSpeed.x < 0.01f))
	{
		if (MoveSpeed.x < -0.01f)
		{
			


			satVec2 posOne = satVec2(Lerp(tempPos.x + 0.375f, tempPos.x + 0.48f, ShipShrink), Lerp(tempPos.y + 1.1f, tempPos.y + 0.325f, ShipShrink));
			satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.400f, tempPos.x + 0.52f, ShipShrink), Lerp(tempPos.y + 1.1f, tempPos.y + 0.325f, ShipShrink));
		
			LevelMatrixBlock checkOne = CollisionCheck(posOne);
			LevelMatrixBlock checkTwo = CollisionCheck(posTwo);
						
			if (checkOne.breakable == tru || checkTwo.breakable == tru )
			{
				if(checkOne.breakable == tru)
				{
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.breakable == tru)
				{
					curLevel->ChangeTo(posTwo);
				}

				horizontalCollide = tru;			
			}

			if (checkOne.health == tru || checkTwo.health == tru )
			{
				if(checkOne.health == tru)
				{
					ShipHealth += checkOne.health;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.health == tru)
				{
					ShipHealth += checkTwo.health;
					curLevel->ChangeTo(posTwo);
				}
				ShipHealth = Min(ShipHealth, 3);
			}

			
			if (checkOne.invincible == tru || checkTwo.invincible == tru )
			{
				if(checkOne.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTwo);
				}
			}

			if (checkOne.doubleShoot == tru || checkTwo.doubleShoot == tru )
			{
				if(checkOne.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
			}

			if (checkOne.autoShoot || checkTwo.autoShoot)
			{
				if(checkOne.autoShoot)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.autoShoot)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
			}
		
			
			if (checkOne.collision == false && 
				checkTwo.collision == false)
			{				
				if (adjust.x > 0)
				{
					if (ScoreMult > 1.0f)
						ScoreMult -= 1.6 * Timer::framef() * FPU * powf(abs(MoveSpeed.x), 0.5f) * speedMod.z * CollisionSpeed * IntroEndParam;
					else if (ScoreMult > 0.05f)
						ScoreMult -= 0.2 * Timer::framef() * FPU * powf(abs(MoveSpeed.x), 0.5f) * speedMod.z * CollisionSpeed * IntroEndParam;
					else
						ScoreMult = 0.05f;

					adjust.x += Lerp(speedMod.z, speedMod.z * speedShrinkMult.x, ShipShrink) * FRAME_TIME * MoveSpeed.x * CollisionSpeed;
				}
				else
				{	adjust.x = 0.0f;	}
			}
			else
			{	}//horizontalCollide = tru;	}
		}

		if (MoveSpeed.x > 0.01f)
		{

			satVec2 posOne = satVec2(Lerp(tempPos.x + 0.375f, tempPos.x + 0.44f, ShipShrink), Lerp(tempPos.y + 1.4f, tempPos.y + 0.45f, ShipShrink));
			satVec2 posTwo = satVec2(Lerp(tempPos.x + 0.400f, tempPos.x + 0.56f, ShipShrink), Lerp(tempPos.y + 1.4f, tempPos.y + 0.45f, ShipShrink));
			
			LevelMatrixBlock checkOne = CollisionCheck(posOne);
			LevelMatrixBlock checkTwo = CollisionCheck(posTwo);

			
			if (checkOne.breakable == tru || checkTwo.breakable == tru )
			{
				if(checkOne.breakable == tru)
				{
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.breakable == tru)
				{
					curLevel->ChangeTo(posTwo);
				}

				horizontalCollide = tru;			
			}

			if (checkOne.health == tru || checkTwo.health == tru )
			{
				if(checkOne.health == tru)
				{
					ShipHealth += checkOne.health;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.health == tru)
				{
					ShipHealth += checkTwo.health;
					curLevel->ChangeTo(posTwo);
				}
				ShipHealth = Min(ShipHealth, 3);
			}

			if (checkOne.invincible == tru || checkTwo.invincible == tru )
			{
				if(checkOne.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.invincible == tru)
				{
					NoDamageTime = NoDamageTimeMax;
					ChargeUpSound.playSound(1.0f, 1.0f, 1.0f);
					curLevel->ChangeTo(posTwo);
				}
			}

			if (checkOne.doubleShoot == tru || checkTwo.doubleShoot == tru )
			{
				if(checkOne.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.doubleShoot == tru)
				{
					DoubleShootTime = DoubleShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
			}
			
			if (checkOne.autoShoot || checkTwo.autoShoot)
			{
				if(checkOne.autoShoot)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posOne);
				}
				elf(checkTwo.autoShoot)
				{
					AutoShootTime = AutoShootTimeMax;
					curLevel->ChangeTo(posTwo);
				}
			}
		

			if (checkOne.collision == false && 
				checkTwo.collision == false)
			{				
				if (adjust.x < curLevel->WinPoint)
				{
					ScoreMult += 1.0 * Timer::framef() * FPU * powf(abs(MoveSpeed.x), 3.0f) * speedMod.w * CollisionSpeed * SatDebugAutoScrollActive * IntroEndParam;
					
					adjust.x += Lerp(speedMod.w, speedMod.w * speedShrinkMult.x, ShipShrink) * FRAME_TIME * MoveSpeed.x * CollisionSpeed;
				}
				else
				{	
					winLevel();	
					tempPos.y = 1;
				}
			}
			else
			{	horizontalCollide = tru;	}
		}
	}

	if(horizontalCollide)
	{
		damageShip();
	}
}


char Game::CollisionAutoScrollUpdate(satVec2 &pos)
{
	return curLevel->lMatrix[pos.x][pos.y];
}

char Game::CollisionVerticalUpdate(satVec2 &pos)
{
	return curLevel->lMatrix[pos.x][pos.y];
}

char Game::CollisionHorizontalUpdate(satVec2 &pos)
{
	return curLevel->lMatrix[pos.x][pos.y];
}

LevelMatrixBlock Game::CollisionCheck(satVec2 &pos)
{
	pos.x = Between(pos.x, 0.0f, (float)curLevel->Y - 1.0f);
	pos.y = Between(pos.y, 0.0f, (float)curLevel->X - 1.0f);

	LevelMatrixBlock temp;
	temp.block   	= curLevel->lMatrix[pos.x][pos.y];
	temp.breakable	= curLevel->lBreakableMatrix[pos.x][pos.y];	
	temp.collision	= curLevel->lCollisionMatrix[pos.x][pos.y];
	temp.shootable	= curLevel->lShootableMatrix[pos.x][pos.y];
	temp.kill		= curLevel->lKillMatrix[pos.x][pos.y];
	temp.changeTo 	= curLevel->lChangeToMatrix[pos.x][pos.y];
	temp.health 	= curLevel->lHealthMatrix[pos.x][pos.y];
	temp.invincible 	= curLevel->lInvincibleMatrix[pos.x][pos.y];
	temp.doubleShoot 	= curLevel->lDoubleShotMatrix[pos.x][pos.y];
	temp.autoShoot 	= curLevel->lAutoShotMatrix[pos.x][pos.y];

	//if(temp.invincible)
	//{
	//	std::cout << " wtf " << curLevel->lInvincibleMatrix[pos.x][pos.y];
	//}

	return temp;
}


void Game::ControllerUpdate()
{
	ControllerInputJoypadRightStick = 0.01f * satVec2(sat::joy::CheckAxis(sf::Joystick::U, controllerDeadzone), -sat::joy::CheckAxis(sf::Joystick::R, controllerDeadzone));
	ControllerInputJoypadLeftStick = 0.01f * satVec2(sat::joy::CheckAxis(sf::Joystick::X, controllerXDeadzone), -sat::joy::CheckAxis(sf::Joystick::Y, controllerDeadzone));
	ControllerInputTrigger = 0.01f * -sat::joy::CheckAxis(sf::Joystick::Z, controllerDeadzone * 0.25f);
	ControllerInputDigitalPad = 0.01f * satVec2(sat::joy::CheckAxis(sf::Joystick::PovY, controllerDeadzone), sat::joy::CheckAxis(sf::Joystick::PovX, controllerDeadzone));
	ControllerInputButtonA = sf::Joystick::isButtonPressed(0, Xbox360Buttons::A);
	ControllerInputButtonB = sf::Joystick::isButtonPressed(0, Xbox360Buttons::B);
	ControllerInputButtonX = sf::Joystick::isButtonPressed(0, Xbox360Buttons::X);
	ControllerInputButtonY = sf::Joystick::isButtonPressed(0, Xbox360Buttons::Y);
	ControllerInputButtonSelect = sf::Joystick::isButtonPressed(0, Xbox360Buttons::Select);
	ControllerInputButtonStart = sf::Joystick::isButtonPressed(0, Xbox360Buttons::Start);
	ControllerInputButtonRightBumper = sf::Joystick::isButtonPressed(0, Xbox360Buttons::RightBumper);
	ControllerInputButtonLeftBumper = sf::Joystick::isButtonPressed(0, Xbox360Buttons::LeftBumper);
	ControllerInputButtonRightStickClick = sf::Joystick::isButtonPressed(0, Xbox360Buttons::RightStickClick);
	ControllerInputButtonLeftStickClick = sf::Joystick::isButtonPressed(0, Xbox360Buttons::LeftStickClick);
}

void Game::MouseUpdate()
{
	if (sat::mouse::CheckClicked(sf::Mouse::Left, 20))
	{

	}
}

void Game::InputUpdateTail()
{
	
	curLevel->setShipPosition(adjust.x + 6.0f);

	if(TitleScreenActive == false)
	{
		curLevel->calcSoundPlay();
	}

	//CameraTransform.LoadIdentity();
	//CameraTransform.RotationY(TotalGameTime * 15.0f);
	//CameraTransform.Translate(0.0f, 5.0f, 20.0f);
	//CameraTransform.RotationX(-20.0f); 
	//
	//ShadowTransform.LoadIdentity();
	//ShadowTransform.Inverse();
	////ShadowTransform.RotationY(TotalGameTime * -0.25f * 400.0f * 0.05f + 5);
	////ShadowTransform.RotationX(TotalGameTime * -0.25f * 50.0f);
	//ShadowTransform.Translate(0.0f, 8.0f, 25.0f);
	//ShadowTransform.RotationX(-30.0f);
	////CameraTransform.RotationY(TotalGameTime * 15.0f);
	//
	////ModelTransform.RotationY(deltaTime * 15.0f);
	////LightTransform.RotationY(deltaTime * 1.0f);

	//Quaternions.LoadIdentity();
	
	//sat::mouse::CheckClicked

	ShipShrink = Between(ShipShrink, 0.0f, 1.0f);
}

void Game::updateMenu()
{
	ReshapeWindow();
	sf::Joystick::update();
	KeyboardDebugUpdate();
	//calcCamera();
	if (PauseActive && !TitleScreenActive)
	{
		if((sat::key::CheckPressed(sf::Keyboard::Escape) || 
			sat::joy::CheckPressed(Xbox360Buttons::B)))
		{
			curLevel->UnloadLevelSounds();
			LevelManager::levelCurrent = 0;
			
			winGame();

			PauseActive = false;
		}

		if((sat::key::CheckPressed(sf::Keyboard::Space) || 
			sat::joy::CheckPressed(Xbox360Buttons::Start) || 
			sat::joy::CheckPressed(Xbox360Buttons::A)))
		{
			//songPitch = 1.0f;
			//songVolume = 1.0f;
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(1.0f);
			//LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume(1.0f);
			float lowpass = 500000.0f;
			LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPLowpass(lowpass);
			LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPFlange(0.0f, 0.0f);
			PauseActive = false;
		}
	}

	calcCamera();
}

void Game::updateOld()
{
	if (adjust.x < 30.0f)
	{
		IntroEndParam = InverseLerp(adjust.x, 0.0f, 26.0f);// * InverseLerp(adjust.x, curLevel->WinPoint, curLevel->WinPoint - 30.0f);
	}
	else
	{
		IntroEndParam = InverseLerp(adjust.x, curLevel->WinPoint, curLevel->WinPoint - 30.0f);
	}

	if(timeDead <= 0.0f)
	{
		//TimeInLevel += Timer::framef() * IntroEndParam;
		float songPitchAdjust = abs(songPitch);
		if(songPitchAdjust > 1.0f)
		{
			songPitchAdjust = powf(songPitchAdjust, 0.5f);
		}
		if(!SatDebugTimerPauseActive)
		{
			double TimeInLevelOld = TimeInLevel; 
			TimeInLevel += Timer::framef() * Min(songPitchAdjust, 3.0f);			
			FrameInLevel = TimeInLevel - TimeInLevelOld;
		}
	}
	else
	{
		float t = InverseLerp(timeDead, timeDeadReverse, 0.0f);
		//TimeInLevel = Lerp(0.0f, timeDead, t * t);

		TimeInLevel += Timer::framef() * t * IntroEndParam;
	}
	// update our clock so we have the delta time since the last update
	//updateTimer->tick();

	//float deltaTime = updateTimer->getElapsedTimeSeconds();
	//TotalGameTime += deltaTime;
		
	
	/////////////////
	//HighScore -= Lerp(0.0, 100.0 * Timer::framef(), InverseLerp(HighScore, 0.0, defaultScore * 1.0));
	/////////////////
	
	
	
	//std::cout << Lerp(0.0, 100.0 * Timer::framef(), InverseLerp(HighScore, 0.0, defaultScore * 1.0)) << std::endl;
	if(adjust.x <= 0.1f)
	{
		songVolume = 0.0f;
	}
	
	LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch(songPitch);
	LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume(songVolume);

	songPitch = curLevel->calcMusicPitch();
	songVolume = curLevel->calcMusicVolume();

	if(LevelManager::level[LevelManager::currentSongPlaying].Song.isPlaying())
	{
		//float lowpass = Timer::sinRange(1.0f, 50.0f, 2500.0f);
		//LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPLowpass(lowpass);
		//
		//float highpass = Timer::sinRange(1.0f, 2000.0f, 7500.0f);
		//LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPHighpass(highpass);
		//		
		float flange = 0.0f;
		float flangeRate = 0.0f;
		LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPFlange(flangeRate, flange);
	}
	
	//for(int i = 0; i < 1; i++)
	//{
	//	float lowpass = Timer::sinRange(1.0f, 50.0f, 2500.0f);
	//	LevelManager::level[i].Song.setDSPLowpass(lowpass);
	//	
	//	float highpass = Timer::sinRange(1.0f, 2000.0f, 7500.0f);
	//	LevelManager::level[i].Song.setDSPHighpass(highpass);
	//			
	//	float flange = 0.5f;
	//	LevelManager::level[i].Song.setDSPFlange(flange);
	//	
	//	float echo = 0.5f;
	//	LevelManager::level[i].Song.setDSPFlange(echo);
	//}



	Sys.update(satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 0.0f), satVec3(0.0f, 0.0f, 1.0f), satVec3(0.0f, 1.0f, 0.0f));

	ReshapeWindow();
	
	//CameraProjection.PerspectiveStereoProjection(FOV, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 20.0f, 50.0f, Lerp(-0.525f, 0.525f, sinf(Timer::currentf() * 5.0f)*0.5f+0.5f), Lerp(-0.4f, -0.4f, sinf(Timer::currentf() * 1.0f)*0.5f+0.5f)); //Set FOV, Aspect Ratio, near and far clipping planes.
	
	//eyeConvergence = -Lerp(0.1f, 0.1f, Timer::sinP(3.5f));
	//calcCamera();

	InputUpdateHead();
	KeyboardUpdate(0);
	InputUpdateTail();

	//for (int i = 0; i < bulletGood.size(); ++i)
	//{
	//	if(MoveSpeed.y != 0.0f)
	//	{
	//		bulletGood[i].position.y += Lerp(speedMod.y, speedMod.y * speedShrinkMult.y, ShipShrink) * FRAME_TIME * speedMult.y * MoveSpeed.y;
	//	}
	//}

	//Double the speed of most things by updating twice as much
	for (int i = 0; i < UPDATES_PER_FRAME * 2; ++i)
	{
		UPDATE_COUNTER = i;
		if (ShipHealth >= 0 && TitleScreenActive != tru)
		{
			CollisionUpdate();
		}

		std::vector<BulletGood> bulletGoodBlur = bulletGood;
		
		for (unsigned i = 0; i < bulletGood.size(); ++i)
		{
			if (bulletGood[i].lifetime > 0)
			{
				
				if(MoveSpeed.y != 0.0f && verticalCollide == false)
				{
					bulletGood[i].displacement.y += Lerp(speedMod.y, speedMod.y * speedShrinkMult.y, ShipShrink) * FRAME_TIME * speedMult.y * MoveSpeed.y;
				}
				if(SatDebugAutoScrollActive && horizontalCollide == false)
				{
					bulletGood[i].displacement.x += Lerp(speedMod.x, speedMod.x * speedShrinkMult.x, ShipShrink) * FRAME_TIME * speedMult.x;
				}
				if(MoveSpeed.x > 0.0f && horizontalCollide == false)
				{
					bulletGood[i].displacement.x += Lerp(speedMod.w, speedMod.w * speedShrinkMult.x, ShipShrink) * FRAME_TIME * speedMult.x * MoveSpeed.x;
				}
				if(MoveSpeed.x < 0.0f && horizontalCollide == false)
				{
					bulletGood[i].displacement.x += Lerp(speedMod.z, speedMod.z * speedShrinkMult.x, ShipShrink) * FRAME_TIME * speedMult.x * MoveSpeed.x;
				}
				//if(MoveSpeed.x != 0.0f)
				//{
				//	bulletGood[i].position.x += Lerp(speedMod.x, speedMod.x * speedShrinkMult.x, ShipShrink) * FRAME_TIME * speedMult.x * MoveSpeed.x;
				//}

				lightBullet[i].color = sat::color::lightcyan * powf(bulletGood[i].lifetime / bulletGood[i].totalLifetime, 0.9f);
				bulletGood[i].process();
				lightBullet[i].pos = -bulletGood[i].position - satVec3(TotalTranslation.x + initialTranslation.x, TotalTranslation.y + initialTranslation.y, initialTranslation.z - 2.5f);
				
			}
			if (bulletGood[i].lifetime <= 0)
			{
				//PewSound.playSound(1.0f);
				bulletGood.erase(bulletGood.begin() + i);
				lightBullet.erase(lightBullet.begin() + i);
			}
		}

		for (unsigned i = 0; i < smokeParticle.size(); ++i)
		{
			if (smokeParticle[i].lifetime > 0)
			{
				smokeParticle[i].process();
			}
			if (smokeParticle[i].lifetime <= 0)
			{
				smokeParticle.erase(smokeParticle.begin() + i);
			}
		}
	}
	
	for (GLuint i = 0; i < lightPoint.size() / 8; i++)
	{
		//lightPositions[i].y = (i % 8 * 4) + sinf(TotalGameTime * 4)*4.0f;
		for (GLuint j = 0; j < lightPoint.size() / 8; j++)
		{
			
			//lightPositions[i].y = (i % 8 * 4);// +sinf(TotalGameTime * 4)*4.0f;
			lightPoint[(i * 8) + j].pos.x = sinf(TimeInLevel * 3 + (((i+1)*(j+1)) * 25))+ initialTranslation.x + (j * 8) - (adjust.x*2.0f) - 30;
			lightPoint[(i * 8) + j].pos.y = sinf(TimeInLevel * 5 + (((i+1)*(j+1)) * 15))+ initialTranslation.y + (i * 4) - 20 - (sat::key::CheckKey(sf::Keyboard::M) * 8.0f);
			lightPoint[(i * 8) + j].pos.z = sinf(TimeInLevel * 4 + (((i+1)*(j+1)) * 20)) * 0.9f - 31;
		}
	}

	lightShipHealth[0].pos = -satVec3(TotalTranslation.x + 11.00f - Lerp(4.0f, 1.0f, ShipShrink), TotalTranslation.y - 0.5f + initialTranslation.y, initialTranslation.z + 5.1f) + satVec3(Lerp(0, 0, ShipShrink), 0.0f, 0.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
	lightShipHealth[1].pos = -satVec3(TotalTranslation.x + 10.25f - Lerp(4.0f, 1.0f, ShipShrink) + (sinf(TimeInLevel * 5.0f + DTR(0)) *		Lerp(1.25f, 0.3125f, ShipShrink)), TotalTranslation.y - 0.8f + initialTranslation.y, initialTranslation.z - 0.4f) + satVec3(Lerp(0, 0, ShipShrink), 0.0f, 0.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
	lightShipHealth[2].pos = -satVec3(TotalTranslation.x + 10.25f - Lerp(4.0f, 1.0f, ShipShrink) + (sinf(TimeInLevel * 5.0f + DTR(90)) *		Lerp(1.25f, 0.3125f, ShipShrink)), TotalTranslation.y - 0.9f + initialTranslation.y, initialTranslation.z - 0.4f) + satVec3(Lerp(0, 0, ShipShrink), 0.0f, 0.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
	lightShipHealth[3].pos = -satVec3(TotalTranslation.x + 10.25f - Lerp(4.0f, 1.0f, ShipShrink) + (sinf(TimeInLevel * 5.0f + DTR(180)) *	Lerp(1.25f, 0.3125f, ShipShrink)), TotalTranslation.y - 1.0f + initialTranslation.y, initialTranslation.z - 0.4f) + satVec3(Lerp(0, 0, ShipShrink), 0.0f, 0.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
	lightShipHealth[4].pos = -satVec3(TotalTranslation.x + 10.25f - Lerp(4.0f, 1.0f, ShipShrink) + (sinf(TimeInLevel * 5.0f + DTR(270)) *	Lerp(1.25f, 0.3125f, ShipShrink)), TotalTranslation.y - 1.1f + initialTranslation.y, initialTranslation.z - 0.4f) + satVec3(Lerp(0, 0, ShipShrink), 0.0f, 0.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
	
	for(unsigned i = 0; i < lightShipInvincible.size(); ++i)
	{
		lightShipInvincible[i].color = Lerp(ShipInvincibleColor[i], ShipInvincibleColor[i+1], Timer::sinP(0.0f, TimeInLevel * 8.0f));
		if (ScoreMult >= defaultScoreMult + defaultScoreMult * 4)
		{
			lightShipInvincible[i].color *= Lerp(satColor(0.0f), lightShipInvincible[i].color, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 4, defaultScoreMult + lightShipScoreMult * (6 + i)));
			lightShipInvincible[i].color *= curLevel->calcShipLightMultiplier();
		}
		else
		{
			//lightShipInvincible[i].color = ShipHealthColor[ShipHealth] * curLevel->calcShipLightMultiplier();
		}
	}
	
	lightShipInvincible[0].pos = 
		- satVec3(
		TotalTranslation.x + 9.0f - Lerp(4.0f, 1.0f, ShipShrink), 
		TotalTranslation.y - 0.5f + initialTranslation.y, 
		initialTranslation.z - 4.1f) 
		+ satVec3(
		5.0f * sinf(TimeInLevel * 5.0f + DTR(0)),
		5.0f * sinf(TimeInLevel * 5.0f + DTR(90)),
		5.0f * sinf(TimeInLevel * 5.0f + DTR(180))
		);

	lightShipInvincible[1].pos = 
		- satVec3(
		TotalTranslation.x + 9.0f - Lerp(4.0f, 1.0f, ShipShrink), 
		TotalTranslation.y - 0.5f + initialTranslation.y, 
		initialTranslation.z - 4.1f) 
		+ satVec3(
		5.5f * sinf(TimeInLevel * 5.0f + DTR(90)),
		5.5f * sinf(TimeInLevel * 5.0f + DTR(180)),
		5.5f * sinf(TimeInLevel * 5.0f + DTR(0))
		);

	lightShipInvincible[2].pos = 
		- satVec3(
		TotalTranslation.x + 9.0f - Lerp(4.0f, 1.0f, ShipShrink), 
		TotalTranslation.y - 0.5f + initialTranslation.y, 
		initialTranslation.z - 5.1f) 
		+ satVec3(
		6.0f *  sinf(TimeInLevel * 5.0f + DTR(180)	),
		6.0f *  sinf(TimeInLevel * 5.0f + DTR(0)		),
		6.0f * -sinf(TimeInLevel * 5.0f + DTR(90)		)
		);

	lightShipInvincible[3].pos = 
		- satVec3(
		TotalTranslation.x + 9.0f - Lerp(4.0f, 1.0f, ShipShrink), 
		TotalTranslation.y - 0.5f + initialTranslation.y, 
		initialTranslation.z - 5.1f) 
		+ satVec3(
		6.5f * -sinf(TimeInLevel * 5.0f + DTR(270)),
		6.5f * -sinf(TimeInLevel * 5.0f + DTR(90)),
		6.5f * -sinf(TimeInLevel * 5.0f + DTR(0))
		);


	lightShipHealth[0].color = satColor(0.25f, 0.5f, 0.5f) * curLevel->calcShipLightMultiplier();
	lightShipHealth[0].linear = 2.0f * curLevel->calcShipLightLinear(); 
	lightShipHealth[0].quad = 2.0f * curLevel->calcShipLightQuad();
	lightShipHealth[0].innerRadius = curLevel->calcShipLightInnerRadius();
	lightShipHealth[0].calcRadius();

	for (unsigned i = 0; i < lightShipHealth.size() - 5; ++i)
	{
		lightShipHealth[i + 5].linear = curLevel->calcShipLightLinear(); 
		lightShipHealth[i + 5].quad = curLevel->calcShipLightQuad();
		lightShipHealth[i + 5].innerRadius = curLevel->calcShipLightInnerRadius();
		lightShipHealth[i + 5].calcRadius();

		lightShipHealth[i + 5].pos = -satVec3(TotalTranslation.x + 9.0f - Lerp((2.0f + i*0.8f), (0.5f + i*0.2f), ShipShrink), TotalTranslation.y - 0.5f + initialTranslation.y, initialTranslation.z);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)
		
				//std::cout << curLevel->calcShipLightMultiplier();
		if (collisionTime >= invincibleTime * 0.5f && ShipHealth < 3 && ShipHealth >= 0)
		{
			lightShipHealth[i + 5].color = Lerp(ShipHealthColor[ShipHealth], ShipHealthColor[ShipHealth + 1], InverseLerp(collisionTime, invincibleTime * 0.5f, invincibleTime)) * curLevel->calcShipLightMultiplier();
		}
		else
		{
			lightShipHealth[i + 5].color = ShipHealthColor[ShipHealth] * curLevel->calcShipLightMultiplier();
		}

		if(SatDebugGodModeActive || NoDamageTime > 0.0f)
		{
			lightShipHealth[i + 5].color = ShipHealthColor[4] * curLevel->calcShipLightMultiplier();
		}
		//lightShipHealth[i + 5].color *= 0.5f;

		lightShipHealth[i + 5].pos += satVec3((sinf(TimeInLevel*10.0f)*0.1f), (sinf(TimeInLevel*10.0f)*0.01f), 5.5f + i*1.0f);// -satVec3(Lerp(-3.5f, -1.0f, ShipShrink), -0.4f, 1.0f)

		//if (rand() % 20 == 1)
		//{
		//	lightShipHealth[i + 1].color = ShipHealthColor[ShipHealth]*0.25;
		//}
	}

	if (sat::key::CheckPressed(sf::Keyboard::Z))
	{
		//std::cout << lightPositions[0].z;
	}

	

	if (DataPoints.size() < 4)
	{	return;	}

	CurrentDistance += 1000.0f * updateTimer->getElapsedTimeSeconds();
	//CurrentDistance

	timeL *= 1.3f;
	if (timeL >= 1.0f)
	{	timeL = 0.03f;	}
	else if (timeL < 0.0f)
	{	timeL = 0.03f;	}

	//Quaternions.LoadIdentity();
	Quaternions.TranslateInstance(initialTranslation + TotalTranslation);
	//Quaternions.Translate(TotalTranslation.x, TotalTranslation.y, TotalTranslation.z);
	
	
	satVec2 shadowRotation = DistanceCorrect(CurrentDistance);


	satMat4 ShadowInitialTransform;
	ShadowInitialTransform.LoadIdentity();
	//ShadowInitialTransform.Translate(17.0f, TotalTranslation.y, 0.0f);
	//ShadowInitialTransform.TranslateInstance(TotalTranslation.x + 40.0f, TotalTranslation.y, TotalTranslation.z + 30.0f);	
	ShadowInitialTransform.TranslateInstance(TotalTranslation + curLevel->calcShadowTranslation());	
	//ShadowInitialTransform.TranslateInstance(30.0f+shadowRotation.x, TotalTranslation.y-(shadowRotation.y*0.5f), 30.0f);
	

	ShadowTransform.LoadIdentity();
	//ShadowTransform.Translate(17.0f, TotalTranslation.y, 20.0f);

	satVec3 shadowRotationTemp = curLevel->calcShadowRotation();

	ShadowTransform.RotateX(shadowRotationTemp.x); 
	ShadowTransform.RotateY(shadowRotationTemp.y);
	ShadowTransform.RotateZ(shadowRotationTemp.z);
	//if(sat::key::CheckKey(sf::Keyboard::Y))
	//{
	//	ShadowInitialTransform.TranslateInstance(50.0f, TotalTranslation.y, -60.0f);	
	//	ShadowTransform.RotateY(140.0f);
	//}

	shadowRotationTemp = curLevel->calcShadowTranslationRotation();
	ShadowTransform.Translate(shadowRotationTemp);

	if(true)
	{
		ShadowInitialTransform.TranslateInstance(3.0f, 0.0, 0.0f);	
		ShadowTransform.LoadIdentity();	
		ShadowTransform.RotateY(-90.f );
	}

	//ShadowTransform.TranslateInstance(0.0f, 0.0f, -20.0f);
	//ShadowTransform.RotateX(shadowRotation.y);
	//ShadowTransform.RotateY(shadowRotation.x);
	ShadowTransform = ShadowInitialTransform * ShadowTransform;
	satVec2 ShadowNearFar = {1.0f, 200.0f};
	if(SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SHADOW_MAP)
	{
		ShadowNearFar.x = 10.0f;
		ShadowNearFar.y = 1000.0f;
	}
	else
	{	

	}

	if(curLevel->ShadowOrthographicActive)
	{
		float shadowOrthoSize = 50.0f;
		ShadowProjection.OrthoProjection(shadowOrthoSize, -shadowOrthoSize, -shadowOrthoSize, shadowOrthoSize, -100.0f, 100.0f);
	}
	else
	{
		ShadowProjection.PerspectiveProjection(curLevel->calcShadowFOV(), 1.0f, ShadowNearFar.x, ShadowNearFar.y);
	}
	ViewToShadowMap.LoadIdentity();
	ViewToShadowMap = biasMat4 * ShadowProjection * ShadowTransform.Inverse() * CameraTransform;
	//ViewToShadowMap = biasMat4 * ShadowProjection * ShadowTransform * CameraTransform;


	//eyeShift.x = 0.0f;
	//if(LevelManager::levelCurrent == 0 && !TitleScreenActive)
	//{
	//	if(adjust.x < 15.0)
	//	{
	//		eyeShift.x = Lerp(-2.0, 0.0, (adjust.x-5.0f) / 10.0f);
	//		
	//	}
	//}
	//else
	//
	//if(TitleScreenActive)
	//{
	//	eyeShift.x = 0.0f;
	//}

	calcCamera();




}

void Game::damageShip(const bool &force)
{	
	if ((collisionTime <= 0 && NoDamageTime < 0.0f) || force)
	{
		//ScoreMult = Between(ScoreMult - 2.0f, 1.0f, 10.0f);

		ScoreMult = Max(ScoreMult * 0.75, 0.25);

		BzztSound.playSound(1.0f, 1.0f, 1.0f);
		--ShipHealth;
		if (ShipHealth <= 0)
		{
			timeDead = 0.0f;
			BlarpSound.playSound(0.75f, 1.0f, 1.0f);
		}
		else
		{
			MoveSpeed.x = -1.0f;
		}
		collisionTime = invincibleTime;
	}
}

void Game::winLevel()
{
	if(LevelManager::levelCurrent < LevelManager::levelFinalNum - 1)
	{		
		if(LevelManager::levelCurrent >= 0)
		{
			curLevel->LevelScore = (long long)(HighScore * ScoreMult);
			curLevel->LevelTime = TimeInLevel;

			currentTotalScore = 0;
			currentTotalTime  = 0;
			for (int i = 0; i <= LevelManager::levelCurrent; ++i)
			{
				currentTotalScore += LevelManager::level[i].LevelScore;
				currentTotalTime += LevelManager::level[i].LevelTime;
			}

			//std::cout << "Score:      " << curLevel->LevelScore << std::endl;
			//std::cout << "TotalScore: " << currentTotalScore << std::endl;
			ScoreBoard.enterScore(curLevel->LevelScore, LevelManager::levelCurrent + 1);
		}
		
		//std::cout << 
		//ScoreBoard.addToHiScore(HighScore * ScoreMult);
		
		levelWinFrame = 1;
		++LevelManager::levelCurrent;
		setupLevel();

		//curLevel = new Level(LevelWidth, LevelHeight);

		curLevel->UnloadLevelSounds();
		curLevel = &LevelManager::level[LevelManager::levelCurrent];

		if(LevelManager::levelCurrent == 0)
		{
			//curLevel->setBlockType(&BlockModel[LevelManager::levelCurrent], &BlockTexture[LevelManager::levelCurrent], &BlockNormal[LevelManager::levelCurrent], &DefaultShiny, &DefaultEmissive);
		}
		else
		{
			//curLevel->setBlockType(&BlockModel[LevelManager::levelCurrent], &BlockTexture[LevelManager::levelCurrent], &BlockNormal[LevelManager::levelCurrent], &DefaultShiny, &DefaultEmissive);
		}

		
		//curLevel->Load();
		curLevel->readLevel(curLevel->filename);
		curLevel->removeStuff();
		curLevel->LoadLevelSounds();
		setSong();
		ScoreBoard.hiScore = ScoreBoard.readScore(1, LevelManager::levelCurrent + 1);
		RENDER_HEIGHT = curLevel->Y;
		RenderHeightHalf = RENDER_HEIGHT * 0.5f;
		Timer::tick();
		Timer::tick();
	}
	else
	{
		curLevel->LevelScore = (long long)(HighScore * ScoreMult);
		std::cout << "Score: " << curLevel->LevelScore << std::endl;
		ScoreBoard.enterScore(curLevel->LevelScore, LevelManager::levelCurrent + 1);
		//levelWinFrame = 1;
		winGame();
		//LevelManager::levelCurrent = 0;
	}
}

void Game::winGame()
{
	totalHighScore = 0;
	for(unsigned i = 0 ; i < LevelManager::level.size(); ++i)
	{
		totalHighScore += LevelManager::level[i].LevelScore;
	}

	for (int i = 0; i < 10; ++i)
	{
		highestTotalHighScore[i] = ScoreBoard.readScore(i + 1, 0);
	}

	//highestTotalHighScore = ScoreBoard.readScore(1, 0);
	ScoreBoard.enterScore(totalHighScore, 0);
	
	if(HighScore * ScoreMult < highestTotalHighScore[0])
	{
		std::cout << "You win! Your score was " << HighScore * ScoreMult << std::endl;
	}
	else
	{
		std::cout << "You set a new highscore!" << std::endl
			<< "You win! Your score was " << HighScore * ScoreMult << std::endl;
	}


	setupLevel();
	
	WinScreenActive = tru;
	TitleScreenActive = true;
	SpecialThanksActive = true;

	currentTotalScore = 0;
	currentTotalTime  = 0.0f;

	setSong();

	for (unsigned i = 0; i < LevelManager::level.size(); ++i)
	{
		LevelManager::level[i].LevelScore = 0;
		LevelManager::level[i].LevelTime = 0;
	}
	
}

void Game::drawDebug()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(0.1f, 0.2f, 0.3f, 0);
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	SSAOWorkBuffer1.Clear();
	SSAOWorkBuffer2.Clear();
	EdgeMap.Clear();
	glClearColor(0.0f, 0.0f, 0.0f, 0);
	DeferredComposite.Clear();
	FinalComposite.Clear();
	FullScreenOutput.Clear();

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(0.0f, 0.0f, 0.0f, 0);
	GBuffer.Clear();
	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();
	
	

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render The s Map //
	glViewport(0, 0, SHADOW_RESOLUTION >> UpScaleShadowResolution, SHADOW_RESOLUTION >> UpScaleShadowResolution); //Set up the viewport (consider putting this in the framebuffer class!)

	computeShadowMap();
	GBufferPass.unbind(); //Unbind the shader so we don't accidentally overwrite data.
	// Render The Scene //
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
	
	if(ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}
	computeRender();
	toonShading();
	deferredRender();
	postProcessing();

	if(ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		DeferredComposite.Clear();
		FinalComposite.Clear();
		GBuffer.Clear();
		WorkBuffer1_2.Clear();
		WorkBuffer2_2.Clear();
		WorkBuffer1_4.Clear();
		WorkBuffer2_4.Clear();
		WorkBuffer1_8.Clear();
		WorkBuffer2_8.Clear();
				
		glClearColor(1.0f, 1.0f, 1.0f, 0);
		EdgeMap.Clear();
		SSAOWorkBuffer1.Clear();
		SSAOWorkBuffer2.Clear();

		computeRender();
		toonShading();
		deferredRender();
		postProcessing();
	}


	//computeRender();
	//toonShading();
	//deferredRender();
	//postProcessing();

	//Swap Buffers and Render//
	//glfwSwapBuffers(window);
	//glutSwapBuffers();
}


//	All the models are drawn in here
void Game::drawOld()
{
	glPolygonMode( GL_FRONT_AND_BACK, wireframeToggle );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	EdgeMap.Clear();
	glClearColor(0.0f, 0.0f, 0.0f, 0);
	GBuffer.Clear();
	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();
	// Clear Buffers //
	
	
	

	satColor clearColor = curLevel->calcFogColor();
	glClearColor(clearColor.r, clearColor.g, clearColor.b, 0);
	FinalComposite.Clear();
	DeferredComposite.Clear();
	
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//glClearColor(0.0f, 0.0f, 0.0f, 0);
	
		// Render The Shadow Map //
	glViewport(0, 0, SHADOW_RESOLUTION >> UpScaleShadowResolution, SHADOW_RESOLUTION >> UpScaleShadowResolution); //Set up the viewport (consider putting this in the framebuffer class!)
	
	computeShadowMap();
	GBufferPass.unbind(); //Unbind the shader so we don't accidentally overwrite data.
	// Render The Scene //
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
	if(ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}
	computeRender();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	toonShading();
	deferredRender();
	postProcessing();

	if(ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		satColor clearColor = curLevel->calcFogColor();
		glClearColor(clearColor.r, clearColor.g, clearColor.b, 0);
		DeferredComposite.Clear();
		FinalComposite.Clear();
		
		//WorkBuffer1.Clear();
		//WorkBuffer2.Clear();
		//SSAOWorkBuffer1.Clear();
		//SSAOWorkBuffer2.Clear();
		//EdgeMap.Clear();

		glClearColor(0.0f, 0.0f, 0.0f, 0);
		GBuffer.Clear();

		glClearColor(1.0f, 1.0f, 1.0f, 0);
		EdgeMap.Clear();
		SSAOWorkBuffer1.Clear();
		SSAOWorkBuffer2.Clear();
		
		glPolygonMode(GL_FRONT_AND_BACK, wireframeToggle);
		computeRender();

		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
		toonShading();
		deferredRender();
		postProcessing();
	}

	

	//Swap Buffers and Render//
	//glfwSwapBuffers(window);
	//glutSwapBuffers();
}

void Game::drawModels()
{

}

void Game::bindBlocks(int& i, int& j)
{
	
}

void Game::bindBlocksShadow(int& i, int& j)
{
	
}

void Game::drawBlocksShadow()
{
	satMat4 tempMatrix;
	float zTranslate;

	float posX;
	float posY;
	char curBlock;

	GLint uPassLocationView		= PassThroughShadowShader.getUniformLocation("uView");
	GLint uPassLocationModel	= PassThroughShadowShader.getUniformLocation("uModel");
	GLint uPassLocationProj		= PassThroughShadowShader.getUniformLocation("uProj");


	



	for(unsigned c = 0; c < curLevel->listVisual.size() * SatDebugDrawVisualActive; ++c)
	{			
		Visual* objectSelection = &curLevel->Block;

		if(curLevel->listVisual[c].object.size() <= 1)
		{
			objectSelection = &curLevel->listVisual[c].object[0];
		
			glBindVertexArray(objectSelection->model->VAO);
			
			glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitiveShadowPolygonMode);
			glLineWidth(objectSelection->primitiveLineWidth);
			glCullFace(objectSelection->primitiveShadowCullFace);
			
		}

		PassThroughShadowShader.bind();
		PassThroughShadowShader.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
		PassThroughShadowShader.sendUniformMat4(uPassLocationProj, ShadowProjection.Float(), tru);
		PassThroughShadowShader.sendUniform("uScale", satVec4(curLevel->listVisual[c].transformScale, 1.0f));
				
		ShadowMap.bind();

		zTranslate = 0.0f;

		for (int i = adjust.y; i < Min(RENDER_HEIGHT + (int)adjust.y, curLevel->Y); ++i)
		{
			for (int j = Max(adjust.x - !SatDebugCascadeToggleActive * 18, 0.0f); j < Min(RENDER_WIDTH + (int)adjust.x + !SatDebugCascadeToggleActive * 12, (int)curLevel->X); ++j)
			{
				posX = j - adjust.x;
				posY = i - adjust.y;
				curBlock = curLevel->lMatrix[i][j];
				
				if(curBlock == curLevel->listVisual[c].character)
				{
					if(curLevel->listVisual[c].object.size() > 1)
					{
						objectSelection = &curLevel->listVisual[c].object[curLevel->lRandomMatrix[i][j][0] % curLevel->listVisual[c].object.size()];
						
						glBindVertexArray(objectSelection->model->VAO);
						
						glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitiveShadowPolygonMode);
						glLineWidth(objectSelection->primitiveLineWidth);
						glCullFace(objectSelection->primitiveShadowCullFace);
					}

					if(SatDebugWavyToggleActive ^ curLevel->BlockWavyActive)
					{
						//zTranslate = Timer::sin(blockZSinMult, (j + i) * blockZSinAddMult) * 0.5;
						//zTranslate = -abs(Timer::sin(curLevel->SongTempo / 60.0f * satMath::PI * 0.5f, (j + i) * blockZSinAddMult) * 1.0f) + 0.5; 
						//zTranslate = -abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (j + i) * blockZSinAddMult) * 1.0f) + 0.5;
						//zTranslate = -powf(abs(Timer::sin(0.0f, (float)(curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (-j + i * 0.25f) * blockZSinAddMult) * 1.0f), 
						//	0.75f) + 0.5f;	
						zTranslate = -powf(abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (-j + (i * Timer::sinRange(0.0f, curLevel->SongTempo / 60.0f * 0.125f * TimeInLevel, -0.125f, 0.125f))) * blockZSinAddMult) * 1.0f), 
							0.75f) + 0.5f;	
					}
					else
					{
						
					}
							
					tempMatrix.TranslateInstance(-TotalTranslation + curLevel->listVisual[c].transform + satVec3(
						-(posX * 2) + 20 - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialXExp) * blockPolynomialXMult * (posX - (RenderWidthHalf + (Timer::sin(0.0f, TimeInLevel * blockXSinMult) * blockXWidthMult * RenderWidthHalf))),
						(posY * 2) - 14 + (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY - (RenderHeightHalf + (Timer::sin(0.0f, TimeInLevel * blockYSinMult) * blockYHeightMult * RenderHeightHalf))),
						zTranslate - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialZExp) * blockPolynomialZMult));
				
					satVec3 rotateMod = curLevel->listVisual[c].speedRotate;

					if (curLevel->listVisual[c].healable || 
						curLevel->listVisual[c].doubleShootPickup || 
						curLevel->listVisual[c].autoShootPickup || 
						curLevel->listVisual[c].invinciblePickup)
					{
						rotateMod.y += 180.0f;
					}

					rotateMod *= curLevel->SongTempo / 60.0f;
					satVec3 rotateRandom = 0.0f;
					
					rotateRandom += curLevel->calcRandomRotations(&curLevel->listVisual[c], i, j);
					
					satMat4 rotateMatrix;
					rotateMatrix.TranslateInstance(-curLevel->listVisual[c].originRotate);
					rotateMatrix.RotateX(TimeInLevel * rotateMod.x + rotateRandom.x);
					rotateMatrix.RotateY(TimeInLevel * rotateMod.y + rotateRandom.y);
					rotateMatrix.RotateZ(TimeInLevel * rotateMod.z + rotateRandom.z);
					rotateMatrix.Translate(curLevel->listVisual[c].originRotate * curLevel->listVisual[c].transformScale);
					tempMatrix = rotateMatrix * tempMatrix;

					PassThroughShadowShader.sendUniformMat4(uPassLocationView, (tempMatrix * ShadowTransform).Inverse().Float(), tru);

					glDrawArrays(GL_TRIANGLES, 0, objectSelection->model->GetNumVertices());
				}
			}
		}
	}

	for(unsigned c = 0; c < curLevel->listEnemy.size() * SatDebugDrawEnemyActive; ++c)
	{
		Visual* objectSelection = &curLevel->Block;

		if(curLevel->listEnemy[c].object.size() <= 1)
		{
			objectSelection = &curLevel->listEnemy[c].object[0];
		
			glBindVertexArray(objectSelection->model->VAO);
			
			glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitiveShadowPolygonMode);
			glLineWidth(objectSelection->primitiveLineWidth);
			glCullFace(objectSelection->primitiveShadowCullFace);
			
		}

		PassThroughShadowShader.bind();
		PassThroughShadowShader.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
		PassThroughShadowShader.sendUniformMat4(uPassLocationProj, ShadowProjection.Float(), tru);

		ShadowMap.bind();

		zTranslate = 0.0f;

		for (int i = adjust.y; i < Min(RENDER_HEIGHT + (int)adjust.y, curLevel->Y); ++i)
		{
			for (int j = Max(adjust.x - !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 18.0f, 0.0f); j < Min(RENDER_WIDTH + (int)adjust.x + !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 6, (int)curLevel->X); ++j)
			{
				posX = j - adjust.x;
				posY = i - adjust.y;
				curBlock = curLevel->lMatrix[i][j];

				if(curBlock == curLevel->listEnemy[c].character)
				{
					if(curLevel->listEnemy[c].object.size() > 1)
					{
						objectSelection = &curLevel->listEnemy[c].object[curLevel->lRandomMatrix[i][j][0] % curLevel->listEnemy[c].object.size()];
						
						glBindVertexArray(objectSelection->model->VAO);
						
						glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitiveShadowPolygonMode);
						glLineWidth(objectSelection->primitiveLineWidth);
						glCullFace(objectSelection->primitiveShadowCullFace);
					}
					
					//tempMatrix.LoadIdentity();
					tempMatrix.TranslateInstance(-TotalTranslation + curLevel->listEnemy[c].transform + satVec3(
						-(posX * 2) + 20 - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialXExp) * blockPolynomialXMult * (posX - (RenderWidthHalf + (Timer::sin(0.0f, TimeInLevel * blockXSinMult) * blockXWidthMult * RenderWidthHalf))),
						(posY * 2) - 14 + (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY - (RenderHeightHalf + (Timer::sin(0.0f, TimeInLevel * blockYSinMult) * blockYHeightMult * RenderHeightHalf))),
						//posY * 2 - 14 + k + powf(abs(posX-(blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY-(RenderHeightHalf)), 
						zTranslate - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialZExp) * blockPolynomialZMult));
				
					satVec3 rotateMod = curLevel->listEnemy[c].speedRotate;

					if (curLevel->listEnemy[c].healable || 
						curLevel->listEnemy[c].doubleShootPickup || 
						curLevel->listEnemy[c].autoShootPickup || 
						curLevel->listEnemy[c].invinciblePickup)
					{
						rotateMod.y += 180.0f;
					}

					rotateMod *= curLevel->SongTempo / 60.0f;
					satVec3 rotateRandom = 0.0f;
										
					rotateRandom += curLevel->calcRandomRotations(&curLevel->listEnemy[c], i, j);

					satMat4 rotateMatrix;
					rotateMatrix.TranslateInstance(-curLevel->listEnemy[c].originRotate);
					rotateMatrix.RotateX(TimeInLevel * rotateMod.x + rotateRandom.x);
					rotateMatrix.RotateY(TimeInLevel * rotateMod.y + rotateRandom.y);
					rotateMatrix.RotateZ(TimeInLevel * rotateMod.z + rotateRandom.z);
					rotateMatrix.Translate(curLevel->listEnemy[c].originRotate * curLevel->listEnemy[c].transformScale);
					tempMatrix = rotateMatrix * tempMatrix;

					PassThroughShadowShader.sendUniformMat4(uPassLocationView, (tempMatrix * ShadowTransform).Inverse().Float(), tru);
									
					glDrawArrays(GL_TRIANGLES, 0, objectSelection->model->GetNumVertices());
				}
			}
		}
	}


	glBindVertexArray(0);

	for (unsigned c = 0; c < curLevel->listBackgroundVisual.size() * SatDebugDrawBackgroundActive; ++c)
	{
		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, BlockHeight.TexObj);

		glBindVertexArray(curLevel->listBackgroundVisual[c].object.model->VAO);
				
		glPolygonMode(GL_FRONT_AND_BACK, curLevel->listBackgroundVisual[c].object.primitiveShadowPolygonMode);
		glLineWidth(curLevel->listBackgroundVisual[c].object.primitiveLineWidth);
		//glEnable(GL_LINE_SMOOTH);
		glCullFace(curLevel->listBackgroundVisual[c].object.primitiveShadowCullFace);

		PassThroughShadowShader.bind();
		PassThroughShadowShader.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
		PassThroughShadowShader.sendUniformMat4(uPassLocationProj, ShadowProjection.Float(), tru);

		ShadowMap.bind();
		
		satVec2 spacing = curLevel->listBackgroundVisual[c].appearLoop;

		for (int i = 0; 
			i < 40 / spacing.y; 
			i += 1)
		{
			for (int j = 0; 
				j < 100 / spacing.x + 2; 
				j += 1)
			{
				satVec2 adjustInt;
				satVec2 adjustFract;
				adjustInt = modf(adjust.x, &adjustFract.x);

				satVec2 adjustMod = adjust.xy();
				int jMult = 0;
				int iMult = 0;
				while(adjustMod.x > spacing.x)
				{
					adjustMod.x = adjustMod.x - spacing.x;
					jMult++;
				}

				posX = (j+jMult)*spacing.x - adjust.x;
				posY = (i+iMult)*spacing.y - adjust.y;
				
				zTranslate = 0.0f;

				tempMatrix.LoadIdentity();
				//tempMatrix.Scale(curLevel->listBackgroundVisual[c].transformScale);
				tempMatrix.Translate(-TotalTranslation + curLevel->listBackgroundVisual[c].transform + satVec3(
					-(posX * 2) + 20,
					(posY * 2) - 14,
					zTranslate));
				
				satVec3 rotation;
				rotation = curLevel->listBackgroundVisual[c].rotate + curLevel->listBackgroundVisual[c].speedRotate * TimeInLevel;
				rotation += Timer::sinRange(0.0f, TimeInLevel * curLevel->listBackgroundVisual[c].rangeSpeedRotate, 0.0f, curLevel->listBackgroundVisual[c].rangeRotate);

				tempMatrix.RotateX(rotation.x);
				tempMatrix.RotateY(rotation.y);
				tempMatrix.RotateZ(rotation.z);

				//tempMatrix.RotateX(curLevel->listBackgroundVisual[c].rotate.x);
				//tempMatrix.RotateY(curLevel->listBackgroundVisual[c].rotate.y);
				//tempMatrix.RotateZ(curLevel->listBackgroundVisual[c].rotate.z);

				PassThroughShadowShader.sendUniform("uScale", satVec4(curLevel->listBackgroundVisual[c].transformScale, 1.0f));
				PassThroughShadowShader.sendUniformMat4(uPassLocationView, (tempMatrix* ShadowTransform).Inverse().Float(), tru);
				
				glDrawArrays(GL_TRIANGLES, 0, curLevel->listBackgroundVisual[c].object.model->GetNumVertices());				
			}
		}

		PassThroughShadowShader.sendUniform("uScale", satVec4(1.0f, 1.0f));

		glBindVertexArray(0);
		
		Texture::unbind(7);
		Texture::unbind(6);
		Texture::unbind(5);
		Texture::unbind(4);
		Texture::unbind(3);
		Texture::unbind(2);
		Texture::unbind(1);
		Texture::unbind(0);


	}


}



//Draw all the Blocks on the grid
void Game::drawBlocks()
{
	int randomSave = rand();

	lightEnemy.clear();

	satMat4 tempMatrix;
	float zTranslate = 0.0f;
	float posX;
	float posY;
	char curBlock;
	bool initialize;

	GLint uPassLocationView		=	GBufferCubePass.getUniformLocation("uView");
	GLint uPassLocationModel	=	GBufferCubePass.getUniformLocation("uModel");
	GLint uPassLocationProj		=	GBufferCubePass.getUniformLocation("uProj");
	GLint uLocationAmbientAdd	=	GBufferCubePass.getUniformLocation("uAmbientAdd");
	GLint uLocationDiffuseAdd	=	GBufferCubePass.getUniformLocation("uDiffuseAdd");
	GLint uLocationSpecularAdd	=	GBufferCubePass.getUniformLocation("uSpecularAdd");
	GLint uLocationShinyAdd		=	GBufferCubePass.getUniformLocation("uShinyAdd");
	GLint uLocationEmissiveAdd	=	GBufferCubePass.getUniformLocation("uEmissiveAdd");
	GLint uLocationAmbientMult	=	GBufferCubePass.getUniformLocation("uAmbientMult");
	GLint uLocationDiffuseMult	=	GBufferCubePass.getUniformLocation("uDiffuseMult");
	GLint uLocationSpecularMult	=	GBufferCubePass.getUniformLocation("uSpecularMult");
	GLint uLocationShinyMult	=	GBufferCubePass.getUniformLocation("uShinyMult");
	GLint uLocationEmissiveMult =	GBufferCubePass.getUniformLocation("uEmissiveMult");




	






	for (unsigned c = 0; c < curLevel->listEnemy.size() * SatDebugDrawEnemyActive; ++c)
	{
		Visual* objectSelection = &curLevel->Block;

		if(curLevel->listEnemy[c].object.size() <= 1)
		{
			objectSelection = &curLevel->listEnemy[c].object[0];
		
			objectSelection->diffuse->bind(0);

			bindEmit(	objectSelection->emissive);
			bindNormal(	objectSelection->normal);
			bindShiny(	objectSelection->specular);
			bindCubeMap(&AmbientCubeMapTexture);

			BlockHeight.bind(7);

			glBindVertexArray(objectSelection->model->VAO);

			glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitivePolygonMode);
			glLineWidth(objectSelection->primitiveLineWidth);
			glCullFace(objectSelection->primitiveCullFace);
				
		}

		



		

		GBufferCubePass.bind();
		GBufferCubePass.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
		GBufferCubePass.sendUniformMat4(uPassLocationProj, activeCamera->GetUniformProjection(), tru);
		GBufferCubePass.sendUniform("uScale", satVec4(curLevel->listEnemy[c].transformScale, 1.0f));
		GBufferCubePass.sendUniform("uDiffuseTex", 0);
		GBufferCubePass.sendUniform("uNormalTex", 1);
		GBufferCubePass.sendUniform("uShinyTex", 2);
		GBufferCubePass.sendUniform("uSpecularTex", 2);
		GBufferCubePass.sendUniform("uEmissiveTex", 3);
		GBufferCubePass.sendUniform("uCubeMap", 6);
		GBufferCubePass.sendUniform("uParallaxTex", 7);	//
		GBufferCubePass.sendUniform("uParallaxActive", true);
		GBufferCubePass.sendUniform("uReflectAmount", curLevel->listEnemy[c].environmentAmount * EnviromentMapActive);

		GBufferCubePass.sendUniform(uLocationAmbientAdd,	curLevel->listEnemy[c].ambientAdd);
		GBufferCubePass.sendUniform(uLocationDiffuseAdd,	curLevel->listEnemy[c].diffuseAdd);
		GBufferCubePass.sendUniform(uLocationSpecularAdd,	curLevel->listEnemy[c].specularAdd);
		GBufferCubePass.sendUniform(uLocationShinyAdd,		curLevel->listEnemy[c].shinyAdd);
		GBufferCubePass.sendUniform(uLocationEmissiveAdd,	curLevel->listEnemy[c].emissiveAdd);
		GBufferCubePass.sendUniform(uLocationAmbientMult,	curLevel->listEnemy[c].ambientMult);
		GBufferCubePass.sendUniform(uLocationDiffuseMult,	curLevel->listEnemy[c].diffuseMult);
		GBufferCubePass.sendUniform(uLocationSpecularMult,	curLevel->listEnemy[c].specularMult);
		GBufferCubePass.sendUniform(uLocationShinyMult,		curLevel->listEnemy[c].shinyMult);
		GBufferCubePass.sendUniform(uLocationEmissiveMult,	curLevel->listEnemy[c].emissiveMult);

		GBuffer.bind();

		int yDrawFrom = adjust.y;
		int xDrawFrom = Max(adjust.x - !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 18, 0.0f);
		int yDrawTo = Min(RENDER_HEIGHT + (int)adjust.y, curLevel->Y);
		int xDrawTo = Min(RENDER_WIDTH + (int)adjust.x + !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 18, (int)curLevel->X);
		
		bool debugRegularLook = false; //sat::key::CheckKey(sf::Keyboard::X);

		for (int i = yDrawFrom; i < yDrawTo; ++i)
		{				
			int yDraw = i + (yDrawTo-yDrawFrom)*0.5f;
			if(yDraw >= yDrawTo)
			{
				yDraw = yDrawTo - (i-yDrawFrom) - 1;
			}
			posY = yDraw - adjust.y;

			for (int j = xDrawFrom; j < xDrawTo; ++j)
			{
				posX = j - adjust.x;
				int xDraw = j + (xDrawTo-xDrawFrom) * 0.5f;;

				if(xDraw >= xDrawTo)
				{	
					xDraw = xDrawTo - (j-xDrawFrom) - 1;
				}
												
				posX = xDraw - adjust.x;

				if(debugRegularLook)
				{
					xDraw = j;	
					posX = xDraw - adjust.x;
				}
				//posX = j - adjust.x;
				//posY = i - adjust.y;
				curBlock = curLevel->lMatrix[yDraw][xDraw];
				
				if (curBlock == curLevel->listEnemy[c].character)
				{
					zTranslate = 0.0f;
					
					if(SatDebugWavyToggleActive ^ curLevel->BlockWavyActive)
					{
						//zTranslate = Timer::sin(blockZSinMult, (xDraw + yDraw) * blockZSinAddMult) * 0.5;
						//zTranslate = -abs(Timer::sin(curLevel->SongTempo / 60.0f * satMath::PI * 0.5f, (xDraw + yDraw) * blockZSinAddMult) * 1.0f) + 0.5; 
						//zTranslate = -abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (xDraw + yDraw) * blockZSinAddMult) * 1.0f) + 0.5f;	
						zTranslate = -powf(abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (-xDraw + yDraw * 0.25f) * blockZSinAddMult) * 1.0f), 
							0.75f) + 0.5f;	
						//zTranslate = sinf(Timer::currentf() * 5.0f + posX * 0.5) * 0.5f + 0.5f;
					}

					//tempMatrix.LoadIdentity();
					tempMatrix.TranslateInstance(initialTranslation + curLevel->listEnemy[c].transform + satVec3(
						-(posX * 2) + 20 - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialXExp) * blockPolynomialXMult * (posX - (RenderWidthHalf + (Timer::sin(0.0f, TimeInLevel * blockXSinMult) * blockXWidthMult * RenderWidthHalf))),
						(posY * 2) - 14 + (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY - (RenderHeightHalf + (Timer::sin(0.0f, TimeInLevel * blockYSinMult) * blockYHeightMult * RenderHeightHalf))),
						//posY * 2 - 14 + k + powf(abs(posX-(blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY-(RenderHeightHalf)), 
						zTranslate - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialZExp) * blockPolynomialZMult));

					satVec3 rotateMod = curLevel->listEnemy[c].speedRotate;

					if (curLevel->listEnemy[c].healable || 
						curLevel->listEnemy[c].doubleShootPickup || 
						curLevel->listEnemy[c].autoShootPickup || 
						curLevel->listEnemy[c].invinciblePickup)
					{
						rotateMod.y += 180.0f;
					}

					rotateMod *= curLevel->SongTempo / 60.0f;
					satVec3 rotateRandom = 0.0f;
										
					rotateRandom += curLevel->calcRandomRotations(&curLevel->listEnemy[c], yDraw, xDraw);

					satMat4 rotateMatrix;
					rotateMatrix.TranslateInstance(-curLevel->listEnemy[c].originRotate);
					rotateMatrix.RotateX(TimeInLevel * rotateMod.x + rotateRandom.x);
					rotateMatrix.RotateY(TimeInLevel * rotateMod.y + rotateRandom.y);
					rotateMatrix.RotateZ(TimeInLevel * rotateMod.z + rotateRandom.z);
					rotateMatrix.Translate(curLevel->listEnemy[c].originRotate * curLevel->listEnemy[c].transformScale);
					tempMatrix = rotateMatrix * tempMatrix;

					GBufferCubePass.sendUniformMat4(uPassLocationView, tempMatrix.Inverse().Float(), tru);
					if(curLevel->listEnemy[c].pointLightActive)
					{
						float lightIntensity = Lerp(1.0f, 0.05f, powf(1.0f - ((posX - 18) * 0.015f), 2.0f));
						lightIntensity *= Lerp(1.0f, 1.25f, Timer::sinP(0.0f, 8 * TimeInLevel + (xDraw/8.0f)));

						//lightEnemy.push_back(PointLight(Lerp(sat::color::slightlightred, sat::color::lightred, Timer::sinP(0.0f, 8 * TimeInLevel + (j/8))), satVec3(
						//	((j * 2 - adjust.x * 2) - 19.5f) - initialTranslation.x,
						//	-(i * 2 - 12.5f + initialTranslation.y),
						//	-initialTranslation.z + 3.0f), 1.0f, lightIntensity, lightIntensity * 0.5f));

						float fadeOut = 1.0f;

						if(posX < 8.0f || posX > 20.0f )
						{
							fadeOut *= InverseLerp(posX, 0.0f, 6.0f);
						}

						lightEnemy.push_back(PointLight(fadeOut * curLevel->listEnemy[c].pointLightMult * Lerp(curLevel->listEnemy[c].pointLightColor * 1.1f, curLevel->listEnemy[c].pointLightColor, Timer::sinP(0.0f, 8 * TimeInLevel + (xDraw/8))), 
							curLevel->listEnemy[c].pointLightPos + satVec3(
							((xDraw * 2 - adjust.x * 2) - 19.5f) - initialTranslation.x,
							-(yDraw * 2 - 12.5f + initialTranslation.y),
							zTranslate-initialTranslation.z), 1.0f * curLevel->listEnemy[c].pointLightAttenMult, lightIntensity * curLevel->listEnemy[c].pointLightAttenMult, lightIntensity * 0.5f * curLevel->listEnemy[c].pointLightAttenMult));
					}
					glDrawArrays(GL_TRIANGLES, 0, objectSelection->model->GetNumVertices());
				}
			}
		}

		glBindVertexArray(0);

		Texture::unbind(7);
		Texture::unbind(6);
		Texture::unbind(5);
		Texture::unbind(4);
		Texture::unbind(3);
		Texture::unbind(2);
		Texture::unbind(1);
		Texture::unbind(0);
	}



	for(unsigned c = 0; c < curLevel->listVisual.size() * SatDebugDrawVisualActive; ++c)
	{
		initialize = false;

		Visual* objectSelection = &curLevel->listVisual[c].object[0];


		GBuffer.bind();

		int yDrawFrom = adjust.y;
		int xDrawFrom = Max(adjust.x - !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 18, 0.0f);
		int yDrawTo = Min(RENDER_HEIGHT + (int)adjust.y, curLevel->Y);
		int xDrawTo = Min(RENDER_WIDTH + (int)adjust.x + !(SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * 18, (int)curLevel->X);
		
		//bool debugPrintZ = sat::key::CheckKey(sf::Keyboard::Z);
		//if(debugPrintZ)
		//	std::cout << "\n";

		bool debugRegularLook = false; //sat::key::CheckKey(sf::Keyboard::X);

		for (int i = yDrawFrom; i < yDrawTo; ++i)
		{				
			int yDraw = i + (yDrawTo-yDrawFrom)*0.5f;
			if(yDraw >= yDrawTo)
			{
				yDraw = yDrawTo - (i-yDrawFrom) - 1;
			}
			posY = yDraw - adjust.y;

			for (int j = xDrawFrom; j < xDrawTo; ++j)
			{
				posX = j - adjust.x;
				int xDraw = j + (xDrawTo-xDrawFrom)*0.5f;;

				if(xDraw >= xDrawTo)
				{	
					xDraw = xDrawTo - (j-xDrawFrom) - 1;
				}
												
				posX = xDraw - adjust.x;

				if(debugRegularLook)
				{
					xDraw = j;	
					posX = xDraw - adjust.x;
				}


				curBlock = curLevel->lMatrix[yDraw][xDraw];

				if(curBlock == curLevel->listVisual[c].character)
				{
					if(initialize == false)
					{
						initialize = tru;

						if(curLevel->listVisual[c].object.size() <= 1)
						{
							objectSelection = &curLevel->listVisual[c].object[0];
						
							objectSelection->diffuse->bind(0);

							bindEmit(	objectSelection->emissive);
							bindNormal(	objectSelection->normal);
							bindShiny(	objectSelection->specular);
							bindCubeMap(&AmbientCubeMapTexture);

							glActiveTexture(GL_TEXTURE7);
							glBindTexture(GL_TEXTURE_2D, BlockHeight.TexObj);

							glBindVertexArray(objectSelection->model->VAO);

							glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitivePolygonMode);
							glLineWidth(objectSelection->primitiveLineWidth);
							glCullFace(objectSelection->primitiveCullFace);
								
						}


						GBufferCubePass.bind();
						GBufferCubePass.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
						GBufferCubePass.sendUniformMat4(uPassLocationProj, activeCamera->GetUniformProjection(), tru);
						GBufferCubePass.sendUniform("uScale", satVec4(curLevel->listVisual[c].transformScale, 1.0f));
						GBufferCubePass.sendUniform("uDiffuseTex", 0);
						GBufferCubePass.sendUniform("uNormalTex", 1);
						GBufferCubePass.sendUniform("uShinyTex", 2);
						GBufferCubePass.sendUniform("uSpecularTex", 2);
						GBufferCubePass.sendUniform("uEmissiveTex", 3);
						GBufferCubePass.sendUniform("uCubeMap", 6);
						GBufferCubePass.sendUniform("uParallaxTex", 7);	
						GBufferCubePass.sendUniform("uParallaxActive", true);
						GBufferCubePass.sendUniform("uReflectAmount", curLevel->listVisual[c].environmentAmount * EnviromentMapActive);

						if (!curLevel->calcBlockAmbientAddToggle() || curLevel->listVisual[c].ambientAddEqualZero == false)
							GBufferCubePass.sendUniform(uLocationAmbientAdd, curLevel->calcBlockAmbientAdd() + curLevel->listVisual[c].ambientAdd);
						if (!curLevel->calcBlockDiffuseAddToggle() || curLevel->listVisual[c].diffuseAddEqualZero == false)
							GBufferCubePass.sendUniform(uLocationDiffuseAdd, curLevel->calcBlockDiffuseAdd() + curLevel->listVisual[c].diffuseAdd);
						if (!curLevel->calcBlockSpecularAddToggle() || curLevel->listVisual[c].specularAddEqualZero == false)
							GBufferCubePass.sendUniform(uLocationSpecularAdd, curLevel->calcBlockSpecularAdd() + curLevel->listVisual[c].specularAdd);
						if (!curLevel->calcBlockShinyAddToggle() || curLevel->listVisual[c].shinyAddEqualZero == false)
							GBufferCubePass.sendUniform(uLocationShinyAdd, curLevel->calcBlockShinyAdd() + curLevel->listVisual[c].shinyAdd);
						if (!curLevel->calcBlockEmissiveAddToggle() || curLevel->listVisual[c].emissiveAddEqualZero == false)
							GBufferCubePass.sendUniform(uLocationEmissiveAdd, curLevel->calcBlockEmissiveAdd() + curLevel->listVisual[c].emissiveAdd);
						if (!curLevel->calcBlockAmbientMultToggle() || curLevel->listVisual[c].ambientMultEqualOne == false)
							GBufferCubePass.sendUniform(uLocationAmbientMult, curLevel->calcBlockAmbientMult() * curLevel->listVisual[c].ambientMult);
						if (!curLevel->calcBlockDiffuseMultToggle() || curLevel->listVisual[c].diffuseMultEqualOne == false)
							GBufferCubePass.sendUniform(uLocationDiffuseMult, curLevel->calcBlockDiffuseMult() * curLevel->listVisual[c].diffuseMult);
						if (!curLevel->calcBlockSpecularMultToggle() || curLevel->listVisual[c].specularMultEqualOne == false)
							GBufferCubePass.sendUniform(uLocationSpecularMult, curLevel->calcBlockSpecularMult() * curLevel->listVisual[c].specularMult);
						if (!curLevel->calcBlockShinyMultToggle() || curLevel->listVisual[c].shinyMultEqualOne == false)
							GBufferCubePass.sendUniform(uLocationShinyMult, curLevel->calcBlockShinyMult() * curLevel->listVisual[c].shinyMult);
						if (!curLevel->calcBlockEmissiveMultToggle() || curLevel->listVisual[c].emissiveMultEqualOne == false)
							GBufferCubePass.sendUniform(uLocationEmissiveMult, curLevel->calcBlockEmissiveMult() * curLevel->listVisual[c].emissiveMult);
					}


					if(curLevel->listVisual[c].object.size() > 1)
					{
						objectSelection = &curLevel->listVisual[c].object[curLevel->lRandomMatrix[yDraw][xDraw][0] % curLevel->listVisual[c].object.size()];
						
						glBindVertexArray(objectSelection->model->VAO);
						
						objectSelection->diffuse->bind(0);
						bindEmit(	objectSelection->emissive);
						bindNormal(	objectSelection->normal);
						bindShiny(	objectSelection->specular);
						bindCubeMap(&AmbientCubeMapTexture);

						glActiveTexture(GL_TEXTURE7);
						glBindTexture(GL_TEXTURE_2D, BlockHeight.TexObj);

						glBindVertexArray(objectSelection->model->VAO);

						glPolygonMode(GL_FRONT_AND_BACK, objectSelection->primitivePolygonMode);
						glLineWidth(objectSelection->primitiveLineWidth);
						glCullFace(objectSelection->primitiveCullFace);
					}


					zTranslate = 0.0f;
					
					if(SatDebugWavyToggleActive ^ curLevel->BlockWavyActive)
					{
						//zTranslate = Timer::sin(blockZSinMult, (xDraw + yDraw) * blockZSinAddMult) * 0.5;
						//zTranslate = -abs(Timer::sin(curLevel->SongTempo / 60.0f * satMath::PI * 0.5f, (xDraw + yDraw) * blockZSinAddMult) * 1.0f) + 0.5; 
						//zTranslate = -abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (xDraw + yDraw) * blockZSinAddMult) * 1.0f) + 0.5f;	
						zTranslate = -powf(abs(Timer::sin(0.0f, (curLevel->SongTempo / 60.0f * satMath::PI * 0.5f * TimeInLevel) + (-xDraw + (yDraw * Timer::sinRange(0.0f, curLevel->SongTempo / 60.0f * 0.125f * TimeInLevel, -0.125f, 0.125f))) * blockZSinAddMult) * 1.0f), 
							0.75f) + 0.5f;	
						//zTranslate = sinf(Timer::currentf() * 5.0f + posX * 0.5) * 0.5f + 0.5f;
					}
					

					//tempMatrix.LoadIdentity();
					tempMatrix.TranslateInstance(initialTranslation + curLevel->listVisual[c].transform + satVec3(
						-(posX * 2) + 20 - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialXExp) * blockPolynomialXMult * (posX - (RenderWidthHalf + (Timer::sin(0.0f, TimeInLevel * blockXSinMult) * blockXWidthMult * RenderWidthHalf))),
						(posY * 2) - 14 + (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY - (RenderHeightHalf + (Timer::sin(0.0f, TimeInLevel * blockYSinMult) * blockYHeightMult * RenderHeightHalf))),
						//posY * 2 - 14 + k + powf(abs(posX-(blockXShift)), blockPolynomialYExp) * blockPolynomialYMult * (posY-(RenderHeightHalf)), 
						zTranslate - (SatDebugCascadeToggleActive ^ curLevel->BlockCascadeActive) * powf(abs(posX - (blockXShift)), blockPolynomialZExp) * blockPolynomialZMult));
				
					//tempMatrix.Self();
					satVec3 rotateMod = curLevel->listVisual[c].speedRotate;

					if (curLevel->listVisual[c].healable || 
						curLevel->listVisual[c].doubleShootPickup || 
						curLevel->listVisual[c].autoShootPickup || 
						curLevel->listVisual[c].invinciblePickup)
					{
						rotateMod.y += 180.0f;
					}

					rotateMod *= curLevel->SongTempo / 60.0f;
					satVec3 rotateRandom = 0.0f;

					
					rotateRandom += curLevel->calcRandomRotations(&curLevel->listVisual[c], yDraw, xDraw);

					satMat4 rotateMatrix;
					rotateMatrix.TranslateInstance(-curLevel->listVisual[c].originRotate);
					rotateMatrix.RotateX(TimeInLevel * rotateMod.x + rotateRandom.x);
					rotateMatrix.RotateY(TimeInLevel * rotateMod.y + rotateRandom.y);
					rotateMatrix.RotateZ(TimeInLevel * rotateMod.z + rotateRandom.z);
					rotateMatrix.Translate(curLevel->listVisual[c].originRotate * curLevel->listVisual[c].transformScale);
					tempMatrix = rotateMatrix * tempMatrix;

					GBufferCubePass.sendUniformMat4(uPassLocationView, tempMatrix.Inverse().Float(), tru);

					if (curLevel->calcBlockAmbientAddToggle())
						GBufferCubePass.sendUniform(uLocationAmbientAdd, curLevel->calcBlockAmbientAdd(yDraw, xDraw) + curLevel->listVisual[c].ambientAdd);
					if (curLevel->calcBlockDiffuseAddToggle())
						GBufferCubePass.sendUniform(uLocationDiffuseAdd, curLevel->calcBlockDiffuseAdd(yDraw, xDraw) + curLevel->listVisual[c].diffuseAdd);
					if (curLevel->calcBlockSpecularAddToggle())
						GBufferCubePass.sendUniform(uLocationSpecularAdd, curLevel->calcBlockSpecularAdd(yDraw, xDraw) + curLevel->listVisual[c].specularAdd);
					if (curLevel->calcBlockShinyAddToggle())
						GBufferCubePass.sendUniform(uLocationShinyAdd, curLevel->calcBlockShinyAdd(yDraw, xDraw) + curLevel->listVisual[c].shinyAdd);
					if (curLevel->calcBlockEmissiveAddToggle())
						GBufferCubePass.sendUniform(uLocationEmissiveAdd, curLevel->calcBlockEmissiveAdd(yDraw, xDraw) + curLevel->listVisual[c].emissiveAdd);
					if (curLevel->calcBlockAmbientMultToggle())
						GBufferCubePass.sendUniform(uLocationAmbientMult, curLevel->calcBlockAmbientMult(yDraw, xDraw) * curLevel->listVisual[c].ambientMult);
					if (curLevel->calcBlockDiffuseMultToggle())
						GBufferCubePass.sendUniform(uLocationDiffuseMult, curLevel->calcBlockDiffuseMult(yDraw, xDraw) * curLevel->listVisual[c].diffuseMult);
					if (curLevel->calcBlockSpecularMultToggle())
						GBufferCubePass.sendUniform(uLocationSpecularMult, curLevel->calcBlockSpecularMult(yDraw, xDraw) * curLevel->listVisual[c].specularMult);
					if (curLevel->calcBlockShinyMultToggle())
						GBufferCubePass.sendUniform(uLocationShinyMult, curLevel->calcBlockShinyMult(yDraw, xDraw) * curLevel->listVisual[c].shinyMult);
					if (curLevel->calcBlockEmissiveMultToggle())
						GBufferCubePass.sendUniform(uLocationEmissiveMult, curLevel->calcBlockEmissiveMult(yDraw, xDraw) * curLevel->listVisual[c].emissiveMult);

					glDrawArrays(GL_TRIANGLES, 0, objectSelection->model->GetNumVertices());
				}
			}
		}
		
		glBindVertexArray(0);
				
		Texture::unbind(7);
		Texture::unbind(6);
		Texture::unbind(5);
		Texture::unbind(4);
		Texture::unbind(3);
		Texture::unbind(2);
		Texture::unbind(1);
		Texture::unbind(0);
	}












	
	for (unsigned c = 0; c < curLevel->listBackgroundVisual.size() * SatDebugDrawBackgroundActive; ++c)
	{
		curLevel->listBackgroundVisual[c].object.diffuse->bind(0);

		bindEmit(curLevel->listBackgroundVisual[c].object.emissive);
		bindNormal(curLevel->listBackgroundVisual[c].object.normal);
		bindShiny(curLevel->listBackgroundVisual[c].object.specular);
		bindCubeMap(&AmbientCubeMapTexture);



		glActiveTexture(GL_TEXTURE7);
		glBindTexture(GL_TEXTURE_2D, BlockHeight.TexObj);

		glBindVertexArray(curLevel->listBackgroundVisual[c].object.model->VAO);

		glPolygonMode(GL_FRONT_AND_BACK, curLevel->listBackgroundVisual[c].object.primitivePolygonMode);
		glLineWidth(curLevel->listBackgroundVisual[c].object.primitiveLineWidth);
		//glEnable(GL_LINE_SMOOTH);
		glCullFace(curLevel->listBackgroundVisual[c].object.primitiveCullFace);

		GBufferCubePass.bind();
		GBufferCubePass.sendUniformMat4(uPassLocationModel, satMat4::Identity().Float(), tru); //We don't have to transpose (true)
		GBufferCubePass.sendUniformMat4(uPassLocationProj, activeCamera->GetUniformProjection(), tru);
		GBufferCubePass.sendUniform("uDiffuseTex", 0);
		GBufferCubePass.sendUniform("uNormalTex", 1);
		GBufferCubePass.sendUniform("uShinyTex", 2);
		GBufferCubePass.sendUniform("uSpecularTex", 2);
		GBufferCubePass.sendUniform("uEmissiveTex", 3);
		GBufferCubePass.sendUniform("uCubeMap", 6);
		GBufferCubePass.sendUniform("uParallaxTex", 7);	//
		GBufferCubePass.sendUniform("uParallaxActive", true);
		GBufferCubePass.sendUniform("uReflectAmount", curLevel->listBackgroundVisual[c].environmentAmount * EnviromentMapActive);

		GBufferCubePass.sendUniform(uLocationAmbientAdd,	curLevel->listBackgroundVisual[c].ambientAdd);
		GBufferCubePass.sendUniform(uLocationDiffuseAdd,	curLevel->listBackgroundVisual[c].diffuseAdd);
		GBufferCubePass.sendUniform(uLocationSpecularAdd,	curLevel->listBackgroundVisual[c].specularAdd);
		GBufferCubePass.sendUniform(uLocationShinyAdd,		curLevel->listBackgroundVisual[c].shinyAdd);
		GBufferCubePass.sendUniform(uLocationEmissiveAdd,	curLevel->listBackgroundVisual[c].emissiveAdd);
		GBufferCubePass.sendUniform(uLocationAmbientMult,	curLevel->listBackgroundVisual[c].ambientMult);
		GBufferCubePass.sendUniform(uLocationDiffuseMult,	curLevel->listBackgroundVisual[c].diffuseMult);
		GBufferCubePass.sendUniform(uLocationSpecularMult,	curLevel->listBackgroundVisual[c].specularMult);
		GBufferCubePass.sendUniform(uLocationShinyMult,		curLevel->listBackgroundVisual[c].shinyMult);
		GBufferCubePass.sendUniform(uLocationEmissiveMult,	curLevel->listBackgroundVisual[c].emissiveMult);

		GBuffer.bind();
		
		//std::cout << curLevel->listBackgroundVisual[c].appearLoop.x << std::endl;
		//std::cout << curLevel->listBackgroundVisual[c].appearLoop.y << std::endl;

		//for (int i = adjust.y; 
		//	i < Min(RENDER_HEIGHT + (int)adjust.y, curLevel->Y); 
		//	i += curLevel->listBackgroundVisual[c].appearLoop.y)
		//{
		//	for (int j = Max(adjust.x - RENDER_WIDTH, -20.0f); 
		//		j < Min(RENDER_WIDTH*2.0f + (int)adjust.x, (int)curLevel->X + 20.0f); 
		//		j += curLevel->listBackgroundVisual[c].appearLoop.x)
		//	{

		satVec2 spacing = curLevel->listBackgroundVisual[c].appearLoop;

		for (int i = 0; 
			i < 40 / spacing.y; 
			i += 1)
		{
			for (int j = 0; 
				j < 100 / spacing.x + 2; 
				j += 1)
			{
				satVec2 adjustInt;
				satVec2 adjustFract;
				adjustInt = modf(adjust.x, &adjustFract.x);

				satVec2 adjustMod = adjust.xy();
				int jMult = 0;
				int iMult = 0;
				while(adjustMod.x > spacing.x)
				{
					adjustMod.x = adjustMod.x - spacing.x;
					jMult++;
				}

				posX = (j+jMult)*spacing.x - adjust.x;
				posY = (i+iMult)*spacing.y - adjust.y;
				
				//posX = adjust.x * 0.1;
				//posY = i - adjust.y;

				zTranslate = 0.0f;

				tempMatrix.LoadIdentity();
				//tempMatrix.Scale(curLevel->listBackgroundVisual[c].transformScale);
				tempMatrix.Translate(initialTranslation + curLevel->listBackgroundVisual[c].transform + satVec3(
					-(posX * 2) + 20,
					(posY * 2) - 14,
					zTranslate));

				satVec3 rotation;
				rotation = curLevel->listBackgroundVisual[c].rotate + curLevel->listBackgroundVisual[c].speedRotate * TimeInLevel;
				rotation += Timer::sinRange(0.0f, TimeInLevel * curLevel->listBackgroundVisual[c].rangeSpeedRotate, 0.0f, curLevel->listBackgroundVisual[c].rangeRotate);

				tempMatrix.RotateX(rotation.x);
				tempMatrix.RotateY(rotation.y);
				tempMatrix.RotateZ(rotation.z);

				GBufferCubePass.sendUniform("uScale", satVec4(curLevel->listBackgroundVisual[c].transformScale, 1.0f));
				GBufferCubePass.sendUniformMat4(uPassLocationView, tempMatrix.Inverse().Float(), tru);
				
				glDrawArrays(GL_TRIANGLES, 0, curLevel->listBackgroundVisual[c].object.model->GetNumVertices());				
			}
		}

		GBufferCubePass.sendUniform("uScale", satVec4(1.0f, 1.0f));

		glBindVertexArray(0);
		
		Texture::unbind(7);
		Texture::unbind(6);
		Texture::unbind(5);
		Texture::unbind(4);
		Texture::unbind(3);
		Texture::unbind(2);
		Texture::unbind(1);
		Texture::unbind(0);


	}






	srand(randomSave);

	FrameBuffer::unbind();
	ShaderProgram::unbind();
}

#pragma region bindStuff

void Game::bindNormal()
{
	DefaultNormal.bind(1);
}

void Game::bindNormal(Texture* tex)
{
	tex->bind(1);
}

void Game::bindShiny()
{
	DefaultShiny.bind(2);
}

void Game::bindShiny(Texture* tex)
{	
	tex->bind(2);
}

void Game::bindEmit()
{
	DefaultEmissive.bind(3);
}

void Game::bindEmit(Texture* tex)
{	
	tex->bind(3);
}

void Game::bindSecondaryTexture()
{
	DefaultShiny.bind(5);
}

void Game::bindSecondaryTexture(Texture* tex)
{
	tex->bind(5);
}


void Game::bindCubeMap()
{
	//DefaultCubeMap->bind(6);
}

void Game::bindCubeMap(Texture* tex)
{
	tex->bindCube(6);
}

#pragma endregion bindStuff

void Game::ReshapeWindow(const bool &force)
{
	ReshapeWindow(0, 0, force);
}

void Game::ReshapeWindow(const int &width, const int &height, const bool &force)
{
	int NEW_WINDOW_WIDTH =	width; 
	int NEW_WINDOW_HEIGHT = height;
	
	//Make sure the pixel doubling is even and lines up
	//if(UpScaleResolutionX)
	//{
	//	NEW_WINDOW_WIDTH = NEW_WINDOW_WIDTH + NEW_WINDOW_WIDTH % 2;
	//}
	//
	//if(UpScaleResolutionY)
	//{
	//	NEW_WINDOW_HEIGHT = NEW_WINDOW_HEIGHT + NEW_WINDOW_HEIGHT % 2;
	//}

	if (tru || FullScreenActive == true)
	{
		//glfwGetFramebufferSize(window, &NEW_WINDOW_WIDTH, &NEW_WINDOW_HEIGHT);
		
		sf::Vector2u newWindowSize = sfWindow->getSize();
		NEW_WINDOW_WIDTH = newWindowSize.x;
		NEW_WINDOW_HEIGHT = newWindowSize.y;


		//Make sure the pixel doubling is even and lines up
		if(UpScaleResolutionX || UpScaleResolutionY)
		{
			if(UpScaleResolutionX)
			{
				NEW_WINDOW_WIDTH = NEW_WINDOW_WIDTH + NEW_WINDOW_WIDTH % 2;
			}
			if(UpScaleResolutionY)
			{
				NEW_WINDOW_HEIGHT = NEW_WINDOW_HEIGHT + NEW_WINDOW_HEIGHT % 2;
			}
			//glfwSetWindowSize(window, NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
			sfWindow->setSize(sf::Vector2u(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT));
		}

		//
	
		if (NEW_WINDOW_WIDTH != FULLSCREEN_WIDTH || NEW_WINDOW_HEIGHT != FULLSCREEN_HEIGHT)
		{
			setFullscreenSize(&NEW_WINDOW_WIDTH, &NEW_WINDOW_HEIGHT);
		}

	//if (NEW_WINDOW_WIDTH != WINDOW_WIDTH || NEW_WINDOW_HEIGHT != WINDOW_HEIGHT)
	//{
	//	std::cout << NEW_WINDOW_WIDTH << " " << WINDOW_WIDTH  << "\t" << NEW_WINDOW_HEIGHT << " " << WINDOW_HEIGHT << std::endl;
	//}

		if (NEW_WINDOW_WIDTH != WINDOW_WIDTH || NEW_WINDOW_HEIGHT != WINDOW_HEIGHT || force)
		{
			if (NEW_WINDOW_WIDTH < 320)
			{
				NEW_WINDOW_WIDTH = 320;
			}

			if (NEW_WINDOW_HEIGHT < 240)
			{
				NEW_WINDOW_HEIGHT = 240;
			}

			if ((float)NEW_WINDOW_WIDTH / (float)NEW_WINDOW_HEIGHT > 2.0f)
			{
				NEW_WINDOW_WIDTH = NEW_WINDOW_HEIGHT * 2.0f;
			}
			else if ((float)NEW_WINDOW_WIDTH / (float)NEW_WINDOW_HEIGHT < 1.2f)
			{
				NEW_WINDOW_WIDTH = NEW_WINDOW_HEIGHT * 1.2f;
			}

			if(SatDebugForce16x9)
			{
				NEW_WINDOW_WIDTH = NEW_WINDOW_HEIGHT * 16.0f / 9.0f;
			}

			//glutReshapeWindow(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
			//glfwSetWindowSize(window, NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT);
			sfWindow->setSize(sf::Vector2u(NEW_WINDOW_WIDTH, NEW_WINDOW_HEIGHT));

			WINDOW_WIDTH = NEW_WINDOW_WIDTH;
			WINDOW_HEIGHT = NEW_WINDOW_HEIGHT;

			setUpInterlaceTexture();

			GBuffer.Reset();
			GBuffer.InitDepthTexture(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
			GBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGBA8, GL_NEAREST, GL_CLAMP_TO_EDGE); //Flat colour
			GBuffer.InitColorTexture((unsigned int)1, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Normals (xyz) Does not require 4th channel
			GBuffer.InitColorTexture((unsigned int)2, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Add On Colours
			//GBuffer.InitColorTexture((unsigned int)3, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Ambient
			GBuffer.InitColorTexture((unsigned int)3, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Specular
			GBuffer.InitColorTexture((unsigned int)4, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_R16F, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Specular Shiny
			GBuffer.InitColorTexture((unsigned int)5, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Emissive
			//GBuffer.InitColorTexture((unsigned int)7, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_NEAREST, GL_CLAMP_TO_EDGE);  //Emissive
			
			//for(unsigned int c = 8; c < 256; ++c)
			//{
			//	GBuffer.InitColorTexture((unsigned int)c, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //Emissive
			//	std::cout << c << std::endl;
			//	checkFBO(&GBuffer);
			//}
			checkFBO(&GBuffer);

			//BackgroundBuffer.Reset();
			//BackgroundBuffer.InitDepthTexture(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
			//BackgroundBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGBA8, GL_LINEAR, GL_CLAMP_TO_EDGE); //Flat colour
			//checkFBO(&BackgroundBuffer);

			DeferredComposite.Reset();
			DeferredComposite.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_LINEAR, GL_MIRRORED_REPEAT);  //View Space Positions (xyz)
			checkFBO(&DeferredComposite);

			FinalComposite.Reset();
			FinalComposite.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB16F, GL_LINEAR, GL_MIRRORED_REPEAT);  //View Space Positions (xyz)
			checkFBO(&FinalComposite);

			LeftEyeBuffer.Reset();
			LeftEyeBuffer.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_MIRRORED_REPEAT);
			checkFBO(&LeftEyeBuffer);

			PreviousFrame.Reset();
			PreviousFrame.InitColorTexture((unsigned int)0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //View Space Positions (xyz)
			//PreviousFrame.InitColorTexture((unsigned int)1, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY, GL_RGB32F, GL_LINEAR, GL_CLAMP_TO_EDGE);  //View Space Positions (xyz)
			checkFBO(&PreviousFrame);

			FullScreenOutput.Reset();
			FullScreenOutput.InitColorTexture((unsigned int)0, WINDOW_WIDTH, WINDOW_HEIGHT, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&FullScreenOutput);

			EdgeMap.Reset();
			EdgeMap.InitColorTexture((unsigned int)0, (WINDOW_WIDTH >> UpScaleSobelResolution) >> UpScaleResolutionX, (WINDOW_HEIGHT >> UpScaleSobelResolution)  >> UpScaleResolutionY, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);  //View Space Positions (xyz)
			checkFBO(&EdgeMap);

			ShadowMap.Reset();
			ShadowMap.InitDepthTexture(SHADOW_RESOLUTION >> UpScaleShadowResolution, SHADOW_RESOLUTION >> UpScaleShadowResolution, GL_LINEAR);
			checkFBO(&ShadowMap);
			//if (!GBuffer.CheckFBO())
			//{
			//	std::cout << "FBO failed to init!\n";
			//	system("Pause");
			//	exit(0);
			//}

			////WorkBuffer1.Unload();
			//WorkBuffer1.Reset();
			//WorkBuffer1.InitColorTexture((unsigned int)0, (WINDOW_WIDTH  >> UpScaleResolutionX) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT  >> UpScaleResolutionY) / BLOOM_DOWNSCALE, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			//checkFBO(&WorkBuffer1);
			//
			////WorkBuffer2.Unload();
			//WorkBuffer2.Reset();
			//WorkBuffer2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH  >> UpScaleResolutionX) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT  >> UpScaleResolutionY) / BLOOM_DOWNSCALE, GL_RGB8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			//checkFBO(&WorkBuffer2);
			
			//SSAOWorkBuffer1.Reset();
			//SSAOWorkBuffer1.InitColorTexture((unsigned int)0, (WINDOW_WIDTH  >> UpScaleResolutionX) / SSAO_DOWNSCALE, (WINDOW_HEIGHT  >> UpScaleResolutionY) / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			//checkFBO(&SSAOWorkBuffer1);
			//
			//SSAOWorkBuffer2.Reset();
			//SSAOWorkBuffer2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH  >> UpScaleResolutionX) / SSAO_DOWNSCALE, (WINDOW_HEIGHT  >> UpScaleResolutionY) / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			//checkFBO(&SSAOWorkBuffer2);
			
			//WorkBuffer1.Unload();
			WorkBuffer1_2.Reset();
			WorkBuffer1_2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer1_2);

			//WorkBuffer2.Unload();
			WorkBuffer2_2.Reset();
			WorkBuffer2_2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer2_2);

			WorkBuffer1_4.Reset();
			WorkBuffer1_4.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer1_4);
			WorkBuffer2_4.Reset();
			WorkBuffer2_4.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer2_4);
			WorkBuffer1_8.Reset();
			WorkBuffer1_8.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer1_8);
			WorkBuffer2_8.Reset();
			WorkBuffer2_8.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4, GL_RGB16F, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&WorkBuffer2_8);

			SSAOWorkBuffer1.Reset();
			SSAOWorkBuffer1.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / SSAO_DOWNSCALE, (WINDOW_HEIGHT) / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&SSAOWorkBuffer1);

			SSAOWorkBuffer2.Reset();
			SSAOWorkBuffer2.InitColorTexture((unsigned int)0, (WINDOW_WIDTH) / SSAO_DOWNSCALE, (WINDOW_HEIGHT) / SSAO_DOWNSCALE, GL_R8, GL_LINEAR, GL_CLAMP_TO_EDGE);
			checkFBO(&SSAOWorkBuffer2);

			calcCamera();
		}
	}
}

void Game::computeShadowMap()
{
	if(SatDebugShadowMapActive)
	{
		glCullFace(GL_FRONT);

		satMat4 tempMatrix;
		//tempMatrix.LoadIdentity();
		//tempMatrix.Translate(-TotalTranslation + satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
		//tempMatrix.RotateX(-90.0f);	
		
		//drawShadowModel(&Background, &BackgroundTexture[0], tempMatrix);
		//drawShadowModel(&Ship, &ShipTexture[Between(ShipHealth, 0, 3)]);
		
		
		drawAnimationShadow();
		drawBlocksShadow();
		
		glCullFace(GL_FRONT);
		
		//drawShadowModel(&Ship, &ShipTexture[Between(ShipHealth, 0, 3)]);
		
		for (unsigned i = 0; i < bulletGood.size(); ++i)
		{
			//tempMatrix = Quaternions;
			tempMatrix.LoadIdentity();
			//tempMatrix.Translate(initialTranslation.x - TotalTranslation.x, 0.0f, -TotalTranslation.z);
			tempMatrix.TranslateInstance(bulletGood[i].position + bulletGood[i].displacement);
			drawShadowModel(bulletGood[i].mesh, bulletGood[i].texture, tempMatrix);
		}
		
		for (unsigned i = 0; i < smokeParticle.size(); ++i)
		{
			//tempMatrix = Quaternions;
			tempMatrix.LoadIdentity();
			//tempMatrix.Translate(initialTranslation.x - TotalTranslation.x, 0.0f, -TotalTranslation.z);
			tempMatrix.TranslateInstance(smokeParticle[i].position);
			drawShadowModel(smokeParticle[i].mesh, smokeParticle[i].texture, tempMatrix);
		}
		
		if(LevelManager::levelCurrent == 0)
		{

		}	
		//std::cout << shipLight.ambient <<std::endl ;

		glCullFace(GL_BACK);
	}
}

void Game::drawShadowModel(Mesh* model, Texture* tex, satMat4 &transformation)
{
	bindShadowMap(transformation);
	//StaticGeometry.bind();
	//StaticGeometry.sendUniformMat4("uModel", satMat4::Identity().Float(), true);
	//StaticGeometry.sendUniformMat4("uView", (transformation * ShadowTransform).Inverse().Float(), true);
	//StaticGeometry.sendUniformMat4("uProj", ShadowProjection.Float(), true);
	ShadowMap.bind();
	
	//tex.bind();
	glBindVertexArray(model->VAO);
	glDrawArrays(GL_TRIANGLES, 0, model->GetNumVertices());
	glBindVertexArray(0);
	//tex.unbind();
	
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
	FrameBuffer::unbind();
}

void Game::bindShadowMap(satMat4 &transformation)
{
	PassThroughShadowShader.bind();
	PassThroughShadowShader.sendUniformMat4("uModel", satMat4::Identity().Float(), tru);
	PassThroughShadowShader.sendUniformMat4("uView", (transformation * ShadowTransform).Inverse().Float(), tru);
	PassThroughShadowShader.sendUniformMat4("uProj", ShadowProjection.Float(), tru);
}

void Game::computeRender()
{
	satMat4 tempMatrix;
	tempMatrix.LoadIdentity();
	
	//tempMatrix.RotateX(-90.0f);
	float t = (adjust.x * 8.0f - (380 * 8.0f)) / 50.0f;
	float tempLerp = Lerp(0.0f, 1.0f, t);

	//backgroundLight.ambient = Lerp(1.0f, 1.0f, t);
	//shipLight.ambient = Lerp(0.5f, 0.5f, t);
	//blockLight.ambient = Lerp(0.65f, 0.35f, t);
	
	//drawModelSecondary(&sat::light::mod::background, &Background, &BackgroundTexture[0], &DefaultNormal, &DefaultSpecular, &DefaultEmissive, &BackgroundTexture[1], tempLerp, tempMatrix);
	//drawModel(&sat::light::mod::background, &Background, &BackgroundTexture[0], &DefaultTestNormal, &DefaultSpecular, &DefaultEmissive, &BackgroundTexture[1], tempLerp, tempMatrix);
	unsigned int shipNum = Between(ShipHealth, 0, 3);
	//drawModel(&shipLight, &Ship, &ShipTexture[shipNum], &ShipNormalTexture[shipNum], &ShipSpecularTexture, &ShipEmissiveTexture[shipNum], &ShipTexture[0], 0.0f, Quaternions);
	drawAnimation();
	
	

	for (unsigned i = 0; i < bulletGood.size(); ++i)
	{
		tempMatrix = Quaternions;
		tempMatrix.Translate(bulletGood[i].position + bulletGood[i].displacement);

		LightAddMult bulletAddMultLight = sat::light::mod::bulletGood;
		bulletAddMultLight.emissiveMult = powf((bulletGood[i].lifetime / bulletGood[i].totalLifetime), 0.5f);
		bulletAddMultLight.diffuseMult = powf((bulletGood[i].lifetime / bulletGood[i].totalLifetime), 0.5f);
		
		drawModelCube(&bulletAddMultLight, bulletGood[i].mesh, bulletGood[i].texture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.25f, tempMatrix);
	}
	
	for (unsigned i = 0; i < smokeParticle.size(); ++i)
	{
		tempMatrix = Quaternions;
		tempMatrix.Translate(smokeParticle[i].position);
		drawModel(&Lerp(sat::light::mod::smokeShipYellow, sat::light::mod::smokeShipRed, 1.0f - (smokeParticle[i].lifetime / smokeParticle[i].totalLifetime)), smokeParticle[i].mesh, smokeParticle[i].texture, &DefaultTestNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
	}
	
	if ((LevelManager::levelCurrent == 0 || LevelManager::levelCurrent == 1) && LevelManager::tutorialActive)
	{
		
		static float frameNumber = 0.0f;
		glActiveTexture(GL_TEXTURE0);
			
		
		//GLuint tempU = ReturnAVIFrame((int)frameNumber);
		//GLuint tempU; 
		
		if(activeCamera == &leftCamera || !ThreeDeeActive)
		{
			if (LevelManager::levelCurrent == 0)
			{
				if(adjust.x < 30.0)
				{
					aviHandle = ReturnAVIFrame(tutorialFrameNumber);			
				}
				elf(adjust.x < 85.0)
				{
					aviHandle = ReturnAVIFrame(tutorialFrameNumber+150);
				}
				elf(adjust.x < 130.0)
				{			
					aviHandle = ReturnAVIFrame(tutorialFrameNumber+75);
				}
				elf(adjust.x < 250.0)
				{
					aviHandle = ReturnAVIFrame(tutorialFrameNumber+225);
				}
				
				if(adjust.x < 250.0f)
				{
					aviTexture.TexObj = aviHandle;
					
					if(!PauseActive)
					{
						tutorialFrameNumber += Timer::framef() * FRAMES_PER_SECOND * 0.5f;
						//++tutorialFrameNumber;
					}
					
					//int lastframe=AVIStreamLength(pavi);							// The Last Frame Of The Stream
					int lastframe = ReturnAVILength();
					if (tutorialFrameNumber >= 75.0f)										// Are We At Or Past The Last Frame?
					{
						tutorialFrameNumber = 0.0f;												// Reset The Frame Back To Zero (Start Of Video)
					}
				}
			}

			if(LevelManager::levelCurrent == 1)
			{
				if(adjust.x < 140.0f && adjust.x > 70.0f)
				{
					aviHandle = ReturnAVIFrame(tutorialFrameNumber+225);			
				}
				
				if(adjust.x < 140.0f && adjust.x > 70.0f)
				{
					aviTexture.TexObj = aviHandle;
					
					if(!PauseActive)
					{
						tutorialFrameNumber += Timer::framef() * FRAMES_PER_SECOND * 0.5f;
						//++tutorialFrameNumber;
					}
					
					//int lastframe=AVIStreamLength(pavi);							// The Last Frame Of The Stream
					int lastframe = ReturnAVILength();
					if (tutorialFrameNumber >= 75.0f)										// Are We At Or Past The Last Frame?
					{
						tutorialFrameNumber = 0.0f;												// Reset The Frame Back To Zero (Start Of Video)
					}
				}
			}
		
		}
		else if(activeCamera == &rightCamera)
		{
			aviTexture.TexObj = aviHandle;
		}

		if(LevelManager::levelCurrent == 0)
		{
			if(adjust.x < 30.0)
			{
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 25.0f, 7.0f, 1.9f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
			}
			elf(adjust.x < 85.0)
			{
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 120.0f, 12.0f, 1.2f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialWideMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
				
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 120.0f, 5.5f, 1.2f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialWideMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
				
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 120.0f, -1.0f, 1.2f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialWideMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
			
				//tempMatrix.LoadIdentity();
				//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 123.5f, 12.0f, 1.9f));
				//drawModelCube(&sat::light::mod::tutorial, &TutorialTallMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
				//
				//tempMatrix.LoadIdentity();
				//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 136.5f, 12.0f, 1.9f));
				//drawModelCube(&sat::light::mod::tutorial, &TutorialTallMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
			
			}
			elf(adjust.x < 130.0)
			{
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 205.0f, 8.0f, 1.9f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialLessTallMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
			}
		}
		if(LevelManager::levelCurrent == 1)
		{
			if(adjust.x < 140.0f && adjust.x > 70.0f)
			{
				//tempMatrix.LoadIdentity();
				//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 185.0f, 11.0f, 1.9f));
				//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
			
				tempMatrix.LoadIdentity();
				tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 207.0f, 6.0f, 1.9f));
				drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
			}
		}
		
		if(activeCamera == &rightCamera || !ThreeDeeActive)
		{
			//VideoUnload(aviHandle);
		}
		//tempMatrix.LoadIdentity();
		//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 20.0f, -2.0f, 1.9f));
		//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
		

		//tempU = ReturnAVIFrame(tutorialFrameNumber + 75);
		//tempTexture.TexObj = tempU;



		//tempMatrix.LoadIdentity();
		//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 60.0f, 6.0f, 1.9f));
		//drawModelCube(&sat::light::mod::tutorial, &TutorialLessTallMesh, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
		
		
		
		
		//tempU = ReturnAVIFrame(tutorialFrameNumber + 150);
		//tempTexture.TexObj = tempU;
		
		
		
		//tempMatrix.LoadIdentity();
		//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 380.0f, 6.0f, 1.9f));
		//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
		//
		//
		//tempMatrix.LoadIdentity();
		//tempMatrix.Translate(initialTranslation + satVec3(adjust.x*2.0f - 380.0f, 6.0f, 1.9f));
		//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
		
		
		
		//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &TutorialTexture[2], &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.1f, tempMatrix);
		
		//std::cout << shipLight.ambient <<std::endl ;
	}

	//if(LevelManager::levelCurrent == 1)
	//{
	//	tempMatrix.TranslateInstance(initialTranslation + satVec3(adjust.x*0.250f - 400, -2.5f, 6.0f));
	//}
	//else
	//{
	//	tempMatrix.TranslateInstance(initialTranslation + satVec3(adjust.x*0.250f - 400, -2.5f, 6.0f));
	//}

	
	drawBlocks();

	if(LevelManager::levelCurrent != 0 && true)
	{
		tempMatrix.TranslateInstance(initialTranslation + satVec3(adjust.x*0.250f - 400, -2.5f, 8.0f));

		float backgroundReflect = curLevel->calcEnvironmentBackground();
		
		if(backgroundReflect >= 0.0)
		{
			drawModelCube(&sat::light::mod::background, &Background, &BackgroundTexture[0], &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, backgroundReflect, tempMatrix);
		}
		else
		{
			drawModel(&sat::light::mod::background, &Background, &BackgroundTexture[0], &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
		}
	}
	
}

void Game::drawAnimation()
{
	satMat4 shipRotation;
	shipRotation.LoadIdentity();
	

	
	if(!TitleScreenActive)
	{
		if (collisionTime > 0.0f && ShipHealth > 0)
		{
			satMat4 rotateMatrix;
			rotateMatrix.TranslateInstance(shipTranslate);
			shipRotation.RotateZ(Timer::sinRange(10.0f, -10.0f, 10.0f) * InverseLerp(collisionTime, 0.0f, invincibleTime));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = rotateMatrix * shipRotation;
		}

		satMat4 translateMatrix;
		if(adjust.x > curLevel->WinPoint - 35.0f)
		{
			satMat4 rotateMatrix;
			translateMatrix.TranslateInstance(satVec3(-40.0f, 0.0f, -30.0f) * powf(1.0f - IntroEndParam, 3.0f));
			//shipRotation = rotateMatrix * shipRotation;
			rotateMatrix.TranslateInstance(shipTranslate);
			rotateMatrix.RotateX(Lerp(0.0f, 12.0f * 360.0f, powf(1.0f - IntroEndParam, 2.0f)));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = rotateMatrix * shipRotation;				
		}

		if (adjust.x < 26.0f)
		{
			satMat4 rotateMatrix;
			translateMatrix.TranslateInstance(satVec3(20.0f, 0.0f, -15.0f) * powf(1.0f - IntroEndParam, 3.0f));
			//shipRotation = rotateMatrix * shipRotation;
			rotateMatrix.TranslateInstance(shipTranslate);
			rotateMatrix.RotateX(Lerp(12.0f * 360.0f, 0.0f, powf(1.0f - IntroEndParam, 2.0f)));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = rotateMatrix * shipRotation;
		}
		
		

		if (timeDead > 0.0f && timeDead < timeDeadReverse)
		{
			satMat4 rotateMatrix;
			//rotateMatrix.LoadIdentity();
			rotateMatrix.TranslateInstance(shipTranslate);
			float lerpParam = InverseLerp(timeDead, 0.0f, timeDeadReverse);
			//rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, InverseLerp(timeDead, 0.0f, timeDeadReverse)));
			rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, powf(lerpParam, 3.0f)));
			rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, powf(lerpParam, 2.0f)));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = shipRotation * rotateMatrix;
		}

		if (timeDead >= timeDeadReverse)
		{
			satMat4 rotateMatrix;
			//rotateMatrix.LoadIdentity();
			rotateMatrix.TranslateInstance(shipTranslate);
			float lerpParam = InverseLerp(timeDead, timeDeadReverse, timeDeadMax);
			//rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, powf(lerpParam, 3.0f)));
			rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, 1.0f - powf(1.0f - lerpParam, 5.0f)));
			rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, 1.0f - powf(1.0f - lerpParam, 4.0f)));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = shipRotation * rotateMatrix;
		}


		if (collisionTime > 0.0f && ShipHealth > 0)
		{
			satMat4 rotateMatrix;
			rotateMatrix.TranslateInstance(shipTranslate);
			float lerpParam = InverseLerp(collisionTime, 0.0f, invincibleTime);
			//rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, powf(lerpParam, 3.0f)));
			//rotateMatrix.RotateX(Lerp(0.0f, 5.0f * 360.0f, powf(lerpParam, 3.0f)));
			rotateMatrix.RotateY(Lerp(0.0f, -6.0f * 360.0f, powf(lerpParam, 4.0f)));
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = shipRotation * rotateMatrix;
		}


		if (SatDebugShipWaggleToggleActive)
		{
			satMat4 rotateMatrix;
			rotateMatrix.TranslateInstance(shipTranslate);
			rotateMatrix.RotateX(Timer::sinRange(0.0f, TimeInLevel * curLevel->SongTempo / 60.0f * satMath::PI, -10.0f, 10.0f) * powf(IntroEndParam, 2.0f) * curLevel->SongBounce);
			rotateMatrix.RotateX(-10.0f);
			rotateMatrix.Translate(-shipTranslate);
			shipRotation = rotateMatrix * shipRotation;
		}

		//if(verticalCollide == false)
		{
			satMat4 rotateMatrix;
			rotateMatrix.TranslateInstance(shipTranslateOriginBack);
			rotateMatrix.RotateZ(Between(averageInput.y, -15.0f, 15.0f));
			
			//rotateMatrix.Translate(-shipTranslateOriginBack+satVec3(-13.0, 0.0, -20.0f));
			rotateMatrix.Translate(-shipTranslateOriginBack);
			//rotateMatrix.RotateY(30.0f);
			//rotateMatrix.RotateZ(10.0f);

			shipRotation = shipRotation * rotateMatrix;
		}

		shipRotation = shipRotation * translateMatrix;
	}
	else
	{
		static float titleScreenLoop = 2.0f;
		satMat4 TranslationRotation;
		
		static float rotate = 35;
		//satMat4 rotateMatrix;

		if(titleScreenLoop > 1.0f)
		{
			
			//rotate = (int)(Timer::currentf() * 720.0f) % 360;
			rotate = (rand() % 3600) / 10.0f;
			titleScreenLoop = 0.0f;
			
			//rotateMatrix.LoadIdentity();
			//rotateMatrix.RotateZ(180.0f);
		}
		
		
		//satMat4 rotateMatrix;
		TranslationRotation.LoadIdentity();
		//TranslationRotation.TranslateInstance(0.0f, 0.0f, 10.0f);
		TranslationRotation.RotateZ(rotate);
		TranslationRotation.Translate(Lerp(25.0f, -25.0f, titleScreenLoop), 0.0f, 0.0f);
		
		TranslationRotation.Translate(shipTranslate);
		TranslationRotation.RotateX(Timer::currentf() * 720.0f * 0.5f + 180.0f);	
		TranslationRotation.Translate(-shipTranslate);	
		//TranslationRotation = rotateMatrix * TranslationRotation;
		shipRotation = TranslationRotation * shipRotation;
		
		TranslationRotation.TranslateInstance(-16.0f, 0.0f, -10.0f);
		shipRotation = shipRotation * TranslationRotation;
		//printf("Lerp Parameter %f\n", Lerp(-20.0f, 20.0f, titleScreenLoop));
		
		//rotateMatrix.TranslateInstance(satVec3(20.0f, 0.0f, 0.0f));
		//shipRotation = rotateMatrix * shipRotation;
		//rotateMatrix.TranslateInstance(shipTranslate);
		//rotateMatrix.RotateX(Lerp(12.0f * 360.0f, 0.0f, 0.0f));
		//rotateMatrix.Translate(-shipTranslate);
		//shipRotation = rotateMatrix * shipRotation;

		titleScreenLoop += 0.5f * Timer::framef() / (ThreeDeeActive + 1);
	}


	//shipRotation = shipRotation * Quaternions;

	//float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	GBufferCubePass.bind();
	//AnimateModel.sendUniform("uInterpParam", ShipShrink);
	GBufferCubePass.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePass.sendUniformMat4("uView", (shipRotation * Quaternions).Inverse().Float(), tru);
	GBufferCubePass.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);	
	GBufferCubePass.sendUniform("uDiffuseTex", 0);
	GBufferCubePass.sendUniform("uNormalTex", 1);
	GBufferCubePass.sendUniform("uShinyTex", 2);
	GBufferCubePass.sendUniform("uEmissiveTex", 3);
	GBufferCubePass.sendUniform("uCubeMap", 6);//uReflectAmount
	float environmentAmount = Min(NoDamageTime * 0.5f, 1.0f);
	GBufferCubePass.sendUniform("uReflectAmount", environmentAmount + 0.0f * EnviromentMapActive);
	
	if(!TitleScreenActive)
	{
		GBufferCubePass.sendUniform("uAmbientAdd",		sat::light::mod::playerShip.ambientAdd);
		GBufferCubePass.sendUniform("uDiffuseAdd",		sat::light::mod::playerShip.diffuseAdd);
		GBufferCubePass.sendUniform("uSpecularAdd",		sat::light::mod::playerShip.specularAdd);
		GBufferCubePass.sendUniform("uShinyAdd",		sat::light::mod::playerShip.shinyAdd);
		GBufferCubePass.sendUniform("uEmissiveAdd",		sat::light::mod::playerShip.emissiveAdd);
		GBufferCubePass.sendUniform("uAmbientMult",		sat::light::mod::playerShip.ambientMult);
		GBufferCubePass.sendUniform("uDiffuseMult",		sat::light::mod::playerShip.diffuseMult);
		GBufferCubePass.sendUniform("uSpecularMult",	sat::light::mod::playerShip.specularMult);
		GBufferCubePass.sendUniform("uShinyMult",		sat::light::mod::playerShip.shinyMult);
		GBufferCubePass.sendUniform("uEmissiveMult",	sat::light::mod::playerShip.emissiveMult);	
	}
	else
	{
		GBufferCubePass.sendUniform("uAmbientAdd",		sat::light::mod::playerShip.ambientAdd + 0.25f);
		GBufferCubePass.sendUniform("uDiffuseAdd",		sat::light::mod::playerShip.diffuseAdd);
		GBufferCubePass.sendUniform("uSpecularAdd",		sat::light::mod::playerShip.specularAdd);
		GBufferCubePass.sendUniform("uShinyAdd",		sat::light::mod::playerShip.shinyAdd);
		GBufferCubePass.sendUniform("uEmissiveAdd",		sat::light::mod::playerShip.emissiveAdd);
		GBufferCubePass.sendUniform("uAmbientMult",		sat::light::mod::playerShip.ambientMult);
		GBufferCubePass.sendUniform("uDiffuseMult",		sat::light::mod::playerShip.diffuseMult * 2.0f);
		GBufferCubePass.sendUniform("uSpecularMult",	sat::light::mod::playerShip.specularMult);
		GBufferCubePass.sendUniform("uShinyMult",		sat::light::mod::playerShip.shinyMult);
		GBufferCubePass.sendUniform("uEmissiveMult",	sat::light::mod::playerShip.emissiveMult);	
	}
	


	GBuffer.bind();

	//glActiveTexture(GL_TEXTURE1);
	//drawModel(&shipLight, &Ship, &ShipTexture[0], &ShipNormalTexture[shipNum], &ShipShinyTexture, &ShipEmissiveTexture[shipNum], &ShipTexture[0], 0.0f, Quaternions);

	unsigned int shipNum = Between(ShipHealth, 0, 3);

	//glActiveTexture(GL_TEXTURE7);
	//glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(&ShipNormalTexture[shipNum]);
	bindShiny(&ShipShinyTexture[shipNum]);
	if (SatDebugGodModeActive || NoDamageTime > 0.0f)
	{
		if (SatDebugGodModeActive || NoDamageTime > 2.0f)
		{
			bindEmit(&ShipInvincibleTexture[shipNum]);
		}
		else if (rand() % 75 + 25 < NoDamageTime * 50.0f)
		{
			bindEmit(&ShipInvincibleTexture[shipNum]);
		}
		else
		{			
			bindEmit(&DefaultEmissive);
		}

		if(NoDamageTime < 2.0f && NoDamageTime > 1.0f &&!ChargeDownSound.isPlaying())
		{
			ChargeDownSound.playSound(1.0f, 1.0f, 1.0f);
		}
	}
	else
	{
		bindEmit(&ShipEmissiveTexture[shipNum]);
	}
	//bindShiny(&DefaultTestShiny);
	//bindEmit(&DefaultTestEmissive);
	//bindSecondaryTexture(&ShipTexture[shipNum]);
	bindCubeMap(&AmbientCubeMapTexture);


	ShipTexture[shipNum].bind(0);
	glBindVertexArray(Ship.VAO);
	glDrawArrays(GL_TRIANGLES, 0, Ship.GetNumVertices());
	glBindVertexArray(0);
	ShipTexture[shipNum].unbind();


	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);
	
	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawAnimationShadow()
{
	satMat4 shipRotation;
	shipRotation.LoadIdentity();




	

	if (collisionTime > 0.0f && ShipHealth > 0)
	{
		satMat4 rotateMatrix;
		rotateMatrix.TranslateInstance(shipTranslate);
		shipRotation.RotateZ(Timer::sinRange(10.0f, -10.0f, 10.0f) * InverseLerp(collisionTime, 0.0f, invincibleTime));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = rotateMatrix * shipRotation;
	}
	
	

	satMat4 translateMatrix;
	if(adjust.x > curLevel->WinPoint - 35.0f)
	{
		satMat4 rotateMatrix;
		translateMatrix.TranslateInstance(satVec3(-40.0f, 0.0f, -30.0f) * powf(1.0f - IntroEndParam, 3.0f));
		//shipRotation = rotateMatrix * shipRotation;
		rotateMatrix.TranslateInstance(shipTranslate);
		rotateMatrix.RotateX(Lerp(0.0f, 12.0f * 360.0f, powf(1.0f - IntroEndParam, 2.0f)));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = rotateMatrix * shipRotation;				
	}

	if (adjust.x < 26.0f)
	{
		satMat4 rotateMatrix;
		translateMatrix.TranslateInstance(satVec3(20.0f, 0.0f, -15.0f) * powf(1.0f - IntroEndParam, 3.0f));
		//shipRotation = rotateMatrix * shipRotation;
		rotateMatrix.TranslateInstance(shipTranslate);
		rotateMatrix.RotateX(Lerp(12.0f * 360.0f, 0.0f, powf(1.0f - IntroEndParam, 2.0f)));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = rotateMatrix * shipRotation;
	}
	
	

	if (timeDead > 0.0f && timeDead < timeDeadReverse)
	{
		satMat4 rotateMatrix;
		//rotateMatrix.LoadIdentity();
		rotateMatrix.TranslateInstance(shipTranslate);
		float lerpParam = InverseLerp(timeDead, 0.0f, timeDeadReverse);
		//rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, InverseLerp(timeDead, 0.0f, timeDeadReverse)));
		rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, powf(lerpParam, 3.0f)));
		rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, powf(lerpParam, 2.0f)));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = shipRotation * rotateMatrix;
	}

	if (timeDead >= timeDeadReverse)
	{
		satMat4 rotateMatrix;
		//rotateMatrix.LoadIdentity();
		rotateMatrix.TranslateInstance(shipTranslate);
		float lerpParam = InverseLerp(timeDead, timeDeadReverse, timeDeadMax);
		//rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, powf(lerpParam, 3.0f)));
		rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, 1.0f - powf(1.0f - lerpParam, 5.0f)));
		rotateMatrix.RotateX(Lerp(0.0f, -4.0f * 360.0f, 1.0f - powf(1.0f - lerpParam, 4.0f)));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = shipRotation * rotateMatrix;
	}


	if (collisionTime > 0.0f && ShipHealth > 0)
	{
		satMat4 rotateMatrix;
		rotateMatrix.TranslateInstance(shipTranslate);
		float lerpParam = InverseLerp(collisionTime, 0.0f, invincibleTime);
		//rotateMatrix.RotateZ(Lerp(0.0f, -2.0f * 360.0f, powf(lerpParam, 3.0f)));
		//rotateMatrix.RotateX(Lerp(0.0f, 5.0f * 360.0f, powf(lerpParam, 3.0f)));
		rotateMatrix.RotateY(Lerp(0.0f, -6.0f * 360.0f, powf(lerpParam, 4.0f)));
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = shipRotation * rotateMatrix;
	}


	if (SatDebugShipWaggleToggleActive)
	{
		satMat4 rotateMatrix;
		rotateMatrix.TranslateInstance(shipTranslate);
		rotateMatrix.RotateX(Timer::sinRange(0.0f, TimeInLevel * curLevel->SongTempo / 60.0f * satMath::PI, -10.0f, 10.0f) * powf(IntroEndParam, 2.0f) * curLevel->SongBounce);
		rotateMatrix.RotateX(-10.0f);
		rotateMatrix.Translate(-shipTranslate);
		shipRotation = rotateMatrix * shipRotation;
	}

	//if(verticalCollide == false)
	{
		satMat4 rotateMatrix;
		rotateMatrix.TranslateInstance(shipTranslateOriginBack);
		rotateMatrix.RotateZ(Between(averageInput.y, -15.0f, 15.0f));
		rotateMatrix.Translate(-shipTranslateOriginBack);
		shipRotation = shipRotation * rotateMatrix;
	}

	shipRotation = shipRotation * translateMatrix;

	PassThroughShadowShader.bind();
	PassThroughShadowShader.sendUniformMat4("uModel", satMat4::Identity().Float(), tru);
	PassThroughShadowShader.sendUniformMat4("uView", (shipRotation * ShadowTransform).Inverse().Float(), tru);
	PassThroughShadowShader.sendUniformMat4("uProj", ShadowProjection.Float(), tru);

	ShadowMap.bind();

	//tex.bind();
	glBindVertexArray(Ship.VAO);
	glDrawArrays(GL_TRIANGLES, 0, Ship.GetNumVertices());
	glBindVertexArray(0);
	//tex.unbind();

	PassThroughShadowShader.unbind(); //Unbind the shader so we don't accidentally overwrite data.
	ShadowMap.unbind();

}

void Game::bindRender(satMat4 &transformation)
{
	float tempLerp = Lerp(0.0f, 1.0f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	bindRender(tempLerp, transformation);
}

void Game::bindRender(float lerpTime, satMat4 &transformation)
{
	GBufferCubePass.bind();
	GBufferCubePass.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePass.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	GBufferCubePass.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);
	GBufferCubePass.sendUniform("uDiffuseTex", 0);
	GBufferCubePass.sendUniform("uNormalTex", 1);
	GBufferCubePass.sendUniform("uShinyTex", 2);
	GBufferCubePass.sendUniform("uEmissiveTex", 3);
	GBufferCubePass.sendUniform("uReflectAmount", 0.0f);
	GBufferCubePass.sendUniform("uParallaxActive", false);
	GBufferCubePass.sendUniform("uAmbientAdd", sat::light::mod::Default.ambientAdd);
	GBufferCubePass.sendUniform("uDiffuseAdd", sat::light::mod::Default.diffuseAdd);
	GBufferCubePass.sendUniform("uSpecularAdd", sat::light::mod::Default.specularAdd);
	GBufferCubePass.sendUniform("uShinyAdd", sat::light::mod::Default.shinyAdd);
	GBufferCubePass.sendUniform("uEmissiveAdd", sat::light::mod::Default.emissiveAdd);
	GBufferCubePass.sendUniform("uAmbientMult", sat::light::mod::Default.ambientMult);
	GBufferCubePass.sendUniform("uDiffuseMult", sat::light::mod::Default.diffuseMult);
	GBufferCubePass.sendUniform("uSpecularMult", sat::light::mod::Default.specularMult);
	GBufferCubePass.sendUniform("uShinyMult", sat::light::mod::Default.shinyMult);
	GBufferCubePass.sendUniform("uEmissiveMult", sat::light::mod::Default.emissiveMult);
}

void Game::bindRender(const satVec3* light, satMat4 &transformation)
{
	float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	bindRender(light, tempLerp, transformation);
}

void Game::bindRender(const satVec3* light, float lerpTime, satMat4 &transformation)
{
	GBufferCubePass.bind();
	GBufferCubePass.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePass.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	GBufferCubePass.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);
	GBufferCubePass.sendUniform("uDiffuseTex", 0);
	GBufferCubePass.sendUniform("uNormalTex", 1);
	GBufferCubePass.sendUniform("uShinyTex", 2);
	GBufferCubePass.sendUniform("uEmissiveTex", 3);
	GBufferCubePass.sendUniform("uReflectAmount", 0.0f);
	GBufferCubePass.sendUniform("uParallaxActive", false);
	GBufferCubePass.sendUniform("uAmbientAdd", sat::light::mod::Default.ambientAdd);
	GBufferCubePass.sendUniform("uDiffuseAdd", sat::light::mod::Default.diffuseAdd);
	GBufferCubePass.sendUniform("uSpecularAdd", sat::light::mod::Default.specularAdd);
	GBufferCubePass.sendUniform("uShinyAdd", sat::light::mod::Default.shinyAdd);
	GBufferCubePass.sendUniform("uEmissiveAdd", sat::light::mod::Default.emissiveAdd);
	GBufferCubePass.sendUniform("uAmbientMult", sat::light::mod::Default.ambientMult);
	GBufferCubePass.sendUniform("uDiffuseMult", sat::light::mod::Default.diffuseMult);
	GBufferCubePass.sendUniform("uSpecularMult", sat::light::mod::Default.specularMult);
	GBufferCubePass.sendUniform("uShinyMult", sat::light::mod::Default.shinyMult);
	GBufferCubePass.sendUniform("uEmissiveMult", sat::light::mod::Default.emissiveMult);	
}

void Game::bindRender(const LightAddMult* light, satMat4 &transformation)
{
	float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	bindRender(light, tempLerp, transformation);
}

void Game::bindRender(const LightAddMult* light, float lerpTime, satMat4 &transformation)
{
	GBufferCubePass.bind();
	GBufferCubePass.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePass.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	GBufferCubePass.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);
	GBufferCubePass.sendUniform("uDiffuseTex", 0);
	GBufferCubePass.sendUniform("uNormalTex", 1);
	GBufferCubePass.sendUniform("uShinyTex", 2);
	GBufferCubePass.sendUniform("uEmissiveTex", 3);
	GBufferCubePass.sendUniform("uReflectAmount", 0.0f);
	GBufferCubePass.sendUniform("uParallaxActive", false);
	GBufferCubePass.sendUniform("uAmbientAdd", light->ambientAdd);
	GBufferCubePass.sendUniform("uDiffuseAdd", light->diffuseAdd);
	GBufferCubePass.sendUniform("uSpecularAdd", light->specularAdd);
	GBufferCubePass.sendUniform("uShinyAdd", light->shinyAdd);
	GBufferCubePass.sendUniform("uEmissiveAdd", light->emissiveAdd);
	GBufferCubePass.sendUniform("uAmbientMult", light->ambientMult);
	GBufferCubePass.sendUniform("uDiffuseMult", light->diffuseMult);
	GBufferCubePass.sendUniform("uSpecularMult", light->specularMult);
	GBufferCubePass.sendUniform("uShinyMult", light->shinyMult);
	GBufferCubePass.sendUniform("uEmissiveMult", light->emissiveMult);
	
}

void Game::bindRenderCube(const LightAddMult* light, float uReflectAmount, satMat4 &transformation)
{
	GBufferCubePass.bind();
	GBufferCubePass.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePass.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	GBufferCubePass.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);
	GBufferCubePass.sendUniform("uDiffuseTex", 0);
	GBufferCubePass.sendUniform("uNormalTex", 1);
	GBufferCubePass.sendUniform("uShinyTex", 2);
	GBufferCubePass.sendUniform("uEmissiveTex", 3);
	GBufferCubePass.sendUniform("uCubeMap", 6);
	GBufferCubePass.sendUniform("uParallaxTex", 7);
	GBufferCubePass.sendUniform("uParallaxActive", false);
	//GBufferCubePass.sendUniform("uParallaxScale", 10.0f);
	GBufferCubePass.sendUniform("uReflectAmount", uReflectAmount * EnviromentMapActive);
	//GBufferCubePass.sendUniform("uParallaxActive", false);
	GBufferCubePass.sendUniform("uAmbientAdd", light->ambientAdd);
	GBufferCubePass.sendUniform("uDiffuseAdd", light->diffuseAdd);
	GBufferCubePass.sendUniform("uSpecularAdd", light->specularAdd);
	GBufferCubePass.sendUniform("uShinyAdd", light->shinyAdd);
	GBufferCubePass.sendUniform("uEmissiveAdd", light->emissiveAdd);
	GBufferCubePass.sendUniform("uAmbientMult", light->ambientMult);
	GBufferCubePass.sendUniform("uDiffuseMult", light->diffuseMult);
	GBufferCubePass.sendUniform("uSpecularMult", light->specularMult);
	GBufferCubePass.sendUniform("uShinyMult", light->shinyMult);
	GBufferCubePass.sendUniform("uEmissiveMult", light->emissiveMult);
}

void Game::drawModel(Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation)
{
	drawModel(model, tex, normalTex, shinyTex, emissiveTex, transformation);
}

void Game::drawModelSecondary(Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* tex2, float lerpTime, satMat4 &transformation)
{
	//float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	bindRender(transformation);

	GBuffer.bind();

	//glActiveTexture(GL_TEXTURE1);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	bindSecondaryTexture(tex2);


	tex->bind(0);
	model->Bind();
	model->Draw();
	model->Unbind();
	tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	GBuffer.unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawModel(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation)
{
	//float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	//bindRender(light, lerpTime, transformation);

	bindRender(addmult, transformation);

	GBuffer.bind();
	
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	glActiveTexture(GL_TEXTURE0);

	tex->bind();
	model->Bind();
	model->Draw();
	model->Unbind();
	tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawModelSecondary(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* tex2, float lerpTime, satMat4 &transformation)
{
	bindRender(addmult, lerpTime, transformation);

	GBuffer.bind();

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	bindSecondaryTexture(tex2);
	glActiveTexture(GL_TEXTURE0);
	
	tex->bind();
	model->Bind();
	model->Draw();
	model->Unbind();
	tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawModelCube(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* cubeTex, float uReflectAmount, satMat4 &transformation)
{
	//float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	//bindRender(light, lerpTime, transformation);

	bindRenderCube(addmult, uReflectAmount, transformation);

	GBuffer.bind();

	//glActiveTexture(GL_TEXTURE1);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	bindCubeMap(cubeTex);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, DefaultTestHeight.TexObj);
	glActiveTexture(GL_TEXTURE0);

	tex->bind();
	glBindVertexArray(model->VAO);
	glDrawArrays(GL_TRIANGLES, 0, model->GetNumVertices());
	glBindVertexArray(0);
	tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawModelParticle(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, float alphaAmount, satMat4 &transformation)	
{
	//float tempLerp = Lerp(0.0f, 0.75f, (adjust.x * 8.0f - 50.0f) / 50.0f);
	//bindRender(light, lerpTime, transformation);

	ParticlesShader.bind();
	ParticlesShader.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	ParticlesShader.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	ParticlesShader.sendUniformMat4("uProj", CameraProjection.Float(), tru);
	ParticlesShader.sendUniform("uDiffuseTex", 0);
	ParticlesShader.sendUniform("uNormalTex", 1);
	ParticlesShader.sendUniform("uSpecularTex", 2);
	ParticlesShader.sendUniform("uEmissiveTex", 3);
	ParticlesShader.sendUniform("uCubeMap", 6);
	ParticlesShader.sendUniform("uParallaxTex", 7);
	ParticlesShader.sendUniform("uParallaxActive", false);
	ParticlesShader.sendUniform("uAlpha", alphaAmount);
	ParticlesShader.sendUniform("uAmbientAdd", addmult->ambientAdd);
	ParticlesShader.sendUniform("uDiffuseAdd", addmult->diffuseAdd);
	ParticlesShader.sendUniform("uSpecularAdd", addmult->specularAdd);
	ParticlesShader.sendUniform("uShinyAdd", addmult->shinyAdd);
	ParticlesShader.sendUniform("uEmissiveAdd", addmult->emissiveAdd);
	ParticlesShader.sendUniform("uAmbientMult", addmult->ambientMult);
	ParticlesShader.sendUniform("uDiffuseMult", addmult->diffuseMult);
	ParticlesShader.sendUniform("uSpecularMult", addmult->specularMult);
	ParticlesShader.sendUniform("uShinyMult", addmult->shinyMult);
	ParticlesShader.sendUniform("uEmissiveMult", addmult->emissiveMult);

	FinalComposite.bind();

	//glActiveTexture(GL_TEXTURE1);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, DefaultTestHeight.TexObj);
	glActiveTexture(GL_TEXTURE0);

	tex->bind();
	glBindVertexArray(model->VAO);
	glDrawArrays(wireframeToggle, 0, model->GetNumVertices());
	glBindVertexArray(0);
	tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}

void Game::drawModelAlpha(const LightAddMult* addmult, Mesh* model, Texture* alpha, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation)
{
	GBufferCubePassAlphaDiscard.bind();
	GBufferCubePassAlphaDiscard.sendUniformMat4("uModel", satMat4::Identity().Float(), tru); //We don't have to transpose (true)
	GBufferCubePassAlphaDiscard.sendUniformMat4("uView", transformation.Inverse().Float(), tru);
	GBufferCubePassAlphaDiscard.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);
	GBufferCubePassAlphaDiscard.sendUniform("uDiffuseTex", 0);
	GBufferCubePassAlphaDiscard.sendUniform("uNormalTex", 1);
	GBufferCubePassAlphaDiscard.sendUniform("uShinyTex", 2);
	GBufferCubePassAlphaDiscard.sendUniform("uEmissiveTex", 3);
	GBufferCubePassAlphaDiscard.sendUniform("uAlphaTex", 4);
	GBufferCubePassAlphaDiscard.sendUniform("uAmbientAdd", addmult->ambientAdd);
	GBufferCubePassAlphaDiscard.sendUniform("uDiffuseAdd", addmult->diffuseAdd);
	GBufferCubePassAlphaDiscard.sendUniform("uSpecularAdd", addmult->specularAdd);
	GBufferCubePassAlphaDiscard.sendUniform("uShinyAdd", addmult->shinyAdd);
	GBufferCubePassAlphaDiscard.sendUniform("uEmissiveAdd", addmult->emissiveAdd);
	GBufferCubePassAlphaDiscard.sendUniform("uAmbientMult", addmult->ambientMult);
	GBufferCubePassAlphaDiscard.sendUniform("uDiffuseMult", addmult->diffuseMult);
	GBufferCubePassAlphaDiscard.sendUniform("uSpecularMult", addmult->specularMult);
	GBufferCubePassAlphaDiscard.sendUniform("uShinyMult", addmult->shinyMult);
	GBufferCubePassAlphaDiscard.sendUniform("uEmissiveMult", addmult->emissiveMult);

	GBuffer.bind();

	//glActiveTexture(GL_TEXTURE1);

	bindNormal(normalTex);
	bindShiny(shinyTex);
	bindEmit(emissiveTex);
	alpha->bind(4);

	tex->bind(0);
	glBindVertexArray(model->VAO);
	glDrawArrays(GL_TRIANGLES, 0, model->GetNumVertices());
	glBindVertexArray(0);
	tex->unbind();

	//tex->bind(0);
	//glBindVertexArray(model->VAO);
	//glDrawArrays(wireframeToggle, 0, model->GetNumVertices());
	//glBindVertexArray(0);
	//tex->unbind();

	Texture::unbind(7);
	Texture::unbind(6);
	Texture::unbind(5);
	Texture::unbind(4);
	Texture::unbind(3);
	Texture::unbind(2);
	Texture::unbind(1);
	Texture::unbind(0);

	FrameBuffer::unbind();
	ShaderProgram::unbind(); //Unbind the shader so we don't accidentally overwrite data.
}


void Game::toonShading()
{
	satVec2 thresholdNormalDepth = curLevel->calcSobelThreshold();

	if(curLevel->SobelActive)
	{
		if (postProcessSobelActive && (!curLevel->SobelThickToggle || !postProcessSobelThickActive))
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glViewport(0, 0, 
				(WINDOW_WIDTH  >> UpScaleSobelResolution) >> UpScaleResolutionX, 
				(WINDOW_HEIGHT >> UpScaleSobelResolution) >> UpScaleResolutionY);

			SobelPass.bind();
			SobelPass.sendUniform("uNormalMap", 0);
			SobelPass.sendUniform("uDepthMap", 1);
			SobelPass.sendUniform("uPixelSize", satVec2(
				1.0f / ((WINDOW_WIDTH  >> UpScaleSobelResolution) >> UpScaleResolutionX), 
				1.0f / ((WINDOW_HEIGHT >> UpScaleSobelResolution) >> UpScaleResolutionY)));

			SobelPass.sendUniform("edgeThresholdNormal", thresholdNormalDepth.x);
			SobelPass.sendUniform("edgeThresholdDepth",  thresholdNormalDepth.y);

			//if (OrthographicActive)
			//{
			//	SobelPass.sendUniform("edgeThresholdNormal", 6.0f);
			//	SobelPass.sendUniform("edgeThresholdDepth", 0.14f);
			//}
			//else
			//{
			//	SobelPass.sendUniform("edgeThresholdNormal", 6.0f);
			//	SobelPass.sendUniform("edgeThresholdDepth", 0.14f);
			//}

			EdgeMap.bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1)); //Normal component of the screen not including normal texture
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
			FrameBuffer::DrawFullScreenQuad();
			Texture::unbind(1);
			Texture::unbind(0);

			FrameBuffer::unbind();
			ShaderProgram::unbind();
		}
		else
		{
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glViewport(0, 0, 
				(WINDOW_WIDTH  >> UpScaleSobelResolution) >> UpScaleResolutionX, 
				(WINDOW_HEIGHT >> UpScaleSobelResolution) >> UpScaleResolutionY);

			SobelThickPass.bind();
			SobelThickPass.sendUniform("uNormalMap", 0);
			SobelThickPass.sendUniform("uDepthMap", 1);
			SobelThickPass.sendUniform("uPixelSize", satVec2(
				1.0f / ((WINDOW_WIDTH  >> UpScaleSobelResolution) >> UpScaleResolutionX), 
				1.0f / ((WINDOW_HEIGHT >> UpScaleSobelResolution) >> UpScaleResolutionY)));
			
			SobelThickPass.sendUniform("edgeThresholdNormal", thresholdNormalDepth.x);
			SobelThickPass.sendUniform("edgeThresholdDepth",  thresholdNormalDepth.y);

			//if (OrthographicActive)
			//{
			//	SobelThickPass.sendUniform("edgeThresholdNormal", 2.0f*128.15f);
			//	SobelThickPass.sendUniform("edgeThresholdDepth", 0.34f);
			//}
			//else
			//{
			//	SobelThickPass.sendUniform("edgeThresholdNormal", 2.0f*128.15f);
			//	SobelThickPass.sendUniform("edgeThresholdDepth", 0.34f);
			//}

			EdgeMap.bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1)); //Normal component of the screen not including normal texture
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
			FrameBuffer::DrawFullScreenQuad();
			Texture::unbind(1);
			Texture::unbind(0);

			FrameBuffer::unbind();
			ShaderProgram::unbind();
		}
	}
}

void Game::buildSSAO()
{
	if (numOfSSAOSamples > 128)
	{
		numOfSSAOSamples = 128;
	}
				

	if (!setUpSSAOKernel)
	{
		ssaoKernel.clear();
		setUpSSAOKernel = tru;

		//for (GLuint i = 0; i < numOfSSAOSamples; ++i)
		//{
		//	satVec3 sample(
		//		randomFloats(generator) * 2.0 - 1.0,
		//		randomFloats(generator) * 2.0 - 1.0,
		//		randomFloats(generator) * 0.9 + 0.1);
		//
		//
		//	sample.Normalize();
		//	sample *= randomFloats(generator);
		//	GLfloat scale = GLfloat(i) / (float)numOfSSAOSamples;
		//
		//	scale = Lerp(0.1f, 1.0f, scale * scale);
		//	sample *= scale;
		//	ssaoKernel.push_back(sample);
		//
		//	//std::cout << ssaoKernel[i];
		//	//std::cout << "\n";
		//}

		for (GLuint i = 0; i < numOfSSAOSamples; ++i)
		{
			satVec3 sample(
				randomFloats(generator) * 2.0 - 1.0,
				randomFloats(generator) * 2.0 - 1.0,
				randomFloats(generator) * 0.95 + 0.05);


			sample.Normalize();
			//sample *= randomFloats(generator);
			GLfloat scale = GLfloat(i) / (float)numOfSSAOSamples;

			scale = Lerp(0.25f, 1.0f, scale * scale);
			sample *= scale;
			ssaoKernel.push_back(sample);

			//std::cout << ssaoKernel[i];
			//std::cout << "\n";
		}
	}
}


void Game::deferredRender()
{
#pragma region SSAO

	if (curLevel->SSAOActive && postProcessSSAOActive || SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SSAO)
	{
		//http://www.learnopengl.com/#!Advanced-Lighting/SSAO

		glClearColor(1.0, 1.0, 1.0, 0);
		SSAOWorkBuffer1.Clear();
		SSAOWorkBuffer2.Clear();

		glViewport((unsigned int)0, (unsigned int)0, (WINDOW_WIDTH) / SSAO_DOWNSCALE, (WINDOW_HEIGHT) / SSAO_DOWNSCALE);



		buildSSAO();

		//static 
		//if (!setUpSSAOKernel)
		//{
		//	setUpSSAOKernel = tru;
		//	ssaoKernel.resize(0);
		//
		//	for (GLuint i = 0; i < 8; ++i)
		//	{
		//		satVec3 sample(
		//			randomFloats(generator) * 2.0 - 1.0,
		//			randomFloats(generator) * 2.0 - 1.0,
		//			randomFloats(generator));
		//
		//
		//		sample.Normalize();
		//		sample *= randomFloats(generator);
		//		GLfloat scale = GLfloat(i) / 8.0;
		//
		//		scale = Lerp(0.1f, 1.0f, scale * scale);
		//		sample *= scale;
		//		ssaoKernel.push_back(sample);
		//
		//		//std::cout << ssaoKernel[i];
		//		//std::cout << "\n";
		//	}
		//}



		SSAOShader.bind();
		//SSAOShader.sendUniform("uPositionTex", 0);
		SSAOShader.sendUniform("uNormalTex", 1);
		SSAOShader.sendUniform("uTexNoise", 2);
		SSAOShader.sendUniform("uDepthMap", 3);

		SSAOShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY));
		
		SSAOShader.sendUniform("uRadius", 0.9f * Timer::sinRange(5.0f, 1.0f, 1.0f));
		SSAOShader.sendUniform("kernelSize", numOfSSAOSamples);

		for (GLuint i = 0; i < numOfSSAOSamples; ++i)
			SSAOShader.sendUniform(("samples[" + std::to_string(i) + "]").c_str(), ssaoKernel[i]);

		SSAOShader.sendUniformMat4("uProj", activeCamera->GetUniformProjection(), tru);

		SSAOShader.sendUniform("uNear", NearPlane);
		SSAOShader.sendUniform("uFar", FarPlane);

		satMat4 ProjBiasMatrix = satMat4(
		2.0f, 0.0f, 0.0f, -1.0f,
		0.0f, 2.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 2.0f, -1.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

		ProjBiasMatrix = activeCamera->GetProjectionInverse() * ProjBiasMatrix;

		SSAOShader.sendUniformMat4("uProjBiasMatrixInverse", ProjBiasMatrix.Float(), tru);

		SSAOWorkBuffer1.bind();

		//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1));
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, noiseTexture);
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());

		SSAOWorkBuffer1.DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		//Compute Blur
		//glViewport(0, 0, WINDOW_WIDTH / SSAO_DOWNSCALE, WINDOW_HEIGHT / SSAO_DOWNSCALE);
		if (postProcessBlurSSAOActive)
		{
			for (int i = 0; i < SSAO_BLUR_PASSES; i++)
			{
				//Horizontal Blur
				SSAOBlurHorizontal.bind();
				SSAOBlurHorizontal.sendUniform("uTex", 0);
				SSAOBlurHorizontal.sendUniform("uPixelSize", 1.0f / (WINDOW_WIDTH >> UpScaleResolutionX));
				SSAOWorkBuffer2.bind();

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer1.GetColorHandle(0));

				FrameBuffer::DrawFullScreenQuad();

				glBindTexture(GL_TEXTURE_2D, GL_NONE);

				FrameBuffer::unbind();
				ShaderProgram::unbind();


				//Vertical Blur
				SSAOBlurVertical.bind();
				SSAOBlurVertical.sendUniform("uTex", 0);
				SSAOBlurVertical.sendUniform("uPixelSize", 1.0f / (WINDOW_HEIGHT >> UpScaleResolutionY));
				SSAOWorkBuffer1.bind();

				glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer2.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
				glBindTexture(GL_TEXTURE_2D, GL_NONE);

				FrameBuffer::unbind();
				ShaderProgram::unbind();
			}
		}

		// Composite to back buffer //

		glViewport(0, 0, WINDOW_WIDTH >> UpScaleResolutionX, WINDOW_HEIGHT >> UpScaleResolutionY);
		SSAOComposite.bind();
		SSAOComposite.sendUniform("uSceneTex", 0);
		SSAOComposite.sendUniform("uDiffuseTex", 1);

		FinalComposite.bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer1.GetColorHandle(0));
		//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
		FrameBuffer::DrawFullScreenQuad();
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

#pragma endregion SSAO
	
	
	
	
	
	
	
	
	
	//Create scene from GBuffer
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);

	satMat4 transformation;
	transformation.LoadIdentity();

	DeferredLighting.bind();
	//GBufferPass.sendUniformMat4("uModel", satMat4::Identity().Float(), true); //We don't have to transpose (true)
	//GBufferPass.sendUniformMat4("uView", transformation.Inverse().Float(), true);
	//GBufferPass.sendUniformMat4("uProj", CameraProjection.Float(), true);
	DeferredLighting.sendUniformMat4("ViewToShadowMap", ViewToShadowMap.Float(), tru);
	DeferredLighting.sendUniform("uShadowOrthoActive", curLevel->ShadowOrthographicActive);

	satMat4 ProjBiasMatrix = satMat4(
		2.0f, 0.0f, 0.0f, -1.0f,
		0.0f, 2.0f, 0.0f, -1.0f,
		0.0f, 0.0f, 2.0f, -1.0f,
		0.0f, 0.0f, 0.0f, 1.0f
		);

	ProjBiasMatrix = activeCamera->GetProjectionInverse() * ProjBiasMatrix;

	DeferredLighting.sendUniformMat4("uProjBiasMatrixInverse", ProjBiasMatrix.Float(), tru);

	DeferredLighting.sendUniform("uScene", 0);
	DeferredLighting.sendUniform("uNormalMap", 1);
	DeferredLighting.sendUniform("uPositionMap", 2);
	//DeferredLighting.sendUniform("uAmbientMap", 3);
	DeferredLighting.sendUniform("uSpecularMap", 3);
	DeferredLighting.sendUniform("uShinyMap", 4);
	DeferredLighting.sendUniform("uEmissiveMap", 5);
	//DeferredLighting.sendUniform("uNormalPolyMap", 6);
	//DeferredLighting.sendUniform("uCubeMap", 6);
	//DeferredLighting.sendUniform("uSSAOMap", 7);
	//uSSAOMap
	DeferredLighting.sendUniform("uShadowTex", 28);
	
	DeferredLighting.sendUniform("uDepthMap", 29);
	DeferredLighting.sendUniform("uEdgeMap", 30);
	DeferredLighting.sendUniform("uShadowMap", 31);
	//DeferredLighting.sendUniform("uPixelSize", satVec2(1.0f / WINDOW_WIDTH, 1.0f / WINDOW_HEIGHT));
	//DeferredLighting.sendUniform("uLightDirection", transformation.GetRotationMatrix() * ShadowTransform.Inverse().GetDirectionVector());
	
	if(curLevel->ShadowOrthographicActive)
	{
		DeferredLighting.sendUniform("uLightPerspPosition", satVec3::Normalize(transformation.GetRotationMatrix() * ShadowTransform.Inverse().GetDirectionVector()));
	
	}
	else
	{
		DeferredLighting.sendUniform("uLightPerspPosition", transformation.GetRotationMatrix() * ShadowTransform.xyz() - satVec3(0.0f, TotalTranslation.y, 0.0f));
	}

	//std::cout << ShadowTransform.xyz() << std::endl;


	curLevel = &LevelManager::level[LevelManager::levelCurrent];
	
	DeferredLighting.sendUniform("uLightAmbient", curLevel->calcAmbient());
	DeferredLighting.sendUniform("uLightDiffuse", curLevel->calcDiffuse());
	//std::cout << satVec3(curLevel->calcDiffuse().r, curLevel->calcDiffuse().g, curLevel->calcDiffuse().b) << std::endl;
	DeferredLighting.sendUniform("uLightSpecular", curLevel->calcSpecular()); 
	DeferredLighting.sendUniform("uRimLightIntensity", RimLightActive * curLevel->calcRim());
	
	DeferredLighting.sendUniform("uCameraPos", 1.0f * satVec3(shiftAverage.x, shiftAverage.y, 0.0f));
	

	//std::cout << curLevel->lMatrix[0][0] << std::endl;
	//std::cout << "";
	//DeferredLighting.sendUniform("uRimLightIntensity", LevelManager::level[LevelManager::levelCurrent].RimLight * RimLightActive);
	//DeferredLighting.sendUniform("uRimLightIntensity", RimLightActive * (curLevel->listLight[0].lightRim));
	//DeferredLighting.sendUniform("uRimLightIntensity", satVec3(0.35f, 0.05f, 0.05f) * RimLightActive);

	//std::cout << curLevel->RimLight << std::endl;
	//std::cout << LevelManager::level[LevelManager::levelCurrent].RimLight << std::endl;

	DeferredLighting.sendUniform("uRimLightSmoothStep", satVec3(0.8f));
	//DeferredLighting.sendUniform("uLightSpecularExponent", 200.0f);
		
	//DeferredLighting.sendUniform("uShadowSize", satVec2(1024.0f * 0.010f));
	// * satVec2(1.0f / (SHADOW_RESOLUTION >> UpScaleShadowResolution))

	
	DeferredLighting.sendUniform("uShadowSize", satVec2(1.0f / 16.0f));


	//DeferredLighting.sendUniform("uDiffuseMap", 0);
	//DeferredLighting.sendUniform("uNormalMap", 1);
	//DeferredLighting.sendUniform("uPositionMap", 2);
	//DeferredLighting.sendUniform("uScene", 3);
	//DeferredLighting.sendUniform("uSpecularMap", 4);
	//DeferredLighting.sendUniform("uShinyMap", 5);
	//DeferredLighting.sendUniform("uEdgeMap", 30);

	int numOfLights = 0;
	//for (GLuint i = 0; i < lightShipHealth.size(); ++i)
	//{
	//	PointLightShader.sendUniformPointLight(i, lightShipHealth[i]);
	//}
	//numOfLights = lightShipHealth.size();
	
	if(TotalTranslation.y < 4.0f-12.5f + curLevel->wrapPoint.x * 2.0f)
	{
		float shipLightDimWrapParam = 
			InverseLerp(TotalTranslation.y, 
			-12.5f + curLevel->wrapPoint.x * 2.0f, 
			4.0f-12.5f + curLevel->wrapPoint.x * 2.0f);
		
		for(int l = 0; l < lightShipHealth.size(); ++l)
			lightShipHealth[l].color *= shipLightDimWrapParam;
	}

	if(TotalTranslation.y > (curLevel->Y - curLevel->wrapPoint.y * 2.0f) * 2.0f - 12.5f-4.0f)
	{
		float shipLightDimWrapParam = 
			InverseLerp(TotalTranslation.y, 
			(curLevel->Y - curLevel->wrapPoint.y * 2.0f) * 2.0f - 12.5f, 
			(curLevel->Y - curLevel->wrapPoint.y * 2.0f) * 2.0f - 12.5f-4.0f);
		
		for(int l = 0; l < lightShipHealth.size(); ++l)
			lightShipHealth[l].color *= shipLightDimWrapParam;
	}


	DeferredLighting.sendUniformPointLight(0, lightShipHealth[0]);
	DeferredLighting.sendUniformPointLight(1, lightShipHealth[5]);
	DeferredLighting.sendUniformPointLight(2, lightShipHealth[6]);
	DeferredLighting.sendUniformPointLight(3, lightShipHealth[7]);


	numOfLights = 4;

	//satColor defaultColorOnShip = satColor(0.25f, 0.5f, 0.5f) * curLevel->calcShipLightMultiplier();
	satColor defaultColorOnShip = sat::color::lightcyan;
	if (ScoreMult > defaultScoreMult)
	{
		lightShipHealth[1].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + 0.0, defaultScoreMult + lightShipScoreMult * 1));

		DeferredLighting.sendUniformPointLight(4, lightShipHealth[1]);
		++numOfLights;
	}
	if (ScoreMult > defaultScoreMult + lightShipScoreMult * 1)
	{
		lightShipHealth[2].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 1, defaultScoreMult + lightShipScoreMult * 2));

		DeferredLighting.sendUniformPointLight(5, lightShipHealth[2]);
		++numOfLights;
	}
	if (ScoreMult > defaultScoreMult + lightShipScoreMult * 2)
	{
		lightShipHealth[3].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 2, defaultScoreMult + lightShipScoreMult * 3));

		DeferredLighting.sendUniformPointLight(6, lightShipHealth[3]);
		++numOfLights;
	}
	if (ScoreMult > defaultScoreMult + lightShipScoreMult * 3)
	{
		lightShipHealth[4].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 3, defaultScoreMult + lightShipScoreMult * 4));

		DeferredLighting.sendUniformPointLight(7, lightShipHealth[4]);
		++numOfLights;
	}

	if (ScoreMult > defaultScoreMult + lightShipScoreMult * 4) // || NoDamageTime > 0.0f)
	{
		for (GLuint i = 0; i < lightShipInvincible.size(); ++i)
		{
			DeferredLighting.sendUniformPointLight(i + numOfLights, lightShipInvincible[i]);
		}
		numOfLights += lightShipInvincible.size();
	}

	for (GLuint i = 0; i < lightEnemy.size(); ++i)
	{
		DeferredLighting.sendUniformPointLight(i + numOfLights, lightEnemy[i]);
	}
	numOfLights += lightEnemy.size();


	for (GLuint i = 0; i < lightBullet.size(); ++i)
	{
		DeferredLighting.sendUniformPointLight(i + numOfLights, lightBullet[i]);
	}
	numOfLights += lightBullet.size();


	for (GLuint i = 0; i < lightPoint.size(); ++i)
	{
		DeferredLighting.sendUniformPointLight(i + numOfLights, lightPoint[i]);
	}
	numOfLights += lightPoint.size();


	if (SatDebugDeferredLightActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		DeferredLighting.sendUniform("uNumLights", numOfLights);
	}
	else
	{
		DeferredLighting.sendUniform("uNumLights", 0);
	}










	DeferredComposite.bind();

	glActiveTexture(GL_TEXTURE0); //Not neccesary since it should already be at TEXTURE0, though it can't hurt to be sure (It probably does hurt)
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
	if (curLevel->SSAOActive && postProcessSSAOActive)
		glBindTexture(GL_TEXTURE_2D, FinalComposite.GetColorHandle(0));
	else
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1));
	glActiveTexture(GL_TEXTURE2);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(3));
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(4));
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer2.GetColorHandle(0));
	glActiveTexture(GL_TEXTURE27);
	glBindTexture(GL_TEXTURE_2D, ThreeDeeInterlaceTextureHandle);
	glActiveTexture(GL_TEXTURE28);
	if (SatDebugShadowMapActive)
		glBindTexture(GL_TEXTURE_2D, ShadowMapFlashlight.TexObj);
	else
		glBindTexture(GL_TEXTURE_2D, DefaultTexture.TexObj);
	glActiveTexture(GL_TEXTURE29);
	glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
	glActiveTexture(GL_TEXTURE30);
	glBindTexture(GL_TEXTURE_2D, EdgeMap.GetColorHandle(0));
	glActiveTexture(GL_TEXTURE31);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	FrameBuffer::DrawFullScreenQuad();

	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	//glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE30);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE29);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE28);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE27);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	
	//glActiveTexture(GL_TEXTURE6);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	
	glActiveTexture(GL_TEXTURE8);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);

	FrameBuffer::unbind();
	ShaderProgram::unbind();






	//////
	//
	//
	//PointLightShader.bind();
	//
	//
	//PointLightShader.sendUniform("uDiffuseMap", 0);
	//PointLightShader.sendUniform("uNormalMap", 1);
	//PointLightShader.sendUniform("uPositionMap", 2);
	//PointLightShader.sendUniform("uScene", 3);
	//PointLightShader.sendUniform("uSpecularMap", 4);
	//PointLightShader.sendUniform("uShinyMap", 5);
	//PointLightShader.sendUniform("uEdgeMap", 30);
	//
	//int numOfLights = 0;
	////for (GLuint i = 0; i < lightShipHealth.size(); ++i)
	////{
	////	PointLightShader.sendUniformPointLight(i, lightShipHealth[i]);
	////}
	////numOfLights = lightShipHealth.size();
	//
	//PointLightShader.sendUniformPointLight(0, lightShipHealth[0]);	
	//PointLightShader.sendUniformPointLight(1, lightShipHealth[5]);
	//PointLightShader.sendUniformPointLight(2, lightShipHealth[6]);
	//PointLightShader.sendUniformPointLight(3, lightShipHealth[7]);
	//
	//
	//numOfLights = 4;
	//
	////satColor defaultColorOnShip = satColor(0.25f, 0.5f, 0.5f) * curLevel->calcShipLightMultiplier();
	//satColor defaultColorOnShip = sat::color::lightcyan;
	//if (ScoreMult > defaultScoreMult)
	//{
	//	lightShipHealth[1].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + 0.0, defaultScoreMult + lightShipScoreMult * 1));
	//
	//	PointLightShader.sendUniformPointLight(4, lightShipHealth[1]);
	//	++numOfLights;
	//}
	//if (ScoreMult > defaultScoreMult + lightShipScoreMult * 1)
	//{
	//	lightShipHealth[2].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 1, defaultScoreMult + lightShipScoreMult * 2));
	//
	//	PointLightShader.sendUniformPointLight(5, lightShipHealth[2]);
	//	++numOfLights;
	//}
	//if (ScoreMult > defaultScoreMult + lightShipScoreMult * 2)
	//{
	//	lightShipHealth[3].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 2, defaultScoreMult + lightShipScoreMult * 3));
	//
	//	PointLightShader.sendUniformPointLight(6, lightShipHealth[3]);
	//	++numOfLights;
	//}
	//if (ScoreMult > defaultScoreMult + lightShipScoreMult * 3)
	//{
	//	lightShipHealth[4].color = Lerp(satColor(0.0f), defaultColorOnShip, InverseLerp(ScoreMult, defaultScoreMult + lightShipScoreMult * 3, defaultScoreMult + lightShipScoreMult * 4));
	//
	//	PointLightShader.sendUniformPointLight(7, lightShipHealth[4]);
	//	++numOfLights;
	//}
	//
	//if (ScoreMult > defaultScoreMult + lightShipScoreMult * 4) // || NoDamageTime > 0.0f)
	//{
	//	for (GLuint i = 0; i < lightShipInvincible.size(); ++i)
	//	{
	//		PointLightShader.sendUniformPointLight(i + numOfLights, lightShipInvincible[i]);
	//	}
	//	numOfLights += lightShipInvincible.size();
	//}
	//
	//for (GLuint i = 0; i < lightEnemy.size(); ++i)
	//{
	//	PointLightShader.sendUniformPointLight(i + numOfLights, lightEnemy[i]);
	//}
	//numOfLights += lightEnemy.size();
	//
	//
	//for (GLuint i = 0; i < lightBullet.size(); ++i)
	//{
	//	PointLightShader.sendUniformPointLight(i + numOfLights, lightBullet[i]);
	//}
	//numOfLights += lightBullet.size();
	//
	//
	//for (GLuint i = 0; i < lightPoint.size(); ++i)
	//{
	//	PointLightShader.sendUniformPointLight(i + numOfLights, lightPoint[i]);
	//}
	//numOfLights += lightPoint.size();
	//
	//
	//if (SatDebugDeferredLightActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	//{
	//	PointLightShader.sendUniform("uNumLights", numOfLights);
	//}
	//else
	//{
	//	PointLightShader.sendUniform("uNumLights", 0);
	//}
	//
	//
	//DeferredComposite.bind();
	//
	//glActiveTexture(GL_TEXTURE0); //Not neccesary since it should already be at TEXTURE0, though it can't hurt to be sure (It probably does hurt)
	////glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
	//if (curLevel->SSAOActive && postProcessSSAOActive)
	//	glBindTexture(GL_TEXTURE_2D, FinalComposite.GetColorHandle(0));
	//else
	//	glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(0));
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(1));
	//glActiveTexture(GL_TEXTURE2);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
	//glActiveTexture(GL_TEXTURE3);
	//glBindTexture(GL_TEXTURE_2D, DeferredComposite.GetColorHandle(0));
	//glActiveTexture(GL_TEXTURE4);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(3));
	//glActiveTexture(GL_TEXTURE5);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(4));
	//glActiveTexture(GL_TEXTURE6);
	//glBindTexture(GL_TEXTURE_2D, SSAOWorkBuffer2.GetColorHandle(0));
	////glActiveTexture(GL_TEXTURE6);
	//////AmbientCubeMapTexture.bind();
	////glBindTexture(GL_TEXTURE_CUBE_MAP, AmbientCubeMapTexture.TexObj);
	//glActiveTexture(GL_TEXTURE29);
	//glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
	//glActiveTexture(GL_TEXTURE30);
	//glBindTexture(GL_TEXTURE_2D, EdgeMap.GetColorHandle(0));
	//
	//FrameBuffer::DrawFullScreenQuad();
	//
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE29);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE6);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	////glActiveTexture(GL_TEXTURE6);
	////glBindTexture(GL_TEXTURE_CUBE_MAP, GL_NONE);
	//glActiveTexture(GL_TEXTURE5);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE4);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE3);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE2);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE1);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//
	//FrameBuffer::Unbind();
	//ShaderProgram::unbind();
	//
	//
}

FrameBuffer* Game::swapFrameBuffer(FrameBuffer * active)
{
	if (active == &DeferredComposite)
	{
		return (active = &FinalComposite);
	}
	else if (active == &FinalComposite)
	{
		return (active = &DeferredComposite);
	}
	else
	{
		//This shouldn't happen
		std::cout << "WHAT'S GOING ON HERE?" << std::endl;
		return active;
	}
}

// Post Processing //
void Game::postProcessing()
{
	FrameBuffer * ActiveFrameBuffer = &FinalComposite;
		
	//hard refresh for unbinding textures, just to be sure
	for(int i = 0; i < 32; ++i)
	{
		Texture::unbind(0 + i);
	}
	glActiveTexture(GL_TEXTURE0);
	
#pragma region sonarPost
	if(postProcessSonarActive && curLevel->SonarActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		SonarShader.bind();
		SonarShader.sendUniform("uSceneTex", 0);
		SonarShader.sendUniform("uDepthTex", 1);
		SonarShader.sendUniform("uEmissiveTex", 2);

		satVec4 colorAddMult = curLevel->calcSonarColor();
		colorAddMult.w *= TimeInLevel;
		SonarShader.sendUniform("uColorAdd", colorAddMult);
		SonarShader.sendUniform("uParameter", curLevel->calcSonarParam());
		//SonarShader.sendUniform("uTime", (float)TimeInLevel * 0.5f);

		satMat4 ProjBiasMatrix = satMat4(
			2.0f, 0.0f, 0.0f, -1.0f,
			0.0f, 2.0f, 0.0f, -1.0f,
			0.0f, 0.0f, 2.0f, -1.0f,
			0.0f, 0.0f, 0.0f, 1.0f);

		ProjBiasMatrix = activeCamera->GetProjectionInverse() * ProjBiasMatrix;

		SonarShader.sendUniformMat4("uProjBiasMatrixInverse", ProjBiasMatrix.Float(), tru);

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

		FrameBuffer::DrawFullScreenQuad();
		
		//glBindTexture(GL_TEXTURE_2D, GL_NONE);
		Texture::unbind(2);
		Texture::unbind(1);
		Texture::unbind(0);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion sonarPost

#pragma region EdgeMap	
if(curLevel->SobelActive) //postProcessSobelActive || postProcessSobelThickActive)
{
	EdgeMultPass.bind();
	EdgeMultPass.sendUniform("uScene", 0);
	EdgeMultPass.sendUniform("uEdgeMap", 1);
	ActiveFrameBuffer->bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));		
	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, EdgeMap.GetColorHandle(0));

	FrameBuffer::DrawFullScreenQuad();

	glBindTexture(GL_TEXTURE_2D, GL_NONE);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);

	FrameBuffer::unbind();
	ShaderProgram::unbind();

	ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
}
#pragma endregion EdgeMap

#pragma region fogPost
	if (curLevel->FogActive && postProcessFogActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive) //|| postProcessFogActive)
	{
		ShaderProgram *FogPostPtr = &FogShader[2]; 

		FogPostPtr = &FogShader[Between(curLevel->FogDetail, (unsigned)0, FogShader.size()-1)];
		if(curLevel->FogDetail < 10)
		{
			FogPostPtr->bind();
			FogPostPtr->sendUniform("uSceneTex", 0);		
			FogPostPtr->sendUniform("uDepthMap", 1);
			FogPostPtr->sendUniform("uEmissiveTex", 2);

			satMat4 ProjBiasMatrix = satMat4(
			2.0f, 0.0f, 0.0f, -1.0f,
			0.0f, 2.0f, 0.0f, -1.0f,
			0.0f, 0.0f, 2.0f, -1.0f,
			0.0f, 0.0f, 0.0f, 1.0f);

			ProjBiasMatrix = activeCamera->GetProjectionInverse() * ProjBiasMatrix;

			FogPostPtr->sendUniformMat4("uProjBiasMatrixInverse", ProjBiasMatrix.Float(), tru);

			FogPostPtr->sendUniform("uAspectRatio", (float)WINDOW_HEIGHT / WINDOW_WIDTH);

			FogPostPtr->sendUniform("uFogColor", curLevel->calcFogColor());
			FogPostPtr->sendUniform("uFogNear", curLevel->calcFogNear());
			FogPostPtr->sendUniform("uFogFar", curLevel->calcFogFar());
			
			FogPostPtr->sendUniform("uRange", curLevel->calcFogRange());
			
			FogPostPtr->sendUniform("uFogTopColor", curLevel->calcFogTopColor());
			FogPostPtr->sendUniform("uFogBottomColor", curLevel->calcFogBottomColor());
			FogPostPtr->sendUniform("uPosMult", curLevel->calcFogPosMult());

			FogPostPtr->sendUniform("uEmissivePass",  curLevel->calcFogEmissivePass());

			//uniform vec3 uFogColor = vec3(0.0, 0.0, 0.0);
			//const float FogDensity = 0.05;
			//uniform float uFogNear = 30.0f;
			//uniform float uFogFar = 60.0f;

			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
			glActiveTexture(GL_TEXTURE1);
			//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

			FrameBuffer::DrawFullScreenQuad();


			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}
		else
		{

			for(int f = 0; f < curLevel->FogLayers; ++f)
			{
				FogPostPtr->bind();
				FogPostPtr->sendUniform("uSceneTex", 0);		
				FogPostPtr->sendUniform("uDepthMap", 1);
				FogPostPtr->sendUniform("uEmissiveTex", 2);
				FogPostPtr->sendUniform("uFogTex0", 3);
				FogPostPtr->sendUniform("uFogTex1", 4);
				

				Texture* tex0 = curLevel->calcFogTex0(f);
				Texture* tex1 = curLevel->calcFogTex1(f);

				
				//std::cout << tex0->TexObj << "\t" << tex1->TexObj << " IDs!\n";

				FogPostPtr->sendUniform("uFogTexLerpParam", curLevel->calcFogLerp(f));


				satMat4 ProjBiasMatrix = satMat4(
				2.0f, 0.0f, 0.0f, -1.0f,
				0.0f, 2.0f, 0.0f, -1.0f,
				0.0f, 0.0f, 2.0f, -1.0f,
				0.0f, 0.0f, 0.0f, 1.0f);

				ProjBiasMatrix = activeCamera->GetProjectionInverse() * ProjBiasMatrix;

				FogPostPtr->sendUniformMat4("uProjBiasMatrixInverse", ProjBiasMatrix.Float(), tru);

				FogPostPtr->sendUniform("uAspectRatio", (float)WINDOW_HEIGHT / WINDOW_WIDTH);

				FogPostPtr->sendUniform("uFogColor", curLevel->calcFogColor(f));
				FogPostPtr->sendUniform("uFogNear", curLevel->calcFogNear(f));
				FogPostPtr->sendUniform("uFogFar", curLevel->calcFogFar(f));
				
				FogPostPtr->sendUniform("uRange", curLevel->calcFogRange(f));
				
				FogPostPtr->sendUniform("uFogTopColor", curLevel->calcFogTopColor(f));
				FogPostPtr->sendUniform("uFogBottomColor", curLevel->calcFogBottomColor(f));
				FogPostPtr->sendUniform("uPosMult", curLevel->calcFogPosMult(f));

				FogPostPtr->sendUniform("uEmissivePass",  curLevel->calcFogEmissivePass(f));

				FogPostPtr->sendUniform("uWaveCount", satMath::PI * curLevel->calcFogWaveCount(f));
				FogPostPtr->sendUniform("uWaveIntensity", curLevel->calcFogWaveIntensity(f));
				FogPostPtr->sendUniform("uTime", (float)TimeInLevel * 4.0f * curLevel->calcFogWaveTime(f));
				FogPostPtr->sendUniform("uOffset", satVec2((float)TimeInLevel) * curLevel->calcFogVelocity(f) * 0.10f + curLevel->calcFogOffset(f));
				


				FogPostPtr->sendUniform("uTexOffsetMult", curLevel->calcFogOffsetMult(f) );
				FogPostPtr->sendUniform("uTexOffsetPosMult", curLevel->calcFogOffsetPosMult(f));
				FogPostPtr->sendUniform("uTexOffsetPosAdd", satVec2(adjust.x, adjust.y) * 0.0f);
				
				ActiveFrameBuffer->bind();
				
				tex0->bind(3);
				tex1->bind(4);

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
				glActiveTexture(GL_TEXTURE1);
				//glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(2));
				glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

				FrameBuffer::DrawFullScreenQuad();

				glBindTexture(GL_TEXTURE_2D, GL_NONE);
				glActiveTexture(GL_TEXTURE3);
				glBindTexture(GL_TEXTURE_2D, GL_NONE);
				glActiveTexture(GL_TEXTURE2);
				glBindTexture(GL_TEXTURE_2D, GL_NONE);
				glActiveTexture(GL_TEXTURE1);
				glBindTexture(GL_TEXTURE_2D, GL_NONE);
				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, GL_NONE);

				FrameBuffer::unbind();
				ShaderProgram::unbind();

				ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
			}
		}
	}
#pragma endregion fogPost
	
#pragma region depthOfFieldPost
// DoF dof DOF
	if (!TitleScreenActive && !SpecialThanksActive && !WinScreenActive && curLevel->DOFActive && postProcessDepthOfFieldActive > 0)
	{
		
		DepthOfFieldPostShaderPtr->bind();

		DepthOfFieldPostShaderPtr->sendUniform("uSceneTex", 0);
		DepthOfFieldPostShaderPtr->sendUniform("uDepthTex", 1);		
		DepthOfFieldPostShaderPtr->sendUniform("uResolution", satVec2(WINDOW_WIDTH, WINDOW_HEIGHT));
		//DepthOfFieldPostShaderPtr->sendUniform("uPixelSize", satVec2(1.0f / WINDOW_WIDTH, 1.0f / WINDOW_HEIGHT));
		//int xpos, ypos;
		//glfwGetWindowPos(window, &xpos, &ypos);
		//DepthOfFieldPostShaderPtr->sendUniform("uPixelLook", satVec2(sat::mouse::PosX() - xpos, WINDOW_HEIGHT - (sat::mouse::PosY() - ypos))); 
		//std::cout <<  WINDOW_HEIGHT - (sat::mouse::PosY() - ypos) << std::endl;

		//DepthOfFieldPostShaderPtr->sendUniformMat4("uProj", activeCamera->GetUniformProjection());		
		float clampAdjust = 1.0f;
		if(postProcessDepthOfFieldActive > 1)
			clampAdjust *= 1.0f;
		DepthOfFieldPostShaderPtr->sendUniform("blurclamp", curLevel->calcDOFClamp() * clampAdjust);
		DepthOfFieldPostShaderPtr->sendUniform("bias", curLevel->calcDOFBias() * clampAdjust);

		// ( a + b / z )
		//
		//Where:
		//
		//   N = number of bits of Z precision
		//   a = zFar / ( zFar - zNear )
		//   b = zFar * zNear / ( zNear - zFar )
		//   z = distance from the eye to the object
		//

		float focus = curLevel->calcDOFFocus();
		float focusDepth;
		float a;
		float b;
		//a = FarPlane / (FarPlane - NearPlane);
		//b = FarPlane * NearPlane / (NearPlane - FarPlane);


		

		if(OrthographicActive)
		{
			a = 2.0f * (focus - (-FarPlane)) / (FarPlane - (-FarPlane));
			b = -1.0f;
			focusDepth = (a + b) * 0.5f + 0.5f;

			DepthOfFieldPostShaderPtr->sendUniform("focus", focusDepth);
		}
		else
		{			
			a = (FarPlane + NearPlane) / (FarPlane - NearPlane);
			b = (-2 * (FarPlane * NearPlane) / (FarPlane - NearPlane)) / focus;
			focusDepth = (a + b) * 0.5f + 0.5f;

			DepthOfFieldPostShaderPtr->sendUniform("focus", focusDepth);
		}

		//satVec4 output = activeCamera->GetProjection() * satVec4(0.0f, 0.0f, curLevel->calcDOFFocus(), 1.0f);

		//std::cout << curLevel->calcDOFFocus() << "\t" << output << std::endl;

		//std::cout << focus << "\t" << focusDepth << std::endl;

		//DepthOfFieldPostShaderPtr->sendUniform("focus", (sinf(TotalGameTime * 2.50f) / 256.0f) + 1.0f/128.0f);
		//DepthOfFieldPostShaderPtr->sendUniform("focus", (sinf(Timer::currentf() * 5.00f) / 2.0f) + 1.0f / 32.0f);
		//DepthOfFieldPostShaderPtr->sendUniform("focus", 64.0f / 64.0f + (sinf(Timer::currentf() * 5.00f) / 2.0f) * sat::key::CheckKey(sf::Keyboard::M));
		//DepthOfFieldPostShaderPtr->sendUniform("focus", 0.55f + (sinf(Timer::currentf() * 0.00f) * 0.05f));
		//focus

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion depthOfFieldPost
		
#pragma region sepiaEmissivePost
	float sepiaAmount = curLevel->calcGreyscaleSepiaAmount();
	if (sepiaAmount > 0.0f && postProcessSepiaPlusEmissiveActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		SepiaPlusEmissivePostShader.bind();
		SepiaPlusEmissivePostShader.sendUniform("uSceneTex", 0);
		SepiaPlusEmissivePostShader.sendUniform("uEmissiveTex", 1);
		//SepiaPlusEmissivePostShader.sendUniform("uAmount", sinf(Timer::currentf() * 1.0f)*0.5f+0.5f);
		SepiaPlusEmissivePostShader.sendUniform("uAmount", sepiaAmount);

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion sepiaEmissivePost
	 
#pragma region sepiaPost
	if (timeDead > 0.0f && postProcessSepiaActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		SepiaPostShader.bind();
		SepiaPostShader.sendUniform("uSceneTex", 0);
		//SepiaPostShader.sendUniform("uAmount", sinf(Timer::currentf() * 1.0f)*0.5f+0.5f);
		//SepiaPostShader.sendUniform("uAmount", 1.0f);
		SepiaPostShader.sendUniform("uAmount", InverseLerp(timeDead, 0.0f, timeDeadMax));

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		
		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}		
#pragma endregion sepiaPost

#pragma region grayscaleEmissivePost
	float greyscaleAmount = curLevel->calcGreyscaleAmount();
	if (greyscaleAmount > 0.0f && postProcessGreyscalePlusEmissiveActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		GreyscalePlusEmissivePostShader.bind();
		GreyscalePlusEmissivePostShader.sendUniform("uSceneTex", 0);
		GreyscalePlusEmissivePostShader.sendUniform("uEmissiveTex", 1);
		//GreyscalePlusEmissivePostShader.sendUniform("uAmount", sinf(Timer::currentf() * 1.0f)*0.5f+0.5f);
		GreyscalePlusEmissivePostShader.sendUniform("uAmount", greyscaleAmount);

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion grayscaleEmissivePost

#pragma region grayscalePost
	if (postProcessGreyscaleActive)
	{
		GreyscalePostShader.bind();
		GreyscalePostShader.sendUniform("uSceneTex", 0);
		//GreyscalePostShader.sendUniform("uAmount", sinf(Timer::currentf() * 1.0f)*0.5f+0.5f);
		
		//GreyscalePostShader.sendUniform("uAmount", InverseLerp(timeDead, 0.0f, timeDeadMax));
		GreyscalePostShader.sendUniform("uAmount", 1.0f);
		

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		
		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}		
#pragma endregion grayscalePost
		
#pragma region contrastPost
	if (postProcessContrastActive && curLevel->calcContrastActive())
	{
		ContrastPostShader.bind();
		ContrastPostShader.sendUniform("uSceneTex", 0);
		//ContrastPostShader.sendUniform("uAmount", 1.0f);
		ContrastPostShader.sendUniform("uAmount", curLevel->calcContrastAmount());
		//ContrastPostShader.sendUniform("uAmount", sinf(Timer::currentf() * 1.0f) * 0.5f + 1.5f);


		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

	timeFaceFlash = Max(timeFaceFlash - (float)Timer::framef() * 2.0f, 0.0f);
	if (timeFaceFlash > 0.0f)
	{
		MotionBlurPostShader.bind();
		MotionBlurPostShader.sendUniform("uSceneTex", 0);
		MotionBlurPostShader.sendUniform("uPreviousSceneTex", 1);
		MotionBlurPostShader.sendUniform("uAmount", timeFaceFlash);

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		FaceFlashTexture[FaceFlashPick].bind(1);

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}


	timeContrast = Max(timeContrast - (float)Timer::framef(), 0.0f);
	timeContrastNegative = Between(timeContrastNegative - (float)Timer::framef(), 0.0f, 0.9f);
	if (timeContrast > 0.0f || timeContrastNegative > 0.0f)
	{
		ContrastPostShader.bind();
		ContrastPostShader.sendUniform("uSceneTex", 0);
		ContrastPostShader.sendUniform("uAmount", 1.0f + timeContrast - timeContrastNegative);

		ActiveFrameBuffer->bind();

		Texture::bindFrameColor(0, *swapFrameBuffer(ActiveFrameBuffer));

		FrameBuffer::DrawFullScreenQuad();

		Texture::unbind(0);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
	
#pragma endregion contrastPost
	
#pragma region bloomPost
	if (postProcessBloomActive && curLevel->calcBloomActive() && !WinScreenActive)
	{
		//Compute High Pass
		glViewport((unsigned int)0, (unsigned int)0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE);

		BloomHighPass.bind();
		BloomHighPass.sendUniform("uTex", 0);
		BloomHighPass.sendUniform("uEmissiveTex", 1);
		BloomHighPass.sendUniform("uThresholdLower", curLevel->calcBloomLowerThreshold());
		BloomHighPass.sendUniform("uThresholdUpper", curLevel->calcBloomUpperThreshold());
		//BloomHighPass.sendUniform("uThresholdLower", BLOOM_THRESHOLD_LOWER);
		//BloomHighPass.sendUniform("uThresholdUpper", BLOOM_THRESHOLD_UPPER);
		WorkBuffer1_2.bind();

		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0)); //Get the main buffer as texture for the post-process fullscreen quad.
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, GBuffer.GetColorHandle(5));

		WorkBuffer1_2.DrawFullScreenQuad();
		glViewport(0U, 0U, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2);
		WorkBuffer1_4.bind();
		WorkBuffer1_4.DrawFullScreenQuad();
		glViewport(0U, 0U, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4);
		WorkBuffer1_8.bind();
		WorkBuffer1_8.DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		WorkBuffer1_2.unbind();
		BloomHighPass.unbind();
		
		//Compute Blur
		glViewport(0, 0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE);
		for (int i = 0; i < BLOOM_BLUR_PASSES; i++)
		{
			for (int k = 0; k < BLOOM_HORIZONTAL_BLUR_PASSES; k++)
			{
				//Horizontal Blur
				BlurHorizontal.bind();
				BlurHorizontal.sendUniform("uTex", 0);
				BlurHorizontal.sendUniform("uPixelSize", 1.0f / (WINDOW_WIDTH) * BLOOM_DOWNSCALE);

				WorkBuffer2_2.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer1_2.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
				WorkBuffer1_2.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer2_2.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
			}

			//Vertical Blur
			BlurVertical.bind();
			BlurVertical.sendUniform("uTex", 0);
			BlurVertical.sendUniform("uPixelSize", 1.0f / (WINDOW_HEIGHT) * BLOOM_DOWNSCALE);

			WorkBuffer2_2.bind();
			glBindTexture(GL_TEXTURE_2D, WorkBuffer1_2.GetColorHandle(0));
			FrameBuffer::DrawFullScreenQuad();
			WorkBuffer1_2.bind();
			glBindTexture(GL_TEXTURE_2D, WorkBuffer2_2.GetColorHandle(0));
			FrameBuffer::DrawFullScreenQuad();
		}

		//Compute Blur
		glViewport(0, 0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 2, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 2);
		for (int i = 0; i < BLOOM_BLUR_PASSES; i++)
		{
			for (int k = 0; k < BLOOM_HORIZONTAL_BLUR_PASSES; k++)
			{
				//Horizontal Blur
				BlurHorizontal.bind();
				BlurHorizontal.sendUniform("uTex", 0);
				BlurHorizontal.sendUniform("uPixelSize", 1.0f / (WINDOW_WIDTH)* BLOOM_DOWNSCALE * 2);

				WorkBuffer2_4.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer1_4.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
				WorkBuffer1_4.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer2_4.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
			}

			//Vertical Blur
			BlurVertical.bind();
			BlurVertical.sendUniform("uTex", 0);
			BlurVertical.sendUniform("uPixelSize", 1.0f / (WINDOW_HEIGHT)* BLOOM_DOWNSCALE * 2);

			WorkBuffer2_4.bind();
			glBindTexture(GL_TEXTURE_2D, WorkBuffer1_4.GetColorHandle(0));
			FrameBuffer::DrawFullScreenQuad();
			WorkBuffer1_4.bind();
			glBindTexture(GL_TEXTURE_2D, WorkBuffer2_4.GetColorHandle(0));
			FrameBuffer::DrawFullScreenQuad();
		}

		//Compute Blur
		glViewport(0, 0, (WINDOW_WIDTH) / BLOOM_DOWNSCALE / 4, (WINDOW_HEIGHT) / BLOOM_DOWNSCALE / 4);
		for (int i = 0; i < BLOOM_BLUR_PASSES; i++)
		{
			for (int k = 0; k < BLOOM_HORIZONTAL_BLUR_PASSES; k++)
			{
				//Horizontal Blur
				BlurHorizontal.bind();
				BlurHorizontal.sendUniform("uTex", 0);
				BlurHorizontal.sendUniform("uPixelSize", 1.0f / (WINDOW_WIDTH)* BLOOM_DOWNSCALE * 4);

				WorkBuffer2_8.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer1_8.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
				WorkBuffer1_8.bind();
				glBindTexture(GL_TEXTURE_2D, WorkBuffer2_8.GetColorHandle(0));
				FrameBuffer::DrawFullScreenQuad();
			}

			//Vertical Blur
			BlurVertical.bind();
			BlurVertical.sendUniform("uTex", 0);
			BlurVertical.sendUniform("uPixelSize", 1.0f / (WINDOW_HEIGHT)* BLOOM_DOWNSCALE * 4);

			WorkBuffer2_8.bind();
			Texture::bindFrameColor(0, WorkBuffer1_8);
			FrameBuffer::DrawFullScreenQuad();
			WorkBuffer1_8.bind();
			Texture::bindFrameColor(0, WorkBuffer2_8);
			FrameBuffer::DrawFullScreenQuad();
		}
		
		// Composite to back buffer //

		glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);
		BloomComposite.bind();
		BloomComposite.sendUniform("uScene", 0);
		BloomComposite.sendUniform("uBloom", 1);		
		ActiveFrameBuffer->bind();
		Texture::bindFrameColor(0, *swapFrameBuffer(ActiveFrameBuffer), 0);
		Texture::bindFrameColor(1, WorkBuffer1_2);
		FrameBuffer::DrawFullScreenQuad();
		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		ActiveFrameBuffer->bind();
		Texture::bindFrameColor(0, *swapFrameBuffer(ActiveFrameBuffer), 0);
		Texture::bindFrameColor(1, WorkBuffer1_4);
		FrameBuffer::DrawFullScreenQuad();
		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		ActiveFrameBuffer->bind();
		Texture::bindFrameColor(0, *swapFrameBuffer(ActiveFrameBuffer), 0);
		Texture::bindFrameColor(1, WorkBuffer1_8);
		FrameBuffer::DrawFullScreenQuad();
		Texture::unbind(1);
		Texture::unbind(0);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion bloomPost

#pragma region ACESPost
	if (postProcessACESActive)
	{
		ACESPostShader.bind();
		ACESPostShader.sendUniform("uScene", 0);
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();
		Texture::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
		
#pragma endregion ACESPost
	
#pragma region hueShiftPost
	if (!LoadingActive && (postProcessHueShiftActive ^ TitleScreenActive))
	{
		int testParam = ((Timer::currentf()-0.5f) * 25.0f);
		testParam = testParam % 100;
		//std::cout << testParam * DTR(720.0f) * 0.01f << std::endl;
		HueShiftPostShader.bind();
		HueShiftPostShader.sendUniform("uSceneTex", 0);
		HueShiftPostShader.sendUniform("uHue", testParam * DTR(720.0f) * 0.01f);
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		DeferredComposite.unbind();
		HueShiftPostShader.unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion hueShiftPost

	if(activeCamera == &leftCamera || !ThreeDeeActive)
	{
		modifierRGBSplitRed = satVec2(
			rand() % 60 - 30,
			rand() % 60 - 30);

		modifierRGBSplitGreen = satVec2(
			rand() % 60 - 30,
			rand() % 60 - 30);

		modifierRGBSplitBlue = satVec2(
			rand() % 60 - 30,
			rand() % 60 - 30);

		modifierRGBSplitRed		*= 0.5f;
		modifierRGBSplitGreen	*= 0.5f;
		modifierRGBSplitBlue	*= 0.5f;
	}

#pragma region RGBSplitEarthquakePost
	if (postProcessRGBSplitEarthquakeActive)
	{
		RGBSplitPostShader.bind();

		RGBSplitPostShader.sendUniform("uSceneTex", 0);
		//RGBSplitPostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY));
		//RGBSplitPostShader.sendUniform("uPixelSize", satVec2(1.0 / WINDOW_WIDTH, 1.0 / WINDOW_HEIGHT));

		//RGBSplitPostShader.sendUniform("rOffset", satVec2(+(sinf(Timer::currentf() * 5.0f) + 1.0f) * 0.05f, +(sinf(Timer::currentf() * 2.50f) + 1.0f) * 0.001f));
		//RGBSplitPostShader.sendUniform("bOffset", satVec2(-(sinf(Timer::currentf() * 5.0f) + 1.0f) * 0.05f, -(sinf(Timer::currentf() * 2.50f) + 1.0f) * 0.001f));

		//satVec2 modifier = satVec2(rand() % (unsigned)((3.0f - ShipHealth) * 3.0f + 5), rand() % (unsigned)((3.0f - ShipHealth) * 3.0f + 5));
		//modifier -= satVec2(2.0f + (3.0f - ShipHealth) * 3.0f, 2.0f + (3.0f - ShipHealth) * 3.0f);

		
		//modifier = 16.0;
		RGBSplitPostShader.sendUniform("rOffset", satVec2(modifierRGBSplitRed.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitRed.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)));
		RGBSplitPostShader.sendUniform("bOffset", satVec2(modifierRGBSplitBlue.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitBlue.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)));
		RGBSplitPostShader.sendUniform("gOffset", satVec2(modifierRGBSplitGreen.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitGreen.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)));


		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion RGBSplitEarthquakePost

#pragma region RGBSplitPost
	if (!TitleScreenActive && !SpecialThanksActive && postProcessRGBSplitActive)
	{
		RGBSplitPostShader.bind();

		RGBSplitPostShader.sendUniform("uSceneTex", 0);
		
		//RGBSplitPostShader.sendUniform("rOffset", satVec2(-	0.0035f	,	0.0000f)	);
		//RGBSplitPostShader.sendUniform("bOffset", satVec2(+	0.0035f	,	0.0000f)	);
		//RGBSplitPostShader.sendUniform("gOffset", satVec2(	0.0000f	,	0.0000f)	);
		
		RGBSplitPostShader.sendUniform("rOffset", satVec2(-0.0045f * Timer::sin(2.0f), -0.0045f * Timer::cos(2.0f)) * Timer::sinRange(0.875f, 0.5f, 1.0f));
		RGBSplitPostShader.sendUniform("bOffset", satVec2(+0.0045f * Timer::sin(2.0f), +0.0045f * Timer::cos(2.0f)) * Timer::sinRange(0.875f, 0.5f, 1.0f));
		RGBSplitPostShader.sendUniform("gOffset", satVec2(0.0000f, 0.0000f));


		//RGBSplitPostShader.sendUniform("rOffset", satVec2(+	0.0015f, 0.0000f));
		//RGBSplitPostShader.sendUniform("bOffset", satVec2(+	0.0015f, 0.0000f));
		//RGBSplitPostShader.sendUniform("gOffset", satVec2(- 0.0015f, 0.0000f));

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion RGBSplitPost

#pragma region ChromaticAberrationPost
	if (postProcessChromaticAberrationActive && !WinScreenActive && !SpecialThanksActive)
	{
		ChromaticAberrationPostShaderPtr = &ChromaticAberrationPostShader[DetailChromaticAberration];
		if(LoadingActive)
		{
			ChromaticAberrationPostShaderPtr = &ChromaticAberrationPostShader[ChromaticAberrationPostShader.size()-1];
		}

		ChromaticAberrationPostShaderPtr->bind();

		ChromaticAberrationPostShaderPtr->sendUniform("uSceneTex", 0);
		//ChromaticAberrationPostShaderPtr->sendUniform("uAspect", satVec2(1.0f, (float)WINDOW_HEIGHT / WINDOW_WIDTH));
		satVec2 aspect = Lerp(satVec2(1.0f, (float)WINDOW_HEIGHT / WINDOW_WIDTH), satVec2(1.0f, 1.0f), 0.25f);
		ChromaticAberrationPostShaderPtr->sendUniform("uAspect", aspect);
		
		if(!LoadingActive)
		{
			ChromaticAberrationPostShaderPtr->sendUniform("uDispersal", Timer::sinRange(0.7f, 4.0f, 4.2f)); 
			//ChromaticAberrationPostShaderPtr->sendUniform("uDispersal", 1.0f); 
			ChromaticAberrationPostShaderPtr->sendUniform("rOffset", 1.0f *  0.0200f * Timer::sinRange(1.0f, 1.0f, 1.05f)); // * Timer::sinRange(1.0f, 0.0f, 3.0f)
			ChromaticAberrationPostShaderPtr->sendUniform("bOffset", 1.0f * -0.0200f * Timer::sinRange(1.0f, 1.0f, 1.05f)); // * Timer::sinRange(1.0f, 0.0f, 3.0f)
			ChromaticAberrationPostShaderPtr->sendUniform("gOffset", 0.0000f);
		}
		else
		{
			ChromaticAberrationPostShaderPtr->sendUniform("uDispersal", 0.5f); 
			ChromaticAberrationPostShaderPtr->sendUniform("rOffset",  0.0250f); 
			ChromaticAberrationPostShaderPtr->sendUniform("bOffset", -0.0250f); 
			ChromaticAberrationPostShaderPtr->sendUniform("gOffset",  0.0000f);
		}

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion ChromaticAberrationPost

#pragma region wavePost

	ShaderProgram *WavePostShaderPtr = &WavePostShader; 

	if(postProcessWave2Active)
	{
		WavePostShaderPtr = &WavePost2Shader;
	}

	satVec2 waveIntensity = curLevel->calcWaveIntensity();

	if (curLevel->WaveActive && postProcessWaveActive && waveIntensity.x > 0.0f && waveIntensity.y > 0.0f && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive)
	{
		WavePostShaderPtr->bind();
		WavePostShaderPtr->sendUniform("uSceneTex", 0);
		WavePostShaderPtr->sendUniform("uWaveCount", curLevel->calcWaveCount() * satMath::PI);
		WavePostShaderPtr->sendUniform("uWaveIntensity", waveIntensity);
		//WavePostShaderPtr->sendUniform("uTime", satVec2((float)Timer::currentf()) * curLevel->calcWaveTime());
		WavePostShaderPtr->sendUniform("uTime", satVec2((float)TimeInLevel) * curLevel->calcWaveTime());
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
	
	if (timeDead > 0.0f && postProcessWaveActive)
	{
		WavePostShaderPtr->bind();
		WavePostShaderPtr->sendUniform("uSceneTex", 0);
		WavePostShaderPtr->sendUniform("uWaveCount", InverseLerp(timeDead, 0.0f, timeDeadMax) * satVec2((15.0f + sin(Timer::currentf() * 1.0f)) * 1.0f * satMath::PI, (320.0f + sin(Timer::currentf()* 0.5f) * 4.0f) * satMath::PI));
		WavePostShaderPtr->sendUniform("uWaveIntensity", 1.0f * InverseLerp(timeDead, timeDeadMax * 0.5f, timeDeadMax) * satVec2(0.03f + sin(Timer::currentf() * 1.0f) * 0.01f, 0.005f + sin(Timer::currentf()* 0.5f) * 0.0015f));
		WavePostShaderPtr->sendUniform("uTime", satVec2((float)Timer::currentf() * 32.0f, (float)Timer::currentf() * 8.0f));
		
		//WavePostShader.sendUniform("uWaveCount", satVec2((3.0f + sin(Timer::currentf() * 1.0f)) * 1.0f * satMath::PI, (8.0f + sin(Timer::currentf()* 0.5f) * 4.0f) * satMath::PI));
		//WavePostShader.sendUniform("uWaveIntensity", satVec2(0.03f + sin(Timer::currentf() * 1.0f) * 0.01f, 0.015f + sin(Timer::currentf()* 0.5f) * 0.005f));
		//WavePostShader.sendUniform("uTime", satVec2((float)Timer::currentf() * 2.0f, (float)Timer::currentf() * 8.0f));

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

	if (collisionTime > 0.0f && postProcessWaveActive)
	{
		WavePostShaderPtr->bind();
		WavePostShaderPtr->sendUniform("uSceneTex", 0);
		WavePostShaderPtr->sendUniform("uWaveCount", satMath::PI * satVec2(WINDOW_WIDTH / 8.0f, WINDOW_HEIGHT / 8.0f));
		WavePostShaderPtr->sendUniform("uWaveIntensity", 0.0525f * InverseLerp(collisionTime, 0.0f, invincibleTime * 0.5f) * satVec2(8.0f / WINDOW_WIDTH, 2.0f / WINDOW_HEIGHT));
		WavePostShaderPtr->sendUniform("uTime", satVec2((float)Timer::currentf() * 8.0f, (float)Timer::currentf() * 8.0f));

		//WavePostShader.sendUniform("uWaveCount", satVec2((3.0f + sin(Timer::currentf() * 1.0f)) * 1.0f * satMath::PI, (8.0f + sin(Timer::currentf()* 0.5f) * 4.0f) * satMath::PI));
		//WavePostShader.sendUniform("uWaveIntensity", satVec2(0.03f + sin(Timer::currentf() * 1.0f) * 0.01f, 0.015f + sin(Timer::currentf()* 0.5f) * 0.005f));
		//WavePostShader.sendUniform("uTime", satVec2((float)Timer::currentf() * 2.0f, (float)Timer::currentf() * 8.0f));

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

	if ((TitleScreenActive || SpecialThanksActive))
	{
		WavePostShaderPtr->bind();
		WavePostShaderPtr->sendUniform("uSceneTex", 0);
		//WavePostShaderPtr->sendUniform("uWaveCount", curLevel->calcWaveCount() * satMath::PI);
		//WavePostShaderPtr->sendUniform("uWaveIntensity", satVec2(0.03f + sin(Timer::currentf() * 1.0f) * 0.01f, 0.015f + sin(Timer::currentf()* 0.5f) * 0.005f));
		//WavePostShaderPtr->sendUniform("uTime", satVec2((float)Timer::currentf() * 2.0f, (float)Timer::currentf() * 8.0f));
		WavePostShaderPtr->sendUniform("uWaveCount", satVec2((3.0f + sin(Timer::currentf() * 1.0f)) * 1.0f * satMath::PI, (8.0f + sin(Timer::currentf()* 0.5f) * 4.0f) * satMath::PI));
		WavePostShaderPtr->sendUniform("uWaveIntensity", satVec2(0.002f + sin(Timer::currentf() * 1.0f) * 0.001f, 0.002f + sin(Timer::currentf()* 0.5f) * 0.001f));
		WavePostShaderPtr->sendUniform("uTime", satVec2((float)Timer::currentf() * 2.0f, (float)Timer::currentf() * 8.0f));
		//WavePostShader.sendUniform("uBackgroundColor", satVec3(0.0f));

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

#pragma endregion wavePost
	
#pragma region CollisionPost
	//if (collisionTime > invincibleTime / 2 || ((collisionTime * invincibleTime / 16) % 2 && collisionTime > 0))
	if (collisionTime > 0)
	{
		//ContrastPostShader.bind();
		//ContrastPostShader.sendUniform("uSceneTex", 0);
		//ContrastPostShader.sendUniform("uAmount", (float)collisionTime * 1.0f * invincibleTimeInverse + 1.0f);
		//
		//ActiveFrameBuffer->bind();
		//
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		//
		//FrameBuffer::DrawFullScreenQuad();
		//
		////glBindTexture(GL_TEXTURE_2D, GL_NONE);
		//
		//FrameBuffer::Unbind();
		//ShaderProgram::unbind();
		//
		//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

		if(collisionTime > invincibleTime * 0.97 && true)
		{
			HueShiftPostShader.bind();
			HueShiftPostShader.sendUniform("uSceneTex", 0);
			HueShiftPostShader.sendUniform("uHue", DTR(180.0f));
			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			DeferredComposite.unbind();
			HueShiftPostShader.unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
			
			InvertPostShader.bind();
			InvertPostShader.sendUniform("uSceneTex", 0);
			
			ActiveFrameBuffer->bind();
			
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
			
			FrameBuffer::DrawFullScreenQuad();
			
			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			
			FrameBuffer::unbind();
			ShaderProgram::unbind();
			
			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}		

		if(collisionTime > invincibleTime * 0.87 && collisionTime < invincibleTime * 0.90  && true)
		{
			InvertPostShader.bind();
			InvertPostShader.sendUniform("uSceneTex", 0);
			
			ActiveFrameBuffer->bind();
			
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
			
			FrameBuffer::DrawFullScreenQuad();
			
			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			
			FrameBuffer::unbind();
			ShaderProgram::unbind();
			
			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}	

		if(collisionTime > invincibleTime * 0.9 && postProcessRGBSplitActive)
		{
			RGBSplitPostShader.bind();
			RGBSplitPostShader.sendUniform("uSceneTex", 0);

			//satVec2 modifierRed = satVec2(
			//	rand() % 60 - 30,
			//	rand() % 60 - 30);
			//
			//satVec2 modifierGreen = satVec2(
			//	rand() % 60 - 30,
			//	rand() % 60 - 30);
			//
			//satVec2 modifierBlue = satVec2(
			//	rand() % 60 - 30,
			//	rand() % 60 - 30);
			//modifier = 16.0;
			
			RGBSplitPostShader.sendUniform("rOffset", satVec2(modifierRGBSplitRed.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitRed.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)) * (float)collisionTime * invincibleTimeInverse);
			RGBSplitPostShader.sendUniform("bOffset", satVec2(modifierRGBSplitBlue.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitBlue.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)) * (float)collisionTime * invincibleTimeInverse);
			RGBSplitPostShader.sendUniform("gOffset", satVec2(modifierRGBSplitGreen.x / (WINDOW_WIDTH  >> UpScaleResolutionX), modifierRGBSplitGreen.y / (WINDOW_HEIGHT  >> UpScaleResolutionY)) * (float)collisionTime * invincibleTimeInverse);


			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}


		RGBSplitPostShader.bind();
		satVec2 modifier = satVec2(rand() % (unsigned)((3.0f - ShipHealth) * 3.0f + 5), rand() % (unsigned)((3.0f - ShipHealth) * 3.0f + 5));
		modifier -= satVec2(2.0f + (3.0f - ShipHealth) * 3.0f, 2.0f + (3.0f - ShipHealth) * 3.0f);
		//modifier = 16.0;
		RGBSplitPostShader.sendUniform("uSceneTex", 0);
		RGBSplitPostShader.sendUniform("rOffset", satVec2(((4.0f - ShipHealth) * 1.75f * (-4.0f)) / WINDOW_WIDTH * (float)collisionTime * invincibleTimeInverse, modifier.x / WINDOW_HEIGHT * (float)collisionTime * invincibleTimeInverse));
		RGBSplitPostShader.sendUniform("bOffset", satVec2(((4.0f - ShipHealth) * 1.75f * (+4.0f)) / WINDOW_WIDTH * (float)collisionTime * invincibleTimeInverse, modifier.y / WINDOW_HEIGHT * (float)collisionTime * invincibleTimeInverse));
		RGBSplitPostShader.sendUniform("gOffset", satVec2(0.0f, 0.0f));

		RGBSplitPostShader.sendUniform("uRandom", modifierRGBSplitGreen.x);
		RGBSplitPostShader.sendUniform("uRange", 0.3f);
		ActiveFrameBuffer->bind();

		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();
		RGBSplitPostShader.sendUniform("uRange", 0.0f);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

		satVec2 grain;
		grain.x = rand() % 500 / 5000.0f + 0.95f;
		grain.y = rand() % 500 / 5000.0f + 0.95f;

		FilmGrainPostShader.bind();
		FilmGrainPostShader.sendUniform("uSceneTex", 0);
		FilmGrainPostShader.sendUniform("uGrain", grain);
		FilmGrainPostShader.sendUniform("uAmount", (float)collisionTime * 0.05f * invincibleTimeInverse + 0.05f);
		FilmGrainPostShader.sendUniform("uAspectRatio", satVec2(Game::WINDOW_WIDTH / Game::WINDOW_HEIGHT, 1.0f));

		ActiveFrameBuffer->bind();

		//glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion CollisionPost

#pragma region motionBlurPost
	if (postProcessMotionBlurActive)
	{
		//MotionBlurPostShader.bind();
		//MotionBlurPostShader.sendUniform("uSceneTex", 0);
		//MotionBlurPostShader.sendUniform("uPreviousSceneTex", 1);
		//MotionBlurPostShader.sendUniform("uAmount", 0.4f);
		//
		//ActiveFrameBuffer->bind();
		//
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		//glActiveTexture(GL_TEXTURE1);
		//glBindTexture(GL_TEXTURE_2D, PreviousFrame.GetColorHandle(0));
		//
		//FrameBuffer::DrawFullScreenQuad();
		//
		//glBindTexture(GL_TEXTURE_2D, GL_NONE);
		//glActiveTexture(GL_TEXTURE0);
		//glBindTexture(GL_TEXTURE_2D, GL_NONE);
		//
		//FrameBuffer::Unbind();
		//ShaderProgram::unbind();
		//
		//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		//
	}
#pragma endregion motionBlurPost

#pragma region testPost
	if (postProcessTestActive)
	{
		TestPostShader.bind();
		TestPostShader.sendUniform("uSceneTex", 0);
		TestPostShader.sendUniform("uPreviousSceneTex", 1);
		TestPostShader.sendUniform("uAmount", 0.75f);
		
		//TestPostShader.sendUniform("uGlobalTime", TotalGameTime * 1.0f);

		

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_2D, PreviousFrame.GetColorHandle(1));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		
	}
#pragma endregion testPost
	
#pragma region FXAAPost
	if (postProcessFXAAActive && !TitleScreenActive && !SpecialThanksActive && !WinScreenActive) // && !postProcessMotionBlurActive
	{
		FXAAPostShader.bind();
		int AACount = 1;
		for (int i = 0; i < AACount; ++i)
		{
			

			//TestPostShader.sendUniform("textureSampler", 0); //
			//TestPostShader.sendUniform("texcoordOffset", satVec2(1.0 / WINDOW_WIDTH, 1.0 / WINDOW_HEIGHT));

			FXAAPostShader.sendUniform("uSceneTex", 0);
			//FXAAPostShader.sendUniform("uDepthTex", 1);
			//TestPostShader.sendUniform("uGlobalTime", TotalGameTime * 1.0f);

			//float tempVariable;
			//TestPostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY));





			FXAAPostShader.sendUniform("uPixelSize", satVec2(1.0 / (WINDOW_WIDTH  >> UpScaleResolutionX), 1.0 / (WINDOW_HEIGHT  >> UpScaleResolutionY)));


			//FXAAPostShader.sendUniform("uWaveCount", satVec2(2.0f, 8.0f) * satMath::PI);
			//FXAAPostShader.sendUniform("uWaveIntensity", satVec2(0.06f, 0.01f));
			//FXAAPostShader.sendUniform("uAmount", satVec2((float)Timer::currentf() * 2.0f, (float)Timer::currentf() * 8.0f));

			//TestPostShader.sendUniform("uResolutionX", WINDOW_WIDTH);
			//TestPostShader.sendUniform("uResolutionY", WINDOW_HEIGHT);
			//TestPostShader.sendUniformDouble("uPixelSizeX", 1.0 / WINDOW_WIDTH);		
			//TestPostShader.sendUniformDouble("uPixelSizeY", 1.0 / WINDOW_HEIGHT);

			//TestPostShader.sendUniform("uAmount", 0.0f);
			//TestPostShader.sendUniform("uModX", 4);
			//TestPostShader.sendUniform("uModY", 8);
			//TestPostShader.sendUniform("uThresholdX", 0);
			//TestPostShader.sendUniform("uThresholdY", 1);

			//TestPostShader.sendUniform("uAmount", 0.0f);
			//TestPostShader.sendUniform("uModX", (int)(sinf(TotalGameTime * 5.0f + satMath::PI) * 14 + 16));
			//TestPostShader.sendUniform("uModY", (int) (sinf(TotalGameTime * 5.0f) * 14 + 16));
			//TestPostShader.sendUniform("uThresholdX", (int)(sinf(TotalGameTime * 5.0f + satMath::PI) * 3 + 4));
			//TestPostShader.sendUniform("uThresholdY", (int)(sinf(TotalGameTime * 5.0f) * 3 + 4));
			//TestPostShader.sendUniform("uThresholdX", (int) (sinf(TotalGameTime * 4.0f) * 3 + 4));
			//TestPostShader.sendUniform("uThresholdY", (int) (sinf(TotalGameTime * 15.0f) * 3 + 4));

			//uniform float uAmount = 0.15;
			//uniform int uModX = 4;
			//uniform int uModY = 4;
			//uniform int uThresholdX = 2;
			//uniform int uThresholdY = 2;

			//TestPostShader.sendUniform("focus", (sinf(TotalGameTime * 2.50f) / 256.0f) + 1.0f/128.0f);
			//TestPostShader.sendUniform("focus", (sinf(TotalGameTime * 2.50f) / 2.0f) + 1.0f / 2.0f);
			//TestPostShader.sendUniform("focus", 0.0f);
			//focus

			//TestPostShader.sendUniform("rOffset", satVec2(+(sinf(TotalGameTime * 5.0f) + 1.0f) * 0.001f, +(sinf(TotalGameTime * 2.50f) + 1.0f) * 0.001f));
			//TestPostShader.sendUniform("bOffset", satVec2(-(sinf(TotalGameTime * 5.0f) + 1.0f) * 0.001f, -(sinf(TotalGameTime * 2.50f) + 1.0f) * 0.001f));

			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
			//glActiveTexture(GL_TEXTURE1);
			//glBindTexture(GL_TEXTURE_2D, GBuffer.GetDepthHandle());

			FrameBuffer::DrawFullScreenQuad();

			//glBindTexture(GL_TEXTURE_2D, GL_NONE);
			//glActiveTexture(GL_TEXTURE0);
			

			
			//

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		ShaderProgram::unbind();
		FrameBuffer::unbind();
	}
#pragma endregion FXAAPost

#pragma region colorGrainPost
	
	if (postProcessColorGrainActive && (ShipHealth < 3 || PauseActive))
	{

		if(activeCamera != &rightCamera)
		{
			colorGrain.x = rand() % 500 / 5000.0f + 0.95f;
			colorGrain.y = rand() % 500 / 5000.0f + 0.95f;
			colorGrain.z = rand() % 500 / 5000.0f + 0.95f;
		}

		ColorGrainPostShader.bind();
		ColorGrainPostShader.sendUniform("uSceneTex", 0);
		ColorGrainPostShader.sendUniform("uGrain", colorGrain);

		if(PauseActive)
		{
			ColorGrainPostShader.sendUniform("uAmount", Timer::sinRange(0.15f, 0.00f, 0.05f)); //0.09
		}
		else
		{
			ColorGrainPostShader.sendUniform("uAmount", Between((3.0f - ShipHealth) * 0.02f, 0.0f, 1.0f)); //0.09
		}

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion colorGrainPost

#pragma region filmGrainPost
	if ((!TitleScreenActive || Game::TransitionTitle > 0.0f) && !SpecialThanksActive && !WinScreenActive && (adjust.x < 16.0f || adjust.x > curLevel->WinPoint - 16.0f || timeDead > 0.0f))
	{
		
		float totalFilmGrain = 0.0f;

		if (!TitleScreenActive)
		{
			if(ShipHealth > 0)
			{
				if(adjust.x < 15.5f)
				{
					totalFilmGrain += InverseLerp(adjust.x, 15.0f, 0.0f);
					//float freqParam = 1.0f - totalFilmGrain * 1.0f;
					//songPitch *= freqParam;
					//songVolume *= 1.0f;
					//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch();
					//LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume(1.0f);
				}
				elf(adjust.x > curLevel->WinPoint - 15.5f)
				{
					totalFilmGrain += InverseLerp(adjust.x, curLevel->WinPoint - 15.0f, curLevel->WinPoint - 0.0f);
					//float fadeout = 1.0f - InverseLerp(adjust.x, curLevel->WinPoint - 15.0f, curLevel->WinPoint - 0.0f);
					//float freqParam = 1.0f + powf(totalFilmGrain, 3.0f) * 36.0f;
					//fadeout = powf(fadeout, 2.0f);
					//songPitch *= freqParam;
					//songVolume *= fadeout;
					//LevelManager::level[LevelManager::currentSongPlaying].Song.changePitch();
					//LevelManager::level[LevelManager::currentSongPlaying].Song.changeVolume());
				}
			}
			


			if (timeDead > 0.0f)
			{
				totalFilmGrain += InverseLerp(timeDead, timeDeadMax * 0.75f, timeDeadMax);
				//std::cout << InverseLerp(timeDead, 0.0f, timeDeadMax) << std::endl;
			}
		}


		if (Game::TransitionTitle > 0.0f)
		{
			totalFilmGrain += InverseLerp(Game::TransitionTitle, 0.0f, Game::TransitionTitleEnd);
			if (SelectSong.isPlaying())
			{
				SelectSong.changeVolume(1.0f - totalFilmGrain);
			}
			//;
		}

		if (totalFilmGrain > 0.0f && postProcessFilmGrainActive)
		{
			satVec2 grain;
			grain.x = rand() % 500 / 5000.0f + 0.95f;
			grain.y = rand() % 500 / 5000.0f + 0.95f;

			FilmGrainPostShader.bind();
			FilmGrainPostShader.sendUniform("uSceneTex", 0);
			FilmGrainPostShader.sendUniform("uGrain", grain);

			totalFilmGrain = Min(totalFilmGrain, 1.0f);
			FilmGrainPostShader.sendUniform("uAmount", totalFilmGrain * totalFilmGrain);


			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}

		//if (totalFilmGrain > 0.0f && postProcessRGBSplitActive)
		//{
		//	RGBSplitPostShader.bind();
		//	RGBSplitPostShader.sendUniform("uSceneTex", 0);
		//	//RGBSplitPostShader.sendUniform("rOffset", satVec2(-0.0075f*totalFilmGrain, 0.0000f));
		//	//RGBSplitPostShader.sendUniform("bOffset", satVec2(+0.0075f*totalFilmGrain, 0.0000f));
		//	//RGBSplitPostShader.sendUniform("gOffset", satVec2(0.0000f, 0.0000f));
		//	RGBSplitPostShader.sendUniform("rOffset", totalFilmGrain * satVec2(-0.0075f * Timer::sin(-3.0f), -0.0075f * Timer::cos(-3.0f)));
		//	RGBSplitPostShader.sendUniform("bOffset", totalFilmGrain * satVec2(+0.0075f * Timer::sin(-3.0f), +0.0075f * Timer::cos(-3.0f)));
		//	RGBSplitPostShader.sendUniform("gOffset", satVec2(0.0000f, 0.0000f));
		//
		//	ActiveFrameBuffer->bind();
		//
		//	glActiveTexture(GL_TEXTURE0);
		//	glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		//
		//	FrameBuffer::DrawFullScreenQuad();
		//
		//	glBindTexture(GL_TEXTURE_2D, GL_NONE);
		//
		//	FrameBuffer::Unbind();
		//	ShaderProgram::unbind();
		//
		//	ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		//}

		if (totalFilmGrain > 0.0f && postProcessChromaticAberrationActive)
		{
			float offset =  1.0f * powf(totalFilmGrain, 3.0f/3.0f);
			float dispersal = 1.0f + totalFilmGrain;

			ShaderProgram *ChromaticAberrationGrainPtr = &ChromaticAberrationPostShader[0]; 

			if(offset > 0.15f / 2)
			{
				ChromaticAberrationGrainPtr = 
					&ChromaticAberrationPostShader
					[Max((unsigned)0, ChromaticAberrationPostShader.size() - 1)];
			}
			elf(offset > 0.15f / 4)
			{
				ChromaticAberrationGrainPtr = 
					&ChromaticAberrationPostShader
					[Max((unsigned)0, ChromaticAberrationPostShader.size() - 2)];
			}
			elf(offset > 0.15f / 8)
			{
				ChromaticAberrationGrainPtr = 
					&ChromaticAberrationPostShader
					[Max((unsigned)0, ChromaticAberrationPostShader.size() - 3)];
			}
			elf(offset > 0.15f / 16)
			{
				ChromaticAberrationGrainPtr = 
					&ChromaticAberrationPostShader
					[Max((unsigned)0, ChromaticAberrationPostShader.size() - 3)];
			}

			ChromaticAberrationGrainPtr->bind();
			ChromaticAberrationGrainPtr->sendUniform("uSceneTex", 0);
			//ChromaticAberrationGrainPtr->sendUniform("uAspect", satVec2(1.0f, (float)WINDOW_HEIGHT / WINDOW_WIDTH));
			satVec2 aspect = Lerp(satVec2(1.0f, (float)WINDOW_HEIGHT / WINDOW_WIDTH), satVec2(1.0f, 1.0f), 0.25f);
			ChromaticAberrationGrainPtr->sendUniform("uAspect", aspect);
		
			ChromaticAberrationGrainPtr->sendUniform("uDispersal", dispersal); 
			ChromaticAberrationGrainPtr->sendUniform("rOffset",  offset); // * Timer::sinRange(1.0f, 0.0f, 3.0f)
			ChromaticAberrationGrainPtr->sendUniform("bOffset", -offset); // * Timer::sinRange(1.0f, 0.0f, 3.0f)
			ChromaticAberrationGrainPtr->sendUniform("gOffset",  0.0000f);

			//ChromaticAberrationGrainPtr->sendUniform("rOffset", totalFilmGrain * satVec2(-0.0075f * Timer::sin(-3.0f), -0.0075f * Timer::cos(-3.0f)));
			//ChromaticAberrationGrainPtr->sendUniform("bOffset", totalFilmGrain * satVec2(+0.0075f * Timer::sin(-3.0f), +0.0075f * Timer::cos(-3.0f)));
			//ChromaticAberrationGrainPtr->sendUniform("gOffset", satVec2(0.0000f, 0.0000f));

			ActiveFrameBuffer->bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}
		

	}
#pragma endregion filmGrainPost
			
#pragma region invertBrightnessPost
	//NoDamageTime > 0.0f && 
	if (postProcessInvertBrightnessActive)
	{
		HueShiftPostShader.bind();
		HueShiftPostShader.sendUniform("uSceneTex", 0);
		HueShiftPostShader.sendUniform("uHue", DTR(180.0f));
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		DeferredComposite.unbind();
		HueShiftPostShader.unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		
		InvertPostShader.bind();
		InvertPostShader.sendUniform("uSceneTex", 0);
		
		ActiveFrameBuffer->bind();
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		
		FrameBuffer::DrawFullScreenQuad();
		
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		
		FrameBuffer::unbind();
		ShaderProgram::unbind();
		
		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}		
#pragma endregion invertBrightnessPost

#pragma region invertPost
	if (postProcessInvertActive)
	{
		InvertPostShader.bind();
		InvertPostShader.sendUniform("uSceneTex", 0);
		
		ActiveFrameBuffer->bind();
		
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		
		FrameBuffer::DrawFullScreenQuad();
		
		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		
		FrameBuffer::unbind();
		ShaderProgram::unbind();
		
		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}		
#pragma endregion invertPost

#pragma region CRTPost
	if (postProcessCRTActive)
	{
		CRTPostShader.bind();
		CRTPostShader.sendUniform("uSceneTex", 0);
		CRTPostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY));
		CRTPostShader.sendUniform("uPixelSize", satVec2(1.0f / (WINDOW_WIDTH  >> UpScaleResolutionX), 1.0f / (WINDOW_HEIGHT  >> UpScaleResolutionY)));
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion CRTPost

#pragma region scanlinePost
	if (curLevel->ScanlineActive && postProcessScanlineActive)
	{
		ScanlinePostShader.bind();

		ScanlinePostShader.sendUniform("uSceneTex", 0);
		ScanlinePostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY));
		//ScanlinePostShader.sendUniform("uPixelSize", satVec2(1.0 / WINDOW_WIDTH, 1.0 / WINDOW_HEIGHT));
						
		ScanlinePostShader.sendUniform("uModX", curLevel->calcScanlineModX());
		ScanlinePostShader.sendUniform("uModY", curLevel->calcScanlineModY());
		ScanlinePostShader.sendUniform("uThresholdX", curLevel->calcScanlineThresholdX());
		ScanlinePostShader.sendUniform("uThresholdY", curLevel->calcScanlineThresholdY());
		ScanlinePostShader.sendUniform("uAmount", curLevel->calcScanlineAmount());
				
		
		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		FrameBuffer::DrawFullScreenQuad();

		glBindTexture(GL_TEXTURE_2D, GL_NONE);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion scanlinePost

#pragma region previousFrame

	//PassThroughShader.bind();
	//PassThroughShader.sendUniform("uSceneTex", 0);
	//
	//PreviousFrame.bind();
	//
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
	//
	//FrameBuffer::DrawFullScreenQuad();
	//
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//
	//FrameBuffer::Unbind();
	//ShaderProgram::unbind();
	//
	//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

#pragma endregion previousFrame


#pragma region DisplayHUD
	
	if(WinScreenActive)
	{
		//glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
		ActiveFrameBuffer->bind();
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
		FrameBuffer::DrawFullScreenQuad();
		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}

	//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

	PassThroughShader.bind();
	PassThroughShader.sendUniform("uSceneTex", 0);
	ActiveFrameBuffer->bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
	FrameBuffer::DrawFullScreenQuad();
	FrameBuffer::unbind();
	ShaderProgram::unbind();

	//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

	if (SatDebugDrawHUDActive && (!TitleScreenActive && !SpecialThanksActive && !WinScreenActive))
	{
		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

		//std::cout << ScoreBoard.hiScore << std::endl;
		satVec2 ThreeDeeOffset = 0;
		int HudDigitOffset = 32;
		int bitShiftScaling = 0;
		
		float EnemyScoreColorBoost = Lerp(0.0f, 1.0f, InverseLerp(timeContrastNegative, 0.0f, 0.25f));
		long long scoreDisplay = HighScore*ScoreMult + currentTotalScore;
		long long scoreDisplayCompare = HighScore*ScoreMult;
		double timeDisplay = TimeInLevel + currentTotalTime;

		//if (timeDisplay >= 6000)
		//{
		//	timeDisplay = 5999.95;
		//}

		double maxTimeDisplay = 3600;
		if (timeDisplay >= maxTimeDisplay)
		{
			timeDisplay = maxTimeDisplay - 0.1;
		}

		int maxScoreDisplay = 999999999;
		if (scoreDisplay >= maxScoreDisplay)
		{
			scoreDisplay = maxScoreDisplay;
		}

		if (WINDOW_WIDTH * ( (float) WINDOW_WIDTH / (float) WINDOW_HEIGHT ) < 2200.0f || OrthographicActive) //1400
		{
			bitShiftScaling = 1;
			HudDigitOffset = HudDigitOffset >> 1;
		}


		if (timeDead > timeDeadReverse)
		{
			float t = InverseLerp((timeDeadMax - timeDeadReverse) - (timeDead - timeDeadReverse), timeDeadMax - timeDeadReverse, 0.0f);
			TimeInLevel = Lerp(timeDeadRewindTime, 0.0f, t * t);
			//TimeInLevel = Lerp(timeDeadRewindTime, 0.0, t * t);
			scoreDisplay = Lerp(scoreDisplay, (long long)(defaultScore * defaultScoreMult + currentTotalScore), t * t);
		}
		else if (timeDead > 0.0f && timeDead <= timeDeadReverse)
		{
			//float t = InverseLerp(timeDead, timeDeadReverse, 0.0f);
			//TimeInLevel = Lerp(0.0f, timeDead, 1.0f - t * t);
		}

		//if (activeCamera == &leftCamera)
		//{
		//	ThreeDeeOffset = -10 - Timer::sin(4.0f) * 4.2;
		//}
		//elf(activeCamera == &rightCamera)
		//{
		//	ThreeDeeOffset = 10 + Timer::sin(4.0f) * 4.2;
		//}

		//if (activeCamera == &leftCamera)
		//{
		//	ThreeDeeOffset.x = (int)(-1 - Timer::sin(3.0f) * 1.1) >> bitShiftScaling;
		//}
		//elf(activeCamera == &rightCamera)
		//{
		//	ThreeDeeOffset.x = (int)(+1 + Timer::sin(3.0f) * 1.1) >> bitShiftScaling;
		//}
		
		if (activeCamera == &leftCamera)
		{
			ThreeDeeOffset.x = -(2 >> bitShiftScaling);
		}
		elf(activeCamera == &rightCamera)
		{
			ThreeDeeOffset.x = (2 >> bitShiftScaling);
		}

		float chance = randomFloats(generator);
		satVec2 randomOffsetTop = satVec2(randomFloats(generator) * 6.0f - 3.0f, randomFloats(generator) * 4.0f - 2.0f);
		satVec2 randomOffsetBottom = satVec2(randomFloats(generator) * 6.0f - 3.0f, randomFloats(generator) * 4.0f - 2.0f);


		if (timeContrastNegative > 0.0f)
		{
			
			//ThreeDeeOffset += randomOffsetTop;
		}
		else
		{
			randomOffsetTop = 0.0f;
			randomOffsetBottom = 0.0f;
		}


		float fractpart, intpart;
		bool rainbow = false;
		float rainbowNumInterp = TimeInLevel * 7.5f;
		rainbowNumInterp = modf(rainbowNumInterp, &intpart);
		int rainbowNum = intpart;
		

		glViewport((24 - 10 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (64 >> bitShiftScaling) - 10 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, (((32 + 32 + 256) >> bitShiftScaling) + 4) >> UpScaleResolutionX, (((32 + 16) >> bitShiftScaling) + 4) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		glViewport((24 - 10 + ((int)(ThreeDeeOffset.x + randomOffsetBottom.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - 10 + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, (((32 + 32 + 256 - 64) >> bitShiftScaling) + 4) >> UpScaleResolutionX, (((32 + 16) >> bitShiftScaling) + 4) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();
		


		glViewport((24 - 10 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (168 >> bitShiftScaling) - 10 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, (((128) >> bitShiftScaling) + 4) >> UpScaleResolutionX, (((24) >> bitShiftScaling) + 4) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);		
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		glViewport((24 - 8 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (168 >> bitShiftScaling) - 8 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, (((128) >> bitShiftScaling)) >> UpScaleResolutionX, (((24) >> bitShiftScaling)) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);		
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(0.0f));
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		float lerpParam = InverseLerp(MoveSpeed.x, -1.0f, 0.925f);
		float rainbowMixParam = InverseLerp(MoveSpeed.x, 0.925f, 1.000f);
		int accelerateWidth = (((128) >> bitShiftScaling) - 4) >> UpScaleResolutionX;
		accelerateWidth = Lerp(accelerateWidth / 3, accelerateWidth, lerpParam);

		satColor speedColor;
		if(lerpParam > 0.925f)
		{
			speedColor = Lerp(
				rainbowArray[(rainbowNum + 2) % rainbowArraySize], 
				rainbowArray[(rainbowNum + 3) % rainbowArraySize], rainbowNumInterp);
			
			speedColor = Lerp(sat::color::green, speedColor, rainbowMixParam);
		}
		else if(lerpParam > 0.5f)
		{
			speedColor = Lerp(sat::color::yellow, sat::color::green, lerpParam * 2 - 1);
		}
		else
		{
			speedColor = Lerp(sat::color::red, sat::color::yellow, lerpParam * 2);
		}

		//speedColor = Lerp(sat::color::red, sat::color::green, lerpParam);

		glViewport(
			(24 - 6 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))) >> UpScaleResolutionX, 
			(WINDOW_HEIGHT - (168 >> bitShiftScaling) - 6 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, 
			accelerateWidth, 
			(((24) >> bitShiftScaling) - 4) >> UpScaleResolutionY		);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);		
		PassThroughFlipMultShader.sendUniform("uMultVec", speedColor);
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		glViewport((24 - 8 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (64 >> bitShiftScaling) - 8 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, (((32 + 32 + 256) >> bitShiftScaling)) >> UpScaleResolutionX, (((32 + 16) >> bitShiftScaling)) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();		
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(0.0f));
		PassThroughFlipMultShader.sendUniform("uAddVec", satColor(EnemyScoreColorBoost));
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();
		
		glViewport((24 - 8 + ((int)(ThreeDeeOffset.x + randomOffsetBottom.x))) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - 8 + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, (((32 + 32 + 256 - 64) >> bitShiftScaling)) >> UpScaleResolutionX, (((32 + 16) >> bitShiftScaling)) >> UpScaleResolutionY);
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uAddVec", satColor(0.0f));
		ActiveFrameBuffer->bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		PassThroughFlipMultShader.sendUniform("uAddVec", satColor(EnemyScoreColorBoost));

		//if (activeCamera == &leftCamera)
		//{
		//	ThreeDeeOffset.x = (int)(-6 - Timer::sin(3.0f) * 2.1) >> bitShiftScaling;
		//}
		//elf(activeCamera == &rightCamera)
		//{
		//	ThreeDeeOffset.x = (int)(+6 + Timer::sin(3.0f) * 2.1) >> bitShiftScaling;
		//}

		if (activeCamera == &leftCamera)
		{
			ThreeDeeOffset.x = -(6 >> bitShiftScaling) - 2;
			ThreeDeeOffset.y = (2 >> bitShiftScaling);
		}
		elf(activeCamera == &rightCamera)
		{
			ThreeDeeOffset.x = (6 >> bitShiftScaling) - 2;
			ThreeDeeOffset.y = (2 >> bitShiftScaling);
		}
		
		if (timeContrastNegative > 0.0f)
		{
			//ThreeDeeOffset.x -= randomOffset.x;
			//ThreeDeeOffset.y = -randomOffset.y;
		}
		else
		{
			randomOffsetTop = 0.0f;
			randomOffsetBottom = 0.0f;
		}

		
		float scaleHighScoreColor = 0.5f;


		satColor scoreMultColor = 1.0f;
		satColor darkZero = 1.0f;
		bool darkZeroActive = false;

		if (scoreDisplay >= maxScoreDisplay)
		{
			scoreMultColor = (satColor(1.0f, 1.0f, 1.0f) + EnemyScoreColorBoost);
		}
		elf (scoreDisplayCompare > ScoreBoard.hiScore * 100000.0f)
		{
			rainbow = tru;
			scoreMultColor = (satColor(1.0f, 1.0f, 1.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * 1.15f)
		{
			rainbow = tru;
			scoreMultColor = (satColor(0.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * 1.10f)
		{
			scoreMultColor = ( satColor(0.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * 1.05f)
		{
			scoreMultColor = (satColor(0.25f, 1.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * 1.0f)
		{
			scoreMultColor = (satColor(0.5f, 1.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor)
		{
			scoreMultColor = (satColor(1.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor * scaleHighScoreColor)
		{
			scoreMultColor = (satColor(1.0f, 0.75f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
		{
			scoreMultColor = (satColor(1.0f, 0.5f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
		{
			scoreMultColor = (satColor(1.0f, 0.25f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
		{
			scoreMultColor = (satColor(1.0f, 0.0f, 0.0f) + EnemyScoreColorBoost);
		}
		elf(scoreDisplayCompare > ScoreBoard.hiScore * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
		{
			scoreMultColor = (Lerp(satColor(1.0f, 0.0f, 0.0f), satColor(0.5f, 0.0f, 0.0f), Timer::sinP(0.0f, TimeInLevel * curLevel->SongTempo / 60.0f * satMath::PI)) + EnemyScoreColorBoost);
		}
		else
		{
			scoreMultColor = (Lerp(satColor(1.0f, 0.0f, 0.0f), satColor(0.25f, 0.0f, 0.0f), Timer::sinP(0.0f, TimeInLevel * curLevel->SongTempo / 60.0f * satMath::PI)) + EnemyScoreColorBoost);
		}


		int c = 0;
		long long k = scoreDisplay;
		for(int i = 100000000; i >= 1 ; i /= 10)
		{			
			glViewport(((288 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetTop.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (64 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

			PassThroughFlipMultShader.bind();
			PassThroughFlipMultShader.sendUniform("uSceneTex", 0);

			if (k <= 0 && !darkZeroActive && i < 100000000)
			{
				darkZero *= 0.25f;
				darkZeroActive = tru;
			}

			if(rainbow)
			{
				PassThroughFlipMultShader.sendUniform("uMultVec", darkZero * Lerp(
					rainbowArray[rainbowNum % rainbowArraySize], 
					rainbowArray[(rainbowNum + 1) % rainbowArraySize], rainbowNumInterp) + EnemyScoreColorBoost);
				++rainbowNum;
			}
			else
			{
				PassThroughFlipMultShader.sendUniform("uMultVec", darkZero * scoreMultColor);
			}
			ActiveFrameBuffer->bind();
			if(k > 0)
			{
				glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, NumberTex[0].TexObj);
			}

			

			FrameBuffer::DrawFullScreenQuad();

			FrameBuffer::unbind();
			ShaderProgram::unbind();
			c += HudDigitOffset;
			k /= 10;
		}


		
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f, 1.0f, 1.0f));
		PassThroughFlipMultShader.sendUniform("uAddVec", satColor(0.0f));

		int seconds = (int)(timeDisplay * 10) % 10;
		k = seconds;
		c = HudDigitOffset;

		glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		ActiveFrameBuffer->bind();
		glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
		FrameBuffer::DrawFullScreenQuad();



		c += HudDigitOffset;
		glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		ActiveFrameBuffer->bind();
		glBindTexture(GL_TEXTURE_2D, NumberPeriodTex.TexObj);
		FrameBuffer::DrawFullScreenQuad();

		seconds = (int)timeDisplay % 60;
		k = seconds;
		c += HudDigitOffset;

		for(int i = 60; i >= 1 ; i /= 10)
		{
			glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

			PassThroughFlipMultShader.bind();
			PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
			ActiveFrameBuffer->bind();
			glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
			FrameBuffer::DrawFullScreenQuad();

			c += HudDigitOffset;
			k /= 10;
		}



		glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		if ((int)(timeDisplay * 10) % 10 >= 5 && timeDisplay < maxTimeDisplay - 0.2)
			PassThroughFlipMultShader.sendUniform("uMultVec", satColor(0.25f));
		ActiveFrameBuffer->bind();
		glBindTexture(GL_TEXTURE_2D, NumberColonTex.TexObj);
		FrameBuffer::DrawFullScreenQuad();

		

		c += HudDigitOffset;
		k = (int)timeDisplay / 60.0f;

		glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f, 1.0f, 1.0f));
		ActiveFrameBuffer->bind();
		glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
		FrameBuffer::DrawFullScreenQuad();

		c += HudDigitOffset;
		k /= 10;

		glViewport(((256 >> bitShiftScaling) + (8 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetBottom.x)) >> UpScaleResolutionX, (WINDOW_HEIGHT - (128 >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetBottom.y))) >> UpScaleResolutionY, 32 >> (bitShiftScaling + UpScaleResolutionX), 32 >> (bitShiftScaling + UpScaleResolutionY));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		ActiveFrameBuffer->bind();
		glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
		FrameBuffer::DrawFullScreenQuad();
	
		
	


		glBindTexture(GL_TEXTURE_2D, GL_NONE);
		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);


	}

	glViewport(0, 0, WINDOW_WIDTH >> UpScaleResolutionX, WINDOW_HEIGHT >> UpScaleResolutionY);



#pragma endregion DisplayHUD


#pragma region VHSPost

	if (postProcessPauseVHSActive && (PauseActive))
	{

		PauseVHSPostShader.bind();
		PauseVHSPostShader.sendUniform("uSceneTex", 0);
		PauseVHSPostShader.sendUniform("uGrainTex", 1);

		PauseVHSPostShader.sendUniform("uGrain", satVec4(colorGrain, (rand() % 100) * 0.05f));
		PauseVHSPostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH, WINDOW_HEIGHT));
		PauseVHSPostShader.sendUniform("uGrainTexMult", satVec2(0.25f, Timer::sinRange(1000.0f, 1.0f, 0.99f)));

		//PauseVHSPostShader.sendUniform("uAmount", Timer::sinRange(1.5f, 0.01f, 0.1f)); //0.09
		PauseVHSPostShader.sendUniform("uSlideAmount", Timer::sinRange(40.5f, 0.01f, 0.015f)); //0.09

		PauseVHSPostShader.sendUniform("uTime", (float)Timer::current);
		

		ActiveFrameBuffer->bind();

		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

		VHSGrainTexture.bind(1);

		FrameBuffer::DrawFullScreenQuad();


		Texture::unbind(1);
		Texture::unbind(0);

		FrameBuffer::unbind();
		ShaderProgram::unbind();

		ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
	}
#pragma endregion VHSPost


	if (SatDebugDisplayMode == SAT_DEBUG_OUTPUT_SCENE)
	{
		if(activeCamera == &leftCamera)
		{
			glViewport(0, 0, WINDOW_WIDTH >> UpScaleResolutionX, WINDOW_HEIGHT >> UpScaleResolutionY);
			PassThroughShader.bind();
			PassThroughShader.sendUniform("uSceneTex", 0);

			LeftEyeBuffer.bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
		}
		else if(activeCamera == &rightCamera)
		{
			glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

			ThreeDeePostShader.bind();
			ThreeDeePostShader.sendUniform("uLeftTex", 0);
			ThreeDeePostShader.sendUniform("uRightTex", 1);
			ThreeDeePostShader.sendUniform("uInterlaceTex", 2);
			
			ThreeDeePostShader.sendUniform("uResolution", satVec2(WINDOW_WIDTH, WINDOW_HEIGHT));
			ThreeDeePostShader.sendUniform("uPixelSize", satVec2(1.0 / WINDOW_WIDTH, 1.0 / WINDOW_HEIGHT));

			FullScreenOutput.bind();

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, LeftEyeBuffer.GetColorHandle(0));
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
			glActiveTexture(GL_TEXTURE2);
			glBindTexture(GL_TEXTURE_2D, ThreeDeeInterlaceTextureHandle);

			FrameBuffer::DrawFullScreenQuad();

			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			glActiveTexture(GL_TEXTURE1);
			glBindTexture(GL_TEXTURE_2D, GL_NONE);
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, GL_NONE);

			FrameBuffer::unbind();
			ShaderProgram::unbind();

			//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
			if(WinScreenActive)
			{
				displayHighScore();

				//glViewport(0, 0, WINDOW_WIDTH, WINDOW_WIDTH);
				//PassThroughFlipMultShader.bind();
				//PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
				//PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
				//FullScreenOutput.bind();
				//glActiveTexture(GL_TEXTURE0);
				//glBindTexture(GL_TEXTURE_2D, FullScreenOutput.GetColorHandle(0));
				//FrameBuffer::DrawFullScreenQuad();
			}

			FullScreenOutput.MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
		}
		else
		{
		
			if (false && FullScreenActive)
			{
				glViewport(0, 0, FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);

				//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);

				PassThroughShader.bind();
				PassThroughShader.sendUniform("uSceneTex", 0);

				FullScreenOutput.bind();

				glActiveTexture(GL_TEXTURE0);
				glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

				FrameBuffer::DrawFullScreenQuad();

				glBindTexture(GL_TEXTURE_2D, GL_NONE);

				FrameBuffer::unbind();
				ShaderProgram::unbind();


				FullScreenOutput.MoveToBackBuffer(FULLSCREEN_WIDTH, FULLSCREEN_HEIGHT);

				//ActiveFrameBuffer = swapFrameBuffer(ActiveFrameBuffer);
			}
			else
			{
				if(!ThreeDeeActive)
				{
					glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

					PassThroughShader.bind();
					PassThroughShader.sendUniform("uSceneTex", 0);
					
					FullScreenOutput.bind();

					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
					//glBindTexture(GL_TEXTURE_2D, ActiveFrameBuffer->GetColorHandle(0));

					FrameBuffer::DrawFullScreenQuad();

					glBindTexture(GL_TEXTURE_2D, GL_NONE);

					FrameBuffer::unbind();
					ShaderProgram::unbind();

					if (WinScreenActive)
					{
						displayHighScore();

						//glViewport(0, 0, WINDOW_WIDTH, WINDOW_WIDTH);
						//PassThroughFlipMultShader.bind();
						//PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
						//PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
						//FullScreenOutput.bind();
						//glActiveTexture(GL_TEXTURE0);
						//glBindTexture(GL_TEXTURE_2D, FullScreenOutput.GetColorHandle(0));
						//FrameBuffer::DrawFullScreenQuad();
					}

					FullScreenOutput.MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
					//ActiveFrameBuffer->MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
				}
				else
				{
					if(activeCamera == &leftCamera)
					{
						//glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
						//
						//PassThroughShader.bind();
						//PassThroughShader.sendUniform("uSceneTex", 0);
						//
						//FullScreenOutput.bind();
						//
						//glActiveTexture(GL_TEXTURE0);
						//glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));
						//
						//FrameBuffer::DrawFullScreenQuad();
						//
						//glBindTexture(GL_TEXTURE_2D, GL_NONE);
						//
						//FrameBuffer::Unbind();
						//ShaderProgram::unbind();
						//
						//FullScreenOutput.MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
						////ActiveFrameBuffer->MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
					}
					else
					{
						glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);

						PassThroughShader.bind();
						PassThroughShader.sendUniform("uSceneTex", 0);
						
						FullScreenOutput.bind();

						glActiveTexture(GL_TEXTURE0);
						glBindTexture(GL_TEXTURE_2D, swapFrameBuffer(ActiveFrameBuffer)->GetColorHandle(0));

						FrameBuffer::DrawFullScreenQuad();

						glBindTexture(GL_TEXTURE_2D, GL_NONE);

						FrameBuffer::unbind();
						ShaderProgram::unbind();

						FullScreenOutput.MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
						//ActiveFrameBuffer->MoveToBackBuffer(WINDOW_WIDTH, WINDOW_HEIGHT);
					}
				}
			}
		}
	}
	else
	{
		displayDebugFrame();
	}

}

void Game::displayTitleScreen()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	EdgeMap.Clear();

	glClearColor(0.0f, 0.0f, 0.0f, 0);

	GBuffer.Clear();
	DeferredComposite.Clear();
	FinalComposite.Clear();

	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();

	satMat4 tempMatrix[3];
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);

	float bobSpeedTS = 6.0f;

	tempMatrix[0].LoadIdentity();
	tempMatrix[1].LoadIdentity();
	tempMatrix[2].LoadIdentity();
	
	tempMatrix[0].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[1].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[2].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	
	tempMatrix[0].RotateY(20.0f * Between(ControllerInputJoypadLeftStick.x, -1.0f, 1.0f));
	tempMatrix[1].RotateY(20.0f * Between(ControllerInputJoypadLeftStick.x, -1.0f, 1.0f));
	tempMatrix[2].RotateY(20.0f * Between(ControllerInputJoypadLeftStick.x, -1.0f, 1.0f));
	tempMatrix[0].RotateX(20.0f * Between(ControllerInputJoypadLeftStick.y, -1.0f, 1.0f));
	tempMatrix[1].RotateX(20.0f * Between(ControllerInputJoypadLeftStick.y, -1.0f, 1.0f));
	tempMatrix[2].RotateX(20.0f * Between(ControllerInputJoypadLeftStick.y, -1.0f, 1.0f));

	tempMatrix[2].RotateZ(Timer::sin(bobSpeedTS * 0.5f));

	tempMatrix[0].Translate(0.0f, 0.0f, 13.0f + 0.0f * -ControllerInputTrigger);
	tempMatrix[1].Translate(0.0f, 0.0f, 5.00f + 6.0f * -ControllerInputTrigger);
	tempMatrix[2].Translate(0.0f, 0.0f, 2.00f + 8.0f * -ControllerInputTrigger + Timer::sinRange(bobSpeedTS, 0.0f, 2.0f));

	GBuffer.bind();

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	
	if(ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}

	drawAnimation();
	drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen, &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
	drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &TitleScreenBackgroundTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
	//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen,  &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
	
	toonShading();
	deferredRender();
	postProcessing();

	if(ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		glClearColor(0.0f, 0.0f, 0.0f, 0);
		DeferredComposite.Clear();
		FinalComposite.Clear();
		GBuffer.Clear();

		drawAnimation();
		//drawModel(&sat::light::mod::titleScreen, &TitleScreen, &TitleScreenBackgroundTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
		drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen, &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
		drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &TitleScreenBackgroundTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
		//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen,  &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
		
		
		toonShading();
		deferredRender();
		postProcessing();
	}
	
}

void Game::displaySpecialThanksScreen()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	EdgeMap.Clear();

	glClearColor(0.0f, 0.0f, 0.0f, 0);

	GBuffer.Clear();
	DeferredComposite.Clear();
	FinalComposite.Clear();

	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();
		
	satMat4 tempMatrix[3];
	glViewport(0, 0, WINDOW_WIDTH >> UpScaleResolutionX, WINDOW_HEIGHT >> UpScaleResolutionY);



	tempMatrix[0].LoadIdentity();
	tempMatrix[1].LoadIdentity();
	tempMatrix[2].LoadIdentity();
	
	tempMatrix[0].TranslateInstance(satVec3(0.0f, 0.0f, 30.0f + 5.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[1].TranslateInstance(satVec3(0.0f, 0.0f, 30.0f + 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[2].TranslateInstance(satVec3(0.0f, 0.0f, 30.0f - 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));

	//tempMatrix[0].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 2.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	//tempMatrix[1].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	//tempMatrix[2].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, -2.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));


	GBuffer.bind();

	//glActiveTexture(GL_TEXTURE1);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());

	if (ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}

	//drawAnimation();
	drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreenSmall, &SpecialThanksScreenAlphaTexture, &SpecialThanksScreenTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
	drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &SpecialThanksScreenBackgroundTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
	//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen,  &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);

	toonShading();
	deferredRender();
	postProcessing();

	if (ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		glClearColor(0.0f, 0.0f, 0.0f, 0);
		DeferredComposite.Clear();
		FinalComposite.Clear();
		GBuffer.Clear();

		//drawAnimation();
		drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreenSmall, &SpecialThanksScreenAlphaTexture, &SpecialThanksScreenTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
		drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &SpecialThanksScreenBackgroundTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
		

		toonShading();
		deferredRender();
		postProcessing();
	}
}

void Game::displayLoadingScreen()
{
	//glClearColor(0.0f, 0.0f, 0.0f, 0);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	EdgeMap.Clear();
	glClearColor(0.0f, 0.0f, 0.0f, 0);

	GBuffer.Clear();
	DeferredComposite.Clear();
	FinalComposite.Clear();

	//glClearColor(0.25f, 0.25f, 0.25f, 0);
	//glClearColor(0.0f, 0.0f, 0.0f, 0);
	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();

	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Render The Shadow Map //
	//glViewport(0, 0, SHADOW_RESOLUTION, SHADOW_RESOLUTION); //Set up the viewport (consider putting this in the framebuffer class!)
	
	satMat4 tempMatrix;
	//tempMatrix.LoadIdentity();
	//tempMatrix.Translate(initialTranslation);
	//tempMatrix.RotateX(-90.0f);	

	//StaticGeometry.unbind(); //Unbind the shader so we don't accidentally overwrite data.
	//// Render The Scene //
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);

	tempMatrix.LoadIdentity();
	//tempMatrix.Translate(initialTranslation + satVec3(-8.0f, -2.5f, 5.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix.Translate(initialTranslation + satVec3(-8.0f, -2.5f - initialTranslation.y, 5.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	
	//tempMatrix.RotateX(-90.0f);
	float t = (adjust.x * 8.0f - 4050.0f) / 50.0f;
	//float tempLerp = Lerp(0.0f, 1.0f, t);



	//drawModel(&sat::light::mod::titleScreen, &TitleScreen, &NowLoading, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
	//toonShading();
	//deferredRender();
	//postProcessing();



	if(ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}
	drawModel(&sat::light::mod::titleScreen, &TitleScreen, &NowLoading, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
	toonShading();
	deferredRender();
	postProcessing();

	if(ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		DeferredComposite.Clear();
		FinalComposite.Clear();
		GBuffer.Clear();

		drawModel(&sat::light::mod::titleScreen, &TitleScreen, &NowLoading, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
		toonShading();
		deferredRender();
		postProcessing();
	}

	//Swap Buffers and Render//
	//glutSwapBuffers();
	//glfwSwapBuffers(window);
}


void Game::displayHighScore()
{
	satVec2 ThreeDeeOffset = 0;
	int HudDigitOffset = 48;
	int bitShiftScaling = 0;

	if (WINDOW_HEIGHT < 750)
	{
		bitShiftScaling = 1;
		HudDigitOffset = HudDigitOffset >> 1;
	}


	float chance = randomFloats(generator);
	satVec2 randomOffsetTop = satVec2(randomFloats(generator) * 6.0f - 3.0f, randomFloats(generator) * 4.0f - 2.0f);
	//satVec2 randomOffsetBottom = satVec2(randomFloats(generator) * 6.0f - 3.0f, randomFloats(generator) * 4.0f - 2.0f);

	randomOffsetTop *= 0.0f;

	



	float fractpart, intpart;
	float EnemyScoreColorBoost = Lerp(0.0f, 1.0f, InverseLerp(timeContrastNegative, 0.0f, 0.25f));
	bool rainbow = false;
	float rainbowNumInterp = Timer::currentf() * 7.5f;
	rainbowNumInterp = modf(rainbowNumInterp, &intpart);
	int rainbowNum = intpart;
	float scaleHighScoreColor = 0.5f;

	for(int f = 1; f <= 10; ++f)
	{
		glViewport((24 - 10 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))), (WINDOW_HEIGHT - ((40 + HudDigitOffset + (HudDigitOffset * 13 / 9) * f) >> bitShiftScaling) - 10 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), (((HudDigitOffset * 9 + 32) >> bitShiftScaling) + 4), (((HudDigitOffset + 16) >> bitShiftScaling) + 4));
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
		FullScreenOutput.bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();

		glViewport((24 - 8 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))), (WINDOW_HEIGHT - ((40 + HudDigitOffset + (HudDigitOffset * 13 / 9) * f) >> bitShiftScaling) - 8 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), (((HudDigitOffset * 9 + 32) >> bitShiftScaling)), (((HudDigitOffset + 16) >> bitShiftScaling)));
		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(0.0f));
		FullScreenOutput.bind();
		NumberBlankTex.bind(0);
		FrameBuffer::DrawFullScreenQuad();
	}

	glViewport((24 - 10 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))), (WINDOW_HEIGHT - ((32 + HudDigitOffset) >> bitShiftScaling) - 10 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), (((HudDigitOffset * 9 + 32) >> bitShiftScaling) + 4), (((HudDigitOffset + 16) >> bitShiftScaling) + 4));
	PassThroughFlipMultShader.bind();
	PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
	//PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
	PassThroughFlipMultShader.sendUniform("uMultVec", Lerp(
		rainbowArray[rainbowNum % rainbowArraySize],
		rainbowArray[(rainbowNum + 1) % rainbowArraySize], rainbowNumInterp) + EnemyScoreColorBoost);
	FullScreenOutput.bind();
	NumberBlankTex.bind(0);
	FrameBuffer::DrawFullScreenQuad();

	glViewport((24 - 8 + ((int)(ThreeDeeOffset.x + randomOffsetTop.x))), (WINDOW_HEIGHT - ((32 + HudDigitOffset) >> bitShiftScaling) - 8 + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), (((HudDigitOffset * 9 + 32) >> bitShiftScaling)), (((HudDigitOffset + 16) >> bitShiftScaling)));
	PassThroughFlipMultShader.bind();
	PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
	PassThroughFlipMultShader.sendUniform("uMultVec", satColor(0.0f));
	FullScreenOutput.bind();
	NumberBlankTex.bind(0);
	FrameBuffer::DrawFullScreenQuad();
	
	satColor scoreMultColor = 1.0f;
	satColor darkZero = 1.0f;
	bool darkZeroActive = false;

	if (totalHighScore >= highestTotalHighScore[0] * 100000.0f)
	{
		scoreMultColor = (satColor(1.0f, 1.0f, 1.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * 1.15f)
	{
		rainbow = tru;
		scoreMultColor = (satColor(0.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * 1.10f)
	{
		scoreMultColor = (satColor(0.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * 1.05f)
	{
		scoreMultColor = (satColor(0.25f, 1.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * 1.0f)
	{
		scoreMultColor = (satColor(0.5f, 1.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor)
	{
		scoreMultColor = (satColor(1.0f, 1.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor * scaleHighScoreColor)
	{
		scoreMultColor = (satColor(1.0f, 0.75f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
	{
		scoreMultColor = (satColor(1.0f, 0.5f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
	{
		scoreMultColor = (satColor(1.0f, 0.25f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
	{
		scoreMultColor = (satColor(1.0f, 0.0f, 0.0f) + EnemyScoreColorBoost);
	}
	elf(totalHighScore >= highestTotalHighScore[0] * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor * scaleHighScoreColor)
	{
		scoreMultColor = (Lerp(satColor(1.0f, 0.0f, 0.0f), satColor(0.5f, 0.0f, 0.0f), Timer::sinP(8.0f)) + EnemyScoreColorBoost);
	}
	else
	{
		scoreMultColor = (Lerp(satColor(1.0f, 0.0f, 0.0f), satColor(0.25f, 0.0f, 0.0f), Timer::sinP(8.0f)) + EnemyScoreColorBoost);
	}



	int c = 0;
	//int k 
	//totalHighScore
	//highestTotalHighScore

	darkZero = 1.0f;
	darkZeroActive = false;
	long long k = totalHighScore;
	for (int i = 100000000; i >= 1; i /= 10)
	{
		glViewport((((HudDigitOffset * 9 - HudDigitOffset / 4) >> bitShiftScaling) + (HudDigitOffset * 3 / 4 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetTop.x)), (WINDOW_HEIGHT - ((32 + HudDigitOffset) >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), HudDigitOffset >> (bitShiftScaling), HudDigitOffset >> (bitShiftScaling));

		PassThroughFlipMultShader.bind();
		PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
		
		if (k <= 0 && !darkZeroActive && i < 100000000)
		{
			darkZero *= 0.25f;
			darkZeroActive = tru;
		}

		if (rainbow)
		{
			PassThroughFlipMultShader.sendUniform("uMultVec", darkZero * Lerp(
				rainbowArray[rainbowNum % rainbowArraySize],
				rainbowArray[(rainbowNum + 1) % rainbowArraySize], rainbowNumInterp) + EnemyScoreColorBoost);
			++rainbowNum;
		}
		FullScreenOutput.bind();
		if (k > 0)
		{
			glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
		}
		else
		{
			glBindTexture(GL_TEXTURE_2D, NumberTex[0].TexObj);
		}

		PassThroughFlipMultShader.sendUniform("uMultVec", satColor(darkZero) * scoreMultColor);

		FrameBuffer::DrawFullScreenQuad();

		FrameBuffer::unbind();
		ShaderProgram::unbind();
		c += HudDigitOffset >> bitShiftScaling;
		k /= 10;
	}

	
	for (int f = 1; f <= 10; ++f)
	{
		c = 0;
		darkZero = 1.0f;
		darkZeroActive = false;
		k = highestTotalHighScore[f-1];
		for (int i = 100000000; i >= 1; i /= 10)
		{
			glViewport((((HudDigitOffset * 9 - HudDigitOffset / 4) >> bitShiftScaling) + (HudDigitOffset * 3 / 4 * bitShiftScaling) - c + (int)(ThreeDeeOffset.x + randomOffsetTop.x)), (WINDOW_HEIGHT - ((40 + HudDigitOffset + (HudDigitOffset * 13 / 9) * f) >> bitShiftScaling) - (4 * bitShiftScaling) + ((int)(ThreeDeeOffset.y + randomOffsetTop.y))), HudDigitOffset >> (bitShiftScaling), HudDigitOffset >> (bitShiftScaling));

			if (k <= 0 && !darkZeroActive)
			{
				darkZero *= 0.25f;
				darkZeroActive = tru;
			}

			PassThroughFlipMultShader.bind();
			PassThroughFlipMultShader.sendUniform("uSceneTex", 0);
			//PassThroughFlipMultShader.sendUniform("uMultVec", satColor(1.0f));
			//if (rainbow)
			//{
			//	PassThroughFlipMultShader.sendUniform("uMultVec", Lerp(
			//		rainbowArray[rainbowNum % rainbowArraySize],
			//		rainbowArray[(rainbowNum + 1) % rainbowArraySize], rainbowNumInterp) + EnemyScoreColorBoost);
			//	++rainbowNum;
			//}
			FullScreenOutput.bind();
			if (k > 0)
			{
				glBindTexture(GL_TEXTURE_2D, NumberTex[(int)k % 10].TexObj);
				PassThroughFlipMultShader.sendUniform("uMultVec", darkZero);
			}
			else
			{
				glBindTexture(GL_TEXTURE_2D, NumberTex[0].TexObj);
				PassThroughFlipMultShader.sendUniform("uMultVec", darkZero);
			}

			FrameBuffer::DrawFullScreenQuad();

			FrameBuffer::unbind();
			ShaderProgram::unbind();
			c += HudDigitOffset >> bitShiftScaling;
			k /= 10;
		}
	}
}

void Game::displayWinScreen()
{
	glClearColor(1.0f, 1.0f, 1.0f, 0);
	EdgeMap.Clear();
	glClearColor(0.0f, 0.0f, 0.0f, 0);

	GBuffer.Clear();
	DeferredComposite.Clear();
	FinalComposite.Clear();

	ShadowMap.Clear();
	WorkBuffer1_2.Clear();
	WorkBuffer2_2.Clear();
	WorkBuffer1_4.Clear();
	WorkBuffer2_4.Clear();
	WorkBuffer1_8.Clear();
	WorkBuffer2_8.Clear();

	//satMat4 tempMatrix;

	//// Render The Scene //
	glViewport(0, 0, WINDOW_WIDTH  >> UpScaleResolutionX, WINDOW_HEIGHT  >> UpScaleResolutionY);

	//tempMatrix.LoadIdentity();
	
	static float frameNumber = 0.0f;
	glActiveTexture(GL_TEXTURE0);
	//Texture tempTexture;	
	//VideoUnload(tempU);
	//GLuint tempU = ReturnAVIFrame((int)frameNumber);
	aviHandle = ReturnAVIFrame(frameNumber + 300);
	aviTexture.TexObj = aviHandle;
	
	if(!PauseActive)
	{
		frameNumber += Timer::framef() * FRAMES_PER_SECOND * 0.5f;
		//++frameNumber;
	}
	//int lastframe=AVIStreamLength(pavi);							// The Last Frame Of The Stream
	int lastframe = ReturnAVILength();
	if (frameNumber + 300 >= lastframe) // || frameNumber + 300 >= 375)	// Are We At Or Past The Last Frame?
	{
		frameNumber = 0;											// Reset The Frame Back To Zero (Start Of Video)
	}
	//drawModel(&sat::light::mod::titleScreen, &TitleScreen, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
	
	
	//tempMatrix.LoadIdentity();
	//tempMatrix.Translate(initialTranslation + satVec3(-8.0f, -2.5f, 5.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	//tempMatrix.RotateX(-90.0f);
	//drawModelCube(&sat::light::mod::tutorial, &TutorialMesh, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, &AmbientCubeMapTexture, 0.0f, tempMatrix);
	
	//drawModel(&sat::light::mod::titleScreen, &TitleScreen, &tempTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix);
	//toonShading();
	//deferredRender();
	//postProcessing();
	satMat4 tempMatrix[3];
	tempMatrix[0].LoadIdentity();
	tempMatrix[1].LoadIdentity();
	tempMatrix[2].LoadIdentity();

	tempMatrix[0].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 7.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[1].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, 0.0f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));
	tempMatrix[2].TranslateInstance(initialTranslation + satVec3(-8.0f, -0.5f - initialTranslation.y, -2.5f));// +satVec3(adjust.x*0.250f - 415, -3.0f, 4.0f));

	satMat4 rotateMatrix;
	rotateMatrix.LoadIdentity();
	//rotateMatrix.RotateX(Timer::currentf() * 10.0f);
	rotateMatrix.RotateY(Timer::sinRange(4.1f, -05.0f, 05.0f));
	rotateMatrix.RotateX(Timer::sinRange(5.3f, -03.5f, 03.5f));
	rotateMatrix.RotateZ(Timer::sinRange(3.5f, -01.5f, 01.5f));
	tempMatrix[0] = rotateMatrix * tempMatrix[0];

	GBuffer.bind();

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_2D, ShadowMap.GetDepthHandle());
	





	if (ThreeDeeActive == false)
	{
		activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &leftCamera;
	}

	
	//drawAnimation();
	//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen, &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
	drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
	//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen,  &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);

	toonShading();
	deferredRender();
	postProcessing();

	if (ThreeDeeActive == false)
	{
		//activeCamera = &mainCamera;
	}
	else
	{
		activeCamera = &rightCamera;

		glClearColor(0.0f, 0.0f, 0.0f, 0);
		DeferredComposite.Clear();
		FinalComposite.Clear();
		GBuffer.Clear();
				
		//drawAnimation();
		//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen, &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);
		drawModel(&sat::light::mod::titleScreen, &TitleScreenLarge, &aviTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[0]);
		//drawModelAlpha(&sat::light::mod::titleScreen, &TitleScreen,  &TitleScreenTextAlphaTexture, &TitleScreenTextTexture, &DefaultNormal, &DefaultShiny, &DefaultEmissive, tempMatrix[2]);


		toonShading();
		deferredRender();
		postProcessing();
	}







	//glViewport(0, 0, WINDOW_WIDTH, WINDOW_HEIGHT);
	//
	//PassThroughShader.bind();
	//PassThroughShader.sendUniform("uSceneTex", 0);
	//
	//FullScreenOutput.bind();
	//
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, tempTexture.TexObj);
	//
	//FrameBuffer::DrawFullScreenQuad();
	//
	//glBindTexture(GL_TEXTURE_2D, GL_NONE);
	//
	//ShaderProgram::unbind();
	//
	//
	//
	//
	//FullScreenOutput.MoveToBackBuffer(WINDOW_WIDTH, WINDOW_WIDTH);
}

void Game::UpdateDistanceTable()
{
	if (DataPoints.size() < 4)
	{
		return;
	}

	//Start from a clean slate
	DistanceTable.clear();

	//Sample all the spline segments
	int tempSegment = 0;
	float totalDistance = 0.0f;

	while (tempSegment <= DataPoints.size() - 4)
	{	/*
		This line adds a redundant row to the table.
		It will ensure we never have a conflict between the gap
		of two different spline segments
		*/
		DistanceTable.push_back(InterpData(totalDistance, 0.0f, tempSegment));

		satVec2 point1(DataPoints[tempSegment + 1]),
			point2(0.0f, 0.0f);

		for (float i = 0.05f; i <= 1.05f; i += 0.05f)
		{
			InterpData tempData; //values are default thanks to constructor "yay constructor!"

			//Get point 2
			point2.x = CatMull(DataPoints[tempSegment].x, DataPoints[tempSegment + 1].x, DataPoints[tempSegment + 2].x, DataPoints[tempSegment + 3].x, i);
			point2.y = CatMull(DataPoints[tempSegment].y, DataPoints[tempSegment + 1].y, DataPoints[tempSegment + 2].y, DataPoints[tempSegment + 3].y, i);
			//point2.x = Bezier(DataPoints[tempSegment].x, DataPoints[tempSegment + 1].x, DataPoints[tempSegment + 2].x, DataPoints[tempSegment + 3].x, i);
			//point2.y = Bezier(DataPoints[tempSegment].y, DataPoints[tempSegment + 1].y, DataPoints[tempSegment + 2].y, DataPoints[tempSegment + 3].y, i);

			//Add to distance table
			tempData.InterpolationParam = i;
			tempData.Segment = tempSegment;

			//Compute Euclidian
			float x = point2.x - point1.x;
			float y = point2.y - point1.y;

			totalDistance += sqrt(x * x + y * y);
			tempData.Distance = totalDistance;

			DistanceTable.push_back(tempData);
			//render
			//drawLine(point1.x, point1.y, point2.x, point2.y);
			//Prepare next step
			point1 = point2;
		}

		tempSegment++;
	}
}

satVec2 Game::DistanceCorrect(float &distance)
{
	if (DataPoints.size() < 4 ||		//Not e nuff points 
		distance < 0.0f)  //Invalid distance
	{
		return satVec2(0.0f, 0.0f);
	}

	if (distance > DistanceTable.back().Distance)
	{
		distance = 0;
		return satVec2(DataPoints[1].x, DataPoints[1].y);
	}

	//if (DataPoints.size() < 4 ||		//Not e nuff points 
	//	distance < 0.0f ||			//Invalid distance
	//	distance > DistanceTable.back().Distance)	//Past the bounds
	//{
	//	return XY_Coordinate(0.0f, 0.0f, 0.0f, 0.0f);
	//}

	if (distance == 0.0f)
	{
		return DataPoints[1];
	}

	//Done checking special cases
	//Loop through the table
	float interpolationParam = 0.0f;
	int segmentNumber = 0;

	for (unsigned i = 1; i < DistanceTable.size(); i++)
	{
		if (distance <= DistanceTable[i].Distance)
		{
			//get our interpolation param
			float percentage = (distance - DistanceTable[i - 1].Distance) /
				(DistanceTable[i].Distance - DistanceTable[i - 1].Distance);

			//This is LERP!
			interpolationParam =
				DistanceTable[i - 1].InterpolationParam * (1.0f - percentage) +
				DistanceTable[i].InterpolationParam * percentage;

			//Capture our segment number
			segmentNumber = DistanceTable[i].Segment;

			if (DistanceTable[i].Segment >= DistanceTable.size())
			{
				std::cout << "WOAH\t";
			}

			break;
		}
	}

	satVec2 result(0.0f, 0.0f);

	result = CatMull(
		DataPoints[segmentNumber],
		DataPoints[segmentNumber + 1],
		DataPoints[segmentNumber + 2],
		DataPoints[segmentNumber + 3],
		interpolationParam);

	//result.y = CatMull(
	//	DataPoints[segmentNumber].y,
	//	DataPoints[segmentNumber + 1].y,
	//	DataPoints[segmentNumber + 2].y,
	//	DataPoints[segmentNumber + 3].y,
	//	interpolationParam);

	return result;
}


void Game::CollisionInvertProcess(FrameBuffer *ActiveFrameBuffer)
{
		
}


void Game::calcCamera()
{	
	satVec2 shift = curLevel->calcEyeShift();

	float yShift = InverseLerp(TotalTranslation.y, 13.0f, -7.0f);
	shift.y += Lerp(eyeShiftShipY, -eyeShiftShipY, yShift);


	float convergence = curLevel->calcEyeConvergence();
	float eyeMult = curLevel->calcEyeDistanceMult();

	float FarMult = 1.0f;

	if (TitleScreenActive || WinScreenActive || SpecialThanksActive)
	{
		shift = 0.0f;
		convergence = 35.0f;
		eyeMult *= 0.8f;
		FarMult = 1.5f;
	}
	
	shiftAverage = Lerp(shiftAverage, shift, Timer::framef() * 3.0f);
	
	satVec2 aspectRatio = { 1.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT };

	if(OrthographicActive)
	{
		mainCamera.Orthographic(
			12.0f * aspectRatio.x, -12.0f * aspectRatio.x, 
			-12.0f * aspectRatio.y, 12.0f * aspectRatio.y, -FarPlane, FarPlane); //Set FOV, Aspect Ratio, near and far clipping planes.
	
		rightCamera.OrthographicStereo(
			12.0f * aspectRatio.x, -12.0f * aspectRatio.x, 
			-12.0f * aspectRatio.y, 12.0f * aspectRatio.y, -FarPlane, FarPlane,
			eyeShift.x + shiftAverage.x + eyeDistance * 0.25f * eyeMult, 0.0f, convergence + eyeConvergenceAdd, eyeConvergenceY);
		leftCamera.OrthographicStereo(
			12.0f * aspectRatio.x, -12.0f * aspectRatio.x, 
			-12.0f * aspectRatio.y, 12.0f * aspectRatio.y, -FarPlane, FarPlane,
			eyeShift.x + shiftAverage.x - eyeDistance * 0.25f * eyeMult, 0.0f, convergence + eyeConvergenceAdd, eyeConvergenceY);
	}
	else
	{
		//float aspectRatio = (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT;
		
		//mainCamera.Perspective(FOV, aspectRatio, 10.0f, 50.0f); //Set FOV, Aspect Ratio, near and far clipping planes.
		mainCamera.PerspectiveStereo(FOV, aspectRatio.y, NearPlane, FarPlane * FarMult, eyeShift.x+shiftAverage.x, shiftAverage.y, convergence + eyeConvergenceAdd, eyeConvergenceY); //Set FOV, Aspect Ratio, near and far clipping planes.
		
		satMat4 tempMat[2];
		tempMat[0].PerspectiveStereoProjection(FOV, aspectRatio.y, NearPlane, FarPlane * FarMult, eyeShift.x + shiftAverage.x, shiftAverage.y, convergence + eyeConvergenceAdd, eyeConvergenceY),
		tempMat[1].OrthoProjection(
				12.0f * aspectRatio.x, -12.0f * aspectRatio.x,
				-12.0f * aspectRatio.y, 12.0f * aspectRatio.y, -FarPlane, FarPlane);
		//mainCamera.projection = 
		//	satMat4::LerpMat4(tempMat[0],
		//		tempMat[1],
		//		Timer::sinRange(1.0f, 0.0f, 1.0f));

		rightCamera.PerspectiveStereo(FOV, aspectRatio.y, NearPlane, FarPlane * FarMult, eyeShift.x+shiftAverage.x+eyeDistance*0.5f*eyeMult, shiftAverage.y, convergence + eyeConvergenceAdd, eyeConvergenceY);
		leftCamera.PerspectiveStereo(FOV, aspectRatio.y, NearPlane, FarPlane * FarMult, eyeShift.x+shiftAverage.x-eyeDistance*0.5f*eyeMult, shiftAverage.y, convergence + eyeConvergenceAdd, eyeConvergenceY);
		
	}

	activeCamera = &mainCamera;
}

void Game::setUpInterlaceTexture()
{
	std::vector<GLubyte> vectorData;
	GLubyte interlace;

	for (GLuint i = 1; i < WINDOW_HEIGHT + 2; ++i)
	{
		interlace = (i % 2) * 255;		
		vectorData.push_back(interlace);
		vectorData.push_back(interlace);
		vectorData.push_back(interlace);
		vectorData.push_back(255);
	}

	

	if (ThreeDeeInterlaceTextureHandle != 0)
	{
		//remove data from GPU
		glDeleteTextures(1, &ThreeDeeInterlaceTextureHandle);
		ThreeDeeInterlaceTextureHandle = 0;
	}
	
	//std::cout << ThreeDeeInterlaceTextureHandle << std::endl;
	
	glGenTextures(1, &ThreeDeeInterlaceTextureHandle);
	glBindTexture(GL_TEXTURE_2D, ThreeDeeInterlaceTextureHandle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, WINDOW_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, &vectorData[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	
	if (ThreeDeeInterlaceInvertTextureHandle != 0)
	{
		//remove data from GPU
		glDeleteTextures(1, &ThreeDeeInterlaceInvertTextureHandle);
		ThreeDeeInterlaceInvertTextureHandle = 0;
	}

	glGenTextures(1, &ThreeDeeInterlaceInvertTextureHandle);
	glBindTexture(GL_TEXTURE_2D, ThreeDeeInterlaceInvertTextureHandle);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 1, WINDOW_HEIGHT, 0, GL_RGBA, GL_UNSIGNED_BYTE, &vectorData[4]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	//std::cout << ThreeDeeInterlaceTextureHandle << std::endl;

}

void Game::setSong()
{
	if(TitleScreenActive)
	{
		for (int i = 0; i < LevelManager::levelFinalNum; ++i)
		{
			if (LevelManager::level[i].Song.isPlaying())
			{
				LevelManager::level[i].Song.stopSound();
			}
		}

		//if (Song.isPlaying())
		//{
		//	Song.stopSound();
		//	//Song.release();
		//}
		//
		if (WinGameSong.isPlaying())
		{
			WinGameSong.stopSound();
		}
		
		SelectSong.playSound();
	
	
	}
	elf(WinScreenActive)
	{
		for (int i = 0; i < LevelManager::levelFinalNum; ++i)
		{
			if (LevelManager::level[i].Song.isPlaying())
			{
				LevelManager::level[i].Song.stopSound();
			}
		}

		if(SelectSong.isPlaying())
		{
			SelectSong.stopSound();
		}
		
		//if (Song.isPlaying())
		//{
		//	Song.stopSound();
		//	Song.release();
		//}
		
		WinGameSong.playSound();
		
	}
	elf(SpecialThanksActive)
	{

	}
	else
	{
		if (SelectSong.isPlaying())
		{
			SelectSong.stopSound();
		}
		
		if (WinGameSong.isPlaying())
		{
			WinGameSong.stopSound();
		}

		//if (Song.isPlaying())
		//{
		//	Song.stopSound();
		//	//Song.release();
		//}

		if(curLevel->SongRepeat == tru)
		{
			//Song.loadFromFile(curLevel->SongFilename, false, tru);
			//Song.playSound();
		}
		if(curLevel->SongRepeat == false)
		{		
			//for (int i = 0; i < LevelManager::levelCurrent; ++i)
			//{
			//	if (LevelManager::level[i].Song.isPlaying())
			//	{
			//		LevelManager::level[i].Song.stopSound();
			//	}
			//}

			if (LevelManager::level[LevelManager::currentSongPlaying].Song.isPlaying())
			{
				LevelManager::level[LevelManager::currentSongPlaying].Song.stopSound();
			}
				
				//if(LevelManager::levelCurrent
			LevelManager::currentSongPlaying = LevelManager::levelCurrent;
			curLevel->Song.playSound();
			
			
			
			
			//if (Song.isPlaying())
			//{
			//	Song.stopSound();
			//	Song.release();
			//}
			//Song.loadFromFile(curLevel->SongFilename, false, tru);
			//Song.playSound();
		}
	}
}
































































































