/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include <string>
#include <memory>
#include "Utilities.h"

class Resource
{
public:
	Resource() = default;
	virtual ~Resource();

	bool HasID(int id);
	int GetID() const;

protected:
	void SetID(std::string const& path);
	void SetID(std::string const& path1, std::string const& path2);
	int ID = 0;
};