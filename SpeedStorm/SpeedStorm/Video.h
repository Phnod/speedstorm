/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Texture.h"

#include <windows.h>											// Header File For Windows
#include <gl\gl.h>												// Header File For The OpenGL32 Library
#include <gl\glu.h>												// Header File For The GLu32 Library
#include <vfw.h>												// Header File For Video For Windows
//#include "NeHeGL.h"												// Header File For NeHeGL

#pragma comment( lib, "opengl32.lib" )							// Search For OpenGL32.lib While Linking
#pragma comment( lib, "glu32.lib" )								// Search For GLu32.lib While Linking
#pragma comment( lib, "vfw32.lib" )								// Search For VFW32.lib While Linking

typedef struct {									// Contains Information Vital To Applications
	HINSTANCE		hInstance;						// Application Instance
	const char*		className;						// Application ClassName
} Application;										// Application

typedef struct {									// Window Creation Info
	Application*		application;				// Application Structure
	char*				title;						// Window Title
	int					width;						// Width
	int					height;						// Height
	int					bitsPerPixel;				// Bits Per Pixel
	BOOL				isFullScreen;				// FullScreen?
} GL_WindowInit;									// GL_WindowInit

typedef struct {									// Contains Information Vital To A Window
	HWND				hWnd;						// Window Handle
	HDC					hDC;						// Device Context
	HGLRC				hRC;						// Rendering Context
	GL_WindowInit		init;						// Window Init
	BOOL				isVisible;					// Window Visible?
	DWORD				lastTickCount;				// Tick Counter
} GL_Window;										// GL_Window


void OpenAVI(LPCWSTR szFile);
void OpenAVI(const std::string& s);

void GrabAVIFrame(int frame);									// Grabs A Frame From The Stream
GLuint ReturnAVIFrame(int frame);	

int ReturnAVILength();

void VideoUnload(GLuint& TexObj);
void CloseAVI(void);											// Properly Closes The Avi File

//void GrabAVIFrame(int frame);									// Grabs A Frame From The Stream

BOOL Initialize (const std::string& s);
BOOL Initialize(LPCWSTR szFile)	;
BOOL Initialize ()	;
void Deinitialize (void);										// Any User DeInitialization Goes Here


void Update (DWORD milliseconds);								// Perform Motion Updates Here



