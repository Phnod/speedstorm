/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

/*

SpeedStorm 2016(C) Stephen Thompson & Jacob Robart

*/

#pragma once
#include <vector>
#include "Math.h"
#include "Texture.h"
#include "Mesh.h"
#include "Light.h"

enum ShipAsset
{
	diffuseTexture,
	normalTexture,
	specularTexture,
	emissiveTexture,
	emissiveInvincibleTexture,
	meshModel
};

class Ship
{
	Ship();

	void LoadAssets();
	
	void setMaxHealth(const int &);
	void setRateOfDecay(const int &);

	void InitializeSizeOfAssets(); //Call this before setting any filenames 

	void setDiffuseFilename		(const int &index, const std::string &name);
	void setNormalFilename		(const int &index, const std::string &name);
	void setSpecularFilename	(const int &index, const std::string &name);
	void setEmissiveFilename	(const int &index, const std::string &name);
	void setInvincibleFilename	(const int &index, const std::string &name);
	void setMeshFilename		(const int &index, const std::string &name);

	//These functions will set the filenames for the current index and copy over to 'end' amount sequential elements
	void setCopyAfterDiffuseFilename	(const int &index, const int &end, const std::string &name);
	void setCopyAfterNormalFilename		(const int &index, const int &end, const std::string &name);
	void setCopyAfterSpecularFilename	(const int &index, const int &end, const std::string &name);
	void setCopyAfterEmissiveFilename	(const int &index, const int &end, const std::string &name);
	void setCopyAfterInvincibleFilename	(const int &index, const int &end, const std::string &name);
	void setCopyAfterMeshFilename		(const int &index, const int &end, const std::string &name);
	
	//Experimental, will prevent a lot of copy pasting
	bool setFilename			(const enum ShipAsset type, const int &index, const std::string &name);
	bool setCopyAfterFilename	(const enum ShipAsset type, const int &index, const int &end, const std::string &name);

	int maxShipHealth; //This determines the size of the asset arrays (1 for each point of health)
	int numberOfAssets; //maxShipHealth + 1 (since dead ship is another asset)
	//If you want the same tex

	int shipHealth; //Current health of ship ingame

	std::vector<Texture*> diffuse;
	std::vector<Texture*> normal;
	std::vector<Texture*> specular;
	std::vector<Texture*> emissive;
	std::vector<Texture*> emissiveInvincible;
	std::vector<Mesh*> mesh;

	std::vector<PointLight> pointLight;

	satMat4 rotation;
	satVec3 translation;

	union //filenames for each asset for when loading happens
	{
		struct
		{
			std::vector<std::string> diffuseFilename;
			std::vector<std::string> normalFilename;
			std::vector<std::string> specularFilename;
			std::vector<std::string> emissiveFilename;
			std::vector<std::string> emissiveInvincibleFilename;
			std::vector<std::string> meshFilename;
		};

		struct
		{
			std::vector<std::string> filenameDiffuse;
			std::vector<std::string> filenameNormal;
			std::vector<std::string> filenameSpecular;
			std::vector<std::string> filenameEmissive;
			std::vector<std::string> filenameEmissiveInvincible;
			std::vector<std::string> filenameMesh;
		};
	};
};

//0.70710678118654752440084436210485