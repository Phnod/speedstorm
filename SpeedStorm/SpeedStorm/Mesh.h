/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <string>
#include <vector>
#include <memory>
//#include <GL/glew.h>
#include "IncludeGL.h"
#include "Math.h"
#include "Resource.h"

class Mesh;
typedef std::shared_ptr<Mesh> MeshPtr;

class Mesh : public Resource
{
public:
	Mesh();
	Mesh(std::string meshFile, bool isBinary);
	~Mesh();

	//Load a mesh from file and sent it to memory
	bool LoadFile(const std::string &file);
	bool LoadFromBinary(const std::string &file);
	bool LoadFromObj(const std::string &file);
	//release data from system and GPU memory
	void Unload();

	bool SetupParticle();

	unsigned int GetNumFaces() const;	 //These are worthless since I made it public but whatever
	unsigned int GetNumVertices() const; //These are worthless since I made it public but whatever

	void Bind() const;
	static void Unbind();

	void Draw() const;

	//OpenGL buffer objects
	GLuint VBO_Vert		= 0;
	GLuint VBO_UVs		= 0;
	GLuint VBO_Normals	= 0;
	GLuint VBO_Tangents = 0;
	GLuint VBO_Bitangents = 0;
	GLuint VAO			= 0; //VAO is needed for Modern OpenGL
	std::string _File; //if I want to know what file this was loaded from
	
	LPFILETIME createTime;
	LPFILETIME lastReadTime;
	LPFILETIME lastWriteTime;

	HANDLE handle;

	unsigned int _NumFaces = 0;
	unsigned int _NumVert = 0;

	bool loaded = false;

//private:
//Why the hell would I make anything private?

	//unsigned int _NumFaces = 0;
	//unsigned int _NumVert = 0;
};


class Frame : public Resource
{
public:
	Frame();
	Frame(std::string meshFileA, std::string meshFileB);
	~Frame();

	//Load a mesh from file and sent it to memory
	bool LoadFile(const std::string &fileA, const std::string &fileB);
	//bool LoadFromBinary(const std::string &file);
	//bool LoadFromObj(const std::string &file);
	//release data from system and GPU memory
	void Unload();

	unsigned int GetNumFaces() const;	 //These are worthless since I made it public but whatever
	unsigned int GetNumVertices() const; //These are worthless since I made it public but whatever

	void Bind() const;
	static void Unbind();

	void Draw() const;

	//OpenGL buffer objects
	std::vector<GLuint> VBO_Vert;
	std::vector<GLuint> VBO_UVs;
	std::vector<GLuint> VBO_Normals;
	std::vector<GLuint> VAO; //VAO is needed for Modern OpenGL
	std::string _File; //if I want to know what file this was loaded from

	unsigned int _NumFaces = 0;
	unsigned int _NumVert = 0;

	//private:
	//Why the hell would I make anything private?

	//unsigned int _NumFaces = 0;
	//unsigned int _NumVert = 0;
};
