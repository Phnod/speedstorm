/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <iostream>
//#include <GLFW/glfw3.h>
#include "Math.h"
//#include <chrono>
#include <cstdio>
#include <ctime>

//#include "GL/glut.h"



/*
 * timer class
 * - this lets us keep track of the elapsed time
 *   in our program easily so we can use it in things
 *   like physics calculations 
 */
class Timer
{
public:

	//float currentTime, previousTime, elapsedTime;
	static double current, old, frame;
	
	//typedef std::chrono::high_resolution_clock Clock;

	//double timer, timerOld, timerFrame;

	inline static void Init()
	{	
		//clock_t clockStart;
		//auto t1 = Clock::now();
		//auto t2 = Clock::now();
		//clockStart = clock();
		//old = glfwGetTime();		
		old = ( clock() ) / (double) CLOCKS_PER_SEC;
		//old = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
		tick();
		tick();
	}

	Timer()
	{

	}

	~Timer();

	// update the timer clock
	inline static double tick()
	{		
		//clock_t clockStart;
		current =	( clock() ) / (double) CLOCKS_PER_SEC;
		frame =		Min(current - old, 0.1);
		old = current;
		//printf("Time: %f \n", frame);
		return frame;
	}

	// delta time in milliseconds 
	inline static double getElapsedTimeMS()
	{
		return frame;
	}

	// delta time in seconds
	inline static double getElapsedTimeSeconds()
	{
		return frame / 1000.f;
	}

	inline static double getCurrentTime()
	{
		return current;
	}




	inline static float framef()
	{
		return (float)frame;
	}

	inline static float currentf()
	{
		return (float)current;
	}

	inline static float sin(const float &speed)
	{
		return sinf((float)current * speed);
	}

	inline static float sinP(const float &speed)
	{
		return sinf((float)current * speed) * 0.5f + 0.5f;
	}

	inline static float sinOld(const float &speed)
	{
		return sinf((float)old * speed);
	}

	inline static float sinPOld(const float &speed)
	{
		return sinf((float)old * speed) * 0.5f + 0.5f;
	}

	inline static float sinRange(const float &speed, const float &a = 0.0f, const float &b = 1.0f)
	{
		return LerpU(a, b, sinP(speed));
	}

	inline static float sin(const float &speed, const float &speedAdd)
	{
		return sinf((float)current * speed + speedAdd);
	}

	inline static float sinP(const float &speed, const float &speedAdd)
	{
		return sinf((float)current * speed + speedAdd) * 0.5f + 0.5f;
	}
	
	inline static float sinOld(const float &speed, const float &speedAdd)
	{
		return sinf((float)old * speed + speedAdd);
	}

	inline static float sinPOld(const float &speed, const float &speedAdd)
	{
		return sinf((float)old * speed + speedAdd) * 0.5f + 0.5f;
	}

	inline static satVec3 sinP(const satVec3 &speed, const satVec3 &speedAdd)
	{
		return satVec3(
			sinf((float)current * speed.x + speedAdd.x) * 0.5f + 0.5f,
			sinf((float)current * speed.y + speedAdd.y) * 0.5f + 0.5f,
			sinf((float)current * speed.z + speedAdd.z) * 0.5f + 0.5f);
	}

	inline static satVec3 sinPOld(const satVec3 &speed, const satVec3 &speedAdd)
	{
		return satVec3(
			sinf((float)old * speed.x + speedAdd.x) * 0.5f + 0.5f,
			sinf((float)old * speed.y + speedAdd.y) * 0.5f + 0.5f,
			sinf((float)old * speed.z + speedAdd.z) * 0.5f + 0.5f);
	}

	inline static float sinRange(const float &speed, const float &speedAdd, const float &a, const float &b)
	{
		return LerpU(a, b, sinP(speed, speedAdd));
	}

	inline static satVec3 sinRange(const satVec3 &speed, const satVec3 &speedAdd, const satVec3 &a, const satVec3 &b)
	{
		return satVec3(
			LerpU(a.x, b.x, sinP(speed.x, speedAdd.x)),
			LerpU(a.y, b.y, sinP(speed.y, speedAdd.y)),
			LerpU(a.z, b.z, sinP(speed.z, speedAdd.z)));
	}

	inline static float cos(const float &speed)
	{
		return cosf((float)current * speed);
	}

	inline static float cosP(const float &speed)
	{
		return cosf((float)current * speed) * 0.5f + 0.5f;
	}

	inline static float cosRange(const float &speed, const float &a = 0.0f, const float &b = 1.0f)
	{
		return LerpU(a, b, cosP(speed));
	}

	
	
};
