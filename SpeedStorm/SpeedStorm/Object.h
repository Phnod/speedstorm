/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once


#include <vector>
#include <memory>
#include <string.h>
#include "Math.h"

//Still need to be done
#include "Sound.h"
#include "Light.h"



#include "Texture.h"
#include "Mesh.h"

//#include "Matrix.h"


//#include "Utilities.h"

enum ObjectType
{
	Block = 0,
	Enemy
};

class Visual
{
public:
	Visual()
	{
		diffuse = 0;
		normal = 0;
		specular = 0;
		emissive = 0;
		model = 0;
				
		diffuseFilename = "../Assets/Textures/Default/defaultTexture.png";
		normalFilename = "../Assets/Textures/Default/defaultNormal.png";
		specularFilename = "../Assets/Textures/Default/defaultSpecular.png";
		emissiveFilename = "../Assets/Textures/Default/defaultEmissive.png";
		modelFilename = "../Assets/Models/Default/defaultBlock.obj";
	}

	Visual(const std::string& mesh, 
		const std::string& diffuse = "../Assets/Textures/Default/defaultTexture.png", 
		const std::string& normal = "../Assets/Textures/Default/defaultTexture.png",
		const std::string& specular = "../Assets/Textures/Default/defaultTexture.png",
		const std::string& emissive = "../Assets/Textures/Default/defaultTexture.png");

	Texture*	diffuse;
	Texture*	normal;
	Texture*	specular;
	Texture*	emissive;
	Mesh*		model;

	std::string diffuseFilename;
	std::string normalFilename;
	std::string specularFilename;
	std::string emissiveFilename;
	std::string modelFilename;

	void Load();
	void LoadModel();
	void LoadDiffuse();
	void LoadNormal();
	void LoadSpecular();
	void LoadEmissive();

	void InitializeTextureParam();
	void InitializePrimitiveParam();

	std::string textureStringFilterMag = "GL_LINEAR";
	std::string textureStringFilterMin = "GL_LINEAR_MIPMAP_LINEAR";
	std::string textureStringWrapTexU = "GL_REPEAT"; //GL_CLAMP_TO_EDGE;
	std::string textureStringWrapTexV = "GL_REPEAT"; //GL_CLAMP_TO_EDGE;

	GLuint textureFilterMag = GL_LINEAR;
	GLuint textureFilterMin = GL_LINEAR_MIPMAP_LINEAR;
	GLuint textureWrapTexU = GL_REPEAT; //GL_CLAMP_TO_EDGE;
	GLuint textureWrapTexV = GL_REPEAT; //GL_CLAMP_TO_EDGE;

	std::string primitiveStringPolygonMode = "GL_FILL";
	std::string primitiveStringCullFace = "GL_BACK";
	
	GLuint primitivePolygonMode = GL_FILL;
	GLuint primitiveCullFace = GL_BACK;
	GLuint primitiveShadowPolygonMode = GL_FILL;
	GLuint primitiveShadowCullFace = GL_FRONT;

	GLfloat primitiveLineWidth = 0.0f;
};

class Object
{
public:
	ObjectType type;
	Visual* visual;

	satMat4 position;
	satMat4 rotation;


};

//class VisualRandom : public Visual
//{
//	VisualRandom()
//	{
//		listDiffuse.resize(1);
//		listNormal.resize(1);
//		listSpecular.resize(1);
//		listEmissive.resize(1);
//		listModel.resize(1);
//		
//		listDiffuseFilename.resize(1);
//		listNormalFilename.resize(1);
//		listSpecularFilename.resize(1);
//		listEmissiveFilename.resize(1);
//		listModelFilename.resize(1);
//
//
//
//
//
//		diffuse =  listDiffuse[0];
//		normal =   listNormal[0];
//		specular = listSpecular[0]; 
//		emissive = listEmissive[0]; 
//		model =    listModel[0];
//				
//		diffuseFilename =	listDiffuseFilename[0];
//		normalFilename =	listNormalFilename[0];
//		specularFilename =	listSpecularFilename[0];
//		emissiveFilename =	listEmissiveFilename[0];
//		modelFilename =		listModelFilename[0];
//	}
//	
//	void Load();
//	void LoadModel();
//	void LoadDiffuse();
//	void LoadNormal();
//	void LoadSpecular();
//	void LoadEmissive();
//
//	void InitializeTextureParam();
//	void InitializePrimitiveParam();
//
//	std::vector<Texture*>	listDiffuse;
//	std::vector<Texture*>	listNormal;
//	std::vector<Texture*>	listSpecular;
//	std::vector<Texture*>	listEmissive;
//	std::vector<Mesh*>		listModel;
//
//	std::vector<std::string> listDiffuseFilename;
//	std::vector<std::string> listNormalFilename;
//	std::vector<std::string> listSpecularFilename;
//	std::vector<std::string> listEmissiveFilename;
//	std::vector<std::string> listModelFilename;
//};





















































