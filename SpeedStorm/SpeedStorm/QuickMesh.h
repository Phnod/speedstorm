/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <string>
#include <vector>
#include <GL/glew.h>
#include "Math.h"

class QuickMesh
{
public:
	QuickMesh();
	~QuickMesh();

	//Load a mesh from file and sent it to memory
	bool LoadFromFile(const std::string &file);
	//release data from system and GPU memory
	void Unload();

	unsigned int GetNumFaces() const;
	unsigned int GetNumVertices() const;

	//Create buffer objects for OpenGL.
	GLuint VBO_Vertices = 0;
	GLuint VBO_UVs = 0;
	GLuint VBO_Normals = 0;
	GLuint VAO = 0; //VAO - needed fro ModernGL


	bool ConvertFile(const std::string &file); // does offline conversion for a new, faster file.

private:
	unsigned int _NumFaces = 0;
	unsigned int _NumVertices = 0;


};