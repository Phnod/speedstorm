/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Node.h"

#pragma region Node

Node::Node()
{
	//Nothing
}

Node::~Node()
{
	clearChildren();

	if (parent != nullptr)
	{
		parent->removeChild(this);
		//parent->removeChild(shared_from_this());
	}
}

void Node::addChild(Node* n)
{
	for (unsigned i = 0; i < children.size(); ++i)
	{
		if (n == children[i])
			return;
	}
	children.push_back(n);
	n->parent = this;
	//if I want to go back to shared pointers
	//n->parent = shared_from_this();
}

void Node::removeChild(Node* n)
{
	for (unsigned i = 0; i < children.size(); ++i)
	{
		if (n == children[i])
		{
			//children[i]->parent = nullptr;
			children.erase(children.begin() + i);
		}
	}
}

void Node::removeChild(unsigned int index)
{
	if (index > children.size())
		return;

	//children[index]->parent = nullptr;
	children.erase(children.begin() + index);
}

void Node::clearChildren()
{
	for (unsigned i = 0; i < children.size(); ++i)
	{
		//children[i]->parent = nullptr;
	}
	children.clear();
}

int Node::findChild(Node* n)
{
	for (unsigned int i = 0; i < children.size(); i++)
	{
		if (n == children[i])
		{
			return i;
		}
	}
	return -1;
}

Node* Node::getParent() const
{
	return parent;
}

unsigned int Node::getNumChildren() const
{
	return children.size();
}

void Node::computeWorld(satMat4 parentTransform)
{
	world = parentTransform * local;
	for (unsigned int i = 0; i < children.size(); i++)//for each child
	{
		children[i]->computeWorld(world);
	}
}

void Node::Process()
{
	satMat4 transform = world * local;
	for (unsigned i = 0; i < children.size(); ++i)
	{
		children[i]->setWorld(transform);
		children[i]->Process();
	}
}

satMat4 Node::getWorld() const
{
	return world * local;
}

void Node::setWorld(satMat4 parentTransform)
{
	world = parentTransform;
}


void Node::RotateX(float degreets)
{
	local.RotateX(degreets);
}

void Node::RotateY(float degreets)
{
	local.RotateY(degreets);
}

void Node::RotateZ(float degreets)
{
	local.RotateZ(degreets);
}

void Node::Translate(satVec3 position)
{
	local.Translate(position);
}

void Node::Translate(float x, float y, float z)
{
	local.Translate(x, y, z);
}

void Node::Scale(satVec3 position)
{
	local.Scale(position);
}

void Node::Scale(float x, float y, float z)
{
	local.Scale(x, y, z);
}

#pragma endregion Node

