/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <GL\glew.h>
#include <SFML\Graphics.hpp>
#include <string>
#include <memory>
#include "Vector.h"
#include "Resource.h"
#include "FrameBuffer.h"


//class Texture;
//typedef std::shared_ptr<Texture> TexturePtr;

class Texture : public Resource
{
public:
	Texture() = default;
	Texture(const std::string &file);
	~Texture();

	bool load(const std::string &file);
	bool load(const std::string &file, const float &anisotropyLevel, const GLint &filterMag = GL_LINEAR, const GLint &filterMin = GL_LINEAR_MIPMAP_LINEAR, const GLint &wrapUV = GL_REPEAT);
	GLuint loadCubemap(std::vector<const GLchar*> faces);
	GLuint loadCubemap(const std::string filename);
	void unload();

	void bind() const;
	void bindCube(const int& num);
	void bind(const int& num);
	static void bindFrameColor(const int& textureSlot, const FrameBuffer &framebuffer, const unsigned &index = 0);
	static void unbind();
	static void unbind(const int& num);

	void setAnisotropyLevel(const float level);

	//- The handle to the texture object
	GLuint TexObj = 0;

	float Anisotropy = 4.0f;
	GLint FilterMag = GL_LINEAR;
	GLint FilterMin = GL_LINEAR_MIPMAP_LINEAR;
	GLint WrapTexU = GL_REPEAT; //GL_CLAMP_TO_EDGE;
	GLint WrapTexV = GL_REPEAT; //GL_CLAMP_TO_EDGE;
	//sf::Image tex;
	unsigned int sizeX;
	unsigned int sizeY;
};


inline void Texture::bind() const
{
	glBindTexture(GL_TEXTURE_2D, TexObj);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterMag); //GL_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterMin); //GL_LINEAR_MIPMAP_LINEAR

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, WrapTexU); //u	GL_CLAMP_TO_EDGE
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, WrapTexV); //v	GL_CLAMP_TO_EDGE
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Anisotropy);	//MMM BABY THAT ANISOTROPIC FILTERING
}

inline void Texture::bindCube(const int & num)
{
	glActiveTexture(GL_TEXTURE0 + num);
	glBindTexture(GL_TEXTURE_CUBE_MAP, TexObj);

	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, FilterMag); //GL_LINEAR
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, FilterMin); //GL_LINEAR_MIPMAP_LINEAR
	//
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, WrapTexU); //u	GL_CLAMP_TO_EDGE
	//glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, WrapTexV); //v	GL_CLAMP_TO_EDGE
	//glTexParameterf(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAX_ANISOTROPY_EXT, Anisotropy);
}

inline void Texture::bind(const int& num)
{
	glActiveTexture(GL_TEXTURE0 + num);
	glBindTexture(GL_TEXTURE_2D, TexObj);
	
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, FilterMag); //GL_LINEAR
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, FilterMin); //GL_LINEAR_MIPMAP_LINEAR

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, WrapTexU); //u	GL_CLAMP_TO_EDGE
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, WrapTexV); //v	GL_CLAMP_TO_EDGE
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, Anisotropy);	//MMM BABY THAT ANISOTROPIC FILTERING
}

inline void Texture::bindFrameColor(const int& textureSlot, const FrameBuffer &framebuffer, const unsigned &index)
{
	glActiveTexture(GL_TEXTURE0 + textureSlot);
	glBindTexture(GL_TEXTURE_2D, framebuffer.GetColorHandle(index));
}

inline void Texture::unbind()
{
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
}

inline void Texture::unbind(const int& num)
{
	glActiveTexture(GL_TEXTURE0 + num);
	glBindTexture(GL_TEXTURE_2D, GL_NONE);
}