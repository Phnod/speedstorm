/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Light.h"
#include "ShaderProgram.h"


Light::Light()
{
	ambient =	satColor(0.0f, 0.0f, 0.0f);
	emissive =	satColor(0.0f, 0.0f, 0.0f);
	diffuse =	satColor(1.0f, 1.0f, 1.0f);
	specular =	satColor(1.0f, 1.0f, 1.0f);
}

Light::Light(const satColor &_ambient, const satColor &_emissive, const satColor &_diffuse, const satColor &_specular)
{
	ambient =	_ambient;
	emissive =	_emissive;
	diffuse =	_diffuse;
	specular =	_specular;
}

Light::Light(const satColor &_color)
{
	ambient =	_color * 0.25f;
	emissive =	0.0f;
	diffuse =	_color;
	specular =	_color;
}

Light::~Light()
{

}



float Light::lightFloat()
{
	return (diffuse.r + diffuse.g + diffuse.b + ambient.r + ambient.g + ambient.b) / 3.0f + 0.01f;
}

float Light::lightFloat(const Light &light)
{
	return (light.diffuse.r + light.diffuse.g + light.diffuse.b + light.ambient.r + light.ambient.g + light.ambient.b) / 3.0f + 0.01f;
}




LightAtt::LightAtt()
{

}

LightAtt::~LightAtt()
{

}

void LightAtt::sendData(ShaderProgram* shader)
{
	shader->sendUniform("uLightPosition", position);
	shader->sendUniform("uLightAmbient", ambient);	//minimum light
	shader->sendUniform("uLightDiffuse", diffuse);	//standard light
	shader->sendUniform("uLightSpecular", specular);	//shiny light
	
	//Used for light distance dropoff
	shader->sendUniform("uAttenuationConstant", AttenuationConstant);
	shader->sendUniform("uAttenuationLinear", AttenuationLinear);
	shader->sendUniform("uAttenuationQuadratic", AttenuationQuadratic);
}


Material::Material()
{

}

Material::~Material()
{

}

LightAddMult::LightAddMult()
{

}

LightAddMult::LightAddMult(const satColor &_diffuseMult)
{
	diffuseMult = _diffuseMult;
}

LightAddMult::LightAddMult(const satColor &_ambientAdd, const satColor &_diffuseAdd, const satColor &_specularAdd, const satColor &_emissiveAdd, const float &_shinyAdd, const satColor &_ambientMult, const satColor &_diffuseMult, const satColor &_specularMult, const satColor &_emissiveMult, const float &_shinyMult)
{
	ambientAdd =	_ambientAdd;
	diffuseAdd =	_diffuseAdd;
	specularAdd =	_specularAdd;
	emissiveAdd =	_emissiveAdd;
	shinyAdd = _shinyAdd;
	ambientMult = _ambientMult;
	diffuseMult = _diffuseMult;
	specularMult = _specularMult;
	emissiveMult = _emissiveMult;
	shinyMult = _shinyMult;
}

LightAddMult::~LightAddMult()
{

}

PointLight::PointLight(satColor _color, satVec3 _pos, float _constant, float _linear, float _quad)
{
	pos = _pos;
	color = _color;

	constant = _constant	* 0.25f;
	linear = _linear		* 0.25f;
	quad = _quad			* 0.25f;

	radius = getRadius();

	innerRadius = 0.5f;
}

PointLight::PointLight(satVec3 _color, satVec3 _pos, float _constant, float _linear, float _quad)
{
	pos = _pos;
	color = _color;

	constant = _constant	* 0.25f;
	linear = _linear		* 0.25f;
	quad = _quad			* 0.25f;

	radius = getRadius();

	innerRadius = 0.5f;
}

PointLight::~PointLight()
{

}

void PointLight::calcRadius()
{
	//const GLfloat lightThreshold = 5.0f; // 5 / 256
	//const GLfloat maxBrightness = std::fmaxf(std::fmaxf(color.r, color.g), color.b);
	//radius = (-linear + static_cast<float>(std::sqrt(linear * linear - 4 * quad * (constant - (256.0 / lightThreshold) * maxBrightness)))) / (2 * quad);
	const GLfloat maxBrightness = 1.0f;
	//const GLfloat lightLuminance = satVec3::Dot(satVec3(color.x, color.y, color.z), satVec3(0.2126f, 0.7152f, 0.0722f));
	const GLfloat lightLuminance = satVec3::Dot(satVec3(color.x, color.y, color.z), satVec3(0.3333f, 0.3333f, 0.3333f));
	const GLfloat minLuminance = 0.25f;
	radius = (-linear + static_cast<float>(std::sqrt(linear * linear - 4 * quad * (constant - (lightLuminance / minLuminance) * maxBrightness)))) / (2 * quad);

}

float PointLight::getRadius()
{
	//const GLfloat lightThreshold = 5.0f; // 5 / 256
	//const GLfloat maxBrightness = std::fmaxf(std::fmaxf(color.r, color.g), color.b);
	const GLfloat maxBrightness = 1.0f;
	//const GLfloat lightLuminance = satVec3::Dot(satVec3(color.x, color.y, color.z), satVec3(0.2126f, 0.7152f, 0.0722f));
	const GLfloat lightLuminance = satVec3::Dot(satVec3(color.x, color.y, color.z), satVec3(0.3333f, 0.3334f, 0.3333f));
	const GLfloat minLuminance = 0.25f;
	radius = (-linear + static_cast<float>(std::sqrt(linear * linear - 4 * quad * (constant - (lightLuminance / minLuminance) * maxBrightness)))) / (2 * quad);

	// Get light's luminance using Rec 709 luminance formula
	

	// Minimum luminance threshold - tweak to taste
	

	// Solve attenuation equation to get radius where it falls to minLuminance
	//radius = (constant - lightLuminance / minLuminance, linear, quad);

	return radius;
}


float PointLight::getRadius(const float &constant, const float &linear, const float &quad)
{
	const GLfloat maxBrightness = 1.0f;
	const GLfloat lightLuminance = satVec3::Dot(satVec3(0.2126f, 0.7152f, 0.0722f), satVec3(0.2126f, 0.7152f, 0.0722f));
	const GLfloat minLuminance = 0.25f;
	float radius = (-linear + static_cast<float>(std::sqrt(linear * linear - 4 * quad * (constant - (lightLuminance / minLuminance) * maxBrightness)))) / (2 * quad);
	
	return radius;
}