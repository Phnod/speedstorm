/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

//Stephen Thompson	100523681
//Jacob Robart		100492435
//Andrew Aultman	100516875
//Marco Valdez		100527945
//Felicia Albertina	100526516

//

/*

Lots of Thanks To:

	Emilian Cioca
	Daniel Yeomans
	Daniel Buckstein
	Michael Gharbharan
	Brandon Drenikow
	Matthew Stephan
	James Robb
	Andrew Hogue
	David Arppe
	Naoya Matsuoka


Special Thanks To:
	
	Jeff Lyons
	Josh Bellyk
	Emmanuel Lajeunesse
	Jacob Vorstenbosch
	Ashley Ezard
	Brianne Stephenson
	Spencer Dowie
	Jonathan Umar-Khitab
	Michael Riku Chan
	Cameron Nicoll
	James Conrad
	Tasos Stamadianos


Also Special Thanks:

	Curtis Gaunt
	Chantal Horan
	Russell Sng
	James Bonnyman
	Bo Ouyang
	Raymond Dee
	Darius Hackney
	Campbell Hamilton
	Mitchell Dinneen
	Donald Smith
	Samantha Stahlke

*/



//#include <GLFW/glfw3.h>
//#define GLFW_INCLUDE_GLU
//Only needed if glu.h isn't already included (Which we did)
//#include <GL/gl.h>
//#include <GL/glu.h>
//#include <GL/glut.h>

#define _CRT_SECURE_NO_WARNINGS

//#include <iostream>

//#include <vld.h>

#include "ResourceManager.h"
#include <windows.h>
#include <iostream>
#include "Game.h"
#include "Mesh.h"
#include "QuickMesh.h"
#include <string>

#include <Steam/steam_api.h>

#include <thread>

//#include <GL\freeglut.h>



//#include <SDKDDKVer.h>

#include "Math.h"

//System::Console::asdfa;

const int FRAME_DELAY_SPRITE = 1000 / FRAMES_PER_SECOND;

Game *theGame;

GLFWwindow* window;

GLFWmonitor* primaryMonitor;
sf::Window* sfWindow;


void swapBuffer() //GLFWwindow *windowPtr
{
	//glfwSwapBuffers(window);
}

void LoadingScreen()
{
	while(Game::LoadingActive)
	{
		//printf("LOADING GAME PLEASE WAIT\n");
		Sleep(500);
		//glfwPollEvents();
		
		theGame->displayLoadingScreen();
		//glfwSwapBuffers(window);
		//glfwPollEvents();
	}
}

void MainFrameLoop()
{
	// Keep running
	


	if (Game::TitleScreenActive == false && Game::WinScreenActive == false && Game::SpecialThanksActive == false)
	{
		if (Game::levelWinFrame == 0)
		{
			theGame->drawOld();
		}
		std::string titleName = "SpeedStorm";
		//titleName += std::to_string((int)theGame->HighScore);
		//glfwSetWindowTitle(window, titleName.c_str());
	}
	else if (Game::TitleScreenActive == true && Game::WinScreenActive == false && Game::SpecialThanksActive == false)
	{
		//glfwSetWindowTitle(window, "SpeedStorm");
		Game::LoadingActive = false;
		
		Game::adjust.x = 15.0f;
		theGame->TotalTranslation.y = 0.0f;

		theGame->ControllerUpdate();
		theGame->displayTitleScreen();
		//theGame->displaySpecialThanksScreen();
		//theGame->displaySpecialThanksScreen();
		//std::cout << sat::joy::CheckPressed(Xbox360Buttons::Start, 1000) << std::endl;
		

		if (theGame->windowSelected == true)
		{

			if(Game::TransitionTitle > 0.0f)
			{
				Game::TransitionTitle += Timer::frame;
			}

			if (Game::TransitionTitle > Game::TransitionTitleEnd)
			{
				Game::TransitionTitle = 0.0f;

				Game::TitleScreenActive = false;
				Game::WinScreenActive = false;

				theGame->curLevel->LoadLevelSounds();
				LevelManager::levelCurrent = -1;
				if(Game::SkipTutorial)
					LevelManager::levelCurrent = 1;
				theGame->winLevel();
			}

			if (sat::joy::CheckButton(Xbox360Buttons::B) || sat::key::CheckKey(sf::Keyboard::Escape))
			{
				if (Game::TransitionTitle > 0.0f)
				{
					Game::TransitionTitle = 0.0f;
				}
				else if (sat::joy::IsPressed(Xbox360Buttons::B) > 1.0f || sat::key::IsPressed(sf::Keyboard::Escape) > 1.0f)
				{
					exit(EXIT_SUCCESS);
				}
			}

			if (sat::joy::CheckPressed(Xbox360Buttons::Start) || sat::joy::CheckPressed(Xbox360Buttons::A) || sat::key::CheckPressed(sf::Keyboard::Space))
			{
				Game::SkipTutorial = false;
				Game::TransitionTitle += Timer::frame;
			}
			else if (sat::joy::CheckButton(Xbox360Buttons::LeftBumper) && sat::joy::CheckButton(Xbox360Buttons::RightBumper))
			{
				Game::SkipTutorial = true;
				Game::TransitionTitle += Timer::frame;
			}
			

			if (sat::joy::CheckPressed(Xbox360Buttons::Select) || sat::key::CheckPressed(sf::Keyboard::C))
			{
				Game::SpecialThanksActive = true;
				Game::TitleScreenActive = false;
				Game::WinScreenActive = false;
				Game::LoadingActive = false;
	
				theGame->setSong();
			}
	
		}
	}
	else if (Game::SpecialThanksActive == true)
	{
		//glfwSetWindowTitle(window, "SpeedStorm");
		theGame->displaySpecialThanksScreen();
		if (theGame->windowSelected == true)
		{
			if (sat::joy::CheckPressed(Xbox360Buttons::Start) || sat::joy::CheckPressed(Xbox360Buttons::A) || sat::key::CheckPressed(sf::Keyboard::Space))
			{
				Game::TitleScreenActive = true;
				Game::SpecialThanksActive = false;
				Game::LoadingActive = false;
				Game::WinScreenActive = false;
			}
		}
	}
	else if (Game::WinScreenActive == true)
	{
		//glfwSetWindowTitle(window, "YOU BEAT SPEEDSTORM");

		Game::adjust.x = 0;
		theGame->TotalTranslation.y = 0.0f;

		theGame->displayWinScreen();
		if (theGame->windowSelected == true)
		{
			if (sat::key::CheckPressed(sf::Keyboard::Space) || sat::joy::CheckPressed(Xbox360Buttons::Start) || sat::joy::CheckPressed(Xbox360Buttons::A))
			{
				Game::TitleScreenActive = true;

				theGame->setSong();

				Game::TitleScreenActive = false;
				Game::SpecialThanksActive = true;
				Game::WinScreenActive = false;
				Game::LoadingActive = false;
			}
		}
	}
}


//Error Display for GLFW
void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

//Receiving input events
//Each window has a large number of callbacks that can be set to receive all the various kinds of events.To receive key press and release events, create a key callback function.
static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE);
}



//The callback function receives the new size of the framebuffer when it is resized, 
//which can for example be used to update the OpenGL viewport.
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	//lol not now
}


void window_focus_callback(GLFWwindow* window, int focused)
{
	//This gives a performance warning, so not 100% sure I should use
	//theGame->windowSelected = focused;

	if (focused)
	{
		theGame->windowSelected = true;
		// The window gained input focus
	}
	else
	{
		theGame->windowSelected = false;
		// The window lost input focus
	}
}









//Now we have to actually create this "OpenGLDebugCallback" function
//Special thanks to Emilian Cioca
void CALLBACK OpenGLDebugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *msg, const void *data)
{
	//Ignored messages
	if (
		//Usage warning: Generic vertex attribute array ... uses a pointer with a small value (...). Is this intended to be used as an offset into a buffer object?
		id == 0x00020004 || 
		//Buffer info :	Total VBO memory usage in the system : ...
		id == 0x00020070)
	{
		return;
	}

	char buffer[9] = { '\0' };
	sprintf(buffer, "%.8x", id);

	std::string message("OpenGL(0x");
	message += buffer;
	message += "): ";

	switch (type)
	{
	case GL_DEBUG_TYPE_ERROR:
		message += "Error\n";
		break;
	case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
		message += "Deprecated behaviour\n";
		break;
	case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
		message += "Undefined behaviour\n";
		break;
	case GL_DEBUG_TYPE_PORTABILITY:
		message += "Portability issue\n";
		break;
	case GL_DEBUG_TYPE_PERFORMANCE:
		message += "Performance issue\n";
		break;
	case GL_DEBUG_TYPE_MARKER:
		message += "Stream annotation\n";
		break;
	case GL_DEBUG_TYPE_OTHER_ARB:
		message += "Other\n";
		break;
	}

	message += "Source: ";
	switch (source)
	{
	case GL_DEBUG_SOURCE_API:
		message += "API\n";
		break;
	case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
		message += "Window system\n";
		break;
	case GL_DEBUG_SOURCE_SHADER_COMPILER:
		message += "Shader compiler\n";
		break;
	case GL_DEBUG_SOURCE_THIRD_PARTY:
		message += "Third party\n";
		break;
	case GL_DEBUG_SOURCE_APPLICATION:
		message += "Application\n";
		break;
	case GL_DEBUG_SOURCE_OTHER:
		message += "Other\n";
		break;
	}

	message += "Severity: ";
	switch (severity)
	{
	case GL_DEBUG_SEVERITY_HIGH:
		message += "HIGH\n";
		break;
	case GL_DEBUG_SEVERITY_MEDIUM:
		message += "Medium\n";
		break;
	case GL_DEBUG_SEVERITY_LOW:
		message += "Low\n";
		break;
	case GL_DEBUG_SEVERITY_NOTIFICATION:
		message += "Notification\n";
		break;
	}

	message.append(msg, length);
	if (message.back() != '\n')
	{
		message.push_back('\n');
	}

	if (type == GL_DEBUG_TYPE_ERROR)
	{
		//Error(message);
		//if(sat::key::CheckPressed(sf::Keyboard::Comma))
		//{
			std::cout << message << std::endl;
		//}
	}
	else
	{
		//Log(message);
		//if(sat::key::CheckPressed(sf::Keyboard::Period))
		//{
			std::cout << message << std::endl;
		//}
	}
}












void SetupOpenGL()
{
	//glewInit();

	if (glewInit() != GLEW_OK)
	{
		std::cout << "GLEW could not be initialized.\n";
		system("PAUSE");
		exit(0);
	}

	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST); 
	glEnable(GL_BLEND);
	//glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


#include "SerialPort.h"

//char *port_name = "\\\\.\\COM5";
SerialPort* arduino;

//String for incoming data

#include <string>
#include <sstream>
#include <vector>
#include <iterator>



int main(int argc, char **argv)
{
	arduino = sat::ard::InitArduino();
	//SerialPort arduino(port_name);
	//if (arduino->isConnected()) std::cout << "Connection Established" << std::endl;
	//else std::cout << "ERROR, check port name\n";


	if (sat::ard::ArduinoActive())
	{
		while (arduino->isConnected() && sat::key::CheckKey(sf::Keyboard::B))
		{
			//Check if data has been read or not
			satVec3 vec = 0.0f;
			int shootButton = 0;
			if (sat::ard::ParseLine(vec, shootButton))
			{

				//prints out data
				//printf("----------------------------------\n");
				//printf("%s", incomingData);
				//satVec3 vecCon = vec / 100;
				//std::cout << (int)vecCon.x << "\t" << (int)vecCon.y << "\t" << (int)vecCon.z << "\t" << std::endl;
				std::cout << (vec) << "\t" << shootButton << std::endl;

			}
			else
			{
				std::cout << "Parse Error\n";
				std::cout << (vec) << "\t" << shootButton << std::endl;
			}


			//puts(incomingData);
			//wait a bit
			Sleep(16);
		}
	}
	else
	{
		printf("##################################\n");
		printf("Arduino not active!\n");
		printf("##################################\n");
	}

	

	bool steamCheck = SteamAPI_IsSteamRunning();
	
	std::cout << steamCheck << std::endl;
	
	Game::FullScreenActive = false;	

	//if (MessageBox(NULL,L"Would You Like To Run In Fullscreen Mode?", L"Start FullScreen?",MB_YESNO|MB_ICONQUESTION)==IDNO)
	//{
	//	Game::FullScreenActive = false;		// Windowed Mode
	//}
	//else
	//{
	//	Game::FullScreenActive = true;	
	//}


	sf::Context context;

	std::vector<sf::VideoMode> vidSizes = sf::VideoMode::getFullscreenModes();

	printf("\nVideo Modes:%u\n\n", vidSizes.size());
	for(int i = 0; i < vidSizes.size(); ++i)
	{
		printf("%i \t%i \t%i\n", vidSizes[i].width, vidSizes[i].height, vidSizes[i].bitsPerPixel);
	}
	printf("\n");

	//sfWindow = sf::Window(vidSizes[0], "SpeedStorm", sf::Style::Fullscreen);





	//if (!glfwInit())
	//{	
	//	exit(EXIT_FAILURE);
	//}
	//
	//
	//
	//primaryMonitor = glfwGetPrimaryMonitor();
	//const GLFWvidmode* mode = glfwGetVideoMode(primaryMonitor);

	//glfwSetErrorCallback(error_callback);
	if (Game::FullScreenActive == true)
	{
		//glfwWindowHint(GLFW_RED_BITS, mode->redBits);
		//glfwWindowHint(GLFW_GREEN_BITS, mode->greenBits);
		//glfwWindowHint(GLFW_BLUE_BITS, mode->blueBits);
		//glfwWindowHint(GLFW_REFRESH_RATE, mode->refreshRate);
		//
		////window = glfwCreateWindow(853, 480, "SpeedStorm", primaryMonitor, NULL);
		//window = glfwCreateWindow(mode->width, mode->height, "SpeedStorm", primaryMonitor, NULL);
		//
		//window = glfwCreateWindow(FULLSCREEN_WIDTH_DEFAULT, FULLSCREEN_HEIGHT_DEFAULT, "SpeedStorm", glfwGetPrimaryMonitor(), NULL);
		//Game::WINDOW_WIDTH = FULLSCREEN_WIDTH_DEFAULT;
		//Game::WINDOW_HEIGHT = FULLSCREEN_HEIGHT_DEFAULT;
		//Game::WINDOW_WIDTH = mode->width;
		//Game::WINDOW_HEIGHT = mode->height;

		sfWindow = new sf::Window(vidSizes[0], "SpeedStorm", sf::Style::Fullscreen);

		Game::WINDOW_WIDTH = vidSizes[0].width;
		Game::WINDOW_HEIGHT = vidSizes[0].height;
	}
	else
	{
		//window = glfwCreateWindow(1, 1, "SpeedStorm", NULL, NULL);
		//glfwSetWindowPos(window, 200, 100);

		Game::WINDOW_WIDTH = WINDOW_WIDTH_DEFAULT;
		Game::WINDOW_HEIGHT = WINDOW_HEIGHT_DEFAULT;

		sfWindow = new sf::Window(sf::VideoMode(Game::WINDOW_WIDTH, Game::WINDOW_HEIGHT), "SpeedStorm", sf::Style::Default);
		sfWindow->setPosition(sf::Vector2i(200, 100));
	}
	
	
	
	//if (!window)
	//{		
	//	glfwTerminate();
	//	exit(EXIT_FAILURE);
	//}
	//
	////This kills the window
	////glfwDestroyWindow(window);
	//
	//
	////Before you can use the OpenGL API, you must have a current OpenGL context.
	//glfwMakeContextCurrent(window);
	//
	////The key callback, like other window related callbacks, are set per-window.
	//glfwSetKeyCallback(window, key_callback);
	//
	//glfwSetWindowFocusCallback(window, window_focus_callback);
	////Rendering with OpenGL
	////Once you have a current OpenGL context, you can use OpenGL normally. 
	////The framebuffer size needs to be retrieved for glViewport.
	////int width, height;
	
	if (Game::FullScreenActive == true)
	{
		glViewport(0, 0, Game::WINDOW_WIDTH, Game::WINDOW_HEIGHT);
	}
	else
	{
		glViewport(0, 0, Game::WINDOW_WIDTH, Game::WINDOW_HEIGHT);
	}
	
	//glfwSwapInterval(0);
	
	//if (!glfwOpenWindow(1024, 768, 8, 8, 8, 8, 24, 0, GLFW_FULLSCREEN))
	//{
	//
	//}

	Timer::Init();

	//*timerFrame = 0.0;
	//*timer = glfwGetTime();
	//*timerOld = *timer;
	
	glewExperimental = true;//Initialize Glew
	
	SetupOpenGL();


	PFNWGLSWAPINTERVALEXTPROC wglSwapIntervalEXT = 0;
	PFNWGLGETSWAPINTERVALEXTPROC wglGetSwapIntervalEXT = 0;
	
	wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC) 
	wglGetProcAddress("wglSwapIntervalEXT");
	wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC) 
	wglGetProcAddress("wglGetSwapIntervalEXT");
	wglSwapIntervalEXT(0); 
	


	#ifdef _DEBUG
			glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
			glDebugMessageCallback(OpenGLDebugCallback, NULL);
			//glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, NULL, GL_TRUE);
			//glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DEBUG_SEVERITY_NOTIFICATION, 0, 0, GL_FALSE);
	#endif
	

	
	//printf("Before I create theGame \n");
	/* init the game */
	theGame = new Game();
	//printf("Before I initialize theGame \n");
	theGame->arduino = arduino;
	//theGame->beginHell();

	//Sleep(12500);
	theGame->setWindow(sfWindow);
	//theGame->setWindow(sfWindow);
	//theGame->setTimers(timer, timerOld, timerFrame);
	if (Game::FullScreenActive == false)
	{
		//glfwSetWindowSize(window, WINDOW_WIDTH_DEFAULT, WINDOW_HEIGHT_DEFAULT);
		//theGame->ReshapeWindow(WINDOW_WIDTH_DEFAULT, WINDOW_HEIGHT_DEFAULT);
		theGame->ReshapeWindow(Game::WINDOW_WIDTH, Game::WINDOW_HEIGHT);
	}
	else
	{
		theGame->ReshapeWindow(Game::WINDOW_WIDTH, Game::WINDOW_HEIGHT);
		//Game::WINDOW_WIDTH = mode->width;
		//Game::WINDOW_HEIGHT = mode->height;
	}
	
	theGame->setupLevel();

	//
	theGame->displayLoadingScreen();
	sfWindow->display();
	
	sfWindow->setFramerateLimit(60);
	//glfwSwapBuffers(window);
	//std::thread load (LoadingScreen);	
	
	theGame->initializeGame();
	//printf("After I initialize theGame \n");
	theGame->TotalTranslation.y = Game::ShipSpawnPosition.y;
	LevelManager::LoadLevelAssets();

	Game::printDebugCommands();
	Timer::tick();
	theGame->setupLevel();
	theGame->setSong();

	Game::LoadingActive = false;
	//load.join();







	
        








	while (sfWindow->isOpen())
    {
		Timer::tick();
		
		//if(sat::key::CheckPressed(sf::Keyboard::Tab))
		//{
		//	glfwHideWindow(window);
		//}
		if(Game::PauseActive)
		{
			Game::TimePaused = Min(Game::TimePaused - Timer::frame, -Timer::frame);
		}
		else
		{
			Game::TimePaused = Max(Game::TimePaused + Timer::frame, Timer::frame);
		}

		float lowpass = 1.0f, highpass = 1.0f;
		lowpass = Timer::sinRange(0.5f, 500.0f, 1500.0f);
			//highpass = Timer::sinRange(0.25f, 2500.0f, 5000.0f);

		if(Game::PauseActive || (Game::TimePaused < 2.5))
		{	
			if(Game::TimePaused < 0.0)
				lowpass = Lerp(22001.0f, lowpass, powf(-Game::TimePaused * 2.0, 0.7f));		
			if(Game::TimePaused > 0.0)
				lowpass = Lerp(lowpass, 22001.0f, powf(Game::TimePaused / 2.0, 2.0f));
			LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPLowpass(lowpass);
			//LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPHighpass(highpass);
			//LevelManager::level[LevelManager::currentSongPlaying].Song.setDSPFlange(1.0f, 0.2f, 1.0f);
		}
		// COMEDY GOLD
		//static unsigned int position = 0;
		//if (Game::PauseActive)
		//{
		//	LevelManager::level[LevelManager::currentSongPlaying].Song.channel->setPosition(position, FMOD_TIMEUNIT_MS);
		//}
		//LevelManager::level[LevelManager::currentSongPlaying].Song.channel->getPosition(&position, FMOD_TIMEUNIT_MS);

		if (theGame->windowSelected == true)
		{			
			if (Game::PauseActive == false && Game::WinScreenActive == false && Game::TitleScreenActive == false && Game::SpecialThanksActive == false)
			{
				
				theGame->updateOld();
			}
			else
			{
				theGame->updateMenu();
			}
			


			
		}

		MainFrameLoop();
		//endif (theGame->windowSelected == true)
	
		if (theGame->windowSelected == true)
		{
		}
		else
		{
			Sleep(80);
		}
		
		sfWindow->display();
		
		// check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (sfWindow->pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
			{	sfWindow->close();	}
			else if (event.type == sf::Event::LostFocus && Game::SatDebugAutoPauseWindowActive)
			{	theGame->windowSelected = false;	}
			else if (event.type == sf::Event::GainedFocus)
			{	theGame->windowSelected = true;	}
        }

    }
	
	//End of Game
	//GAME OVER
	
	delete sfWindow;
	exit(EXIT_SUCCESS);

	return 0;
}
