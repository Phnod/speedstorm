/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <assert.h>

class satVec2;
class satVec3;
class satVec4;
class satMat4;

/*
//=======================================================================================
//
//	satMat2 - 2x2 matrix
//
//=======================================================================================
*/

#pragma region satMat2

class satMat2
{
public:
	satMat2();
	explicit satMat2(const float xx, const float xy, const float yx, const float yy);
	explicit satMat2(const satVec2& x, const satVec2& y);

	const satVec2& operator[](int a) const;
	satVec2& operator[](int a);

	satMat2 operator+(const satMat2 &a) const;
	satMat2 operator-(const satMat2 &a) const;
	satVec2 operator*(const satVec2 &x) const;
	satMat2 operator*(const satMat2 &x) const;
	satMat2 operator*(const float a) const;
	satMat2 operator-() const;

	satMat2& operator+=(const satMat2 &x);
	satMat2& operator-=(const satMat2 &x);
	satMat2& operator*=(const satMat2 &x);
	satMat2& operator*=(const float a);

	friend satMat2 operator*(const float a, const satMat2 &mat);
	friend satVec2 operator*(const satVec2 &vec, const satMat2 &mat);
	friend satVec2& operator*=(satVec2 &vec, const satMat2 &mat);

	bool operator==(const satMat2 &a) const;
	bool operator!=(const satMat2 &a) const;

	void Zero();
	void Identity();

	float Trace() const;
	float Determinant() const;
	satMat2 Transpose() const;
	satMat2& TransposeSelf();
	satMat2 Inverse() const;
	bool InverseSelf(); //false if determinant is zero
	satMat2 InverseFast() const;
	bool InverseFastSelf();

	int GetDimension() const;

	const float * ToFloatPtr() const;
	float * ToFloatPtr();
	const char * ToString(int precision = 2) const;

private:

	satVec2 mat[2];


};



#pragma endregion satMat2

//extern satMat2 mat2_zero;
//extern satMat2 mat2_identity;
//#define mat2_default	mat2_identity





/*
//=======================================================================================
//
//	satMat3
//
//=======================================================================================
*/

class satMat3 {
public:
	satMat3();
	explicit satMat3(const satVec3 &x, const satVec3 &y, const satVec3 &z);
	explicit satMat3(const float xx, const float xy, const float xz, const float yx, const float yy, const float yz, const float zx, const float zy, const float zz);
	explicit satMat3(const float src[3][3]);

	const satVec3 &	operator[](int index) const;
	satVec3 &		operator[](int index);
	satMat3 &		operator=(const satMat3 &m);
	satMat3			operator-() const;
	satMat3			operator*(const float a) const;
	satVec3			operator*(const satVec3 &vec) const;
	satMat3			operator*(const satMat3 &m) const;
	satMat3			operator+(const satMat3 &m) const;
	satMat3			operator-(const satMat3 &m) const;
	satMat3 &		operator*=(const float a);
	satMat3 &		operator*=(const satMat3 &m);
	satMat3 &		operator+=(const satMat3 &m);
	satMat3 &		operator-=(const satMat3 &m);

	friend satMat3	operator*(const float a, const satMat3 &m);
	friend satVec3	operator*(const satVec3 &vec, const satMat3 &m);
	friend satVec3 &	operator*=(satVec3 &vec, const satMat3 &m);

	bool			Equals(const satMat3 &m) const;
	bool			Equals(const satMat3 &m, const float margin) const;
	bool			operator==(const satMat3 &m) const;
	bool			operator!=(const satMat3 &m) const;

	void			Zero();
	satMat3			Identity();
	void			LoadIdentity();

	satMat3		OrthoNormalize() const;
	satMat3		Transpose() const;	// returns transpose
	satMat3 &	TransposeSelf();
	satMat3		InverseFast();	// returns the inverse ( m * m.Inverse() = identity )
	bool			InverseFastSelf();	// returns false if determinant is zero
	float			Determinant();
	satMat3		TransposeMultiply(const satMat3 &b) const;

	void RotateX(const float degrees);
	void RotateY(const float degrees);
	void RotateZ(const float degrees);

	void Scale(float x, float y, float z);
	void Scale(const satVec3 &vec);
	void Scale(float xyz);
	void ScaleX(float x);
	void ScaleY(float y);
	void ScaleZ(float z);

	static satMat3 RotationX(const float degrees);
	static satMat3 RotationY(const float degrees);
	static satMat3 RotationZ(const float degrees);

	int			GetDimension() const;

	satMat4			ToMat4() const;
	
	
	satVec3			mat[3];

private:
	
};









/*
//=======================================================================================
//
//	satMat4
//
//=======================================================================================
*/

class satMat4 {
public:
	satMat4();
	explicit satMat4(const satVec4& right, const satVec4& up, const satVec4& dir, const satVec4& pos);
	explicit satMat4(const float xx, const float xy, const float xz, const float xw,
		const float yx, const float yy, const float yz, const float yw,
		const float zx, const float zy, const float zz, const float zw,
		const float wx, const float wy, const float wz, const float ww);
	satMat4(const satMat3 &rotation, const satVec3 &translation);
	satMat4(const satMat3 &notIdentity);
	satMat4(const satVec3& right, const satVec3& up, const satVec3& dir, const satVec3& position);
	satMat4(const satMat4& m);

	const satVec4 &	operator[](int index) const;
	satVec4 &		operator[](int index);
	satMat4			operator-() const;
	satMat4			operator*(const float a) const;
	satVec4			operator*(const satVec4 &vec) const;
	satVec3			operator*(const satVec3 &vec) const;
	satMat4			operator*(const satMat4 &m) const;
	satMat4			operator+(const satMat4 &m) const;
	satMat4			operator-(const satMat4 &m) const;
	satMat4 &		operator*=(const float a);
	satMat4 &		operator*=(const satMat4 &m);
	satMat4 &		operator+=(const satMat4 &m);
	satMat4 &		operator-=(const satMat4 &m);
	satMat4 & 		operator=(const satMat4 &m);

	friend satMat4	operator*(const float a, const satMat4 &mat);
	friend satVec4	operator*(const satVec4 &vec, const satMat4 &mat);
	friend satVec3	operator*(const satVec3 &vec, const satMat4 &mat);
	friend satVec4 &	operator*=(satVec4 &vec, const satMat4 &mat);
	friend satVec3 &	operator*=(satVec3 &vec, const satMat4 &mat);

	bool			Equals(const satMat4 &a) const;
	bool			Equals(const satMat4 &a, const float margin) const;	//margin of difference
	bool			operator==(const satMat4 &a) const;
	bool			operator!=(const satMat4 &a) const;

	void	SetZero();
	
	satVec3 GetPos();
	satVec3 xyz();

	
	satMat4		Transpose() const;	// returns transpose
	satMat4 &	TransposeSelf();
	satMat4		Inverse() const;
	bool			InverseSelf();
	satMat4		InverseFast();	// returns the inverse ( m * m.Inverse() = identity )
	bool			InverseFastSelf();	// returns false if determinant is zero
	satMat4		OrthoNormalize() const;
	float			Determinant() const; 
	float			GetDeterminant() const;
	satMat4		TransposeMultiply(const satMat4 &b) const;

	static satMat4 LerpMat4(const satMat4 &a, const satMat4 &b, const float &t);

	void RotateX(const float degreets);
	void RotateY(const float degreets);
	void RotateZ(const float degreets);

	void Translate(float x, float y, float z);
	void Translate(const satVec3 &pos);
	void TranslateInstance(const float &x, const float &y, const float &z);
	void TranslateInstance(const satVec3 &vec);
	void TranslateX(float x);
	void TranslateY(float y);
	void TranslateZ(float z);

	void Scale(float x, float y, float z);
	void Scale(const satVec3 &vec);
	void Scale(float xyz);
	void ScaleX(float x);
	void ScaleY(float y);
	void ScaleZ(float z);

	static satMat4 RotationX(const float degreets);
	static satMat4 RotationY(const float degreets);
	static satMat4 RotationZ(const float degreets);
	static satMat4 Translation(float x, float y, float z);
	static satMat4 Translation(satVec3 vec);
	static satMat4 TranslationInstance(float x, float y, float z);
	static satMat4 TranslationInstance(satVec3 vec);
	static satMat4 Transpose(satMat4 &m);
	
	satMat3 GetRotationMatrix();
	satVec3 GetDirectionVector();

	void PerspectiveProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar);
	void PerspectiveStereoProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skew, float convergence);
	void PerspectiveStereoProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY);
	void PerspectiveProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, satMat4 &inverse);
	void OrthoProjection(float top, float bottom, float left, float right, float zNear, float zFar);
	void OrthoStereoProjection(float top, float bottom, float left, float right, float zNear, float zFar, float skew, float convergence);
	void OrthoStereoProjection(float top, float bottom, float left, float right, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY);


	static satMat4 CatmullRomIdentity();
	static satMat4 BezierIdentity();

	int	GetDimension() const;
	
	static satMat4 Identity();
	void LoadIdentity();

	void Print();

	union
	{
		struct
		{
			satVec4	mat[4];
		};		
	};

	float* Float();
	//float data[16];

private:
	
};

//====================//
// satVec4::operators //
//====================//

#pragma region satMat4operators

inline const satVec4& satMat4::operator[](int index) const
{
	return mat[index];
}

inline satVec4 &	satMat4::operator[](int index)
{
	return mat[index];
}

inline satMat4		satMat4::operator-() const
{
	return satMat4(
		-mat[0][0], -mat[0][1], -mat[0][2], -mat[0][3],
		-mat[1][0], -mat[1][1], -mat[1][2], -mat[1][3],
		-mat[2][0], -mat[2][1], -mat[2][2], -mat[2][3],
		-mat[3][0], -mat[3][1], -mat[3][2], -mat[3][3]);
}

inline satMat4 &	satMat4::operator=(const satMat4 &m)
{
		mat[0][0] = m[0][0]; mat[0][1] = m[0][1]; mat[0][2] = m[0][2]; mat[0][3] = m[0][3];
		mat[1][0] = m[1][0]; mat[1][1] = m[1][1]; mat[1][2] = m[1][2]; mat[1][3] = m[1][3];
		mat[2][0] = m[2][0]; mat[2][1] = m[2][1]; mat[2][2] = m[2][2]; mat[2][3] = m[2][3];
		mat[3][0] = m[3][0]; mat[3][1] = m[3][1]; mat[3][2] = m[3][2]; mat[3][3] = m[3][3];

		return *this;
}

inline satMat4		satMat4::operator*(const float a) const
{
	return satMat4(
		mat[0][0] * a, mat[0][1] * a, mat[0][2] * a, mat[0][3] * a,
		mat[1][0] * a, mat[1][1] * a, mat[1][2] * a, mat[1][3] * a,
		mat[2][0] * a, mat[2][1] * a, mat[2][2] * a, mat[2][3] * a,
		mat[3][0] * a, mat[3][1] * a, mat[3][2] * a, mat[3][3] * a);
}

inline satVec4		satMat4::operator*(const satVec4 &vec) const
{
	return satVec4(
		mat[0][0] * vec[0] + mat[1][0] * vec[1] + mat[2][0] * vec[2] + mat[3][0] * vec[3],
		mat[0][1] * vec[0] + mat[1][1] * vec[1] + mat[2][1] * vec[2] + mat[3][1] * vec[3],
		mat[0][2] * vec[0] + mat[1][2] * vec[1] + mat[2][2] * vec[2] + mat[3][2] * vec[3],
		mat[0][3] * vec[0] + mat[1][3] * vec[1] + mat[2][3] * vec[2] + mat[3][3] * vec[3]);
}

inline satMat4 satMat4::operator*(const satMat4 &m) const
{
	return satMat4(
		m.mat[0][0] * mat[0][0] + m.mat[1][0] * mat[0][1] + m.mat[2][0] * mat[0][2] + m.mat[3][0] * mat[0][3],
		m.mat[0][1] * mat[0][0] + m.mat[1][1] * mat[0][1] + m.mat[2][1] * mat[0][2] + m.mat[3][1] * mat[0][3],
		m.mat[0][2] * mat[0][0] + m.mat[1][2] * mat[0][1] + m.mat[2][2] * mat[0][2] + m.mat[3][2] * mat[0][3],
		m.mat[0][3] * mat[0][0] + m.mat[1][3] * mat[0][1] + m.mat[2][3] * mat[0][2] + m.mat[3][3] * mat[0][3],
		m.mat[0][0] * mat[1][0] + m.mat[1][0] * mat[1][1] + m.mat[2][0] * mat[1][2] + m.mat[3][0] * mat[1][3],
		m.mat[0][1] * mat[1][0] + m.mat[1][1] * mat[1][1] + m.mat[2][1] * mat[1][2] + m.mat[3][1] * mat[1][3],
		m.mat[0][2] * mat[1][0] + m.mat[1][2] * mat[1][1] + m.mat[2][2] * mat[1][2] + m.mat[3][2] * mat[1][3],
		m.mat[0][3] * mat[1][0] + m.mat[1][3] * mat[1][1] + m.mat[2][3] * mat[1][2] + m.mat[3][3] * mat[1][3],
		m.mat[0][0] * mat[2][0] + m.mat[1][0] * mat[2][1] + m.mat[2][0] * mat[2][2] + m.mat[3][0] * mat[2][3],
		m.mat[0][1] * mat[2][0] + m.mat[1][1] * mat[2][1] + m.mat[2][1] * mat[2][2] + m.mat[3][1] * mat[2][3],
		m.mat[0][2] * mat[2][0] + m.mat[1][2] * mat[2][1] + m.mat[2][2] * mat[2][2] + m.mat[3][2] * mat[2][3],
		m.mat[0][3] * mat[2][0] + m.mat[1][3] * mat[2][1] + m.mat[2][3] * mat[2][2] + m.mat[3][3] * mat[2][3],
		m.mat[0][0] * mat[3][0] + m.mat[1][0] * mat[3][1] + m.mat[2][0] * mat[3][2] + m.mat[3][0] * mat[3][3],
		m.mat[0][1] * mat[3][0] + m.mat[1][1] * mat[3][1] + m.mat[2][1] * mat[3][2] + m.mat[3][1] * mat[3][3],
		m.mat[0][2] * mat[3][0] + m.mat[1][2] * mat[3][1] + m.mat[2][2] * mat[3][2] + m.mat[3][2] * mat[3][3],
		m.mat[0][3] * mat[3][0] + m.mat[1][3] * mat[3][1] + m.mat[2][3] * mat[3][2] + m.mat[3][3] * mat[3][3]);
}

inline satMat4		satMat4::operator+(const satMat4 &m) const
{
	return satMat4(
		mat[0][0] + m[0][0], mat[1][0] + m[1][0], mat[2][0] + m[2][0], mat[3][0] + m[3][0],
		mat[0][1] + m[0][1], mat[1][1] + m[1][1], mat[2][1] + m[2][1], mat[3][1] + m[3][1],
		mat[0][2] + m[0][2], mat[1][2] + m[1][2], mat[2][2] + m[2][2], mat[3][2] + m[3][2],
		mat[0][3] + m[0][3], mat[1][3] + m[1][3], mat[2][3] + m[2][3], mat[3][3] + m[3][3]);
}

inline satMat4		satMat4::operator-(const satMat4 &m) const
{
	return satMat4(
		mat[0][0] - m[0][0], mat[1][0] - m[1][0], mat[2][0] - m[2][0], mat[3][0] - m[3][0],
		mat[0][1] - m[0][1], mat[1][1] - m[1][1], mat[2][1] - m[2][1], mat[3][1] - m[3][1],
		mat[0][2] - m[0][2], mat[1][2] - m[1][2], mat[2][2] - m[2][2], mat[3][2] - m[3][2],
		mat[0][3] - m[0][3], mat[1][3] - m[1][3], mat[2][3] - m[2][3], mat[3][3] - m[3][3]);
}

inline satMat4 &	satMat4::operator*=(const float a)
{
	mat[0] *= a;
	mat[1] *= a;
	mat[2] *= a;
	mat[3] *= a;

	return *this;
}

inline satMat4 &	satMat4::operator*=(const satMat4 &m)
{
	mat[0] *= m[0];
	mat[1] *= m[1];
	mat[2] *= m[2];
	mat[3] *= m[3];

	return *this;
}

inline satMat4 &	satMat4::operator+=(const satMat4 &m)
{
	mat[0] += m[0];
	mat[1] += m[1];
	mat[2] += m[2];
	mat[3] += m[3];

	return *this;
}

inline satMat4 &	satMat4::operator-=(const satMat4 &m)
{
	mat[0] -= m[0];
	mat[1] -= m[1];
	mat[2] -= m[2];
	mat[3] -= m[3];

	return *this;
}


//	Reversered operations

inline satMat4	operator*(const float a, const satMat4 &m)
{
	return m * a;
}

inline satVec4	operator*(const satVec4 &vec, const satMat4 &m)
{
	return m * vec;
}

inline satVec3	operator*(const satVec3 &vec, const satMat4 &m)
{
	return m * vec;
}

inline satVec4 &	operator*=(satVec4 &vec, const satMat4 &m)
{
	vec = m * vec;
	return vec;
}

inline satVec3 &	operator*=(satVec3 &vec, const satMat4 &m)
{
	vec = m * vec;
	return vec;
}


#pragma endregion satMat4operators

//===========================//
// end of satMat4::operators //
//===========================//


//	INVERSE AND TRANSPOSE

inline satMat4& satMat4::TransposeSelf()
{
	satMat4 temp = satMat4(
		mat[0][0], mat[1][0], mat[2][0], mat[3][0],
		mat[0][1], mat[1][1], mat[2][1], mat[3][1],
		mat[0][2], mat[1][2], mat[2][2], mat[3][2],
		mat[0][3], mat[1][3], mat[2][3], mat[3][3]);

	return ((*this) = temp);
}

inline satMat4	satMat4::Inverse() const
{
	satMat4 invMat;

	invMat = *this;
	invMat.InverseSelf();
	//assert(invMat.InverseSelf());
	return invMat;
}

inline bool		satMat4::InverseSelf()
{
	// 84+4+16 = 104 multiplications
	//			   1 division
	double det, invDet;

	// 2x2 sub-determinants required to calculate 4x4 determinant
	float det2_01_01 = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
	float det2_01_02 = mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0];
	float det2_01_03 = mat[0][0] * mat[1][3] - mat[0][3] * mat[1][0];
	float det2_01_12 = mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1];
	float det2_01_13 = mat[0][1] * mat[1][3] - mat[0][3] * mat[1][1];
	float det2_01_23 = mat[0][2] * mat[1][3] - mat[0][3] * mat[1][2];

	// 3x3 sub-determinants required to calculate 4x4 determinant
	float det3_201_012 = mat[2][0] * det2_01_12 - mat[2][1] * det2_01_02 + mat[2][2] * det2_01_01;
	float det3_201_013 = mat[2][0] * det2_01_13 - mat[2][1] * det2_01_03 + mat[2][3] * det2_01_01;
	float det3_201_023 = mat[2][0] * det2_01_23 - mat[2][2] * det2_01_03 + mat[2][3] * det2_01_02;
	float det3_201_123 = mat[2][1] * det2_01_23 - mat[2][2] * det2_01_13 + mat[2][3] * det2_01_12;

	det = (-det3_201_123 * mat[3][0] + det3_201_023 * mat[3][1] - det3_201_013 * mat[3][2] + det3_201_012 * mat[3][3]);

	if (abs(det) < 0.0000000000000000000001f)
	{
		return false;
	}

	invDet = 1.0f / det;

	// remaining 2x2 sub-determinants
	float det2_03_01 = (float)(mat[0][0] * mat[3][1] - mat[0][1] * mat[3][0]);
	float det2_03_02 = (float)(mat[0][0] * mat[3][2] - mat[0][2] * mat[3][0]);
	float det2_03_03 = (float)(mat[0][0] * mat[3][3] - mat[0][3] * mat[3][0]);
	float det2_03_12 = (float)(mat[0][1] * mat[3][2] - mat[0][2] * mat[3][1]);
	float det2_03_13 = (float)(mat[0][1] * mat[3][3] - mat[0][3] * mat[3][1]);
	float det2_03_23 = (float)(mat[0][2] * mat[3][3] - mat[0][3] * mat[3][2]);
																			
	float det2_13_01 = (float)(mat[1][0] * mat[3][1] - mat[1][1] * mat[3][0]);
	float det2_13_02 = (float)(mat[1][0] * mat[3][2] - mat[1][2] * mat[3][0]);
	float det2_13_03 = (float)(mat[1][0] * mat[3][3] - mat[1][3] * mat[3][0]);
	float det2_13_12 = (float)(mat[1][1] * mat[3][2] - mat[1][2] * mat[3][1]);
	float det2_13_13 = (float)(mat[1][1] * mat[3][3] - mat[1][3] * mat[3][1]);
	float det2_13_23 = (float)(mat[1][2] * mat[3][3] - mat[1][3] * mat[3][2]);

	// remaining 3x3 sub-determinants
	float det3_203_012 = (float)(mat[2][0] * det2_03_12 - mat[2][1] * det2_03_02 + mat[2][2] * det2_03_01);
	float det3_203_013 = (float)(mat[2][0] * det2_03_13 - mat[2][1] * det2_03_03 + mat[2][3] * det2_03_01);
	float det3_203_023 = (float)(mat[2][0] * det2_03_23 - mat[2][2] * det2_03_03 + mat[2][3] * det2_03_02);
	float det3_203_123 = (float)(mat[2][1] * det2_03_23 - mat[2][2] * det2_03_13 + mat[2][3] * det2_03_12);

	float det3_213_012 = (float)(mat[2][0] * det2_13_12 - mat[2][1] * det2_13_02 + mat[2][2] * det2_13_01);
	float det3_213_013 = (float)(mat[2][0] * det2_13_13 - mat[2][1] * det2_13_03 + mat[2][3] * det2_13_01);
	float det3_213_023 = (float)(mat[2][0] * det2_13_23 - mat[2][2] * det2_13_03 + mat[2][3] * det2_13_02);
	float det3_213_123 = (float)(mat[2][1] * det2_13_23 - mat[2][2] * det2_13_13 + mat[2][3] * det2_13_12);

	float det3_301_012 = (float)(mat[3][0] * det2_01_12 - mat[3][1] * det2_01_02 + mat[3][2] * det2_01_01);
	float det3_301_013 = (float)(mat[3][0] * det2_01_13 - mat[3][1] * det2_01_03 + mat[3][3] * det2_01_01);
	float det3_301_023 = (float)(mat[3][0] * det2_01_23 - mat[3][2] * det2_01_03 + mat[3][3] * det2_01_02);
	float det3_301_123 = (float)(mat[3][1] * det2_01_23 - mat[3][2] * det2_01_13 + mat[3][3] * det2_01_12);

	mat[0][0] = (float)(-det3_213_123 * invDet);
	mat[1][0] = (float)(+det3_213_023 * invDet);
	mat[2][0] = (float)(-det3_213_013 * invDet);
	mat[3][0] = (float)(+det3_213_012 * invDet);

	mat[0][1] = (float)(+det3_203_123 * invDet);
	mat[1][1] = (float)(-det3_203_023 * invDet);
	mat[2][1] = (float)(+det3_203_013 * invDet);
	mat[3][1] = (float)(-det3_203_012 * invDet);

	mat[0][2] = (float)(+det3_301_123 * invDet);
	mat[1][2] = (float)(-det3_301_023 * invDet);
	mat[2][2] = (float)(+det3_301_013 * invDet);
	mat[3][2] = (float)(-det3_301_012 * invDet);

	mat[0][3] = (float)(-det3_201_123 * invDet);
	mat[1][3] = (float)(+det3_201_023 * invDet);
	mat[2][3] = (float)(-det3_201_013 * invDet);
	mat[3][3] = (float)(+det3_201_012 * invDet);

	return true;
}

inline satMat4	satMat4::InverseFast()
{
	//https://i.gyazo.com/8ece30b374aba324415b2bc412978752.png
	//https://i.gyazo.com/3ca1e222225e189e0b94d4ac110a7341.png

	float deter = GetDeterminant();
	assert(deter != 0);
	deter = 1.0f / deter;

	satMat4 temp = satMat4(
		mat[2][1] * mat[3][2] * mat[1][3] - mat[3][1] * mat[2][2] * mat[1][3] + mat[3][1] * mat[1][2] * mat[2][3] - mat[1][1] * mat[3][2] * mat[2][3] - mat[2][1] * mat[1][2] * mat[3][3] + mat[1][1] * mat[2][2] * mat[3][3],
		mat[3][0] * mat[2][2] * mat[1][3] - mat[2][0] * mat[3][2] * mat[1][3] - mat[3][0] * mat[1][2] * mat[2][3] + mat[1][0] * mat[3][2] * mat[2][3] + mat[2][0] * mat[1][2] * mat[3][3] - mat[1][0] * mat[2][2] * mat[3][3],
		mat[2][0] * mat[3][1] * mat[1][3] - mat[3][0] * mat[2][1] * mat[1][3] + mat[3][0] * mat[1][1] * mat[2][3] - mat[1][0] * mat[3][1] * mat[2][3] - mat[2][0] * mat[1][1] * mat[3][3] + mat[1][0] * mat[2][1] * mat[3][3],
		mat[3][0] * mat[2][1] * mat[1][2] - mat[2][0] * mat[3][1] * mat[1][2] - mat[3][0] * mat[1][1] * mat[2][2] + mat[1][0] * mat[3][1] * mat[2][2] + mat[2][0] * mat[1][1] * mat[3][2] - mat[1][0] * mat[2][1] * mat[3][2],
		mat[3][1] * mat[2][2] * mat[0][3] - mat[2][1] * mat[3][2] * mat[0][3] - mat[3][1] * mat[0][2] * mat[2][3] + mat[0][1] * mat[3][2] * mat[2][3] + mat[2][1] * mat[0][2] * mat[3][3] - mat[0][1] * mat[2][2] * mat[3][3],
		mat[2][0] * mat[3][2] * mat[0][3] - mat[3][0] * mat[2][2] * mat[0][3] + mat[3][0] * mat[0][2] * mat[2][3] - mat[0][0] * mat[3][2] * mat[2][3] - mat[2][0] * mat[0][2] * mat[3][3] + mat[0][0] * mat[2][2] * mat[3][3],
		mat[3][0] * mat[2][1] * mat[0][3] - mat[2][0] * mat[3][1] * mat[0][3] - mat[3][0] * mat[0][1] * mat[2][3] + mat[0][0] * mat[3][1] * mat[2][3] + mat[2][0] * mat[0][1] * mat[3][3] - mat[0][0] * mat[2][1] * mat[3][3],
		mat[2][0] * mat[3][1] * mat[0][2] - mat[3][0] * mat[2][1] * mat[0][2] + mat[3][0] * mat[0][1] * mat[2][2] - mat[0][0] * mat[3][1] * mat[2][2] - mat[2][0] * mat[0][1] * mat[3][2] + mat[0][0] * mat[2][1] * mat[3][2],
		mat[1][1] * mat[3][2] * mat[0][3] - mat[3][1] * mat[1][2] * mat[0][3] + mat[3][1] * mat[0][2] * mat[1][3] - mat[0][1] * mat[3][2] * mat[1][3] - mat[1][1] * mat[0][2] * mat[3][3] + mat[0][1] * mat[1][2] * mat[3][3],
		mat[3][0] * mat[1][2] * mat[0][3] - mat[1][0] * mat[3][2] * mat[0][3] - mat[3][0] * mat[0][2] * mat[1][3] + mat[0][0] * mat[3][2] * mat[1][3] + mat[1][0] * mat[0][2] * mat[3][3] - mat[0][0] * mat[1][2] * mat[3][3],
		mat[1][0] * mat[3][1] * mat[0][3] - mat[3][0] * mat[1][1] * mat[0][3] + mat[3][0] * mat[0][1] * mat[1][3] - mat[0][0] * mat[3][1] * mat[1][3] - mat[1][0] * mat[0][1] * mat[3][3] + mat[0][0] * mat[1][1] * mat[3][3],
		mat[3][0] * mat[1][1] * mat[0][2] - mat[1][0] * mat[3][1] * mat[0][2] - mat[3][0] * mat[0][1] * mat[1][2] + mat[0][0] * mat[3][1] * mat[1][2] + mat[1][0] * mat[0][1] * mat[3][2] - mat[0][0] * mat[1][1] * mat[3][2],
		mat[2][1] * mat[1][2] * mat[0][3] - mat[1][1] * mat[2][2] * mat[0][3] - mat[2][1] * mat[0][2] * mat[1][3] + mat[0][1] * mat[2][2] * mat[1][3] + mat[1][1] * mat[0][2] * mat[2][3] - mat[0][1] * mat[1][2] * mat[2][3],
		mat[1][0] * mat[2][2] * mat[0][3] - mat[2][0] * mat[1][2] * mat[0][3] + mat[2][0] * mat[0][2] * mat[1][3] - mat[0][0] * mat[2][2] * mat[1][3] - mat[1][0] * mat[0][2] * mat[2][3] + mat[0][0] * mat[1][2] * mat[2][3],
		mat[2][0] * mat[1][1] * mat[0][3] - mat[1][0] * mat[2][1] * mat[0][3] - mat[2][0] * mat[0][1] * mat[1][3] + mat[0][0] * mat[2][1] * mat[1][3] + mat[1][0] * mat[0][1] * mat[2][3] - mat[0][0] * mat[1][1] * mat[2][3],
		mat[1][0] * mat[2][1] * mat[0][2] - mat[2][0] * mat[1][1] * mat[0][2] + mat[2][0] * mat[0][1] * mat[1][2] - mat[0][0] * mat[2][1] * mat[1][2] - mat[1][0] * mat[0][1] * mat[2][2] + mat[0][0] * mat[1][1] * mat[2][2]) * deter;

	return temp;
}

inline float	satMat4::Determinant()	const //Compressed Version
{
	satVec3 det = (
		mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0],
		mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0],
		mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);

	return mat[0][0] * det[0] - mat[0][1] * det[1] + mat[0][2] * det[2];
}

inline float	satMat4::GetDeterminant() const //Expanded version
{
	// 2x2 sub-determinants
	float det2_01_01 = mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0];
	float det2_01_02 = mat[0][0] * mat[1][2] - mat[0][2] * mat[1][0];
	float det2_01_03 = mat[0][0] * mat[1][3] - mat[0][3] * mat[1][0];
	float det2_01_12 = mat[0][1] * mat[1][2] - mat[0][2] * mat[1][1];
	float det2_01_13 = mat[0][1] * mat[1][3] - mat[0][3] * mat[1][1];
	float det2_01_23 = mat[0][2] * mat[1][3] - mat[0][3] * mat[1][2];

	// 3x3 sub-determinants
	float det3_201_012 = mat[2][0] * det2_01_12 - mat[2][1] * det2_01_02 + mat[2][2] * det2_01_01;
	float det3_201_013 = mat[2][0] * det2_01_13 - mat[2][1] * det2_01_03 + mat[2][3] * det2_01_01;
	float det3_201_023 = mat[2][0] * det2_01_23 - mat[2][2] * det2_01_03 + mat[2][3] * det2_01_02;
	float det3_201_123 = mat[2][1] * det2_01_23 - mat[2][2] * det2_01_13 + mat[2][3] * det2_01_12;

	return (-det3_201_123 * mat[3][0] + det3_201_023 * mat[3][1] - det3_201_013 * mat[3][2] + det3_201_012 * mat[3][3]);

	//return (
	//	mat[0][3] * mat[1][2] * mat[2][1] * mat[3][0] - mat[0][2] * mat[1][3] * mat[2][1] * mat[3][0] - mat[0][3] * mat[1][1] * mat[2][2] * mat[3][0] + mat[0][1] * mat[1][3] * mat[2][2] * mat[3][0] +
	//	mat[0][2] * mat[1][1] * mat[2][3] * mat[3][0] - mat[0][1] * mat[1][2] * mat[2][3] * mat[3][0] - mat[0][3] * mat[1][2] * mat[2][0] * mat[3][1] + mat[0][2] * mat[1][3] * mat[2][0] * mat[3][1] +
	//	mat[0][3] * mat[1][0] * mat[2][2] * mat[3][1] - mat[0][0] * mat[1][3] * mat[2][2] * mat[3][1] - mat[0][2] * mat[1][0] * mat[2][3] * mat[3][1] + mat[0][0] * mat[1][2] * mat[2][3] * mat[3][1] +
	//	mat[0][3] * mat[1][1] * mat[2][0] * mat[3][2] - mat[0][1] * mat[1][3] * mat[2][0] * mat[3][2] - mat[0][3] * mat[1][0] * mat[2][1] * mat[3][2] + mat[0][0] * mat[1][3] * mat[2][1] * mat[3][2] +
	//	mat[0][1] * mat[1][0] * mat[2][3] * mat[3][2] - mat[0][0] * mat[1][1] * mat[2][3] * mat[3][2] - mat[0][2] * mat[1][1] * mat[2][0] * mat[3][3] + mat[0][1] * mat[1][2] * mat[2][0] * mat[3][3] +
	//	mat[0][2] * mat[1][0] * mat[2][1] * mat[3][3] - mat[0][0] * mat[1][2] * mat[2][1] * mat[3][3] - mat[0][1] * mat[1][0] * mat[2][2] * mat[3][3] + mat[0][0] * mat[1][1] * mat[2][2] * mat[3][3]);
}

inline satMat4	satMat4::TransposeMultiply(const satMat4 &m) const
{
	return satMat4(
		mat[0][0] * m[0][0] + mat[1][0] * m[1][0] + mat[2][0] * m[2][0] + mat[3][0] * m[3][0],
		mat[0][0] * m[0][1] + mat[1][0] * m[1][1] + mat[2][0] * m[2][1] + mat[3][0] * m[3][1],
		mat[0][0] * m[0][2] + mat[1][0] * m[1][2] + mat[2][0] * m[2][2] + mat[3][0] * m[3][2],
		mat[0][0] * m[0][3] + mat[1][0] * m[1][3] + mat[2][0] * m[2][3] + mat[3][0] * m[3][3],
		mat[0][1] * m[0][0] + mat[1][1] * m[1][0] + mat[2][1] * m[2][0] + mat[3][1] * m[3][0],
		mat[0][1] * m[0][1] + mat[1][1] * m[1][1] + mat[2][1] * m[2][1] + mat[3][1] * m[3][1],
		mat[0][1] * m[0][2] + mat[1][1] * m[1][2] + mat[2][1] * m[2][2] + mat[3][1] * m[3][2],
		mat[0][1] * m[0][3] + mat[1][1] * m[1][3] + mat[2][1] * m[2][3] + mat[3][1] * m[3][3],
		mat[0][2] * m[0][0] + mat[1][2] * m[1][0] + mat[2][2] * m[2][0] + mat[3][2] * m[3][0],
		mat[0][2] * m[0][1] + mat[1][2] * m[1][1] + mat[2][2] * m[2][1] + mat[3][2] * m[3][1],
		mat[0][2] * m[0][2] + mat[1][2] * m[1][2] + mat[2][2] * m[2][2] + mat[3][2] * m[3][2],
		mat[0][2] * m[0][3] + mat[1][2] * m[1][3] + mat[2][2] * m[2][3] + mat[3][3] * m[3][3],
		mat[0][3] * m[0][0] + mat[1][3] * m[1][0] + mat[2][3] * m[2][0] + mat[3][3] * m[3][0],
		mat[0][3] * m[0][1] + mat[1][3] * m[1][1] + mat[2][3] * m[2][1] + mat[3][3] * m[3][1],
		mat[0][3] * m[0][2] + mat[1][3] * m[1][2] + mat[2][3] * m[2][2] + mat[3][3] * m[3][2],
		mat[0][3] * m[0][3] + mat[1][3] * m[1][3] + mat[2][3] * m[2][3] + mat[3][3] * m[3][3]);
}


// TRANSLATIONS

inline void satMat4::Translate(float x, float y, float z)
{
	(*this) = Translation(x, y, z) * (*this);
	//TranslateX(x);
	//TranslateY(y);
	//TranslateZ(z);
}

inline void satMat4::Translate(const satVec3 &pos)
{
	(*this) = Translation(pos) * (*this);
	//Translate(pos.x, pos.y, pos.z);
}

inline void satMat4::TranslateInstance(const float &x, const float &y, const float &z)
{
	(*this) = satMat4(
		1.f, 0, 0, x,
		0, 1.f, 0, y,
		0, 0, 1.f, z,
		0, 0, 0, 1.f);
}

inline void satMat4::TranslateInstance(const satVec3 &vec)
{
	(*this) = satMat4(
		1.f, 0, 0, vec.x,
		0, 1.f, 0, vec.y,
		0, 0, 1.f, vec.z,
		0, 0, 0, 1.f);
}

inline void satMat4::TranslateX(float x)
{
	(*this) = Translation(x, 0, 0) * (*this);
}

inline void satMat4::TranslateY(float y)
{
	(*this) = Translation(0, y, 0) * (*this);
}

inline void satMat4::TranslateZ(float z)
{
	(*this) = Translation(0, 0, z) * (*this);
}

inline satMat4 satMat4::Translation(float x, float y, float z)
{
	return satMat4(
		1.f, 0, 0, x,
		0, 1.f, 0, y,
		0, 0, 1.f, z,
		0, 0, 0, 1.f);
}

inline satMat4 satMat4::Translation(satVec3 vec)
{
	return satMat4(
		1.f, 0, 0, vec.x,
		0, 1.f, 0, vec.y,
		0, 0, 1.f, vec.z,
		0, 0, 0, 1.f);
}

inline satMat4 satMat4::TranslationInstance(float x, float y, float z)
{
	return satMat4(
		1.f, 0, 0, x,
		0, 1.f, 0, y,
		0, 0, 1.f, z,
		0, 0, 0, 1.f);
}

inline satMat4 satMat4::TranslationInstance(satVec3 vec)
{
	return satMat4(
		1.f, 0, 0, vec.x,
		0, 1.f, 0, vec.y,
		0, 0, 1.f, vec.z,
		0, 0, 0, 1.f);
}