/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include "Math.h"

class satVec3;
class satVec4;
class satMat3;
class satMat4;

/*
//=======================================================================================
//
//	satQuat - Quaternion
//
//=======================================================================================
*/

class satQuat
{
public:
	
	union
	{
		struct
		{
			satVec4 qt;
		};

		struct
		{
			satVec3 v;
			float w;
		};

		struct
		{
			float x, y, z, w;
		};

		struct
		{
			float a, b, c, d;
		};
	};

	satQuat();
	satQuat(float x);
	satQuat(float x, float y, float z, float w);
	//satQuat(float degreets, satVec3 v);
	satQuat(satVec3 v);
	satQuat(satVec4 v);

	satQuat(float angleRad, satVec3 axis);

	satQuat		operator*(const satQuat &b) const;
	float		magnitude(satQuat a);
	satQuat		normalize(satQuat a);
	satVec3		rotateVector(satQuat q, const satVec3 b);

	float		operator[](int index) const;
	float &		operator[](int index);

	satQuat		operator-() const;
	satQuat &	operator=(const satQuat &a);
	satQuat		operator+(const satQuat &a) const;
	satQuat &	operator+=(const satQuat &a);
	satQuat		operator-(const satQuat &a) const;
	satQuat &	operator-=(const satQuat &a);
	//satQuat		operator*(const satQuat &a) const;
	satVec3		operator*(const satVec3 &a) const;
	satQuat		operator*(float a) const;
	satQuat &	operator*=(const satQuat &a);
	satQuat &	operator*=(float a);
	satQuat		operator/(float a);
	satQuat &	operator/=(float a);

	friend satQuat	operator*(const float a, const satQuat &b);
	friend satVec3	operator*(const satVec3 &a, const satQuat &b);

	bool			Equals(const satQuat &a) const;								
	bool			Equals(const satQuat &a, const float epsilon) const;	
	bool			operator==(const satQuat &a) const;							
	bool			operator!=(const satQuat &a) const;		

	satQuat Inverse() const;
	float Length() const;
	satQuat & Normalize();

	float Dot(const satQuat &a) const;

	void satQuat::SLerp(const satQuat &v0, const satQuat &v1, const float t);
	void satQuat::SLerpNoBounds(const satQuat &v0, const satQuat &v1, const float t);


	satMat3 satQuat::ToMat3() const;
	satMat4 satQuat::ToMat4() const;
};