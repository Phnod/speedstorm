/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.

#include "GlobalSettings.h"
#include "Math.h"
#include "Color.h"

#include <iostream>
#include <stdio.h>
#include <sstream>
#include <fstream>

#define elf else if
#define CHAR_BUFFER_SIZE 128 //Size of the character buffer.


namespace GlobalSettings
{
	ShipSettings ship;
	SongSettings song;

	ShipSettings getShipSettings()
	{
		return ship;
	}
	
	SongSettings getSongSettings()
	{
		return song;
	}

	bool GlobalSettings::initSettings()
	{	
		std::ifstream inFile("../assets/Settings/Settings.ini");
	
		char line[CHAR_BUFFER_SIZE];
		char lineChar[CHAR_BUFFER_SIZE];
		std::string lineString;
		int lineNumber = 0;
	
		
		float x, y, z, w;
		int ix, iy, iz, iw;
		unsigned int ux, uy, uz, uw;
	
		while (!inFile.eof())
		{
			++lineNumber;
	
			inFile.getline(line, CHAR_BUFFER_SIZE);
			if (!strncmp(line, "asdfasdfasdfasdf: ", 10))
			{
				std::cout << "Wow I didn't expect that" << std::endl;
			}
			elf(!strncmp(line, "Song", 4))
			{
				if (!strncmp(line, "SongOverride: ", 4+8+1))
				{
					std::sscanf(line, "SongOverride: %u\n", &uw);
					SAT_LOG_SAFE("SONG OVERRIDE %u", uw);
					song.Override = uw;
				}
				elf(!strncmp(line, "SongOverrideName: ", 4+8+4+1))
				{
					std::sscanf(line, "SongOverrideName: %s\n", &lineChar);
					SAT_LOG_SAFE("SONG OVERRIDE NAME %s", lineChar);
					song.Filename = lineChar;
				}
				elf(!strncmp(line, "SongOverrideTempo: ", 4+8+5+1))
				{
					std::sscanf(line, "SongOverrideTempo: %u", &uw);				
					SAT_LOG_SAFE("SONG OVERRIDE TEMPO %u\n", uw);
					song.Tempo = uw;
				}
			}
			elf(!strncmp(line, "fdsa", 4))
			{
				std::cout << "Wow I didn't expect that either" << std::endl;
			}
		}
	
		return true;
	}
}

ShipSettings::ShipSettings()
{
	Texture.resize(4);
	NormalTexture.resize(4);
	EmissiveTexture.resize(4);
	InvincibleTexture.resize(4);
	ShinyTexture.resize(4);

	Texture[0] = "../Assets/Textures/ShipPlayer/Texture_0.png";
	Texture[1] = "../Assets/Textures/ShipPlayer/Texture_1.png";
	Texture[2] = "../Assets/Textures/ShipPlayer/Texture_2.png";
	Texture[3] = "../Assets/Textures/ShipPlayer/Texture_3.png";
	NormalTexture[0] = "../Assets/Textures/ShipPlayer/Normal_0.png";
	NormalTexture[1] = "../Assets/Textures/ShipPlayer/Normal_1.png";
	NormalTexture[2] = "../Assets/Textures/ShipPlayer/Normal_2.png";
	NormalTexture[3] = "../Assets/Textures/ShipPlayer/Normal_3.png";
	EmissiveTexture[0] = "../Assets/Textures/ShipPlayer/Emissive_0.png";
	EmissiveTexture[1] = "../Assets/Textures/ShipPlayer/Emissive_1.png";
	EmissiveTexture[2] = "../Assets/Textures/ShipPlayer/Emissive_2.png";
	EmissiveTexture[3] = "../Assets/Textures/ShipPlayer/Emissive_3.png";
	InvincibleTexture[0] = "../Assets/Textures/ShipPlayer/Emissive_0INV.png";
	InvincibleTexture[1] = "../Assets/Textures/ShipPlayer/Emissive_1INV.png";
	InvincibleTexture[2] = "../Assets/Textures/ShipPlayer/Emissive_2INV.png";
	InvincibleTexture[3] = "../Assets/Textures/ShipPlayer/Emissive_3INV.png";
	ShinyTexture[0] = "../Assets/Textures/ShipPlayer/Shiny_0.png";
	ShinyTexture[1] = "../Assets/Textures/ShipPlayer/Shiny_1.png";
	ShinyTexture[2] = "../Assets/Textures/ShipPlayer/Shiny_2.png";
	ShinyTexture[3] = "../Assets/Textures/ShipPlayer/Shiny_3.png";
}