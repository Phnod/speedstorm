/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "IncludeGL.h"
#include <string>

class FrameBuffer
{
public:
	FrameBuffer() = delete; //Cause compile error if we use the default constructor.
	FrameBuffer(unsigned numColorAttachments);
	~FrameBuffer();

	void InitDepthTexture(unsigned width, unsigned height);
	void InitDepthTexture(unsigned width, unsigned height, GLint filter, GLint wrap = GL_CLAMP_TO_EDGE);
	void InitColorTexture(unsigned index, unsigned width, unsigned height, GLint internalFormat = GL_RGBA8, GLint filter = GL_LINEAR, GLint wrap = GL_CLAMP_TO_EDGE);
	bool CheckFBO();

	//Clears all openGL memory
	void Unload();
	
	//Clears all attached textures
	void Clear();

	//Inbetween of previous two
	void Reset(unsigned numColorAttachments);
	void Reset();

	void DrawFromSource(unsigned textureHandle);
	void DrawFromSource(unsigned textureHandle1, unsigned textureHandle2);

	static void DrawToBackFromSource(unsigned textureHandle);
	static void DrawToBackFromSource(unsigned textureHandle1, unsigned textureHandle2);

	void bind() const;
	static void unbind();

	void MoveToBackBuffer(int windowWidth, int windowHeight);

	void CopyFromFramebuffer(unsigned index, FrameBuffer *source, unsigned sourceIndex);
	void CopyDepthFromFramebuffer(FrameBuffer *source);

	GLuint GetDepthHandle() const;
	unsigned GetDepthWidth() const;
	unsigned GetDepthHeight() const;
	GLuint GetDepthFilter() const;
	GLuint GetDepthWrap() const;
	GLuint GetColorHandle(unsigned index) const;
	unsigned GetColorWidth(unsigned index) const;
	unsigned GetColorHeight(unsigned index) const;
	GLuint GetColorFormat(unsigned index) const;
	GLuint GetColorFilter(unsigned index) const;
	GLuint GetColorWrap(unsigned index) const;

	void PrintInfo();

	//
	static void InitFullScreenQuad();
	static void DrawFullScreenQuad();

private:
	GLuint _FBO = GL_NONE;
	GLuint _DepthAttachment = GL_NONE;
	GLuint *_ColorAttachments = nullptr;
	GLenum *_Bufs = nullptr;

	unsigned _DepthWidth = 0;
	unsigned _DepthHeight = 0;
	GLint	_DepthFilter = GL_NONE;
	GLint	_DepthWrap = GL_NONE;

	unsigned *_ColorWidth = nullptr;
	unsigned *_ColorHeight = nullptr;
	GLint *	_ColorFormat = nullptr;
	GLint * _ColorFilter = nullptr;
	GLint * _ColorWrap = nullptr;

	static GLuint FullScreenQuadVAO;
	static GLuint FullScreenQuadVBO;

	bool IsLoaded_ = false;
	bool DepthTestActive = false;

	unsigned _NumColorAttachments = 0;

};