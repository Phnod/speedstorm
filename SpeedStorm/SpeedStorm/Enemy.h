/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <vector>
#include <memory>
#include "Mesh.h"
#include "Math.h"

//Still need to be done
#include "Sound.h"
#include "Light.h"
#include "Model.h"
#include "Object.h"
#include "level.h"

class Game;

//#include "ResourceManager.h"
//#include "Utilities.h"

enum EnemyType
{
	Dice = 0,
	ChessBishop,
	ChessKing,
	ChessQueen,
	Paper
};

class Enemy //: public Object
{
public:
	EnemyType enemyType;

	Enemy();
	Enemy(Mesh * model, Texture * tex);
	Enemy(Mesh * model, Texture * tex, satVec3 acc, satVec3 vel, satVec3 pos, int health);

	satVec3 acceleration;
	satVec3 velocity;
	satVec3 position;
	satVec3 positionOld;
	int life;
	Game* game;
	Mesh* mesh;
	Texture* texture;
	Level* level;

	void update();
	virtual void process();


	
};