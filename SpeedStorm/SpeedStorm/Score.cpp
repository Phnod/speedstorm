/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Score.h"

#define SCORE_PATH "../assets/Other/hiScores.txt"

Score::Score()
{
	currScore = 0;
	hiScore = 0;
	totScore = 0;
}

Score::~Score()
{

}

int Score::readScore(const int &place, const int &level)
{

	// Open file
	std::ifstream infile(SCORE_PATH, std::ios::binary);
	assert(infile && "'hiScores.txt' NOT FOUND");


	if (infile)
	{
		//Will eventually hold desired score
		std::string currLine;
		int score = 0;

		//How much of the document to skip (to get to what we want)
		int skipAmt;
		//Magic #'s: 12 == # of spaces to next 'level block'. 10 == amount of hi scores per block  
		skipAmt = (12 * level) + (place);

		//std::cout << "Reading score from file\n" << std::endl;

		//Reading from the file
		//skipping the useless lines
		for (int i = 0; i < skipAmt; ++i)
			std::getline(infile, currLine, '\n');

		//saving desired score as a string
		std::getline(infile, currLine, '\n');

		infile.close();


		//Converting the string score into an int score
		std::stringstream stream;
		stream << currLine;
		stream >> score;
		stream.str(""); stream.clear();

		return score;
	}

	std::cout << "JUST MESS MY STUFF UP TBH FAM" << std::endl;
	return 0;
}

std::vector<int> Score::readFullLevel(const int &level)
{

	// Open file
	std::ifstream infile(SCORE_PATH, std::ios::binary);
	assert(infile && "'hiScores.txt' NOT FOUND");


	if (infile)
	{
		//Will eventually hold desired score
		std::string currLine;
		int score = 0;
		std::vector<int> scoreList;

		//For the string-int conversion
		std::stringstream stream;

		//How much of the document to skip (to get to what we want)
		int skipAmt;
		//Magic #'s: 12 == # of spaces to next 'level block'. 10 == amount of hi scores per block  
		skipAmt = (12 * level);

		//std::cout << "Reading score from file" << std::endl;

		//skipping the title in the file
		std::getline(infile, currLine, '\n');

		//Skipping the other levels' scores
		for (int i = 0; i < skipAmt; ++i)
		{
			std::getline(infile, currLine, '\n');
		}
		//Reading the desired level's 10 scores
		for (int i = 0; i < 10; ++i)
		{
			std::getline(infile, currLine, '\n');
			//Converting the string score into an int score
			stream << currLine;
			stream >> score;
			stream.str(""); stream.clear();
			scoreList.push_back(score);
		}
		infile.close();

		return scoreList;
	}

	std::cout << "JUST MESS MY STUFF UP TBH FAM" << std::endl;
	std::vector<int> scoreList;
	return scoreList;
}

bool Score::enterScore(const int &score, const int &level)
{
	//Checks if players score even places on the charts. 
	if (score <= readScore(10, level))
	{
		//std::cout << "Filthy casual" << std::endl;;
		return false;
	}
	else
	{
		//Opening the file
		std::ifstream outfile(SCORE_PATH);

		//Creates a vector of scores in their new order:
		std::vector<int> scoreList = readFullLevel(level);
		for (int i = 0; i < 10; ++i)
		{
			if (score > scoreList.at(i))
			{
				scoreList.insert(scoreList.begin() + i, score);
				scoreList.pop_back();
				i = 10;
			}
		}
		//Replaces each line of score for given level in txt file:

		// Open file
		std::ifstream infile(SCORE_PATH);
		assert(infile && "'hiScores.txt' NOT FOUND");


		if (infile)
		{
			std::string currLine;
			std::stringstream stream;
			std::string tempNum;

			//std::cout << "Saving. Do not remove memory card in Slot A or press power button." << std::endl;

			//We gonna stick every line of this txt into a vector of strings.
			//Yeah, I'm sure it's a cruddy way to replace a line, but this wat I got. Simplest I can think at least.
			std::vector<std::string> modList;

			//First, takes in everything before our current level:
			int before = (12 * level);
			std::getline(infile, currLine, '\n'); //Getting down the first line (makes the rest esier)
			modList.push_back(currLine);
			for (int i = 0; i < before; ++i)
			{
				std::getline(infile, currLine, '\n');
				modList.push_back(currLine);
			}

			//Next, goes through our level's scores:
			for (int i = 0; i < 10; ++i)
			{
				std::getline(infile, currLine, '\n'); //We're not really using this. It's just here to skip these lines.
				stream << scoreList.at(i);
				stream >> tempNum;
				modList.push_back(tempNum);
				stream.str(""); stream.clear();
			}

			//Finally, takes in everything after our current level:
			while (!infile.eof())
			{
				std::getline(infile, currLine, '\n');
				//modList.push_back(currLine);
				if (!infile.eof())
				{
					modList.push_back(currLine);
				}
			}


			infile.close();


			//Now, our string vector will be used to overwrite the score txt
			std::ofstream outfile(SCORE_PATH, std::ios::out);
			if (!outfile)
			{
				std::cout << "something went horribly wrong in writing hiScore file" << std::endl;
			}
			else
			{
				for (unsigned i = 0; i < modList.size(); ++i)
				{
					outfile << modList.at(i) << std::endl;
				}
			}
			outfile.close();
			//Returning true, cause you know, our score totally made it into the list. Whoo.
			return true;
		}
	}
	return true;
}


bool Score::resetHiScore(const int &level, const int &score)
{
	{
		//Opening the file
		std::ifstream outfile(SCORE_PATH);

		//Creates a vector of scores in their new order:
		std::vector<int> scoreList = readFullLevel(level);
		for (int i = 0; i < 10; ++i)
		{
			if (score > scoreList.at(i))
			{
				scoreList.insert(scoreList.begin() + i, score);
				scoreList.pop_back();
				i = 10;
			}
		}
		//Replaces each line of score for given level in txt file:

		// Open file
		std::ifstream infile(SCORE_PATH);
		assert(infile && "'hiScores.txt' NOT FOUND");


		if (infile)
		{
			std::string currLine;
			std::stringstream stream;
			std::string tempNum;

			//std::cout << "Saving. Do not remove memory card in Slot A or press power button." << std::endl;

			//We gonna stick every line of this txt into a vector of strings.
			//Yeah, I'm sure it's a cruddy way to replace a line, but this wat I got. Simplest I can think at least.
			std::vector<std::string> modList;

			//First, takes in everything before our current level:
			int before = (12 * level);
			std::getline(infile, currLine, '\n'); //Getting down the first line (makes the rest easier)
			modList.push_back(currLine);
			for (int i = 0; i < before; ++i)
			{
				std::getline(infile, currLine, '\n');
				modList.push_back(currLine);
			}

			//Next, goes through our level's scores:
			for (int i = 0; i < 10; ++i)
			{
				std::getline(infile, currLine, '\n'); //We're not really using this. It's just here to skip these lines.
				stream << scoreList.at(i);
				stream >> tempNum;
				//modList.push_back(std::to_string(score - (score * i / 10)));
				modList.push_back(std::to_string(score / (int)powf(2.0f, (float)i)));
				stream.str(""); stream.clear();
			}

			//Finally, takes in everything after our current level:
			while (!infile.eof())
			{
				std::getline(infile, currLine, '\n');
				if (!infile.eof())
				{
					modList.push_back(currLine);
				}
			}

			infile.close();


			//Now, our string vector will be used to overwrite the score txt
			std::ofstream outfile(SCORE_PATH, std::ios::out);
			if (!outfile)
			{
				std::cout << "something went horribly wrong in writing hiScore file" << std::endl;
			}
			else
			{
				for (unsigned i = 0; i < modList.size(); ++i)
				{
					outfile << modList.at(i) << std::endl;
				}
			}
			outfile.close();
			//Returning true, cause you know, our score totally made it into the list. Whoo.
			return true;
		}
	}
	return true;
}