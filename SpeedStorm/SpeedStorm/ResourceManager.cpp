/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "ResourceManager.h"
//#include "File.h"

//namespace sat
//{
	namespace ResourceManager
	{
		std::vector<Mesh*> Meshes;
		std::vector<Texture*> Textures;
		std::vector<ShaderProgram*> Shaders;


		Mesh* AddMesh(const std::string &meshFile, bool isBinary)
		{
			int id = StringToID(meshFile);

			
		
			int min = 0;
			int max = (int)Meshes.size() - 1;
			int mid = 0;
		
			if (max > 0)
			{
				//std::cout << "There's a chance! " << std::endl;

				while (min <= max)
				{
					mid = min + (max - min) / 2;

					//std::cout << id << "\t" << Meshes[mid]->GetID() << "\t" << std::endl;
		
					if (Meshes[mid]->GetID() < id)
					{
						min = mid + 1;
					}
					else if (Meshes[mid]->GetID() > id)
					{
						max = mid - 1;
					}
					else
					{	
						//if(Meshes[mid]->loaded)
						//{
						//	LPFILETIME createTime;
						//	LPFILETIME lastReadTime;
						//	LPFILETIME lastWriteTime;
						//
						//	bool result = SatGetFileTime(meshFile, createTime, lastReadTime, lastWriteTime);
						//
						//	if(result == true)
						//	{
						//		LONG compare = CompareFileTime(lastWriteTime, Meshes[mid]->lastWriteTime);
						//		if (compare > 0)
						//		{
						//			std::cout << "File Updated!" << std::endl;
						//			Meshes[mid]->Unload();
						//			delete Meshes[mid];
						//			Meshes[mid] = new Mesh(meshFile, isBinary);
						//			return Meshes[mid];
						//		}
						//	}
						//	else
						//	{
						//		std::cout << "UH OH" << std::endl;
						//	}
						//}
						//printf("Duplicate Mesh Found!\n");
						return Meshes[mid];
					}
				}
		
				if (Meshes[mid]->GetID() < id)
					++mid;
			}
		
			std::cout << id << "\t" << meshFile << std::endl;
			Mesh* newMesh = new Mesh(meshFile, isBinary);
			//MeshPtr newMesh = std::make_shared<Mesh>(meshFile, isBinary);
			Meshes.insert(Meshes.begin() + mid, newMesh); // insert while keeping sorted
			return newMesh;
		}
		
		Texture* AddTexture(const std::string &imageFile)
		{
			int id = StringToID(imageFile);
		
			int min = 0;
			int max = (int)Textures.size() - 1;
			int mid = 0;
		
			if (max > 0)
			{
				//std::cout << "There's a chance! " << std::endl;

				while (min <= max)
				{
					mid = min + (max - min) / 2;
		
					if (Textures[mid]->GetID() < id)
						min = mid + 1;
					else if (Textures[mid]->GetID() > id)
						max = mid - 1;
					else
					{	
						//printf("Duplicate Texture Found!\n");
						return Textures[mid];
					}
				}
		
				if (Textures[mid]->GetID() < id)
					++mid;
			}
			
			std::cout << id << "\t" << imageFile << std::endl;
			Texture* newTex = new Texture(imageFile);
			//Texture* newTex = new Texture();
			//newTex->Load(imageFile);
			//Texture* newTex = std::make_shared<Texture>(imageFile);
			Textures.insert(Textures.begin() + mid, newTex);
			return newTex;
		}

		ShaderProgram* AddShader(ShaderProgram * shader)
		{
			Shaders.push_back(shader);
			return shader;
		}

		void reloadShaders()
		{
			for (int i = 0; i < Shaders.size(); ++i)
			{
				Shaders[i]->reload();
			}
		}
		
		

	}
//}