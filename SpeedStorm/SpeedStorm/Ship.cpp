/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Ship.h"

Ship::Ship()
{
	maxShipHealth = 3;
	numberOfAssets = maxShipHealth + 1;
	shipHealth = maxShipHealth;


}

bool Ship::setFilename(const enum ShipAsset type, const int &index, const std::string &name)
{
	std::vector<std::string> *vectorPtr; //We use this to tell it what vector filename to change
	
	if(type == diffuseTexture)
	{
		vectorPtr = &diffuseFilename;
	}
	else if(type == normalTexture)
	{
		vectorPtr = &normalFilename;
	}
	else if(type == specularTexture)
	{
		vectorPtr = &specularFilename;
	}
	else if(type == emissiveTexture)
	{
		vectorPtr = &emissiveFilename;
	}
	else if(type == emissiveInvincibleTexture)
	{
		vectorPtr = &emissiveInvincibleFilename;
	}
	else if(type == meshModel)
	{
		vectorPtr = &meshFilename;
	}
	else
	{
		printf("ERROR: asset type does not exist?\n");
		return false;
	}

	if(index < vectorPtr->size() && index >= 0)
	{
		(*vectorPtr)[index] = name;
	}
	else
	{
		printf("ERROR: %i is out of range 0-%i \n", index, vectorPtr->size() - 1);
		return false;
	}
	
	return true;
}

bool Ship::setCopyAfterFilename(const enum ShipAsset type, const int &index, const int &end, const std::string &name)
{
	std::vector<std::string> *vectorPtr; //We use this to tell it what vector filename to change
	
	if(type == diffuseTexture)
	{
		vectorPtr = &diffuseFilename;
	}
	else if(type == normalTexture)
	{
		vectorPtr = &normalFilename;
	}
	else if(type == specularTexture)
	{
		vectorPtr = &specularFilename;
	}
	else if(type == emissiveTexture)
	{
		vectorPtr = &emissiveFilename;
	}
	else if(type == emissiveInvincibleTexture)
	{
		vectorPtr = &emissiveInvincibleFilename;
	}
	else if(type == meshModel)
	{
		vectorPtr = &meshFilename;
	}
	else
	{
		printf("ERROR: asset type does not exist?\n");
		return false;
	}

	
	if(index < vectorPtr->size() && index >= 0)
	{
		(*vectorPtr)[index] = name;

		for(int i = 0; i < end; ++i)
		{
			if(index + i < vectorPtr->size())
			{
				(*vectorPtr)[index + i] = name;
			}
		}
		return true;
	}
	else
	{
		printf("ERROR: %i is out of range 0-%i \n", index, vectorPtr->size() - 1);
		return false;
	}
	
	return true;
}

