/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "DebugLog.h"
#include <string>
#include <vector>
//This is where we'll read a file that allows anyone to modify a lot of game aspects without breaking code

class SongSettings
{
public:

	std::string Filename;
	bool Override = false;
	int Tempo = 120;
};

class ShipSettings
{
public:
	ShipSettings();

	std::vector<std::string> Texture;
	std::vector<std::string> NormalTexture;
	std::vector<std::string> EmissiveTexture;
	std::vector<std::string> InvincibleTexture;
	std::vector<std::string> ShinyTexture;
};

namespace GlobalSettings
{
	//GlobalSettings();
	bool initSettings();
	ShipSettings getShipSettings();
	SongSettings getSongSettings();
	
};

//namespace GS = GlobalSettings;