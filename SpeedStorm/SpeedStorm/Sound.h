/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#pragma comment(lib, "./fmodex_vc.lib")

#include "./FMOD/fmod.hpp"
#include "./FMOD/fmod_errors.h"
#include "Vector.h"

#include <windows.h>
#include <stdio.h>
#include <string>
#include <memory>
#include <iostream>
#include <math.h>
#include <conio.h>



const float DISTANCEFACTOR = 1.0f; //For doppler effect

class SoundSystem
{
public:
	SoundSystem();
	~SoundSystem();

	void init();
	FMOD_VECTOR listenerPos;
	FMOD_VECTOR listenerVel;
	FMOD_VECTOR forward;
	FMOD_VECTOR up;

	void update(satVec3 &pos, satVec3 &vel, satVec3 &forward, satVec3 &up);

	FMOD::System *system;
	int numDrivers;
	bool listenerFlag = true;
	unsigned int version;
	FMOD_SPEAKERMODE speakermode;
	FMOD_CAPS caps;
	char name[256];
		
};

class SoundLoc;
typedef std::shared_ptr<SoundLoc> SoundLocPtr;

class SoundLoc
{
public:
	SoundLoc();

	bool loadFromFile(std::string file, bool is3D, bool loop);
	void release();
	void playSound();
	void playSound(float freqMod);
	void playSound(float volume, float freqMult, float freqRandom);

	void stopSound();
	bool isPlaying();
	void update();

	void changePitch(float freq);
	void changeVolume(float volume);

	void initDSPHighpass();
	void setDSPHighpass(const float &param);

	void initDSPLowpass();
	void setDSPLowpass(const float &param);

	void initDSPFlange();
	void setDSPFlange(const float &param);
	void setDSPFlange(const float &param, const float &rate, const float &depth = 0.0f);

	void initDSPEcho();
	void setDSPEcho(const float &param, const float &decay = 0.5f);

	bool is3DSound = true;
	bool loaded = false;

	static SoundSystem *SManager;
	FMOD_VECTOR soundPos;
	FMOD_VECTOR soundVel;
	FMOD_VECTOR zeroVec;
	
	FMOD::DSP *pitchShift;
	FMOD::DSP
		*dspReverb,
		*dspEcho,
		*dsp,
		*dspFlange,
		*dspLow,
		*dspHigh;

	bool dspAdded = false;

	FMOD::Sound *sound;
	FMOD::Channel *channel = 0;
};

