/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
//
//#include <vector>
//#include <memory>
//#include "Mesh.h"
//#include <SFML\Graphics.hpp>
//#include "Vector.h"
//#include "Matrix.h"
//#include "Timer.h"
//#include "Texture.h"
//#include "ShaderProgram.h"
//#include "FrameBuffer.h"
//#include "Node.h"
//
////Still need to be done
//#include "Sound.h"
//#include "Light.h"
//#include "Model.h"
//
////#include "Matrix.h"
//
//#include "ResourceManager.h"
//#include "Utilities.h"
//
//class Entity;
//
//class Entity : public Model
//{
//public:
//	Entity(ResourceManager* resources, RenderManager* renderer);
//
//	virtual void Process() override;
//
//	virtual void RotateX(float degrees) override;
//	virtual void RotateY(float degrees) override;
//	virtual void RotateZ(float degrees) override;
//
//	virtual void SetMesh(const std::string &meshFile, bool isBinaryMesh) override;
//
//	
//
//	void ScaleBBUp(float f);
//	void ScaleBBDown(float f);
//	void ScaleBBLeft(float f);
//	void ScaleBBRight(float f);
//	void ScaleBBNear(float f);
//	void ScaleBBFar(float f);
//
//	Mesh * BBoxMesh;
//	Texture * BBoxTexture;
//	ShaderProgram * BBoxShaderProgram;
//
//	
//
//	
//
//	satVec3 GetBBoxMax();
//	satVec3 GetBBoxMin();
//
//	satVec3 topCorn; //Bounding box corner
//	satVec3 btmCorn; //Bounding box corner
//
//	bool SetBB(const std::string & file);
//
//	//make checkpoint functions for Pejman eventually
//	//void GoToCheckpoint();
//	//void SetCheckpoint(satVec3 position);
//
//	satVec3 Velocity;
//
//	bool HasCollisionUp = false;
//	bool HasCollisionDown = false;
//	bool HasCollisionRight = false;
//	bool HasCollisionLeft = false;
//	bool IsVisible = true;
//	
//	bool BBoxIsVisible = false;
//protected:
//	bool SetBB();
//	void DrawBB(); //outline to debug bounding boxes
//	bool isBinaryMesh = false;
//private:
//};
//
//enum typeEnum
//{
//	//main typing
//	unknown = 0,
//	mainType,
//	object,
//	thing,
//
//	//subtype
//	subType,
//	player,
//	HUD,
//	wall,
//	enemy,
//	checkpoint,
//	bulletP,
//	bulletE,
//
//	//names
//	name
//};
//