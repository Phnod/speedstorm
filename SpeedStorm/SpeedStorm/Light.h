/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Vector.h"
#include "Color.h"

class ShaderProgram;
class Light;
class LightAtt;
class Material;

class Light
{
public:
	Light();
	Light(const satColor &_ambient, const satColor &_emissive, const satColor &_diffuse  = 1.0f, const satColor &_specular = 1.0f);
	Light(const satColor &_color);
	~Light();

	float lightFloat();
	static float lightFloat(const Light &light);

	satColor ambient;
	satColor emissive;
	satColor diffuse;
	satColor specular;
};

class LightAtt
{
public:
	LightAtt();
	~LightAtt();

	satVec3 position = { 0.0f, 0.0f, 0.0f };
	satVec3 ambient = { 0.5f, 0.5f, 0.5f };
	satVec3 diffuse = { 1.0f, 1.0f, 1.0f };
	satVec3 specular = { 1.0f, 1.0f, 1.0f };

	float AttenuationConstant = 1.0f;		//Won't really need these anymore
	float AttenuationLinear = 0.01f;		//Won't really need these anymore
	float AttenuationQuadratic = 0.001f;	//Won't really need these anymore

	void sendData(ShaderProgram* shader);
};

class Material
{
public:
	Material();
	~Material();

	satVec3 ambient =	{ 0.0f, 0.0f, 0.0f };
	satVec3 emissive =	{ 0.0f, 0.0f, 0.0f };
	satVec3 diffuse =	{ 1.0f, 1.0f, 1.0f };
	satVec3 specular =	{ 1.0f, 1.0f, 1.0f };
	//satVec3 color =		{ 1.0f, 1.0f, 1.0f };
	float shininess = 1.0f;
};

class LightAddMult
{
public:
	LightAddMult();
	LightAddMult(const satColor &_diffuseMult);
	LightAddMult(const satColor &_ambientAdd, const satColor &_diffuseAdd, const satColor &_specularAdd, const satColor &_emissiveAdd = 0.0f, const float &_shinyAdd = 1.0f, const satColor &_ambientMult = 1.0f, const satColor &_diffuseMult = 1.0f, const satColor &_specularMult = 1.0f, const satColor &_emissiveMult = 1.0f, const float &_shinyMult = 1.0f);
	~LightAddMult();

#pragma region operatorOverload


	inline LightAddMult LightAddMult::operator+(const LightAddMult &a) const
	{
		return LightAddMult(
			ambientAdd + a.ambientAdd, 
			diffuseAdd + a.diffuseAdd, 
			specularAdd + a.specularAdd,
			emissiveAdd + a.emissiveAdd,
			shinyAdd +	a.shinyAdd,
			ambientMult + a.ambientMult,
			diffuseMult + a.diffuseMult,
			specularMult + a.specularMult,
			emissiveMult + a.emissiveMult,
			shinyMult	+ a.shinyMult);
	}

	inline LightAddMult LightAddMult::operator*(const LightAddMult &a) const
	{
		return LightAddMult(
			ambientAdd * a.ambientAdd, 
			diffuseAdd * a.diffuseAdd, 
			specularAdd * a.specularAdd,
			emissiveAdd * a.emissiveAdd,
			shinyAdd	* a.shinyAdd,
			ambientMult * a.ambientMult,
			diffuseMult * a.diffuseMult,
			specularMult * a.specularMult,
			emissiveMult * a.emissiveMult,
			shinyMult	 * a.shinyMult);
	}

	inline LightAddMult LightAddMult::operator*(const float &a) const
	{
		return LightAddMult(
			ambientAdd *	a,
			diffuseAdd *	a,
			specularAdd *	a,
			emissiveAdd *	a,
			shinyAdd *	a,
			ambientMult *	a,
			diffuseMult *	a,
			specularMult *	a,
			emissiveMult *	a,
			shinyMult * a);
	}

	//inline LightAddMult LightAddMult::operator*(const float &a) const
	friend inline LightAddMult operator*(const float a, const LightAddMult b);
	
#pragma endregion operatorOverload

	satColor ambientAdd =	{ 0.0f, 0.0f, 0.0f };
	satColor diffuseAdd =	{ 0.0f, 0.0f, 0.0f };
	satColor specularAdd =	{ 0.0f, 0.0f, 0.0f };
	satColor emissiveAdd =	{ 0.0f, 0.0f, 0.0f };
	float	 shinyAdd =		0.0f;
	satColor ambientMult =	{ 1.0f, 1.0f, 1.0f };
	satColor diffuseMult =	{ 1.0f, 1.0f, 1.0f };
	satColor specularMult = { 1.0f, 1.0f, 1.0f };
	satColor emissiveMult = { 1.0f, 1.0f, 1.0f };	
	float	 shinyMult =	1.0f;
	
};

inline LightAddMult operator*(const float a, const LightAddMult b)
{
	return LightAddMult(
			a * b.ambientAdd,
			a * b.diffuseAdd,
			a * b.specularAdd,
			a * b.emissiveAdd,
			a * b.shinyAdd,
			a * b.ambientMult,
			a * b.diffuseMult,
			a * b.specularMult,
			a * b.emissiveMult,
			a * b.shinyMult);
}

class PointLight
{
public:

	//PointLight();
	PointLight(satColor _color = satColor(1.0f), satVec3 _pos = 0.0f, float constant = 1.0f, float _linear = 0.2f, float _quad = 0.1f);
	PointLight(satVec3 _color, satVec3 _pos = 0.0f, float constant = 1.0f, float _linear = 0.2f, float _quad = 0.1f);
	~PointLight();
	void calcRadius();
	float getRadius();
	static float getRadius(const float &constant, const float &linear, const float &quad);
	
	satVec3 pos;
	satColor color;

	float constant;
	float linear;
	float quad;
	float radius;
	float innerRadius;
};

namespace sat
{
	namespace light
	{
		namespace lit
		{
			//const Light Default =	 Light();
			const Light Default =	 Light(0.10f,	0.0f,	0.6f,	0.6f);
			const Light fulldark =	 Light(0.00f,	0.0f,	0.0f,	0.0f);
			const Light fullbright = Light(1.00f,	0.0f,	1.0f,	1.0f);
			const Light brighter =	 Light(0.25f,	0.0f,	1.0f,	1.0f);
			const Light bright =	 Light(0.225f,	0.0f,	0.75f,	1.0f);
			const Light medium =	 Light(0.20f,	0.0f,	0.5f,	0.75f);
			const Light dark =		 Light(0.10f,	0.0f,	0.35f,	0.75f);
									 
			const Light darker =	 Light(0.125f,	0.0f,	0.25f,	0.75f);
			const Light temple =	 Light(0.0f,	0.0f,	0.0f,	0.0f);

			const Light test =		 Light(0.125f,	0.0f,	0.25f,	0.75f);
		}

		namespace mod
		{
			//	Formatting for LightAddMult (Parameters are optional after specularAdd)
			//	1				2				3				4				5				6				7				8				9				10
			//	ambientAdd,		diffuseAdd,		specularAdd,	emissiveAdd,	shinyAdd,		ambientMult,	diffuseMult,	specularMult,	emissiveMult,	shinyMult
	
			const LightAddMult Default =		LightAddMult(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.35f, 1.0f, 6.0f);
			const LightAddMult playerShip =		LightAddMult(0.0f, 0.25f, 1.0f, 0.0f, 50.0f, 1.0f, 0.5f, 2.0f, 1.0f, 2.5f);
			const LightAddMult titleScreen =	LightAddMult(1.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 50.0f);
			const LightAddMult ambientHigh =	LightAddMult(0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f);
			const LightAddMult background =		LightAddMult(0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.25f, 0.25f, 1.0f, 1.0f);
			const LightAddMult tutorial =		LightAddMult(1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.5f, 0.05f, 1.0f, 6.0f);
			const LightAddMult enemy =			LightAddMult(0.0f, 0.0f, 1.0f, satColor(0.0f, 0.0f, 0.0f), 5.0f, 0.0f, 1.0f, satColor(1.25f, 0.5f, 0.5f), 1.0f, 10.0f);
			const LightAddMult bulletGood =		LightAddMult(0.0f, 0.0f, 1.0f, satColor(0.125f, 0.375f, 0.375f), 0.0f, satColor(0.0f, 0.25f, 0.25f));
			const LightAddMult smokeShipYellow =LightAddMult(satColor(0.5f, 0.5f, 0.0f), 0.0f, 1.0f, satColor(0.25f, 0.25f, 0.0f), 0.0f,	1.0f, satColor(1.0f, 1.0f, 0.0f), 0.0f, satColor(1.0f, 1.0f, 0.0f), 1.0f);
			const LightAddMult smokeShipRed =	LightAddMult(satColor(0.5f, 0.0f, 0.0f),  0.0f, 1.0f, satColor(0.25f, 0.0f, 0.0f), 0.0f,	1.0f, satColor(1.0f, 0.0f, 0.0f), 0.0f, satColor(1.0f, 0.0f, 0.0f), 1.0f);
		
		}
	}
}