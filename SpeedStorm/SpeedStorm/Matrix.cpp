/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Math.h"
#include <iostream>
//#include <verify.h>

/*
//=======================================================================================
//
//	satMat2 - 2x2 matrix
//
//=======================================================================================
*/

#pragma region satMat2

//=======================//
// satMat2::constructors //
//=======================//

#pragma region satMat2constructors

satMat2::satMat2()
{

}

satMat2::satMat2(const satVec2 &x, const satVec2 &y)
{
	this->mat[0].x = x.x; this->mat[0].y = x.y;
	this->mat[1].x = y.x; this->mat[1].y = y.y;
}

satMat2::satMat2(const float xx, const float xy, const float yx, const float yy)
{
	this->mat[0].x = xx; this->mat[0].y = xy;
	this->mat[1].x = yx; this->mat[1].y = yy;
}

#pragma endregion satMat2constructors

//==============================//
// end of satMat2::constructors //
//==============================//

//====================//
// satMat2::operators //
//====================//

#pragma region satMat2operators

const satVec2& satMat2::operator[](int a) const
{
	return mat[a];
}

satVec2& satMat2::operator[](int a)
{
	return mat[a];
}

satMat2 satMat2::operator-() const
{
	return satMat2(-mat[0][0], -mat[0][1],
		-mat[1][0], -mat[1][1]
		);
}

satVec2 satMat2::operator*(const satVec2 &x) const
{
	return satVec2(
		mat[0].x * x.x + mat[0].y * x.y,
		mat[1].x * x.x + mat[1].y * x.y
		);
}

satMat2 satMat2::operator*(const satMat2 &a) const
{
	return satMat2(
		mat[0].x * a[0].x + mat[0].y * a[1].x,
		mat[0].x * a[0].y + mat[0].y * a[1].y,
		mat[1].x * a[0].x + mat[1].y * a[1].x,
		mat[1].x * a[0].y + mat[1].y * a[1].y
		);
}

satMat2 satMat2::operator*(const float a) const
{
	return satMat2(
		mat[0].x * a, mat[0].y * a,
		mat[1].x * a, mat[1].y * a
		);
}

satMat2 satMat2::operator+(const satMat2 &a) const
{
	return satMat2(
		mat[0].x + a[0].x, mat[0].y + a[0].y,
		mat[1].x + a[1].x, mat[1].y + a[1].y
		);
}

satMat2 satMat2::operator-(const satMat2 &a) const
{
	return satMat2(
		mat[0].x - a[0].x, mat[0].y - a[0].y,
		mat[1].x - a[1].x, mat[1].y - a[1].y
		);
}

satMat2 &satMat2::operator*=(const float a)
{
	mat[0].x *= a; mat[0].y *= a;
	mat[1].x *= a; mat[1].y *= a;

	return *this;
}

satMat2 &satMat2::operator*=(const satMat2 &a)
{
	float x = mat[0].x;
	float y = mat[0].y;

	mat[0].x = x * a[0].x + y * a[1].x;
	mat[0].y = x * a[0].y + y * a[1].y;

	x = mat[1].x;
	y = mat[1].y;

	mat[1].x = x * a[0].x + y * a[1].x;
	mat[1].y = x * a[0].y + y * a[1].y;

	return *this;
}

satVec2 &operator*=(satVec2 &vector, const satMat2 &matrix)
{
	vector = matrix * vector;
	return vector;
}

satVec2 operator*(const satVec2 &vector, const satMat2 &matrix)
{
	return matrix * vector;
}

satMat2 operator*(const float a, satMat2 const &matrix)
{
	return matrix * a;
}

#pragma endregion satMat2operators

//===========================//
// end of satMat2::operators //
//===========================//

//============================//
// satMat2::general functions //
//============================//

#pragma region satMat2functions

#pragma endregion satMat2functions

//===================================//
// end of satVec3::general functions //
//===================================//

#pragma endregion satMat2

/*
//=======================================================================================
//
//	satMat3 - 3x3 matrix
//
//=======================================================================================
*/

#pragma region satMat3

//=======================//
// satMat3::constructors //
//=======================//

#pragma region satMat3constructors

satMat3::satMat3()
{
	mat[0][0] = 1.0f; mat[0][1] = 0.0f; mat[0][2] = 0.0f;
	mat[1][0] = 0.0f; mat[1][1] = 1.0f; mat[1][2] = 0.0f;
	mat[2][0] = 0.0f; mat[2][1] = 0.0f; mat[2][2] = 1.0f;
}

satMat3::satMat3(const satVec3 &x, const satVec3 &y, const satVec3 &z)
{
	mat[0].x = x.x; mat[0].y = x.y; mat[0].z = x.z;
	mat[1].x = y.x; mat[1].y = y.y; mat[1].z = y.z;
	mat[2].x = z.x; mat[2].y = z.y; mat[2].z = z.z;
}

satMat3::satMat3(const float xx, const float xy, const float xz, const float yx, const float yy, const float yz, const float zx, const float zy, const float zz)
{
	mat[0].x = xx; mat[0].y = xy; mat[0].z = xz;
	mat[1].x = yx; mat[1].y = yy; mat[1].z = yz;
	mat[2].x = zx; mat[2].y = zy; mat[2].z = zz;
}

#pragma endregion satMat3constructors

//==============================//
// end of satMat3::constructors //
//==============================//

//====================//
// satMat3::operators //
//====================//

#pragma region satMat3operators

const satVec3& satMat3::operator[](int index) const
{
	return mat[index];
}

satVec3 &	satMat3::operator[](int index)
{
	return mat[index];
}

satMat3 &	satMat3::operator=(const satMat3 &m)
{
	mat[0][0] = m[0][0]; mat[1][0] = m[1][0]; mat[2][0] = m[2][0];
	mat[0][1] = m[0][1]; mat[1][1] = m[1][1]; mat[2][1] = m[2][1];
	mat[0][2] = m[0][2]; mat[1][2] = m[1][2]; mat[2][2] = m[2][2];

	return (*this);
}

satMat3		satMat3::operator-() const
{
	return satMat3(
		-mat[0][0], -mat[0][1], -mat[0][2],
		-mat[1][0], -mat[1][1], -mat[1][2],
		-mat[2][0], -mat[2][1], -mat[2][2]);
}

satMat3		satMat3::operator*(const float a) const
{
	return satMat3(
		mat[0].x * a, mat[0].y * a, mat[0].z * a,
		mat[1].x * a, mat[1].y * a, mat[1].z * a,
		mat[2].x * a, mat[2].y * a, mat[2].z * a);
}

satVec3		satMat3::operator*(const satVec3 &vec) const
{
	return satVec3(
		mat[0][0] * vec[0] + mat[1][0] * vec[1] + mat[2][0] * vec[2],
		mat[0][1] * vec[0] + mat[1][1] * vec[1] + mat[2][1] * vec[2],
		mat[0][2] * vec[0] + mat[1][2] * vec[1] + mat[2][2] * vec[2]);
}

satMat3		satMat3::operator*(const satMat3 &m) const
{
	return satMat3(
		m.mat[0][0] * mat[0][0] + m.mat[1][0] * mat[0][1] + m.mat[2][0] * mat[0][2],
		m.mat[0][1] * mat[0][0] + m.mat[1][1] * mat[0][1] + m.mat[2][1] * mat[0][2],
		m.mat[0][2] * mat[0][0] + m.mat[1][2] * mat[0][1] + m.mat[2][2] * mat[0][2],
		m.mat[0][0] * mat[1][0] + m.mat[1][0] * mat[1][1] + m.mat[2][0] * mat[1][2],
		m.mat[0][1] * mat[1][0] + m.mat[1][1] * mat[1][1] + m.mat[2][1] * mat[1][2],
		m.mat[0][2] * mat[1][0] + m.mat[1][2] * mat[1][1] + m.mat[2][2] * mat[1][2],
		m.mat[0][0] * mat[2][0] + m.mat[1][0] * mat[2][1] + m.mat[2][0] * mat[2][2],
		m.mat[0][1] * mat[2][0] + m.mat[1][1] * mat[2][1] + m.mat[2][1] * mat[2][2],
		m.mat[0][2] * mat[2][0] + m.mat[1][2] * mat[2][1] + m.mat[2][2] * mat[2][2]);
}

satMat3		satMat3::operator+(const satMat3 &m) const
{
	return satMat3(
		mat[0][0] + m[0][0], mat[1][0] + m[1][0], mat[2][0] + m[2][0],
		mat[0][1] + m[0][1], mat[1][1] + m[1][1], mat[2][1] + m[2][1],
		mat[0][2] + m[0][2], mat[1][2] + m[1][2], mat[2][2] + m[2][2]);
}

satMat3		satMat3::operator-(const satMat3 &m) const
{
	return satMat3(
		mat[0][0] - m[0][0], mat[1][0] - m[1][0], mat[2][0] - m[2][0],
		mat[0][1] - m[0][1], mat[1][1] - m[1][1], mat[2][1] - m[2][1],
		mat[0][2] - m[0][2], mat[1][2] - m[1][2], mat[2][2] - m[2][2]);
}

satMat3 &	satMat3::operator*=(const float a)
{
	mat[0] *= a;
	mat[1] *= a;
	mat[2] *= a;

	return *this;
}

satMat3 &	satMat3::operator*=(const satMat3 &m)
{
	mat[0] *= m[0];
	mat[1] *= m[1];
	mat[2] *= m[2];

	return *this;
}

satMat3 &	satMat3::operator+=(const satMat3 &m)
{
	mat[0] += m[0];
	mat[1] += m[1];
	mat[2] += m[2];

	return *this;
}

satMat3 &	satMat3::operator-=(const satMat3 &m)
{
	mat[0] -= m[0];
	mat[1] -= m[1];
	mat[2] -= m[2];

	return *this;
}

//friend operators (allow you to reverse order of multiplication in code)

satMat3	operator*(const float a, const satMat3 &m)
{
	return m * a;
}

satVec3	operator*(const satVec3 &vec, const satMat3 &m)
{
	return m * vec;
}

satVec3 &	operator*=(satVec3 &vec, const satMat3 &m)
{
	vec = m * vec;
	return vec;
}

#pragma endregion satMat3operators

//===========================//
// end of satMat3::operators //
//===========================//

//============================//
// satMat3::general functions //
//============================//

#pragma region satMat3functions

bool			satMat3::Equals(const satMat3 &m) const
{
	if (
		mat[0].Equals(m[0]) &&
		mat[1].Equals(m[1]) &&
		mat[2].Equals(m[2]))
	{
		return true;
	}

	return false;
}

bool			satMat3::Equals(const satMat3 &m, const float margin) const
{
	if (
		mat[0].Equals(m[0], margin) &&
		mat[1].Equals(m[1], margin) &&
		mat[2].Equals(m[2], margin))
	{
		return true;
	}

	return false;
}

bool			satMat3::operator==(const satMat3 &m) const
{
	return Equals(m);
}

bool			satMat3::operator!=(const satMat3 &m) const
{
	return !Equals(m);
}


void			satMat3::Zero()
{
	mat[0][0] = 0.0f; mat[1][0] = 0.0f; mat[2][0] = 0.0f;
	mat[0][1] = 0.0f; mat[1][1] = 0.0f; mat[2][1] = 0.0f;
	mat[0][2] = 0.0f; mat[1][2] = 0.0f; mat[2][2] = 0.0f;
}

satMat3 satMat3::Identity()
{
	return satMat3(
		1.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 1.0f);
}

void satMat3::LoadIdentity()
{
	mat[0] = satVec3(1.0f, 0.0f, 0.0f);
	mat[1] = satVec3(0.0f, 1.0f, 0.0f);
	mat[2] = satVec3(0.0f, 0.0f, 1.0f);
	//1.0f, 0.0f, 0.0f, 0.0f,
	//0.0f, 1.0f, 0.0f, 0.0f,
	//0.0f, 0.0f, 1.0f, 0.0f,
	//0.0f, 0.0f, 0.0f, 1.0f);
}

satMat3		satMat3::OrthoNormalize() const
{
	satMat3 ortho = *this;
	ortho[0].Normalize();
	ortho[2].Cross(mat[0], mat[1]);
	ortho[2].Normalize();
	ortho[1].Cross(mat[2], mat[0]);
	ortho[1].Normalize();
	return ortho;
}

satMat3		satMat3::Transpose() const
{
	return satMat3(
		mat[0][0], mat[1][0], mat[2][0],
		mat[0][1], mat[1][1], mat[2][1],
		mat[0][2], mat[1][2], mat[2][2]);
}

satMat3 &	satMat3::TransposeSelf()
{
	float tmp0, tmp1, tmp2;

	tmp0 = mat[0][1];
	mat[0][1] = mat[1][0];
	mat[1][0] = tmp0;
	tmp1 = mat[0][2];
	mat[0][2] = mat[2][0];
	mat[2][0] = tmp1;
	tmp2 = mat[1][2];
	mat[1][2] = mat[2][1];
	mat[2][1] = tmp2;

	return *this;
}

satMat3		satMat3::InverseFast()
{
	//https://i.gyazo.com/8ece30b374aba324415b2bc412978752.png
	//https://i.gyazo.com/3ca1e222225e189e0b94d4ac110a7341.png

	float deter = Determinant();
	assert(deter != 0);
	deter = 1.0f / deter;

	return satMat3(
		mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1],
		mat[1][2] * mat[2][0] - mat[1][0] * mat[2][2],
		mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0],
		mat[0][2] * mat[2][1] - mat[0][1] * mat[2][2],
		mat[0][0] * mat[2][2] - mat[0][2] * mat[2][0],
		mat[0][1] * mat[2][0] - mat[0][0] * mat[2][1],
		mat[0][1] * mat[1][2] - mat[1][1] * mat[0][2],
		mat[0][2] * mat[1][2] - mat[0][0] * mat[1][2],
		mat[0][0] * mat[1][1] - mat[0][1] * mat[1][0]) * deter;
}

float satMat3::Determinant()
{
	satVec3 det = (
		mat[1][0] * mat[2][1] - mat[1][1] * mat[2][0],
		mat[1][0] * mat[2][2] - mat[1][2] * mat[2][0],
		mat[1][1] * mat[2][2] - mat[1][2] * mat[2][1]);

	return mat[0][0] * det[0] - mat[0][1] * det[1] + mat[0][2] * det[2];
}

satMat3		satMat3::TransposeMultiply(const satMat3 &m) const
{
	return satMat3(
		mat[0][0] * m[0][0] + mat[1][0] * m[1][0] + mat[2][0] * m[2][0],
		mat[0][0] * m[0][1] + mat[1][0] * m[1][1] + mat[2][0] * m[2][1],
		mat[0][0] * m[0][2] + mat[1][0] * m[1][2] + mat[2][0] * m[2][2],
		mat[0][1] * m[0][0] + mat[1][1] * m[1][0] + mat[2][1] * m[2][0],
		mat[0][1] * m[0][1] + mat[1][1] * m[1][1] + mat[2][1] * m[2][1],
		mat[0][1] * m[0][2] + mat[1][1] * m[1][2] + mat[2][1] * m[2][2],
		mat[0][2] * m[0][0] + mat[1][2] * m[1][0] + mat[2][2] * m[2][0],
		mat[0][2] * m[0][1] + mat[1][2] * m[1][1] + mat[2][2] * m[2][1],
		mat[0][2] * m[0][2] + mat[1][2] * m[1][2] + mat[2][2] * m[2][2]);
}


int			satMat3::GetDimension() const
{
	return 3;
}

//"degreets" was a typo of "degrees", I decided to stick with it for no good reason
//I should probably follow conventions to prevent confusion from things like this but I think it's close enough to the original to not be a problem
void satMat3::RotateX(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat3 rotation = satMat3(
		1.0f, 0.0f, 0.0f,
		0.0f, cosT, -sinT,
		0.0f, sinT, cosT);

	*this = rotation * (*this);
}

void satMat3::RotateY(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat3 rotation = satMat3(
		cosT, 0.0f, sinT,
		0.0f, 1.0f, 0.0f,
		-sinT, 0.0f, cosT);

	*this = rotation * (*this);
}

void satMat3::RotateZ(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat3 rotation = satMat3(
		cosT, -sinT, 0.0f,
		sinT, cosT, 0.0f,
		0.0f, 0.0f, 1.0f);

	*this = rotation * (*this);
}

void satMat3::Scale(float x, float y, float z)
{
	mat[0][0] *= x;
	mat[1][1] *= y;
	mat[2][2] *= z;
}

void satMat3::Scale(const satVec3 &vec)
{
	mat[0][0] *= vec.x;
	mat[1][1] *= vec.y;
	mat[2][2] *= vec.z;
}

void satMat3::Scale(float xyz)
{
	mat[0][0] *= xyz;
	mat[1][1] *= xyz;
	mat[2][2] *= xyz;
}

void satMat3::ScaleX(float x)
{
	mat[0][0] *= x;
}

void satMat3::ScaleY(float y)
{
	mat[1][1] *= y;
}

void satMat3::ScaleZ(float z)
{
	mat[2][2] *= z;
}

satMat3 satMat3::RotationX(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return  satMat3(
		1.0f, 0.0f, 0.0f,
		0.0f, cosT, -sinT,
		0.0f, sinT, cosT);
}

satMat3 satMat3::RotationY(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return satMat3(
		cosT, 0.0f, sinT,
		0.0f, 1.0f, 0.0f,
		-sinT, 0.0f, cosT);
}

satMat3 satMat3::RotationZ(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return satMat3(
		cosT, -sinT, 0.0f,
		sinT, cosT, 0.0f,
		0.0f, 0.0f, 1.0f);
}



satMat4 satMat3::ToMat4() const
{
	// NOTE: idMat3 is transposed because it is column-major
	return satMat4(
		mat[0][0], mat[1][0], mat[2][0], 0.0f,
		mat[0][1], mat[1][1], mat[2][1], 0.0f,
		mat[0][2], mat[1][2], mat[2][2], 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

#pragma endregion satMat3functions

//===================================//
// end of satVec3::general functions //
//===================================//



#pragma endregion satMat3

/*
//=======================================================================================
//
//	satMat4 - 4x4 matrix
//
//=======================================================================================
*/

#pragma region satMat4

//=======================//
// satVec4::constructors //
//=======================//

#pragma region satMat4constructors

satMat4::satMat4()
{
	mat[0][0] = 1.0f; mat[0][1] = 0.0f; mat[0][2] = 0.0f; mat[0][3] = 0.0f;
	mat[1][0] = 0.0f; mat[1][1] = 1.0f; mat[1][2] = 0.0f; mat[1][3] = 0.0f;
	mat[2][0] = 0.0f; mat[2][1] = 0.0f; mat[2][2] = 1.0f; mat[2][3] = 0.0f;
	mat[3][0] = 0.0f; mat[3][1] = 0.0f; mat[3][2] = 0.0f; mat[3][3] = 1.0f;
}

satMat4::satMat4(const satVec4& right, const satVec4& up, const satVec4& dir, const satVec4& pos){
	mat[0] = right;
	mat[1] = up;
	mat[2] = dir;
	mat[3] = pos;
}

satMat4::satMat4(const float xx, const float xy, const float xz, const float xw,
	const float yx, const float yy, const float yz, const float yw,
	const float zx, const float zy, const float zz, const float zw,
	const float wx, const float wy, const float wz, const float ww)
{
	mat[0][0] = xx; mat[0][1] = xy; mat[0][2] = xz; mat[0][3] = xw;
	mat[1][0] = yx; mat[1][1] = yy; mat[1][2] = yz; mat[1][3] = yw;
	mat[2][0] = zx; mat[2][1] = zy; mat[2][2] = zz; mat[2][3] = zw;
	mat[3][0] = wx; mat[3][1] = wy; mat[3][2] = wz; mat[3][3] = ww;
}

satMat4::satMat4(const satVec3& right, const satVec3& up, const satVec3& dir, const satVec3& pos)
{
	mat[0] = satVec4(right, 0.0f);
	mat[1] = satVec4(up, 0.0f);
	mat[2] = satVec4(pos, 0.0f);
	mat[3] = satVec4(pos, 1.0f);
}

satMat4::satMat4(const satMat3 &m, const satVec3 &t)
{
	mat[0][0] = m[0][0];	mat[0][1] = m[0][1];	mat[0][2] = m[0][2];	mat[0][3] = t[0];
	mat[1][0] = m[1][0];	mat[1][1] = m[1][1];	mat[1][2] = m[1][2];	mat[1][3] = t[1];
	mat[2][0] = m[2][0];	mat[2][1] = m[2][1];	mat[2][2] = m[2][2];	mat[2][3] = t[2];
	mat[3][0] = 0.0f;		mat[3][1] = 0.0f;		mat[3][2] = 0.0f;		mat[3][3] = 1.0f;
}

satMat4::satMat4(const satMat3 &m)
{
	mat[0][0] = m[0][0];	mat[0][1] = m[0][1];	mat[0][2] = m[0][2];	mat[0][3] = 0.0f;
	mat[1][0] = m[1][0];	mat[1][1] = m[1][1];	mat[1][2] = m[1][2];	mat[1][3] = 0.0f;
	mat[2][0] = m[2][0];	mat[2][1] = m[2][1];	mat[2][2] = m[2][2];	mat[2][3] = 0.0f;
	mat[3][0] = 0.0f;		mat[3][1] = 0.0f;		mat[3][2] = 0.0f;		mat[3][3] = 1.0f;
}

satMat4::satMat4(const satMat4& m)
{
	mat[0][0] = m[0][0]; mat[0][1] = m[0][1]; mat[0][2] = m[0][2]; mat[0][3] = m[0][3];
	mat[1][0] = m[1][0]; mat[1][1] = m[1][1]; mat[1][2] = m[1][2]; mat[1][3] = m[1][3];
	mat[2][0] = m[2][0]; mat[2][1] = m[2][1]; mat[2][2] = m[2][2]; mat[2][3] = m[2][3];
	mat[3][0] = m[3][0]; mat[3][1] = m[3][1]; mat[3][2] = m[3][2]; mat[3][3] = m[3][3];
}

#pragma endregion satMat4constructors

//==============================//
// end of satVec4::constructors //
//==============================//



//============================//
// satMat4::general functions //
//============================//

#pragma region satMat4functions

bool			satMat4::Equals(const satMat4 &m) const
{
	if (
		mat[0].Equals(m[0]) &&
		mat[1].Equals(m[1]) &&
		mat[2].Equals(m[2]) &&
		mat[3].Equals(m[3]))
	{
		return true;
	}

	return false;
}

bool			satMat4::Equals(const satMat4 &m, const float margin) const
{
	if (
		mat[0].Equals(m[0], margin) &&
		mat[1].Equals(m[1], margin) &&
		mat[2].Equals(m[2], margin) &&
		mat[3].Equals(m[3], margin))
	{
		return true;
	}

	return false;
}

bool			satMat4::operator==(const satMat4 &m) const
{
	return Equals(m);
}

bool			satMat4::operator!=(const satMat4 &m) const
{
	return !Equals(m);
}

void	satMat4::SetZero()
{
	mat[0][0] = 0.0f; mat[1][0] = 0.0f; mat[2][0] = 0.0f;  mat[3][0] = 0.0f;
	mat[0][1] = 0.0f; mat[1][1] = 0.0f; mat[2][1] = 0.0f;	 mat[3][1] = 0.0f;
	mat[0][2] = 0.0f; mat[1][2] = 0.0f; mat[2][2] = 0.0f;	 mat[3][2] = 0.0f;
	mat[0][3] = 0.0f; mat[1][3] = 0.0f; mat[2][3] = 0.0f;	 mat[3][3] = 0.0f;
}

satVec3 satMat4::GetPos()
{
	return satVec3(mat[0][3], mat[1][3], mat[2][3]);
}

satVec3 satMat4::xyz()
{
	return satVec3(mat[0][3], mat[1][3], mat[2][3]);
}

satMat4		satMat4::OrthoNormalize() const
{
	return (*this);
}

satMat4		satMat4::Transpose() const
{
	return satMat4(
		mat[0][0], mat[1][0], mat[2][0], mat[3][0],
		mat[0][1], mat[1][1], mat[2][1], mat[3][1],
		mat[0][2], mat[1][2], mat[2][2], mat[3][2],
		mat[0][3], mat[1][3], mat[2][3], mat[3][3]);
}



void satMat4::RotateX(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat4 rotation = satMat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, cosT, -sinT, 0.0f,
		0.0f, sinT, cosT, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	*this = rotation * (*this);
}

void satMat4::RotateY(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat4 rotation = satMat4(
		cosT, 0.0f, sinT, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		-sinT, 0.0f, cosT, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	*this = rotation * (*this);
}

void satMat4::RotateZ(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	satMat4 rotation = satMat4(
		cosT, sinT, 0.0f, 0.0f,
		-sinT, cosT, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);

	*this = rotation * (*this);
}

satMat4 satMat4::RotationX(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return satMat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, cosT, -sinT, 0.0f,
		0.0f, sinT, cosT, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

satMat4 satMat4::RotationY(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return satMat4(
		cosT, 0.0f, sinT, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		-sinT, 0.0f, cosT, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

satMat4 satMat4::RotationZ(const float degreets)
{
	float cosT = (cosf(DTR(degreets)));
	float sinT = (sinf(DTR(degreets)));

	return satMat4(
		cosT, sinT, 0.0f, 0.0f,
		-sinT, cosT, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

void satMat4::Scale(float x, float y, float z)
{
	mat[0][0] *= x;
	mat[1][1] *= y;
	mat[2][2] *= z;
}

void satMat4::Scale(const satVec3 &vec)
{
	mat[0][0] *= vec.x;
	mat[1][1] *= vec.y;
	mat[2][2] *= vec.z;
}

void satMat4::Scale(float xyz)
{
	mat[0][0] *= xyz;
	mat[1][1] *= xyz;
	mat[2][2] *= xyz;
}

void satMat4::ScaleX(float x)
{
	mat[0][0] *= x;
}

void satMat4::ScaleY(float y)
{
	mat[1][1] *= y;
}

void satMat4::ScaleZ(float z)
{
	mat[2][2] *= z;
}


int	satMat4::GetDimension() const
{
	return 4;
}

satMat3 satMat4::GetRotationMatrix()
{
	return satMat3(
		mat[0].xyz(),
		mat[1].xyz(),
		mat[2].xyz());
}

satVec3 satMat4::GetDirectionVector()
{
	satVec3 vec = mat[2].xyz();
	vec.Normalize();
	return vec;
}

satMat4		satMat4::Transpose(satMat4 &m)
{
	return satMat4(
		m[0][0], m[1][0], m[2][0], m[3][0],
		m[0][1], m[1][1], m[2][1], m[3][1],
		m[0][2], m[1][2], m[2][2], m[3][2],
		m[0][3], m[1][3], m[2][3], m[3][3]);
}


satMat4 satMat4::LerpMat4(const satMat4 &a, const satMat4 &b, const float &t)
{
	return satMat4(
		Lerp(a[0][0], b[0][0], t),
		Lerp(a[0][1], b[0][1], t),
		Lerp(a[0][2], b[0][2], t),
		Lerp(a[0][3], b[0][3], t),
		Lerp(a[1][0], b[1][0], t),
		Lerp(a[1][1], b[1][1], t),
		Lerp(a[1][2], b[1][2], t),
		Lerp(a[1][3], b[1][3], t),
		Lerp(a[2][0], b[2][0], t),
		Lerp(a[2][1], b[2][1], t),
		Lerp(a[2][2], b[2][2], t),
		Lerp(a[2][3], b[2][3], t),
		Lerp(a[3][0], b[3][0], t),
		Lerp(a[3][1], b[3][1], t),
		Lerp(a[3][2], b[3][2], t),
		Lerp(a[3][3], b[3][3], t)
	);
}

void satMat4::PerspectiveProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar)
{
	assert(fovyDegrees != 0);
	assert(aspectRatio != 0);
	
	float zNearMinusFar = zNear - zFar;
	assert(zNearMinusFar != 0);
	
	float cotHalfTheta = 1.f / tanf(DTR(fovyDegrees) * 0.5f);
	
	mat[0][0] = cotHalfTheta / aspectRatio;
	mat[0][1] = 0.0f;
	mat[0][2] =	0.0f;
	mat[0][3] =	0.0f;

	mat[1][0] = 0.0f;
	mat[1][1] = cotHalfTheta;
	mat[1][2] = 0.0f;
	mat[1][3] = 0.0f;

	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = (zFar + zNear) / zNearMinusFar;
	mat[2][3] = (2.0f * zFar * zNear) / zNearMinusFar;

	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = -1.0f;
	mat[3][3] = 0.0f;
}

void satMat4::PerspectiveStereoProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skew, float convergence)
{
	assert(fovyDegrees != 0);
	assert(aspectRatio != 0);
	
	float zNearMinusFar = zNear - zFar;
	assert(zNearMinusFar != 0);
	
	float cotHalfTheta = 1.f / tanf(DTR(fovyDegrees) * 0.5f);

	mat[0][0] = cotHalfTheta / aspectRatio;
	mat[0][1] = 0.0f;
	mat[0][2] =	0.0f;
	mat[0][3] =	0.0f;

	mat[1][0] = 0.0f;
	mat[1][1] = cotHalfTheta;
	mat[1][2] = 0.0f;
	mat[1][3] = 0.0f;

	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = (zFar + zNear) / zNearMinusFar;
	mat[2][3] = (2.0f * zFar * zNear) / zNearMinusFar;

	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = -1.0f;
	mat[3][3] = 0.0f;

	satMat4 mat3D;

	mat3D[0][0] = 1.0f;
	mat3D[0][1] = 0.0f;
	mat3D[0][2] = skew / convergence;
	mat3D[0][3] = skew;

	mat3D[1][0] = 0.0f;
	mat3D[1][1] = 1.0f;
	mat3D[1][2] = 0.0f;
	mat3D[1][3] = 0.0f;

	mat3D[2][0] = 0.0f;
	mat3D[2][1] = 0.0f;
	mat3D[2][2] = 1.0f;
	mat3D[2][3] = 0.0f;

	mat3D[3][0] = 0.0f;
	mat3D[3][1] = 0.0f;
	mat3D[3][2] = 0.0f;
	mat3D[3][3] = 1.0f;

	*this = (*this) * mat3D;
}

void satMat4::PerspectiveStereoProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY)
{
	assert(fovyDegrees != 0);
	assert(aspectRatio != 0);
	
	float zNearMinusFar = zNear - zFar;
	assert(zNearMinusFar != 0);
	
	float cotHalfTheta = 1.f / tanf(DTR(fovyDegrees) * 0.5f);

	mat[0][0] = cotHalfTheta / aspectRatio;
	mat[0][1] = 0.0f;
	mat[0][2] =	0.0f;
	mat[0][3] =	0.0f;

	mat[1][0] = 0.0f;
	mat[1][1] = cotHalfTheta;
	mat[1][2] = 0.0f;
	mat[1][3] = 0.0f;

	mat[2][0] = 0.0f;
	mat[2][1] = 0.0f;
	mat[2][2] = (zFar + zNear) / zNearMinusFar;
	mat[2][3] = (2.0f * zFar * zNear) / zNearMinusFar;

	mat[3][0] = 0.0f;
	mat[3][1] = 0.0f;
	mat[3][2] = -1.0f;
	mat[3][3] = 0.0f;

	satMat4 mat3D;

	mat3D[0][0] = 1.0f;
	mat3D[0][1] = 0.0f;
	mat3D[0][2] = skewX / convergenceX;
	mat3D[0][3] = skewX;

	mat3D[1][0] = 0.0f;
	mat3D[1][1] = 1.0f;
	mat3D[1][2] = skewY / convergenceY;
	mat3D[1][3] = skewY;

	mat3D[2][0] = 0.0f;
	mat3D[2][1] = 0.0f;
	mat3D[2][2] = 1.0f;
	mat3D[2][3] = 0.0f;

	mat3D[3][0] = 0.0f;
	mat3D[3][1] = 0.0f;
	mat3D[3][2] = 0.0f;
	mat3D[3][3] = 1.0f;

	*this = (*this) * mat3D;
}

void satMat4::OrthoStereoProjection(float top, float bottom, float left, float right, float zNear, float zFar, float skewX, float skewY, float convergenceX, float convergenceY)
{
	float rightLeft = right - left;
	assert(rightLeft != 0);

	float topBottom = top - bottom;
	assert(topBottom != 0);

	float farNear = zFar - zNear;
	assert(farNear != 0);

	mat[0][0] = 2.0f / rightLeft;
	mat[0][1] = 0;
	mat[0][2] = 0;
	mat[0][3] = 0;
	mat[1][0] = 0;
	mat[1][1] = 2.0f / topBottom;
	mat[1][2] = 0;
	mat[1][3] = 0;
	mat[2][0] = 0;
	mat[2][1] = 0;
	mat[2][2] = -2.0f / farNear;
	mat[2][3] = 0;
	mat[3][0] = -(right + left) / rightLeft;
	mat[3][1] = -(top + bottom) / topBottom;
	mat[3][2] = -(zFar + zNear) / farNear;
	mat[3][3] = 1.0f;


	satMat4 mat3D;

	mat3D[0][0] = 1.0f;
	mat3D[0][1] = 0.0f;
	mat3D[0][2] = skewX / convergenceX;
	mat3D[0][3] = skewX;

	mat3D[1][0] = 0.0f;
	mat3D[1][1] = 1.0f;
	mat3D[1][2] = skewY / convergenceY;
	mat3D[1][3] = skewY;

	mat3D[2][0] = 0.0f;
	mat3D[2][1] = 0.0f;
	mat3D[2][2] = 1.0f;
	mat3D[2][3] = 0.0f;

	mat3D[3][0] = 0.0f;
	mat3D[3][1] = 0.0f;
	mat3D[3][2] = 0.0f;
	mat3D[3][3] = 1.0f;

	*this = (*this) * mat3D;
}


void satMat4::PerspectiveProjection(float fovyDegrees, float aspectRatio, float zNear, float zFar, satMat4 &inverse)
{
	assert(fovyDegrees != 0);
	assert(aspectRatio != 0);

	float NearFar = zNear - zFar;
	assert(NearFar != 0);

	float cotHalfTheta = 1.f / tanf(fovyDegrees * satMath::PI / 360.f);

	mat[0][0] = cotHalfTheta / aspectRatio;
	mat[0][1] = 0;
	mat[0][2] = 0;
	mat[0][3] = 0;
	mat[1][0] = 0;
	mat[1][1] = cotHalfTheta;
	mat[1][2] = 0;
	mat[1][3] = 0;
	mat[2][0] = 0;
	mat[2][1] = 0;
	mat[2][2] = (zFar + zNear) / NearFar;
	mat[2][3] = -1.f;
	mat[3][0] = 0;
	mat[3][1] = 0;
	mat[3][2] = (2.f * zFar * zNear) / NearFar;
	mat[3][3] = 0;

	inverse[0][0] = 1.f / mat[0][0];
	inverse[0][1] = 0;
	inverse[0][2] = 0;
	inverse[0][3] = 0;
	inverse[1][0] = 0;
	inverse[1][1] = 1.f / mat[1][1];
	inverse[1][2] = 0;
	inverse[1][3] = 0;
	inverse[2][0] = 0;
	inverse[2][1] = 0;
	inverse[2][2] = 0;
	inverse[2][3] = 1.f / mat[3][2];
	inverse[3][0] = 0;
	inverse[3][1] = 0;
	inverse[3][2] = 1.f / mat[2][3];
	inverse[3][3] = -mat[2][2] / (mat[3][2] * mat[2][3]);
}

void satMat4::OrthoProjection(float top, float bottom, float left, float right, float near, float far)
{
	//T n2 = 2 * near;
	//T tmb = top - bottom;
	//T tpd = top + bottom;
	//T nmf = near - far;
	//return _mat4 < T >
	//	(2 / (right - left), 0, 0, 0,
	//	0, 2 / tmb, 0, 0,
	//	0, 0, 2 / nmf, 0,
	//	(left + right) / (left - right), (bottom + top) / (bottom - top), (near + far) / nmf, 1);

	//float n2 = near * 2.0f;
	//float tmb = top - bottom;
	//float tpb = top + bottom;
	//float nmf = near - far;
	//
	//*this = satMat4(
	//	2 / (right - left), 0.0f, 0.0f, 0.0f,
	//	0.0f, 2 / tmb, 0.0f, 0.0f,
	//	0.0f, 0.0f, 2 / tmb, 0.0f,
	//	(left + right) / (left - right), (bottom + top) / (bottom - top), (near + far) / nmf, 1.0f
	//	);

	float rightLeft = right - left;
	assert(rightLeft != 0);

	float topBottom = top - bottom;
	assert(topBottom != 0);

	float farNear = far - near;
	assert(farNear != 0);

	mat[0][0] = 2.0f / rightLeft;
	mat[0][1] = 0;
	mat[0][2] = 0;
	mat[0][3] = 0;
	mat[1][0] = 0;
	mat[1][1] = 2.0f / topBottom;
	mat[1][2] = 0;
	mat[1][3] = 0;
	mat[2][0] = 0;
	mat[2][1] = 0;
	mat[2][2] = -2.0f / farNear;
	mat[2][3] = 0;
	mat[3][0] = -(right + left) / rightLeft;
	mat[3][1] = -(top + bottom) / topBottom;
	mat[3][2] = -(far + near) / farNear;
	mat[3][3] = 1.0f;

	//float rightLeft = right - left;
	//assert(rightLeft != 0);
	//
	//float topBottom = top - bottom;
	//assert(topBottom != 0);
	//
	//float farNear = far - near;
	//assert(farNear != 0);
	//
	//mat[0][0] = 2.f / rightLeft;
	//mat[0][1] = 0;
	//mat[0][2] = 0;
	//mat[0][3] = 0;
	//mat[1][0] = 0;
	//mat[1][1] = 2.f / topBottom;
	//mat[1][2] = 0;
	//mat[1][3] = 0;
	//mat[2][0] = 0;
	//mat[2][1] = 0;
	//mat[2][2] = -2.f / farNear;
	//mat[2][3] = 0;
	//mat[3][0] = -(right + left) / rightLeft;
	//mat[3][1] = -(top + bottom) / topBottom;
	//mat[3][2] = -(far + near) / farNear;
	//mat[3][3] = 1.f;
}

void satMat4::OrthoStereoProjection(float top, float bottom, float left, float right, float near, float far, float skew, float convergence)
{
	float rightLeft = right - left;
	assert(rightLeft != 0);

	float topBottom = top - bottom;
	assert(topBottom != 0);

	float farNear = far - near;
	assert(farNear != 0);

	mat[0][0] = 2.0f / rightLeft;
	mat[0][1] = 0;
	mat[0][2] = 0;
	mat[0][3] = 0;
	mat[1][0] = 0;
	mat[1][1] = 2.0f / topBottom;
	mat[1][2] = 0;
	mat[1][3] = 0;
	mat[2][0] = 0;
	mat[2][1] = 0;
	mat[2][2] = -2.0f / farNear;
	mat[2][3] = 0;
	mat[3][0] = -(right + left) / rightLeft;
	mat[3][1] = -(top + bottom) / topBottom;
	mat[3][2] = -(far + near) / farNear;
	mat[3][3] = 1.0f;

	
		
	satMat4 mat3D;

	mat3D[0][0] = 1.0f;
	mat3D[0][1] = 0.0f;
	mat3D[0][2] = skew / convergence;
	mat3D[0][3] = skew;

	mat3D[1][0] = 0.0f;
	mat3D[1][1] = 1.0f;
	mat3D[1][2] = 0.0f;
	mat3D[1][3] = 0.0f;

	mat3D[2][0] = 0.0f;
	mat3D[2][1] = 0.0f;
	mat3D[2][2] = 1.0f;
	mat3D[2][3] = 0.0f;

	mat3D[3][0] = 0.0f;
	mat3D[3][1] = 0.0f;
	mat3D[3][2] = 0.0f;
	mat3D[3][3] = 1.0f;

	*this = (*this) * mat3D;
}






float* satMat4::Float()
{
	//data[0]  = mat[0][0];
	//data[1]  = mat[1][0];
	//data[2]  = mat[2][0];
	//data[3]  = mat[3][0];
	//		   
	//data[4]  = mat[0][1];
	//data[5]  = mat[1][1];
	//data[6]  = mat[2][1];
	//data[7]  = mat[3][1];
	//		   
	//data[8]  = mat[0][2];
	//data[9]  = mat[1][2];
	//data[10] = mat[2][2];
	//data[11] = mat[3][2];
	//
	//data[12] = mat[0][3];
	//data[13] = mat[1][3];
	//data[14] = mat[2][3];
	//data[15] = mat[3][3];

	return &mat[0][0];
}

satMat4 satMat4::Identity()
{
	return satMat4(
		1.0f, 0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f,
		0.0f, 0.0f, 1.0f, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f);
}

void satMat4::LoadIdentity()
{
	mat[0] = satVec4(1.0f, 0.0f, 0.0f, 0.0f);
	mat[1] = satVec4(0.0f, 1.0f, 0.0f, 0.0f);
	mat[2] = satVec4(0.0f, 0.0f, 1.0f, 0.0f);
	mat[3] = satVec4(0.0f, 0.0f, 0.0f, 1.0f);
		//1.0f, 0.0f, 0.0f, 0.0f,
		//0.0f, 1.0f, 0.0f, 0.0f,
		//0.0f, 0.0f, 1.0f, 0.0f,
		//0.0f, 0.0f, 0.0f, 1.0f);
}

satMat4 satMat4::CatmullRomIdentity()
{
	return satMat4(
		-0.5f, 1.5f, -1.5f, 0.5f,
		1.0f, -2.5f, 2.0f, -0.5f,
		-0.5f, 0.0f, 0.5f, 0.0f,
		0.0f, 1.0f, 0.0f, 0.0f);
}

satMat4 satMat4::BezierIdentity()
{
	return satMat4(
		-1.0f, 3.0f, -3.0f, 1.0f,
		3.0f, -6.0f, 3.0f, 0.0f,
		-3.0f, 3.0f, 0.0f, 0.0f,
		1.0f, 0.0f, 0.0f, 0.0f);
}

void satMat4::Print()
{
	std::cout << mat[0][0] << std::endl;
	std::cout << mat[1][0] << std::endl;
	std::cout << mat[2][0] << std::endl;
	std::cout << mat[3][0] << std::endl;
	std::cout << mat[0][1] << std::endl;
	std::cout << mat[1][1] << std::endl;
	std::cout << mat[2][1] << std::endl;
	std::cout << mat[3][1] << std::endl;
	std::cout << mat[0][2] << std::endl;
	std::cout << mat[1][2] << std::endl;
	std::cout << mat[2][2] << std::endl;
	std::cout << mat[3][2] << std::endl;
	std::cout << mat[0][3] << std::endl;
	std::cout << mat[1][3] << std::endl;
	std::cout << mat[2][3] << std::endl;
	std::cout << mat[3][3] << std::endl;
}

#pragma endregion satMat4functions

//===================================//
// end of satMat4::general functions //
//===================================//

#pragma endregion satMat4









































