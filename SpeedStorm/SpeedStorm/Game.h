/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include <windows.h>
#include <fstream>
//#include <vector>

#include "DebugLog.h"
#include "Mesh.h" //Include OBJ mesh
#include "ShaderProgram.h" //Include shaders
#include "Texture.h"
#include "FrameBuffer.h"
#include "ResourceManager.h"

#include "Camera.h"

//PINEAPPLE
#include "level.h"
//END PINEAPPLE



#include "Key.h"
#include "Timer.h"

#include "Sound.h"
#include "Entity.h"

#include "Bullet.h"

#include "Color.h"

#include "Video.h"

#include "Score.h"


#include "SerialPort.h"


//#include "Animation.h"

//class Game;

//#define WINDOW_WIDTH_DEFAULT			960
//#define WINDOW_HEIGHT_DEFAULT			720

#pragma region DebugMacros

#define elf else if

#define WINDOW_WIDTH_DEFAULT			1600
#define WINDOW_HEIGHT_DEFAULT			900

//#define WINDOW_WIDTH_DEFAULT			1280
//#define WINDOW_HEIGHT_DEFAULT			720

//#define FULLSCREEN_WIDTH_DEFAULT		853
//#define FULLSCREEN_HEIGHT_DEFAULT		480

#define FULLSCREEN_WIDTH_DEFAULT		1920
#define FULLSCREEN_HEIGHT_DEFAULT		1080

#define FRAMES_PER_SECOND	60
#define SECONDS_PER_FRAME	1.0f/FRAMES_PER_SECOND
#define UPDATES_PER_FRAME	16*2
#define FPU					0.0625f/2.0f	 
#define BLOOM_THRESHOLD_LOWER		0.615f
#define BLOOM_THRESHOLD_UPPER		0.9f
#define BLOOM_DOWNSCALE		4
#define SSAO_DOWNSCALE		2
#define BLOOM_BLUR_PASSES	4
#define BLOOM_HORIZONTAL_BLUR_PASSES	2
#define SSAO_BLUR_PASSES	1
#define SHADOW_RESOLUTION	8192


#define SAT_DEBUG_OUTPUT_SCENE			0		//Regular scene
#define SAT_DEBUG_OUTPUT_ALBEDO			1		//Flat colour
#define SAT_DEBUG_OUTPUT_NORMAL			2		//Normals (xyz) Does not require 4th channel
#define SAT_DEBUG_OUTPUT_POSITION		3		//View Space Positions (xyz)
//#define SAT_DEBUG_OUTPUT_AMBIENT		4		//Ambient
#define SAT_DEBUG_OUTPUT_SPECULAR		4		//Specular
#define SAT_DEBUG_OUTPUT_SHINY			5		//Specular Exponent
#define SAT_DEBUG_OUTPUT_EMISSIVE		6		//Emissive
//#define SAT_DEBUG_OUTPUT_NORMAL_POLY	7		//Normals without normal texture applied
#define SAT_DEBUG_OUTPUT_EDGE_MAP		7		//Edge Map
#define SAT_DEBUG_OUTPUT_SHADOW_MAP		8		//Shadow Map
#define SAT_DEBUG_OUTPUT_DEPTH			9		//Depth 
#define SAT_DEBUG_OUTPUT_BLOOM_OUTPUT	10		//Bloom Output
#define SAT_DEBUG_OUTPUT_SSAO			11		//Screen Space Ambient Occlusion
#define SAT_DEBUG_OUTPUT_SIZE			12		//Number of Debug Scenes





#pragma endregion DebugMacros

//PINEAPPLE
//Initializes each level
//Level* OneA = new Level(443, 12);
//END PINEAPPLE

enum Xbox360Buttons
{
	A = 0,
	B,
	X,
	Y,
	LeftBumper,
	RightBumper,
	Select,
	Start,
	LeftStickClick,
	RightStickClick
};

class XY_Coordinate;
struct InterpData;

//static int WINDOW_WIDTH;
//static int WINDOW_HEIGHT;

class Game
{
public:
	Game();
	~Game();

#pragma region staticVariables

	static int SettingsPerformance;

	static bool FullScreenActive;

	static bool PauseActive;
	static bool TitleScreenActive;
	static bool WinScreenActive;
	static bool LoadingActive;
	static bool SpecialThanksActive;
	static bool ThreeDeeActive;
	static bool ControllerActive;
	static float TransitionTitle;
	static float TransitionTitleEnd;
	static bool SkipTutorial;

	static bool SatDebugMode;
	static int	SatDebugDisplayMode;
	static int  SatDebugDisplayModeSize;
	static int	SatDebugFrameRateLimit;
	static bool SatDebugAutoPauseWindowActive;
	static bool SatDebugDrawHUDActive;
	static bool SatDebugDeferredLightActive;
	static bool SatDebugShadowMapActive;
	static bool SatDebugTimerPauseActive;
	static bool SatDebugWavyToggleActive;
	static bool SatDebugCascadeToggleActive;
	static int	SatDebugCascadeReverseActive;
	static bool SatDebugShipWaggleToggleActive;
	static bool SatDebugDrawEnemyActive;
	static bool SatDebugDrawVisualActive;
	static bool SatDebugDrawBackgroundActive;

	static float SatDebugFOVMult;

	static bool SatDebugForce16x9;

	static bool SatDebugInputLeftCtrl;
	static bool SatDebugInputRightCtrl;
	static bool SatDebugInputLeftAlt;
	static bool SatDebugInputRightAlt;
	static bool SatDebugInputLeftShift;
	static bool SatDebugInputRightShift;

	static satVec2 ControllerInputJoypadRightStick;
	static satVec2 ControllerInputJoypadLeftStick;	
	static satVec2 ControllerInputDigitalPad;
	static float ControllerInputTrigger;

	static float ControllerInputButtonA;
	static float ControllerInputButtonB;
	static float ControllerInputButtonX;
	static float ControllerInputButtonY;
	static float ControllerInputButtonSelect;
	static float ControllerInputButtonStart;
	static float ControllerInputButtonRightBumper;
	static float ControllerInputButtonLeftBumper;
	static float ControllerInputButtonRightStickClick;
	static float ControllerInputButtonLeftStickClick;


	static bool SatDebugAutoScrollActive;
	static bool SatDebugGodModeActive;
	static bool SatDebugDrunkModeActive;
	static bool SatDebugSluggishMovementActive;

	static int LightIntensityMode;
	static int LightIntensityModeSize;
	static Light LightIntensity;

	static int FULLSCREEN_WIDTH;
	static int FULLSCREEN_HEIGHT;
	static int WINDOW_WIDTH;
	static int WINDOW_HEIGHT;
	static int RENDER_WIDTH;
	static int RENDER_HEIGHT;

	static int UPDATE_COUNTER;
	static float InterpParam;

	static double HighScore;
	static double TimeInLevel;
	static double FrameInLevel;
	static double TimePaused; 
	static double ScoreEnemy;
	
	static float collisionTime;
	static const float invincibleTime;
	static const float invincibleTimeInverse;
	static float timeDead;
	static float timeDeadMax;
	static float timeDeadReverse;
	static float timeDeadRewindPosition;
	static float timeDeadRewindTime;
	
	static float timeContrast;
	static float timeContrastNegative;
	static const float timeContrastMax;

	static float timeFaceFlash;
	static const float timeFaceFlashMax;
	static const int numOfFaceFlash;
	static Texture FaceFlashTexture[10];
	static Texture VHSGrainTexture;
	static int FaceFlashPick;
	
	static float DoubleShootTime;
	static float DoubleShootTimeMax;
	static float AutoShootTime;
	static float AutoShootTimeMax;
	static float NoDamageTime;
	static float NoDamageTimeMax;

	static GLuint aviHandle;
	static Texture aviTexture;

	static bool UpScaleResolutionX;
	static bool UpScaleResolutionY;
	static int UpScaleShadowResolution;
	static int UpScaleShadowResolutionRange;

	static bool UpScaleSobelResolution;

	static float ShipShrink;
	static int ShipHealth;
	static satColor ShipHealthColor[5];
	static satColor ShipInvincibleColor[9];

	static satVec2 InputMove;
	
	static double ScoreMult;

	static satVec3 adjust;
	static satVec2 ShipSpawnPosition;

	static int levelWinFrame;

	static Score ScoreBoard;

#pragma endregion staticVariables

	bool windowSelected;

	//static double* timerTotal;
	//static double* timerOld;
	//static double* timerFrame;
		

	SoundSystem Sys;
	SoundLoc Song;
	SoundLoc SelectSong;
	SoundLoc WinGameSong;

	SoundLoc test;
	SoundLoc shoot;
	SoundLoc PewSound;
	SoundLoc FizzSound;
	SoundLoc WallVoiceSound;
	SoundLoc BzztSound;
	SoundLoc BoomSound;
	SoundLoc BlarpSound;

	SoundLoc ChargeUpSound;
	SoundLoc ChargeDownSound;

	std::vector <satVec2>		DataPoints;
	std::vector <InterpData>	DistanceTable;


	float songPitch;
	float songVolume;

	float songLowPass;
	float songHighPass;
	float songFlange;


	float CurrentDistance;
	float InterpolationParam;
	int CurrentSegment;

	static void cycleLightMode();
	static void cycleLightMode(const Light &light);

	void calcCamera();

	void setFullscreenSize(const int *width, const int *height);
	void setWindow(GLFWwindow* window);
	void setWindow(sf::Window* window);
	//void setTimers(double* timer, double* timerOld, double* timerFrame);
	void beginHell(); //New init function
	void initializeGame();
	void update();
	void updateMenu();
	void updateOld();
	void draw();
	void drawOld();
	void drawDebug();
	void drawModels();
	void computeShadowMap();
	void computeRender();

	bool FrameBufferFinalActive;
	FrameBuffer* swapFrameBuffer(FrameBuffer * active);
	
#pragma region loadInline

	inline void loadError(const std::string &errorMessage)
	{
		std::cout << errorMessage;
		system("PAUSE");
		exit(0);
	}

	inline void loadError()
	{
		SAT_LOG_ERROR("Error loading file!\nABORT ABORT AAAAAAAAAAAAAAAAAAAAAA");
		std::cout << "\n";
		system("PAUSE");
		exit(0);
	}

	inline void loadTexture(Texture* texture, const std::string &file)
	{
		if (!texture->load(file))
		{
			SAT_LOG_WARNING("WARNING: failed to load Texture: \n%s", file);
			system("PAUSE");
			exit(0);
		}
		else
		{
			
			std::cout << "Texture: \"" + file + "\"\n";
		}
	}

	inline void loadTexture(Texture* texture, const std::string &file, const float &anisotropyLevel, const GLint &filterMag = GL_LINEAR, const GLint &filterMin = GL_LINEAR_MIPMAP_LINEAR, const GLint &clampUV = GL_CLAMP_TO_EDGE)
	{
		if (!texture->load(file, anisotropyLevel, filterMag, filterMin, clampUV))
		{
			SAT_LOG_WARNING("WARNING: failed to load Texture: \n%s", file);
			system("PAUSE");
			exit(0);
		}
		else
		{
			std::cout << "Texture: \"" + file + "\"\n";
		}
	}

	inline void loadModel(Mesh* mesh, const std::string &file)
	{
		if (!mesh->LoadFile(file))
		{
			SAT_LOG_WARNING("ERROR: failed to load Model: \n%s", file);
			system("PAUSE");
			exit(0);
		}
		else
		{
			std::cout << "Model:\t \"" + file + "\"\n";
		}
	}

	inline void loadShader(ShaderProgram* shader, const std::string &vertex, const std::string &frag)
	{
		ResourceManager::AddShader(shader);
		if (!shader->load(vertex, frag))
		{
			SAT_LOG_ERROR("ERROR: failed to load Shader: \n%s", frag);
			//std::cout << "ERROR: failed to load Shader: \n\t\"" + frag + "\"\n";
			//system("PAUSE");
			//exit(0);
		}
		else
		{
			std::cout << "Shader:\t \"" + vertex + "\"\n\t \"" + frag + "\"\n";
		}
	}

	inline void loadShader(ShaderProgram* shader, const std::string &vertex, const std::string &frag, const std::string &geom)
	{
		if (!shader->load(vertex, frag, geom))
		{
			std::cout << "ERROR: failed to load Shader: \n\t\"" + frag + "\"\n";
			system("PAUSE");
			exit(0);
		}
		else
		{
			std::cout << "Shader:\t \"" + vertex  + "\"\n\t \"" + frag + "\"\n\t \"" + geom + "\"\n";
		}
	}

	inline void checkFBO(FrameBuffer* framebuffer, const std::string &file)
	{
		if (!framebuffer->CheckFBO())
		{
			loadError(file);
		}
		else
		{
			//std::cout << "Framebuffer:\t \"" + file + "\"\n";
		}
	}

	inline void checkFBO(FrameBuffer* framebuffer)
	{
		if (!framebuffer->CheckFBO())
		{
			loadError("FBO WENT KAPUT");
		}
		else
		{
			//std::cout << "Framebuffer:\t ";
			//framebuffer->PrintInfo();
		}
	}

#pragma endregion loadInline

	static inline void printDebugCommands()
	{
		printf("|=========================================================| \n" );
		printf("| REGULAR COMMANDS                                        | \n" );
		printf("|=========================================================| \n" );
		printf("|  Back      :: Toggle 3D                                 | \n" );
		printf("|  \\         :: Toggle Upscale Resolution                 | \n");
		printf("|  LCtrl & \\ :: Toggle Upscale Resolution X               | \n");
		printf("|  LAlt & \\  :: Toggle Upscale Resolution Y               | \n");
		printf("|  [         :: Lower Settings (Higher Performance)       | \n" );
		printf("|  ]         :: Higher Settings (Lower Performance)       | \n" );
		printf("|  `         :: Print Command List                        | \n" );
		printf("|=========================================================| \n" );
		printf("| DEBUG COMMANDS                                          | \n" );
		printf("|=========================================================| \n" );
		printf("|  1    :: Wireframe Toggle                               | \n" );
		printf("|  2    :: postProcessBloom                               | \n" );
		printf("|  3    :: postProcessToonShading                         | \n" );
		printf("|  4    :: postProcessGreyscalePlusEmissive               | \n" );
		printf("|  5    :: postProcessDepthOfField                        | \n" );
		printf("|  6    :: postProcessHueShift                            | \n" );
		printf("|  7    :: postProcessColorGrain                          | \n" );
		printf("|  8    :: postProcessFilmGrain                           | \n" );
		printf("|  9    :: Teleport to Beginning                          | \n" );
		printf("|  0    :: Teleport to End                                | \n" );
		printf("|  -    :: Damage Ship                                    | \n" );
		printf("|  +    :: Heal Ship                                      | \n" );
		printf("|  F11  :: Move Light                                     | \n" );
		printf("|=========================================================| \n" );
		printf("|  LCtrl & 1      :: postProcessCRTPost                   | \n" );
		printf("|  LCtrl & 2      :: postProcessScanline                  | \n" );
		printf("|  LCtrl & 3      :: postProcessRGBSplitEarthquake        | \n" );
		printf("|  LCtrl & 4      :: postProcessGreyscale                 | \n" );
		printf("|  LCtrl & 5      :: postProcessWave                      | \n" );
		printf("|  LCtrl & 6      :: Enviroment Map                       | \n" );
		printf("|  LCtrl & 7      :: Rim Light                            | \n" );
		printf("|  LCtrl & 8      :: postProcessMotionBlur                | \n" );
		printf("|  LCtrl & 9      :: postProcessFXAA                      | \n" ); 
		printf("|  LCtrl & 0      :: postProcessFogActive                 | \n" );
		printf("|  LCtrl & [      :: Decrease Shadow Resolution           | \n" );
		printf("|  LCtrl & ]      :: Increase Shadow Resolution           | \n" );
		printf("|  LCtrl & PgUp   :: + Eye Convergence                    | \n" );
		printf("|  LCtrl & PgDown :: - Eye Convergence                    | \n" );
		printf("|  LCtrl & F4     :: Crash Game (Why would you do this?)  | \n" );
		printf("|=========================================================| \n" );
		printf("|  LAlt & 1      :: SatDebugAutoScrollActive              | \n" );
		printf("|  LAlt & 2      :: SatDebugGodModeActive                 | \n" );
		printf("|  LAlt & 3      :: Something                             | \n" );
		printf("|  LAlt & 4      :: Something                             | \n" );
		printf("|  LAlt & 5      :: Something                             | \n" );
		printf("|  LAlt & 6      :: Something                             | \n" );
		printf("|  LAlt & 7      :: Something                             | \n" );
		printf("|  LAlt & 8      :: Something                             | \n" );
		printf("|  LAlt & 9      :: Something                             | \n" );
		printf("|  LAlt & 0      :: Something                             | \n" );
		printf("|  LAlt & PgUp   :: + Eye Distance                        | \n" );
		printf("|  LAlt & PgDown :: - Eye Distance                        | \n" );
		printf("|  LAlt & -      :: Prev Level                            | \n" );
		printf("|  LAlt & +      :: Next Level                            | \n" );
		printf("|=========================================================| \n" );
		printf("|  RCtrl & 1 :: postProcessSSAO                           | \n" );
		printf("|  RCtrl & 2 :: Something                                 | \n" );
		printf("|  RCtrl & 3 :: Something                                 | \n" );
		printf("|  RCtrl & 4 :: Something                                 | \n" );
		printf("|  RCtrl & 5 :: Ship Waggle                               | \n" );
		printf("|  RCtrl & 6 :: Cascade Toggle                            | \n" );
		printf("|  RCtrl & 7 :: Toggle Wavy Blocks                        | \n" );
		printf("|  RCtrl & 8 :: Toggle Deferred Light                     | \n" );
		printf("|  RCtrl & 9 :: Toggle Shadow Map                         | \n" );
		printf("|  RCtrl & 0 :: Default Display                           | \n" );
		printf("|  RCtrl & - :: Prev Display Frame                        | \n" );
		printf("|  RCtrl & + :: Next Display Frame                        | \n" );
		printf("|=========================================================| \n" );
		printf("|  RAlt & 1 :: postProcessSepia                           | \n" );
		printf("|  RAlt & 2 :: postProcessSepiaPlusEmissive               | \n" );
		printf("|  RAlt & 3 :: postProcessInvert                          | \n" );
		printf("|  RAlt & 4 :: postProcessInvertBrightness                | \n" );
		printf("|  RAlt & 5 :: Something                                  | \n" );
		printf("|  RAlt & 6 :: Something                                  | \n" );
		printf("|  RAlt & 7 :: Something                                  | \n" );
		printf("|  RAlt & 8 :: Something                                  | \n" );
		printf("|  RAlt & 9 :: Something                                  | \n" );
		printf("|  RAlt & 0 :: Something                                  | \n" );
		printf("|=========================================================| \n" );
		printf("|  I          :: Move Up                                  | \n" );
		printf("|  K          :: Move Down                                | \n" );
		printf("|  J          :: Move Right                               | \n" );			
		printf("|  L          :: Move Left                                | \n" );
		printf("|  I & LShift :: Move Fast Up                             | \n" );
		printf("|  K & LShift :: Move Fast Down                           | \n" );
		printf("|  J & LShift :: Move Fast Right                          | \n" );			
		printf("|  L & LShift :: Move Fast Left                           | \n" );
		printf("|=========================================================| \n" );
		printf("|  IMPORTANT DEBUG COMMANDS                               | \n" );
		printf("|=========================================================| \n" );
		printf("|  Left Ctrl + Left Alt + Enter                           | \n" );
		printf("|  TOGGLE DEBUG MODE                                      | \n" );
		printf("|---------------------------------------------------------| \n" );
		printf("|  Left Shift + Right Alt + Right Ctrl + Enter            | \n" );
		printf("|  WIPE HIGH SCORE LIST                                   | \n" );
		printf("|---------------------------------------------------------| \n" );
		printf("|                                                         | \n" );
		printf("|=========================================================| \n" );
		
	}

	void displayDebugFrame();

	

	void drawShadowModel(Mesh* model, Texture* tex, satMat4 &transformation = satMat4::Identity());
	void bindShadowMap(satMat4 &transformation = satMat4::Identity());
	void bindRender(satMat4 &transformation = satMat4::Identity());
	void bindRender(float lerpTime = 0.0f, satMat4 &transformation = satMat4::Identity());
	void bindRender(const satVec3* light, satMat4 &transformation = satMat4::Identity());
	void bindRender(const satVec3* light, float lerpTime = 0.0f, satMat4 &transformation = satMat4::Identity());
	void bindRender(const LightAddMult* light, satMat4 &transformation = satMat4::Identity());
	void bindRender(const LightAddMult* light, float lerpTime = 0.0f, satMat4 &transformation = satMat4::Identity());
	void bindRenderCube(const LightAddMult* light, float lerpTime = 0.0f, satMat4 &transformation = satMat4::Identity());
	void drawModelSecondary(Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* tex2, float lerpTime, satMat4 &transformation = satMat4::Identity());
	void drawModel(Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation = satMat4::Identity());
	void drawModelSecondary(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* tex2, float lerpTime, satMat4 &transformation = satMat4::Identity());
	void drawModel(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation = satMat4::Identity());
	void drawModelCube(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, Texture* cubeTex, float uReflectAmount, satMat4 &transformation = satMat4::Identity());
	void drawModelParticle(const LightAddMult* addmult, Mesh* model, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, float alphaAmount, satMat4 &transformation = satMat4::Identity());	
	
	void drawModelAlpha(const LightAddMult* addmult, Mesh* model, Texture* alpha, Texture* tex, Texture* normalTex, Texture* shinyTex, Texture* emissiveTex, satMat4 &transformation);


	void drawShip();
	void drawBlocks();
	void drawBlocksShadow();
	void drawFullBlocks();
	void bindBlocks(int& x, int& y);
	void bindBlocksShadow(int& x, int& y);

	void bindNormal();
	void bindNormal(Texture* tex);

	void bindShiny();
	void bindShiny(Texture* tex);

	void bindEmit();
	void bindEmit(Texture* tex);

	void bindSecondaryTexture();
	void bindSecondaryTexture(Texture* tex);

	void bindCubeMap();
	void bindCubeMap(Texture* tex);

	/* input callback functions */

	void ReshapeWindow(const int& width, const int& height, const bool &force = false);
	void ReshapeWindow(const bool &force = false);
	//void KeyboardUpdate();
	void KeyboardUpdate(const int& updateNumber);
	void KeyboardDebugUpdate();
	void ControllerUpdate();
	void MouseUpdate();
	void InputUpdateHead();
	void InputUpdateTail();

	void CollisionUpdate();
	char CollisionAutoScrollUpdate(satVec2 &pos);
	char CollisionVerticalUpdate(satVec2 &pos);
	char CollisionHorizontalUpdate(satVec2 &pos);
	LevelMatrixBlock CollisionCheck(satVec2 &pos);

	void Cheater(float amount = 1.0f);

	void buildSSAO();

	bool checkCollision(const satVec2& position);

	void toonShading();
	void deferredRender();
	void postProcessing();
	void CollisionInvertProcess(FrameBuffer *ActiveFrameBuffer);

	void damageShip(const bool &force = false);
	void winLevel();
	void setupLevel();
	void winGame();
	void setSong();

	void displayTitleScreen();
	void displaySpecialThanksScreen();
	void displayLoadingScreen();
	void displayWinScreen();
	void displayHighScore();

	void drawPaused();

	void drawAnimation();
	void drawAnimationShadow();

	
	void setUpInterlaceTexture();

	/* Data Members */
#pragma region DataMembers


	Timer *updateTimer	= nullptr;
	GLFWwindow* window;
	sf::Window* sfWindow;


	//RenderManager* _RenderMan;
	//ResourceManager* _ResourceMan = new ResourceManager();
	//std::vector<Entity*> entityTest;

	float TotalGameTime = 0.0f;
	satVec3 TotalRotation = 0.0f;
	satVec3 TotalTranslation = 0.0f;

	int LevelWidth = 0;
	int LevelHeight = 0;
	Mesh Block;
	
	std::vector<satVec3> BlockPosition;
	

	std::vector<PointLight> lightPoint;
	//std::vector<satVec3> lightColors;

	std::vector<PointLight> lightShipHealth;
	std::vector<PointLight> lightShipInvincible;
	std::vector<PointLight> lightBullet;
	std::vector<PointLight> lightEnemy;

	satVec2 JoystickLPos;
	satVec2 JoystickRPos;
	satVec2 DpadPos;
	//PINEAPPLE
	Level* curLevel;

	SerialPort* arduino;
	//Level OneA(433, 12);
	//ENDPINEAPPLE

	// Shader Programs //
	//ShaderProgram FunctionTest;
	ShaderProgram StaticGeometry;
	ShaderProgram NoEffect;
	ShaderProgram BloomHighPass;	//Bloom input
	ShaderProgram BlurHorizontal;	//Bloom	blur
	ShaderProgram BlurVertical;		//Bloom blur
	ShaderProgram BloomComposite;	//Bloom merge/output

	ShaderProgram SSAOShader;
	ShaderProgram SSAOBlurHorizontal;
	ShaderProgram SSAOBlurVertical;
	ShaderProgram SSAOComposite;
	
	ShaderProgram GBufferPass;		//Deferred Rendering
	ShaderProgram GBufferCubePass;		//Deferred Rendering
	ShaderProgram GBufferPassAlphaDiscard;		//Deferred Rendering
	ShaderProgram GBufferCubePassAlphaDiscard;		//Deferred Rendering
	ShaderProgram DeferredLighting;	//Deferred Rendering
	ShaderProgram PointLightShader;
	ShaderProgram SobelPass;		//Toon Shading
	ShaderProgram SobelThickPass;	//Super Toon Shading
	ShaderProgram EdgeMultPass;		//Get dem edges in
	ShaderProgram AnimateModel;
	std::vector<ShaderProgram> FogShader;		//Create good fog (transform this into an array for different levels of fog in the future)
	ShaderProgram GreyscalePostShader;				
	ShaderProgram GreyscalePlusEmissivePostShader;
	ShaderProgram SepiaPostShader;
	ShaderProgram SepiaPlusEmissivePostShader;
	ShaderProgram InvertPostShader;
	ShaderProgram FilmGrainPostShader;
	ShaderProgram ColorGrainPostShader;
	ShaderProgram PauseVHSPostShader;
	ShaderProgram ContrastPostShader;	
	ShaderProgram HueShiftPostShader;
	ShaderProgram CRTPostShader;
	ShaderProgram ScanlinePostShader;
	ShaderProgram RGBSplitPostShader;
	ShaderProgram RGBSplitFastPostShader;
	std::vector<ShaderProgram> ChromaticAberrationPostShader;
	ShaderProgram *ChromaticAberrationPostShaderPtr;
	std::vector<ShaderProgram>  DepthOfFieldPostShader;
	ShaderProgram *DepthOfFieldPostShaderPtr;
	ShaderProgram WavePostShader;
	ShaderProgram WavePost2Shader;
	ShaderProgram MotionBlurPostShader;
	ShaderProgram FXAAPostShader;
	ShaderProgram SonarShader;
	ShaderProgram ACESPostShader;

	ShaderProgram SMAAEdgeDetectPostShader;
	ShaderProgram SMAAWeightsPostShader;
	ShaderProgram SMAAPostShader;
	//
	ShaderProgram PassThroughShader;
	ShaderProgram PassThroughFlipShader;
	ShaderProgram PassThroughFlipMultShader;
	ShaderProgram PassThroughMultShader;
	ShaderProgram PassThroughShadowShader;
	ShaderProgram TestPostShader;

	ShaderProgram ThreeDeePostShader;
	
	ShaderProgram ParticlesShader;

	bool RimLightActive;
	bool EnviromentMapActive;
	bool OrthographicActive;
	

	static bool postProcessFogActive;
	static bool postProcessBloomActive;
	static bool postProcessSSAOActive;
	static bool postProcessBlurSSAOActive;
	static bool postProcessSobelActive;
	static bool postProcessSobelThickActive;
	static bool postProcessFilmGrainActive;
	static bool postProcessColorGrainActive;
	static bool postProcessPauseVHSActive;
	static bool postProcessInvertColourActive;
	static bool postProcessGreyscaleActive;
	static bool postProcessGreyscalePlusEmissiveActive;
	static bool postProcessSepiaActive;
	static bool postProcessSepiaPlusEmissiveActive;
	static bool postProcessInvertActive;
	static bool postProcessInvertBrightnessActive;
	static bool postProcessContrastActive;
	static bool postProcessHueShiftActive;
	static bool postProcessCRTActive;
	static bool postProcessScanlineActive;
	static bool postProcessRGBSplitEarthquakeActive;
	static bool postProcessRGBSplitActive;
	static bool postProcessChromaticAberrationActive;
	static int postProcessDepthOfFieldActive;
	static bool postProcessWaveActive;
	static bool postProcessWave2Active;
	static bool postProcessMotionBlurActive;
	static bool postProcessFXAAActive;
	
	static bool postProcessSonarActive;
	static bool postProcessACESActive;

	//
	static bool postProcessTestActive;

	static int DetailChromaticAberration;


#pragma region DebugMember

	bool boolNothing;

	
	

#pragma endregion DebugMember




	// These textures are what you'd expext
	Texture DefaultTexture;			//Flat 255 (White)
	Texture DefaultNormal;			//Flat (0.5, 0.5, 1.0) -> (0.0, 0.0, 1.0)
	Texture DefaultSpecular;		//Flat 224
	Texture DefaultEmissive;		//Flat 0 (Black)
	Texture DefaultShiny;			//Flat 224

	Texture DefaultTestTexture;		//Jacob's spaceship texture
	Texture DefaultTestNormal;		//Really bumpy surface
	Texture DefaultTestSpecular;	//16x16 Grid between 33 & 224
	Texture DefaultTestEmissive;	//16x16 Grid testing multiple values of RGB + Greyscale
	Texture DefaultTestShiny;	//16x16 Grid between 33 & 224
	Texture DefaultTestHeight;		//64x64 Grid between 33 & 224




	//Scene
	Mesh Background;

	// Entities //
	Mesh Ship;
	Mesh ShipSmall;
	//Animation ShipAnimation;
	Frame ShipFrame;

	//PINEAPPLE
	Mesh TopBlock;
	Mesh BottomBlock;
	Mesh FrontBlock;
	Mesh FullBlock;
	Mesh HalfBlock;
	Mesh Die;
	Mesh ChessKing;
	Mesh ChessQueen;
	Mesh ChessBishop;
	Mesh BulletMesh;
	
	Mesh SmokeMesh;	
	Mesh TutorialMesh;
	Mesh TutorialLessTallMesh;
	Mesh TutorialTallMesh;
	Mesh TutorialWideMesh;

	Texture TutorialTexture[3];
	
	Mesh DisplayTest;

	std::vector<BulletGood> bulletGood;
	std::vector<ParticleN> smokeParticle;
	std::vector<BulletBad> bulletBad;

	//ENDPINEAPPLE

	Texture BackgroundTexture[2];

	Texture TitleScreenTexture;
	Texture TitleScreenTextTexture;
	Texture TitleScreenTextAlphaTexture;
	Texture TitleScreenBackgroundTexture;

	Mesh TitleScreen;
	Mesh TitleScreenLarge;
	Mesh TitleScreenSmall;

	Texture SpecialThanksScreenTexture;
	Texture SpecialThanksScreenAlphaTexture;
	Texture SpecialThanksScreenBackgroundTexture;
	


	Texture AmbientCubeMapTexture;
	Texture ShadowMapFlashlight;

	Texture NowLoading;

	Texture GroundTexture;
	Texture ShipTexture[4];
	Texture ShipNormalTexture[4];
	Texture ShipEmissiveTexture[4];
	Texture ShipInvincibleTexture[4];
	Texture ShipShinyTexture[4];

	//PINEAPPLE
	//std::vector<Texture> BlockTexture;
	std::vector<LightAddMult> BlockColor;
	//std::vector<Mesh> BlockModel;

	//std::vector<Texture> BlockNormal;
	Texture BlockHeight;
	//Texture TopBlockTexture;
	//Texture	BottomBlockTexture;
	//Texture	FrontBlockTexture;
	Texture	DieTexture;
	Texture DieNormalTexture;
	Texture	ChessKingTexture;
	Texture	ChessQueenTexture;
	Texture	ChessBishopTexture;
	Texture FullBlockTexture;
	Texture EnemyEmissiveTexture;
	Texture BulletGoodEmissiveTexture;
	Texture PickupEmissiveTexture;

	Texture DisplayTestTexture;
	//END PINEAPPLE


	// FrameBuffers //
	//FrameBuffer MainBuffer;
	FrameBuffer GBuffer; //for deferred rendering
	FrameBuffer DeferredComposite;
	FrameBuffer FinalComposite;
	FrameBuffer LeftEyeBuffer;
	FrameBuffer ShadowMap;
	FrameBuffer EdgeMap;
	FrameBuffer PreviousFrame;
	FrameBuffer FullScreenOutput;
	//Bloom work buffers
	FrameBuffer WorkBuffer1_2;
	FrameBuffer WorkBuffer2_2;
	FrameBuffer WorkBuffer1_4;
	FrameBuffer WorkBuffer2_4;
	FrameBuffer WorkBuffer1_8;
	FrameBuffer WorkBuffer2_8;

	FrameBuffer SSAOWorkBuffer1;
	FrameBuffer SSAOWorkBuffer2;
	
	FrameBuffer BackgroundBuffer; //for background rendering

	Texture NumberTex[10];
	Texture NumberColonTex;
	Texture NumberPeriodTex;
	Texture NumberBlankTex;
	
	Camera mainCamera;
	Camera rightCamera;
	Camera leftCamera;
	
	Camera secondaryCamera;
	Camera *activeCamera;




	satMat4 CameraTransform;
	satMat4 CameraProjection;
	//satMat4 ModelTransform;
	satMat4 LightTransform;
	satMat4 ShadowTransform;
	satMat4 ShadowProjection;
	satMat4 ViewToShadowMap;

	satMat4 ViewToShadowRightMap;
	satMat4 ViewToShadowLeftMap;

	satMat4* ViewToShadowMapActive;

	bool wireframe;
	
	unsigned wireframeToggle = GL_FILL;

	float timeL;
	int counterL;
	int arraySizeL;
	std::vector<satVec3> points;
	float incrementConst;


	satVec3 colours[5];
	int counterC;

	float fovA[3];
	int counterF;

	

	//sf::Window _Window;
	//sf::ContextSettings cs;
#pragma endregion DataMembers
	
	void UpdateDistanceTable();
	satVec2 DistanceCorrect(float &distance);

private:
	
};

class XY_Coordinate
{
public:
	XY_Coordinate(float _x, float _y, float _u, float _v);
	~XY_Coordinate();
	float x, y;
	float u, v;
};

struct InterpData
{
	InterpData()
	{
		Distance = 0.0f;
		InterpolationParam = 0.0f;
		Segment = 0;
	}

	InterpData(float distance, float interpolationParam, unsigned int segment)
	{
		Distance = distance;
		InterpolationParam = interpolationParam;
		Segment = segment;
	}

	float Distance;
	float InterpolationParam;
	unsigned int Segment;

};

