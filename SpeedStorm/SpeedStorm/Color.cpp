/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Color.h"

satColor & satColor::operator= (const float &a)
{
	r = a;
	g = a;
	b = a;

	return *this;
}

//satColor & satColor::operator= (const satVec3 &a)
//{
//	r = a.r;
//	g = a.g;
//	b = a.b;
//	return *this;
//}

satColor & satColor::operator= (const int &a)
{
	r = a * 0.003921568f;	//0.003921568f
	g = a * 0.003921568f;	//	1 / 255
	b = a * 0.003921568f;

	return *this;
}