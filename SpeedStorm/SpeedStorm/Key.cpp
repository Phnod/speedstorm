/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once
#include "Key.h"
#include "Timer.h"

#include <string>
#include <sstream>
#include <vector>
#include <iterator>


namespace sat
{
	namespace ard
	{
		//char *port_name = "\\\\.\\COM5";
		char *port_name = "\\\\.\\COM5";

		char incomingData[MAX_DATA_LENGTH];

		SerialPort* arduino;
		bool arduinoInit = false;

		SerialPort* InitArduino()
		{
			if (arduino == nullptr)
			{
				arduino = new SerialPort(port_name);
			}

			if (arduino->isConnected())
			{
				arduinoInit = true;
				std::cout << "Connection Established" << std::endl;
			}
			else std::cout << "ERROR, check port name\n";

			return arduino;
		}

		std::vector<std::string> split(const std::string &s, char delim)
		{
			std::stringstream ss(s);
			std::string item;
			std::vector<std::string> tokens;
			while (getline(ss, item, delim))
			{
				tokens.push_back(item);
			}
			return tokens;
		}

		char* ReadPort()
		{
			int read_result = arduino->readSerialPort(incomingData, MAX_DATA_LENGTH);
			incomingData[read_result%MAX_DATA_LENGTH] = 0;
			return incomingData;
		}

		bool ParseLine(satVec3 &vec3, int &shoot)
		{
			std::vector<std::string> strings = split(ReadPort(), ',');
			std::vector<int> inputValues;
			inputValues.resize(strings.size());
			for (int i = 0; i < inputValues.size(); ++i)
			{
				inputValues[i] = (atoi(strings[i].c_str()));
			}

			//if (inputValues.size() >= 3)
			//	printf("%i\t%i\t%i\n", inputValues[0], inputValues[1], inputValues[2]);
			//else
			//	printf("Error Parsing Data\n");

			if (inputValues.size() <= 2)
				return false;

			if (inputValues.size() > 6)
				return false;

			if (inputValues[0] != -1 && inputValues[5] != -1)
				return false;

			if (inputValues[4] < 0 || inputValues[4] > 1)
				return false;

			vec3.x = inputValues[1];
			vec3.y = inputValues[2];
			vec3.z = inputValues[3];
			shoot = inputValues[4];

			return true;
		}
		

		bool ArduinoActive()
		{
			return arduinoInit;
		}
	}

	namespace key
	{
		const int numOfKeys = sf::Keyboard::KeyCount;
		
		std::map<std::string, enum sf::Keyboard::Key> MapKeys;

		//long long int KeyPressed[numOfKeys];
		float KeyPressed[numOfKeys];
		

		

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		void Init()
		{
			Map();
		}

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		void Map()
		{
			//MapKeys[NULL]			= sf::Keyboard::Unknown;

			MapKeys["A"]	= sf::Keyboard::A;
			MapKeys["B"]	= sf::Keyboard::B;
			MapKeys["C"]	= sf::Keyboard::C;
			MapKeys["D"]	= sf::Keyboard::D;
			MapKeys["E"]	= sf::Keyboard::E;
			MapKeys["F"]	= sf::Keyboard::F;
			MapKeys["G"]	= sf::Keyboard::G;
			MapKeys["H"]	= sf::Keyboard::H;
			MapKeys["I"]	= sf::Keyboard::I;
			MapKeys["J"]	= sf::Keyboard::J;
			MapKeys["K"]	= sf::Keyboard::K;
			MapKeys["L"]	= sf::Keyboard::L;
			MapKeys["M"]	= sf::Keyboard::M;
			MapKeys["N"]	= sf::Keyboard::N;
			MapKeys["O"]	= sf::Keyboard::O;
			MapKeys["P"]	= sf::Keyboard::P;
			MapKeys["Q"]	= sf::Keyboard::Q;
			MapKeys["R"]	= sf::Keyboard::R;
			MapKeys["S"]	= sf::Keyboard::S;
			MapKeys["T"]	= sf::Keyboard::T;
			MapKeys["U"]	= sf::Keyboard::U;
			MapKeys["V"]	= sf::Keyboard::V;
			MapKeys["W"]	= sf::Keyboard::W;
			MapKeys["X"]	= sf::Keyboard::X;
			MapKeys["Y"]	= sf::Keyboard::Y;
			MapKeys["Z"]	= sf::Keyboard::Z;

			MapKeys["a"]	= sf::Keyboard::A;
			MapKeys["b"]	= sf::Keyboard::B;
			MapKeys["c"]	= sf::Keyboard::C;
			MapKeys["d"]	= sf::Keyboard::D;
			MapKeys["e"]	= sf::Keyboard::E;
			MapKeys["f"]	= sf::Keyboard::F;
			MapKeys["g"]	= sf::Keyboard::G;
			MapKeys["h"]	= sf::Keyboard::H;
			MapKeys["i"]	= sf::Keyboard::I;
			MapKeys["j"]	= sf::Keyboard::J;
			MapKeys["k"]	= sf::Keyboard::K;
			MapKeys["l"]	= sf::Keyboard::L;
			MapKeys["m"]	= sf::Keyboard::M;
			MapKeys["n"]	= sf::Keyboard::N;
			MapKeys["o"]	= sf::Keyboard::O;
			MapKeys["p"]	= sf::Keyboard::P;
			MapKeys["q"]	= sf::Keyboard::Q;
			MapKeys["r"]	= sf::Keyboard::R;
			MapKeys["s"]	= sf::Keyboard::S;
			MapKeys["t"]	= sf::Keyboard::T;
			MapKeys["u"]	= sf::Keyboard::U;
			MapKeys["v"]	= sf::Keyboard::V;
			MapKeys["w"]	= sf::Keyboard::W;
			MapKeys["x"]	= sf::Keyboard::X;
			MapKeys["y"]	= sf::Keyboard::Y;
			MapKeys["z"]	= sf::Keyboard::Z;

			MapKeys["Num0"]	= sf::Keyboard::Num0;
			MapKeys["Num1"]	= sf::Keyboard::Num1;
			MapKeys["Num2"]	= sf::Keyboard::Num2;
			MapKeys["Num3"]	= sf::Keyboard::Num3;
			MapKeys["Num4"]	= sf::Keyboard::Num4;
			MapKeys["Num5"]	= sf::Keyboard::Num5;
			MapKeys["Num6"]	= sf::Keyboard::Num6;
			MapKeys["Num7"]	= sf::Keyboard::Num7;
			MapKeys["Num8"]	= sf::Keyboard::Num8;
			MapKeys["Num9"]	= sf::Keyboard::Num9;

			MapKeys["Numpad0"] = sf::Keyboard::Numpad0;
			MapKeys["Numpad1"] = sf::Keyboard::Numpad1;
			MapKeys["Numpad2"] = sf::Keyboard::Numpad2;
			MapKeys["Numpad3"] = sf::Keyboard::Numpad3;
			MapKeys["Numpad4"] = sf::Keyboard::Numpad4;
			MapKeys["Numpad5"] = sf::Keyboard::Numpad5;
			MapKeys["Numpad6"] = sf::Keyboard::Numpad6;
			MapKeys["Numpad7"] = sf::Keyboard::Numpad7;
			MapKeys["Numpad8"] = sf::Keyboard::Numpad8;
			MapKeys["Numpad9"] = sf::Keyboard::Numpad9;

			MapKeys["0"]	= sf::Keyboard::Num0;
			MapKeys["1"]	= sf::Keyboard::Num1;
			MapKeys["2"]	= sf::Keyboard::Num2;
			MapKeys["3"]	= sf::Keyboard::Num3;
			MapKeys["4"]	= sf::Keyboard::Num4;
			MapKeys["5"]	= sf::Keyboard::Num5;
			MapKeys["6"]	= sf::Keyboard::Num6;
			MapKeys["7"]	= sf::Keyboard::Num7;
			MapKeys["8"]	= sf::Keyboard::Num8;
			MapKeys["9"]	= sf::Keyboard::Num9;

			MapKeys["Escape"]		= sf::Keyboard::Escape;
			MapKeys["LControl"]		= sf::Keyboard::LControl;
			MapKeys["LShift"]		= sf::Keyboard::LShift;
			MapKeys["LAlt"]			= sf::Keyboard::LAlt;
			MapKeys["LSystem"]		= sf::Keyboard::LSystem;
			MapKeys["RControl"]		= sf::Keyboard::RControl;
			MapKeys["RShift"]		= sf::Keyboard::RShift;
			MapKeys["RAlt"]			= sf::Keyboard::RAlt;
			MapKeys["RSystem"]		= sf::Keyboard::RSystem;
			MapKeys["Menu"]			= sf::Keyboard::Menu;
			MapKeys["LBracket"]		= sf::Keyboard::LBracket;
			MapKeys["RBracket"]		= sf::Keyboard::RBracket;
			MapKeys["["]			= sf::Keyboard::LBracket;
			MapKeys["]"]			= sf::Keyboard::RBracket;
			MapKeys["SemiColon"]	= sf::Keyboard::SemiColon;
			MapKeys[";"]			= sf::Keyboard::SemiColon;
			MapKeys["Comma"]		= sf::Keyboard::Comma;
			MapKeys[","]			= sf::Keyboard::Comma;
			MapKeys["Period"]		= sf::Keyboard::Period;
			MapKeys["."]			= sf::Keyboard::Period;
			MapKeys["Quote"]		= sf::Keyboard::Quote;
			MapKeys["'"]			= sf::Keyboard::Quote;
			MapKeys["Slash"]		= sf::Keyboard::Slash;
			MapKeys["/"]			= sf::Keyboard::Slash;
			MapKeys["BackSlash"]	= sf::Keyboard::BackSlash;
			MapKeys["Backslash"]	= sf::Keyboard::BackSlash;
			MapKeys["\\"]			= sf::Keyboard::BackSlash;
			MapKeys["Tilde"]		= sf::Keyboard::Tilde;
			MapKeys["`"]			= sf::Keyboard::Tilde;
			MapKeys["Equal"]		= sf::Keyboard::Equal;
			MapKeys["="]			= sf::Keyboard::Equal;
			MapKeys["Dash"]			= sf::Keyboard::Dash;
			MapKeys["-"]			= sf::Keyboard::Dash;
			MapKeys["Space"]		= sf::Keyboard::Space;
			MapKeys[" "]			= sf::Keyboard::Space;
			MapKeys["Return"]		= sf::Keyboard::Return;
			MapKeys["Enter"]		= sf::Keyboard::Return;
			MapKeys["Start"]		= sf::Keyboard::Return;
			MapKeys["BackSpace"]	= sf::Keyboard::BackSpace;
			MapKeys["Back"]			= sf::Keyboard::BackSpace;
			MapKeys["Tab"]			= sf::Keyboard::Tab;
			MapKeys["Tabulation"]	= sf::Keyboard::Tab;
			MapKeys["PageUp"]		= sf::Keyboard::PageUp;
			MapKeys["PgUp"]			= sf::Keyboard::PageUp;
			MapKeys["PageDown"]		= sf::Keyboard::PageDown;			
			MapKeys["PgDown"]		= sf::Keyboard::PageDown;
			MapKeys["PgDn"]			= sf::Keyboard::PageDown;
			MapKeys["End"]			= sf::Keyboard::End;
			MapKeys["Home"]			= sf::Keyboard::Home;
			MapKeys["Insert"]		= sf::Keyboard::Insert;
			MapKeys["Delete"]		= sf::Keyboard::Delete;
			MapKeys["Add"]			= sf::Keyboard::Add;
			MapKeys["+"]			= sf::Keyboard::Add;
			MapKeys["Subtract"]		= sf::Keyboard::Subtract;
			MapKeys["--"]			= sf::Keyboard::Subtract;
			MapKeys["Multiply"]		= sf::Keyboard::Multiply;
			MapKeys["*"]			= sf::Keyboard::Multiply;
			MapKeys["Divide"]		= sf::Keyboard::Divide;
			MapKeys["//"]			= sf::Keyboard::Divide;

			MapKeys["Left"]		= sf::Keyboard::Left;
			MapKeys["Right"]	= sf::Keyboard::Right;
			MapKeys["Up"]		= sf::Keyboard::Up;
			MapKeys["Down"]		= sf::Keyboard::Down;

			MapKeys["Numpad0"]	= sf::Keyboard::Numpad0;
			MapKeys["Numpad1"]	= sf::Keyboard::Numpad1;
			MapKeys["Numpad2"]	= sf::Keyboard::Numpad2;
			MapKeys["Numpad3"]	= sf::Keyboard::Numpad3;
			MapKeys["Numpad4"]	= sf::Keyboard::Numpad4;
			MapKeys["Numpad5"]	= sf::Keyboard::Numpad5;
			MapKeys["Numpad6"]	= sf::Keyboard::Numpad6;
			MapKeys["Numpad7"]	= sf::Keyboard::Numpad7;
			MapKeys["Numpad8"]	= sf::Keyboard::Numpad8;
			MapKeys["Numpad9"]	= sf::Keyboard::Numpad9;

			MapKeys["F1"]	= sf::Keyboard::F1;
			MapKeys["F2"]	= sf::Keyboard::F2;
			MapKeys["F3"]	= sf::Keyboard::F3;
			MapKeys["F4"]	= sf::Keyboard::F4;
			MapKeys["F5"]	= sf::Keyboard::F5;
			MapKeys["F6"]	= sf::Keyboard::F6;
			MapKeys["F7"]	= sf::Keyboard::F7;
			MapKeys["F8"]	= sf::Keyboard::F8;
			MapKeys["F9"]	= sf::Keyboard::F9;
			MapKeys["F10"]	= sf::Keyboard::F10;
			MapKeys["F11"]	= sf::Keyboard::F11;
			MapKeys["F12"]	= sf::Keyboard::F12;
			MapKeys["F13"]	= sf::Keyboard::F13;
			MapKeys["F14"]	= sf::Keyboard::F14;
			MapKeys["F15"]	= sf::Keyboard::F15;

			MapKeys["Pause"]	= sf::Keyboard::Pause;
		}

		void setVar()
		{
			//Sets the keyPressed array to default to false
			for (int i = 0; i < numOfKeys; i++)
			{
				KeyPressed[i] = 0;
			}
		}

		void update()
		{
			
			
		}

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		float IsPressed(const sf::Keyboard::Key &key)
		{
			return KeyPressed[key];
		}

		float IsPressed(const std::string &key)
		{
			std::cout << "WARNING: TIME WASTING FUNCTION!\t IsPressed(" << key << ")\n";
			if (MapKeys.find(key) != MapKeys.end())
			{				
				return IsPressed(MapKeys[key]);
			}

			std::cout << key << " is not a key.\n";
			return -1;
		}

		bool CheckPressed(const sf::Keyboard::Key &key)
		{
			if (KeyPressed[key] == 0 && sf::Keyboard::isKeyPressed(key))
			{
				return KeyPressed[key] += sf::Keyboard::isKeyPressed(key) * Timer::framef(); //returns true
			}
			KeyPressed[key] += sf::Keyboard::isKeyPressed(key) * Timer::framef();
			KeyPressed[key] *= sf::Keyboard::isKeyPressed(key);
			return false;
		}

		bool CheckPressed(const sf::Keyboard::Key &key, const float &num)
		{
			if (KeyPressed[key] == 0 && sf::Keyboard::isKeyPressed(key))
			{
				return KeyPressed[key] += sf::Keyboard::isKeyPressed(key) * Timer::framef(); //returns true
			}
			else if	(KeyPressed[key] >= num)
			{
				KeyPressed[key] = 0; //returns false
				return false;
			}
			KeyPressed[key] += sf::Keyboard::isKeyPressed(key) * Timer::framef();
			KeyPressed[key] *= sf::Keyboard::isKeyPressed(key);	
			return false;
		}

		bool CheckPressed(const std::string &key)
		{
			std::cout << "WARNING: TIME WASTING FUNCTION!\t CheckPressed(" << key << ")\n";
			if (MapKeys.find(key) != MapKeys.end())
			{
				return CheckPressed(MapKeys[key]);
			}

			std::cout << key << " is not a key.\n";
			return false;
		}
		
		bool CheckPressed(const std::string &key, const float &num)
		{
			std::cout << "WARNING: TIME WASTING FUNCTION!\t IsPressed(" << key << ", "<< num << ")\n";
			if (MapKeys.find(key) != MapKeys.end())
			{
				return CheckPressed(MapKeys[key], num);
			}

			std::cout << key << " is not a key.\n";
			return false;
		}

		bool CheckKey(const sf::Keyboard::Key &key)
		{
			if (sf::Keyboard::isKeyPressed(key))
			{
				if (KeyPressed[key] <= 0)
				{
					return KeyPressed[key] = Timer::framef(); //Raise to positive pressed state
				}
				return KeyPressed[key] += Timer::framef(); 
			}
			if (KeyPressed[key] >= 0)
			{
				return !(KeyPressed[key] = -Timer::framef()); //Lower to negative released state
			}
			return !(KeyPressed[key] -= Timer::framef());
		}

		bool CheckKey(const std::string &key)
		{
			std::cout << "WARNING: TIME WASTING FUNCTION!\t CheckKey(" << key << ")\n";
			if (MapKeys.find(key) != MapKeys.end())
			{
				return CheckKey(MapKeys[key]);
			}
			std::cout << key << " is not a key.\n";
			return false;
		}



		int toggleBoolean(const sf::Keyboard::Key &key, bool &boolean)
		{
			if (sat::key::CheckPressed(key))
			{
				boolean = !boolean;
				if(boolean)
					return 1;
				return -1;
			}
			return 0;
		}

		int toggleBoolean(const std::string &key, bool &boolean)
		{
			std::cout << "WARNING: TIME WASTING FUNCTION!\t toggleBoolean(" << key << ", " << boolean << ")\n";
			if (sat::key::CheckPressed(key))
			{
				boolean = !boolean;
				if(boolean)
					return 1;
				return -1;
			}
			return 0;
		}


		bool addInt(const sf::Keyboard::Key &key, int &variable, const int &number)
		{
			if (sat::key::CheckPressed(key))
			{
				variable += number;
				return true;
			}
			return false;
		}


		void CheckPressedAll()
		{
			
		}
















	}




	namespace joy
	{
		const int numOfButtons = sf::Joystick::ButtonCount;
		//long long int ButtonPressed[numOfButtons];
		float ButtonPressed[numOfButtons];

		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////

		void Init()
		{
			Map();
		}

		void Map()
		{

		}


		float IsPressed(const int button)
		{
			return ButtonPressed[button];
		
		}

		bool CheckPressed(const int button)
		{
			if (ButtonPressed[button] == 0 && sf::Joystick::isButtonPressed(0, button))
			{
				return ButtonPressed[button] += sf::Joystick::isButtonPressed(0, button) * Timer::framef(); //returns true
			}
			ButtonPressed[button] += sf::Joystick::isButtonPressed(0, button) * Timer::framef();
			ButtonPressed[button] *= sf::Joystick::isButtonPressed(0, button);
			return false;
		}

		bool CheckPressed(const int button, const float &num)
		{
			if (ButtonPressed[button] == 0 && sf::Joystick::isButtonPressed(0, button))
			{
				return ButtonPressed[button] += sf::Joystick::isButtonPressed(0, button) * Timer::framef(); //returns true
			}
			else if (ButtonPressed[button] >= num)
			{
				ButtonPressed[button] = 0; //returns false
				return false;
			}
			ButtonPressed[button] += sf::Joystick::isButtonPressed(0,button) * Timer::framef();
			ButtonPressed[button] *= sf::Joystick::isButtonPressed(0,button);
			return false;
		}

		bool CheckPressed(const int button, const int &num)
		{
			if (ButtonPressed[button] == 0 && sf::Joystick::isButtonPressed(0, button))
			{
				return ButtonPressed[button] += sf::Joystick::isButtonPressed(0, button) * Timer::framef(); //returns true
			}
			else if (ButtonPressed[button] >= num)
			{
				ButtonPressed[button] = 0; //returns false
				return false;
			}
			ButtonPressed[button] += sf::Joystick::isButtonPressed(0, button) * Timer::framef();
			ButtonPressed[button] *= sf::Joystick::isButtonPressed(0, button);
			return false;
		}

		bool CheckButton(const int button)
		{
			if (sf::Joystick::isButtonPressed(0, button))
			{
				if (ButtonPressed[button] <= 0)
				{
					return ButtonPressed[button] = Timer::framef(); //Raise to positive pressed state
				}
				return ButtonPressed[button] += Timer::framef();
			}
			if (ButtonPressed[button] >= 0)
			{
				return !(ButtonPressed[button] = -Timer::framef()); //Lower to negative released state
			}
			return !(ButtonPressed[button] -= Timer::framef());
		}






		float CheckAxis(const sf::Joystick::Axis joy, const float deadzone)
		{
			if (sf::Joystick::getAxisPosition(0, joy) > deadzone * 100.0f)
			{
				return Min((sf::Joystick::getAxisPosition(0, joy) - deadzone * 100.0f) * 100.0f / (100.0f - deadzone) * satMath::SQRT_TWO, 100.0f);
			}

			if (sf::Joystick::getAxisPosition(0, joy) < -deadzone * 100.0f)
			{
				return Max((sf::Joystick::getAxisPosition(0, joy) + deadzone * 100.0f) * 100.0f / (100.0f - deadzone) * satMath::SQRT_TWO, -100.0f);
			}

			return 0.0f;
		}

		//void 
	}

	namespace mouse
	{
		const int numOfKeys = sf::Mouse::ButtonCount;
		//std::map<std::string, enum sf::Mouse::Button> MapKeys;
		long long int KeyPressed[numOfKeys];

		int window_width = 1600;
		int window_height = 900;


		void Init()
		{
			
		}

		void Update() 
		{
			
		}

		void Update(int width, int height)
		{
			window_width = width;
			window_height = height;
		}

		satVec2 Pos()
		{
			return satVec2((float) PosX(), (float) PosY());
		}

		int PosX() 
		{
			return sf::Mouse::getPosition().x;
		}

		int PosY()
		{
			return sf::Mouse::getPosition().y;
		}



		long long IsClicked(const sf::Mouse::Button key)
		{
			return KeyPressed[key];
		}

		bool CheckClicked(const sf::Mouse::Button key)
		{
			if (KeyPressed[key] == 0 && sf::Mouse::isButtonPressed(key))
			{
				return KeyPressed[key] += sf::Mouse::isButtonPressed(key); //returns true
			}
			KeyPressed[key] += sf::Mouse::isButtonPressed(key);
			KeyPressed[key] *= sf::Mouse::isButtonPressed(key);
			return false;
		}

		bool CheckClicked(const sf::Mouse::Button key, const long long &num)
		{
			if (KeyPressed[key] == 0 && sf::Mouse::isButtonPressed(key))
			{
				return KeyPressed[key] += sf::Mouse::isButtonPressed(key); //returns true
			}
			else if (KeyPressed[key] >= num)
			{
				KeyPressed[key] = 0; //returns false
				return false;
			}
			KeyPressed[key] += sf::Mouse::isButtonPressed(key);
			KeyPressed[key] *= sf::Mouse::isButtonPressed(key);
			return false;
		}

		bool CheckClicked(const sf::Mouse::Button key, const int &num)
		{
			if (KeyPressed[key] == 0 && sf::Mouse::isButtonPressed(key))
			{
				return KeyPressed[key] += sf::Mouse::isButtonPressed(key); //returns true
			}
			else if (KeyPressed[key] >= num)
			{
				KeyPressed[key] = 0; //returns false
				return false;
			}
			KeyPressed[key] += sf::Mouse::isButtonPressed(key);
			KeyPressed[key] *= sf::Mouse::isButtonPressed(key);
			return false;
		}

		bool CheckButton(const sf::Mouse::Button key)
		{
			if (sf::Mouse::isButtonPressed(key))
			{
				if (KeyPressed[key] <= 0)
				{
					return KeyPressed[key] = 1; //Raise to positive pressed state
				}
				return KeyPressed[key] += 1;
			}
			if (KeyPressed[key] >= 0)
			{
				return !(KeyPressed[key] = -1); //Lower to negative released state
			}
			return !(KeyPressed[key] -= 1);
		}


	}
}

