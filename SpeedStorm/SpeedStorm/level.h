/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include <string>
#include <assert.h>
#include <vector>
#include "LevelList.h"
#include "Texture.h"
#include "Mesh.h"
#include "GlobalSettings.h"

class Level;

class LevelManager
{
public:
	LevelManager();
	


	static std::vector<Level> level;
	static const std::vector<std::string> levelFilenames;
	//static 
	static int levelCurrent;
	static int levelFinalNum;
	
	static std::vector<std::string> MakeLevelList();
	static std::vector<Level> MakeLevels();
	static void LoadLevelAssets();

	static int currentSongPlaying;

	static bool tutorialActive;
	//
};

class Level
{
public:
	Level(const std::string* filename);
	~Level();
	
	void Load();
	void LoadLevelSounds();
	void Unload();
	void UnloadLevelSounds();
	void ResetLevelSounds();

	const std::string* filename;
	
	unsigned Seed = 0;
	
	float WinPoint;
	bool FogActive;
	unsigned FogDetail = 2;
	unsigned FogLayers = 1;
	bool WaveActive;
	bool BloomActive;
	bool ContrastActive;
	bool DOFActive;
	bool ScanlineActive;
	bool SSAOActive;
	bool ShadowOrthographicActive = false;

	bool BlockWavyActive;
	bool BlockCascadeActive;

	bool SonarActive = false;

	bool SobelActive		= false;
	bool SobelThickToggle	= false;

	int LevelScore; 
	float LevelTime;

	//float FogNear;
	//float FogFar;
	//satColor FogColor;



	sat::level::ListBlockColorToggle Toggle;
	
	
	//std::vector<int> soundPositionList;
	std::vector<sat::level::ListSound> listSound;
	std::vector<sat::level::ListLight> listLight;
	std::vector<sat::level::ListFog> listFog;
	std::vector<sat::level::ListEye> listEye;
	std::vector<sat::level::ListWave> listWave;
	std::vector<sat::level::ListSpeed> listSpeed;
	std::vector<sat::level::ListBloom> listBloom;
	std::vector<sat::level::ListContrast> listContrast;
	std::vector<sat::level::ListEnvironment> listEnvironment;
	std::vector<sat::level::ListShipLight> listShipLight;
	std::vector<sat::level::ListGreyscale> listGreyscale;
	std::vector<sat::level::ListDOF> listDOF;
	std::vector<sat::level::ListScanline> listScanline;
	std::vector<sat::level::ListSobel> listSobel;
	std::vector<sat::level::ListSonar> listSonar;
	std::vector<sat::level::ListShadow> listShadow;

	satVec3 ShadowRotationTotal;
	satVec3 ShadowTranslationRotationTotal;

	std::vector<sat::level::ListBlockColor> listBlockColor;

	std::vector<sat::level::ListMusic> listMusic;

	//0 is first value, 1 is ordered, 2 is random  
	

	//Texture*	BlockDiffuseTexture;
	//Texture*	BlockNormalTexture;
	//Texture*	BlockSpecularTexture;
	//Texture*	BlockEmissiveTexture;
	//Mesh*		BlockModel;
	//
	//std::string BlockDiffuseTextureFilename;
	//std::string BlockNormalTextureFilename;
	//std::string BlockSpecularTextureFilename;
	//std::string BlockEmissiveTextureFilename;
	//std::string BlockModelFilename;

	Visual Block;
	std::vector<sat::level::ListVisual> listVisual;
	std::vector<sat::level::ListEnemy>  listEnemy;
	std::vector<sat::level::BackgroundVisual> listBackgroundVisual;

	std::string SongFilename;
	SoundLoc* SongPtr;
	SoundLoc Song;
	float SongTempo;
	float SongBounce;
	bool SongRepeat;

	float shipPos;

	satVec2 wrapPoint = satVec2(0.0f, 0.0f);

	void setShipPosition(const float &p);

	satColor	calcAmbient();
	satColor	calcDiffuse();
	satColor	calcSpecular();
	satColor	calcRim();

	satColor	calcFogColor(const int &layer = 0);
	float		calcFogNear(const int &layer = 0);
	float		calcFogFar(const int &layer = 0);
	satVec2		calcFogRange(const int &layer = 0);
	satColor	calcFogBottomColor(const int &layer = 0);
	satColor	calcFogTopColor(const int &layer = 0);
	satVec3		calcFogPosMult(const int &layer = 0);
	float		calcFogEmissivePass(const int &layer = 0);
	Texture*	calcFogTex0(const int &layer = 0);
	Texture*	calcFogTex1(const int &layer = 0);
	float		calcFogLerp(const int &layer = 0);
	satVec2		calcFogWaveIntensity(const int &layer = 0);
	satVec2		calcFogWaveCount(const int &layer = 0);
	satVec2		calcFogWaveTime(const int &layer = 0);
	satVec2		calcFogVelocity(const int &layer = 0);
	satVec2		calcFogOffset(const int &layer = 0);
	satVec2		calcFogOffsetMult(const int &layer = 0);
	satVec2		calcFogOffsetPosMult(const int &layer = 0);

	satVec2		calcEyeShift();
	float		calcEyeDistanceMult();
	float		calcEyeConvergence();

	satVec2		calcWaveIntensity();
	satVec2		calcWaveCount();
	satVec2		calcWaveTime();

	satVec2		calcSpeedMult();
	float		calcSpeedXMult();
	float		calcSpeedYMult();
	satVec4		calcSpeed();
	satVec2		calcSpeedShrinkMult();

	
	float		calcGreyscaleAmount();
	float		calcGreyscaleSepiaAmount();

	void		calcSoundPlay();

	bool		calcBloomActive();
	float		calcBloomUpperThreshold();
	float		calcBloomLowerThreshold();

	float		calcMusicPitch();
	float		calcMusicVolume();

	bool		calcContrastActive();
	float		calcContrastAmount();

	float		calcEnvironmentBackground();

	LightAddMult	calcBlockMaterial(int x = 0, int y = 0);
	satColor		calcBlockAmbientAdd(int x = 0, int y = 0);
	satColor		calcBlockAmbientMult(int x = 0, int y = 0);
	satColor		calcBlockDiffuseAdd(int x = 0, int y = 0);
	satColor		calcBlockDiffuseMult(int x = 0, int y = 0);
	satColor		calcBlockSpecularAdd(int x = 0, int y = 0);
	satColor		calcBlockSpecularMult(int x = 0, int y = 0);
	satColor		calcBlockEmissiveAdd(int x = 0, int y = 0);
	satColor		calcBlockEmissiveMult(int x = 0, int y = 0);
	float			calcBlockShinyAdd(int x = 0, int y = 0);
	float			calcBlockShinyMult(int x = 0, int y = 0);

	bool	calcBlockMaterialToggle();
	bool	calcBlockAmbientAddToggle();
	bool	calcBlockAmbientMultToggle();
	bool	calcBlockDiffuseAddToggle();
	bool	calcBlockDiffuseMultToggle();
	bool	calcBlockSpecularAddToggle();
	bool	calcBlockSpecularMultToggle();
	bool	calcBlockEmissiveAddToggle();
	bool	calcBlockEmissiveMultToggle();
	bool	calcBlockShinyAddToggle();
	bool	calcBlockShinyMultToggle();


	float	calcShipLightMultiplier();
	float	calcShipLightConstant();
	float	calcShipLightLinear();
	float	calcShipLightQuad();
	float	calcShipLightRadius();
	float	calcShipLightInnerRadius();
	
	

	//void	calcSongBegin();
	//void	calcSongUnload();

	float calcDOFFocus();
	float calcDOFClamp();
	float calcDOFBias();

	satVec2 calcScanlineAmount();
	int calcScanlineThresholdX(); 
	int calcScanlineThresholdY(); 
	int calcScanlineModX(); 
	int calcScanlineModY(); 

	satVec2 calcSobelThreshold(); //x = normal, y = depth

	satVec4 calcSonarColor();
	satVec4 calcSonarParam();

	float	calcShadowFOV();
	satVec3 calcShadowRotation();
	satVec3 calcShadowTranslation();
	satVec3 calcShadowTranslationRotation();



	satVec3 calcRandomRotations(sat::level::ListVisual* visual, int x, int y);


	void setBlockType(Mesh* model, Texture* diffuse, Texture* normal, Texture* specular, Texture* emissive);

	void readLevel(const std::string* fileName);	//what reads the level. Saves it to 2-Dimensional vector 'lMatrix'
	void readLevel(const std::string* fileName, const int &w, const int &h);	//what reads the level. Saves it to 2-Dimensional vector 'lMatrix'
	void removeStuff(); //temp method for converting non-existant stuff into empty space
	
	
	void SetMatrixConditions();
	void SetMatrixCondition(const int &i, const int &j);
	void ChangeTo(satVec2 pos);
	//Set the 2D vector matrices conditional bools based on what the letters are
	//This will save us from so many && and || in the future


	//Doing template-y stuff for differentiating the different game objects within the level
	template <class T>
	T displayLevel();

	//Size of Level
	int X;
	int Y;

	//The level, stored in a single array. (Level stored in chars)
	std::vector<std::vector<char>> lMatrix;

	//Level conditionals
	std::vector<std::vector<bool>> lCollisionMatrix;
	std::vector<std::vector<bool>> lBreakableMatrix;
	std::vector<std::vector<bool>> lShootableMatrix;
	std::vector<std::vector<bool>> lKillMatrix;
	std::vector<std::vector<bool>> lHealthMatrix;
	std::vector<std::vector<bool>> lInvincibleMatrix;
	std::vector<std::vector<bool>> lDoubleShotMatrix;
	std::vector<std::vector<bool>> lAutoShotMatrix;
	std::vector<std::vector<char>> lChangeToMatrix;
	std::vector<std::vector<std::vector<int>>> lRandomMatrix;

};

class LevelMatrixBlock
{
public:
	LevelMatrixBlock();
	char block;
	bool collision;
	bool breakable;
	bool shootable;
	bool kill;
	bool invincible;
	bool doubleShoot;
	bool autoShoot;
	char changeTo;
	bool health;
	std::vector<int> random;
};
