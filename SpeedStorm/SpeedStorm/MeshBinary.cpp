/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#define _CRT_SECURE_NO_WARNINGS //Remove warnings from deprecated functions. Shut up, Microsoft.
#include "MeshBinary.h"
#include <fstream>
#include <iostream>

#define CHAR_BUFFER_SIZE 128 //Size of the character buffer.
#define BUFFER_OFFSET(i) ((char *)0 + (i))

MeshBinary::MeshBinary(){

}

MeshBinary::~MeshBinary(){

}
//OBJ face structure

union translate
{
public:
	float m_array[9];
	char m_charArray[sizeof(float) * 9];

};

struct MeshFace
{
	MeshFace()
	{
		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;

		textureUVs[0] = 0;
		textureUVs[1] = 0;
		textureUVs[2] = 0;

		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;

	}
	MeshFace(unsigned v1, unsigned v2, unsigned v3,
		unsigned t1, unsigned t2, unsigned t3,
		unsigned n1, unsigned n2, unsigned n3)
	{
		vertices[0] = v1;
		vertices[1] = v2;
		vertices[2] = v3;

		textureUVs[0] = t1;
		textureUVs[1] = t2;
		textureUVs[2] = t3;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;
	}

	unsigned vertices[3];
	unsigned textureUVs[3];
	unsigned normals[3];

};

struct MeshFaceFloat
{
	MeshFaceFloat()
	{
		vertices[0] = 0;
		vertices[1] = 0;
		vertices[2] = 0;

		textureUVs[0] = 0;
		textureUVs[1] = 0;
		textureUVs[2] = 0;

		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;

	}
	MeshFaceFloat(float v1, float v2, float v3,
		float t1, float t2,
		float n1, float n2, float n3)
	{
		vertices[0] = v1;
		vertices[1] = v2;
		vertices[2] = v3;
		vertices[3] = 1.0f;

		textureUVs[0] = t1;
		textureUVs[1] = t2;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;
	}
	union
	{
		struct
		{
			float vertices[4];
			float textureUVs[2];
			float normals[3];
		};

		float face[9];
	};

};





bool MeshBinary::WriteBinaryFile(const std::string &file, std::vector<MeshFaceFloat> &faceData)
{
	std::ofstream output(file);
	
	for (int i = 0; i < faceData.size(); ++i)
	{
		translate temp;
		
		temp.m_array[0] = faceData[i].face[0];	
		temp.m_array[1] = faceData[i].face[1];
		temp.m_array[2] = faceData[i].face[2];
		temp.m_array[3] = faceData[i].face[3];
		temp.m_array[4] = faceData[i].face[4];
		temp.m_array[5] = faceData[i].face[5];
		temp.m_array[6] = faceData[i].face[6];
		temp.m_array[7] = faceData[i].face[7];
		temp.m_array[8] = faceData[i].face[8];

		output.write(temp.m_charArray, sizeof(float)*9);

	}
	//faceData
	return true;
}

//Load a mesh from file and sent it to v-memory
bool MeshBinary::LoadFile(const std::string &file)
{
	std::ifstream input;
	input.open(file);

	if (!input)
	{
		std::cout << "Error: Could not open file \"" << file << "\"!\n";
		return false;

	}
	char inputString[CHAR_BUFFER_SIZE];

	satVec3 topCorn; //Bounding box corner
	satVec3 btmCorn; //Bounding box corner

	//Unique data
	std::vector<satVec3> vertexData;
	std::vector<satVec2> textureData;
	std::vector<satVec3> normalData;
	//index/face data
	std::vector<MeshFace> faceData;
	//Use the face data to create an array of verts, uvs, and normals for faster use in openGL
	std::vector<float> unPackedVertexData;
	std::vector<float> unPackedTextureData;
	std::vector<float> unPackedNormalData;

	std::vector<MeshFaceFloat> unPackedFaceData;

	while (!input.eof()) //While the file has not ended, continue pulling in data.
	{
		input.getline(inputString, CHAR_BUFFER_SIZE);
		if (std::strstr(inputString, "#") != nullptr) //strstr, or string string, checks to see if a string contains a substring. It's like substring, 
			//but with the entire string instead of a set amount of characters. For this line, we want to handle comments.
		{
			//This line is a comment. This line in the code is also a comment, coincidence?
			continue;

		}
		else if (std::strstr(inputString, "vt") != nullptr)
		{
			//This line has uv data
			satVec2 temp;
			std::sscanf(inputString, "vt %f %f", &temp.x, &temp.y);
			textureData.push_back(temp);

		}
		else if (std::strstr(inputString, "vn") != nullptr)
		{
			//This line has normal data
			satVec3 temp;
			std::sscanf(inputString, "vn %f %f %f", &temp.x, &temp.y, &temp.z);
			normalData.push_back(temp);

		}
		else if (std::strstr(inputString, "v") != nullptr)
		{
			//This line has vertex data
			satVec3 temp;
			std::sscanf(inputString, "v %f %f %f", &temp.x, &temp.y, &temp.z);
			vertexData.push_back(temp);

			if (vertexData.size() <= 1)
			{
				topCorn = { temp.x, temp.y, temp.z };
				btmCorn = { temp.x, temp.y, temp.z };
			}

			if (temp.x > topCorn.x)
			{
				topCorn.x = temp.x;
			}
			if (temp.x < btmCorn.x)
			{
				btmCorn.x = temp.x;
			}
			if (temp.y > topCorn.y)
			{
				topCorn.y = temp.y;
			}
			if (temp.y < btmCorn.y)
			{
				btmCorn.y = temp.y;
			}
			if (temp.z > topCorn.z)
			{
				topCorn.z = temp.z;
			}
			if (temp.z < btmCorn.z)
			{
				btmCorn.z = temp.z;
			}
		}


		else if (std::strstr(inputString, "f") != nullptr)
		{
			//This line contains face data.
			MeshFace temp;

			std::sscanf(inputString, "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&temp.vertices[0], &temp.textureUVs[0], &temp.normals[0],
				&temp.vertices[1], &temp.textureUVs[1], &temp.normals[1],
				&temp.vertices[2], &temp.textureUVs[2], &temp.normals[2]
				);
			faceData.push_back(temp);

		}
	}
	input.close();

	//satVec3 midPos;	//Middle of the obj, calculated by taking the average of the corners
	//
	//midPos = { (btmCorn.x + topCorn.x) / 2, (btmCorn.y + topCorn.y) / 2, (btmCorn.z + topCorn.z) / 2 };
	//
	//for (unsigned i = 0; i < vertexData.size(); i++)
	//{
	//	vertexData[i] -= midPos;
	//}

	//Unpack the data
	for (unsigned i = 0; i < faceData.size(); i++)
	{
		for (unsigned j = 0; j < 3; j++)
		{
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].x);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].y);
			unPackedVertexData.push_back(vertexData[faceData[i].vertices[j] - 1].z);//We use -1 because OBJ specs start at 1 instead of 0.

			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].u);
			unPackedTextureData.push_back(textureData[faceData[i].textureUVs[j] - 1].v);

			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].x);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].y);
			unPackedNormalData.push_back(normalData[faceData[i].normals[j] - 1].z);
		}
	}

	unPackedFaceData.resize(faceData.size());

	for (unsigned i = 0; i < faceData.size(); i++)
	{
		unPackedFaceData.push_back(MeshFaceFloat(
			unPackedVertexData[0], unPackedVertexData[1], unPackedVertexData[2],
			unPackedTextureData[0], unPackedTextureData[1],
			unPackedNormalData[0], unPackedNormalData[1], unPackedNormalData[2]));
	}

	_NumFaces = faceData.size();
	_NumVert = _NumFaces * 3;

	//remove .obj, add .bin at end of file name
	WriteBinaryFile(file, unPackedFaceData);

	//Send data to OpenGL.
	//glGenVertexArrays(1, &VAO);
	//glGenBuffers(1, &VBO_Vert);
	//glGenBuffers(1, &VBO_UVs);
	//glGenBuffers(1, &VBO_Normals);
	//
	//glBindVertexArray(VAO);
	////The VAO keeps track of what we're doing with the VBOs until it is unbound.
	////This lets us keep track of what we specify for all our VBOs and their shader data, instead of repeating what's inside the buffers each time.
	////It's like handing OpenGL a list instead of orally telling it each frame.
	//
	//glEnableVertexAttribArray(0); //Vertex
	//glEnableVertexAttribArray(1); //Uvs
	//glEnableVertexAttribArray(2); //Normals
	//
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Vert);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedVertexData.size(), &unPackedVertexData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)0, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	////Uvs
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_UVs);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedTextureData.size(), &unPackedTextureData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)1, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 2, BUFFER_OFFSET(0));
	////Normals
	//glBindBuffer(GL_ARRAY_BUFFER, VBO_Normals);
	//glBufferData(GL_ARRAY_BUFFER, sizeof(float) * unPackedNormalData.size(), &unPackedNormalData[0], GL_STATIC_DRAW);
	//glVertexAttribPointer((GLuint)2, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 3, BUFFER_OFFSET(0));
	//
	////OBJ data is now modern GL ready!
	//
	////Begin cleanup
	//
	//glBindBuffer(GL_ARRAY_BUFFER, 0); //Unbind VBO
	//glBindVertexArray(0); //Unbind VAO

	vertexData.clear(); //Clear the vectors from RAM now that everything's in the GPU.
	textureData.clear();
	normalData.clear();
	faceData.clear();
	unPackedVertexData.clear();
	unPackedTextureData.clear();
	unPackedNormalData.clear();

	return true;
}
//release data from system and GPU memory
void MeshBinary::Unload() //Clears the mesh data from the GPU
{
	//Remove buffers in the opposite order they were created
	glDeleteBuffers(1, &VBO_Normals);
	glDeleteBuffers(1, &VBO_UVs);
	glDeleteBuffers(1, &VBO_Vert);
	glDeleteVertexArrays(1, &VAO);
	//Clear out contents of variables.
	VBO_Normals = 0;
	VBO_UVs = 0;
	VBO_Vert = 0;
	VAO = 0;

	_NumFaces = 0;
	_NumVert = 0;
}

unsigned int MeshBinary::GetNumFaces() const
{
	return _NumFaces;
}
unsigned int MeshBinary::GetNumVertices() const
{
	return _NumVert;
}