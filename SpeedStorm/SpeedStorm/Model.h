/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#pragma once

#include "Node.h"
#include "ResourceManager.h"
#include <memory>
#include <string>
