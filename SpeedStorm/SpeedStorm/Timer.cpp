/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Timer.h"

double Timer::current = 0.0;
double Timer::old = 0.0;
double Timer::frame = 0.0;

Timer::~Timer()
{
}
