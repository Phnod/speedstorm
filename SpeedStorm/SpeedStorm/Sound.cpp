/*
===========================================================================

SpeedStorm Source Code
Copyright (C) 2015-2017 Stephen Thompson

===========================================================================
*/

#include "Sound.h"
#include "Math.h"

SoundSystem * SoundLoc::SManager = nullptr;

void error(FMOD_RESULT result)
{
	if (result != FMOD_OK)
	{
		printf("FMOD error! (%d) %s\n", result, FMOD_ErrorString(result));
		
		//system("pause");
		//exit(-1);
	}
}

SoundSystem::SoundSystem()
{
	init();
}

SoundSystem::~SoundSystem()
{
	error(system->close());
}

void SoundSystem::init()
{
	SoundLoc::SManager = this;
	
	error(FMOD::System_Create(&system));

	error(system->getVersion(&version));

	if (version < FMOD_VERSION)
	{
		//printf("[WARNING] Wrong version: %08x \tYou should be using: %08x\n", version, FMOD_VERSION);
	}

	error(system->getNumDrivers(&numDrivers));

	if (numDrivers == 0)
	{
		error(system->setOutput(FMOD_OUTPUTTYPE_NOSOUND));
	}
	else
	{
		error(system->getDriverCaps(0, &caps, 0, &speakermode));

		error(system->setSpeakerMode(speakermode));       /* Set the user selected speaker mode. */

		if (caps & FMOD_CAPS_HARDWARE_EMULATED)             /* The user has the 'Acceleration' slider set to off!  This is really bad for latency!. */
		{                                                   /* You might want to warn the user about this. */
			error(system->setDSPBufferSize(1024, 10));
		}

		error(system->getDriverInfo(0, name, 256, 0));

		if (strstr(name, "SigmaTel"))   /* Sigmatel sound devices crackle for some reason if the format is PCM 16bit.  PCM floating point output seems to solve it. */
		{
			error(system->setSoftwareFormat(48000, FMOD_SOUND_FORMAT_PCMFLOAT, 0, 0, FMOD_DSP_RESAMPLER_LINEAR));
		}
	}

	if (system->init(128, FMOD_INIT_3D_RIGHTHANDED, 0) == FMOD_ERR_OUTPUT_CREATEBUFFER)         /* Ok, the speaker mode selected isn't supported by this soundcard.  Switch it back to stereo... */
	{
		error(system->setSpeakerMode(FMOD_SPEAKERMODE_STEREO));
		printf("TEST");
		error(system->init(128, FMOD_INIT_3D_RIGHTHANDED, 0));/* ... and re-init. */
	}

	//Set the distance units. (meters/feet etc).
	error(system->set3DSettings(1.0, DISTANCEFACTOR, 1.0f));

	listenerPos = satVec3(0, 0, 0);
	listenerVel = satVec3(0, 0, 0);
	forward = satVec3(0.0f, 0.0f, 1.0f );
	up = satVec3(0.0f, 1.0f, 0.0f );
	//printf("TEST");




}

void SoundSystem::update(satVec3 &pos, satVec3 &vel, satVec3 &forward, satVec3 &up)
{
	FMOD_VECTOR tmpPos, tmpVel, tmpFor, tmpUp;
	
	tmpPos = pos;
	tmpVel = vel;
	tmpFor = forward;
	tmpUp = up;

	listenerPos = tmpPos;
	error(system->set3DListenerAttributes(0, &tmpPos, &tmpVel, &tmpFor, &tmpUp));
	system->update();
}

SoundLoc::SoundLoc()
{
	soundPos = satVec3(0, 0, 0);
	soundVel = satVec3(0, 0, 0);
	zeroVec =  satVec3(0, 0, 0);
}

bool SoundLoc::loadFromFile(std::string file, bool is3D, bool loop)
{
	if(!loaded)
	{
		is3DSound = is3D;
		FMOD_RESULT result = SManager->system->createSound(file.c_str(), is3D ? FMOD_3D : FMOD_2D, 0, &sound);
		

		if (result != FMOD_OK)
		{
			printf("[ERROR] FMOD! (%d) %s\n", result, FMOD_ErrorString(result));
			//system("pause");
			return false;
		}
		
		error(sound->set3DMinMaxDistance(0.5f * DISTANCEFACTOR, 40.0f * DISTANCEFACTOR));
		error(sound->setMode(loop ? FMOD_LOOP_NORMAL : FMOD_LOOP_OFF));

		error(SManager->system->createDSPByType(FMOD_DSP_TYPE_FLANGE, &dspFlange));
		error(dspFlange->setParameter(FMOD_DSP_FLANGE_RATE, 1.0f));			
		error(dspFlange->setParameter(FMOD_DSP_FLANGE_DRYMIX, 1.0f));
		error(dspFlange->setParameter(FMOD_DSP_FLANGE_WETMIX, 0.0f));
		
		error(SManager->system->createDSPByType(FMOD_DSP_TYPE_LOWPASS, &dspLow));
		error(dspLow->setParameter(FMOD_DSP_LOWPASS_CUTOFF, 5000000.0f));
		
		error(SManager->system->createDSPByType(FMOD_DSP_TYPE_HIGHPASS, &dspHigh));
		error(dspHigh->setParameter(FMOD_DSP_HIGHPASS_CUTOFF, 0.0f));
		
		loaded = true;
	}

	return true;
}

void SoundLoc::release()
{
	if(loaded)
		error(sound->release());
	loaded = false;
	dspAdded = false;
}

void SoundLoc::playSound()
{
	soundPos = satVec3( 10.0f * DISTANCEFACTOR, 0.0f, 0.0f );
	soundVel = satVec3(0.0f);

	error(SManager->system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel));
	if(!dspAdded)
	{
		error(channel->addDSP(dspFlange, 0));
		error(channel->addDSP(dspHigh, 0));
		error(channel->addDSP(dspLow, 0));
		//error(channel->addDSP(dspEcho, 0));

		dspAdded = true;
	}

	if (is3DSound)
	{
		error(channel->setMode(FMOD_3D));
		error(channel->set3DAttributes(&soundPos, &soundVel));
	}
	else
	{
		error(channel->setMode(FMOD_2D));
	}
	error(channel->setPaused(false));
}

void SoundLoc::playSound(float freqMod)
{
	soundPos = satVec3( 10.0f * DISTANCEFACTOR, 0.0f, 0.0f );
	soundVel = satVec3(0.0f);

	
	error(SManager->system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel));
	if(!dspAdded)
	{
		error(channel->addDSP(dspFlange, 0));
		error(channel->addDSP(dspHigh, 0));
		error(channel->addDSP(dspLow, 0));
		//error(channel->addDSP(dspEcho, 0));

		dspAdded = true;
	}

	float freq = 44100.0f;

	if(freqMod > 1.0f)
	{
		if(rand() % 2 == 0)
		{
			freq = Lerp(freq, freq * freqMod, rand() % 10000 / 10000.0f);
			//freq += freq * freqMult * (rand() % 10000 / 10000.0f);
		}
		else
		{
			freq = Lerp(freq, freq / freqMod, rand() % 10000 / 10000.0f);
			//freq -= freq * freqMult * (rand() % 10000 / 10000.0f);
		}
	}
	//float freq = 44100 * ((rand() % (10000) / (5000.0 / (1 - freqMod))) + (freqMod));
	channel->setFrequency(freq);


	if (is3DSound)
	{
		error(channel->setMode(FMOD_3D));
		error(channel->set3DAttributes(&soundPos, &soundVel));
	}
	else
	{
		error(channel->setMode(FMOD_2D));
	}
	error(channel->setPaused(false));
}


void SoundLoc::playSound(float volume, float freqMult, float freqRandom)
{
	soundPos = satVec3( 10.0f * DISTANCEFACTOR, 0.0f, 0.0f );
	soundVel = satVec3(0.0f);

	
	error(SManager->system->playSound(FMOD_CHANNEL_FREE, sound, true, &channel));
	if(!dspAdded)
	{
		error(channel->addDSP(dspFlange, 0));
		error(channel->addDSP(dspHigh, 0));
		error(channel->addDSP(dspLow, 0));
		//error(channel->addDSP(dspEcho, 0));

		dspAdded = true;
	}

	float freq = 44100.0f * freqMult;

	if(freqRandom > 1.0f)
	{
		if(rand() % 2 == 0)
		{
			freq = Lerp(freq, freq * freqRandom, rand() % 10000 / 10000.0f);
			//freq += freq * freqMult * (rand() % 10000 / 10000.0f);
		}
		else
		{
			freq = Lerp(freq, freq / freqRandom, rand() % 10000 / 10000.0f);
			//freq -= freq * freqMult * (rand() % 10000 / 10000.0f);
		}
	}


	//float freq = 44100 * ((rand() % (10000) / (5000.0 / (1 - freqMod))) + (freqMod));
	channel->setVolume(volume);
	channel->setFrequency(freq);
	


	if (is3DSound)
	{
		error(channel->setMode(FMOD_3D));
		error(channel->set3DAttributes(&soundPos, &soundVel));
	}
	else
	{
		error(channel->setMode(FMOD_2D));
	}
	error(channel->setPaused(false));
}


void SoundLoc::stopSound()
{
	if (isPlaying())
	{		
		//error(channel->setPaused(true));
		error(channel->stop());
		channel = nullptr;
		dspAdded = false;
	}
}

bool SoundLoc::isPlaying()
{
	if (channel == nullptr)
	{
		return false;
	}

	bool result = false;
	FMOD_RESULT lastError = channel->isPlaying(&result);
	if (lastError == FMOD_ERR_INVALID_HANDLE ||
		lastError == FMOD_ERR_CHANNEL_STOLEN)
	{
		lastError = FMOD_OK;
		return false;
	}
	else
	{
		error(lastError);
		return result;
	}
}

void SoundLoc::update()
{
	if (isPlaying() && is3DSound)
	{
		error(channel->set3DAttributes(&soundPos, &soundVel));
	}
}

void SoundLoc::changePitch(float freq)
{
	error(channel->setFrequency(44100.0f * freq));
}

void SoundLoc::changeVolume(float volume)
{
	error(channel->setVolume(volume));
}



void SoundLoc::initDSPHighpass()
{
	//error(channel->addDSP(dspHigh, 0));
	error(dspHigh->setActive(true));
}

void SoundLoc::setDSPHighpass(const float &param)
{
	error(dspLow->setParameter(FMOD_DSP_HIGHPASS_CUTOFF, param));
}

void SoundLoc::initDSPLowpass()
{
	//error(channel->addDSP(dspLow, 0));
	error(dspLow->setActive(true));
}

void SoundLoc::setDSPLowpass(const float &param)
{
	error(dspLow->setParameter(FMOD_DSP_LOWPASS_CUTOFF, param));
}

void SoundLoc::initDSPFlange()
{
	//error(channel->addDSP(dspFlange, 0));
	error(dspFlange->setActive(true));
}

void SoundLoc::setDSPFlange(const float &param)
{
	//error(SManager->dspFlange->setParameter(FMOD_DSP_FLANGE_RATE, param));
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_DRYMIX, param));
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_WETMIX, 1 - param));
}

void SoundLoc::setDSPFlange(const float &param, const float &rate, const float &depth)
{
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_DRYMIX, param));
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_WETMIX, 1 - param));
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_RATE, rate));
	error(dspFlange->setParameter(FMOD_DSP_FLANGE_DEPTH, depth));
}

void SoundLoc::initDSPEcho()
{
	//error(channel->addDSP(dspLow, 0));
	error(dspEcho->setActive(true));
}

void SoundLoc::setDSPEcho(const float &param, const float &decay)
{
	error(dspEcho->setParameter(FMOD_DSP_ECHO_DELAY, param));
	error(dspEcho->setParameter(FMOD_DSP_ECHO_DECAYRATIO, decay));
}