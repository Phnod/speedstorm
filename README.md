The first game I made in OpenGL/C++.

Looking back, there's tons of mistakes I made when programming this game, mostly
due to me not really understanding proper game programming conventions at the
time, or even how to use classes to their full extent. A lot was jumbled 
together into a mess that I'd like to eventually fix in the future.

Speedstorm is a simple side-scrolling shoot 'em up.
Players control a swift ship that hurtles through a variety of levels. 
I handled almost all of the programming for the project, from the level editor, 
gameplay, audio, UX, to the graphics which featured deferred shading, several 
post-processing effects, AVI video playback, and stereoscopic 3D support for 
certain displays. 

I was extremely passionate about graphics at the time, I ended up adding 
feature after feature. The game became infamous at my university, being 
simply known as "Shaders: The Game". 

Somehow though we won the Best Overall Gameplay & Best 2nd Year Game awards 
at the end of year school contest. 
